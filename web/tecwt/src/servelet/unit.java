package servelet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datas.DBCmd;
import datas.DateTime;
import datas.Log;
import datas.Unit;

/**
 * Servlet implementation class login
 */
@WebServlet("/unit")
public class unit extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public unit() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGetPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		doGetPost(request, response);
	}

	protected void doGetPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		String func = request.getParameter("action");
		String json;

		if (func.equals("login")) {
			json = login(request);
		} else if (func.equals("logout")) {
			json = logout(request);
		} else if (func.equals("entry")) {
			json = entry(request);
		} else if (func.equals("remove")) {
			json = delete(request);
		} else {
			json = "{\"status\":\"error\"\n";
			json += ("\"errorcode\":\"unit-1\",\n");
			json += ("\"message\":\"処理が判別できません\",\n}");
		}

		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write(json);
	}

	private String login(HttpServletRequest request) {
		StringBuilder sb = new StringBuilder();

		String oidStr = request.getParameter("oid");
		int oid = 0;

		try {
			oid = Integer.parseInt(oidStr);
		} catch (Exception e) {
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"unit-11\",\n");
			sb.append("\"message\":\"oid(組織ID)が判別できませんでした\"\n}");
			return sb.toString();
		}

		String unitID = request.getParameter("unitid");
		if (unitID == null) {
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"unit-12\",\n");
			sb.append("\"message\":\"端末IDが取得できませんでした\"\n}");
			return sb.toString();
		}

		Unit u = Unit.GetUnit(oid, unitID);
		if (u == null) {
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"unit-13\",\n");
			sb.append("\"message\":\"この端末は登録されていません\"\n}");
			return sb.toString();
		}

		if (!u.isEnable()) {
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"unit-14\",\n");
			sb.append("\"message\":\"この端末からの入力は受け付けられません\"\n}");
			return sb.toString();
		}

		String pass = request.getParameter("pass");
		if (!u.getPass().equals(pass)) {
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"unit-15\",\n");
			sb.append("\"message\":\"ログインに失敗しました\"\n}");
			return sb.toString();
		}

		//セッションを保存
		HttpSession s = request.getSession(true);
		s.setAttribute("oid", oid);
		s.setAttribute("unitid", unitID);

		DBCmd cmd = new DBCmd("INSERT INTO log (note, idate, itime) "
				+ "VALUES(?, ?, ?)");
		try {
			cmd.setStrParam(1, "CreateSessionID:" + s.getId());
			cmd.setIntParam(2, DateTime.GetTodayInt());
			cmd.setIntParam(3, DateTime.GetNowInt());
			cmd.ExcuteNonQuery();
		} catch (Exception e) {
			Log.exceptionWrite(e);
		} finally {
			cmd.Close();
		}

		sb.append("{\"status\":\"success\",\n");
		sb.append("\"message\":\"ログインしました\"\n}");
		return sb.toString();
	}

	private String logout(HttpServletRequest request) {
		StringBuilder sb = new StringBuilder();
		HttpSession s = request.getSession(false);

		if (s != null)
		{
			s.invalidate();
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"unit-21\",\n");
			sb.append("\"message\":\"ログインしていません\"\n}");
			return sb.toString();
		}

		sb.append("{\"status\":\"success\",\n");
		sb.append("\"message\":\"ログアウトしました\"\n}");
		return sb.toString();
	}

	private String entry(HttpServletRequest request) {

		StringBuilder sb = new StringBuilder();
		String oidStr = request.getParameter("oid");
		int oid = 0;

		try {
			oid = Integer.parseInt(oidStr);
		} catch (Exception e) {
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"unit-31\",\n");
			sb.append("\"message\":\"oid(組織ID)が判別できませんでした\"\n}");
			return sb.toString();
		}

		String unitID = request.getParameter("unitid");
		if (unitID == null) {
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"unit-33\",\n");
			sb.append("\"message\":\"端末IDが取得できませんでした\"\n}");
			return sb.toString();
		}

		String pass = request.getParameter("pass");
		if (pass == null) {
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"unit-34\",\n");
			sb.append("\"message\":\"パスワードが取得できませんでした\"\n}");
			return sb.toString();
		}

		Unit preUnit = Unit.GetUnit(oid, unitID);
		if (preUnit != null) {
			if (!preUnit.getPass().equals(pass)) {
				sb.append("{\"status\":\"error\",\n");
				sb.append("\"errorcode\":\"unit-36\",\n");
				sb.append("\"message\":\"パスワードが違います\"\n}");
				return sb.toString();
			} else {
				// 再登録
				sb.append("{\"status\":\"success\",\n");
				sb.append("\"message\":\"端末を再登録しました\"\n}");
				return sb.toString();
			}
		}

		if (!Unit.IsStandby(oid)) {
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"unit-32\",\n");
			sb.append("\"message\":\"現在端末の登録を受け付けていません\"\n}");
			return sb.toString();
		}

		Unit pu = Unit.GetUnit(oid, "standby");
		if (!pu.getPass().equals(pass)) {
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"unit-35\",\n");
			sb.append("\"message\":\"パスワードが違います\"\n}");
			return sb.toString();
		}

//		pu = Unit.GetUnit(oid, unitID);
//		if (pu != null) {
//			sb.append("{\"status\":\"error\",\n");
//			sb.append("\"errorcode\":\"unit-36\",\n");
//			sb.append("\"message\":\"指定された端末IDはすでに登録されています\"\n}");
//			return sb.toString();
//		}

		String name = request.getParameter("name");
		if (name == null) name = "";

		if (!Unit.Entry(oid, unitID, name, pass)) {
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"unit-37\",\n");
			sb.append("\"message\":\"端末の登録に失敗しました\"\n}");
			return sb.toString();
		}

		sb.append("{\"status\":\"success\",\n");
		sb.append("\"message\":\"端末を登録しました\"\n}");
		return sb.toString();
	}

	private String delete(HttpServletRequest request) {

		StringBuilder sb = new StringBuilder();

		String oidStr = request.getParameter("oid");
		int oid = 0;

		try {
			oid = Integer.parseInt(oidStr);
		} catch (Exception e) {
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"unit-41\",\n");
			sb.append("\"message\":\"oid(組織ID)が判別できませんでした\"\n}");
			return sb.toString();
		}

		String unitID = request.getParameter("unitid");
		if (unitID == null)
		{
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"unit-42\",\n");
			sb.append("\"message\":\"端末番号が判別できませんでした\"\n}");
			return sb.toString();
		}

		Unit.Remove(oid, unitID);

		sb.append("{\"status\":\"success\",\n");
		sb.append("\"message\":\"端末の登録を解除しました\"\n}");
		return sb.toString();

	}
}
