package servelet;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import datas.Organization;

/**
 * Servlet implementation class download
 */
@WebServlet("/update/android")
public class update extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public update() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGetPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGetPost(request, response);
	}

	private void doGetPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		int oid = Organization.GetOidFromSesion(request);
		if (oid == 0)
		{
			StringBuilder sb = new StringBuilder();
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"session-error\",\n");
			sb.append("\"message\":\"不正なアクセスです。\"\n}");
			response.setContentType("application/json; charset=UTF-8");
			response.setHeader("Cache-Control", "no-cache");
			response.getWriter().write(sb.toString());
			return;
		}

		String func = request.getParameter("action");
		String json;

		if (func.equals("check")) {
			json = checkVersion(request);
			response.setContentType("application/json; charset=UTF-8");
			response.setHeader("Cache-Control", "no-cache");
			response.getWriter().write(json);
			return;
		} else if (func.equals("download")) {
			File file = getDLFile();
			response.setContentType("application/octet-stream");
			response.setHeader("Content-disposition", "attachment; filename=\"" + file.getName() + "\"");
			response.setContentLength((int)file.length());
			doDownload(response, file);
			return;
		} else {
			json = "{\"status\":\"error\"}";
			response.setContentType("application/json; charset=UTF-8");
			response.setHeader("Cache-Control", "no-cache");
			response.getWriter().write(json);
			return;
		}
	}

	private String checkVersion(HttpServletRequest request) {

		StringBuilder sb = new StringBuilder();

		String path = this.getServletContext().getRealPath("/") + "WEB-INF/files/update.xml";
		File file = new File(path);
		BufferedInputStream in = null;
		try {
			in = new BufferedInputStream(new FileInputStream(file));
			DocumentBuilderFactory document_builder_factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder document_builder = document_builder_factory.newDocumentBuilder();
			Document document = document_builder.parse(in);
			Element root = document.getDocumentElement();

			HashMap<String, String> map = new HashMap<String, String>();
			if(root.getTagName().equals("update")){
				NodeList nodelist = root.getChildNodes();
				for (int j=0;j<nodelist.getLength();j++){
					Node node = nodelist.item(j);
					if(node.getNodeType()==Node.ELEMENT_NODE){
						Element element = (Element) node;
						String name = element.getTagName();
						String value = element.getTextContent().trim();
						map.put(name, value);
					}
				}
				sb.append("{\n\"status\":\"success\",\n");
				sb.append("\"versionCode\":" + map.get("versionCode") + ", ");
				sb.append("\"versionName\":\"" + map.get("versionName") + "\", ");
				sb.append("\"url\":\"" + map.get("url") + "\"}");
				return sb.toString();
			}

			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"updt-4\",\n");
			sb.append("\"message\":\"XMLファイルを解析できませんでした。\",\n}");

		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			sb.delete(0, sb.length());
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"updt-1\",\n");
			sb.append("\"message\":\"更新情報の取得に失敗しました\",\n}");
		} catch (ParserConfigurationException ex) {
			ex.printStackTrace();
			sb.delete(0, sb.length());
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"updt-2\",\n");
			sb.append("\"message\":\"更新情報の取得に失敗しました\",\n}");
		} catch (IOException ex) {
			ex.printStackTrace();
			sb.delete(0, sb.length());
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"updt-3\",\n");
			sb.append("\"message\":\"更新情報の取得に失敗しました\",\n}");
		} catch (SAXException ex) {
			ex.printStackTrace();
			sb.delete(0, sb.length());
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"updt-4\",\n");
			sb.append("\"message\":\"更新情報の取得に失敗しました\",\n}");
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception ex) {}
			}
		}

		return sb.toString();
	}

	private File getDLFile() {
		String path = this.getServletContext().getRealPath("/") + "WEB-INF/files/jp.co.tecwt.medi_techno_service.apk";
		File file = new File(path);
		return file;
	}

	private void doDownload(HttpServletResponse response, File file) {
		BufferedInputStream in = null;
		BufferedOutputStream out = null;
		try {
			in = new BufferedInputStream(new FileInputStream(file));
			out = new BufferedOutputStream(response.getOutputStream());
			byte buf[]=new byte[1024];
			int len;
			while((len=in.read(buf))!=-1){
				out.write(buf,0,len);
			}
		} catch (Exception e) {
			e.printStackTrace();
			//ファイルダウンロード用のHTTPヘッダをリセットします。
			response.reset();
			try {
				response.sendError(HttpURLConnection.HTTP_INTERNAL_ERROR , e.toString());
			} catch (IOException ex) {
				// 失敗
				ex.printStackTrace();
			}
		} finally {
			try {
				if (in != null) {
					in.close();
				}
				if (out != null) {
					out.flush();
					out.close();
				}
			} catch (IOException ex) {}
		}
	}

}
