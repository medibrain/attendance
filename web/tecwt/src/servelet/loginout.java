package servelet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datas.Organization;
import datas.User;

/**
 * Servlet implementation class login
 */
@WebServlet("/loginout")
public class loginout extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public loginout() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGetPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
			IOException {
		doGetPost(request, response);
	}

	protected void doGetPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		String func = request.getParameter("func");

		if (func.equals("login")) {
			response.getWriter().write(login(request));
		} else if (func.equals("logout")) {
			response.getWriter().write(logout(request));
		} else {
			String json = "{\n\"status\":\"error\",\n";
			json += "\"errorcode\":\"login-1\",\n";
			json += "\"message\":\"すでにログインしています\"\n}";
			response.getWriter().write(json);
		}
	}

	private String login(HttpServletRequest request) {
		String json = "";

		HttpSession session = request.getSession(false);
		if (session != null) {
			json += "{\n\"status\":\"error\",\n";
			json += "\"errorcode\":\"login-1\",\n";
			json += "\"message\":\"すでにログインしています\"\n}";
			return json;
		}

		int oid;
		String oidStr = request.getParameter("oid");
		try {
			oid = Integer.valueOf(oidStr);
		} catch (Exception e) {
			json += "{\n\"status\":\"error\",\n";
			json += "\"errorcode\":\"login-2\",\n";
			json += "\"message\":\"組織が判別できません\"\n}";
			return json;
		}

		String lname = request.getParameter("lname");
		if (lname == null) {
			json += "{\n\"status\":\"error\",\n";
			json += "\"errorcode\":\"login-3\",\n";
			json += "\"message\":\"ユーザーが判別できません\"\n}";
			return json;
		}

		User u = User.GetUser(oid, lname);
		if (u == null) {
			json += "{\n\"status\":\"error\",\n";
			json += "\"errorcode\":\"login-4\",\n";
			json += "\"message\":\"ユーザーIDまたはパスワードが違います\"\n}";
			return json;
		}

		if (!u.isEnable()) {
			json += "{\n\"status\":\"error\",\n";
			json += "\"errorcode\":\"login-5\",\n";
			json += "\"message\":\"ユーザーIDまたはパスワードが違います\"\n}";
			return json;
		}

		String pass = request.getParameter("pass");
		if (pass.isEmpty() || !pass.equals(u.getPass())) {
			json += "{\n\"status\":\"error\",\n";
			json += "\"errorcode\":\"login-6\",\n";
			json += "\"message\":\"ユーザーIDまたはパスワードが違います\"\n}";
			return json;
		}

		Organization org = Organization.getOrganization(oid);
		if (org == null) {
			json += "{\n\"status\":\"error\",\n";
			json += "\"errorcode\":\"login-7\",\n";
			json += "\"message\":\"ログインに失敗しました\"\n}";
			return json;
		}

		session = request.getSession(true);
		session.setAttribute("oid", oid);
		session.setAttribute("uid", u.getUid());
		session.setAttribute("uname", u.getName());
		session.setAttribute("admin", u.isAdmin());

		json += "{\n\"status\":\"success\",\n";
		json += "\"orgname\":\"" + u.getName() + "\",\n";
		json += "\"name\":\"" + u.getName() + "\",\n";
		json += "\"message\":\"ログインしました\"\n}";
		return json;
	}

	private String logout(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		String json;

		if (session == null) {
			json = "{\n\"status\":\"error\",\n";
			json += "\"errorcode\":\"login-8\",\n";
			json += "\"message\":\"ログインしていません\"\n}";
			return json;
		}

		session.invalidate();
		json = "{\n\"status\":\"success\",\n";
		json += "\"message\":\"ログアウトしました\"\n}";
		return json;
	}

}
