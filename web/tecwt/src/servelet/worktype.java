package servelet;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datas.Common;
import datas.Organization;
import datas.WorkType;

/**
 * Servlet implementation class WorkType
 */
@WebServlet("/worktype")
public class worktype extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public worktype() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGetPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGetPost(request, response);
	}

	protected void doGetPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String func = request.getParameter("action");
		String json;

		if (func.equals("getlist")) {
			json = getList(request);
		} else if (func.equals("insert")) {
			json = "";
		} else if (func.equals("update")) {
			json = "";
		} else if (func.equals("delete")) {
			json = "";
		} else {
			json = "{\"status\":\"error\"}";
		}

		// jsonデータを返す
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write(json);
	}

	private String getList(HttpServletRequest request) {

		StringBuilder sb = new StringBuilder();

		int oid = Organization.GetOidFromSesion(request);
		if (oid == 0)
		{
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"session-error\",\n");
			sb.append("\"message\":\"oid(組織ID)が判別できませんでした\"\n}");
			return sb.toString();
		}

		sb.append("{\"status\":\"success\",\n");
		sb.append("\"data\":\n[\n");

		try {
			List<WorkType> list = WorkType.GetWorkTypeList(oid);
			{
				for (int i = 0; i < list.size(); i++) {
					if (i != 0)
						sb.append(",\n");
					WorkType wt = list.get(i);
					sb.append("{\"wid\":" + wt.getWid() + ", ");
					sb.append("\"name\":\"" + Common.JsonEscape(wt.getName()) + "\", ");
					sb.append("\"kana\":\"" + Common.JsonEscape(wt.getKana()) + "\"}");
				}
			}
			sb.append("\n]\n}");
			return sb.toString();
		} catch (Exception e) {
			sb.delete(0, sb.length());
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"message\":\"業務種別の取得に失敗しました\",\n}");
			return sb.toString();
		}
	}
}
