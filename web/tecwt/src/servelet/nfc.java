package servelet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import datas.NFC;
import datas.NFCWait;
import datas.Organization;
import datas.User;

/**
 * Servlet implementation class nfc
 */
@WebServlet("/nfc")
public class nfc extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public nfc() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGetPost(request, response);

		HttpSession ss = request.getSession();
		ss.getId();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGetPost(request, response);
	}

	protected void doGetPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String func = request.getParameter("action");
		String json;

		if (func.equals("entry")) {
			json = entry(request);
		} else if (func.equals("getwaitlist")) {
			json = getWaitList(request);
		} else if (func.equals("delete")) {
			json = "";
		} else if (func.equals("waitentry")) {
			json = waitEntry(request);
		} else if (func.equals("getuser")) {
			json = getUser(request);
		} else {
			json = "{\n\"status\":\"error\"\n";
			json += ("\"errorcode\":\"nfc-1\",\n");
			json += ("\"message\":\"処理が判別できません\",\n}");
		}

		// jsonデータを返す
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write(json);
	}

	private String getWaitList(HttpServletRequest request) {

		StringBuilder sb = new StringBuilder();

		int oid = Organization.GetOidFromSesion(request);
		if (oid == 0)
		{
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"session-error\",\n");
			sb.append("\"message\":\"oid(組織ID)が判別できませんでした\"\n}");
			return sb.toString();
		}

		sb.append("{\n\"status\":\"success\",\n");
		sb.append("\"data\":\n[\n");

		try {
			List<NFCWait> list = NFCWait.GetNFCWaitList(oid);
			{
				for (int i = 0; i < list.size(); i++) {
					if (i != 0)
						sb.append(",\n");
					NFCWait nw = list.get(i);
					sb.append("{\"uid\":" + nw.getUid() + ", ");
					sb.append("\"name\":\"" + nw.getName() + "\"}");
				}
			}
			sb.append("\n]\n}");
			return sb.toString();
		} catch (Exception e) {
			sb.delete(0, sb.length());
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"nfc-3\",\n");
			sb.append("\"message\":\"NFCタグの登録待ちリスト取得に失敗しました\"\n}");
			return sb.toString();
		}
	}

	private String entry(HttpServletRequest request) {
		StringBuilder sb = new StringBuilder();

		try {
			//OIDの取得
			int oid = Organization.GetOidFromSesion(request);
			if (oid == 0)
			{
				sb.append("{\n\"status\":\"error\",\n");
				sb.append("\"errorcode\":\"session-error\",\n");
				sb.append("\"message\":\"oid(組織ID)が判別できませんでした\"\n}");
				return sb.toString();
			}

			//UID取得
			String uidStr = request.getParameter("uid");
			int uid = 0;
			try {
				uid = Integer.parseInt(uidStr);
			} catch (Exception e) {
				sb.append("{\"status\":\"error\",\n");
				sb.append("\"errorcode\":\"nfc-5\",\n");
				sb.append("\"message\":\"uid(ユーザーID)が判別できませんでした\"\n}");
				return sb.toString();
			}

			//タグIDm取得
			String idm = request.getParameter("idm");
			if (idm == null || idm.isEmpty() || idm.length() < 8) {
				sb.append("{\"status\":\"error\",\n");
				sb.append("\"errorcode\":\"nfc-6\",\n");
				sb.append("\"message\":\"NFCタグIDmが取得できませんでした\"\n}");
				return sb.toString();
			}

			//待機状態確認
			if (!NFCWait.IsWait(oid, uid)) {
				sb.append("{\"status\":\"error\",\n");
				sb.append("\"errorcode\":\"nfc-7\",\n");
				sb.append("\"message\":\"指定されたユーザーはNFC登録待機状態ではありません\"\n}");
				return sb.toString();
			}

			//登録状況確認
			if (NFC.GetNFC(oid, idm) != null)
			{
				sb.append("{\"status\":\"error\",\n");
				sb.append("\"errorcode\":\"nfc-8\",\n");
				sb.append("\"message\":\"指定されたNFCタグIDmは既に登録されています\"\n}");
				return sb.toString();
			}

			//登録
			if (!NFC.Entry(idm, oid, uid, "", 0))
			{
				sb.append("{\"status\":\"error\",\n");
				sb.append("\"errorcode\":\"nfc-9\",\n");
				sb.append("\"message\":\"登録に失敗しました\"\n}");
				return sb.toString();
			}

			NFC nnfc = NFC.GetNFC(oid, idm);

			sb.append("{\"status\":\"success\",\n");
			sb.append("\"name\":\"" + nnfc.getIdm() + "\",\n");
			sb.append("\"idm\":\"" + nnfc.getName() + "\",\n");
			sb.append("\"message\":\"登録が完了しました\"\n}");
			return sb.toString();

		} catch (Exception e) {
			sb.delete(0, sb.length());
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"nfc-10\",\n");
			sb.append("\"message\":\"登録に失敗しました\"\n}");
			return sb.toString();
		}
	}

	/**
	 * NFC登録待機<br>
	 * @param request
	 * @return
	 */
	private String waitEntry(HttpServletRequest request) {

		StringBuilder sb = new StringBuilder();

		int oid = Organization.GetOidFromSesion(request);
		if (oid == 0)
		{
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"session-error\",\n");
			sb.append("\"message\":\"oid(組織ID)が判別できませんでした\"\n}");
			return sb.toString();
		}

		String uidStr = request.getParameter("uid");
		int uid = 0;

		try {
			uid = Integer.parseInt(uidStr);
		} catch (Exception e) {
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"nfc-12\",\n");
			sb.append("\"message\":\"uid(ユーザーID)が判別できませんでした\"\n}");
			return sb.toString();
		}

		//登録
		if (!NFCWait.Entry(oid, uid, 0))
		{
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"nfc-3\",\n");
			sb.append("\"message\":\"NFCタグの登録待ち失敗しました\"\n}");
			return sb.toString();
		}

		sb.append("{\"status\":\"success\",\n");
		sb.append("\"message\":\"NFC登録待機になりました\"\n}");
		return sb.toString();
	}

	private String getUser(HttpServletRequest request) {

		StringBuilder sb = new StringBuilder();

		//OIDの取得
		int oid = Organization.GetOidFromSesion(request);
		if (oid == 0)
		{
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"session-error\",\n");
			sb.append("\"message\":\"oid(組織ID)が判別できませんでした\"\n}");
			return sb.toString();
		}

		//タグIDm取得
		String idm = request.getParameter("idm");
		if (idm == null || idm.isEmpty() || idm.length() < 8) {
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"nfc-15\",\n");
			sb.append("\"message\":\"NFCタグIDmが取得できませんでした\"\n}");
			return sb.toString();
		}

		//UIDの取得
		NFC nfc = NFC.GetNFC(oid, idm);
		if (nfc == null)
		{
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"nfc-16\",\n");
			sb.append("\"message\":\"指定されたNFCタグIDmは登録されていません\"\n}");
			return sb.toString();
		}
		int uid = nfc.getUid();

		//Userの取得
		User user = User.GetUser(oid, uid);
		if (user == null)
		{
			sb.append("{\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"nfc-17\",\n");
			sb.append("\"message\":\"ユーザーが判別できませんでした\"\n}");
			return sb.toString();
		}

		sb.append("{\n\"status\":\"success\",\n");
		sb.append("\"message\":\"NFCタグIDmを識別しました。\",\n");
		sb.append("\"user\":").append(user.GetJson()).append("\n}");
		return sb.toString();
	}
}
