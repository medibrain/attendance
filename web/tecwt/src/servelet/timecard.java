package servelet;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import datas.Common;
import datas.Log;
import datas.NFC;
import datas.Organization;
import datas.TimeCard;
import datas.User;
import datas.WorkType;

/**
 * Servlet implementation class timecard
 */
@WebServlet("/timecard")
public class timecard extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public timecard() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGetPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGetPost(request, response);
	}

	protected void doGetPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		String func = request.getParameter("action");
		String json;

		if (func.equals("in")) {
			json = in(request);
		} else if (func.equals("move")) {
			json = move(request);
		} else if (func.equals("out")) {
			json = out(request);
		} else if (func.equals("logtest")) {
			json = logtest(request);
		}
		else {
			json = "{\n\"status\":\"error\"\n";
			json += ("\"errorcode\":\"timecard-post1\",\n");
			json += ("\"message\":\"処理が判別できません\",\n}");
		}

		// jsonデータを返す
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write(json);
	}

	private String in(HttpServletRequest request) {
		StringBuilder sb = new StringBuilder();

		int oid = Organization.GetOidFromSesion(request);
		if (oid == 0)
		{
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"session-error\",\n");
			sb.append("\"message\":\"oid(組織ID)が判別できませんでした\"\n}");
			return sb.toString();
		}

		String idm = request.getParameter("idm");
		if (idm == null || idm.isEmpty()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-in01\",\n");
			sb.append("\"message\":\"IDm(NFCタグID)が判別できませんでした\"\n}");
			return sb.toString();
		}

		NFC nfc = NFC.GetNFC(oid, idm);
		if (nfc == null) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-in02\",\n");
			sb.append("\"message\":\"読込まれたIDmは登録されていません\"\n}");
			return sb.toString();
		}

		User user = User.GetUser(oid, nfc.getUid());
		if (user == null || !user.isEnable()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-in03\",\n");
			sb.append("\"message\":\"指定されたユーザーは無効です\"\n}");
			return sb.toString();
		}

		String latitude = request.getParameter("latitude");
		if (latitude == null || latitude.isEmpty()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-in07\",\n");
			sb.append("\"message\":\"緯度が指定されていません\"\n}");
			return sb.toString();
		}

		String longitude = request.getParameter("longitude");
		if (longitude == null || longitude.isEmpty()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-in08\",\n");
			sb.append("\"message\":\"経度が指定されていません\"\n}");
			return sb.toString();
		}

		String unitid = request.getParameter("unitid");
		if (unitid == null || unitid.isEmpty()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-in09\",\n");
			sb.append("\"message\":\"端末IDが判別できませんでした\"\n}");
			return sb.toString();
		}

		String inTimeStr = request.getParameter("intime");
		int inDate = 0;
		int inTime = 0;
		try {
			long iLong = Long.parseLong(inTimeStr);
			inDate = (int) (iLong / 10000);
			inTime = (int) (iLong % 10000);
		} catch (Exception e) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-in10\",\n");
			sb.append("\"message\":\"出勤時間が判別できませんでした\"\n}");
			return sb.toString();
		}

		int yyyy = inDate / 10000;
		int MM = (inDate - (yyyy * 10000)) / 100;
		int dd = inDate - (yyyy * 10000) - (MM * 100);
		Calendar now = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"), Locale.JAPAN);
		boolean check = true;
		check &= yyyy == now.get(Calendar.YEAR);
		check &= MM == now.get(Calendar.MONTH) + 1;
		check &= dd == now.get(Calendar.DAY_OF_MONTH);
		if(!check) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-in11\",\n");
			sb.append("\"message\":\"タイムカードの日付が本日ではありません\"\n}");
			return sb.toString();
		}

		if (((inTime % 100) % 15) != 0) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-in12\",\n");
			sb.append("\"message\":\"指定された出勤時間は15分単位ではありません\"\n}");
			return sb.toString();
		}

		if ((inTime / 100) > 24 || (inTime / 100) < 0) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-in13\",\n");
			sb.append("\"message\":\"指定された出勤時間が不明です\"\n}");
			return sb.toString();
		}

		TimeCard.TimeExcessResult ter = TimeCard.IsInTimeExcess(oid, user.getUid(), inDate, inTime);
		if (ter.excess) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-in16\",\n");
			sb.append(String.format(
					"\"message\":\"出勤時間が%s時間(%d)を超えることはできません。時間を確認して下さい。\"\n}",
					TimeCard.GetWorkStr(ter.compareWork),
					ter.compareTime));
			return sb.toString();
		}

		String eTimeStr = request.getParameter("entrytime");
		int eDate = 0;
		int eTime = 0;
		try {
			long eLong = Long.parseLong(eTimeStr);
			eDate = (int) (eLong / 10000);
			eTime = (int) (eLong % 10000);
		} catch (Exception e) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-in14\",\n");
			sb.append("\"message\":\"登録時間が判別できませんでした\"\n}");
			return sb.toString();
		}

		try {
			boolean res = TimeCard.NfcIn(oid, user.getUid(), inDate,
					latitude, longitude, inTime, eDate, eTime, unitid);

			if (!res) {
				sb.append("{\n\"status\":\"error\",\n");
				sb.append("\"errorcode\":\"timecard-in15\",\n");
				sb.append("\"message\":\"出勤記録でエラーが発生しました\"\n}");
				return sb.toString();
			}

			return getReturnSuccessJSON(oid, user.getUid(), inDate, sb);

		} catch (Exception e) {
			Log.exceptionWrite(e);
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-inex\",\n");
			sb.append("\"message\":\"出勤記録でエラーが発生しました\"\n}");
			return sb.toString();
		}
	}

	private String move(HttpServletRequest request) {
		StringBuilder sb = new StringBuilder();

		int oid = Organization.GetOidFromSesion(request);
		if (oid == 0)
		{
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"session-error\",\n");
			sb.append("\"message\":\"oid(組織ID)が判別できませんでした\"\n}");
			return sb.toString();
		}

		String idm = request.getParameter("idm");
		if (idm == null || idm.isEmpty()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move01\",\n");
			sb.append("\"message\":\"IDm(NFCタグID)が判別できませんでした\"\n}");
			return sb.toString();
		}

		NFC nfc = NFC.GetNFC(oid, idm);
		if (nfc == null) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move02\",\n");
			sb.append("\"message\":\"読込まれたIDmは登録されていません\"\n}");
			return sb.toString();
		}

		User user = User.GetUser(oid, nfc.getUid());
		if (user == null || !user.isEnable()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move03\",\n");
			sb.append("\"message\":\"指定されたユーザーは無効です\"\n}");
			return sb.toString();
		}

		String widStr = request.getParameter("wid");
		int wid = 0;
		try {
			wid = Integer.parseInt(widStr);
		} catch (Exception e) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move04\",\n");
			sb.append("\"message\":\"wid(業務種別ID)が判別できませんでした\"\n}");
			return sb.toString();
		}

		WorkType wt = WorkType.Select(oid, wid);
		if (wt == null) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move05\",\n");
			sb.append("\"message\":\"業務種別が判別できませんでした\"\n}");
			return sb.toString();
		}

		if (!wt.isEnable()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move06\",\n");
			sb.append("\"message\":\"指定された業務種別は無効です\"\n}");
			return sb.toString();
		}

		String latitude = request.getParameter("latitude");
		if (latitude == null || latitude.isEmpty()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move07\",\n");
			sb.append("\"message\":\"緯度が指定されていません\"\n}");
			return sb.toString();
		}

		String longitude = request.getParameter("longitude");
		if (longitude == null || longitude.isEmpty()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move08\",\n");
			sb.append("\"message\":\"経度が指定されていません\"\n}");
			return sb.toString();
		}

		String unitid = request.getParameter("unitid");
		if (unitid == null || unitid.isEmpty()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move09\",\n");
			sb.append("\"message\":\"端末IDが判別できませんでした\"\n}");
			return sb.toString();
		}

		String mTimeStr = request.getParameter("movetime");
		int mDate = 0;
		int mTime = 0;
		try {
			long mLong = Long.parseLong(mTimeStr);
			mDate = (int) (mLong / 10000);
			mTime = (int) (mLong % 10000);
		} catch (Exception e) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move10\",\n");
			sb.append("\"message\":\"移動時刻が判別できませんでした\"\n}");
			return sb.toString();
		}

		int yyyy = mDate / 10000;
		int MM = (mDate - (yyyy * 10000)) / 100;
		int dd = mDate - (yyyy * 10000) - (MM * 100);
		Calendar now = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"), Locale.JAPAN);
		boolean check = true;
		check &= yyyy == now.get(Calendar.YEAR);
		check &= MM == now.get(Calendar.MONTH) + 1;
		check &= dd == now.get(Calendar.DAY_OF_MONTH);
		if(!check) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move11\",\n");
			sb.append("\"message\":\"タイムカードの日付が本日ではありません\"\n}");
			return sb.toString();
		}

		if (((mTime % 100) % 15) != 0) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move12\",\n");
			sb.append("\"message\":\"指定された移動時刻は15分単位ではありません\"\n}");
			return sb.toString();
		}

		if ((mTime / 100) > 24 || (mTime / 100) < 0) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move13\",\n");
			sb.append("\"message\":\"指定された移動時刻が不明です\"\n}");
			return sb.toString();
		}

		TimeCard.TimeExcessResult ter = TimeCard.IsMoveTimeExcess(oid, user.getUid(), mDate, mTime);
		if (ter.excess) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move18\",\n");
			sb.append(String.format(
					"\"message\":\"移動時間が%s時間(%d)を超えることはできません。時間を確認して下さい。\"\n}",
					TimeCard.GetWorkStr(ter.compareWork),
					ter.compareTime));
			return sb.toString();
		}

		String eTimeStr = request.getParameter("entrytime");
		int eDate = 0;
		int eTime = 0;
		try {
			long eLong = Long.parseLong(eTimeStr);
			eDate = (int) (eLong / 10000);
			eTime = (int) (eLong % 10000);
		} catch (Exception e) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move14\",\n");
			sb.append("\"message\":\"登録時間が判別できませんでした\"\n}");
			return sb.toString();
		}

		String wCountStr = request.getParameter("workcount");
		int wcount = 0;
		try {
			if (!wCountStr.isEmpty())
			{
				wcount = Integer.parseInt(wCountStr);
			}
		} catch (Exception e) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move15\",\n");
			sb.append("\"message\":\"処理枚数が判別できませんでした\"\n}");
			return sb.toString();
		}

		String rCountStr = request.getParameter("reportcount");
		int rcount = 0;
		try {
			if (!rCountStr.isEmpty())
			{
				rcount = Integer.parseInt(rCountStr);
			}
		} catch (Exception e) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-move16\",\n");
			sb.append("\"message\":\"申出枚数が判別できませんでした\"\n}");
			return sb.toString();
		}

		String content = Common.URLDec(request.getParameter("content"));
		// 改行コード削除
		if(content != null) {
			content = content.replaceAll("\r\n|[\n\r\u2028\u2029\u0085]", "");
		}

		try {
			boolean res = TimeCard.NfcMove(oid, user.getUid(), mDate, wt,
					latitude, longitude, mTime, eDate, eTime, content, wcount, rcount, unitid);
			if (!res) {
				sb.append("{\n\"status\":\"error\",\n");
				sb.append("\"errorcode\":\"timecard-move17\",\n");
				sb.append("\"message\":\"移動記録でエラーが発生しました\"\n}");
				return sb.toString();
			}

			return getReturnSuccessJSON(oid, user.getUid(), mDate, sb);

		} catch (Exception e) {
			Log.exceptionWrite(e);
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-moveex\",\n");
			sb.append("\"message\":\"移動記録でエラーが発生しました\"\n}");
			return sb.toString();
		}
	}

	private String out(HttpServletRequest request) {
		StringBuilder sb = new StringBuilder();

		int oid = Organization.GetOidFromSesion(request);
		if (oid == 0)
		{
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"session-error\",\n");
			sb.append("\"message\":\"oid(組織ID)が判別できませんでした\"\n}");
			return sb.toString();
		}

		String idm = request.getParameter("idm");
		if (idm == null || idm.isEmpty()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out01\",\n");
			sb.append("\"message\":\"IDm(NFCタグID)が判別できませんでした\"\n}");
			return sb.toString();
		}

		NFC nfc = NFC.GetNFC(oid, idm);
		if (nfc == null) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out02\",\n");
			sb.append("\"message\":\"読込まれたIDmは登録されていません\"\n}");
			return sb.toString();
		}

		User user = User.GetUser(oid, nfc.getUid());
		if (user == null || !user.isEnable()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out03\",\n");
			sb.append("\"message\":\"指定されたユーザーは無効です\"\n}");
			return sb.toString();
		}

		String widStr = request.getParameter("wid");
		int wid = 0;
		try {
			wid = Integer.parseInt(widStr);
		} catch (Exception e) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out04\",\n");
			sb.append("\"message\":\"wid(業務種別ID)が判別できませんでした\"\n}");
			return sb.toString();
		}

		WorkType wt = WorkType.Select(oid, wid);
		if (wt == null) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out05\",\n");
			sb.append("\"message\":\"業務種別が判別できませんでした\"\n}");
			return sb.toString();
		}

		if (!wt.isEnable()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out06\",\n");
			sb.append("\"message\":\"指定された業務種別は無効です\"\n}");
			return sb.toString();
		}

		String latitude = request.getParameter("latitude");
		if (latitude == null || latitude.isEmpty()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out07\",\n");
			sb.append("\"message\":\"緯度が指定されていません\"\n}");
			return sb.toString();
		}

		String longitude = request.getParameter("longitude");
		if (longitude == null || longitude.isEmpty()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out08\",\n");
			sb.append("\"message\":\"経度が指定されていません\"\n}");
			return sb.toString();
		}

		String unitid = request.getParameter("unitid");
		if (unitid == null || unitid.isEmpty()) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out09\",\n");
			sb.append("\"message\":\"端末IDが判別できませんでした\"\n}");
			return sb.toString();
		}

		String oTimeStr = request.getParameter("outtime");
		int oDate = 0;
		int oTime = 0;
		try {
			long oLong = Long.parseLong(oTimeStr);
			oDate = (int) (oLong / 10000);
			oTime = (int) (oLong % 10000);
		} catch (Exception e) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out10\",\n");
			sb.append("\"message\":\"退勤時間が判別できませんでした\"\n}");
			return sb.toString();
		}

		int yyyy = oDate / 10000;
		int MM = (oDate - (yyyy * 10000)) / 100;
		int dd = oDate - (yyyy * 10000) - (MM * 100);
		Calendar now = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"), Locale.JAPAN);
		boolean check = true;
		check &= yyyy == now.get(Calendar.YEAR);
		check &= MM == now.get(Calendar.MONTH) + 1;
		check &= dd == now.get(Calendar.DAY_OF_MONTH);
		if(!check) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out11\",\n");
			sb.append("\"message\":\"タイムカードの日付が本日ではありません\"\n}");
			return sb.toString();
		}

		if (((oTime % 100) % 15) != 0) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out12\",\n");
			sb.append("\"message\":\"指定された退勤時間は15分単位ではありません\"\n}");
			return sb.toString();
		}

		if ((oTime / 100) > 24 || (oTime / 100) < 0) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out13\",\n");
			sb.append("\"message\":\"指定された退勤時間が不明です\"\n}");
			return sb.toString();
		}

		TimeCard.TimeExcessResult ter = TimeCard.IsOutTimeExcess(oid, user.getUid(), oDate, oTime);
		if (ter.excess) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out21\",\n");
			sb.append(String.format(
					"\"message\":\"退勤時間が%s時間(%d)を下回ることはできません。時間を確認して下さい。\"\n}",
					TimeCard.GetWorkStr(ter.compareWork),
					ter.compareTime));
			return sb.toString();
		}

		String eTimeStr = request.getParameter("entrytime");
		int eDate = 0;
		int eTime = 0;
		try {
			long eLong = Long.parseLong(eTimeStr);
			eDate = (int) (eLong / 10000);
			eTime = (int) (eLong % 10000);
		} catch (Exception e) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out14\",\n");
			sb.append("\"message\":\"登録時間が判別できませんでした\"\n}");
			return sb.toString();
		}

		String wCountStr = request.getParameter("workcount");
		int wcount = 0;
		try {
			if (!wCountStr.isEmpty())
			{
				wcount = Integer.parseInt(wCountStr);
			}
		} catch (Exception e) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out15\",\n");
			sb.append("\"message\":\"登録時間が判別できませんでした\"\n}");
			return sb.toString();
		}

		String rCountStr = request.getParameter("reportcount");
		int rcount = 0;
		try {
			if (!rCountStr.isEmpty())
			{
				rcount = Integer.parseInt(rCountStr);
			}
		} catch (Exception e) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out16\",\n");
			sb.append("\"message\":\"登録時間が判別できませんでした\"\n}");
			return sb.toString();
		}

		String fareStr = request.getParameter("fare");
		int fare = 0;
		try {
			if (!fareStr.isEmpty())
			{
				fare = Integer.parseInt(fareStr);
			}
		} catch (Exception e) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out17\",\n");
			sb.append("\"message\":\"交通費が判別できませんでした\"\n}");
			return sb.toString();
		}

		String content = request.getParameter("content");
		// 改行コード削除
		if(content != null) {
			content = content.replaceAll("\r\n|[\n\r\u2028\u2029\u0085]", "");
		}

		String breakStr = request.getParameter("breaktime");
		int breaktime = 0;
		try {
			if (!breakStr.isEmpty())
			{
				breaktime = Integer.parseInt(breakStr);
			}
		} catch (Exception e) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out18\",\n");
			sb.append("\"message\":\"休憩時間が判別できませんでした\"\n}");
			return sb.toString();
		}

		if (breaktime % 15 != 0) {
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-out19\",\n");
			sb.append("\"message\":\"指定された休憩時間は15分単位ではありません\"\n}");
			return sb.toString();
		}

		try {
			boolean res = TimeCard.NfcOut(oid, user.getUid(), oDate, wt, latitude, longitude,
					oTime, eDate, eTime, content, wcount, rcount, breaktime, fare, unitid);
			if (!res) {
				sb.append("{\n\"status\":\"error\",\n");
				sb.append("\"errorcode\":\"timecard-out20\",\n");
				sb.append("\"message\":\"退勤記録でエラーが発生しました\"\n}");
				return sb.toString();
			}

			return getReturnSuccessJSON(oid, user.getUid(), oDate, sb);

		} catch (Exception e) {
			Log.exceptionWrite(e);
			sb.append("{\n\"status\":\"error\",\n");
			sb.append("\"errorcode\":\"timecard-outex\",\n");
			sb.append("\"message\":\"退勤記録でエラーが発生しました\"\n}");
			return sb.toString();
		}
	}

	private String getReturnSuccessJSON(int oid, int uid, int tdate, StringBuilder sb) {

		sb.append("{\n\"status\":\"success\",\n");
		sb.append("\"message\":\"受付が完了しました\",\n");
		sb.append("\"timecard\":[\n");

		//本日のタイムカード
		List<TimeCard> tcl = TimeCard.getTimeCards(oid, uid, tdate);
		for (int i = 0; i < tcl.size(); i++) {
			sb.append(tcl.get(i).GetJson());
			if (i != tcl.size() - 1) sb.append(",");
			sb.append("\n");
		}
		sb.append("]\n");

		//前回のタイムカード
		List<TimeCard> ltcl = TimeCard.GetLastWorkTimeCard(oid, uid);
		if(ltcl.size() > 0) {
			sb.append(",\n");
			sb.append("\"lasttimecard\":[\n");
			for (int i = 0; i < ltcl.size(); i++) {
				sb.append(ltcl.get(i).GetJson());
				if (i != ltcl.size() - 1) sb.append(",");
				sb.append("\n");
			}
			sb.append("]\n");
		}

		sb.append("}\n");
		return sb.toString();
	}

	private String logtest(HttpServletRequest request) {

		try {
			throw new Exception("テストログ エラーログ確認用");
		} catch (Exception e){
			Log.exceptionWrite(e);
		}

		Log.Write("テストログ 単純ログ確認");
		StringBuilder sb = new StringBuilder();

		sb.append("{\n\"status\":\"success\",\n");
		sb.append("\"message\":\"テストログを出力しました\",\n");
		sb.append("\"timecard\":[\n");
		sb.append("]\n}\n");

		return sb.toString();
	}
}
