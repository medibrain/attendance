package datas;

public class TimeCardLog {

	int logid;
	int oid;
	int uid;
	String logtype;
	String note;
	int cardDate;
	int logdate;
	int logtime;
	String latitude;
	String longitude;
	String unitid;

	public String getLogtype() {
		return logtype;
	}

	public void setLogtype(String logtype) {
		this.logtype = logtype;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public int getCardDate() {
		return cardDate;
	}

	public void setCardDate(int cardDate) {
		this.cardDate = cardDate;
	}

	public int getLogdate() {
		return logdate;
	}

	public void setLogdate(int logdate) {
		this.logdate = logdate;
	}

	public int getLogtime() {
		return logtime;
	}

	public void setLogtime(int logtime) {
		this.logtime = logtime;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setUnitID(String unitid) {
		this.unitid = unitid;
	}

	public String getUnitID() {
		return unitid;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public int getLogid() {
		return logid;
	}

	public int getOid() {
		return oid;
	}

	public int getUid() {
		return uid;
	}

	public TimeCardLog(int oID, int uID) {
		this.oid = oID;
		this.uid = uID;
	}

	public boolean Write(Transaction tran) {
		DBCmd cmd = new DBCmd("INSERT INTO tclog "
				+ "(oid, uid, logtype, func, carddate, logdate, "
				+ "logtime, latitude, longitude, unitid) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", tran);
		try {
			cmd.setIntParam(1, this.oid);
			cmd.setIntParam(2, this.uid);
			cmd.setStrParam(3, this.logtype);
			cmd.setStrParam(4, this.note);
			cmd.setIntParam(5, this.cardDate);
			cmd.setIntParam(6, this.logdate);
			cmd.setIntParam(7, this.logtime);
			cmd.setStrParam(8, this.latitude);
			cmd.setStrParam(9, this.longitude);
			cmd.setStrParam(10, this.unitid);
			return cmd.ExcuteNonQuery();
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return false;
		}
	}

}
