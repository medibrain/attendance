package datas;

import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TimeCard {

	public static final int WORK_IN = 1;
	public static final int WORK_MOVE = 2;
	public static final int WORK_OUT = 3;

	int oid;
	int uid;
	String name;
	int tdate;
	int times;
	int intime;
	int outtime;
	int breaktime;
	int wid;
	String content;
	int fare;
	int wcount;
	int rcount;

	public int getOid() {
		return oid;
	}

	public int getUid() {
		return uid;
	}

	public int getTdate() {
		return tdate;
	}

	public int getTimes() {
		return times;
	}

	public int getIntime() {
		return intime;
	}

	public int getOuttime() {
		return outtime;
	}

	public int getBreaktime() {
		return breaktime;
	}

	public int getWid() {
		return wid;
	}

	public String getNote() {
		return content;
	}

	public int getFare() {
		return fare;
	}

	public int getWcount() {
		return wcount;
	}

	public int getRcount() {
		return rcount;
	}

	public String getUserName() {
		return name;
	}

	public TimeCard(int oid, int uid, int tdate, int times) {
		this.oid = oid;
		this.uid = uid;
		this.tdate = tdate;
		this.times = times;
		this.content = "";
		this.intime = -1;
		this.outtime = -1;
	}

	private boolean insert(Transaction tran) {
		DBCmd cmd = new DBCmd("INSERT INTO timecard"
				+ "(oid, uid, tdate, times, intime, outtime, breaktime, "
				+ "wid, note, fare, wcount, rcount) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", tran);
		try
		{
			cmd.setIntParam(1, oid);
			cmd.setIntParam(2, uid);
			cmd.setIntParam(3, tdate);
			cmd.setIntParam(4, times);
			cmd.setIntParam(5, intime);
			cmd.setIntParam(6, outtime);
			cmd.setIntParam(7, breaktime);
			cmd.setIntParam(8, wid);
			cmd.setStrParam(9, content);
			cmd.setIntParam(10, fare);
			cmd.setIntParam(11, wcount);
			cmd.setIntParam(12, rcount);
			return cmd.ExcuteNonQuery();
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return false;
		}
	}

	private boolean update(Transaction tran) {
		DBCmd cmd = new DBCmd("UPDATE timecard SET intime=?, outtime=?, "
				+ "breaktime=?, wid=?, note=?, fare=?, wcount=?, rcount=? "
				+ "WHERE  oid=? AND uid=? AND tdate=? AND times=?;", tran);
		try {
			cmd.setIntParam(1, intime);
			cmd.setIntParam(2, outtime);
			cmd.setIntParam(3, breaktime);
			cmd.setIntParam(4, wid);
			cmd.setStrParam(5, content);
			cmd.setIntParam(6, fare);
			cmd.setIntParam(7, wcount);
			cmd.setIntParam(8, rcount);
			cmd.setIntParam(9, oid);
			cmd.setIntParam(10, uid);
			cmd.setIntParam(11, tdate);
			cmd.setIntParam(12, times);
			return cmd.ExcuteNonQuery();
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return false;
		}
	}

	public String GetJson() {
		StringBuilder sb = new StringBuilder();

		WorkType wt = WorkType.Select(oid, wid);
		String wts = wt == null ? "" : wt.name;

		sb.append("{\"oid\":\"" + String.valueOf(oid) + "\",");
		sb.append("\"uid\":\"" + String.valueOf(uid) + "\",");
		sb.append("\"name\":\"" + Common.JsonEscape(name) + "\",");
		sb.append("\"tdate\":\"" + DateTime.IntDayToString(tdate) + "\",");
		sb.append("\"times\":\"" + String.valueOf(times) + "\",");
		sb.append("\"intime\":\"" + DateTime.IntTimeToString(intime) + "\",");
		sb.append("\"outtime\":\"" + DateTime.IntTimeToString(outtime) + "\",");
		sb.append("\"breaktime\":\"" + String.valueOf(breaktime) + "\",");
		sb.append("\"worktype\":\"" + Common.JsonEscape(wts) + "\",");
		sb.append("\"content\":\"" + Common.JsonEscape(content) + "\",");
		sb.append("\"fare\":\"" + String.valueOf(fare) + "\",");
		sb.append("\"wcount\":\"" + String.valueOf(wcount) + "\",");
		sb.append("\"rcount\":\"" + String.valueOf(rcount) + "\"}");

		return sb.toString();
	}

	public static TimeCard getTimeCard(int oid, int uid, int tdate, int times) {

		DBCmd cmd = new DBCmd("SELECT t.oid, t.uid, u.name, t.tdate, "
				+ "t.times, t.intime, t.outtime, t.breaktime, t.wid, "
				+ "t.note, t.fare, t.wcount, t.rcount "
				+ "FROM timecard AS t, Users AS u "
				+ "WHERE t.oid=u.oid AND t.uid=u.uid "
				+ "AND t.oid=? AND t.uid=? AND tdate=? AND times=?;");
		try {
			cmd.setIntParam(1, oid);
			cmd.setIntParam(2, uid);
			cmd.setIntParam(3, tdate);
			cmd.setIntParam(4, times);
			ResultSet res = cmd.ExcuteResult();

			if (!res.next()) return null;

			TimeCard tc = new TimeCard(oid, uid, tdate, times);
			tc.name = res.getString("name");
			tc.intime = res.getInt("intime");
			tc.outtime = res.getInt("outtime");
			tc.breaktime = res.getInt("breaktime");
			tc.wid = res.getInt("wid");
			tc.content = res.getString("note");
			tc.fare = res.getInt("fare");
			tc.wcount = res.getInt("wcount");
			tc.rcount = res.getInt("rcount");
			return tc;

		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		} finally {
			cmd.Close();
		}
	}

	/***
	 * 現在記録されている複数のタイムカードを取得します
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @return
	 */
	public static List<TimeCard> getTimeCards(int oid, int uid, int tdate) {

		DBCmd cmd = new DBCmd("SELECT t.oid, t.uid, u.name, t.tdate, "
				+ "t.times, t.intime, t.outtime, t.breaktime, t.wid, "
				+ "t.note, t.fare, t.wcount, t.rcount "
				+ "FROM timecard AS t, Users AS u "
				+ "WHERE t.oid=u.oid AND t.uid=u.uid "
				+ "AND t.oid=? AND t.uid=? AND tdate=?;");
		try {
			cmd.setIntParam(1, oid);
			cmd.setIntParam(2, uid);
			cmd.setIntParam(3, tdate);
			ResultSet res = cmd.ExcuteResult();
			if (res == null) return null;

			List<TimeCard> list = new ArrayList<TimeCard>();

			while (res.next()) {
				int times = res.getInt("times");
				TimeCard tc = new TimeCard(oid, uid, tdate, times);
				tc.name = res.getString("name");
				tc.intime = res.getInt("intime");
				tc.outtime = res.getInt("outtime");
				tc.breaktime = res.getInt("breaktime");
				tc.wid = res.getInt("wid");
				tc.content = res.getString("note");
				tc.fare = res.getInt("fare");
				tc.wcount = res.getInt("wcount");
				tc.rcount = res.getInt("rcount");
				list.add(tc);
			}

			return list;

		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		} finally {
			cmd.Close();
		}
	}

	/**
	 * 現在登録されているタイムカードのうち、過去最後に打刻されたタイムカードを取得します
	 * @param oid
	 * @param uid
	 * @return
	 */
	public static List<TimeCard> GetLastWorkTimeCard(int oid, int uid) {

		DBCmd cmd = new DBCmd("SELECT t.oid, t.uid, u.name, t.tdate, "
				+ "t.times, t.intime, t.outtime, t.breaktime, t.wid, "
				+ "t.note, t.fare, t.wcount, t.rcount "
				+ "FROM timecard AS t, Users AS u "
				+ "WHERE t.oid=u.oid AND t.uid=u.uid "
				+ "AND t.oid=? AND t.uid=? AND t.tdate<=?"
				+ "ORDER BY t.tdate DESC "
				+ "LIMIT 2");
		try {
			cmd.setIntParam(1, oid);
			cmd.setIntParam(2, uid);
			cmd.setIntParam(3, DateTime.GetTodayInt());
			ResultSet res = cmd.ExcuteResult();
			if (res == null) return null;

			List<TimeCard> list = new ArrayList<TimeCard>();

			while (res.next()) {
				int tdate = res.getInt("tdate");
				int times = res.getInt("times");
				TimeCard tc = new TimeCard(oid, uid, tdate, times);
				tc.name = res.getString("name");
				tc.intime = res.getInt("intime");
				tc.outtime = res.getInt("outtime");
				tc.breaktime = res.getInt("breaktime");
				tc.wid = res.getInt("wid");
				tc.content = res.getString("note");
				tc.fare = res.getInt("fare");
				tc.wcount = res.getInt("wcount");
				tc.rcount = res.getInt("rcount");
				list.add(tc);
			}

			if (list.size() == 2) {
				//二日分はいらない
				if(list.get(0).tdate != list.get(1).tdate) {
					list.remove(1);
				}
			}

			return list;

		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		} finally {
			cmd.Close();
		}
	}

	/***
	 * NFCによる出勤報告を記録します
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @param wt
	 * @param latitude
	 * @param longitude
	 * @param intime
	 * @param entrytime
	 * @return
	 */
	public static boolean NfcIn(int oid, int uid, int tdate, String latitude,
			String longitude, int intime, int entryDate, int entrytime, String unitid) {
		List<TimeCard> tcs = getTimeCards(oid, uid, tdate);

		if (tcs == null) return false;

		// ログ
		DecimalFormat df = new DecimalFormat("00:00");
		TimeCardLog tcl = new TimeCardLog(oid, uid);
		tcl.setLatitude(latitude);
		tcl.setLongitude(longitude);
		tcl.setCardDate(tdate);
		tcl.setLogdate(entryDate);
		tcl.setLogtime(entrytime);
		tcl.setLogtype("NFC 出勤報告");
		tcl.setNote("出勤時刻:" + df.format(intime) + ",");
		tcl.setUnitID(unitid);

		TimeCard tc;
		boolean tcIsNew = false;

		Optional<TimeCard> ot = tcs.stream().filter(t -> t.times == 1).findFirst();
		if (ot.isPresent()) {
			tc = ot.get();
		} else {
			tc = new TimeCard(oid, uid, tdate, 1);
			tcIsNew = true;
		}

		tc.intime = intime;

		Transaction tran = Transaction.CreateTran();
		try {
			if (!tcl.Write(tran)) {
				return false;
			}

			if (tcIsNew)
			{
				if (!tc.insert(tran)) {
					tran.Rollback();
					return false;
				}
			} else {
				if (!tc.update(tran)) {
					tran.Rollback();
					return false;
				}
			}
			return tran.Commit();
		} catch (Exception e) {
			tran.Rollback();
			Log.exceptionWrite(e);
			return false;
		}
	}

	/***
	 * NFCによる移動報告を記録します
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @param wt
	 * @param latitude
	 * @param longitude
	 * @param movetime
	 * @param entrytime
	 * @param content
	 * @param wcount
	 * @param rcount
	 * @return
	 */
	public static boolean NfcMove(int oid, int uid, int tdate, WorkType wt,
			String latitude, String longitude, int movetime, int entryDate,
			int entrytime, String content, int wcount, int rcount, String unitid) {

		List<TimeCard> tcs = getTimeCards(oid, uid, tdate);
		if (tcs == null)
			return false;

		// ログ
		DecimalFormat df = new DecimalFormat("00:00");
		TimeCardLog tcl = new TimeCardLog(oid, uid);
		tcl.setLatitude(latitude);
		tcl.setLongitude(longitude);
		tcl.setCardDate(tdate);
		tcl.setLogdate(DateTime.GetTodayInt());
		tcl.setLogtime(entrytime);
		tcl.setLogtype("NFC 移動報告");
		tcl.setNote("移動時刻:" + df.format(movetime) + ","
				+ "移動前業務内容:" + content + ","
				+ "移動前処理枚数:" + String.valueOf(wcount) + ","
				+ "移動前申出枚数:" + String.valueOf(rcount) + ","
				+ "移動後業務種別:" + wt.getName() + ",");
		tcl.setUnitID(unitid);

		Transaction tran = Transaction.CreateTran();
		TimeCard tc1;
		TimeCard tc2;
		boolean tc1IsNew = false;
		boolean tc2IsNew = false;

		Optional<TimeCard> ot1 = tcs.stream().filter(tc -> tc.times == 1).findFirst();
		if (ot1.isPresent()) {
			tc1 = ot1.get();
		} else {
			tc1 = new TimeCard(oid, uid, tdate, 1);
			tc1IsNew = true;
		}

		Optional<TimeCard> ot2 = tcs.stream().filter(tc -> tc.times == 2).findFirst();
		if (ot2.isPresent()) {
			tc2 = ot2.get();
		} else {
			tc2 = new TimeCard(oid, uid, tdate, 2);
			tc2.wid = tc1.wid;
			tc2.content = tc1.content;
			tc2.wcount = tc1.wcount;
			tc2.rcount = tc1.rcount;
			if (tc1.outtime != 0) {
				tc2.outtime = tc1.outtime;
			}
			tc2IsNew = true;
		}

		tc1.content = content;
		tc1.wcount = wcount;
		tc1.outtime = movetime;
		tc1.rcount = rcount;
		tc1.wid = wt.wid;
		tc2.intime = movetime;

		try {
			if (!tcl.Write(tran)) {
				tran.Rollback();
				return false;
			}

			if (tc1IsNew) {
				if (!tc1.insert(tran)) {
					tran.Rollback();
					return false;
				}
			} else {
				if (!tc1.update(tran)) {
					tran.Rollback();
					return false;
				}
			}

			if (tc2IsNew) {
				if (!tc2.insert(tran)) {
					tran.Rollback();
					return false;
				}
			} else {
				if (!tc2.update(tran)) {
					tran.Rollback();
					return false;
				}
			}
			return tran.Commit();
		} catch (Exception e) {
			tran.Rollback();
			Log.exceptionWrite(e);
			return false;
		}
	}

	public static boolean NfcOut(int oid, int uid, int tdate, WorkType wt, String latitude,
			String longitude, int outtime, int entryDate, int entrytime,
			String content, int wcount, int rcount, int breaktime, int fare, String unitid) {
		List<TimeCard> tcs = getTimeCards(oid, uid, tdate);

		if (tcs == null)
			return false;

		// ログ
		DecimalFormat df = new DecimalFormat("00:00");
		TimeCardLog tcl = new TimeCardLog(oid, uid);
		tcl.setLatitude(latitude);
		tcl.setLongitude(longitude);
		tcl.setCardDate(tdate);
		tcl.setLogdate(DateTime.GetTodayInt());
		tcl.setLogtime(entrytime);
		tcl.setLogtype("NFC 退勤報告");
		tcl.setNote("退勤時刻:" + df.format(outtime) + ","
				+ "業務種別:" + wt.getName() + ","
				+ "業務内容:" + content + ","
				+ "休憩時間:" + breaktime + ","
				+ "交通費:" + fare + ","
				+ "処理枚数:" + String.valueOf(wcount) + ","
				+ "申出枚数:" + String.valueOf(rcount) + ",");
		tcl.setUnitID(unitid);

		Transaction tran = Transaction.CreateTran();
		TimeCard tc1 = null;
		TimeCard tc2 = null;
		boolean tc1IsNew = false;
		boolean tc2IsNew = false;

		Optional<TimeCard> ot1 = tcs.stream().filter(tc -> tc.times == 1).findFirst();
		if (ot1.isPresent()) {
			tc1 = ot1.get();
		} else {
			tc1 = new TimeCard(oid, uid, tdate, 1);
			tc1IsNew = true;
		}

		Optional<TimeCard> ot2 = tcs.stream().filter(tc -> tc.times == 2).findFirst();
		if (ot2.isPresent()) {
			tc2 = ot2.get();
		}

		if (tc2 == null) {
			tc1.outtime = outtime;
			tc1.breaktime = breaktime;
			tc1.wid = wt.wid;
			tc1.content = content;
			tc1.wcount = wcount;
			tc1.rcount = rcount;
			tc1.fare = fare;
		} else {
			tc1.breaktime = breaktime;
			tc1.fare = fare;
			tc2.breaktime = 0;
			tc2.outtime = outtime;
			tc2.wid = wt.wid;
			tc2.content = content;
			tc2.wcount = wcount;
			tc2.rcount = rcount;
		}

		try {
			if (!tcl.Write(tran)) {
				tran.Rollback();
				return false;
			}

			if (tc1IsNew) {
				if (!tc1.insert(tran)) {
					tran.Rollback();
					return false;
				}
			} else {
				if (!tc1.update(tran)) {
					tran.Rollback();
					return false;
				}
			}

			if (tc2 != null) {
				if (tc2IsNew) {
					if (!tc2.insert(tran)) {
						tran.Rollback();
						return false;
					}
				} else {
					if (!tc2.update(tran)) {
						tran.Rollback();
						return false;
					}
				}
			}

			return tran.Commit();
		} catch (Exception e) {
			tran.Rollback();
			Log.exceptionWrite(e);
			return false;
		}
	}

	public static String GetWorkStr(int work) {
		switch (work) {
		case WORK_IN:
			return "出勤";
		case WORK_MOVE:
			return "移動";
		case WORK_OUT:
			return "退勤";
		default:
			return "不明";
		}
	}

	public static TimeExcessResult IsInTimeExcess(int oid, int uid, int tdate, int intime) {
		TimeExcessResult r = new TimeExcessResult(WORK_IN);
		List<TimeCard> tcs = getTimeCards(oid, uid, tdate);
		if (tcs == null || tcs.size() == 0) {
			r.excess = false;
			return r;	// 比較対象なしにつき問題なし
		}
		if (tcs.size() == 1) {
			int outtime = tcs.get(0).outtime;
			if (outtime >= 0 && outtime < intime) {
				r.compareWork = WORK_OUT;
				r.excess = true;
				r.compareTime = outtime;
				return r;	// 退勤＜出勤
			}
		} else if (tcs.size() > 1) {
			for (int i=0; i<tcs.size(); i++) {
				if (tcs.get(i).times == 1) {
					r.compareWork = WORK_MOVE;
					int movetime = tcs.get(i).outtime;
					if (movetime >= 0 && movetime < intime) {
						r.excess = true;
						r.compareTime = movetime;
						return r;	// 移動＜出勤（退勤との比較は移動時に行っているのでここではしない）
					}
					r.excess = false;
					return r;
				}
			}
		}
		return r;
	}

	public static TimeExcessResult IsMoveTimeExcess(int oid, int uid, int tdate, int movetime) {
		TimeExcessResult r = new TimeExcessResult(WORK_MOVE);
		List<TimeCard> tcs = getTimeCards(oid, uid, tdate);
		if (tcs == null || tcs.size() == 0) {
			r.excess = false;
			return r;	// 比較対象なしにつき問題なし
		}
		if (tcs.size() == 1) {
			int outtime = tcs.get(0).outtime;
			if (outtime >= 0 && outtime < movetime) {
				r.compareWork = WORK_OUT;
				r.excess = true;
				r.compareTime = outtime;
				return r;	// 退勤＜移動
			}
		} else if (tcs.size() > 1) {
			for (int i=0; i<tcs.size(); i++) {
				if (tcs.get(i).times == 2) {
					r.compareWork = WORK_OUT;
					int outtime = tcs.get(i).outtime;
					if (outtime >= 0 && outtime < movetime) {
						r.excess = true;
						r.compareTime = outtime;
						return r;	// 退勤＜移動
					}
					r.excess = false;
					return r;
				}
			}
		}
		return r;
	}

	public static TimeExcessResult IsOutTimeExcess(int oid, int uid, int tdate, int outtime) {
		TimeExcessResult r = new TimeExcessResult(WORK_OUT);
		List<TimeCard> tcs = getTimeCards(oid, uid, tdate);
		if (tcs == null || tcs.size() == 0) {
			r.excess = false;
			return r;	// 比較対象なしにつき問題なし
		}
		if (tcs.size() == 1) {
			int intime = tcs.get(0).intime;
			if (intime >= 0 && intime > outtime) {
				r.compareWork = WORK_IN;
				r.excess = true;
				r.compareTime = intime;
				return r;	// 出勤＞退勤
			}
		} else if (tcs.size() > 1) {
			for (int i=0; i<tcs.size(); i++) {
				if (tcs.get(i).times == 2) {
					r.compareWork = WORK_MOVE;
					int movetime = tcs.get(i).intime;
					if (movetime >= 0 && movetime > outtime) {
						r.excess = true;
						r.compareTime = movetime;
						return r;	// 移動＞退勤
					}
					r.excess = false;
					return r;
				}
			}
		}
		return r;
	}

	public static class TimeExcessResult {
		public int work;
		public int compareWork = -1;
		public boolean excess;
		public int compareTime;
		public TimeExcessResult(int work) {
			this.work = work;
		}
	}
}
