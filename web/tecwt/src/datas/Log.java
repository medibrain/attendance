package datas;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;

public class Log {

	static String dirName;
	static File file;

	static {
		dirName = new File("").getAbsolutePath();
		if (dirName.equals(File.separator)) {
			dirName = "/usr/local/test/log";
		} else {
			dirName += File.separator + "log";
		}
		file = new File(dirName + File.separator + "test.log");
	}

	static void createLogDir() {
		File f = new File(dirName);
		if (!f.exists()) {
			f.mkdir();
		}
	}

	public static void Write(String str) {
		createLogDir();
		//        String fn = "log\\test.txt";
		//        File file = new File(fn);
		try {
			FileWriter filewriter = new FileWriter(file, true);
			filewriter.write(str + "\r\n");
			filewriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void exceptionWrite(Exception e) {
		createLogDir();
		String fn = dirName + File.separator + "error" + DateTime.GetTodayStr() + ".log";
		File file = new File(fn);
		try {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			String trace = sw.toString();
			sw.close();
			pw.close();

			FileWriter filewriter = new FileWriter(file, true);
			filewriter.write(DateTime.GetNowStr());
			filewriter.write(trace + "\r\n");

			filewriter.close();
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}
}
