package datas;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.UIDFolder;

public class ReceiveMail {

	private int oid;
	private int muid;
	private Date date;
	private String subject;
	private String address;
	private String body;

	public int getOid() {
		return oid;
	}

	public int getMuid() {
		return muid;
	}

	public String getAddress() {
		return address;
	}

	public String getBody() {
		return body;
	}

	public String getSubject() {
		return subject;
	}

	public Date getDate() {
		return date;
	}

	/**
	 * メールを受信します
	 *
	 * @param org
	 * @return
	 */
	public static ArrayList<ReceiveMail> Receive(Organization org) {
		// String USER = "techno.setest.4102.90@gmail.com";
		// String HOST = "imap.gmail.com";
		// int PORT = 993;
		// String PASSWORD = "technoadmin";
		// String TARGET_FOLDER = "inbox";

		String USER = org.getLoginID();
		String HOST = org.getImapHost();
		int PORT = org.getImapPort();
		String PASSWORD = org.getPassword();
		String TARGET_FOLDER = org.getFolder();

		ArrayList<ReceiveMail> list = new ArrayList<>();

		try {
			Properties prop = System.getProperties();
			prop.setProperty("mail.imap.connectiontimeout", "10000");
			prop.setProperty("mail.imap.timeout", "10000");
			Session session = Session.getInstance(prop);
			// session.setDebug(true); // デバッグモードにする
			Store store = session.getStore("imaps");

			try {
				store.connect(HOST, PORT, USER, PASSWORD);
			} catch (Exception e) {
				// 接続失敗時 ログには残さない
				store.close();
				return null;
			}

			Folder fol = store.getFolder(TARGET_FOLDER);

			try {
				if (fol.exists()) {
					fol.open(Folder.READ_WRITE);

					int receiveCount = 0;
					for (Message m : fol.getMessages()) {
						int muid = (int) ((UIDFolder) fol).getUID(m);

						// DBにあればメールを削除するフラグを立て、メール処理は飛ばす
						if (Mail.ExistDB(org.oid, muid)) {
							m.setFlag(Flags.Flag.DELETED, true);
							continue;
						}

						ReceiveMail rMail = ReceiveMail.createReceiveMail(m,
								org.oid, muid);
						list.add(rMail);
						receiveCount++;
					}

					if (receiveCount != 0) {
						Log.Write(DateTime.GetNowStr() + org.getName() + ": "
								+ Integer.toString(receiveCount)
								+ " mail received");
					}
				}
			} catch (Exception e) {
				Log.exceptionWrite(e);
				return null;
			} finally {
				// trueでフラグ付きを削除
				// fol.close(true);
				fol.close(false);
				store.close();
			}
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		}

		return list;
	}

	/**
	 * メッセージパートから本文テキストデータを選択・取得します
	 *
	 * @param p
	 * @return
	 * @throws Exception
	 */
	private static String getTextBody(Part p) throws Exception {
		// テキストの場合
		if (p.isMimeType("text/plain")) {
			return (String) p.getContent();
		}
		// マルチパートの場合、再帰的に探索する。
		if (p.isMimeType("multipart/*")) {
			Multipart mp = (Multipart) p.getContent();
			int count = mp.getCount();
			for (int i = 0; i < count; i++) {
				BodyPart bp = mp.getBodyPart(i);
				String rs = getTextBody(bp);
				if (rs != null) {
					return rs;
				}
			}
		}
		// その他の場合、添付ファイル名とContent-Typeを表示する。
		return null;
	}

	/***
	 * 受信メール内容を整理してインスタンスを作成します
	 *
	 * @param m
	 * @param uid
	 * @return
	 */
	private static ReceiveMail createReceiveMail(Message m, int oid, int muid) {
		ReceiveMail rm = new ReceiveMail();
		try {
			rm.oid = oid;
			rm.muid = muid;
			rm.subject = m.getSubject();
			String[] s = m.getHeader("from");
			// Log.Write("uid=" + Integer.toString(rm.id) + " from=" + s[0]);
			if (s.length > 0) {
				if (s[0].contains("<")) {
					rm.address = s[0].substring(s[0].indexOf('<') + 1,
							s[0].indexOf('>'));
				} else {
					rm.address = s[0];
				}
			}
			rm.date = m.getSentDate();
			rm.body = getTextBody(m);
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		}

		return rm;
	}
}
