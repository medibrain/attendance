package datas;
import java.sql.ResultSet;

public class Mail {

	private int oid;
	private int muid;
	private int rcvDate;
	private int rcvTime;
	private String maillAdd;
	private int uID;
	private String rcvSubject;
	private String rcvBody;
	private int sndTime;
	private String sndSubject;
	private String sndBody;
	private int status;

	public int getOid() {
		return oid;
	}

	public int getMuid() {
		return muid;
	}

	public int getRcvDate() {
		return rcvDate;
	}

	public int getRcvTime() {
		return rcvTime;
	}

	public String getMaillAdd() {
		return maillAdd;
	}

	public void setMaillAdd(String maillAdd) {
		this.maillAdd = maillAdd;
	}

	public int getUID() {
		return uID;
	}

	public void setuID(int uID) {
		this.uID = uID;
	}

	public String getRcvSubject() {
		return rcvSubject;
	}

	public void setRcvSubject(String rcvSubject) {
		this.rcvSubject = rcvSubject;
	}

	public String getRcvBody() {
		return rcvBody;
	}

	public void setRcvBody(String rcvBody) {
		this.rcvBody = rcvBody;
	}

	public int getSndTime() {
		return sndTime;
	}

	public void setSndTime(int sndTime) {
		this.sndTime = sndTime;
	}

	public String getSndSubject() {
		return sndSubject;
	}

	public void setSndSubject(String sndSubject) {
		this.sndSubject = sndSubject;
	}

	public String getSndBody() {
		return sndBody;
	}

	public void setSndBody(String sndBody) {
		this.sndBody = sndBody;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * メール受信処理が終了し、DBに記録されているかを取得します
	 *
	 * @param oid
	 * @param muid
	 * @return
	 */
	public static boolean ExistDB(int oid, int muid) {
		DBCmd cmd = new DBCmd("SELECT muid FROM mails "
				+ "WHERE oid=? AND muid=?");
		try {
			ResultSet res = cmd.ExcuteResult();
			return res.next();
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		} finally {
			cmd.Close();
		}
	}

	/**
	 * 受信メールからデータを作成し、データベースに記録します
	 *
	 * @param m
	 * @param org
	 * @return
	 */
	public static Mail CreateMailAndWrite(ReceiveMail rm) {
		Mail m = new Mail();
		m.oid = rm.getOid();
		m.muid = rm.getMuid();
		m.rcvDate = DateTime.DateToIntDate(rm.getDate());
		m.rcvTime = DateTime.DateToIntTime(rm.getDate());
		m.maillAdd = rm.getAddress();
		m.rcvSubject = rm.getSubject();
		m.rcvBody = rm.getBody();
		m.sndTime = 0;
		m.sndSubject = "";
		m.sndBody = "";
		m.status = 0;

		if (m.Insert())
			return null;
		return m;
	}

	/***
	 * DBに登録します
	 *
	 * @return
	 */
	public boolean Insert() {
		DBCmd cmd = new DBCmd(
				"INSERT INTO mails( "
						+ "oid, muid, rcvdate, rcvtime, mailladd, uid, rcvsubject, rcvbody, "
						+ "sndtime, sndsubject, sndbody, status) "
						+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
		try {
			cmd.setIntParam(1, this.oid);
			cmd.setIntParam(2, this.muid);
			cmd.setIntParam(3, this.rcvDate);
			cmd.setIntParam(4, this.rcvTime);
			cmd.setIntParam(5, this.uID);
			cmd.setStrParam(6, this.rcvSubject);
			cmd.setStrParam(7, this.rcvBody);
			cmd.setIntParam(8, this.sndTime);
			cmd.setStrParam(9, this.sndSubject);
			cmd.setStrParam(10, this.sndBody);
			cmd.setIntParam(11, this.status);

			return cmd.ExcuteNonQuery();
		} catch (Exception e) {
			return false;
		} finally {
			cmd.Close();
		}
	}

	/***
	 * DBの情報をアップデートします
	 *
	 * @return
	 */
	public boolean Update() {
		DBCmd cmd = new DBCmd(
				"UPDATE mails "
						+ "SET oid=?, muid=?, rcvdate=?, rcvtime=?, mailladd=?, uid=?, rcvsubject=?, "
						+ "rcvbody=?, sndtime=?, sndsubject=?, sndbody=?, status=? "
						+ "WHERE oid=? AND muid=?;");
		try {
			cmd.setIntParam(1, this.rcvDate);
			cmd.setIntParam(2, this.rcvTime);
			cmd.setIntParam(3, this.uID);
			cmd.setStrParam(4, this.rcvSubject);
			cmd.setStrParam(5, this.rcvBody);
			cmd.setIntParam(6, this.sndTime);
			cmd.setStrParam(7, this.sndSubject);
			cmd.setStrParam(8, this.sndBody);
			cmd.setIntParam(9, this.status);
			cmd.setIntParam(10, this.oid);
			cmd.setIntParam(11, this.muid);

			return cmd.ExcuteNonQuery();
		} catch (Exception e) {
			return false;
		} finally {
			cmd.Close();
		}
	}
}
