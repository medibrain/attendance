package datas;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.Normalizer;

/**
 *
 * @author TechnoSystemMastor
 */
public class Common {

	/**
	 * 半角文字にします。
	 *
	 * @param str
	 * @return
	 */
	public static String toHalf(String str) {
		return Normalizer.normalize(str, Normalizer.Form.NFKC);
	}

	/**
	 * 半角文字にし、/:;- ､で区切られた２つの文字配列を返します
	 *
	 * @param str
	 * @return
	 */
	public static String[] toHalfSpirit(String str) {
		String[] ss = new String[2];
		str = toHalf(str);

		for (int c = 0; c < str.length(); c++) {
			if ("/:;- ､".indexOf(str.charAt(c)) >= 0) {
				ss[0] = str.substring(0, c).trim();
				ss[1] = str.substring(c + 1).trim();
				return ss;
			}
		}

		ss[0] = str.trim();
		ss[1] = "";
		return ss;
	}

	/**
	 * 文字列を数値に変換します。失敗した場合、-1が返ります。
	 *
	 * @param s
	 * @return
	 */
	public static int tryParse(String s) {
		try {
			return Integer.parseInt(s);
		} catch (Exception e) {
			return -1;
		}
	}

	public static String URLEnc(String str) {
		String encodeStr;
		try {
			encodeStr = URLEncoder.encode("*-_", "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return "";
		}
		encodeStr = encodeStr.replace("*", "%2a");
		encodeStr = encodeStr.replace("-", "%2d");
		encodeStr = encodeStr.replace("+", "%20");
		return encodeStr;
	}

	public static String URLDec(String str) {
		try {
			return URLDecoder.decode(str, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return "";
		}
	}
	
	public static String JsonEscape(String json)
	{
		json = json.replace("\\", "\\\\");
		json = json.replace("/", "\\/");
		json = json.replace("\"", "\\\"");
		return json;
	}
}
