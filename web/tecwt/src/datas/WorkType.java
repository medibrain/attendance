package datas;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class WorkType {

	int oid;
	int wid;
	String name;
	String kana;
	boolean enable;

	public int getOid() {
		return oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public int getWid() {
		return wid;
	}

	public void setWid(int wid) {
		this.wid = wid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKana() {
		return kana;
	}

	public void setKana(String kana) {
		this.kana = kana;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public boolean Insert() {
		DBCmd cmd = new DBCmd("INSERT INTO worktype "
				+ "oid, wid, name, kana, enable, idate, iuid, udate, uuid) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);");

		try {
			cmd.setIntParam(1, oid);
			cmd.setIntParam(2, wid);
			cmd.setStrParam(3, name);
			cmd.setStrParam(4, kana);
			cmd.setBoolParam(5, enable);
			cmd.setIntParam(6, DateTime.GetTodayInt());
			cmd.setIntParam(7, 0);
			cmd.setIntParam(8, 0);
			cmd.setIntParam(9, 0);

			return cmd.ExcuteNonQuery();
		} catch (Exception ex) {
			Log.exceptionWrite(ex);
			return false;
		} finally {
			cmd.Close();
		}
	}

	public static List<WorkType> GetWorkTypeList(int oid) {
		List<WorkType> list = new ArrayList<WorkType>();

		DBCmd cmd = new DBCmd("SELECT oid, wid, name, kana, enable "
				+ "FROM worktype WHERE oid=? " + "AND enable=TRUE "
				+ "ORDER BY kana");
		try {
			cmd.setIntParam(1, oid);
			ResultSet rs = cmd.ExcuteResult();
			while (rs.next()) {
				WorkType wt = new WorkType();
				wt.oid = rs.getInt("oid");
				wt.wid = rs.getInt("wid");
				wt.name = rs.getString("name");
				wt.kana = rs.getString("kana");
				wt.enable = rs.getBoolean("enable");
				list.add(wt);
			}
		} catch (Exception ex) {
			Log.exceptionWrite(ex);
			return null;
		} finally {
			cmd.Close();
		}

		return list;
	}

	public static WorkType Select(int oid, int wid) {

		DBCmd cmd = new DBCmd("SELECT oid, wid, name, kana, enable "
				+ "FROM worktype WHERE oid=? AND wid=? AND enable=TRUE "
				+ "ORDER BY kana");
		try {
			cmd.setIntParam(1, oid);
			cmd.setIntParam(2, wid);
			ResultSet rs = cmd.ExcuteResult();
			if (!rs.next()) return null;

			WorkType wt = new WorkType();
			wt.oid = rs.getInt("oid");
			wt.wid = rs.getInt("wid");
			wt.name = rs.getString("name");
			wt.kana = rs.getString("kana");
			wt.enable = rs.getBoolean("enable");
			return wt;
		} catch (Exception ex) {
			Log.exceptionWrite(ex);
			return null;
		} finally {
			cmd.Close();
		}

	}

}
