package datas;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class Organization {

	int oid;
	String name;
	boolean enable;
	String loginID;
	String password;
	String imapHost;
	int imapPort;
	String folder;
	String smtpHost;
	int smtpPort;
	String mailAdd;

	public int getOid() {
		return oid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getLoginID() {
		return loginID;
	}

	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getImapHost() {
		return imapHost;
	}

	public void setImapHost(String imapHost) {
		this.imapHost = imapHost;
	}

	public int getImapPort() {
		return imapPort;
	}

	public void setImapPort(int imapPort) {
		this.imapPort = imapPort;
	}

	public String getFolder() {
		return folder;
	}

	public void setFolderName(String folder) {
		this.folder = folder;
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public int getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPortNo(int smtpPort) {
		this.smtpPort = smtpPort;
	}

	public String getMailAdd() {
		return mailAdd;
	}

	public void setMailAdd(String mailAdd) {
		this.mailAdd = mailAdd;
	}

	public static int GetOidFromSesion(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		if (session == null) return 0;

		int oid;
		try {
			oid = (int) session.getAttribute("oid");
		} catch (Exception e) {
			return 0;
		}
		return oid;
	}

	public static Organization getOrganization(int oid) {

		DBCmd cmd = new DBCmd("SELECT "
				+ "oid, name, enable, loginid, pass, imaphost, imapport, "
				+ "folder, smtphost, smtpport, mailladd "
				+ "FROM organization WHERE oid=?");
		try {
			cmd.setIntParam(1, oid);
			ResultSet res = cmd.ExcuteResult();
			if (res == null || !res.next()) return null;

			Organization org = new Organization();
			org.oid = res.getInt("oid");
			org.name = res.getString("name");
			org.enable = res.getBoolean("enable");
			org.loginID = res.getString("loginid");
			org.password = res.getString("pass");
			org.imapHost = res.getString("imaphost");
			org.imapPort = res.getInt("imapport");
			org.folder = res.getString("folder");
			org.smtpHost = res.getString("smtphost");
			org.smtpPort = res.getInt("smtpport");
			org.mailAdd = res.getString("mailladd");
			return org;
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		} finally {
			cmd.Close();
		}
	}

	/**
	 * 有効な組織の一覧を返します
	 * @return
	 */
	public static List<Organization> GetVaildOrg() {
		List<Organization> list = new ArrayList<Organization>();

		DBCmd cmd = new DBCmd(
				"SELECT "
						+ "oid, name, enable, loginid, pass, imaphost, imapport, "
						+ "folider, smtphost, smtpport, mailladd "
						+ "FROM organization" + "WHERE enable");

		try {
			ResultSet res = cmd.ExcuteResult();
			if (res == null)
				return null;

			while (res.next()) {
				Organization org = new Organization();
				org.oid = res.getInt("oid");
				org.name = res.getString("name");
				org.enable = res.getBoolean("enable");
				org.loginID = res.getString("loginid");
				org.password = res.getString("pass");
				org.imapHost = res.getString("imaphost");
				org.imapPort = res.getInt("imapport");
				org.folder = res.getString("folder");
				org.smtpHost = res.getString("smtphost");
				org.smtpPort = res.getInt("smtpport");
				org.mailAdd = res.getString("mailladd");
				list.add(org);
			}
		} catch (Exception e) {
			Log.Write(e.getMessage());
			return null;
		} finally {
			cmd.Close();
		}

		return list;
	}
}
