package datas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

public class DBCmd {

	//TODO: medi-kintai
	//static String server = "160.16.75.99:5432"; // URL
	//static String dbname = "tecwt"; // データベース名
	//static String usr = "tecadmin"; // ユーザ名
	//static String pwd = "Tec242-Post"; // パスワード
	static String server = "133.167.104.74:5432"; // URL
	static String dbname = "tecwt"; // データベース名
	static String usr = "postgres"; // ユーザ名
	static String pwd = "NiCE2018"; // パスワード


	//public static PGPoolingDataSource source;
	public static DataSource source;
	boolean exTran = false;

	static {
		PoolProperties p = new PoolProperties();

		// 接続情報の設定
		p.setUrl("jdbc:postgresql://" + server + "/" + dbname);
		p.setDriverClassName("org.postgresql.Driver");
		p.setUsername(usr);
		p.setPassword(pwd);

		// その他オプション
		p.setJmxEnabled(true);
		p.setTestWhileIdle(false);
		p.setTestOnBorrow(true);
		p.setValidationQuery("SELECT 1");
		p.setTestOnReturn(false);
		p.setValidationInterval(30000);
		p.setTimeBetweenEvictionRunsMillis(30000);
		p.setMaxActive(100);
		p.setInitialSize(10);
		p.setMaxWait(10000);
		p.setRemoveAbandonedTimeout(60);
		p.setMinEvictableIdleTimeMillis(30000);
		p.setMinIdle(10);
		p.setLogAbandoned(true);
		p.setRemoveAbandoned(true);
		p.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"
				+ "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");

		source = new DataSource();
		source.setPoolProperties(p);

		/*
		source = new PGPoolingDataSource();
		source.setDataSourceName("Default Source");
		source.setServerName(server);
		source.setDatabaseName(dbname);
		source.setUser(usr);
		source.setPassword(pwd);
		source.setMaxConnections(10);


		try {
			// JDBCドライバのロード
			Class.forName("org.postgresql.Driver");
		} catch (Exception e) {
			Log.exceptionWrite(e);
		}
		*/
	}

	private boolean open() {
		try {
			//コネクションプールの使用
			conn = source.getConnection();
			return true;
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return false;
		}
	}

	private Connection conn;
	private PreparedStatement st;

	public DBCmd(String sqlStr) {
		open();
		try {
			st = conn.prepareStatement(sqlStr);
		} catch (Exception e) {
			Log.exceptionWrite(e);
		}
	}

	public DBCmd(String sqlStr, Transaction tran) {
		exTran = true;
		try {
			conn = tran.getConnection();
		} catch (Exception e) {
			Log.exceptionWrite(e);
		}

		try {
			st = conn.prepareStatement(sqlStr);
			tran.AddStatement(st);
		} catch (Exception e) {
			Log.exceptionWrite(e);
		}
	}

	public void setStrParam(int index, String s) throws SQLException {
		st.setString(index, s);
	}

	public void setIntParam(int index, int i) throws SQLException {
		st.setInt(index, i);
	}

	public void setLongParam(int index, long l) throws SQLException {
		st.setLong(index, l);
	}

	public void setBoolParam(int index, boolean b) throws SQLException {
		st.setBoolean(index, b);
	}

	public void setTimeStampParam(int index, java.util.Date d)
			throws SQLException {
		st.setTimestamp(index, new java.sql.Timestamp(d.getTime()));
	}

	public void setDateParam(int index, java.util.Date d) throws SQLException {
		st.setDate(index, new java.sql.Date(d.getTime()));
	}

	public boolean ExcuteNonQuery() {
		try {
			st.executeUpdate();
			return true;
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return false;
		}
	}

	public int ExcuteNonQueryCount() {
		try {
			return st.executeUpdate();
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return -1;
		}
	}

	public ResultSet ExcuteResult() {
		try {
			return st.executeQuery();
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		}
	}

	public void Close() {
		try {
			st.close();
			if (!exTran) conn.close();
		} catch (Exception e) {
			Log.exceptionWrite(e);
		}
	}
}
