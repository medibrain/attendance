package datas;
import java.sql.ResultSet;

public class MailAdd {

	private int oid;
	private String address;
	private int uid;
	private boolean enable;
	private boolean send;


	public int getOid() {
		return oid;
	}

	public String getAddress() {
		return address;
	}

	public int getUid() {
		return uid;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public boolean isSend() {
		return send;
	}

	public void setSend(boolean send) {
		this.send = send;
	}

	private MailAdd() {
	}

	public MailAdd(String add, int oid, int uid) {
		this.address = add;
		this.oid = oid;
		this.uid = uid;
	}

	/***
	 * 指定したメールアドレスから、メールアドレス情報を返します
	 *
	 * @param maillAdd
	 * @return
	 */
	public static MailAdd GetUID(int oid, String mailAdd) {
		DBCmd cmd = new DBCmd("SELECT uid, enable " + "FROM mailadd "
				+ "WHERE oid=? AND add=?;");
		try {
			cmd.setIntParam(1, oid);
			cmd.setStrParam(2, mailAdd);
			ResultSet res = cmd.ExcuteResult();

			if (res.next()) {
				MailAdd ma = new MailAdd();
				ma.address = mailAdd;
				ma.oid = oid;
				ma.uid = res.getInt("uid");
				ma.enable = res.getBoolean("enable");
				return ma;
			} else {
				return null;
			}
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		} finally {
			cmd.Close();
		}
	}

	public boolean Insert() {
		DBCmd cmd =new DBCmd("INSERT INTO ");
		return cmd.ExcuteNonQuery();
	}

	public boolean Update() {
		DBCmd cmd =new DBCmd("UPDATE INTO ");
		return cmd.ExcuteNonQuery();
	}

}
