package datas;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class logtest
 */
@WebServlet("/logtest")
public class Logtest extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Logtest() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String func = request.getParameter("action");
		String id = request.getParameter("nfcidm");
		String time = request.getParameter("time");

		if (func == null)
			func = "null";

		if (time == null)
			time = "000000";

		int timei = DateTime.GetNowInt();

		DBCmd cmd = new DBCmd("UPDATE log SET itime=? WHERE id=1");
		try {

			cmd.setIntParam(1, timei);
			cmd.ExcuteNonQuery();
		} catch (Exception e) {
			id=e.getMessage();
		} finally {
			cmd.Close();
		}

		// jsonデータを返す
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		response.getWriter().write(id + ":" + String.valueOf(timei));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
