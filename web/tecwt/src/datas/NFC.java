package datas;

import java.sql.ResultSet;

public class NFC {

	String idm;
	int oid;
	int uid;
	String note;
	String name;

	public String getIdm() {
		return idm;
	}

	public int getOid() {
		return oid;
	}

	public int getUid() {
		return uid;
	}

	public String getNote() {
		return note;
	}

	public String getName() {
		return name;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public static boolean Entry(String IDm, int oid, int uid, String note,
			int iuid) {
		Transaction tran = Transaction.CreateTran();
		DBCmd icmd = new DBCmd("INSERT INTO "
				+ "nfc(idm, oid, uid, note, idate, iuid) "
				+ "VALUES(?, ?, ?, ?, ?, ?)", tran);

		DBCmd dcmd = new DBCmd(
				"DELETE FROM nfcwait " + "WHERE oid=? AND uid=?", tran);

		try {
			icmd.setStrParam(1, IDm);
			icmd.setIntParam(2, oid);
			icmd.setIntParam(3, uid);
			icmd.setStrParam(4, note);
			icmd.setIntParam(5, DateTime.GetTodayInt());
			icmd.setIntParam(6, iuid);
			if (!icmd.ExcuteNonQuery()) {
				tran.Rollback();
				return false;
			}

			dcmd.setIntParam(1, oid);
			dcmd.setIntParam(2, uid);
			if (!dcmd.ExcuteNonQuery()) {
				tran.Rollback();
				return false;
			}

			return tran.Commit();
		} catch (Exception e) {
			tran.Rollback();
			Log.exceptionWrite(e);
			return false;
		} finally {
			icmd.Close();
		}
	}

	public static NFC GetNFC(int oid, String IDm) {
		DBCmd cmd = new DBCmd(
				"SELECT nfc.oid, nfc.uid, users.name FROM nfc, users "
						+ "WHERE nfc.oid=users.oid AND nfc.uid=users.uid "
						+ "AND nfc.oid=? AND nfc.idm=?");
		try {
			cmd.setIntParam(1, oid);
			cmd.setStrParam(2, IDm);
			ResultSet res = cmd.ExcuteResult();
			if (!res.next())
				return null;

			NFC nfc = new NFC();
			nfc.idm = IDm;
			nfc.oid = oid;
			nfc.uid = res.getInt("uid");
			nfc.name = res.getString("name");
			return nfc;
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		} finally {
			cmd.Close();
		}
	}

}
