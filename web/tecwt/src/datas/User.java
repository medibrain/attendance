package datas;

import java.sql.ResultSet;

public class User {
	private int oid;
	private int uid;
	private String lname;
	private String name;
	private String kana;
	private boolean enable;
	private String pass;
	private boolean admin;
	private String code;

	public int getOid() {
		return oid;
	}

	public int getUid() {
		return uid;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKana() {
		return kana;
	}

	public void setKana(String kana) {
		this.kana = kana;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public static User GetUser(int oid, int uid) {
		DBCmd cmd = new DBCmd("SELECT oid, uid, lname, "
				+ "name, kana, enable, pass, admin, code FROM users "
				+ "WHERE oid=? AND uid=?;");
		try {
			cmd.setIntParam(1, oid);
			cmd.setIntParam(2, uid);

			ResultSet res = cmd.ExcuteResult();
			if (!res.next())
				return null;

			User u = new User();
			u.oid = oid;
			u.uid = uid;
			u.lname = res.getString("lname");
			u.name = res.getString("name");
			u.kana = res.getString("kana");
			u.enable = res.getBoolean("enable");
			u.pass = res.getString("pass");
			u.admin = res.getBoolean("admin");
			u.code = res.getString("code");

			return u;

		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		} finally{
			cmd.Close();
		}
	}

	public static User GetUser(int oid, String lname) {
		DBCmd cmd = new DBCmd("SELECT oid, uid, lname, "
				+ "name, kana, enable, pass, admin, code FROM users "
				+ "WHERE oid=? AND lname=?;");
		try {
			cmd.setIntParam(1, oid);
			cmd.setStrParam(2, lname);

			ResultSet res = cmd.ExcuteResult();
			if (!res.next())
				return null;

			User u = new User();
			u.oid = oid;
			u.uid = res.getInt("uid");
			u.lname = res.getString("lname");
			u.name = res.getString("name");
			u.kana = res.getString("kana");
			u.enable = res.getBoolean("enable");
			u.pass = res.getString("pass");
			u.admin = res.getBoolean("admin");
			u.code = res.getString("code");

			return u;

		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		} finally{
			cmd.Close();
		}
	}

	public String GetJson() {
		StringBuilder sb = new StringBuilder();

		sb.append("{\"oid\":\"" + String.valueOf(oid) + "\",");
		sb.append("\"uid\":\"" + String.valueOf(uid) + "\",");
		sb.append("\"name\":\"" + Common.JsonEscape(name) + "\",");
		sb.append("\"kana\":\"" + Common.JsonEscape(kana) + "\",");
		sb.append("\"enable\":\"" + String.valueOf(enable) + "\",");
		sb.append("\"code\":\"" + Common.JsonEscape(code) + "\"}");

		return sb.toString();
	}



	/**
	 * データベースへ登録します。
	 *
	 * @param inUID
	 *            登録作業を行ったユーザーID
	 * @return
	 */
	public boolean Insert(int inUID) {
		DBCmd cmd = new DBCmd("INSERT INTO users "
				+ "(oid, uid, lname, name, kana, enable, pass, "
				+ "admin, iuid, idate)"
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		try {
			cmd.setIntParam(1, oid);
			cmd.setIntParam(2, uid);
			cmd.setStrParam(3, lname);
			cmd.setStrParam(4, name);
			cmd.setStrParam(5, kana);
			cmd.setBoolParam(6, enable);
			cmd.setStrParam(7, pass);
			cmd.setBoolParam(8, admin);
			cmd.setIntParam(9, inUID);
			cmd.setIntParam(10, DateTime.GetTodayInt());
			return cmd.ExcuteNonQuery();
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		} finally {
			cmd.Close();
		}
	}

	/**
	 * データベースの情報を更新します
	 * @param upUID
	 * @return
	 */
	public boolean Update(int upUID){

		DBCmd cmd = new DBCmd("UPDATE users SET "
				+ "lname=?, name=?, kana=?, enable=?, pass=?, "
				+ "admin=?, uuid=?, udate=? "
				+ "WHERE oid=? AND uid=?,;");
		try {
			cmd.setStrParam(1, lname);
			cmd.setStrParam(2, name);
			cmd.setStrParam(3, kana);
			cmd.setBoolParam(4, enable);
			cmd.setStrParam(5, pass);
			cmd.setBoolParam(6, admin);
			cmd.setIntParam(7, upUID);
			cmd.setIntParam(8, DateTime.GetTodayInt());
			cmd.setIntParam(9, oid);
			cmd.setIntParam(10, uid);

			return cmd.ExcuteNonQuery();
		} catch (Exception e) {
			// TODO: handle exception
			return false;
		} finally {
			cmd.Close();
		}
	}

}
