package datas;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class NFCWait {
	int oid;
	int uid;
	String name;

	public int getOid() {
		return oid;
	}

	public int getUid() {
		return uid;
	}

	public String getName() {
		return name;
	}

	/**
	 *
	 * @param oid
	 * @param uid
	 * @param iuid
	 * @return
	 */
	public static boolean Entry(int oid, int uid, int iuid) {
		DBCmd cmd = new DBCmd("INSERT INTO nfcwait "
				+ "(oid, uid, idate, iuid) " + "VALUES (?, ?, ?, ?)");
		try {
			cmd.setIntParam(1, oid);
			cmd.setIntParam(2, uid);
			cmd.setIntParam(3, iuid);
			cmd.setIntParam(4, DateTime.GetTodayInt());

			return cmd.ExcuteNonQuery();

		} catch (Exception e) {
			Log.exceptionWrite(e);
			return false;
		}
	}

	public static List<NFCWait> GetNFCWaitList(int oid) {
		DBCmd cmd = new DBCmd("SELECT nfcwait.uid, users.name "
				+ "FROM nfcwait, users WHERE nfcwait.oid=? "
				+ "AND nfcwait.uid=users.uid AND nfcwait.uid=users.uid "
				+ "AND users.enable;");
		try {
			cmd.setIntParam(1, oid);
			ResultSet rs = cmd.ExcuteResult();

			List<NFCWait> l = new ArrayList<NFCWait>();
			while (rs.next()) {
				NFCWait nw = new NFCWait();
				nw.oid = oid;
				nw.uid = rs.getInt("uid");
				nw.name = rs.getString("name");

				l.add(nw);
			}
			return l;
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		}finally{
			cmd.Close();
		}
	}

	/**
	 * 指定したNFC登録待機データを削除します
	 * @param oid
	 * @param uid
	 * @return
	 */
	public static boolean Delete(int oid, int uid){
		DBCmd cmd = new DBCmd("DELETE FROM nfcwait "
				+ "WHERE oid=? AND uid=?;");
		try {
			cmd.setIntParam(1, oid);
			cmd.setIntParam(2, uid);
			return cmd.ExcuteNonQuery();
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return false;
		}finally{
			cmd.Close();
		}
	}

	/**
	 * 指定したIDがNFC登録待機データか判断します
	 * @param oid
	 * @param uid
	 * @return
	 */
	public static boolean IsWait(int oid, int uid){
		DBCmd cmd = new DBCmd("SELECT uid FROM nfcwait "
				+ "WHERE oid=? AND uid=?;");
		try {
			cmd.setIntParam(1, oid);
			cmd.setIntParam(2, uid);
			ResultSet rs = cmd.ExcuteResult();
			return rs.next();
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return false;
		}finally{
			cmd.Close();
		}
	}
}
