package datas;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class Transaction {

	Connection conn;
	List<Statement> statementList;

	private Transaction()
	{
		statementList = new ArrayList<Statement>();
	}

	public static Transaction CreateTran() {
		// データベースの指定
		Transaction tran = new Transaction();

		// データベースとの接続
		try {
			//コネクションプールの使用
			tran.conn = DBCmd.source.getConnection();
			tran.conn.setAutoCommit(false);
		} catch (Exception e) {
			return null;
		}

		return tran;
	}

	public void AddStatement(Statement st){
		statementList.add(st);
	}

	public boolean Commit() {
		try {
			conn.commit();
			for (Statement st : statementList) {
				st.close();
			}
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.setAutoCommit(true);
					conn.close();
				} catch (Exception e2) {}
			}
		}
	}

	public boolean Rollback() {
		try {
			conn.rollback();
			for (Statement st : statementList) {
				st.close();
			}
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			if(conn != null) {
				try {
					conn.setAutoCommit(true);
					conn.close();
				} catch (Exception e2) {}
			}
		}
	}

	public Connection getConnection() {
		return this.conn;
	}
}
