package datas;

import java.sql.ResultSet;

public class Unit {
	int oid;
	String unitID;
	String name;
	String pass;
	boolean enable;
	String note;

	public int getOid() {
		return oid;
	}

	public String getUnitID() {
		return unitID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Unit(int oid, String unitid) {
		this.oid = oid;
		this.unitID = unitid;
	}

	/**
	 * 機器登録情報を記録します
	 * @return
	 */
	public boolean Insert() {
		Transaction tran = Transaction.CreateTran();
		DBCmd cmd = new DBCmd("INSERT INTO unit "
				+ "oid, unitid, name, pass, enable, note) "
				+ "VALUES (?, ?, ?, ?, ?, ?);", tran);
		try {
			cmd.setIntParam(1, oid);
			cmd.setStrParam(2, unitID);
			cmd.setStrParam(3, name);
			cmd.setStrParam(4, pass);
			cmd.setBoolParam(5, enable);
			cmd.setStrParam(6, note);
			cmd.ExcuteNonQuery();
		} catch (Exception e) {
			tran.Rollback();
			Log.exceptionWrite(e);
			return false;
		} finally {
			tran.Commit();
			cmd.Close();
		}
		return true;
	}

	public static Unit GetUnit(int oid, String unitID) {
		DBCmd cmd = new DBCmd("SELECT name, pass, enable, note "
				+ "FROM unit "
				+ "WHERE oid=? AND unitid=?;");
		try {
			cmd.setIntParam(1, oid);
			cmd.setStrParam(2, unitID);
			ResultSet rs = cmd.ExcuteResult();

			if (rs == null || !rs.next()) return null;
			Unit unit = new Unit(oid, unitID);
			unit.name = rs.getString("name");
			unit.pass = rs.getString("pass");
			unit.enable = rs.getBoolean("enable");
			unit.note = rs.getString("note");
			return unit;
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		} finally {
			cmd.Close();
		}
	}

	/**
	 * 機器登録スタンバイ状態か確認します
	 * @param oid
	 * @return
	 */
	public static boolean IsStandby(int oid) {
		DBCmd cmd = new DBCmd("SELECT pass FROM unit "
				+ "WHERE oid=? AND unitid='standby'");
		try {
			cmd.setIntParam(1, oid);
			ResultSet rs = cmd.ExcuteResult();
			return rs.next();
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return false;
		} finally {
			cmd.Close();
		}
	}

	/**
	 * 機器登録スタンバイ状態にします
	 * @param oid
	 * @param pass
	 * @return
	 */
	public static boolean EntryStandby(int oid, String pass) {
		Transaction tran = Transaction.CreateTran();
		DBCmd cmd = new DBCmd("INSERT INTO unit "
				+ "oid, unitid, name, pass, enable, note) "
				+ "VALUES (?, ?, ?, ?, ?, ?);", tran);
		try {
			cmd.setIntParam(1, oid);
			cmd.setStrParam(2, "standby");
			cmd.setStrParam(3, "");
			cmd.setStrParam(4, pass);
			cmd.setBoolParam(5, true);
			cmd.setStrParam(6, "");
			cmd.ExcuteNonQuery();
		} catch (Exception e) {
			tran.Rollback();
			Log.exceptionWrite(e);
			return false;
		} finally {
			tran.Commit();
			cmd.Close();
		}
		return true;
	}

	/**
	 * スタンバイ状態を解除します
	 * @param oid
	 * @param pass
	 * @return
	 */
	public static boolean RemoveStandby(int oid) {
		Transaction tran = Transaction.CreateTran();
		DBCmd cmd = new DBCmd("DELETE FROM unit "
				+ "WHERE oid=? AND unitid='standby'", tran);
		try {
			cmd.setIntParam(1, oid);
			cmd.ExcuteNonQuery();
		} catch (Exception e) {
			tran.Rollback();
			Log.exceptionWrite(e);
			return false;
		} finally {
			tran.Commit();
			cmd.Close();
		}
		return true;
	}

	/**
	 * 機器登録スタンバイ状態のとき、機器を登録します
	 * @param oid
	 * @param unitID
	 * @param pass
	 * @param note
	 * @return
	 */
	public static boolean Entry(
			int oid, String unitID, String name, String pass) {

		//すでに登録されている端末IDのときはOKとする
		Unit alreadyUnit = GetUnit(oid, unitID);
		if(alreadyUnit != null) return true;

		Unit u = GetUnit(oid, "standby");
		if (u == null) return false;
		if (!u.pass.equals(pass)) return false;
		u.name = name;
		u.unitID = unitID;

		Transaction tran = Transaction.CreateTran();
		DBCmd cmd = new DBCmd("UPDATE unit SET "
				+ "unitid=?, name=?, pass=?, enable=?, idate=? "
				+ "WHERE oid=? AND unitid=?", tran);
		try {
			cmd.setStrParam(1, u.unitID);
			cmd.setStrParam(2, u.name);
			cmd.setStrParam(3, u.pass);
			cmd.setBoolParam(4, u.enable);
			cmd.setIntParam(5, DateTime.GetTodayInt());
			cmd.setIntParam(6, u.oid);
			cmd.setStrParam(7, "standby");
			return cmd.ExcuteNonQueryCount() == 1;
		} catch (Exception e) {
			tran.Rollback();
			Log.exceptionWrite(e);
			return false;
		} finally {
			tran.Commit();
			cmd.Close();
		}

	}

	public static boolean Remove(int oid, String unitid) {
		Transaction tran = Transaction.CreateTran();
		DBCmd cmd = new DBCmd("DELETE FROM unit "
				+ "WHERE oid=? AND unitid=?", tran);
		try {
			cmd.setIntParam(1, oid);
			cmd.setStrParam(2, unitid);
			return cmd.ExcuteNonQuery();
		} catch (Exception e) {
			tran.Rollback();
			Log.exceptionWrite(e);
			return false;
		} finally {
			tran.Commit();
			cmd.Close();
		}
	}
}
