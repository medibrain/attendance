<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ page import="tecwt_mail.servlet.ControlServlet.OrgStatusHolder" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>勤怠管理-tecwt.com</title>
<meta http-equiv="content-style-type" content="text/css">
<style type="text/css">
<!--
html,body {
	margin: 0;
	height: 100%;
	min-width: 900px;
	text-align: center;
	font-family: 'メイリオ' ,Meiryo, 'ヒラギノ角ゴ Pro W3' , 'Hiragino Kaku Gothic Pro' , 'ＭＳ Ｐゴシック' , 'Osaka' ,sans-serif;
	background-color: lightskyblue;
}
input {
	font-family: 'メイリオ' ,Meiryo, 'ヒラギノ角ゴ Pro W3' , 'Hiragino Kaku Gothic Pro' , 'ＭＳ Ｐゴシック' , 'Osaka' ,sans-serif;
}
#wrap {
	width: 1000px;
	margin-left: auto;
	margin-right: auto;
	background-color: white;
}
#header {
	width: 1000px;
	height: 80px;
	background-color: white;
	border-bottom: 2px cornflowerblue solid;
}
#headerMain_text {
	height: 80px;
	margin-top: 0;
	margin-right: 10px;
	margin-bottom: 0;
	margin-left: 0;
	text-align: right;
}
#inner {
	width: 1000px;
	background-color: white;
	margin-top: 0;
	margin-bottom: 0;
	padding-top: 30px;
	padding-bottom: 60px;
}
#footer {
	width: 1000px;
	background-color: white;
	margin-top: 0;
	margin-bottom: 0;
	padding-top: 5px;
	padding-bottom: 5px;
	border-top: 2px cornflowerblue solid;
}
p {
	margin: 0;
}
#header2:after {
	display: block;
	clear: both;
	content: "";
}
#route {
	margin-left: 10px;
	text-align: left;
	font-size: 12px;
	float: left;
}
#logout {
	margin-right: 10px;
	text-align: right;
	font-size: 12px;
	float: right;
}
#myhome {
	text-align: right;
	margin-right: 10px;
}
#myhome input {
	width: 90px;
	margin-left: 3px;
	margin-right: 3px;
	font-size: 12px;
	padding: 3px 5px;
	background-color: chocolate;
	color: white;
	border-radius: 3px;
	border-style: none;
}
#myhome input:hover {
	background-color: navajowhite;
	color: chocolate;
}
#footerMain p{
	/* コピーライト文字設定 */
	margin-top: 0;
	margin-bottom: 0;
	padding-top: 0;
	padding-bottom: 0;
	padding-right: 0;
	font-size: 1em;
	text-align: center;
}
.container {
	width: 650px;
	margin-left: auto;
	margin-right: auto;
	margin-bottom: 70px;
}
.container h5 {
	width: 600px;
	margin-top: 0;
	margin-bottom: 0;
	margin-left: auto;
	margin-right: auto;
	text-align: left;
}
.container hr {
	width: 600px;
	margin-top: 5px;
	margin-bottom: 20px;
}
.container table {
	width: 600px;
	margin-left: auto;
	margin-right: auto;
}
.container .loginCellLeft {
	width: 150px;
	padding-left: 10px;
	padding-right: 10px;
	text-align: center;
	border: solid 1px #000000;
}
.container .loginCellRight {
	width: auto;
	padding-left: 10px;
	padding-right: 10px;
	text-align: left;
	border: solid 1px #000000;
}
.container .loginInputText {
	width: 275px;
}
.container .seMailCellLeft {
	width: 150px;
	padding-left: 10px;
	padding-right: 10px;
	text-align: center;
	border: solid 1px #000000;
}
.container .seMailCellRight {
	width: auto;
	padding-left: 10px;
	padding-right: 10px;
	text-align: left;
	border: solid 1px #000000;
}
.container .seMailInputText {
	width: 275px;
}
.container .mailWatchCellLeft {
	width: 150px;
	padding-left: 10px;
	padding-right: 10px;
	text-align: center;
	border: solid 1px #000000;
}
.container .mailWatchCellRight {
	width: auto;
	padding-left: 10px;
	padding-right: 10px;
	text-align: left;
	border: solid 1px #000000;
}
.container .mailWatchInputText {
	width: 225px;
}
.container .cellLeft {
	width: 150px;
	padding-left: 10px;
	padding-right: 10px;
	text-align: center;
	border: solid 1px #000000;
}
.container .cellCenter {
	width: 200px;
	padding-left: 10px;
	padding-right: 10px;
	text-align: left;
	border: solid 1px #000000;
}
.container .cellRight {
	width: 40px;
	padding-left: 5px;
	padding-right: 5px;
	text-align: center;
}
.container .submitBtn {
	width: 90px;
	margin-left: 3px;
	margin-right: 3px;
	font-size: 12px;
	padding: 3px 5px;
	background-color: cornflowerblue;
	color: white;
	border-radius: 3px;
	border-style: none;
}
.container .submitBtn:hover {
	margin-top: -1px;
	margin-bottom: -1px;
	background-color: white;
	color: cornflowerblue;
	border-radius: 3px;
	border-style: none;
	border: 1px cornflowerblue solid;
}
.container .submitBtn:disabled {
	margin-top: -1px;
	margin-bottom: -1px;
	background-color: gray;
	color: darkgray;
	border-radius: 3px;
	border-style: none;
	border: 1px gray solid;
}
.container .submitBtnRed {
	width: 90px;
	margin-left: 3px;
	margin-right: 3px;
	font-size: 12px;
	padding: 3px 5px;
	background-color: lightcoral;
	color: white;
	border-radius: 3px;
	border-style: none;
}
.container .submitBtnRed:hover {
	margin-top: -1px;
	margin-bottom: -1px;
	background-color: white;
	color: lightcoral;
	border-radius: 3px;
	border-style: none;
	border: 1px lightcoral solid;
}
.container .submitBtnRed:disabled {
	margin-top: -1px;
	margin-bottom: -1px;
	background-color: gray;
	color: darkgray;
	border-radius: 3px;
	border-style: none;
	border: 1px gray solid;
}
.red {
	color: red;
}
.blue {
	color: blue;
}
.enableTd {
	background-color: white;
}
.disableTd {
	background-color: darkgray;
}

-->
</style>
</head>
<body>
<div id="wrap">
	<div id="header">
		<div id="headerMain">
			<a href="index.html"><img src="image/logo.png" width="302" height="78" border="0" align="left"></a>
			<div id="headerMain_text">
				<font color="#a1a1a1"><small>
				総合勤怠管理システム　-Tecwt.com-　　　提供：株式会社メディブレーン<br>
				〒540-0025　大阪市中央区徳井町2-4-14<br>
				<!-- TEL.06-6944-5997　　FAX.06-6944-7103  --></small>
				</font>
			</div>
		</div>
	</div>
	<div id="header2">
		<div id="route">
			稼働状況
		</div>
	</div>
	<div id="myhome">
		<input type="button" onclick="location.href='login'" value="ログアウト">
	</div>
	<div id="inner">
		<%
		// 例外
		boolean isFuncException = (boolean)request.getAttribute("exceptionExist");
		String exceptions = "なし";
		if(isFuncException) {
			exceptions = (String)request.getAttribute("exceptions");
		}
		// ログイン
		String loginId = (String)request.getAttribute("loginId");
		String loginPass = (String)request.getAttribute("loginPass");
		// システムエラー発生時送信先
		String seMailaddress = (String)request.getAttribute("system-error-mailaddress");
		// 受信監視
		boolean isWatch = (boolean)request.getAttribute("watcher");
		String mailwatchIdleStr = isWatch ? "稼働中" : "停止中";
		String mailwatchIdleBtn	= isWatch ?
				"<input class=\"submitBtnRed\" type=\"submit\" value=\"停止する\">"
				: "<input class=\"submitBtn\" type=\"submit\" value=\"再起動\">";
		String mailwatchIdleBtnValue = isWatch ? "mailwatcher-stop" : "mailwatcher-start";
		// 組織の管理状況
		List<OrgStatusHolder> osHolders = (List<OrgStatusHolder>)request.getAttribute("org-all-status");
		%>
		<!-- 発生例外 -->
		<div class="container">
			<h5>発生中の例外</h5>
			<hr>
			<table>
				<tr>
					<td style="font-size:8px;text-align:left;color:red;"><%= exceptions  %></td>
				</tr>
			</table>
		</div>
		<!-- ログイン情報 -->
		<div class="container">
			<h5>ログイン情報</h5>
			<hr>
			<%
			// 実行結果
			String setLoginPass = (String)request.getAttribute("system-set-login-pass");
			boolean cmdResult = (boolean)request.getAttribute("cmdResult");
			if(setLoginPass != null && !cmdResult) { %>
				<div style="color:red;">実行に失敗しました。パスワードが異なっている可能性があります。</div>
			<%}%>
			<table border="1">
				<tr>
					<td class="loginCellLeft">ID</td>
					<td class="loginCellRight">
						<form action="control" method="post">
							<input type="hidden" name="cmd" value="system-set-login-id">
							<input class="loginInputText" type="text" name="loginId" value="<%= loginId %>">
							<input class="submitBtn" type="submit" value="更新する">
						</form>
					</td>
				</tr>
				<tr>
					<td class="loginCellLeft">パスワード</td>
					<td class="loginCellRight">
						<form action="control" method="post">
							<input type="hidden" name="cmd" value="system-set-login-pass">
							<input class="loginInputText" type="password" name="loginPass" value="<%= loginPass %>"><br>
							<input class="loginInputText" type="password" name="loginPassConfirm" placeholder="再入力">
							<input class="submitBtn" type="submit" value="更新する">
						</form>
					</td>
				</tr>
			</table>
		</div>
		<!-- エラー発生時送信先 -->
		<div class="container">
			<h5>エラー発生時送信先</h5>
			<hr>
			<table border="1">
				<tr>
					<td class="seMailCellLeft">メールアドレス</td>
					<td class="seMailCellRight">
						<form action="control" method="post">
							<input type="hidden" name="cmd" value="system-set-error-mailaddress">
							<input class="seMailInputText" type="text" name="mailaddress" value="<%= seMailaddress %>">
							<input class="submitBtn" type="submit" value="更新する">
						</form>
					</td>
				</tr>
			</table>
		</div>
		<!-- 全メール打刻スレッドの監視 -->
		<div class="container">
			<h5>全メール打刻スレッドの監視</h5>
			<hr>
			<table border="1">
				<tr>
					<td class="cellLeft">監視スレッド</td>
					<td class="cellCenter"><%= mailwatchIdleStr %></td>
					<td class="cellRight">
						<form action="control" method="post">
						<input type="hidden" name="cmd" value="<%= mailwatchIdleBtnValue %>">
						<%= mailwatchIdleBtn %>
						</form>
					</td>
				</tr>
			</table>
		</div>
		<!-- 一括管理 -->
		<div class="container">
			<h5>一括管理</h5>
			<hr>
			<table border="1">
				<tr>
					<td class="cellLeft">組織情報</td>
					<td class="cellRight" colspan="2">
						<form action="control" method="post">
						<input type="hidden" name="cmd" value="all-init">
						<input class="submitBtn" type="submit" value="再読み込み">
						</form>
					</td>
				</tr>
				<tr>
					<td class="cellLeft">全てのメール受信スレッド</td>
					<td class="cellRight">
						<form action="control" method="post">
						<input type="hidden" name="cmd" value="mailreceive-all-start">
						<input class="submitBtn" type="submit" value="再起動">
						</form>
					</td>
					<td class="cellRight">
						<form action="control" method="post">
						<input type="hidden" name="cmd" value="mailreceive-all-stop">
						<input class="submitBtnRed" type="submit" value="全て停止する">
						</form>
					</td>
				</tr>
				<tr>
					<td class="cellLeft">全ての打刻管理スレッド</td>
					<td class="cellRight">
						<form action="control" method="post">
						<input type="hidden" name="cmd" value="timecardwatch-all-start">
						<input class="submitBtn" type="submit" value="再起動">
						</form>
					</td>
					<td class="cellRight">
						<form action="control" method="post">
						<input type="hidden" name="cmd" value="timecardwatch-all-stop">
						<input class="submitBtnRed" type="submit" value="全て停止する">
						</form>
					</td>
				</tr>
				<tr>
					<td class="cellLeft">ログの書き込み</td>
					<td class="cellRight">
						<form action="control" method="post">
						<input type="hidden" name="cmd" value="log-start">
						<input class="submitBtn" type="submit" value="開始">
						</form>
					</td>
					<td class="cellRight">
						<form action="control" method="post">
						<input type="hidden" name="cmd" value="log-stop">
						<input class="submitBtnRed" type="submit" value="終了&保存">
						</form>
					</td>
				</tr>
			</table>
		</div>
		<!-- 各組織状況 -->
		<%
		// 組織
		String cmdOrg = (String)request.getAttribute("cmdOrg");
		for(OrgStatusHolder holder : osHolders) {
			String org = holder.org;
			String oname = holder.oname;
			// mailreceive
			boolean mrEnable = holder.mailreceiveEnable;
			String mrEnableStr = holder.mailreceiveEnable ? "使用できます" : "使用不可";
			String mrEnableBtn = holder.mailreceiveEnable ?
					"<input class=\"submitBtnRed\" type=\"submit\" value=\"無効にする\">"
					: "<input class=\"submitBtn\" type=\"submit\" value=\"有効にする\">";
			String mrEnableValue = holder.mailreceiveEnable ? "false" : "true";
			String mrIdleStr = holder.mailreceiveIdle ? "スレッド：稼働中" : "スレッド：停止中";
			String mrIdleBtn;
			String mrIdleClass;
			if(holder.mailreceiveIdle) {
				if(mrEnable) {
					mrIdleBtn = "<input class=\"submitBtnRed\" type=\"submit\" value=\"停止する\">";
					mrIdleClass = "class=\"enableTd\"";
				} else {
					mrIdleBtn = "<input class=\"submitBtnRed\" type=\"submit\" disabled=\"true\" value=\"停止する\">";
					mrIdleClass = "class=\"disableTd\"";
				}
			} else {
				if(mrEnable) {
					mrIdleBtn = "<input class=\"submitBtn\" type=\"submit\" value=\"起動\">";
					mrIdleClass = "class=\"enableTd\"";
				} else {
					mrIdleBtn = "<input class=\"submitBtn\" type=\"submit\" disabled=\"true\" value=\"起動\">";
					mrIdleClass = "class=\"disableTd\"";
				}
			}
			String mrIdleBtnValue = holder.mailreceiveIdle ? "mailreceive-stop" : "mailreceive-start";
			// timecardwatch
			boolean twEnable = holder.timecardwatchEnable;
			String twEnableStr = holder.timecardwatchEnable ? "使用できます" : "使用不可";
			String twEnableBtn = holder.timecardwatchEnable ?
					"<input class=\"submitBtnRed\" type=\"submit\" value=\"無効にする\">"
					: "<input class=\"submitBtn\" type=\"submit\" value=\"有効にする\">";
			String twEnableValue = holder.timecardwatchEnable ? "false" : "true";
			String twIdleBtn;
			String twIdleClass;
			String twIdleStr;
			String twIdleBtnValue;
			if(holder.timecardwatchIdle) {
				twIdleStr = "スレッド：稼働中";
				twIdleBtnValue = "timecardwatch-stop";
				if(twEnable) {
					twIdleBtn = "<input class=\"submitBtnRed\" type=\"submit\" value=\"停止する\">";
					twIdleClass = "class=\"enableTd\"";
				} else {
					twIdleBtn = "<input class=\"submitBtnRed\" type=\"submit\" disabled=\"true\" value=\"停止する\">";
					twIdleClass = "class=\"disableTd\"";
				}
			} else {
				twIdleStr = "スレッド：停止中";
				twIdleBtnValue = "timecardwatch-start";
				if(twEnable) {
					twIdleBtn = "<input class=\"submitBtn\" type=\"submit\" value=\"起動\">";
					twIdleClass = "class=\"enableTd\"";
				} else {
					twIdleBtn = "<input class=\"submitBtn\" type=\"submit\" disabled=\"true\" value=\"起動\">";
					twIdleClass = "class=\"disableTd\"";
				}
			}
			%>
			<div class="container">
				<h5><%= holder.oname %></h5>
				<hr>
				<% if(!cmdResult && cmdOrg != null && cmdOrg.equals(org)) { %>
					<div style="color:red;">実行に失敗しました</div>
				<%}%>
				<table border="1">
				<tr>
					<td rowspan="2">メール打刻機能</td>
					<td><%= mrEnableStr %></td>
					<td>
						<form action="control" method="post">
							<input type="hidden" name="org" value="<%= org %>">
							<input type="hidden" name="enable" value="<%= mrEnableValue %>">
							<input type="hidden" name="cmd" value="mailreceive-set-enable">
							<%= mrEnableBtn %>
						</form>
					</td>
				</tr>
				<tr>
					<td <%= mrIdleClass %>><%= mrIdleStr %></td>
					<td <%= mrIdleClass %>>
						<form action="control" method="post">
							<input type="hidden" name="org" value="<%= org %>">
							<input type="hidden" name="cmd" value="<%= mrIdleBtnValue %>">
							<%= mrIdleBtn %>
						</form>
					</td>
				</tr>
				<tr>
					<td rowspan="2">打刻管理機能</td>
					<td><%= twEnableStr %></td>
					<td>
						<form action="control" method="post">
							<input type="hidden" name="org" value="<%= org %>">
							<input type="hidden" name="enable" value="<%= twEnableValue %>">
							<input type="hidden" name="cmd" value="timecardwatch-set-enable">
							<%= twEnableBtn %>
						</form>
					</td>
				</tr>
				<tr>
					<td <%= twIdleClass %>><%= twIdleStr %></td>
					<td <%= twIdleClass %>>
						<form action="control" method="post">
							<input type="hidden" name="org" value="<%= org %>">
							<input type="hidden" name="cmd" value="<%= twIdleBtnValue %>">
							<%= twIdleBtn %>
						</form>
					</td>
				</tr>
				<tr>
					<td colspan="3" style="background-color:royalblue;color:white;font-size:12px;">打刻管理スケジュール</td>
				</tr>
				<%
				String id;
				String updateEnable;
				String enableText;
				String enableSubmitBtn;
				String enableSubmitBtnValue;
				Map<String, Boolean> enables = holder.timecardwatchScheduleEnables;
				for(Map.Entry<String, String[]> e : holder.timecardwatchSchedules.entrySet()) {
					id = e.getKey();
					if(twEnable) {
						updateEnable = "";
					} else {
						updateEnable = "disabled=\"true\"";
					}
					if(enables.get(id)) {
						enableText = "";
						if(twEnable) {
							enableSubmitBtn = "<input class=\"submitBtnRed\" type=\"submit\" value=\"使用しない\">";
							enableSubmitBtnValue = "<input type=\"hidden\" name=\"enable\" value=\"false\">";
						} else {
							enableSubmitBtn = "<input class=\"submitBtnRed\" type=\"submit\" disabled=\"true\" value=\"使用しない\">";
							enableSubmitBtnValue = "<input type=\"hidden\" name=\"enable\" value=\"false\">";
						}
					} else {
						enableText = "<span style=\"color:red;font-size:12px;\">(未使用)</span>";
						if(twEnable) {
							enableSubmitBtn = "<input class=\"submitBtn\" type=\"submit\" value=\"使用する\">";
							enableSubmitBtnValue = "<input type=\"hidden\" name=\"enable\" value=\"true\">";
						} else {
							enableSubmitBtn = "<input class=\"submitBtn\" type=\"submit\" disabled=\"true\" value=\"使用する\">";
							enableSubmitBtnValue = "<input type=\"hidden\" name=\"enable\" value=\"true\">";
						}
					}
				%>
					<tr>
						<td colspan="3"<%= twIdleClass %>>
							<table>
								<tr>
									<td colspan="2" style="text-align:left">
										●<u style="font-size:14px;text-align:left;padding-bottom:5px;"><%= id %></u>　<%= enableText %>
									</td>
								</tr>
								<tr>
									<td>
										<form action="control" method="post">
											<input type="hidden" name="org" value="<%= org %>">
											<input type="hidden" name="cmd" value="timecardwatch-set-schedule">
											<input type="hidden" name="id" value="<%= id %>">
											<select name="minute" <%= updateEnable %>>
											<% if(e.getValue()[0].equals("*")) { %>
												<option value="*" selected="selected">*</option>
											<%} else {%>
												<option value="*">*</option>
											<%}%>
											<% for(int i=0; i<=59; i++) { %>
												<% if(e.getValue()[0].equals(String.valueOf(i))) { %>
													<option value="<%= i %>" selected="selected"><%= i %></option>
												<%} else {%>
													<option value="<%= i %>"><%= i %></option>
												<%}%>
											<%}%>
											</select>分
											<select name="hour" <%= updateEnable %>>
											<% if(e.getValue()[1].equals("*")) { %>
												<option value="*" selected="selected">*</option>
											<%} else {%>
												<option value="*">*</option>
											<%}%>
											<% for(int i=0; i<=23; i++) { %>
												<% if(e.getValue()[1].equals(String.valueOf(i))) { %>
													<option value="<%= i %>" selected="selected"><%= i %></option>
												<%} else {%>
													<option value="<%= i %>"><%= i %></option>
												<%}%>
											<%}%>
											</select>時
											<select name="day" <%= updateEnable %>>
											<% if(e.getValue()[2].equals("*")) { %>
												<option value="*" selected="selected">*</option>
											<%} else {%>
												<option value="*">*</option>
											<%}%>
											<% for(int i=1; i<=31; i++) { %>
												<% if(e.getValue()[2].equals(String.valueOf(i))) { %>
													<option value="<%= i %>" selected="selected"><%= i %></option>
												<%} else {%>
													<option value="<%= i %>"><%= i %></option>
												<%}%>
											<%}%>
											</select>日
											<select name="month" <%= updateEnable %>>
											<% if(e.getValue()[3].equals("*")) { %>
												<option value="*" selected="selected">*</option>
											<%} else {%>
												<option value="*">*</option>
											<%}%>
											<% for(int i=1; i<=12; i++) { %>
												<% if(e.getValue()[3].equals(String.valueOf(i))) { %>
													<option value="<%= i %>" selected="selected"><%= i %></option>
												<%} else {%>
													<option value="<%= i %>"><%= i %></option>
												<%}%>
											<%}%>
											</select>月
											<select name="dayOfWeek" <%= updateEnable %>>
											<% if(e.getValue()[4].equals("*")) { %>
												<option value="*" selected="selected">*</option>
											<%} else {%>
												<option value="*">*</option>
											<%}%>
											<% for(int i=0; i<=6; i++) { %>
												<% if(e.getValue()[4].equals(String.valueOf(i))) { %>
													<option value="<%= i %>" selected="selected"><%= i %></option>
												<%} else {%>
													<option value="<%= i %>"><%= i %></option>
												<%}%>
											<%}%>
											</select>曜日　
											<input class="submitBtn" type="submit" <%= updateEnable %> value="更新する">
										</form>
									</td>
									<td>
										<form action="control" method="post">
											<input type="hidden" name="org" value="<%= org %>">
											<input type="hidden" name="cmd" value="timecardwatch-set-schedule-enable">
											<input type="hidden" name="id" value="<%= id %>">
											<%= enableSubmitBtnValue %>
											<%= enableSubmitBtn %>
										</form>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				<%}%>
				</table>
			</div>
		<%}%>
	</div>
	<!-- TODO: medi-kintai  -->
	<!-- <div id="footer">
		<div id="footerMain">
			<p><font color="#ffffff"><small><a href="http://www.medi-ts.com/index.html">トップページ</a>｜
			<a href="http://www.medi-ts.com/corporateprofile.html">会社概要</a>｜
			<a href="http://www.medi-ts.com/WorkContent.html">業務内容</a>｜
			<a href="http://www.medi-ts.com/Privacypolicy.html">プライバシーポリシー</a>｜
			<a href="http://www.medi-ts.com/access.html">アクセス</a>｜
			<a href="http://www.medi-ts.com/Inquiry.html">お問い合わせ</a>｜ <a href="Site map.html">サイトマップ</a></small></font><br>
			copyright&copy;2013-2016 medi-ts all&nbsp;rights&nbsp;reserved.</p>
		</div>
	</div> -->
</div>
</body>
</html>