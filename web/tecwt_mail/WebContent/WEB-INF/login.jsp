<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>勤怠管理-tecwt.com</title>
<meta http-equiv="content-style-type" content="text/css">
<style type="text/css">
<!--
html,body {
	margin: 0;
	height: 100%;
	min-width: 900px;
	text-align: center;
	font-family: 'メイリオ' ,Meiryo, 'ヒラギノ角ゴ Pro W3' , 'Hiragino Kaku Gothic Pro' , 'ＭＳ Ｐゴシック' , 'Osaka' ,sans-serif;
	background-color: lightskyblue;
}
input {
	font-family: 'メイリオ' ,Meiryo, 'ヒラギノ角ゴ Pro W3' , 'Hiragino Kaku Gothic Pro' , 'ＭＳ Ｐゴシック' , 'Osaka' ,sans-serif;
}
#wrap {
	width: 1000px;
	margin-left: auto;
	margin-right: auto;
	background-color: white;
}
#header {
	width: 1000px;
	height: 80px;
	background-color: white;
	border-bottom: 2px cornflowerblue solid;
}
#headerMain_text {
	height: 80px;
	margin-top: 0;
	margin-right: 10px;
	margin-bottom: 0;
	margin-left: 0;
	text-align: right;
}
#inner {
	width: 1000px;
	background-color: white;
	margin-top: 0;
	margin-bottom: 0;
	padding-top: 30px;
	padding-bottom: 60px;
}
#footer {
	width: 1000px;
	background-color: white;
	margin-top: 0;
	margin-bottom: 0;
	padding-top: 5px;
	padding-bottom: 5px;
	border-top: 2px cornflowerblue solid;
}
p {
	margin: 0;
}
#footerMain p{
	/* コピーライト文字設定 */
	margin-top: 0;
	margin-bottom: 0;
	padding-top: 0;
	padding-bottom: 0;
	padding-right: 0;
	font-size: 1em;
	text-align: center;
}
#login {
	width: 400px;
	margin-left: auto;
	margin-right: auto;
}
#login h5 {
	width: 400px;
	margin-top: 0;
	margin-bottom: 0;
	margin-left: auto;
	margin-right: auto;
	text-align: left;
}
#login hr {
	width: 400px;
	margin-top: 5px;
	margin-bottom: 20px;
}
#login form {
	width: 400px;
}
#login table {
	margin-left: auto;
	margin-right: auto;
	text-align: center;
}
#login .loginBtn {
	padding-top: 20px;
}
#login .message {
	color: red;
}

-->
</style>
</head>
<body>
<div id="wrap">
	<div id="header">
		<div id="headerMain">
			<a href="index.html"><img src="image/logo.png" width="302" height="78" border="0" align="left"></a>
			<div id="headerMain_text">
				<font color="#a1a1a1"><small>
				総合勤怠管理システム　-Tecwt.com-　　　提供：株式会社メディブレーン<br>
				〒540-0025　大阪市中央区徳井町2-4-14<br>
				<!-- TEL.06-6944-5997　　FAX.06-6944-7103  --></small>
				</font>
			</div>
		</div>
	</div>
	<div id="inner">
		<div id="login">
			<h5>ログイン</h5>
			<hr>
			<%
			Object attr = request.getAttribute("error");
			boolean error = false;
			if (attr != null) error = (boolean)attr;
			if (error) %><div class="message">IDもしくはパスワードが違います</div>
			<form action="login" method="post">
				<table>
					<tr>
						<td>ログインID</td>
						<td><input type="text" name="id" value="" size="24"></td>
					</tr>
					<tr>
						<td>パスワード</td>
						<td><input type="password" name="pass" value="" size="24"></td>
					</tr>
					<tr>
						<td class="loginBtn" colspan="2"><input type="submit" value="ログイン"></td>
					</tr>
				</table>
			</form>
		</div>
	</div>
	<!-- TODO: medi-kintai  -->
	<!-- <div id="footer">
		<div id="footerMain">
			<p><font color="#ffffff"><small><a href="http://www.medi-ts.com/index.html">トップページ</a>｜
			<a href="http://www.medi-ts.com/corporateprofile.html">会社概要</a>｜
			<a href="http://www.medi-ts.com/WorkContent.html">業務内容</a>｜
			<a href="http://www.medi-ts.com/Privacypolicy.html">プライバシーポリシー</a>｜
			<a href="http://www.medi-ts.com/access.html">アクセス</a>｜
			<a href="http://www.medi-ts.com/Inquiry.html">お問い合わせ</a>｜ <a href="Site map.html">サイトマップ</a></small></font><br>
			copyright&copy;2013-2016 medi-ts all&nbsp;rights&nbsp;reserved.</p>
		</div>
	</div>  -->
</div>
</body>
</html>