package tecwt_mail.mail;

import it.sauronsoftware.cron4j.Scheduler;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javax.mail.MessagingException;

import tecwt_mail.util.Util;



public class MailWatcher {

	private Map<String, Long> watchAddresses;//受信監視一覧
	private Map<String, Boolean> receiveStatusAddresses;//各監視対象の受信状況一覧(true:受信済)
	private Scheduler scheduler;

	public MailWatcher(Map<String, Long> watchAddresses) {
		this.watchAddresses = watchAddresses;
		this.receiveStatusAddresses = new HashMap<String, Boolean>();
		for(Map.Entry<String, Long> e : watchAddresses.entrySet()) {
			this.receiveStatusAddresses.put(e.getKey(), false);
		}
		this.scheduler = new Scheduler();
		this.scheduler.schedule("0 4 * * *", restartRun);
		this.scheduler.schedule("0 8 * * *", checkRun1);
		this.scheduler.schedule("0 11 * * *", checkRun1);
		this.scheduler.schedule("0 16 * * *", checkRun1);
		this.scheduler.schedule("0 20 * * *", checkRun1);
	}

	private Runnable restartRun = new Runnable() {
		public void run() {
			String mailaddress;
			String receiverKey;
			for(Map.Entry<String, Long> e : watchAddresses.entrySet()) {
				mailaddress = e.getKey();
				receiverKey = MailManager.getKey(mailaddress);
				//再起動
				MailManager.restartMailReceiver(receiverKey);
				//ログ
				MailManager.getLog(receiverKey).write(
						getLogTitle("MailReceiver restart.", receiverKey),
						"This is a regular restart.",
						true);
			}
		}
	};

	private Runnable checkRun1 = new Runnable() {
		public void run() {
			//全監視対象の状態を初期化
			resetReceiveStatus();
			//全監視対象にテストメールを送信
			sendCheckMails1();
			//5分後に受信確認
			Timer timer1 = new Timer();
			timer1.schedule(checkRun2, TimeUnit.MINUTES.toMillis(5));
		}
	};

	private TimerTask checkRun2 = new TimerTask() {
		public void run() {
			//全監視対象の状態を確認＆再度テストメール送信
			checkReceiveStatus1();
			//5分後に再度確認
			Timer timer1 = new Timer();
			timer1.schedule(checkRun3, TimeUnit.MINUTES.toMillis(5));
		}
	};

	private TimerTask checkRun3 = new TimerTask() {
		public void run() {
			//全監視対象の状態を最終確認（受信していなければ再起動する）
			checkReceiveStatus2();
		}
	};

	private void resetReceiveStatus() {
		for(Map.Entry<String, Long> e : watchAddresses.entrySet()) {
			receiveStatusAddresses.put(e.getKey(), false);
		}
	}

	private void sendCheckMails1() {
		for(Map.Entry<String, Long> e : watchAddresses.entrySet()) {
			// 送信（別スレッド）
			sendCheckMail(e.getKey());
		}
	}

	private void checkReceiveStatus1() {
		String mailaddress;
		for(Map.Entry<String, Long> e : watchAddresses.entrySet()) {
			mailaddress = e.getKey();
			if(!receiveStatusAddresses.get(mailaddress)) {
				sendCheckMail(mailaddress);
			}
		}
	}

	private void checkReceiveStatus2() {
		String mailaddress;
		String receiverKey;
		for(Map.Entry<String, Long> e : watchAddresses.entrySet()) {
			mailaddress = e.getKey();
			receiverKey = MailManager.getKey(mailaddress);
			if(!receiveStatusAddresses.get(e.getKey())) {
				//再起動
				MailManager.restartMailReceiver(receiverKey);
				//メールで知らせる
				sendSystemErrorMail(
						"[tecwt_mail]["+MailManager.getOrgName(receiverKey)+"] MailReceiver restart.",
						"Because mail receive stoped.");
				//ログ
				MailManager.getLog(receiverKey).write(
						getLogTitle("MailReceiver restart.", receiverKey),
						"Because mail receive stoped.",
						true);
			}
		}
	}

	private void sendCheckMail(final String mailaddress) {
		final String key = MailManager.getKey(mailaddress);
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					sendMail(mailaddress, MailInfo.WATCHER_TITLE, "["+Util.GetNowDateTimeFull()+"] "+MailInfo.WATCHER_PART_TEXT);
					MailManager.getLog(key).write(getLogTitle("MailWatcher send.", key), null, true);
				} catch (Exception ex) {
					MailManager.getLog(key).write(getLogTitle("MailWatcher send failed.", key), ex);
				}
			}
		}).start();
	}

	private String getLogTitle(String title, String receiverKey) {
		return "<"+receiverKey+"><"+this.getClass().getSimpleName()+"> "+title;
	}

	/**
	 * テストメールを受信したことを記録します。
	 * @param mailaddress
	 */
	public void setReceived(String mailaddress) {
		receiveStatusAddresses.put(mailaddress, true);
	}

	/**
	 * 監視スレッドの開始
	 * @return
	 */
	public synchronized boolean start() {
		try {
			if(!scheduler.isStarted()) {
				scheduler.start();
			}
		} catch(IllegalStateException ex) {
			System.out.println("["+Util.GetNowDateTimeFull()+"] <tecwt_mail><MailWatcher> start failed.");
			ex.printStackTrace();
		}
		return isIdle();
	}

	/**
	 * 監視スレッドの停止
	 * @return
	 */
	public synchronized boolean end() {
		if(scheduler.isStarted()) {
			scheduler.stop();
		}
		return !isIdle();
	}

	/**
	 * 稼働中の場合はtrueを返します。
	 * @return
	 */
	public synchronized boolean isIdle() {
		return scheduler.isStarted();
	}

	/**
	 * 開発者にシステムエラーメールを送信します。
	 * @param subject
	 * @param text
	 * @return
	 */
	private void sendSystemErrorMail(String subject, String text) {
		try {
			sendMail(MailManager.getSystemErrorMailaddress(), subject, text);
		} catch (Exception ex) {
			System.out.println("["+Util.GetNowDateTimeFull()+"] <tecwt_mail><MailWatcher> send system error mail failed.");
			ex.printStackTrace();
		}
	}

	/**
	 * メールを送信します。
	 * @param to
	 * @param subject
	 * @param text
	 * @return
	 * @throws MessagingException
	 */
	private Mail sendMail(String to, String subject, String text) throws MessagingException {
		return Mail.send(
				MailInfo.WATCHER_SEND_HOST,
				String.valueOf(MailInfo.WATCHER_SEND_PORT),
				MailInfo.WATCHER_USER,
				MailInfo.WATCHER_PASSWORD,
				MailInfo.WATCHER_ADDRESS,
				to,
				subject,
				text
		);
	}


}
