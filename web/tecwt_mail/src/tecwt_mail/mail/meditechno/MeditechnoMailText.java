package tecwt_mail.mail.meditechno;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tecwt_mail.db.Result;
import tecwt_mail.db.Timecard;
import tecwt_mail.db.Worktype;
import tecwt_mail.util.Util;

public class MeditechnoMailText {

	@SuppressWarnings("unchecked")
	public static String GetSendText(int oid, int uid, int tdate, String syuttai, String uname, String oname) throws Exception {

		TimecardsData tcsd = GetTimecardsData(oid, uid, tdate);

		int tcSIZE = tcsd.tcs.size();
		Timecard tcFIRST = tcsd.tcs.get(0);
		Timecard tcLAST = tcsd.tcs.get(tcSIZE - 1);

		// 冒頭
		StringBuilder header = new StringBuilder();
		header.append(syuttai).append("メールは正常に受け付けられました。");
		header.append(tcsd.isWorkComplete ? "本日の勤怠":"受け付けられた");
		header.append("情報は以下の通りです。間違い等がある場合は再度メールを送信して下さい。\r\n\r\n");

		// 冒頭メッセージ
		StringBuilder headerMessage = new StringBuilder();
		if(!tcsd.isWorkComplete) {
			if(tcsd.isOuted) {
				// - - 退
				headerMessage.append("【注意】出勤がまだ報告されていません！\r\n\r\n");
			} else if(!tcsd.isIned) {
				// - 移 -
				headerMessage.append("【注意】出勤がまだ報告されていません！\r\n\r\n");
			} else {
				// 出 - -
				/*
				 * メッセージなし
				 */
			}
		}

		// 氏名：
		StringBuilder name = new StringBuilder();
		name.append("氏名: ").append(uname).append("\r\n");

		// 日付：
		StringBuilder date = new StringBuilder();
		date.append("日付: ").append(Util.GetDateToString(tdate)).append("\r\n");

		// 労働時間：
		StringBuilder worktime = new StringBuilder();
		if(tcsd.isWorkComplete) {
			worktime.append("労働時間: ");
			int inT = tcFIRST.getIntime();
			int inH = inT / 100;
			int inM = inT % 100;
			int outT = tcLAST.getOuttime();
			int outH = outT / 100;
			int outM = outT % 100;
			int breakT = tcFIRST.getBreaktime();

			Calendar baseC = Calendar.getInstance();
			baseC.set(Calendar.HOUR_OF_DAY, outH);
			baseC.set(Calendar.MINUTE, outM);
			baseC.set(Calendar.SECOND, 0);

			baseC.add(Calendar.HOUR_OF_DAY, -inH);	// 勤務時間=退勤ー出勤
			baseC.add(Calendar.MINUTE, -inM);

			baseC.add(Calendar.MINUTE, -breakT);	// 実働時間=勤務時間ー休憩時間

			int h = baseC.get(Calendar.HOUR_OF_DAY);
			int m = baseC.get(Calendar.MINUTE);
			if(h < 10) worktime.append("0");
			worktime.append(h).append(":");
			if(m < 10) worktime.append("0");
			worktime.append(m).append("\r\n");
		}
		worktime.append("\r\n");

		// 出勤時刻：
		StringBuilder intime = new StringBuilder();
		if(tcsd.isIned) {
			String time = Util.GetTimeToString(tcFIRST.getIntime());
			intime.append("出勤時刻: ").append(time).append("\r\n");
		}

		// 移動時刻：
		StringBuilder movetime = new StringBuilder();
		if(tcsd.isMoved) {
			int moveCount = 1;
			int moveStartIndex = 1;
			for(int i=moveStartIndex; i<tcSIZE; i++) {
				movetime.append("移動時刻").append("(").append(moveCount).append(")").append(": ");
				movetime.append(Util.GetTimeToString(tcsd.tcs.get(moveStartIndex).getIntime())).append("\r\n");
				moveCount++;
			}
		}

		// 退勤時刻：
		StringBuilder outtime = new StringBuilder();
		if(tcsd.isOuted) {
			String time = Util.GetTimeToString(tcLAST.getOuttime());
			outtime.append("退勤時刻: ").append(time).append("\r\n");
		}

		// 休憩時間：
		StringBuilder breaktime = new StringBuilder();
		if(tcsd.isOuted) {
			breaktime.append("休憩時間: ").append(tcFIRST.getBreaktime()).append("分\r\n");
		}

		// 交通費：
		StringBuilder fare = new StringBuilder();
		if(tcsd.isOuted) {
			fare.append("交通費: ").append(tcFIRST.getFare()).append("円\r\n");
		}

		Map<Integer, String> wMap = new HashMap<Integer, String>();
		Result r = Worktype.FindOfOid(oid);
		if(r.isSuccess()) {
			List<Worktype> list = (List<Worktype>)r.getList();
			for(Worktype w : list) {
				wMap.put(w.getWid(), w.getName());
			}
		} else {
			throw new Exception(r.getMessage());
		}

		// 勤務種別：
		// 業務内容：
		// 処理枚数：
		// 申出枚数：
		StringBuilder worktypeNoteWcountRcount = new StringBuilder();
		if(tcsd.isMoved || tcsd.isOuted) {
			int moveCount = 1;
			for(int i=0; i<tcSIZE; i++) {
				worktypeNoteWcountRcount.append("--------\r\n");
				worktypeNoteWcountRcount.append(GetContextText(tcsd.tcs.get(i), wMap, tcsd.isMoved, moveCount));
				moveCount++;
			}
		}

		// 末尾メッセージ
		StringBuilder footerMessage = new StringBuilder();
		footerMessage.append("\r\n");
		if(tcsd.isWorkComplete) {
			footerMessage.append("お仕事お疲れ様でした。\r\n");
		} else {
			if(tcsd.isMoved) {
				footerMessage.append("引き続きよろしくお願いいたします。\r\n");
			} else if(tcsd.isIned) {
				footerMessage.append("本日もよろしくお願いいたします。\r\n");
			} else {
				footerMessage.append("お仕事お疲れ様でした。\r\n");
			}
		}

		// 日付
		StringBuilder footerDateTime = new StringBuilder();
		footerDateTime.append(Util.GetNowDateTime()).append("\r\n");

		// 社名
		StringBuilder footerOrganization = new StringBuilder();
		footerOrganization.append(oname).append("\r\n");

		StringBuilder result = new StringBuilder();
		result.append(header.toString());
		result.append(headerMessage.toString());
		result.append(name.toString());
		result.append(date.toString());
		result.append(worktime.toString());
		result.append(intime.toString());
		result.append(movetime.toString());
		result.append(outtime.toString());
		result.append(breaktime.toString());
		result.append(fare.toString());
		result.append(worktypeNoteWcountRcount.toString());
		result.append(footerMessage.toString());
		result.append(footerDateTime.toString());
		result.append(footerOrganization.toString());

		return result.toString();
	}

	/**
	 * true=完成,false=未完成
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private static TimecardsData GetTimecardsData(int oid, int uid, int tdate) throws Exception {
		TimecardsData tcsd = new TimecardsData();

		List<Timecard> tcs;
		Result r = Timecard.FindOfTdate(oid, uid, tdate);
		if(r.isSuccess()) {
			tcs = (List<Timecard>)r.getList();
			tcsd.tcs = tcs;
		} else {
			throw new Exception(r.getMessage());
		}

		int timesSize = tcs.size();
		int intime;
		int outtime;
		if(timesSize == 1) {
			// １レコード
			intime = tcs.get(0).getIntime();
			outtime = tcs.get(0).getOuttime();
			if(intime != -1) {
				tcsd.isIned = true;
			}
			if(outtime != -1) {
				tcsd.isOuted = true;
			}
		} else {
			// 複数レコード
			intime = tcs.get(0).getIntime();
			outtime = tcs.get(timesSize - 1).getOuttime();
			tcsd.isMoved = true;
			if(intime != -1) {
				tcsd.isIned = true;
			}
			if(outtime != -1) {
				tcsd.isOuted = true;
			}
		}
		if(intime != -1 && outtime != -1) {
			tcsd.isWorkComplete = true;
		}
		return tcsd;
	}

	private static String GetContextText(Timecard tc, Map<Integer, String> wMap, boolean isMoved, int moveCount) {
		StringBuilder sb = new StringBuilder();

		// 勤務種別
		int wid = tc.getWid();
		sb.append("勤務種別");
		if(isMoved) sb.append("(").append(moveCount).append(")");
		sb.append(": ");
		sb.append(wMap.getOrDefault(wid, "----")).append("\r\n");

		// 業務内容
		String note = tc.getNote();
		sb.append("業務内容");
		if(isMoved) sb.append("(").append(moveCount).append(")");
		sb.append(": ");
		sb.append(note).append("\r\n");

		// 処理枚数
		int wcount = tc.getWcount();
		sb.append("処理枚数");
		if(isMoved) sb.append("(").append(moveCount).append(")");
		sb.append(": ");
		sb.append(wcount).append("\r\n");

		// 申出枚数
		int rcount = tc.getRcount();
		sb.append("申出枚数");
		if(isMoved) sb.append("(").append(moveCount).append(")");
		sb.append(": ");
		sb.append(rcount).append("\r\n");

		return sb.toString();
	}

	private static class TimecardsData {
		public boolean isWorkComplete;
		public boolean isIned;
		public boolean isMoved;
		public boolean isOuted;
		public List<Timecard> tcs;
	}

}
