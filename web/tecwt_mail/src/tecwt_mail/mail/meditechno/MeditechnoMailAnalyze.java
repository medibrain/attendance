package tecwt_mail.mail.meditechno;

import java.util.ArrayList;
import java.util.List;

import tecwt_mail.db.Result;
import tecwt_mail.db.Timecard;
import tecwt_mail.mail.MailTimecard;
import tecwt_mail.util.Util;

public class MeditechnoMailAnalyze extends MailTimecard {

	private String uidName;
	private String tmpPassward;
	private String tmpTime;
	private String tmpWorktype;
	private String tmpNote;
	private String tmpWcount;
	private String tmpRcount;
	private String tmpBreaktime;
	private String tmpFare;


	/******************
	 * コンストラクタ *
	 *****************/

	/**
	 * SELECT用
	 * @param oid
	 */
	public MeditechnoMailAnalyze(int oid) {
		super(oid);
	}

	/**
	 * 出勤用
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @param intime
	 * @param uidName
	 */
	public MeditechnoMailAnalyze(int oid, int uid, int tdate, String intime, String uidName) {
		this(oid);
		this.oid = oid;
		this.uid = uid;
		this.tdate = tdate;
		this.tmpTime = intime;
		this.uidName = uidName;
		// timecard default
		this.intime = -1;
		this.outtime = -1;
		this.note = "";
		// tclog default
		this.logtype = "メール 出勤報告";
		this.latitude = "0.0";
		this.longitude = "0.0";
		this.unitid = "不明";
	}

	/**
	 * 退勤・移動用
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @param outORmoveTime
	 * @param worktype
	 * @param note
	 * @param breaktime
	 * @param fare
	 * @param wcount
	 * @param rcount
	 * @param uidName
	 * @param syuttai
	 */
	public MeditechnoMailAnalyze(int oid, int uid, int tdate, String outORmoveTime,
			String worktype, String note, String breaktime, String fare, String wcount, String rcount, String uidName, int syuttai) {
		this(oid);
		this.oid = oid;
		this.uid = uid;
		this.tdate = tdate;
		this.tmpTime = outORmoveTime;
		this.tmpBreaktime = breaktime;
		this.tmpFare = fare;
		this.tmpWorktype = worktype;
		this.tmpNote = note;
		this.tmpWcount = wcount;
		this.tmpRcount = rcount;
		this.uidName = uidName;
		// default
		this.intime = -1;
		this.outtime = -1;
		this.note = "";
		// tclog default
		switch(syuttai) {
		case TAIKIN:
			this.logtype = "メール 退勤報告";
			break;
		case IDOU:
			this.logtype = "メール 移動報告";
			break;
		}
		this.latitude = "0.0";
		this.longitude = "0.0";
		this.unitid = "不明";
	}

	/**
	 * 退勤(残業承認パスワードあり)用
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @param passward
	 * @param outTime
	 * @param worktype
	 * @param note
	 * @param breaktime
	 * @param fare
	 * @param wcount
	 * @param rcount
	 * @param uidName
	 * @param syuttai
	 */
	public MeditechnoMailAnalyze(int oid, int uid, int tdate, String passward, String outtime,
			String worktype, String note, String breaktime, String fare, String wcount, String rcount, String uidName, int syuttai) {
		this(oid);
		this.oid = oid;
		this.uid = uid;
		this.tdate = tdate;
		this.tmpPassward = passward;
		this.tmpTime = outtime;
		this.tmpBreaktime = breaktime;
		this.tmpFare = fare;
		this.tmpWorktype = worktype;
		this.tmpNote = note;
		this.tmpWcount = wcount;
		this.tmpRcount = rcount;
		this.uidName = uidName;
		// default
		this.intime = -1;
		this.outtime = -1;
		this.note = "";
		// tclog default
		this.logtype = "メール 退勤報告";
		this.latitude = "0.0";
		this.longitude = "0.0";
		this.unitid = "不明";
	}

	/*******
	 * SQL *
	 ******/


	/******************
	 * 出退勤メソッド *
	 *****************/

	/**
	 * 出勤報告をします。
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public TimecardResult doSyukkin() {
		TimecardResult result = new TimecardResult();
		result.isError = true;

		// timesの設定
		Result timesResult = Timecard.FindOfTdate(oid, uid, tdate);
		if(!timesResult.isSuccess()) {
			result.message = timesResult.getMessage();
			return result;
		}
		int timecardCount = timesResult.getList().size();
		boolean insertable;
		if(timecardCount == 0) {
			// 未出勤の場合
			insertable = true;
		} else {
			// 既出勤・既退勤・既移動の場合
			insertable = false;
		}
		times = 1;

		// 出勤時間の解析
		AnalyzeResult timeAR = getARTime(tmpTime);
		if(timeAR.isError) {
			result.message = timeAR.message;
			return result;
		}
		intime = new Integer(timeAR.result.toString());

		// 出勤時間の前後関係確認
		AnalyzeResult timeAR2 = getARTime2(SYUKKIN, intime, (List<Timecard>)timesResult.getList());
		if(timeAR2.isError) {
			result.message = timeAR2.message;
			return result;
		}

		// tclog:
		logdate = Util.GetNowDateInt();
		logtime = Util.GetNowTimeInt();

		// tclog:funcの記述
		StringBuilder sb = new StringBuilder();
		sb.append("出勤時刻:").append(intime).append(",");
		func = sb.toString();

		// 実行
		Result r;
		if(insertable) {
			r = insert();
		} else {
			outtime = -2;
			wid = -2;
			note = null;
			wcount = -2;
			rcount = -2;
			breaktime = -2;
			fare = -2;
			r = update();
		}

		// メッセージ作成
		if(r.isSuccess()) {
			result.isError = false;
			try {
				result.text = MeditechnoMailText.GetSendText(oid, uid, tdate, "出勤", uidName, oidName);
			} catch (Exception ex) {
				// 返信文章作成時エラー対応
				result.isError = true;
				result.message = String.format(
						"出勤メールの受付が完了しました。\r\n" +
						"\r\n" +
						"【備考】本メールはデータ処理中に予期しないエラーが発生した場合、通常の返信メールに代わって発行されます。" +
						"そのため本メールに登録状況が記載されておりませんが、データ登録は完了しているため、<<再度メールを送信していただく必要はありません>>。\r\n" +
						"また勤怠情報を確認したい場合は再度同じ内容のメールを送信していただくか、管理者までお問い合わせ下さい。\r\n" +
						"\r\n" +
						"%s\r\n" +
						"%s\r\n",
						Util.GetNowDateTime(), oidName);
			}
		} else {
			result.message = r.getMessage();
		}

		return result;
	}

	/**
	 * 退勤報告をします。
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public TimecardResult doTaikin() {
		TimecardResult result = new TimecardResult();
		result.isError = true;

		// timesの設定
		Result timesResult = Timecard.FindOfTdate(oid, uid, tdate);
		if(!timesResult.isSuccess()) {
			result.message = timesResult.getMessage();
			return result;
		}
		int timecardCount = timesResult.getList().size();

		// 退勤時間の解析
		AnalyzeResult timeAR = getARTime(tmpTime);
		if(timeAR.isError) {
			result.message = timeAR.message;
			return result;
		}
		int _outtime = new Integer(timeAR.result.toString());

		// 退勤時間の前後関係確認
		AnalyzeResult timeAR2 = getARTime2(TAIKIN, _outtime, (List<Timecard>)timesResult.getList());
		if(timeAR2.isError) {
			result.message = timeAR2.message;
			return result;
		}

		// 残業承認パスワード必要確認
		if (_outtime >= 1745) {
			String message = String.format(
					"[時刻: %s] >>> 17時45分を超える退勤はパスワードが必要です。一行目に3文字のパスワードを入力して下さい。\r\n",
					timeAR.result.toString());
			result.message = this.getErrorText(message);
			return result;
		}

		// 勤務種別の解析
		AnalyzeResult worktypeAR = getARWorktype(tmpWorktype);
		if(worktypeAR.isError) {
			result.message = worktypeAR.message;
			return result;
		}
		int _wid = new Integer(worktypeAR.result.toString());

		// 業務内容の解析
		AnalyzeResult noteAR = getARNote(tmpNote);
		if(noteAR.isError) {
			result.message = noteAR.message;
			return result;
		}
		String _note = noteAR.result.toString();

		// 処理枚数の解析
		AnalyzeResult wcountAR = getARWcount(tmpWcount);
		if(wcountAR.isError) {
			result.message = wcountAR.message;
			return result;
		}
		int _wcount = new Integer(wcountAR.result.toString());

		// 申出枚数の解析
		AnalyzeResult rcountAR = getARRcount(tmpRcount);
		if(rcountAR.isError) {
			result.message = rcountAR.message;
			return result;
		}
		int _rcount = new Integer(rcountAR.result.toString());

		// 休憩時間の解析
		AnalyzeResult breaktimeAR = getARBreaktime(tmpBreaktime);
		if(breaktimeAR.isError) {
			result.message = breaktimeAR.message;
			return result;
		}
		int _breaktime = new Integer(breaktimeAR.result.toString());

		// 交通費の解析
		AnalyzeResult fareAR = getARFare(tmpFare);
		if(fareAR.isError) {
			result.message = fareAR.message;
			return result;
		}
		int _fare = new Integer(fareAR.result.toString());

		// tclog:
		logdate = Util.GetNowDateInt();
		logtime = Util.GetNowTimeInt();

		// tclog:funcの記述
		StringBuilder sb = new StringBuilder();
		sb.append("退勤時刻:").append(_outtime).append(",");
		sb.append("勤務種別:").append(tmpWorktype).append(",");
		sb.append("業務内容:").append(_note).append(",");
		sb.append("処理枚数:").append(_wcount).append(",");
		sb.append("申出枚数:").append(_rcount).append(",");
		sb.append("休憩時間:").append(_breaktime).append(",");
		sb.append("交通費:").append(_fare).append(",");
		func = sb.toString();

		// 実行
		Result r;
		if(timecardCount == 0) {
			// 初追加
			List<String> sqls = new ArrayList<String>();
			times = 1;
			intime = -2;
			outtime = _outtime;
			wid = _wid;
			note = _note;
			wcount = _wcount;
			rcount = _rcount;
			breaktime = _breaktime;
			fare = _fare;
			sqls.add(getTimecardInsertSQL());
			sqls.add(getTclogInsertSQL());
			r = execSQL(sqls);
		} else if(timecardCount == 1) {
			// 既出勤or既退勤
			List<String> sqls = new ArrayList<String>();
			times = 1;
			intime = -2;
			outtime = _outtime;
			wid = _wid;
			note = _note;
			wcount = _wcount;
			rcount = _rcount;
			breaktime = _breaktime;
			fare = _fare;
			sqls.add(getTimecardUpdateSQL());
			sqls.add(getTclogInsertSQL());
			r = execSQL(sqls);
		} else {
			// 既移動
			List<String> sqls = new ArrayList<String>();
			times = 1;
			intime = -2;
			outtime = -2;
			wid = -2;
			note = null;
			wcount = -2;
			rcount = -2;
			breaktime = _breaktime;
			fare = _fare;
			sqls.add(getTimecardUpdateSQL());
			times = 2;
			intime = -2;
			outtime = _outtime;
			wid = _wid;
			note = _note;
			wcount = _wcount;
			rcount = _rcount;
			breaktime = 0;
			fare = 0;
			sqls.add(getTimecardUpdateSQL());
			sqls.add(getTclogInsertSQL());
			r = execSQL(sqls);
		}

		// メッセージ作成
		if(r.isSuccess()) {
			result.isError = false;
			try {
				result.text = MeditechnoMailText.GetSendText(oid, uid, tdate, "退勤", uidName, oidName);
			} catch (Exception ex) {
				// 返信文章作成時エラー対応
				result.isError = true;
				result.message = String.format(
						"退勤メールの受付が完了しました。\r\n" +
						"\r\n" +
						"【備考】本メールはデータ処理中に予期しないエラーが発生した場合、通常の返信メールに代わって発行されます。" +
						"そのため本メールに登録状況が記載されておりませんが、データ登録は完了しているため、<<再度メールを送信していただく必要はありません>>。\r\n" +
						"また勤怠情報を確認したい場合は再度同じ内容のメールを送信していただくか、管理者までお問い合わせ下さい。\r\n" +
						"\r\n" +
						"%s\r\n" +
						"%s\r\n",
						Util.GetNowDateTime(), oidName);
			}
		} else {
			result.message = r.getMessage();
		}

		return result;
	}

	/**
	 * 退勤報告(残業承認パスワードあり)をします。
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public TimecardResult doTaikinWithPassward() {
		TimecardResult result = new TimecardResult();
		result.isError = true;

		// timesの設定
		Result timesResult = Timecard.FindOfTdate(oid, uid, tdate);
		if(!timesResult.isSuccess()) {
			result.message = timesResult.getMessage();
			return result;
		}
		int timecardCount = timesResult.getList().size();

		// 退勤時間の解析
		AnalyzeResult timeAR = getARTime(tmpTime);
		if(timeAR.isError) {
			result.message = timeAR.message;
			return result;
		}
		int _outtime = new Integer(timeAR.result.toString());

		// 退勤時間の前後関係確認
		AnalyzeResult timeAR2 = getARTime2(TAIKIN, _outtime, (List<Timecard>)timesResult.getList());
		if(timeAR2.isError) {
			result.message = timeAR2.message;
			return result;
		}

		//パスワード確認
		AnalyzeResult passAR = this.isValidPassward(tmpPassward);
		if (passAR.isError) {
			result.message = passAR.message;
			return result;
		}

		// 勤務種別の解析
		AnalyzeResult worktypeAR = getARWorktype(tmpWorktype);
		if(worktypeAR.isError) {
			result.message = worktypeAR.message;
			return result;
		}
		int _wid = new Integer(worktypeAR.result.toString());

		// 業務内容の解析
		AnalyzeResult noteAR = getARNote(tmpNote);
		if(noteAR.isError) {
			result.message = noteAR.message;
			return result;
		}
		String _note = noteAR.result.toString();

		// 処理枚数の解析
		AnalyzeResult wcountAR = getARWcount(tmpWcount);
		if(wcountAR.isError) {
			result.message = wcountAR.message;
			return result;
		}
		int _wcount = new Integer(wcountAR.result.toString());

		// 申出枚数の解析
		AnalyzeResult rcountAR = getARRcount(tmpRcount);
		if(rcountAR.isError) {
			result.message = rcountAR.message;
			return result;
		}
		int _rcount = new Integer(rcountAR.result.toString());

		// 休憩時間の解析
		AnalyzeResult breaktimeAR = getARBreaktime(tmpBreaktime);
		if(breaktimeAR.isError) {
			result.message = breaktimeAR.message;
			return result;
		}
		int _breaktime = new Integer(breaktimeAR.result.toString());

		// 交通費の解析
		AnalyzeResult fareAR = getARFare(tmpFare);
		if(fareAR.isError) {
			result.message = fareAR.message;
			return result;
		}
		int _fare = new Integer(fareAR.result.toString());

		// tclog:
		logdate = Util.GetNowDateInt();
		logtime = Util.GetNowTimeInt();

		// tclog:funcの記述
		StringBuilder sb = new StringBuilder();
		sb.append("パスワード:").append(tmpPassward).append(",");
		sb.append("退勤時刻:").append(_outtime).append(",");
		sb.append("勤務種別:").append(tmpWorktype).append(",");
		sb.append("業務内容:").append(_note).append(",");
		sb.append("処理枚数:").append(_wcount).append(",");
		sb.append("申出枚数:").append(_rcount).append(",");
		sb.append("休憩時間:").append(_breaktime).append(",");
		sb.append("交通費:").append(_fare).append(",");
		func = sb.toString();

		// 実行
		Result r;
		if(timecardCount == 0) {
			// 初追加
			List<String> sqls = new ArrayList<String>();
			times = 1;
			intime = -2;
			outtime = _outtime;
			wid = _wid;
			note = _note;
			wcount = _wcount;
			rcount = _rcount;
			breaktime = _breaktime;
			fare = _fare;
			sqls.add(getTimecardInsertSQL());
			sqls.add(getTclogInsertSQL());
			r = execSQL(sqls);
		} else if(timecardCount == 1) {
			// 既出勤or既退勤
			List<String> sqls = new ArrayList<String>();
			times = 1;
			intime = -2;
			outtime = _outtime;
			wid = _wid;
			note = _note;
			wcount = _wcount;
			rcount = _rcount;
			breaktime = _breaktime;
			fare = _fare;
			sqls.add(getTimecardUpdateSQL());
			sqls.add(getTclogInsertSQL());
			r = execSQL(sqls);
		} else {
			// 既移動
			List<String> sqls = new ArrayList<String>();
			times = 1;
			intime = -2;
			outtime = -2;
			wid = -2;
			note = null;
			wcount = -2;
			rcount = -2;
			breaktime = _breaktime;
			fare = _fare;
			sqls.add(getTimecardUpdateSQL());
			times = 2;
			intime = -2;
			outtime = _outtime;
			wid = _wid;
			note = _note;
			wcount = _wcount;
			rcount = _rcount;
			breaktime = 0;
			fare = 0;
			sqls.add(getTimecardUpdateSQL());
			sqls.add(getTclogInsertSQL());
			r = execSQL(sqls);
		}

		// メッセージ作成
		if(r.isSuccess()) {
			result.isError = false;
			try {
				result.text = MeditechnoMailText.GetSendText(oid, uid, tdate, "退勤", uidName, oidName);
			} catch (Exception ex) {
				// 返信文章作成時エラー対応
				result.isError = true;
				result.message = String.format(
						"退勤メールの受付が完了しました。\r\n" +
						"\r\n" +
						"【備考】本メールはデータ処理中に予期しないエラーが発生した場合、通常の返信メールに代わって発行されます。" +
						"そのため本メールに登録状況が記載されておりませんが、データ登録は完了しているため、<<再度メールを送信していただく必要はありません>>。\r\n" +
						"また勤怠情報を確認したい場合は再度同じ内容のメールを送信していただくか、管理者までお問い合わせ下さい。\r\n" +
						"\r\n" +
						"%s\r\n" +
						"%s\r\n",
						Util.GetNowDateTime(), oidName);
			}
		} else {
			result.message = r.getMessage();
		}

		return result;
	}

	/**
	 * 移動報告をします。
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public TimecardResult doIdou() {
		TimecardResult result = new TimecardResult();
		result.isError = true;

		// 出勤状況の有無
		Result timesResult = Timecard.FindOfTdate(oid, uid, tdate);
		if(!timesResult.isSuccess()) {
			result.message = timesResult.getMessage();
			return result;
		}
		int timecardCount = timesResult.getList().size();

		// 移動時間の解析
		AnalyzeResult timeAR = getARTime(tmpTime);
		if(timeAR.isError) {
			result.message = timeAR.message;
			return result;
		}
		int _time = new Integer(timeAR.result.toString());

		// 移動時間の前後関係確認
		AnalyzeResult timeAR2 = getARTime2(IDOU, _time, (List<Timecard>)timesResult.getList());
		if(timeAR2.isError) {
			result.message = timeAR2.message;
			return result;
		}

		// 勤務種別の解析
		AnalyzeResult worktypeAR = getARWorktype(tmpWorktype);
		if(worktypeAR.isError) {
			result.message = worktypeAR.message;
			return result;
		}
		int _wid = new Integer(worktypeAR.result.toString());

		// 業務内容の解析
		AnalyzeResult noteAR = getARNote(tmpNote);
		if(noteAR.isError) {
			result.message = noteAR.message;
			return result;
		}
		String _note = noteAR.result.toString();

		// 処理枚数の解析
		AnalyzeResult wcountAR = getARWcount(tmpWcount);
		if(wcountAR.isError) {
			result.message = wcountAR.message;
			return result;
		}
		int _wcount = new Integer(wcountAR.result.toString());

		// 申出枚数の解析
		AnalyzeResult rcountAR = getARRcount(tmpRcount);
		if(rcountAR.isError) {
			result.message = rcountAR.message;
			return result;
		}
		int _rcount = new Integer(rcountAR.result.toString());

		// tclog:
		logdate = Util.GetNowDateInt();
		logtime = Util.GetNowTimeInt();

		// tclog:funcの記述
		StringBuilder sb = new StringBuilder();
		sb.append("移動時刻:").append(_time).append(",");
		sb.append("移動前勤務種別:").append(tmpWorktype).append(",");
		sb.append("移動前業務内容:").append(_note).append(",");
		sb.append("移動前処理枚数:").append(_wcount).append(",");
		sb.append("移動前申出枚数:").append(_rcount).append(",");
		func = sb.toString();

		// 実行
		Result r;
		if(timecardCount == 0) {
			// 初追加
			List<String> sqls = new ArrayList<String>();
			times = 1;
			intime = -1;
			outtime = _time;
			wid = _wid;
			note = _note;
			wcount = _wcount;
			rcount = _rcount;
			breaktime = 0;
			fare = 0;
			sqls.add(getTimecardInsertSQL());
			times = 2;
			intime = _time;
			outtime = -1;
			wid = 0;
			note = "";
			wcount = 0;
			rcount = 0;
			breaktime = 0;
			fare = 0;
			sqls.add(getTimecardInsertSQL());
			sqls.add(getTclogInsertSQL());
			r = execSQL(sqls);
		} else if(timecardCount == 1) {
			// 既出勤or既退勤
			Timecard tc = (Timecard)timesResult.getList().get(0);
			List<String> sqls = new ArrayList<String>();
			times = 1;
			intime = -2;
			outtime = _time;
			wid = _wid;
			note = _note;
			wcount = _wcount;
			rcount = _rcount;
			breaktime = -2;
			fare = -2;
			sqls.add(getTimecardUpdateSQL());
			times = 2;
			intime = _time;
			outtime = tc.getOuttime();
			wid = tc.getWid();
			note = tc.getNote();
			wcount = tc.getWcount();
			rcount = tc.getRcount();
			breaktime = 0;
			fare = 0;
			sqls.add(getTimecardInsertSQL());
			sqls.add(getTclogInsertSQL());
			r = execSQL(sqls);
		} else {
			// 既移動
			List<String> sqls = new ArrayList<String>();
			times = 1;
			intime = -2;
			outtime = _time;
			wid = _wid;
			note = _note;
			wcount = _wcount;
			rcount = _rcount;
			breaktime = -2;
			fare = -2;
			sqls.add(getTimecardUpdateSQL());
			times = 2;
			intime = _time;
			outtime = -2;
			wid = -2;
			note = null;
			wcount = -2;
			rcount = -2;
			sqls.add(getTimecardUpdateSQL());
			sqls.add(getTclogInsertSQL());
			r = execSQL(sqls);
		}

		// メッセージ作成
		if(r.isSuccess()) {
			result.isError = false;
			try {
				result.text = MeditechnoMailText.GetSendText(oid, uid, tdate, "移動", uidName, oidName);
			} catch (Exception ex) {
				// 返信文章作成時エラー対応
				result.isError = true;
				result.message = String.format(
						"移動メールの受付が完了しました。\r\n" +
						"\r\n" +
						"【備考】本メールはデータ処理中に予期しないエラーが発生した場合、通常の返信メールに代わって発行されます。" +
						"そのため本メールに登録状況が記載されておりませんが、データ登録は完了しているため、<<再度メールを送信していただく必要はありません>>。\r\n" +
						"また勤怠情報を確認したい場合は再度同じ内容のメールを送信していただくか、管理者までお問い合わせ下さい。\r\n" +
						"\r\n" +
						"%s\r\n" +
						"%s\r\n",
						Util.GetNowDateTime(), oidName);
			}
		} else {
			result.message = r.getMessage();
		}

		return result;
	}

	/**
	 * 業務内容の解析
	 * @return
	 */
	@Override
	protected AnalyzeResult getARNote(String note) {
		AnalyzeResult ar = new AnalyzeResult();
		ar.isError = true;

		if(note == null) {
			// null
			/*
			 * 許可。その場合は空文字
			 */
			ar.result = "";
			ar.isError = false;
			return ar;
		}
		if(note.equals("")) {
			// 空文字
			/*
			 * 許可。その場合は空文字
			 */
			ar.result = "";
			ar.isError = false;
			return ar;
		}
		if(note.length() > 50) {
			// 50字以上
			ar.message = getErrorText(String.format("[業務内容: %d字] >>> 50字以内で入力して下さい。\r\n", note.length()));
			return ar;
		}

		ar.result = note;
		ar.isError = false;
		return ar;
	}

	/**
	 * 処理枚数の解析
	 * @return
	 */
	@Override
	protected AnalyzeResult getARWcount(String wcount) {
		AnalyzeResult ar = new AnalyzeResult();
		ar.isError = true;

		if(wcount == null) {
			// null
			/*
			 * 許可。その場合は０
			 */
			ar.result = 0;
			ar.isError = false;
			return ar;
		}
		if(wcount.equals("")) {
			// 空文字
			/*
			 * 許可。その場合は０
			 */
			ar.result = 0;
			ar.isError = false;
			return ar;
		}
		int intWcount;
		try {
			intWcount = Integer.parseInt(wcount);
		} catch(NumberFormatException ex) {
			// 数値以外
			ar.message = getErrorText(String.format("[処理数: %s] >>> 数値で入力して下さい。\r\n", wcount));
			return ar;
		}
		if(intWcount < 0) {
			// マイナス
			ar.message = getErrorText(String.format("[処理数: %s] >>> 0以上で入力して下さい。\r\n", wcount));
			return ar;
		}

		ar.result = intWcount;
		ar.isError = false;
		return ar;
	}

	/**
	 * 申出枚数の解析
	 * @return
	 */
	@Override
	protected AnalyzeResult getARRcount(String rcount) {
		AnalyzeResult ar = new AnalyzeResult();
		ar.isError = true;

		if(rcount == null) {
			// null
			/*
			 * 許可。その場合は０
			 */
			ar.result = 0;
			ar.isError = false;
			return ar;
		}
		if(rcount.equals("")) {
			// 空文字
			/*
			 * 許可。その場合は０
			 */
			ar.result = 0;
			ar.isError = false;
			return ar;
		}
		int intRcount;
		try {
			intRcount = Integer.parseInt(rcount);
		} catch(NumberFormatException ex) {
			// 数値以外
			ar.message = getErrorText(String.format("[申出数: %s] >>> 数値で入力して下さい。\r\n", rcount));
			return ar;
		}
		if(intRcount < 0) {
			// マイナス
			ar.message = getErrorText(String.format("[申出数: %s] >>> 0以上で入力して下さい。\r\n", rcount));
			return ar;
		}

		ar.result = intRcount;
		ar.isError = false;
		return ar;
	}

}
