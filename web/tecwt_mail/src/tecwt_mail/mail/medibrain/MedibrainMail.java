package tecwt_mail.mail.medibrain;

import javax.mail.MessagingException;

import tecwt_mail.db.Result;
import tecwt_mail.db.Timecard;
import tecwt_mail.db.Users;
import tecwt_mail.mail.Mail;
import tecwt_mail.mail.MailInterface;
import tecwt_mail.mail.MailTimecard.TimecardResult;
import tecwt_mail.util.Util;



public class MedibrainMail extends MailInterface {

	private MedibrainMailReceiver receiver;

	public MedibrainMail(MedibrainMailReceiver receiver) {
		this.receiver = receiver;
	}

	public Mail getMail(Mail mail, Users user) {

		// 組織名の取得
		String oidName = receiver.oname;

		// 本文解析
		String[] contents;
		MedibrainMailAnalyze analyze;
		TimecardResult tcResult;
		Mail tmpMail;
		String subject = mail.subject;
		if(subject == null) {
			subject = "";
		}
		switch(subject) {

		case "出勤":
			// ----
			// 出勤時刻
			// ----
			contents = getContents(mail.text, 1);
			analyze = new MedibrainMailAnalyze(user.getOid(), user.getUid(), mail.date, contents[0], user.getName());
			tcResult = analyze.doSyukkin();	// 打刻
			if(tcResult.isError) {
				// エラーメール送信
				System.out.println("SYUKKIN sql error. "+tcResult.message);
				try {
					Mail errorMail = sendErrorMailOfDoNotSyuttaiOrSQLError(mail.from, "出勤メールエラー", tcResult.message);
					Result r = receiver.saveSendMail(mail.messageid, mail.from, errorMail, user.getUid());
					if(!r.isSuccess()) {
						receiver.org.writeLog(
								"<MedibrainMail> workin, send error mail save failed.",
								"from: "+mail.from+", subject: "+mail.subject+", content: "+mail.text, true);
					}
				} catch (Exception ex) {
					receiver.org.writeLog(
							"<MedibrainMail> workin, send error mail save failed.",
							ex,
							"from: "+mail.from+", subject: "+mail.subject+", content: "+mail.text);
				}
				return null;
			}
			tmpMail = new Mail(receiver.mailaddress, mail.from, mail.messageid,
					"出勤メール確認", tcResult.text, Util.GetNowDateInt(), Util.GetNowTimeInt());
			return tmpMail;

		case "退勤":
			// ----
			// 残業承認パスワード(行自体が存在しない場合あり)
			// 退勤時刻(hhmm)
			// (ここから作業内容分は最大10セット繰り返し)
			// 勤務種別(登録のある文言以外は不可)
			// 作業内容(登録のある文言以外は不可)
			// 作業時間(hhmm)
			// (ここまで)
			// 休憩時間(分)
			// 交通費
			// ----
			contents = getContents(mail.text, 2 + 30 + 2 + 1);	// 最終行検知用に+1
			MedibrainLeavingMailAnalyze leaving = new MedibrainLeavingMailAnalyze(user.getOid(), user.getUid(),
					mail.date, contents, user.getName());
			tcResult = leaving.doTaikin();	// 打刻;

			if(tcResult.isError) {
				// エラーメール送信
				System.out.println("TAIKIN sql error. "+tcResult.message);
				try {
					Mail errorMail = sendErrorMailOfDoNotSyuttaiOrSQLError(mail.from, "退勤メールエラー", tcResult.message);
					Result r = receiver.saveSendMail(mail.messageid, mail.from, errorMail, user.getUid());
					if(!r.isSuccess()) {
						receiver.org.writeLog(
								"<MedibrainMail> workout, send error mail save failed.",
								"from: "+mail.from+", subject: "+mail.subject+", content: "+mail.text, true);
					}
				} catch (Exception ex) {
					receiver.org.writeLog(
							"<MedibrainMail> workout, send error mail save failed.",
							ex,
							"from: "+mail.from+", subject: "+mail.subject+", content: "+mail.text);
				}
				return null;
			}
			tmpMail = new Mail(receiver.mailaddress, mail.from, mail.messageid,
					"退勤メール確認", tcResult.text, Util.GetNowDateInt(), Util.GetNowTimeInt());
			return tmpMail;

		case "移動":
			// ----
			// 退勤時刻
			// 勤務種別
			// 業務内容(null可)
			// 処理枚数(null可)
			// 申出枚数(null可)
			// ----
			contents = getContents(mail.text, 5);
			analyze = new MedibrainMailAnalyze(
					user.getOid(), user.getUid(), mail.date,
					contents[0], contents[1], contents[2], null, null, contents[3], contents[4],
					user.getName(), Timecard.IDOU);
			tcResult = analyze.doIdou();	// 打刻
			if(tcResult.isError) {
				// エラーメール送信
				System.out.println("IDOU sql error. "+tcResult.message);
				try {
					Mail errorMail = sendErrorMailOfDoNotSyuttaiOrSQLError(mail.from, "移動メールエラー", tcResult.message);
					Result r = receiver.saveSendMail(mail.messageid, mail.from, errorMail, user.getUid());
					if(!r.isSuccess()) {
						receiver.org.writeLog(
								"<MedibrainMail> workmove, send error mail save failed.",
								"from: "+mail.from+", subject: "+mail.subject+", content: "+mail.text, true);
					}
				} catch (Exception ex) {
					receiver.org.writeLog(
							"<MedibrainMail> workmove, send error mail save failed.",
							ex,
							"from: "+mail.from+", subject: "+mail.subject+", content: "+mail.text);
				}
				return null;
			}
			tmpMail = new Mail(receiver.mailaddress, mail.from, mail.messageid,
					"移動メール確認", tcResult.text, Util.GetNowDateInt(), Util.GetNowTimeInt());
			return tmpMail;

		default:
			// タイトルエラーメール返信
			System.out.println("title not match. "+mail.subject);
			try {
				Mail errorMail = sendErrorMailOfTitleNotMatch(mail.from, oidName);
				Result r = receiver.saveSendMail(mail.messageid, mail.from, errorMail, user.getUid());
				if(!r.isSuccess()) {
					receiver.org.writeLog(
							"<MedibrainMail> work, send error mail save failed.",
							"from: "+mail.from+", subject: "+mail.subject+", content: "+mail.text, true);
				}
			} catch (Exception ex) {
				receiver.org.writeLog(
						"<MedibrainMail> work, send error mail save failed.",
						ex,
						"from: "+mail.from+", subject: "+mail.subject+", content: "+mail.text);
			}
			return null;
		}
	}

	/**
	 * 本文を改行で分割。<br>
	 * その際、wantSize数まで分割仕切れなかった場合はnullが入ります。
	 * @param text
	 * @param wantSize
	 * @return
	 */
	private String[] getContents(String text, int wantSize) {
		// コピペ？かなにかで本文にUTF8のBOMがついてくることがあるので削除
        text = text.replace("\uFEFF", "");
		String[] result = new String[wantSize];
		String[] splits = text.split("\r\n|[\n\r\u2028\u2029\u0085]");
		for(int i=0; i<wantSize; i++) {
			if(splits.length - 1 < i) {
				result[i] = null;
			} else {
				result[i] = splits[i];
			}
		}
		return result;
	}

	private Mail sendErrorMailOfTitleNotMatch(String to, String oidName) throws MessagingException {
		String subject = "メール受付エラー";
		String text = "メールの受付でエラーが発生しました。以下の理由により正常に受け付けられませんでした。\r\n"
				+ "正確に記述の上、再度メールを送信して下さい。\r\n"
				+ "\r\n"
				+ "題名(件名、またはタイトル)が違います。\r\n"
				+ "[ 出勤 ][ 退勤 ][ 移動 ]のいずれかで指定して下さい。\r\n"
				+ "\r\n"
				+ Util.GetNowDateTime()+"\r\n"
				+ oidName;
		return receiver.sendMail(to, subject, text);
	}

	private Mail sendErrorMailOfDoNotSyuttaiOrSQLError(String to, String subject, String text) throws MessagingException {
		return receiver.sendMail(to, subject, text);
	}



}
