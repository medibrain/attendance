package tecwt_mail.mail.medibrain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import tecwt_mail.db.Result;
import tecwt_mail.db.Timecard;
import tecwt_mail.db.WorklistRequest;
import tecwt_mail.db.WorklistRequest.Work;
import tecwt_mail.mail.MailTimecard;
import tecwt_mail.util.Util;

// 作業内容付の退勤メール用
public class MedibrainLeavingMailAnalyze extends MailTimecard {
    private String[] contents;
	private String uidName;

	public MedibrainLeavingMailAnalyze(int oid, int uid, int tdate, String[] contents, String uidName) {
        super(oid);

		this.oid = oid;
		this.uid = uid;
        this.tdate = tdate;

        this.contents = contents;
        this.uidName = uidName;

		// default
		this.intime = -1;
		this.outtime = -1;
        this.note = "";

		// tclog default
		this.logtype = "メール 退勤報告";
		this.latitude = "0.0";
		this.longitude = "0.0";
		this.unitid = "不明";
    }

    /******************
	 * 出退勤メソッド *
	 *****************/

     /**
	 * 退勤報告をします。
	 */
	public TimecardResult doTaikin() {
		TimecardResult result = new TimecardResult();
		result.isError = true;

		// timesの設定
		Result timesResult = FindOfTdate(oid, uid, tdate);
		if(!timesResult.isSuccess()) {
			result.message = timesResult.getMessage();
			return result;
		}
		@SuppressWarnings("unchecked")
		List<Timecard> timecardList = (List<Timecard>)timesResult.getList();

        boolean hasPassword = (contents[0].length() == 3 && contents[1].length() == 4);
        String tmpPassward = hasPassword ? contents[0] : null;
        String tmpTime = hasPassword ? contents[1] : contents[0];

		// 退勤時間の解析
		AnalyzeResult timeAR = getARTime(tmpTime);
		if(timeAR.isError) {
			result.message = timeAR.message;
			return result;
		}

		try {
			outtime = Integer.parseInt(timeAR.result.toString());
		} catch (Exception e) {
			result.message = "不明なエラー: " + timeAR.result.toString();
			return result;
		}

		// 退勤時間の前後関係確認
		AnalyzeResult timeAR2 = getARTime2(TAIKIN, outtime, timecardList);
		if(timeAR2.isError) {
			result.message = timeAR2.message;
			return result;
		}

        // 現在時刻チェック
		if (outtime >= Util.GetNowTimeInt()) {
            String message = String.format(
				"[時刻: %s] >>> 退勤時刻を現在時刻よりも遅くすることはできません。\r\n", outtime);
			result.message = this.getErrorText(message);
    		return result;
		}

		// 残業承認パスワード必要確認
		if (outtime >= 1745) {
            if (hasPassword) {
	        	// パスワード確認
	        	AnalyzeResult passAR = this.isValidPassward(tmpPassward);
	        	if (passAR.isError) {
	        		result.message = passAR.message;
	        		return result;
	        	}
            } else {
                String message = String.format(
					"[時刻: %s] >>> 17時45分を超える退勤はパスワードが必要です。一行目に3文字のパスワードを入力して下さい。\r\n",
					timeAR.result.toString());
    			result.message = this.getErrorText(message);
	    		return result;
            }
        }

        // 作業内容の解析
        // 3行1セット
		int total = 0;		// 作業の合計時間
		HashMap<Integer, HashSet<Integer>> workSet = new HashMap<>();
		StringBuffer sb = new StringBuffer();

        int index = hasPassword ? 2 : 1;
        List<Work> works = new ArrayList<Work>();
        while (true) {
            String tmpWorktype = contents[index];
            String tmpWorkDetail = contents[index + 1];
            String tmpWorkTime = contents[index + 2];

            if (tmpWorkTime == null || tmpWorkTime.trim().isEmpty()) {
                break;  // 作業内容が終わったみたいなので抜ける
            }

            // 勤務種別の解析
            AnalyzeResult worktypeAR = getARWorktype(tmpWorktype);
            if (worktypeAR.isError) {
                result.message = worktypeAR.message;
                return result;
            }
            sb.append("勤務種別: ").append(tmpWorktype).append("\r\n");

            // 業務内容の解析
            AnalyzeResult detailAR = getARWorkDetail(tmpWorkDetail);
            if (detailAR.isError) {
                result.message = detailAR.message;
                return result;
            }
            sb.append("業務内容: ").append(tmpWorkDetail).append("\r\n");

            // 作業時間の解析
            AnalyzeResult worktimeAR = getARTime(tmpWorkTime);
            if (worktimeAR.isError) {
                result.message = worktimeAR.message;
                return result;
            }
            sb.append("作業時間: ").append(tmpWorkTime).append("\r\n");

    		try {
                Work work = new Work();
                work.wid = Integer.parseInt(worktypeAR.result.toString());
                work.detailID = Integer.parseInt(detailAR.result.toString());
                int hhmm = Integer.parseInt(worktimeAR.result.toString());
                work.minute = (hhmm / 100) * 60 + (hhmm % 100);
                works.add(work);

                // 重複チェック
                HashSet<Integer> set = workSet.get(work.wid);
                if (set == null) {
                	set = new HashSet<>();
                	set.add(work.detailID);
                	workSet.put(work.wid, set);
                } else {
        			if (!set.add(work.detailID)) {
        				result.message = getErrorText(String.format("[勤務種別: %s 業務内容: %s] >>> 重複しています。\r\n",
        						tmpWorktype, tmpWorkDetail));
        				return result;
        			}
                }
    			total += work.minute;
    		} catch (Exception e) {
    			result.message = "不明なエラー";
    			return result;
    		}

            index += 3;
        }

        String tmpBreaktime = contents[index];
        String tmpFare = contents[index + 1];

		// 休憩時間の解析
		AnalyzeResult breaktimeAR = getARBreaktime(tmpBreaktime);
		if (breaktimeAR.isError) {
			result.message = breaktimeAR.message;
			return result;
		}

		try {
			breaktime = Integer.parseInt(breaktimeAR.result.toString());
		} catch (Exception e) {
			result.message = "不明なエラー: " + breaktimeAR.result.toString();
			return result;
		}

		// 交通費の解析
		AnalyzeResult fareAR = getARFare(tmpFare);
		if(fareAR.isError) {
			result.message = fareAR.message;
			return result;
		}

		try {
			fare = Integer.parseInt(fareAR.result.toString());
		} catch (Exception e) {
			result.message = "不明なエラー: " + fareAR.result.toString();
			return result;
		}

        WorklistRequest data = new WorklistRequest();
	    data.tdate = tdate;

	    data.oid = oid;
	    data.uid = uid;

	    data.intime = timecardList.size() > 0 ? timecardList.get(0).getIntime() : -1;
	    data.outtime = outtime;
	    data.breaktime = breaktime;
	    data.fare = fare;

	    data.works = works.toArray(new Work[works.size()]);

	    data.latitude = latitude;
	    data.longitude = longitude;
	    data.unitid = unitid;

		if (data.intime < 0) {
			// 出勤時間ないのでチェック不要
		} else {
			int intime = (data.intime / 100) * 60 + (data.intime % 100);
			int outtime = (data.outtime / 100) * 60 + (data.outtime % 100);
			int worktime = outtime - intime - data.breaktime;
			if (total > worktime) {
				int diff = total - worktime;
				result.message = getErrorText(String.format("作業時間の合計が労働時間より%d:%02d超過しています。\r\n",
					diff / 60, diff % 60));
				return result;
			} else if (worktime > total) {
				int diff = worktime - total;
				result.message = getErrorText(String.format("作業時間の合計が労働時間より%d:%02d不足しています。\r\n",
					diff / 60, diff % 60));
				return result;
			}
		}

	    WorklistRequest.Result response = data.execLeavingWorkByMail();
	    if (response.error == null) {
			// メッセージ作成
			result.isError = false;
			try {
				result.text = MedibrainMailText.GetSendText(oid, uid, tdate, "退勤", uidName, oidName, sb.toString());
			} catch (Exception ex) {
				// 返信文章作成時エラー対応
				result.isError = true;
				result.message = String.format(
						"退勤メールの受付が完了しました。\r\n" +
						"\r\n" +
						"【備考】本メールはデータ処理中に予期しないエラーが発生した場合、通常の返信メールに代わって発行されます。" +
						"そのため本メールに登録状況が記載されておりませんが、データ登録は完了しているため、<<再度メールを送信していただく必要はありません>>。\r\n" +
						"また勤怠情報を確認したい場合は再度同じ内容のメールを送信していただくか、管理者までお問い合わせ下さい。\r\n" +
						"\r\n" +
						"%s\r\n" +
						"%s\r\n",
						Util.GetNowDateTime(), oidName);
			}
	    } else {
			result.message = response.error.getMessage();
	    }

		return result;
	}
}
