package tecwt_mail.mail.medibrain;

import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import tecwt_mail.db.Result;
import tecwt_mail.db.Users;
import tecwt_mail.mail.Mail;
import tecwt_mail.mail.MailInterface;
import tecwt_mail.mail.MailReceiver;
import tecwt_mail.mail.Org;
import tecwt_mail.util.Util;

public class MedibrainMailReceiver extends MailReceiver {

	protected Org org;

	public MedibrainMailReceiver(
			Org org,
			String key, int oid, String oname,
			String mailaddress, String password,
			String imaphost, int imapport, String smtphost, int smtpport) {
		super(org, key, oid, oname, mailaddress, password, imaphost, imapport, smtphost, smtpport);

		this.org = org;	// MedibrainMailからの操作用
	}

	/**
	 * メールを解析して対応した返信を行う。また受信・送信メールの保存。
	 */
	@Override
	public boolean onReturnMail(Mail mail) {

		// メール削除フラグ
		boolean delete = true;

		/*
		 * ユーザ判定
		 */
		Users user = getUser(mail);

		/*
		 * 受信メール保存
		 */
		delete = doSaveMail(mail, user);
		if(user == null) return true;	// アドレス未登録者はここで終わり

		/*
		 * 解析
		 */
		Mail reMail = getReturnMail(mail, user);
		if(reMail == null) return false;

		/*
		 * 返信
		 */
		Mail sendMail = doSendReturnMail(mail, reMail);
		if(sendMail == null) return false;

		/*
		 * 返信メール保存
		 */
		doSaveSendMail(mail, sendMail, user);

		return delete;
	}

	@Override
	public void onReturnMailDelete(List<String> messageids) {
		Timer timer = new Timer();
		Random r = new Random();
		int delay = (r.nextInt(20) + 1) * 1000;// 1~20秒
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				try {
					Mail.delete(
							MedibrainMailReceiver.this.imaphost,
							MedibrainMailReceiver.this.imapport,
							MedibrainMailReceiver.this.mailaddress,
							MedibrainMailReceiver.this.password,
							messageids);
				} catch(Exception ex) {
					// 削除失敗した場合はログのみ残す
					StringBuilder sb = new StringBuilder();
					for(String mid : messageids) {
						sb.append(mid).append(", ");
					}
					MedibrainMailReceiver.this.org.writeLog("<MedibrainMailReceiver> receive mail delete failed.", ex, sb.toString());
				}
			}
		}, delay);// エラー対策用に時間をズラす
	}

	/**
	 * メールのFromからユーザ情報を取得します。<br>
	 * ユーザ登録されていない場合、メールアドレスが登録されていない場合、<br>
	 * メール打刻許可がされていない場合、凍結処理がされている場合、<br>
	 * またはエラーが発生した場合はnullを返します。
	 * @param mail
	 * @return
	 */
	private Users getUser(Mail mail) {
		Users user;
		try {
			Result userResult = Users.MatchOfMail(this.oid, mail.from, true, true);
			if(!userResult.isSuccess()) {
				// DBエラー
				Mail errorMail = this.sendErrorMailOfNormal(mail.from);
				Result r = this.saveSendMail(mail.messageid, mail.from, errorMail, 0);
				if(!r.isSuccess()) {
					this.org.writeLog(
							"<MedibrainMailReceiver> send error mail save failed.(1)",
							"from: "+mail.from+", subject: "+mail.subject+", content: "+mail.text, true);
				}
				return null;
			}
			if(!userResult.isBool()) {
				int errorUid = 0;
				Mail errorMail;
				if(userResult.getList() != null && userResult.getList().size() > 0) {
					// 非メール打刻許可者
					errorUid = ((Users)userResult.getList().get(0)).getUid();
					errorMail = this.sendErrorMailOfMailDisable(mail.from);
				} else {
					// ユーザ未登録 or メールアドレス未登録 or 凍結者
					errorMail = this.sendErrorMailOfUid(mail.from);
				}
				Result r = this.saveSendMail(mail.messageid, mail.from, errorMail, errorUid);
				if(!r.isSuccess()) {
					this.org.writeLog(
							"<MedibrainMailReceiver> send error mail save failed.(2)",
							"from: "+mail.from+", subject: "+mail.subject+", content: "+mail.text, true);
				}
				return null;
			}

			user = (Users)userResult.getList().get(0);
			return user;

		} catch (Exception ex) {
			this.org.SendSystemErrorMail(
					"["+Util.GetNowDateTimeFull()+"] <tecwt_mail><MedibrainMailReceiver> exception in the getUser.", ex);
			this.org.writeLog(
					"<MedibrainMailReceiver> exception in the getUser.", ex);
			try {
				Mail errorMail = this.sendErrorMailOfNormal(mail.from);
				Result r = this.saveSendMail(mail.messageid, mail.from, errorMail, 0);
				if(!r.isSuccess()) {
					this.org.writeLog(
							"<MedibrainMailReceiver> send error mail of normal save error.",
							"from: "+mail.from+", subject: "+mail.subject+", content: "+mail.text,
							true);
				}
			} catch (Exception ex2) {
				this.org.writeLog(
						"<MedibrainMailReceiver> send error mail of normal error.", ex2);
			}
			return null;
		}
	}

	/**
	 * 受信メールを保存します。
	 * @param mail
	 * @param user
	 */
	private boolean doSaveMail(Mail mail, Users user) {
		try {
			int uid = user == null ? 0 : user.getUid();
			Result insertResult = this.saveReceiveMail(mail, this.oid, uid);
			if(!insertResult.isSuccess()) {
				// 失敗しても処理は続ける
				this.org.writeLog(
						"<MedibrainMailReceiver> receive mail save failed.",
						"from: "+mail.from+", subject: "+mail.subject+", content: "+mail.text, true);
				return false;
			}
			return true;
		} catch (ArrayIndexOutOfBoundsException ex) {
			// 対応外のメールなのでログのみ残す
			this.org.writeLog(
					"<MedibrainMailReceiver> receive another mail save failed.", ex);
			return false;
		} catch (Exception ex) {
			// 失敗しても処理は続ける
			this.org.writeLog(
					"<MedibrainMailReceiver> exception in the doSaveMail.", ex);
			return false;
		}
	}

	/**
	 * 返信用メールを返します。
	 * @param mail
	 * @param user
	 * @return
	 */
	private Mail getReturnMail(Mail mail, Users user) {
		try {

			// 本文解析 & 返信用メール取得
			MailInterface mi = new MedibrainMail(this);
			return mi.getMail(mail, user);

		} catch (Exception ex) {
			this.org.writeLog(
					"<MedibrainMailReceiver> exception in the getReturnMail.", ex);
			try {
				Mail errorMail = this.sendErrorMailOfNormal(mail.from);
				Result r = this.saveSendMail(mail.messageid, mail.from, errorMail, 0);
				if(!r.isSuccess()) {
					this.org.writeLog(
							"<MedibrainMailReceiver> send error mail of normal error.",
							"from: "+mail.from+", subject: "+mail.subject+", content: "+mail.text, true);
				}
			} catch (Exception ex2) {
				this.org.writeLog(
						"<MedibrainMailReceiver> send error mail of normal error.", ex2);
			}
		}
		return null;
	}

	/**
	 * 返信用メールを送信します。
	 * @param mail
	 * @param reMail
	 * @return
	 */
	private Mail doSendReturnMail(Mail mail, Mail reMail) {
		try {
			Mail sendMail = this.sendMail(reMail.to, reMail.subject, reMail.text);
			return sendMail;
		} catch(Exception ex) {
			this.org.writeLog(
					"<MedibrainMailReceiver> send return mail failed.(1)", ex);
			// もう一度試す
			try {
				Mail sendMail = this.sendMail(reMail.to, reMail.subject, reMail.text);
				return sendMail;
			} catch (Exception ex2) {
				this.org.writeLog(
						"<MedibrainMailReceiver> send return mail failed.(2)", ex2);
			}
		}
		return null;
	}

	/**
	 * 送信メールを保存します。
	 * @param mail
	 * @param sendMail
	 * @param user
	 */
	private void doSaveSendMail(Mail mail, Mail sendMail, Users user) {
		try {
			Result insertResult = this.saveSendMail(mail.messageid, mail.from, sendMail, user.getUid());
			if(!insertResult.isSuccess()) {
				// 失敗しても処理は進める
				this.org.writeLog(
						"<MedibrainMailReceiver> return mail save failed.", null, true);
			}
		} catch (Exception ex) {
			// 失敗しても処理は進める
			this.org.writeLog(
					"<MedibrainMailReceiver> exception in the doSaveSendMail.", ex);
		}
	}

}
