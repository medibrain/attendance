package tecwt_mail.mail.medibrain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import tecwt_mail.db.Result;
import tecwt_mail.db.Timecard;
import tecwt_mail.db.Timecard.TodaysNotCompleter;
import tecwt_mail.mail.MailReceiver;
import tecwt_mail.mail.Org;
import tecwt_mail.mail.TimecardWatcher;
import tecwt_mail.util.Util;

public class Medibrain extends Org {

	// MailManager
	private MailReceiver receiver;
	private TimecardWatcher watcher;

	// TimecardWatcher
	private static final String TIMECARD_COMPLETE_CHECKER = "打刻未完者通知メール";


	public Medibrain(
			String key, int oid, String oname,
			boolean mailreceive, boolean timecardwatch,
			String mailaddress, String password,
			String imaphost, int imapport, String smtphost, int smtpport) {

		// Org初期化
		super(
				key, oid, oname,
				mailreceive, timecardwatch,
				mailaddress, password,
				imaphost, imapport, smtphost, smtpport);

		// MailReceiver初期化
		initReceiver(
				key, oid, oname,
				mailaddress, password,
				imaphost, imapport, smtphost, smtpport);

		// TimecardWatcher初期化
		initWatcher(
				key, oid, oname,
				mailaddress, password,
				smtphost, smtpport);
	}


	@Override
	public MailReceiver getReceiver() {
		return receiver;
	}

	@Override
	public TimecardWatcher getWatcher() {
		return watcher;
	}



	private void initReceiver(
			String key, int oid, String oname,
			String mailaddress, String password,
			String imaphost, int imapport, String smtphost, int smtpport) {
		receiver = new MedibrainMailReceiver(
				this,
				key, oid, oname,
				mailaddress, password,
				imaphost, imapport, smtphost, smtpport);
	}

	private void initWatcher(
			String uri, int oid, String oname,
			String mailaddress, String password,
			String smtphost, int smtpport) {
		watcher = new TimecardWatcher(
				this,
				uri, oid, oname,
				mailaddress, password,
				smtphost, smtpport);

		// 以下、スケジュール登録
		Runner tcc = getTimecardCompleteChecker();
		watcher.addTask(tcc.minute, tcc.hour, tcc.day, tcc.month, tcc.dayOfWeek, TIMECARD_COMPLETE_CHECKER, tcc.runnable);
	}

	/**
	 * スケジュール情報と実行インスタンスを内蔵したRunnerインスタンスを返します。<br>
	 * ----スケジュール・実行内容----<br>
	 * 0 21 * * * (毎日21時)<br>
	 * 打刻未完者に警告メールを送信します。
	 * @return
	 */
	private Runner getTimecardCompleteChecker() {
		Runner runner = new Runner();
		runner.minute = "0";
		runner.hour = "21";
		runner.day = "*";
		runner.month = "*";
		runner.dayOfWeek = "*";
		runner.runnable = new Runnable() {
			@SuppressWarnings("unchecked")
			public void run() {
				// 打刻未完了のユーザを特定 (凍結者は除く)
				Result result = Timecard.FindOfTodaysNotCompleter(Medibrain.this.oid, Util.GetNowDateInt(), true);
				if(!result.isSuccess()) {
					// DB error
					Medibrain.this.log.write("<Medibrain> TodaysNotComplete error.", null, true);
					return;
				}
				List<TodaysNotCompleter> tcs = (List<TodaysNotCompleter>)result.getList();
				if(tcs.size() == 0) {
					// size 0
					return;
				}
				final int notIn = 1;
				final int notOut = 2;
				final int notInOut = 3;
				Map<String, Integer> notCompleterUids = new HashMap<String, Integer>();
				for(TodaysNotCompleter tc : tcs) {
					if(tc.mailladd == null || tc.mailladd.equals("")) {
						// メールアドレス未登録
						continue;
					}
					if(tc.intime == -1) {
						if(notCompleterUids.containsKey(tc.mailladd)) {
							// not in && not out
							notCompleterUids.put(tc.mailladd, notInOut);
							continue;
						}
						// not in
						notCompleterUids.put(tc.mailladd, notIn);
					}
					else if(tc.outtime == -1) {
						if(notCompleterUids.containsKey(tc.mailladd)) {
							// not in && not out
							notCompleterUids.put(tc.mailladd, notInOut);
							continue;
						}
						// not out
						notCompleterUids.put(tc.mailladd, notOut);
					}
				}
				// 打刻未完了者のメールアドレスリスト(勤務状況に応じて分ける)
				List<InternetAddress> notInTos = new ArrayList<InternetAddress>();
				List<InternetAddress> notOutTos = new ArrayList<InternetAddress>();
				List<InternetAddress> notInOutTos = new ArrayList<InternetAddress>();
				for(Map.Entry<String, Integer> e : notCompleterUids.entrySet()) {
					try {
						switch(e.getValue()) {
						case notIn:
							// 未出勤者
							notInTos.add(new InternetAddress(e.getKey(), true));	// true:アドレスチェック
							break;
						case notOut:
							// 未退勤者
							notOutTos.add(new InternetAddress(e.getKey(), true));
							break;
						case notInOut:
							// 未出勤かつ未退勤者
							notInOutTos.add(new InternetAddress(e.getKey(), true));
							break;
						}
					} catch (AddressException ex) {
						// アドレスが不正ならパス
						continue;
					}
				}
				// 打刻未完了者へ送信する(別スレッド)
				if(notInTos.size() > 0) {
					// 未出勤者へ
					new Thread(new Runnable() {
						public void run() {
							InternetAddress[] tos = new InternetAddress[notInTos.size()];
							Medibrain.this.watcher.sendMail(notInTos.toArray(tos), "勤怠通知：出勤が行われていません", getNotInText());
						}
					}).start();
				}
				if(notOutTos.size() > 0) {
					// 未退勤者へ
					new Thread(new Runnable() {
						public void run() {
							InternetAddress[] tos = new InternetAddress[notOutTos.size()];
							Medibrain.this.watcher.sendMail(notOutTos.toArray(tos), "勤怠通知：退勤が行われていません", getNotOutText());
						}
					}).start();
				}
				if(notInOutTos.size() > 0) {
					// 未出勤かつ未退勤者へ
					new Thread(new Runnable() {
						public void run() {
							InternetAddress[] tos = new InternetAddress[notInOutTos.size()];
							Medibrain.this.watcher.sendMail(notInOutTos.toArray(tos), "勤怠通知：出勤、退勤が行われていません", getNotInOutText());
						}
					}).start();
				}
			}
		};

		return runner;
	}

	private String getNotInText() {
		StringBuilder sb = new StringBuilder();
		sb.append("【注意】\r\n");
		sb.append("本日の[ 出勤 ]がまだ行われていません！\r\n");
		sb.append("\r\n");
		sb.append("打刻が可能な方は本日中に行って下さい。\r\n");
		sb.append("打刻ができない方は可能な限り速やかに担当者へ申し出て下さい。");
		sb.append("その際に必要な情報は以下の通りです。\r\n");
		sb.append("--------\r\n");
		sb.append("・出勤時刻\r\n");
		sb.append("--------\r\n");
		sb.append("\r\n");
		sb.append(Util.GetNowDateTime()).append("\r\n");
		sb.append(this.oname);

		return sb.toString();
	}
	private String getNotOutText() {
		StringBuilder sb = new StringBuilder();
		sb.append("【注意】\r\n");
		sb.append("本日の[ 退勤 ]がまだ行われていません！\r\n");
		sb.append("\r\n");
		sb.append("打刻が可能な方は本日中に行って下さい。\r\n");
		sb.append("打刻ができない方は可能な限り速やかに担当者へ申し出て下さい。");
		sb.append("その際に必要な情報は以下の通りです。\r\n");
		sb.append("--------\r\n");
		sb.append("・退勤時刻\r\n");
		sb.append("・勤務種別\r\n");
		sb.append("・業務内容\r\n");
		sb.append("・作業時間\r\n");
		sb.append("・休憩時間\r\n");
		sb.append("・交通費\r\n");
		sb.append("--------\r\n");
		sb.append("\r\n");
		sb.append(Util.GetNowDateTime()).append("\r\n");
		sb.append(this.oname);

		return sb.toString();
	}
	private String getNotInOutText() {
		StringBuilder sb = new StringBuilder();
		sb.append("【注意】\r\n");
		sb.append("本日の[ 出勤 ][ 退勤 ]がまだ行われていません！\r\n");
		sb.append("\r\n");
		sb.append("打刻が可能な方は本日中に行って下さい。\r\n");
		sb.append("打刻ができない方は可能な限り速やかに担当者へ申し出て下さい。");
		sb.append("その際に必要な情報は以下の通りです。\r\n");
		sb.append("--------\r\n");
		sb.append("・出勤時刻\r\n");
		sb.append("--------\r\n");
		sb.append("・退勤時刻\r\n");
		sb.append("・勤務種別\r\n");
		sb.append("・業務内容\r\n");
		sb.append("・作業時間\r\n");
		sb.append("・休憩時間\r\n");
		sb.append("・交通費\r\n");
		sb.append("--------\r\n");
		sb.append("\r\n");
		sb.append(Util.GetNowDateTime()).append("\r\n");
		sb.append(this.oname);

		return sb.toString();
	}


	private class Runner {
		public String minute;
		public String hour;
		public String day;
		public String month;
		public String dayOfWeek;
		public Runnable runnable;
	}

}
