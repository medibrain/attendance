package tecwt_mail.mail;

import java.util.List;
import java.util.regex.Pattern;

import tecwt_mail.db.Result;
import tecwt_mail.db.Timecard;
import tecwt_mail.db.WorkDetailCode;
import tecwt_mail.db.Worktype;
import tecwt_mail.util.Passward;
import tecwt_mail.util.Util;

public class MailTimecard extends Timecard {

	public MailTimecard(int oid) {
		super(oid);
	}

	/******************
	 * 出退勤メソッド *
	 *****************/

	protected AnalyzeResult isValidPassward(String passward) {
		AnalyzeResult ar = new AnalyzeResult();
		ar.isError = true;
		if (passward == null) {
			// null
			ar.message = getErrorText("パスワードを取得できませんでした。\r\n");
			return ar;
		}
		if (passward.equals("")) {
			// 空文字
			ar.message = getErrorText(String.format("[パスワード: %s] >>> 入力されていません。\r\n", passward));
			return ar;
		}
		if (passward.length() != 3) {
			// 3文字以外
			ar.message = getErrorText(String.format("[パスワード: %s] >>> 半角英数字3文字で入力して下さい。例）123\r\n", passward));
			return ar;
		}
		if (!passward.equals(Passward.GetZangyoPassward(Util.GetNowDateString()))) {
			// パスワード違い
			ar.message = getErrorText(String.format("[パスワード: %s] >>> パスワードが違います", passward));
			return ar;
		}

		ar.isError = false;
		return ar;
	}

	/**
	 * 時刻の解析
	 * @return
	 */
	protected AnalyzeResult getARTime(String time) {
		AnalyzeResult ar = new AnalyzeResult();
		ar.isError = true;
		if(time == null) {
			// null
			ar.message = getErrorText("時刻を取得できませんでした。\r\n");
			return ar;
		}
		if(time.equals("")) {
			// 空文字
			ar.message = getErrorText(String.format("[時刻: %s] >>> 入力されていません。\r\n", time));
			return ar;
		}
		if(time.length() != 4) {
			// 4文字以外
			ar.message = getErrorText(String.format("[時刻: %s] >>> 半角数字4桁で入力して下さい。例）0900\r\n", time));
			return ar;
		}
		Pattern ptn = Pattern.compile("\\d{4}");
		if(!ptn.matcher(time).matches()) {
			// 数値[0-9]4桁以外("-"がついてもダメ)
			ar.message = getErrorText(String.format("[時刻: %s] >>> 半角数字以外使用できません。\r\n", time));
			return ar;
		}
		int intTime;
		try {
			intTime = Integer.parseInt(time);
		} catch(NumberFormatException ex) {
			// 上記以外
			ar.message = getErrorText(String.format("[時刻: %s] >>> 特定できませんでした。\r\n", time));
			return ar;
		}
		int dev100 = intTime / 100;			// 1115 >> 11
		int mm = intTime - (dev100 * 100);	// 1115 - (11 * 100) = 15
		if(mm % 15 != 0 || mm > 45) {
			// 分が15の倍数でない
			ar.message = getErrorText(String.format("[時刻: %s] >>> 分で指定可能なのは、00,15,30,45のいずれかです。\r\n", time));
			return ar;
		}
		if(intTime > 2400) {
			// 24時以降
			ar.message = getErrorText(String.format("[時刻: %s] >>> 24時以降は入力できません。\r\n", time));
			return ar;
		}

		ar.result = intTime;
		ar.isError = false;
		return ar;
	}

	/**
	 * 時間の前後関係確認
	 * @param syuttai
	 * @param time
	 * @param tcList
	 * @return
	 */
	protected AnalyzeResult getARTime2(int syuttai, int time, List<Timecard> tcList) {
		AnalyzeResult ar = new AnalyzeResult();
		ar.isError = true;

		int tcSIZE = tcList.size();
		if(tcSIZE == 0) {
			ar.isError = false;
			return ar;
		}
		int intime = tcList.get(0).getIntime();
		int outtime = tcList.get(tcSIZE - 1).getOuttime();
		int movetime;
		switch(syuttai) {
		case SYUKKIN:
			movetime = tcSIZE > 1 ? tcList.get(0).getOuttime() : -1;
			if(movetime != -1) {
				if(time > movetime) {
					ar.message = getErrorText(String.format(
							"[出:%d 移:%d] >>> 出勤時刻を移動時刻よりも遅くすることはできません。\r\n",
							time, movetime));
					return ar;
				}
			}
			if(outtime != -1) {
				if(time > outtime) {
					ar.message = getErrorText(String.format(
							"[出:%d 退:%d] >>> 出勤時刻を退勤時刻よりも遅くすることはできません。\r\n",
							time, outtime));
					return ar;
				}
			}
			break;
		case TAIKIN:
			movetime = tcSIZE > 1 ? tcList.get(tcSIZE - 1).getIntime() : -1;
			if(intime != -1) {
				if(time < intime) {
					ar.message = getErrorText(String.format(
							"[出:%d 退:%d] >>> 退勤時刻を出勤時刻よりも早くすることはできません。\r\n",
							intime, time));
					return ar;
				}
			}
			if(movetime != -1) {
				if(time < movetime) {
					ar.message = getErrorText(String.format(
							"[移:%d 退:%d] >>> 退勤時刻を移動時刻よりも早くすることはできません。\r\n",
							movetime, time));
					return ar;
				}
			}
			break;
		case IDOU:
			if(intime != -1) {
				if(time < intime) {
					ar.message = getErrorText(String.format(
							"[出:%d 移:%d] >>> 移動時刻を出勤時刻よりも早くすることはできません。\r\n",
							intime, time));
					return ar;
				}
			}
			if(outtime != -1) {
				if(time > outtime) {
					ar.message = getErrorText(String.format(
							"[移:%d 退:%d] >>> 移動時刻を退勤時刻よりも遅くすることはできません。\r\n",
							time, outtime));
					return ar;
				}
			}
		}

		ar.isError = false;
		return ar;
	}

	/**
	 * 勤務種別の解析
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected AnalyzeResult getARWorktype(String worktype) {
		AnalyzeResult ar = new AnalyzeResult();
		ar.isError = true;
		if(worktype == null) {
			// null
			ar.message = getErrorText("勤務種別を取得できませんでした。\r\n");
			return ar;
		}
		if(worktype.equals("")) {
			// 空文字
			ar.message = getErrorText(String.format("[勤務種別: %s] >>> 入力されていません。\r\n", worktype));
			return ar;
		}
		Result wResult = Worktype.MatchOfName(oid, worktype, true, true);
		if(!wResult.isSuccess()) {
			// SQL error
			ar.message = getErrorText("勤務種別照合中にエラーが発生しました。\r\n");
			return ar;
		}
		if(!wResult.isBool()) {
			// 非合致
			String text = String.format(
					"[勤務種別: %s] >>> 指定した勤務種別は存在しないか、もしくは現在利用できない状態にあります。\r\n",
					worktype);
			String s = worktype.substring(0, 1);
			Result wLikeResult = Worktype.FindOfNameFirst(oid, s);
			if(wLikeResult.isSuccess() && wLikeResult.isBool()) {
				text += "以下に使用可能な候補を列挙します。\r\n";
				for(Worktype w : (List<Worktype>)wLikeResult.getList()) {
					text += "・"+w.getName() + "\r\n";
				}
			}
			ar.message = getErrorText(text);

			return ar;
		}

		ar.result = ((List<Worktype>)wResult.getList()).get(0).getWid();
		ar.isError = false;
		return ar;
	}

	/**
	 * 作業内容の解析
	 * @return
	 */
	protected AnalyzeResult getARWorkDetail(String detail) {
		AnalyzeResult ar = new AnalyzeResult();
		ar.isError = true;
		if(detail == null) {
			// null
			ar.message = getErrorText("作業内容を取得できませんでした。\r\n");
			return ar;
		}
		if(detail.equals("")) {
			// 空文字
			ar.message = getErrorText(String.format("[作業内容: %s] >>> 入力されていません。\r\n", detail));
			return ar;
		}

		WorkDetailCode.MatchResult result = WorkDetailCode.exists(oid, uid, detail);
		if (result.workDetailID != null) {
			// 一致したのでIDを返す
			ar.result = result.workDetailID;
			ar.isError = false;
		} else if (result.names != null) {
			// 非合致
			String text;
			if (result.names.length == 0) {
				text = "作業内容が登録されていません。責任者へ確認してください。\r\n";
			} else {
				text = String.format(
					"[作業内容: %s] >>> 指定した作業内容は存在しないか、もしくは現在利用できない状態にあります。\r\n",
					detail);

				text += "以下に使用可能な候補を列挙します。\r\n";
				for(String value : result.names) {
					text += "・" + value + "\r\n";
				}
			}
			ar.message = getErrorText(text);
		} else {
			// SQL error
			ar.message = getErrorText("作業内容照合中にエラーが発生しました。\r\n");
		}
		return ar;
	}

	/**
	 * 業務内容の解析
	 * @return
	 */
	protected AnalyzeResult getARNote(String note) {
		AnalyzeResult ar = new AnalyzeResult();
		ar.isError = true;

		if(note == null) {
			// null
			/*
			 * 許可。その場合は空文字
			 */
			ar.result = "";
			ar.isError = false;
			return ar;
		}
		if(note.equals("")) {
			// 空文字
			/*
			 * 許可。その場合は空文字
			 */
			ar.result = "";
			ar.isError = false;
			return ar;
		}
		if(note.length() > 50) {
			// 50字以上
			ar.message = getErrorText(String.format("[業務内容: %d字] >>> 50字以内で入力して下さい。\r\n", note.length()));
			return ar;
		}

		ar.result = note;
		ar.isError = false;
		return ar;
	}

	/**
	 * 処理枚数の解析
	 * @return
	 */
	protected AnalyzeResult getARWcount(String wcount) {
		AnalyzeResult ar = new AnalyzeResult();
		ar.isError = true;

		if(wcount == null) {
			// null
			/*
			 * 許可。その場合は０
			 */
			ar.result = 0;
			ar.isError = false;
			return ar;
		}
		if(wcount.equals("")) {
			// 空文字
			/*
			 * 許可。その場合は０
			 */
			ar.result = 0;
			ar.isError = false;
			return ar;
		}
		int intWcount;
		try {
			intWcount = Integer.parseInt(wcount);
		} catch(NumberFormatException ex) {
			// 数値以外
			ar.message = getErrorText(String.format("[処理枚数: %s] >>> 数値で入力して下さい。\r\n", wcount));
			return ar;
		}
		if(intWcount < 0) {
			// マイナス
			ar.message = getErrorText(String.format("[処理枚数: %s] >>> 0以上で入力して下さい。\r\n", wcount));
			return ar;
		}

		ar.result = intWcount;
		ar.isError = false;
		return ar;
	}

	/**
	 * 申出枚数の解析
	 * @return
	 */
	protected AnalyzeResult getARRcount(String rcount) {
		AnalyzeResult ar = new AnalyzeResult();
		ar.isError = true;

		if(rcount == null) {
			// null
			/*
			 * 許可。その場合は０
			 */
			ar.result = 0;
			ar.isError = false;
			return ar;
		}
		if(rcount.equals("")) {
			// 空文字
			/*
			 * 許可。その場合は０
			 */
			ar.result = 0;
			ar.isError = false;
			return ar;
		}
		int intRcount;
		try {
			intRcount = Integer.parseInt(rcount);
		} catch(NumberFormatException ex) {
			// 数値以外
			ar.message = getErrorText(String.format("[申出枚数: %s] >>> 数値で入力して下さい。\r\n", rcount));
			return ar;
		}
		if(intRcount < 0) {
			// マイナス
			ar.message = getErrorText(String.format("[申出枚数: %s] >>> 0以上で入力して下さい。\r\n", rcount));
			return ar;
		}

		ar.result = intRcount;
		ar.isError = false;
		return ar;
	}

	/**
	 * 休憩時間の解析
	 * @return
	 */
	protected AnalyzeResult getARBreaktime(String breaktime) {
		AnalyzeResult ar = new AnalyzeResult();
		ar.isError = true;

		if(breaktime == null) {
			// null
			ar.message = getErrorText("休憩時間を取得できませんでした。\r\n");
			return ar;
		}
		if(breaktime.equals("")) {
			// 空文字
			ar.message = getErrorText(String.format("[休憩時間: %s] >>> 入力されていません。\r\n", breaktime));
			return ar;
		}
		int intBreaktime;
		try {
			intBreaktime = Integer.parseInt(breaktime);
		} catch (NumberFormatException ex) {
			// 数値以外
			ar.message = getErrorText(String.format("[休憩時間: %s] >>> 数値で入力して下さい。\r\n", breaktime));
			return ar;
		}
		if(intBreaktime < 0) {
			// マイナス
			ar.message = getErrorText(String.format("[休憩時間: %s] >>> 0以上で入力して下さい。\r\n", breaktime));
			return ar;
		}
		if(intBreaktime % 15 != 0) {
			// 15の倍数以外
			ar.message = getErrorText(String.format("[休憩時間: %s] >>> 15の倍数で入力して下さい。\r\n", breaktime));
			return ar;
		}

		ar.result = intBreaktime;
		ar.isError = false;
		return ar;
	}

	/**
	 * 交通費の解析
	 * @return
	 */
	protected AnalyzeResult getARFare(String fare) {
		AnalyzeResult ar = new AnalyzeResult();
		ar.isError = true;

		if(fare == null) {
			// null
			ar.message = getErrorText("交通費を取得できませんでした。\r\n");
			return ar;
		}
		if(fare.equals("")) {
			// 空文字
			ar.message = getErrorText(String.format("[交通費: %s] >>> 入力されていません。\r\n", fare));
		}
		int intFare;
		try {
			intFare = Integer.parseInt(fare);
		} catch (NumberFormatException ex) {
			// 数値以外
			ar.message = getErrorText(String.format("[交通費: %s] >>> 数値で入力して下さい。\r\n", fare));
			return ar;
		}
		if(intFare < 0) {
			// マイナス
			ar.message = getErrorText(String.format("[交通費: %s] >>> 0以上で入力して下さい。\r\n", fare));
			return ar;
		}

		ar.result = intFare;
		ar.isError = false;
		return ar;
	}

	protected String getErrorText(String message) {
		StringBuilder sb = new StringBuilder();
		sb.append("以下の理由によりメールは受け付けられませんでした。\r\n");
		sb.append("正確に記述の上、再度メールを送信して下さい。\r\n");
		sb.append("\r\n");
		sb.append(message);
		sb.append("\r\n");
		sb.append(Util.GetNowDateTime()).append("\r\n");
		sb.append(oidName);
		return sb.toString();
	}

	/*********
	 * class *
	 ********/

	/**
	 * 解析結果
	 */
	public class AnalyzeResult {
		public boolean isError;
		public String message;
		public Object result;
	}

	/**
	 * 出退勤報告結果
	 */
	public class TimecardResult {
		public boolean isError;
		public String message;
		public String text;
	}

}
