package tecwt_mail.mail;

import it.sauronsoftware.cron4j.Scheduler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

public class TimecardWatcher {

	// Org
	protected Org org;

	// Timer
	private Scheduler scheduler;
	private Map<String, TaskScheduleHolder> tsHolders;
	private boolean idle;

	// organization
	public String uri;
	public int oid;
	public String oname;

	// mail
	public String mailaddress;
	public String password;
	public String smtphost;
	public int smtpport;


	public TimecardWatcher(
			Org org,
			String uri, int oid, String oname,
			String mailaddress, String password,
			String smtphost, int smtpport) {
		this.org = org;
		this.uri = uri;
		this.oid = oid;
		this.oname = oname;
		this.mailaddress = mailaddress;
		this.password = password;
		this.smtphost = smtphost;
		this.smtpport = smtpport;
		scheduler = new Scheduler();
		tsHolders = new HashMap<String, TaskScheduleHolder>();
	}

	/**
	 * 各スケジュール名(tag)を取得します。
	 * @param enable true:有効なスケジュールのみ
	 * @return
	 */
	public String[] getTags(boolean enable) {
		List<String> tags = new ArrayList<String>();
		for(Map.Entry<String, TaskScheduleHolder> e : tsHolders.entrySet()) {
			if(enable) {
				if(e.getValue().enable) {
					tags.add(e.getKey());
				}
			} else {
				tags.add(e.getKey());
			}

		}
		return tags.toArray(new String[tags.size()]);
	}

	/**
	 * スケジュール情報を格納したTaskScheduleHolderインスタンスを取得します。
	 * @param tag
	 * @return
	 */
	public TaskScheduleHolder getHolder(String tag) {
		return tsHolders.get(tag);
	}

	/**
	 * 定期的処理(スケジューラ)を全て開始します。
	 */
	public final boolean start() {
		try {
			idle = true;
			scheduler.start();
		} catch (IllegalStateException ex) {
			org.SendSystemErrorMail("[tecwt_mail][TimecardWatcher] Scheduler#start error.", ex);
			idle = false;
			return false;
		}
		return true;
	}

	/**
	 * 定期的処理(スケジューラ)を全て停止します。
	 */
	public final void stop() {
		if(scheduler.isStarted()) {
			scheduler.stop();
			idle = false;
		} else {
			if(idle) {
				idle = false;
			}
		}
	}

	/**
	 * 定期的処理(スケジューラ)の状態を確認します。
	 * @return
	 */
	public boolean isIdle() {
		return idle;
	}

	/**
	 * タスク設定を行います。<br>
	 * 戻り値がfalseの場合は不正な値が設定されたことを意味します。
	 * 値に*が指定された部分はワイルドカードとなり、<br>
	 * 下記の範囲内の数値を指定すると、その日時の間隔で行えるようインターバルを自動作成します。<br>
	 * 例えば(0, 23, 1, *, *)と指定すると毎月1日の23時となります。<br>
	 * またdayOfWeekは曜日指定です。0が日曜日となります。<br>
	 * その他の設定方法については以下のサイトを参照してください。<br>
	 * http://www.sauronsoftware.it/projects/cron4j/manual.php#p02
	 * @param minute 0-59 or *
	 * @param hour   0-23 or *
	 * @param day    1-31 or *
	 * @param month  1-12 or *
	 * @param dayOfWeek 0-6 (0=Sunday) or *
	 * @param tag
	 * @param run
	 * @return
	 */
	public final boolean addTask(String minute, String hour, String day, String month, String dayOfWeek, String tag, Runnable runnable) {
		if(!isEnableSchedule(minute, hour, day, month, dayOfWeek)) {
			return false;
		}
		if(isNullOrBlank(tag)) {
			return false;
		}
		if(runnable == null) {
			return false;
		}
		// スケジュール文字列作成
		String scheduleStr = toScheduleString(minute, hour, day, month, dayOfWeek);
		// スケジューラに登録
		String id = scheduler.schedule(scheduleStr, runnable);
		// スケジュールを保存
		TaskScheduleHolder holder = new TaskScheduleHolder();
		holder.id = id;
		holder.month = month;
		holder.day = day;
		holder.hour = hour;
		holder.minute = minute;
		holder.dayOfWeek = dayOfWeek;
		holder.runnable = runnable;
		holder.enable = true;
		tsHolders.put(tag, holder);

		return true;
	}

	/**
	 * スケジュールを変更します。<br>
	 * 実行内容を変更することはできません。<br>
	 * 指定方法についてはaddTask(minute, hour, day, month, dayOfWeek, tag, runnable)を参照してください。
	 * @param tag
	 * @param minute
	 * @param hour
	 * @param day
	 * @param month
	 * @param dayOfWeek
	 * @return
	 */
	public final boolean setTask(String tag, String minute, String hour, String day, String month, String dayOfWeek) {
		TaskScheduleHolder ts = tsHolders.get(tag);
		if(ts == null) {
			return false;
		}
		if(isNullOrBlank(ts.id)) {
			return false;
		}
		if(!isEnableSchedule(minute, hour, day, month, dayOfWeek)) {
			return false;
		}
		// スケジュール変更
		scheduler.reschedule(ts.id, toScheduleString(minute, hour, day, month, dayOfWeek));
		// スケジュール保存
		ts.minute = minute;
		ts.hour = hour;
		ts.day = day;
		ts.month = month;
		ts.dayOfWeek = dayOfWeek;
		ts.enable = true;

		return true;
	}

	/**
	 * スケジュールの有効/無効を切り替えます。<br>
	 * すでに有効もしくは無効である場合は無視されますが、戻り値はtrueとなります。
	 * @param tag
	 * @param enable
	 * @return
	 */
	public final boolean setTaskEnable(String tag, boolean enable) {
		TaskScheduleHolder ts = tsHolders.get(tag);
		if(ts == null) {
			return false;
		}
		if(enable) {
			if(!ts.enable) {
				scheduler.reschedule(ts.id, toScheduleString(ts.minute, ts.hour, ts.day, ts.month, ts.dayOfWeek));
				ts.enable = true;
			}
		} else {
			if(ts.enable) {
				scheduler.deschedule(ts.id);
				ts.enable = false;
			}
		}
		return true;
	}

	/**
	 * メールをBCCで一斉送信します。
	 * @param tos
	 * @param subject
	 * @param text
	 */
	public void sendMail(InternetAddress[] tos, String subject, String text) {
		try {
			Mail.send(
					smtphost,
					String.valueOf(smtpport),
					mailaddress,
					password,
					mailaddress,
					tos,
					subject,
					text
			);
		} catch (MessagingException ex) {
			String tosStr = "";
			for(InternetAddress to : tos) {
				tosStr = tosStr + to.getAddress() + ",";
			}
			org.SendSystemErrorMail("[tecwt_mail][TimecardWatcher] Scheduler#start error.", ex, tosStr);
		}
	}


	private boolean isEnableSchedule(String minute, String hour, String day, String month, String dayOfWeek) {
		if(isNullOrBlank(month) || isNotNumberOrNotAsteriskOrNumberOutOfRange(month, 1, 12)) {
			return false;
		}
		if(isNullOrBlank(day) || isNotNumberOrNotAsteriskOrNumberOutOfRange(day, 1, 31)) {
			return false;
		}
		if(isNullOrBlank(hour) || isNotNumberOrNotAsteriskOrNumberOutOfRange(hour, 0, 23)) {
			return false;
		}
		if(isNullOrBlank(minute) || isNotNumberOrNotAsteriskOrNumberOutOfRange(minute, 0, 59)) {
			return false;
		}
		if(isNullOrBlank(dayOfWeek) || isNotNumberOrNotAsteriskOrNumberOutOfRange(dayOfWeek, 0, 6)) {
			return false;
		}
		return true;
	}
	private String toScheduleString(String minute, String hour, String day, String month, String dayOfWeek) {
		StringBuilder sb = new StringBuilder();
		sb.append(minute).append(" ");
		sb.append(hour).append(" ");
		sb.append(day).append(" ");
		sb.append(month).append(" ");
		sb.append(dayOfWeek);
		return sb.toString();
	}
	private boolean isNullOrBlank(String s) {
		if(s == null || s.equals("")) {
			return true;
		}
		return false;
	}
	private boolean isNotNumberOrNotAsteriskOrNumberOutOfRange(String s, int range1, int range2) {
		if(s.equals("*")) {
			return false;
		}
		try {
			int number = Integer.parseInt(s);
			if(number < range1 || range2 < number) {
				return true;
			}
		} catch (NumberFormatException ex) {
			return true;
		}
		return false;
	}


	public class TaskScheduleHolder {
		public boolean enable;
		public String id;
		public String month;
		public String day;
		public String hour;
		public String minute;
		public String dayOfWeek;
		public Runnable runnable;
	}

}
