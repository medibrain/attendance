package tecwt_mail.mail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.ReceivedDateTerm;

import tecwt_mail.util.Log2;

import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.IMAPMessage;

public class Mail {

	public String from;
	public String to;
	public String messageid;
	public String subject;
	public String text;
	public int date;
	public int time;

	/**
	 * 汎用
	 */
	public Mail() {}

	/**
	 * 受信したMessageをメール形式に解析したものを取得します。
	 * @param message
	 * @throws MessagingException
	 */
	public Mail(Message message) throws MessagingException {
		this.from = getFrom(message);
		this.to = getTo(message);
		this.messageid = getMessageid(message);
		this.subject = getSubject(message);
		this.text = getText(message);
		this.date = getDate(message);
		this.time = getTime(message);
	}

	/**
	 * Mailオブジェクトを作成します。
	 * @param from
	 * @param to
	 * @param messageid
	 * @param subject
	 * @param text
	 * @param date
	 * @param time
	 */
	public Mail(String from, String to, String messageid, String subject, String text, int date, int time) {
		this.from = from;
		this.to = to;
		this.messageid = messageid;
		this.subject = subject;
		this.text = text;
		this.date = date;
		this.time = time;
	}

	/**
	 * 送信元アドレス
	 * @param message
	 * @return
	 * @throws MessagingException
	 */
	public static String getFrom(Message message) throws MessagingException {
		String from = ((InternetAddress)message.getFrom()[0]).getAddress();
		return from;
	}

	/**
	 * 宛先
	 * @param message
	 * @return
	 * @throws MessagingException
	 */
	public static String getTo(Message message) throws MessagingException {
		Address[] toAddresses = message.getRecipients(Message.RecipientType.TO);
		String to = "";
		if(toAddresses != null && toAddresses.length > 0) {
			Address toAddress = toAddresses[0];
			to = toAddress.toString();
		}
		return to;
	}

	/**
	 * メッセージＩＤ
	 * @param message
	 * @return
	 * @throws MessagingException
	 */
	public static String getMessageid(Message message) throws MessagingException {
		IMAPMessage imapM = (IMAPMessage)message;
		String messageid =imapM.getMessageID();
		if(messageid != null && messageid.length() > 2) {
			if(messageid.startsWith("<") && messageid.endsWith(">")) {
				messageid = messageid.substring(1, messageid.length()-1);	// 両端が<>なら中身のみ抽出
			}
		}
		return messageid;
	}

	/**
	 * 題名
	 * @param message
	 * @return
	 * @throws MessagingException
	 */
	public static String getSubject(Message message) throws MessagingException {
		String subject = message.getSubject();
		return subject;
	}

	/**
	 * 本文
	 * @param part
	 * @return
	 * @throws MessagingException
	 */
	public static String getText(Part part) throws MessagingException {
		try {
			// テキストの場合
			if(part.isMimeType("text/plain")) {
				return part.getContent().toString();
			}
			// マルチパートの場合は再帰的に走査
			if(part.isMimeType("multipart/*")) {
				Multipart mp = (Multipart)part.getContent();
				int count = mp.getCount();
				BodyPart bp;
				for(int i=0; i<count; i++) {
					bp = mp.getBodyPart(i);
					String str = getText(bp);
					if(str != null) {
						return str;
					}
				}
			}
		} catch(IOException ex) {
			return null;
		}
		return null;
	}

	/**
	 * 送信日付(YYYYMMdd)
	 * @param message
	 * @return
	 * @throws Exception
	 */
	public static int getDate(Message message) throws MessagingException {
		Date date = message.getReceivedDate();
		return date(date);
	}
	private static int date(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int YYMMdd = (cal.get(Calendar.YEAR) * 10000) + ((cal.get(Calendar.MONTH) + 1) * 100) + cal.get(Calendar.DAY_OF_MONTH);
		return YYMMdd;
	}

	/**
	 * 送信時間(hhmm)
	 * @param message
	 * @return
	 * @throws Exception
	 */
	public static int getTime(Message message) throws MessagingException {
		Date date = message.getReceivedDate();
		return time(date);
	}
	private static int time(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int hhmm = (cal.get(Calendar.HOUR_OF_DAY) * 100) + cal.get(Calendar.MINUTE);
		return hhmm;
	}

	/**
	 * メールを送信します。
	 * @param host
	 * @param port
	 * @param user
	 * @param pass
	 * @param from
	 * @param to
	 * @param subject
	 * @param text
	 * @return
	 * @throws MessagingException
	 */
	/*
	public static Mail send(String host, String port, String user, String pass, String from, String to, String subject, String text) throws MessagingException {

		// 認証情報の作成
		Authenticator auth = new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, pass);
			}
		};

		// セッション情報の作成
		Properties props = new Properties();

		// ホストの設定
		props.put("mail.smtp.host", host);
		props.setProperty("mail.smtp.port", port);

		// SMTP関連の設定
		props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.setProperty("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.socketFactory.port", port);

		// タイムアウトの設定
		props.setProperty("mail.smtp.connectiontimeout", "20000");
		props.setProperty("mail.smtp.timeout", "20000");

		// 認証を有効にする
		props.setProperty("mail.smtp.auth", "true");

		// セッションの取得
		Session session = Session.getInstance(props, auth);

		// 送信日時の取得
		Date sendDate = new Date();

		// 送信メール作成
		Message message = new MimeMessage(session);

		// 送信元
		message.setFrom(new InternetAddress(from));

		// 宛先
		InternetAddress address = new InternetAddress(to);
		message.setRecipient(Message.RecipientType.TO, address);

		// 題名、本文
		message.setSubject(subject);
		message.setText(text);

		// 送信日時
		message.setSentDate(sendDate);

		// 送信
		Transport.send(message);

		// Mailインスタンスの返却
		Mail mail = new Mail(from, to, null, subject, text, date(sendDate), time(sendDate));
		return mail;
	}
	*/

	/**
	 * メールを送信します。(To)
	 * @param smtphost
	 * @param smtpport
	 * @param user
	 * @param pass
	 * @param from
	 * @param to
	 * @param subject
	 * @param text
	 * @return
	 * @throws MessagingException
	 */
	public static Mail send(
			String smtphost, String smtpport, String user, String pass,
			String from, String to, String subject, String text) throws MessagingException {
		Message.RecipientType type = Message.RecipientType.TO;	// Toで送信
		InternetAddress[] tos = new InternetAddress[]{new InternetAddress(to)};
		Date sendDate = execSend(type, smtphost, smtpport, user, pass, from, tos, subject, text);
		Mail mail = new Mail(from, to, null, subject, text, date(sendDate), time(sendDate));
		return mail;
	}

	/**
	 * メールを一斉送信します。(Bcc)
	 * @param smtphost
	 * @param smtpport
	 * @param user
	 * @param pass
	 * @param from
	 * @param tos
	 * @param subject
	 * @param text
	 * @throws MessagingException
	 */
	public static void send(
			String smtphost, String smtpport, String user, String pass,
			String from, InternetAddress[] tos, String subject, String text) throws MessagingException {
		Message.RecipientType type = Message.RecipientType.BCC;	// Bccで送信
		execSend(type, smtphost, smtpport, user, pass, from, tos, subject, text);
	}

	private static Date execSend(
			Message.RecipientType type, String smtphost, String smtpport,
			String user, String pass, String from,
			InternetAddress[] tos, String subject, String text) throws MessagingException {

		// 認証情報の作成
		Authenticator auth = new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, pass);
			}
		};

		// セッション情報の作成
		Properties props = new Properties();

		// ホストの設定
		props.put("mail.smtp.host", smtphost);
		props.setProperty("mail.smtp.port", smtpport);

		// SMTP関連の設定
		props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.setProperty("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.socketFactory.port", smtpport);

		// タイムアウトの設定
		props.setProperty("mail.smtp.connectiontimeout", "20000");
		props.setProperty("mail.smtp.timeout", "20000");

		// 認証を有効にする
		props.setProperty("mail.smtp.auth", "true");

		// セッションの取得
		Session session = Session.getInstance(props, auth);
		//session.setDebug(true);

		// 送信日時の取得
		Date sendDate = new Date();

		// 送信メール作成
		MimeMessage message = new MimeMessage(session);

		// 送信元
		message.setFrom(new InternetAddress(from));

		// 宛先
		message.setRecipients(type, tos);

		// 題名、本文
		message.setSubject(subject, "UTF-8");
		message.setText(text, "UTF-8");

		// 送信日時
		message.setSentDate(sendDate);

		// 送信
		Transport.send(message);

		return sendDate;
	}

	/**
	 * messageidをもとに受信メールを削除します。
	 * @param host
	 * @param port
	 * @param user
	 * @param pass
	 * @param messageids
	 * @throws MessagingException
	 */
	public static void delete(String host, int port, String user, String pass, List<String> messageids) throws MessagingException {

		Folder folder = null;
		try {

			Properties props = System.getProperties();

			// セッションの取得
			Session session = Session.getInstance(props, null);
			//session.setDebug(true);

			// Store オブジェクトの取得
			Store store = session.getStore("imaps");

			// 接続
			store.connect(host, port, user, pass);

			// フォルダの取得
			folder = store.getFolder("INBOX");

			// フォルダの展開
			folder.open(Folder.READ_WRITE);

			// 全件取得
			Message[] messages = folder.getMessages();

			// messageid照合
			Mail mail;
			String mid;
			for(String messageid : messageids) {
				for(Message m : messages) {
					mail = new Mail(m);
					mid = mail.messageid;
					if(mid.equals(messageid)) {
						// 削除
						m.setFlag(Flags.Flag.DELETED, true);
						break;
					}
				}
			}

		} finally {
			// フォルダを閉じる
			if(folder != null) {
				folder.close(true);	// 削除を確定
			}
		}
	}

	/**
	 * messageidをもとに送信済みメールを削除します。
	 * @param host
	 * @param port
	 * @param user
	 * @param pass
	 * @param messageids
	 * @throws MessagingException
	 */
	public static void deleteSentMail(
			String host, int port, String user, String pass, List<String> messageids) throws MessagingException {

		Folder folder = null;
		try {

			Properties props = System.getProperties();

			// セッションの取得
			Session session = Session.getInstance(props, null);
			//session.setDebug(true);

			// Store オブジェクトの取得
			Store store = session.getStore("imaps");

			// 接続
			store.connect(host, port, user, pass);

			// 送信済みメールフォルダの取得
			folder = store.getFolder("[Gmail]/送信済みメール");

			// フォルダの展開
			folder.open(Folder.READ_WRITE);

			// 全件取得
			Message[] messages = folder.getMessages();

			// messageid照合
			Mail mail;
			String mid;
			for(String messageid : messageids) {
				for(Message m : messages) {
					mail = new Mail(m);
					mid = mail.messageid;
					if(mid.equals(messageid)) {
						// 削除
						m.setFlag(Flags.Flag.DELETED, true);
						break;
					}
				}
			}

		} finally {
			// フォルダを閉じる
			if(folder != null) {
				folder.close(true);	// 削除を確定
			}
		}
	}

	/**
	 * 指定日以降の全メールを取得します。
	 * @param host
	 * @param port
	 * @param user
	 * @param pass
	 * @return
	 * @throws MessagingException
	 */
	public static List<Mail> getMails(String host, int port, String user, String pass, Date date, Log2 log) throws MessagingException {
		List<Mail> list = null;
		Folder folder = null;
		Thread keeper = null;
		try {

			Properties props = System.getProperties();

			// セッションの取得
			Session session = Session.getInstance(props, null);
			//session.setDebug(true);

			// Store オブジェクトの取得
			Store store = session.getStore("imaps");

			// 接続
			store.connect(host, port, user, pass);

			// フォルダの取得
			folder = store.getFolder("INBOX");

			// フォルダの展開
			folder.open(Folder.READ_ONLY);

			// 条件式「指定日以降」
			ReceivedDateTerm term = new ReceivedDateTerm(ComparisonTerm.GE, date);

			keeper = new Thread(new MailKeepAliver(user, (IMAPFolder)folder, log, null));
			keeper.start();

			// 取得
			Message[] messages = folder.search(term);

			// Mail化
			list = new ArrayList<Mail>();
			try {
				for(Message m : messages) {
					list.add(new Mail(m));
				}
			} catch (Exception ex) {
				log.write("<"+user+"><"+Thread.currentThread().getName()+"><Mail> receive date term error.", ex);
				return null;
			}

		} finally {
			// フォルダを閉じる
			if(folder != null) {
				folder.close(true);	// 削除を確定
			}
			if(keeper != null) {
				if(keeper.isAlive()) {
					keeper.interrupt();
				}
				keeper = null;
			}
		}
		return list;
	}

}
