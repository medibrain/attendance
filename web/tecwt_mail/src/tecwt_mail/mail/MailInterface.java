package tecwt_mail.mail;

import tecwt_mail.db.Users;

public abstract class MailInterface {

	private String oname;

	public void setOname(String oname) {
		this.oname = oname;
	}
	public String getOname() {
		return oname;
	}

	/**
	 * 返信用のテキストを内包したMailオブジェクトを取得します。<br>
	 */
	public abstract Mail getMail(Mail mail, Users user);

}
