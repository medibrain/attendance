package tecwt_mail.mail;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import tecwt_mail.db.Organization;
import tecwt_mail.db.Result;
import tecwt_mail.util.Log2;
import tecwt_mail.util.Util;
import tecwt_mail.util.Xml;

public class MailManager implements ServletContextListener {

	private static String xmlPath;					// XMLファイル所在地
	private static Map<String, Org> orgs;			// 組織リスト
	private static MailWatcher watcher;				// メール受信監視スレッド
	private static List<String> exceptions;			// コマンド実行時のエラー内容一覧
	private static String loginId;					// ログインID
	private static String loginPass;				// ログインパスワード
	private static String systemErrorMailaddress;	// エラー時送信先メールアドレス


	/**
	 * WEBアプリケーション起動時に呼ばれる
	 */
	@Override
	public void contextInitialized(ServletContextEvent event) {
		// 初期化
		orgs = new HashMap<String, Org>();
		exceptions = new ArrayList<String>();
		// ルートまでのパス
		ServletContext context = event.getServletContext();
		xmlPath = context.getRealPath("/");
		// 各実行スレッド開始
		doManagementStart();
	}
	@Override
	public void contextDestroyed(ServletContextEvent event) {}

	/**
	 * 全組織状況を初期化します。<br>
	 * スタートアップに登録されているメールレシーバーは起動されます。<br>
	 * organization情報を変更した場合、このメソッドを実行することで情報が反映されます。<br>
	 */
	public static void initAllOrg() {
		// 全MailReceiver停止
		stopAllMailReceiver();
		// 初期化
		orgs.clear();
		exceptions.clear();
		// 各実行スレッド開始
		doManagementStart();
	}

	private static void doManagementStart() {
		// システム情報取得
		doInitSystemInfo();
		// 各MailReceiverインスタンス生成
		doInitOrgs();
		// 全メール受信スレッドを起動する
		startAllMailReceiver();
		// メール受信監視スレッドを起動する
		startMailWatcher();
		// 全打刻管理スレッドを起動する
		startAllTimecardWatcher();
	}


	private static void doInitSystemInfo() {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = factory.newDocumentBuilder();
			Document document = documentBuilder.parse(xmlPath + MailInfo.XML_SYSTEM_INFO);
			// system(root)
			Element root = document.getDocumentElement();
			NodeList rootChildren = root.getChildNodes();
			for(int i=0; i < rootChildren.getLength(); i++) {
				Node node = rootChildren.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element)node;
					if (element.getNodeName().equals("login")) {
						// login
						NodeList loginChildren = node.getChildNodes();
						for (int j=0; j < loginChildren.getLength(); j++) {
							Node loginNode = loginChildren.item(j);
							if(loginNode.getNodeType() == Node.ELEMENT_NODE) {
								if(loginNode.getNodeName().equals("id")) {
									// id
									loginId = loginNode.getTextContent();
								} else if(loginNode.getNodeName().equals("pass")) {
									// pass
									loginPass = loginNode.getTextContent();
								}
							}
						}
					}
					else if (element.getNodeName().equals("error")) {
						// error
						NodeList errorChildren = node.getChildNodes();
						for (int j=0; j < errorChildren.getLength(); j++) {
							Node errorNode = errorChildren.item(j);
							if(errorNode.getNodeType() == Node.ELEMENT_NODE) {
								if(errorNode.getNodeName().equals("mailaddress")) {
									// mailaddress
									systemErrorMailaddress = errorNode.getTextContent();
								}
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			exceptions.add("doInitSystemInfoエラー:\n"+Util.GetErrorMessage(ex));
			return;
		}
	}


	private static void doInitOrgs() {
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = factory.newDocumentBuilder();
			Document document = documentBuilder.parse(xmlPath + MailInfo.XML_ORGANIZATION);
			// organizations(root)
			Element root = document.getDocumentElement();
			NodeList rootChildren = root.getChildNodes();
			for(int i=0; i < rootChildren.getLength(); i++) {
				Node node = rootChildren.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					// organization
					Element element = (Element)node;
					if (element.getNodeName().equals("organization")) {
						NodeList organizationChildren = node.getChildNodes();
						String key = element.getAttribute("id");
						int oid = -1;
						boolean mailreceive = false;
						boolean timecardwatch = false;
						String className = null;
						for (int j=0; j < organizationChildren.getLength(); j++) {
							Node organizationNode = organizationChildren.item(j);
							if(organizationNode.getNodeType() == Node.ELEMENT_NODE) {
								if(organizationNode.getNodeName().equals("oid")) {
									// oid
									oid = Integer.parseInt(organizationNode.getTextContent());
								} else if(organizationNode.getNodeName().equals("mailreceive")) {
									// mailreceive
									mailreceive = Boolean.valueOf(organizationNode.getTextContent());
								} else if(organizationNode.getNodeName().equals("timecardwatch")) {
									// timecardwatch
									timecardwatch = Boolean.valueOf(organizationNode.getTextContent());
								} else if(organizationNode.getNodeName().equals("orgclass")) {
									// class
									className = organizationNode.getTextContent();
								}
							}
						}
						// Org登録
						orgs.put(key, getOrg(key, oid, mailreceive, timecardwatch, className));
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			exceptions.add("doInitOrgsエラー:\n"+Util.GetErrorMessage(ex));
			return;
		}
	}

	private static boolean setOrganizationConfigTextContext(String key, String nodeName, String textContext) {
		String fileName = xmlPath + MailInfo.XML_ORGANIZATION;
		if(key == null || key.equals("")) {
			exceptions.add("setOrganizationConfigTextContextエラー:"+key+"\n"+"key is null or blank.\n");
			return false;
		}
		if(nodeName == null || nodeName.equals("")) {
			exceptions.add("setOrganizationConfigTextContextエラー:"+key+"\n"+"nodeName is null or blank.\n");
			return false;
		}
		if(textContext == null || textContext.equals("")) {
			exceptions.add("setOrganizationConfigTextContextエラー:"+key+"\n"+"textContext is null or blank.\n");
			return false;
		}
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = factory.newDocumentBuilder();
			Document document = documentBuilder.parse(fileName);
			// organizations(root)
			Element root = document.getDocumentElement();
			NodeList rootChildren = root.getChildNodes();
			for(int i=0; i < rootChildren.getLength(); i++) {
				Node node = rootChildren.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					// organization
					Element element = (Element)node;
					if (element.getNodeName().equals("organization")) {
						NodeList organizationChildren = node.getChildNodes();
						String xmlId = element.getAttribute("id");
						if(xmlId != null && xmlId.equals(key)) {
							// id合致
							for (int j=0; j < organizationChildren.getLength(); j++) {
								Node organizationNode = organizationChildren.item(j);
								if(organizationNode.getNodeType() == Node.ELEMENT_NODE) {
									if(organizationNode.getNodeName().equals(nodeName)) {
										// 記入
										organizationNode.setTextContent(textContext);
										// XMLファイルを上書き保存
										String result = Xml.write(fileName, document);
										if(result != null) {
											exceptions.add(result);
											return false;
										}
										return true;
									}
								}
							}
							return true;
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			exceptions.clear();
			exceptions.add("setOrganizationConfigTextContextエラー:"+key+"\n"+Util.GetErrorMessage(ex));
		}
		exceptions.add("setOrganizationConfigTextContextエラー:"+key+"\n"+"key or nodeName or is not match.\n");
		return false;
	}

	private static boolean setSystemConfigTextContext(String configName, String nodeName, String textContext) {
		String fileName = xmlPath + MailInfo.XML_SYSTEM_INFO;
		if(nodeName == null || nodeName.equals("")) {
			exceptions.add("setSystemConfigTextContextエラー:"+configName+"\n"+"nodeName is null or blank.\n");
			return false;
		}
		if(textContext == null || textContext.equals("")) {
			exceptions.add("setSystemConfigTextContextエラー:"+configName+"\n"+"textContext is null or blank.\n");
			return false;
		}
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = factory.newDocumentBuilder();
			Document document = documentBuilder.parse(fileName);
			// system(root)
			Element root = document.getDocumentElement();
			NodeList rootChildren = root.getChildNodes();
			for(int i=0; i < rootChildren.getLength(); i++) {
				Node node = rootChildren.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					// error
					Element element = (Element)node;
					if (element.getNodeName().equals(configName)) {
						NodeList organizationChildren = node.getChildNodes();
						for (int j=0; j < organizationChildren.getLength(); j++) {
							Node organizationNode = organizationChildren.item(j);
							if(organizationNode.getNodeType() == Node.ELEMENT_NODE) {
								if(organizationNode.getNodeName().equals(nodeName)) {
									// 記入
									organizationNode.setTextContent(textContext);
									// XMLファイルを上書き保存
									String result = Xml.write(fileName, document);
									if(result != null) {
										exceptions.add(result);
										return false;
									}
									return true;
								}
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			exceptions.clear();
			exceptions.add("setSystemConfigTextContextエラー:"+configName+"\n"+Util.GetErrorMessage(ex));
			return false;
		}
		exceptions.add("setSystemConfigTextContextエラー:"+configName+"\n"+"configName or nodeName is not match.\n");
		return false;
	}

	private static Org getOrg(
			String key, int oid, boolean mailreceive, boolean timecardwatch, String classname) throws Exception {
		// organization情報を取得
		Result result = Organization.MatchOfOid(oid);
		if(result != null && result.getList().size() != 0) {
			// メール情報を取得
			Organization organization = (Organization)result.getList().get(0);
			String oName = organization.getName();
			String mailaddress = organization.getMailladd();
			String password = organization.getPass();
			String imaphost = organization.getImaphost();
			int imapport = organization.getImapport();
			String smtphost = organization.getSmtphost();
			int smtpport = organization.getSmtpport();
			// リフレクションでOrg継承クラスを生成
			Class<?> clazz = Class.forName(classname);
			Constructor<?> constructor = clazz.getConstructor(
					String.class, int.class, String.class,
					boolean.class, boolean.class,
					String.class, String.class,
					String.class, int.class, String.class, int.class);
			Org org = (Org)constructor.newInstance(
					key, oid, oName,
					mailreceive, timecardwatch,
					mailaddress, password,
					imaphost, imapport, smtphost, smtpport);
			return org;
		} else {
			// DB接続エラー
			throw new Exception(result.getMessage());
		}
	}

	/**
	 * 全組織を取得します。
	 * @return
	 */
	public static List<Org> getAllOrg() {
		List<Org> orgList = new ArrayList<Org>();
		for(Map.Entry<String, Org> e : orgs.entrySet()) {
			orgList.add(e.getValue());
		}
		return orgList;
	}

	/**
	 * 例外が発生しているかを確認します。
	 * @return
	 */
	public static boolean isFuncedException() {
		if(exceptions.size() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 発生している例外一覧を取得します。
	 * @return
	 */
	public static List<String> getExceptions() {
		return exceptions;
	}

	/**
	 * 発生している例外一覧をHTML用の文字列で取得します。
	 * @return
	 */
	public static String getExceptionsToHtmlText() {
		StringBuilder sb = new StringBuilder();
		for(String s : exceptions) {
			sb.append(s.replaceAll("\r\n|[\n\r\u2028\u2029\u0085]", "<br>"));
			sb.append("<br>");
		}
		return sb.toString();
	}

	/**
	 * 例外を記述します。<br>
	 * see getExceptions,getExceptionsToHtmlText
	 * @param text
	 */
	public static void setException(String text) {
		exceptions.add(text);
	}

	/**
	 * 組織名を取得します。
	 * @param key
	 * @return
	 */
	public static String getOrgName(String key) {
		return orgs.get(key).oname;
	}

	/**
	 * keyに該当する組織のメール受信の有効/無効を確認します。
	 * @param key
	 * @return
	 */
	public static boolean isMailReceiverEnable(String key) {
		return orgs.get(key).isReceiverEnable();
	}

	/**
	 * 全組織のメール受信の有効/無効を取得します。
	 * @return
	 */
	public static synchronized List<StatusHolder> getAllMailReceiverEnable() {
		List<StatusHolder> statuses = new ArrayList<StatusHolder>();
		StatusHolder holder;
		for(Map.Entry<String, Org> e : orgs.entrySet()) {
			holder = new StatusHolder();
			holder.oname = e.getValue().oname;
			holder.org = e.getKey();
			holder.idle = e.getValue().isReceiverEnable();
			statuses.add(holder);
		}
		return statuses;
	}

	/**
	 * keyに該当する組織の打刻管理の有効/無効を確認します。
	 * @param key
	 * @return
	 */
	public static boolean isTimecardWatcherEnable(String key) {
		return orgs.get(key).isWatcherEnable();
	}

	/**
	 * 全組織の打刻管理の有効/無効を取得します。
	 * @return
	 */
	public static synchronized List<StatusHolder> getAllTimecardWatcherEnable() {
		List<StatusHolder> statuses = new ArrayList<StatusHolder>();
		StatusHolder holder;
		for(Map.Entry<String, Org> e : orgs.entrySet()) {
			holder = new StatusHolder();
			holder.oname = e.getValue().oname;
			holder.org = e.getKey();
			holder.idle = e.getValue().isWatcherEnable();
			statuses.add(holder);
		}
		return statuses;
	}

	/**
	 * keyに該当する組織のメール受信(有効/無効)を操作します。<br>
	 * 有効でない場合、この組織のメール受信に関する操作はできません。<br>
	 * true:有効
	 * @param key
	 * @param enable
	 * @return true:成功
	 */
	public static synchronized boolean setMailReceiverEnable(String key, boolean enable) {
		if(enable) {
			if(setOrganizationConfigTextContext(key, "mailreceive", "true")) {
				orgs.get(key).setReceiverEnable(enable);
				return true;
			}
		} else {
			if(setOrganizationConfigTextContext(key, "mailreceive", "false")) {
				Org org = orgs.get(key);
				if(org.isReceiverEnable()) {
					// 有効>無効になる場合はスレッドを停止する
					stopMailReceiver(key);
				}
				orgs.get(key).setReceiverEnable(enable);
				return true;
			}
		}
		return false;
	}

	/**
	 * keyに該当する組織のメール受信(有効/無効)を操作します。<br>
	 * 有効でない場合、この組織の打刻管理に関する操作はできません。<br>
	 * true:有効
	 * @param key
	 * @param enable
	 * @return true:成功
	 */
	public static synchronized boolean setTimecardWatcherEnable(String key, boolean enable) {
		if(enable) {
			if(setOrganizationConfigTextContext(key, "timecardwatch", "true")) {
				orgs.get(key).setWatcherEnable(enable);
				return true;
			}
		} else {
			if(setOrganizationConfigTextContext(key, "timecardwatch", "false")) {
				Org org = orgs.get(key);
				if(org.isWatcherEnable()) {
					// 有効>無効になる場合はスレッドを停止する
					stopTimecardWatcher(key);
				}
				org.setWatcherEnable(enable);
				return true;
			}
		}
		return false;
	}

	/**
	 * keyに該当する組織のメール受信インスタンスを取得します。
	 * @param key
	 * @return
	 */
	public static MailReceiver getMailReceiver(String key) {
		return orgs.get(key).getReceiver();
	}

	/**
	 * keyに該当する組織の打刻管理インスタンスを取得します。
	 * @param key
	 * @return
	 */
	public static TimecardWatcher getTimecardWatcher(String key) {
		return orgs.get(key).getWatcher();
	}

	/**
	 * keyに該当する組織のメール受信を開始します。<br>
	 * すでに稼働状態にある場合は停止した後に開始されます。
	 * @param key
	 * @return false:失敗もしくは無効
	 */
	public static synchronized boolean startMailReceiver(String key) {
		if(!isMailReceiverEnable(key)) {
			return false;
		}
		if(isMailReceiverIdle(key)) {
			stopMailReceiver(key);
		}
		return orgs.get(key).getReceiver().start();
	}

	/**
	 * keyに該当する組織のメール受信を1分後に再起動します。
	 * @param key
	 */
	public static synchronized void restartMailReceiver(String key) {
		if(!isMailReceiverEnable(key)) {
			return;
		}
		orgs.get(key).getReceiver().restart();
	}

	/**
	 * 全組織のメール受信を開始します。<br>
	 * すでに開始状態にある組織も自動的に停止され、改めて開始されます。<br>
	 * なお、無効となっている組織は無視されます。
	 */
	public static synchronized void startAllMailReceiver() {
		stopAllMailReceiver();
		exceptions.clear();
		for(Map.Entry<String, Org> e : orgs.entrySet()) {
			try {
				if(!isMailReceiverEnable(e.getKey())) {
					continue;
				}
				e.getValue().getReceiver().start();
			} catch (Exception ex) {
				ex.printStackTrace();
				exceptions.add("startAllMailReceiverエラー:"+e.getKey()+"\n"+Util.GetErrorMessage(ex));
				continue;
			}
		}
	}

	/**
	 * keyに該当する組織のメール受信を停止します。
	 * @param key
	 */
	public static synchronized void stopMailReceiver(String key) {
		orgs.get(key).getReceiver().end();
	}

	/**
	 * 全組織のメール受信を停止します。
	 */
	public static synchronized void stopAllMailReceiver() {
		for(Map.Entry<String, Org> e : orgs.entrySet()) {
			if(!isMailReceiverEnable(e.getKey())) {
				continue;
			}
			e.getValue().getReceiver().end();
		}
	}

	/**
	 * keyに該当する組織のメール受信が稼働しているかを確認します。<br>
	 * true:稼働中
	 * @param key
	 * @return
	 */
	public static synchronized boolean isMailReceiverIdle(String key) {
		return orgs.get(key).getReceiver().isIdle();
	}

	/**
	 * 全組織のメール受信稼働状態を取得します。<br>
	 * StatusHolderオブジェクトには組織名とkey、稼働状態が含まれます。<br>
	 * true:稼働中
	 * @return
	 */
	public static synchronized List<StatusHolder> getAllMailReceiverIdleStatus() {
		List<StatusHolder> statuses = new ArrayList<StatusHolder>();
		StatusHolder holder;
		for(Map.Entry<String, Org> e : orgs.entrySet()) {
			holder = new StatusHolder();
			holder.oname = e.getValue().oname;
			holder.org = e.getKey();
			holder.idle = e.getValue().getReceiver().isIdle();
			statuses.add(holder);
		}
		return statuses;
	}

	/**
	 * 全組織のメール受信監視を開始します。<br>
	 * すでに稼働状態にある場合は停止した後に開始されます。
	 * @return
	 */
	public static synchronized boolean startMailWatcher() {
		if(watcher != null && isMailWatcherIdle()) {
			stopMailWatcher();
		}
		Map<String, Long> watchAddresses = new HashMap<String, Long>();
		Long currentTime = Calendar.getInstance().getTimeInMillis();
		for(Map.Entry<String, Org> e : orgs.entrySet()) {
			// 開始時刻を記録　[メールアドレス, 現在時刻(ミリ秒)]
			watchAddresses.put(e.getValue().mailaddress, currentTime);
		}
		watcher = new MailWatcher(watchAddresses);
		return watcher.start();
	}

	/**
	 * 全組織のメール受信監視を停止します。
	 * @return
	 */
	public static synchronized boolean stopMailWatcher() {
		return watcher.end();
	}

	/**
	 * 全組織のメール受信監視が稼働状態にあるかを確認します。<br>
	 * true:稼働中
	 * @return
	 */
	public static synchronized boolean isMailWatcherIdle() {
		return watcher.isIdle();
	}

	/**
	 * keyに該当する組織の打刻管理を開始します。<br>
	 * すでに稼働状態にある場合は停止した後に開始されます。
	 * @param key
	 * @return false:失敗もしくは無効
	 */
	public static synchronized boolean startTimecardWatcher(String key) {
		if(!isTimecardWatcherEnable(key)) {
			return false;
		}
		if(isTimecardWatcherIdle(key)) {
			stopTimecardWatcher(key);
		}
		return orgs.get(key).getWatcher().start();
	}

	/**
	 * 全組織の打刻管理を開始します。<br>
	 * すでに開始状態にある場合は停止した後に開始されます。<br>
	 * なお、無効となっている組織は無視されます。
	 */
	public static synchronized void startAllTimecardWatcher() {
		stopAllTimecardWatcher();
		for(Map.Entry<String, Org> e : orgs.entrySet()) {
			if(!isTimecardWatcherEnable(e.getKey())) {
				continue;
			}
			e.getValue().getWatcher().start();
		}
	}

	/**
	 * keyに該当する組織の打刻管理を停止します。
	 * @param key
	 */
	public static synchronized void stopTimecardWatcher(String key) {
		orgs.get(key).getWatcher().stop();
	}

	/**
	 * 全組織の打刻管理を停止します。
	 */
	public static synchronized void stopAllTimecardWatcher() {
		for(Map.Entry<String, Org> e : orgs.entrySet()) {
			if(!isTimecardWatcherEnable(e.getKey())) {
				continue;
			}
			e.getValue().getWatcher().stop();
		}
	}

	/**
	 * keyに該当する組織の打刻管理状態を確認します。<br>
	 * true:稼働中
	 * @param key
	 * @return
	 */
	public static synchronized boolean isTimecardWatcherIdle(String key) {
		return orgs.get(key).getWatcher().isIdle();
	}

	/**
	 * 全組織の打刻管理稼働状態を取得します。<br>
	 * StatusHolderオブジェクトには組織名とkey、稼働状態が含まれます。<br>
	 * true:稼働中
	 * @return
	 */
	public static synchronized List<StatusHolder> getAllTimecardWatcherIdleStatus() {
		List<StatusHolder> statuses = new ArrayList<StatusHolder>();
		StatusHolder holder;
		for(Map.Entry<String, Org> e : orgs.entrySet()) {
			holder = new StatusHolder();
			holder.oname = e.getValue().oname;
			holder.org = e.getKey();
			holder.idle = e.getValue().getWatcher().isIdle();
			statuses.add(holder);
		}
		return statuses;
	}

	/**
	 * keyに該当する組織の実行スケジュールIDを取得します。<br>
	 * 戻り値は配列であり、値にはスケジュールを識別するためのIDが含まれています。<br>
	 * 取得したIDを用いてスケジュールを取得できます。
	 * @param key
	 * @param enable true:有効なスケジュールのみ
	 * @return
	 */
	public static String[] getTimecardWatcherScheduleId(String key, boolean enable) {
		return orgs.get(key).getWatcher().getTags(enable);
	}

	/**
	 * keyに該当する組織の実行スケジュールのうち、idに指定されているものを取得します。<br>
	 * 配列のインデックス0から順に「分」「時」「日」「月」「曜日」と格納されています。<br>
	 * @param key
	 * @param id
	 * @return
	 */
	public static String[] getTimecardWatcherSchedule(String key, String id) {
		TimecardWatcher tw = getTimecardWatcher(key);
		String[] scheduleValues = new String[5];
		scheduleValues[0] = tw.getHolder(id).minute;
		scheduleValues[1] = tw.getHolder(id).hour;
		scheduleValues[2] = tw.getHolder(id).day;
		scheduleValues[3] = tw.getHolder(id).month;
		scheduleValues[4] = tw.getHolder(id).dayOfWeek;
		return scheduleValues;
	}

	/**
	 * keyに該当する組織の実行スケジュールのうち、idに指定されているものが有効かを確認します。
	 * @param key
	 * @param id
	 * @return
	 */
	public static boolean isTimecardWatcherScheduleEnable(String key, String id) {
		TimecardWatcher tw = getTimecardWatcher(key);
		return tw.getHolder(id).enable;
	}

	/**
	 * keyに該当する組織の実行スケジュールを変更します。<br>
	 * @param key
	 * @param id
	 * @param minute
	 * @param hour
	 * @param day
	 * @param month
	 * @param dayOfWeek
	 * @return
	 */
	public static boolean setTimecardWatcherSchedule(
			String key, String id,
			String minute, String hour, String day, String month, String dayOfWeek) {
		return orgs.get(key).getWatcher().setTask(id, minute, hour, day, month, dayOfWeek);
	}

	/**
	 * keyに該当する組織の実行スケジュールのうち、idに指定されているものの有効/無効を切り替えます。
	 * @param key
	 * @param id
	 * @param enable
	 * @return
	 */
	public static boolean setTimecardWatcherScheduleEnable(String key, String id, boolean enable) {
		return getTimecardWatcher(key).setTaskEnable(id, enable);
	}

	/**
	 * keyに該当する組織のログ出力インスタンスを取得します。
	 * @param key
	 * @return
	 */
	public static Log2 getLog(String key) {
		return orgs.get(key).getLog();
	}

	/**
	 * 全ての組織のログ記述を開始します。<br>
	 * 停止する場合はcloseLog()を呼びます。
	 * @param key
	 */
	public static void openLog() {
		for(Map.Entry<String, Org> e : orgs.entrySet()) {
			orgs.get(e.getKey()).getLog().open();
		}
	}

	/**
	 * 全ての組織のログ記述を停止します。<br>
	 * ファイルを安全に保存するために呼び出す必要があります。<br>
	 * 再開する場合はopenLog()を呼びます。
	 * @param key
	 */
	public static void closeLog() {
		for(Map.Entry<String, Org> e : orgs.entrySet()) {
			orgs.get(e.getKey()).getLog().close();
		}
	}

	/**
	 * メールアドレスからkeyを取得します。<br>
	 * このkeyを使って組織の各種機能を操作できます。<br>
	 * see getMailReceiver(),startMailReceiver(),stopMailReceiver(),
	 * getTimecardWatcher(),startTimecardWatcher(),stopTimecardWatcher()
	 * @param mailaddress
	 * @return
	 */
	public static String getKey(String mailaddress) {
		for(Map.Entry<String, Org> e : orgs.entrySet()) {
			if(e.getValue().mailaddress.equals(mailaddress)) {
				return e.getKey();
			}
		}
		return null;
	}

	/**
	 * key一覧を取得します。
	 * @return
	 */
	public static String[] getAllKey() {
		return orgs.keySet().toArray(new String[orgs.size()]);
	}

	/**
	 * keyの有効/無効を確認します。
	 * @param key
	 * @return
	 */
	public static boolean isEnableKey(String key) {
		return orgs.containsKey(key);
	}

	/**
	 * テストメールを受信したことを記録します。
	 * @param key
	 */
	public static synchronized void setReceived(String key) {
		watcher.setReceived(orgs.get(key).mailaddress);
	}

	/**
	 * ログインIDを取得する。
	 * @return
	 */
	public static synchronized String getLoginId() {
		return loginId;
	}

	/**
	 * ログインIDを設定する。
	 * @param id
	 * @return
	 */
	public static synchronized boolean setLoginId(String id) {
		if(setSystemConfigTextContext("login", "id", id)) {
			loginId = id;
			return true;
		}
		return false;
	}

	/**
	 * ログインパスワードを取得する。
	 * @return
	 */
	public static synchronized String getLoginPass() {
		return loginPass;
	}

	/**
	 * ログインパスワードを設定する。
	 * @param pass
	 * @return
	 */
	public static synchronized boolean setLoginPass(String pass) {
		if(setSystemConfigTextContext("login", "pass", pass)) {
			loginPass = pass;
			return true;
		}
		return false;
	}

	/**
	 * ログインを行います。
	 * @param id
	 * @param pass
	 * @return true:成功
	 */
	public static synchronized boolean login(String id, String pass) {
		if(id == null || id.equals("")) {
			return false;
		}
		if(pass == null || pass.equals("")) {
			return false;
		}
		if(id.equals(loginId) && pass.equals(loginPass)) {
			return true;
		}
		return false;
	}

	/**
	 * システムエラー時送信先メールアドレスを取得する。
	 * @return
	 */
	public static String getSystemErrorMailaddress() {
		return systemErrorMailaddress;
	}

	/**
	 * システムエラー発生時に送信するメールアドレスを設定します。<br>
	 * このメソッドではメールアドレスの有効性をチェックしません。
	 * @param mailaddress
	 * @return true:成功
	 */
	public static synchronized boolean setSystemErrorMailaddress(String mailaddress) {
		if(setSystemConfigTextContext("error", "mailaddress", mailaddress)) {
			systemErrorMailaddress = mailaddress;
			return true;
		}
		return false;
	}





	/**
	 * 稼働状況クラス
	 */
	public static class StatusHolder {
		public String oname;
		public String org;
		public boolean idle;
	}


}
