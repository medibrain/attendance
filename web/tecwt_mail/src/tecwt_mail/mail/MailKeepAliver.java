package tecwt_mail.mail;

import javax.mail.MessagingException;

import tecwt_mail.util.Log2;

import com.sun.mail.iap.ProtocolException;
import com.sun.mail.imap.IMAPFolder;
import com.sun.mail.imap.protocol.IMAPProtocol;

public class MailKeepAliver implements Runnable {

	private static final long INTERVAL = 300000;//5 minutes

	private String oname;
	private IMAPFolder folder;
	private Log2 log;
	private OnMessagingExceptionCallback callback;

	public interface OnMessagingExceptionCallback {
		public abstract void onMessagingException(MessagingException ex);
	}

	public MailKeepAliver(String oname, IMAPFolder folder, Log2 log, OnMessagingExceptionCallback callback){
		this.oname = oname;
		this.folder = folder;
		this.log = log;
		this.callback = callback;
	}

	@Override
	public void run(){
		while (!Thread.interrupted()){//割り込まれるまでループ
			try {
				//一定時間待機
				Thread.sleep(INTERVAL);

				//IMAPFolderとの接続を維持するコードを発行
				folder.doCommand(new IMAPFolder.ProtocolCommand() {
					@Override
					public Object doCommand(IMAPProtocol p) throws ProtocolException {
						p.simpleCommand("NOOP", null);
						return null;
					}
				});

			} catch (InterruptedException ex) {
				//停止処理を受けたので終了する
				break;
			} catch (MessagingException ex) {
				//コマンド実行にエラー
				log.write("<MailKeeepAliver> command error.", ex);
				//コールバック
				if (this.callback != null) {
					this.callback.onMessagingException(ex);
				}
				break;
			}
		}
		log.write("<"+oname+"><"+Thread.currentThread().getName()+"><MailKeepAliver> keeper finished.", null, true);
	}

}
