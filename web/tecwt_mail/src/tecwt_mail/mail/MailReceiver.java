package tecwt_mail.mail;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javax.mail.Folder;
import javax.mail.FolderClosedException;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.event.MessageCountEvent;
import javax.mail.event.MessageCountListener;

import tecwt_mail.db.MailReceive;
import tecwt_mail.db.MailSend;
import tecwt_mail.db.Result;
import tecwt_mail.util.Util;

import com.sun.mail.imap.IMAPFolder;

public abstract class MailReceiver implements Runnable {

	// Org
	protected Org org;

	// organization
	public String key;
	public int oid;
	public String oname;

	// mail
	public String mailaddress;
	public String password;
	public String imaphost;
	public int imapport;
	public String smtphost;
	public int smtpport;

	// thread
	private boolean idle;
	private Thread thread;
	private Folder folder;
	private Thread keeper;
	private boolean isRestartRunning;

	// log stream
	private PrintStream stream;


	public MailReceiver(
			Org org,
			String key, int oid, String oname,
			String mailaddress, String password,
			String imaphost, int imapport, String smtphost, int smtpport) {
		this.org = org;
		this.key = key;
		this.oid = oid;
		this.oname = oname;
		this.mailaddress = mailaddress;
		this.password = password;
		this.imaphost = imaphost;
		this.imapport = imapport;
		this.smtphost = smtphost;
		this.smtpport = smtpport;
	}

	private String getLogTitle(String title) {
		return "<"+this.getClass().getSimpleName()+"> "+title;
	}

	/**
	 * レシーバーを起動します。
	 * @return
	 */
	public final synchronized boolean start() {
		if(thread == null) {
			// 稼働フラグ
			idle = true;
			// スレッド再生成
			thread = new Thread(this);
			thread.start();
			org.writeLog(getLogTitle("MailReceiver started."), null, true);
			return true;
		}
		org.writeLog(
				getLogTitle("MailReceiver start failed."),
				"thread not null: "+(thread!=null)+", idle: "+idle,
				true);
		return false;
	}

	/**
	 * レシーバーを停止します。
	 */
	public final void end() {
		// 停止フラグ
		idle = false;
		// フォルダ閉じる
		doFolderClose();
		// フォルダ解除
		folder = null;
		// スレッド破棄
		thread = null;
		// ストリーム解放
		if(stream != null) {
			stream.close();
			stream = null;
		}
		org.writeLog(getLogTitle("MailReceiver finished."), null, true);
	}

	/**
	 * レシーバーを1分後に再起動します。
	 */
	public final void restart() {
		isRestartRunning = true;
		end();
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			public void run() {
				org.writeLog(getLogTitle("MailReceiver restart now."), null, true);
				start();
			}
		}, TimeUnit.MINUTES.toMillis(1));//1 minutes delay
	}

	private synchronized void doFolderClose() {
		if(folder == null) {
			return;
		}
		try {
			// リスナー解除
			folder.removeMessageCountListener(messageCountListener);
		} catch (Exception ex) {
			org.writeLog(getLogTitle("remove listener failed."), ex);
		}
		try {
			// フォルダを閉じる
			if(folder instanceof IMAPFolder) {
				IMAPFolder imapFld = (IMAPFolder)folder;
				if(imapFld.isOpen()) {
					imapFld.close(true);
				}
			} else {
				if(folder.isOpen()) {
					folder.close(true);
				}
			}
		} catch(Exception ex) {
			// フォルダ閉じれないけどスレッドは終了
			org.writeLog(getLogTitle("folder close failed."), ex);
		}
	}

	/**
	 * 稼働状態の取得
	 * @return
	 */
	public final synchronized boolean isIdle() {
		if(thread != null && idle) {
			return true;
		}
		return false;
	}


	@Override
	public void run() {
		isRestartRunning = false;
		try {

			// 過去未返信分処理
			execUnEditMails();

			// プロパティの取得
			Properties props = System.getProperties();

			// セッションの取得
			Session session = Session.getInstance(props, null);
			stream = org.getLog().getDebugLogStream();
			session.setDebugOut(stream);
			session.setDebug(true);

			// Store オブジェクトの取得
			Store store = session.getStore("imaps");

			// 接続
			store.connect(imaphost, imapport, mailaddress, password);

			// フォルダの取得
			folder = store.getFolder("INBOX");

			// フォルダの展開
			folder.open(Folder.READ_ONLY);	// 削除不可

			// 受信登録
			folder.addMessageCountListener(messageCountListener);

			// 接続を維持する
			keeper = new Thread(
						new MailKeepAliver(
							oname,
							(IMAPFolder)folder,
							org.getLog(),
							new MailKeepAliver.OnMessagingExceptionCallback() {
								@Override
								public void onMessagingException(MessagingException ex) {
									//キーパー内で例外発生した場合でも再起動する
									org.writeLog("idle keeper error.", "restart now.", true);
									restart();
								}
					}));
			keeper.start();

			// ログ
			org.writeLog(getLogTitle("idle start now."), null, true);

			// メール受信待機
			IMAPFolder imapFld;
			try {
				while(idle && folder instanceof IMAPFolder) {
					imapFld = (IMAPFolder)folder;
					if(imapFld.isOpen()) {
						// 受信するまで or close()されるまで待機
						imapFld.idle();
					}
				}
				org.writeLog(getLogTitle("idle finished."), "idle: "+idle, true);
			} catch (FolderClosedException ex) {
				org.writeLog(getLogTitle("idle failed.(1)"), ex);
			} catch (MessagingException ex) {
				org.writeLog(getLogTitle("idle failed.(2)"), ex);
			} catch (IllegalStateException ex) {
				org.writeLog(getLogTitle("idle failed.(3)"), ex);
			}

		} catch(Exception ex) {
			org.writeLog(getLogTitle("exception in the run."), ex);
		} finally {
			if(keeper != null) {
				if(keeper.isAlive()) {
					keeper.interrupt();
				}
				keeper = null;
			}
		}

		//稼働フラグの状態で終了しようとする場合は再起動させる
		if(idle) {
			//再起動
			restart();
			return;
		}

		if(!isRestartRunning) {
			//念のため終了を通知する
			this.org.SendSystemErrorMail(
					"[tecwt_mail]["+this.oname+"] MailReceiver#run finished.",
					Util.GetNowDateTimeFull() + " This end is normal.",
					true);
		}
		org.writeLog(getLogTitle("MailReceiver#run finsihed."), null, true);
	}

	/**
	 * 未処理のメールを処理します。当日分のみ処理します。<br>
	 * この処理は別スレッドで行われます。<br>
	 * 本処理中に再度要求があった場合は無視されます。<br>
	 * @param folder
	 */
	private void execUnEditMails() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					org.writeLog(getLogTitle("unEdit process start now."), null, true);
					//本日分
					Calendar cal = Calendar.getInstance();
					cal.set(Calendar.HOUR, 0);
					cal.set(Calendar.MINUTE, 0);
					Date limit = cal.getTime();
					//メール取得
					List<Mail> mails = Mail.getMails(imaphost, imapport, mailaddress, password, limit, org.getLog());
					Result r;
					List<Mail> unEdits = new ArrayList<Mail>();
					List<String> deleteMids = new ArrayList<String>();
					if(mails == null || mails.size() == 0) return;
					for(Mail mail : mails) {
						if(isWatchersMail(mail, false) || isNoReplyMail(mail)) {
							//処理が必要ないメール
							continue;
						}
						r = MailReceive.matchMessageid(mail.messageid);
						if(r == null || r.isBool()) {
							//すでに処理済み
							continue;
						}
						if(!r.isBool()) {
							//未処理メール
							unEdits.add(mail);
							deleteMids.add(mail.messageid);
						}
					}
					//未処理メールを処理する
					for(Mail unEdit : unEdits) {
						boolean mailDelete = onReturnMail(unEdit);
						if(!mailDelete) {
							//何らかの問題が発生したことにより、メール削除フラグから外す(メールを残す)
							deleteMids.remove(unEdit.messageid);
						}
					}
					//削除
					onReturnMailDelete(deleteMids);
				} catch (MessagingException ex) {
					org.writeLog(getLogTitle("unEdit processing failed.(1)"), ex);
				} catch (IllegalStateException ex) {
					org.writeLog(getLogTitle("unEdit processing failed.(2)"), ex);
				} catch (Exception ex) {
					org.writeLog(getLogTitle("unEdit processing failed.(3)"), ex);
				}
				org.writeLog(getLogTitle("unEdit process finished."), null, true);
			}
		}).start();
	}

	/**
	 * メール受信イベントハンドラ
	 */
	private MessageCountListener messageCountListener = new MessageCountListener() {
		@Override
		public void messagesRemoved(MessageCountEvent ev) {}
		@Override
		public void messagesAdded(MessageCountEvent ev) {
			org.writeLog(getLogTitle("mail received."), null, true);
			MailManager.setReceived(key);

			// リアルタイム受信message一覧
			Message[] msgs = ev.getMessages();

			// messageid一覧
			List<String> messageids = new ArrayList<String>();

			// 読み込み失敗フラグ
			boolean mailCreateError = false;

			// 受信メール一覧
			List<Mail> mails = new ArrayList<Mail>();
			for(Message m : msgs) {
				Mail mail = null;
				try {
					mail = new Mail(m);// Mail形式に変換
				} catch(Exception ex) {
					mailCreateError = true;
					org.writeLog(getLogTitle("received mail create 'Mail' instance failed."), ex);
					// 予備対応(おそらく無駄だろうけど...)
					try {
						Mail tmpMail = new Mail();
						// Fromだけでも取得できるか試す
						if(getFrom(m, tmpMail)) {
							if(isMailWatcher(tmpMail)) {
								if(getMessageid(m, tmpMail)) {
									messageids.add(tmpMail.messageid);
								}
							} else {
								sendErrorMailOfNormal(tmpMail.from);// ユーザへエラー通知
							}
							continue;
						}
						// Subjectだけでも取得できるか試す
						if(getSubject(m, tmpMail)) {
							if(isMailWatcher(tmpMail)) {
								if(getMessageid(m, tmpMail)) {
									messageids.add(tmpMail.messageid);
								}
								continue;//監視メールなのでシステムエラメはしない
							}
						}
						// Textだけでも取得できるか試す
						if(getText(m, tmpMail)) {
							if(isMailWatcher(tmpMail)) {
								if(getMessageid(m, tmpMail)) {
									messageids.add(tmpMail.messageid);
								}
								continue;//監視メールなのでシステムエラメはしない
							}
						}
					} catch(Exception ex2) {
						org.writeLog(getLogTitle("send error mail of user failed."), ex);
					}
					continue;
				}

				messageids.add(mail.messageid);	// 削除するのに必要
				mails.add(mail);// 処理用に格納
			}

			// 返信処理(一度返さないと次が呼ばれないっぽい？ので別スレッドで)
			new Thread(new Runnable() {
				@Override
				public void run() {
					for(Mail mail : mails) {
						// 通常メールであれば返信処理を行う
						if(!isWatchersMail(mail, true) && !isNoReplyMail(mail)) {
							org.writeLog(getLogTitle("received mail processing now."), "MessageID: "+mail.messageid, true);
							//返信処理結果
							boolean mailDelete = onReturnMail(mail);
							if(!mailDelete) {
								// 何らかの問題が発生したことにより、メール削除フラグから外す(メールを残す)
								messageids.remove(mail.messageid);
							}
						}
					}
					// 削除
					onReturnMailDelete(messageids);
				}
			}).start();

			// message読み込み中にエラーが発生したので再起動する
			//
			// [2016-09-13][r-kubo]
			// Messageインスタンスから各種の値を取ろうとしたこのタイミングで、
			// すでにIDLE状態は解除されており(ログ：Terminating connection.Please try again.)、
			// そのせいか待機スレッド(本クラスのrun())内のIMAPFolder#idle()からは何も(例外が)発せられず保持されたままとなる。
			// そのため終了を検知できず無放置状態となる。
			// ※おそらく引数のMessageCountEventはidle()を行っているIMAPFolderの一部で、
			// 例外通知はidleもしくはリスナーに対して一回しか行わないのだと思われる。(idle()でも発行してくれればいいのに...)
			// >> 対策：リスナー内であっても例外が発生した場合は再起動する
			if(mailCreateError) {
				org.writeLog("receive mail process error.", "restart now.", true);
				restart();
			}
		}

		private boolean getFrom(Message m, Mail mail) {
			if(m == null || mail == null) return false;
			try {
				mail.from = Mail.getFrom(m);
				return true;
			} catch(Exception ex) {
				return false;
			}
		}
		private boolean getSubject(Message m, Mail mail) {
			if(m == null || mail == null) return false;
			try {
				String subject = Mail.getSubject(m);
				mail.subject = subject == null ? "" : subject;
				return true;
			} catch(Exception ex) {
				return false;
			}
		}
		private boolean getText(Message m, Mail mail) {
			if(m == null || mail == null) return false;
			try {
				String text = Mail.getText(m);
				mail.text = text == null ? "" : text;
				return true;
			} catch(Exception ex) {
				return false;
			}
		}
		private boolean getMessageid(Message m, Mail mail) {
			if(m == null || mail == null) return false;
			try {
				mail.messageid = Mail.getMessageid(m);
				return true;
			} catch(Exception ex) {
				return false;
			}
		}
	};

	private boolean isMailWatcher(Mail mail) {
		try {
			if(mail.from.equals(MailInfo.WATCHER_ADDRESS)) return true;
			if(mail.subject.equals(MailInfo.WATCHER_TITLE)) return true;
			if(mail.text.contains(MailInfo.WATCHER_PART_TEXT)) return true;
		} catch(Exception ex) {
			return false;
		}
		return false;
	}

	/**
	 * メールが監視用かどうかを返します。<br>
	 * またメールが監視用メールの場合はMailWatcherに受信時刻を保存し、送信ボックスから削除します。
	 * @param mail
	 * @param delete
	 */
	private boolean isWatchersMail(Mail mail, boolean delete) {
		if(isMailWatcher(mail)) {
			// 受信時刻を記録
			if(!delete) return true;
			try {
				// 今回の監視用メールを送信ボックスから削除
				String sentMid = mail.messageid;
				Thread delThWM = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							List<String> sentList = new ArrayList<String>();
							sentList.add(sentMid);
							Mail.deleteSentMail(MailInfo.WATCHER_RECEIVE_HOST, MailInfo.WATCHER_RECEIVE_PORT,
									MailInfo.WATCHER_USER, MailInfo.WATCHER_PASSWORD, sentList);
						} catch (Exception ex) {
							org.writeLog(getLogTitle("watcher's mail delete failed."), ex);
						}
					}
				});
				delThWM.start();
			} catch (Exception ex) {
				org.writeLog(getLogTitle("exception in the mailwatcher's processing."), ex);
			}
			return true;
		}
		return false;
	}

	/**
	 * このメールが返信不可メールかどうかを確認します。
	 * @param mail
	 * @return
	 */
	private boolean isNoReplyMail(Mail mail) {
		if(mail == null) return false;
		if(mail.from == null) return false;
		String address = mail.from;
		String[] checkArgs = {"noreply", "no-reply", "mailer-daemon"};
		for(String arg : checkArgs) {
			if(address.contains(arg)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 引数のメールに対する返信処理を行います。
	 * @param mail
	 * @return true:受信メール削除、false:受信メール保存
	 */
	public abstract boolean onReturnMail(Mail mail);

	/**
	 * メールの削除を行います。
	 * @param messsageids
	 */
	public abstract void onReturnMailDelete(List<String> messsageids);

	/**
	 * 受信メールを保存します。
	 * @param mail
	 * @param oid
	 * @param uid
	 * @return
	 */
	public Result saveReceiveMail(Mail mail, int oid, int uid) {
		MailReceive mr = new MailReceive();
		mr.setOid(oid);
		mr.setUid(uid);
		mr.setMailadd(mail.from);
		mr.setMessageid(mail.messageid);
		mr.setSubject(mail.subject);
		mr.setText(mail.text);
		mr.setReceivedate(mail.date);
		mr.setReceivetime(mail.time);
		return MailReceive.INSERT(mr);
	}

	/**
	 * 送信メールを保存します。
	 * @param receiveMessageid
	 * @param to
	 * @param sendMail
	 * @param uid
	 * @return
	 */
	public Result saveSendMail(String receiveMessageid, String to, Mail sendMail, int uid) {
		String mid = receiveMessageid == null ? "" : receiveMessageid;
		MailSend ms = new MailSend();
		ms.setReceivemid(mid);
		ms.setOid(oid);
		ms.setUid(uid);
		ms.setMailadd(to);
		ms.setSubject(sendMail.subject);
		ms.setText(sendMail.text);
		ms.setSenddate(sendMail.date);
		ms.setSendtime(sendMail.time);
		return MailSend.INSERT(ms);
	}

	/**
	 * メールを送信します。
	 * @param to
	 * @param subject
	 * @param text
	 * @return
	 * @throws MessagingException
	 */
	public Mail sendMail(String to, String subject, String text) throws MessagingException {
		Mail sendMail = Mail.send(
				smtphost,
				String.valueOf(smtpport),
				mailaddress,
				password,
				mailaddress,
				to,
				subject,
				text
		);
		return sendMail;
	}

	/**
	 * **************<br>
	 * メールの受付でエラーが発生しました。以下の理由により正常に受け付けられませんでした。<br>
	 * <br>
	 * データ照合中にエラーが発生しました。<br>
	 * 再度送信することで解決する場合があります。解決しない場合は担当者までお問い合わせ下さい。<br>
	 * <br>
	 * 日付時刻<br>
	 * 組織名<br>
	 * **************<br>
	 * @param to
	 * @return
	 * @throws MessagingException
	 */
	public Mail sendErrorMailOfNormal(String to) throws MessagingException {
		String subject = "メール受付エラー";
		String text = "メールの受付でエラーが発生しました。以下の理由により正常に受け付けられませんでした。\r\n"
				+ "\r\n"
				+ "データ照合中にエラーが発生しました。\r\n"
				+ "再度送信することで解決する場合があります。解決しない場合は担当者までお問い合わせ下さい。\r\n"
				+ "\r\n"
				+ Util.GetNowDateTime() + "\r\n"
				+ oname;
		return sendMail(to, subject, text);
	}

	/**
	 * **************<br>
	 * メールの受付でエラーが発生しました。以下の理由により正常に受け付けられませんでした。<br>
	 * <br>
	 * ・ユーザ登録が行われていない<br>
	 * ・メールアドレスが登録されていない、もしくは異なるメールアドレスが登録されている<br>
	 * ・データ照合中にエラーが発生した<br>
	 * <br>
	 * 打刻を希望する場合は担当者に確認を行って下さい。<br>
	 * <br>
	 * 日付時刻<br>
	 * 組織名<br>
	 * **************<br>
	 * @param to
	 * @return
	 * @throws MessagingException
	 */
	public Mail sendErrorMailOfUid(String to) throws MessagingException {
		String subject = "メール受付エラー";
		String text = "メールの受付でエラーが発生しました。以下の理由により正常に受け付けられませんでした。\r\n"
				+ "\r\n"
				+ "・ユーザ登録が行われていない\r\n"
				+ "・メールアドレスが登録されていない、もしくは異なるメールアドレスが登録されている\r\n"
				+ "・データ照合中にエラーが発生した\r\n"
				+ "\r\n"
				+ "打刻を希望する場合は担当者に確認を行って下さい。\r\n"
				+ "\r\n"
				+ Util.GetNowDateTime() + "\r\n"
				+ oname;
		return sendMail(to, subject, text);
	}

	/**
	 * **************<br>
	 * メールの受付でエラーが発生しました。以下の理由により正常に受け付けられませんでした。<br>
	 * <br>
	 * ・メールでの打刻が許可されていない<br>
	 * <br>
	 * 打刻を希望する場合は担当者に確認を行って下さい。<br>
	 * <br>
	 * 日付時刻<br>
	 * 組織名<br>
	 * **************<br>
	 * @param to
	 * @return
	 * @throws MessagingException
	 */
	public Mail sendErrorMailOfMailDisable(String to) throws MessagingException {
		String subject = "メール受付エラー";
		String text = "メールの受付でエラーが発生しました。以下の理由により正常に受け付けられませんでした。\r\n"
				+ "\r\n"
				+ "・メールでの打刻が許可されていない\r\n"
				+ "\r\n"
				+ "打刻を希望する場合は担当者に確認を行って下さい。\r\n"
				+ "\r\n"
				+ Util.GetNowDateTime() + "\r\n"
				+ oname;
		return sendMail(to, subject, text);
	}

}
