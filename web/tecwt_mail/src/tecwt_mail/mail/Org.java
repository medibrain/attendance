package tecwt_mail.mail;

import tecwt_mail.util.Log2;

public abstract class Org {

	// organization
	protected String key;
	protected int oid;
	protected String oname;

	// mail
	private boolean mailreceive;
	protected String imaphost;
	protected int imapport;
	protected String smtphost;
	protected int smtpport;
	protected String mailaddress;
	protected String password;

	// timecard
	private boolean timecardwatch;

	// log
	protected Log2 log;


	public Org(
			String key, int oid, String oname,
			boolean mailreceive, boolean timecardwatch,
			String mailaddress, String password,
			String imaphost, int imapport, String smtphost, int smtpport) {
		this.key = key;
		this.oid = oid;
		this.oname = oname;
		this.mailreceive = mailreceive;
		this.timecardwatch = timecardwatch;
		this.mailaddress = mailaddress;
		this.password = password;
		this.imaphost = imaphost;
		this.imapport = imapport;
		this.smtphost = smtphost;
		this.smtpport = smtpport;
		this.log = new Log2(this.key);
	}

	/**
	 * 独自実装したMailReceiverを返します。
	 * @return
	 */
	public abstract MailReceiver getReceiver();

	/**
	 * 独自実装したTimecardWatcherを返します。
	 * @return
	 */
	public abstract TimecardWatcher getWatcher();

	/**
	 * ログ出力インスタンスを返します。
	 * @return
	 */
	public Log2 getLog() {
		return this.log;
	}

	/**
	 * 時刻、組織名、スレッド名を乗せたログを記述します。<br>
	 * titleは1行目の最後に書き込まれます。<br>
	 * textはnullでない場合2行目以降に書き込まれます。<br>
	 * itIsIgnoreValueは無視される値です。他のwriteLogメソッドと区別するためのものです。<br>
	 * sample: <br>
	 * [yyyy-MM-dd hh:mm:ss] &lt;Organization name>&lt;Thread name> title<br>
	 * text
	 * @param title
	 * @param text
	 */
	public void writeLog(String title, String text, boolean itIsIgnoreValue) {
		String mark = "<"+this.key+"><"+Thread.currentThread().getName()+">";
		this.log.write(mark + title, text, true);
	}

	/**
	 * 時刻、組織名、スレッド名を乗せたログを記述します。<br>
	 * titleは1行目の最後に書き込まれます。<br>
	 * textはnullでない場合2行目以降に書き込まれます。<br>
	 * sample: <br>
	 * [yyyy-MM-dd hh:mm:ss] &lt;Organization name>&lt;Thread name> title<br>
	 * ex
	 * @param title
	 * @param ex
	 */
	public void writeLog(String title, Exception ex) {
		String mark = "<"+this.key+"><"+Thread.currentThread().getName()+">";
		this.log.write(mark + title, ex);
	}

	/**
	 * 時刻、組織名、スレッド名を乗せたログを記述します。<br>
	 * titleは1行目の最後に書き込まれます。<br>
	 * textはnullでない場合2行目以降に書き込まれます。<br>
	 * sample: <br>
	 * [yyyy-MM-dd hh:mm:ss] &lt;Organization name>&lt;Thread name> title<br>
	 * ex<br>
	 * text
	 * @param title
	 * @param ex
	 * @param text
	 */
	public void writeLog(String title, Exception ex, String text) {
		String mark = "<"+this.key+"><"+Thread.currentThread().getName()+">";
		this.log.write(mark + title, ex, text);
	}

	/**
	 * MailReceiverの有効/無効を設定します。
	 * @param enable
	 */
	public void setReceiverEnable(boolean enable) {
		mailreceive = enable;
	}

	/**
	 * MailReceiverの有効/無効を返します。
	 * @return
	 */
	public boolean isReceiverEnable() {
		return mailreceive;
	}

	/**
	 * TimecardWatcherの有効/無効を設定します。
	 * @param enable
	 */
	public void setWatcherEnable(boolean enable) {
		timecardwatch = enable;
	}

	/**
	 * TimecardWatcherの有効/無効を返します。
	 * @return
	 */
	public boolean isWatcherEnable() {
		return timecardwatch;
	}



	/**
	 * ログに記載し、システムエラーメールを送信します。
	 * @param mail date,time,messageidのみ使用します
	 * @param subject
	 * @param result
	 */
	/*
	public void doErrorCall(Mail mail, String subject, Result result) {
		SendSystemErrorMail(subject, result.getMessage());
		if(mail == null) {
			Util.WriteLog(0, 0, "", result.getMessage());
		} else {
			Util.WriteLog(mail.date, mail.time, mail.messageid, result.getMessage());
		}
	}
	*/
	/**
	 * ログに記載し、システムエラーメールを送信します。
	 * @param mail date,time,messageidのみ使用します
	 * @param subject
	 * @param result
	 */
	/*
	public void doErrorCall(Mail mail, String subject, Exception ex) {
		String cause = Util.GetErrorMessage(ex);
		SendSystemErrorMail(subject, cause);
		if(mail == null) {
			Util.WriteLog(0, 0, "", cause);
		} else {
			Util.WriteLog(mail.date, mail.time, mail.messageid, cause);
		}
	}
	*/
	/**
	 * ログに記載し、システムエラーメールを送信します。
	 * @param mail date,time,messageidのみ使用します
	 * @param subject
	 * @param result
	 */
	/*
	public void doErrorCall(Mail mail, String subject, String text) {
		SendSystemErrorMail(subject, key+"\r\n"+text);
		if(mail == null) {
			Util.WriteLog(0, 0, "", text);
		} else {
			Util.WriteLog(mail.date, mail.time, mail.messageid, text);
		}
	}
	*/
	/**
	 * システムエラーメールを送信します。
	 * @param subject
	 * @param text
	 */
	public void SendSystemErrorMail(String subject, String text, boolean itIsIgnoreValue) {
		try {
			Mail.send(
					smtphost,
					String.valueOf(smtpport),
					mailaddress,
					password,
					mailaddress,
					MailManager.getSystemErrorMailaddress(),
					subject,
					text
			);
		} catch (Exception ex) {
			String s = "";
			s += "========\n";
			s += "subject: " + subject + "\n";
			s += text + "\n";
			s += "========";
			writeLog("<"+this.getClass().getSimpleName()+"> SendSystemErrorMail failed.", ex, s);
		}
	}

	/**
	 * システムエラーメールを送信します。
	 * @param subject
	 * @param text
	 */
	public void SendSystemErrorMail(String subject, Exception ex) {
		try {
			Mail.send(
					smtphost,
					String.valueOf(smtpport),
					mailaddress,
					password,
					mailaddress,
					MailManager.getSystemErrorMailaddress(),
					subject,
					Log2.getStackTrace(ex)
			);
		} catch (Exception ex2) {
			String s = "";
			s += "========\n";
			s += "subject: " + subject + "\n";
			s += Log2.getStackTrace(ex) + "\n";
			s += "========";
			writeLog("<"+this.getClass().getSimpleName()+"> SendSystemErrorMail failed.", ex2, s);
		}
	}

	/**
	 * システムエラーメールを送信します。
	 * @param subject
	 * @param text
	 */
	public void SendSystemErrorMail(String subject, Exception ex, String text) {
		try {
			Mail.send(
					smtphost,
					String.valueOf(smtpport),
					mailaddress,
					password,
					mailaddress,
					MailManager.getSystemErrorMailaddress(),
					subject,
					Log2.getStackTrace(ex) + text
			);
		} catch (Exception ex2) {
			String s = "";
			s += "========\n";
			s += "subject: " + subject + "\n";
			s += Log2.getStackTrace(ex) + text + "\n";
			s += "========";
			writeLog("<"+this.getClass().getSimpleName()+"> SendSystemErrorMail failed.", ex2, s);
		}
	}

}
