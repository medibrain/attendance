package tecwt_mail.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

public class Log {

    static String dirName;	// ログ置き場
    static File file;		// 通常ログの書き込み場所

    static {
    	// 現在のパスを取得
        dirName = new File("").getAbsolutePath();
        if (dirName.equals(File.separator)) {
        	// rootなら書き込みたいディレクトリまでのパスを記述
            dirName = "/usr/local/tomcat8/webapps/logs/tecwt_mail";
        } else {
        	// rootじゃないなら直下に
            dirName += File.separator + "logs/tecwt_mail";	// テスト環境用
        }
        // 通常ログの記載場所
        file = new File(dirName + File.separator + "log.log");
    }

    /**
     * エラーログを書き込みます。<br>
     * すべて同じ場所に追記されます。
     * @param str
     */
    public static void Write(String str) {
        createLogDirs();
        try {
        	if(!createLogFile(file)) return;
            FileWriter filewriter = new FileWriter(file, true);
            filewriter.write(str + "\r\n\r\n");
            filewriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void exceptionWrite(Exception e) {
    	e.printStackTrace();
        try {
	        StringWriter sw = new StringWriter();
	        PrintWriter pw = new PrintWriter(sw);
	        e.printStackTrace(pw);
	        String trace = sw.toString();
			sw.close();
	        pw.close();

	        Log.Write(trace);
		} catch (Exception e1) {
			e.printStackTrace();
		}
    }

    private static void createLogDirs() {
        File f = new File(dirName);
        if (!f.exists()) {
            f.mkdirs();
        }
    }

    private static boolean createLogFile(File file) throws IOException {
    	if(!file.exists()) {
    		return file.createNewFile();
    	}
    	return true;
    }
}
