package tecwt_mail.util;

import java.util.Calendar;
import java.util.Date;

// Web側のソースを流用するにあたり、そのまま持ってこれないようなデータを
// ブリッジする為のデータをまとめたところ

public class BridgeWeb {
	public static class DBinfo {
		public static final String TABLE_TIMECARD = "timecard";
		public static final String TABLE_USERS = "users";
		public static final String TABLE_TCLOG = "tclog";
		// 日毎の作業実績
		public static final String TABLE_DAILY_WORK_RESULT = "daily_work_result";
	}

	public enum ErrorMessage {
	    timecard_out07("緯度が指定されていません"),
	    timecard_out08("経度が指定されていません"),
	    timecard_out09("端末IDが判別できませんでした"),
	    timecard_out10("退勤時間が判別できませんでした"),
	    timecard_out11("タイムカードの日付が本日ではありません"),
	    timecard_out12("指定された退勤時間は15分単位ではありません"),
	    timecard_out13("指定された退勤時間が不明です"),
	    // timecard_out17("交通費が判別できませんでした"),
	    // timecard_out18("休憩時間が判別できませんでした"),
	    timecard_out19("指定された休憩時間は15分単位ではありません"),
	    timecard_out20("退勤記録でエラーが発生しました"),
	    timecard_out21_1("退勤時間が出勤時間を下回ることはできません。時間を確認して下さい"),
	    timecard_out21_2("現在時刻より後の退勤時間は指定できません"),
	    timecard_outex("退勤記録でエラーが発生しました");

	    private final String message;

	    private ErrorMessage(final String message) {
	        this.message = message;
	    }

	    public String getMessage() {
	        return this.message;
	    }
	}

	public static class DateTime {
		/**
		 * yyyyMMddの数字をDateに変換します。失敗した場合、nullを返します。
		 *
		 * @param yyyyMMdd
		 * @return
		 */
		public static Date IntToDate(int yyyyMMdd) {
			Calendar c = Calendar.getInstance();
			try {
				c.set(yyyyMMdd / 10000, yyyyMMdd / 100 % 100, yyyyMMdd % 100);
				setDay0(c);
				c.setLenient(false);
				return c.getTime();
			} catch (Exception e) {
				return null;
			}
		}

		/**
		 * 今日の日付をyyyyMMddのintで取得します
		 *
		 * @return
		 */
		public static int GetTodayInt() {
			Calendar c = Calendar.getInstance();
			int y = c.get(Calendar.YEAR); // 年を取得
			int m = c.get(Calendar.MONTH) + 1; // 月を取得(※1)
			int d = c.get(Calendar.DATE); // 日を取得
			return y * 10000 + m * 100 + d;
		}

		/**
		 * 現在時刻をhhmmのintで取得します
		 *
		 * @return
		 */
		public static int GetNowHHMMInt() {
			Calendar c = Calendar.getInstance();
			int h = c.get(Calendar.HOUR_OF_DAY); // 時を取得
			int m = c.get(Calendar.MINUTE) + 1; // 分を取得(※1)
			return h * 100 + m;
		}

		private static void setDay0(Calendar c) {
			c.set(Calendar.HOUR, 0);
			c.set(Calendar.HOUR_OF_DAY, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0);
		}
	}
}
