package tecwt_mail.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;


public class Xml {

	/**
	 * document(XMLファイル)をfileNameに書き込みます。
	 * @param fileName
	 * @param document
	 * @return null:正常、String:エラーメッセージ
	 */
	public static String write(String fileName, Document document) {
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			transformerFactory.setAttribute("indent-number","4");
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			OutputStreamWriter outputStreamWriter =new OutputStreamWriter(new FileOutputStream(fileName),"utf-8");
			transformer.transform(new DOMSource(document),new StreamResult(outputStreamWriter));
			try {
				outputStreamWriter.close();
			} catch(IOException e){
				// 無視
			}
		} catch (TransformerConfigurationException ex){
			return Util.GetErrorMessage(ex);
		} catch (FileNotFoundException ex){
			return Util.GetErrorMessage(ex);
		} catch (UnsupportedEncodingException ex){
			return Util.GetErrorMessage(ex);
		} catch (TransformerException ex){
			return Util.GetErrorMessage(ex);
		}

		return null;
	}

}
