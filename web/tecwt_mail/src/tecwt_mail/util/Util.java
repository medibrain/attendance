package tecwt_mail.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Util {

	/**
	 * 数字文字列を数値化します。失敗した場合は-1を返します。
	 * @param number
	 * @return
	 */
	public static int ToInt(String number) {
		try {
			return Integer.parseInt(number);
		} catch (Exception ex) {
			return -1;
		}
	}

	/**
	 * 数字文字列を各桁ごとに数値化した配列を返します。
	 * 失敗した場合はnullを返します。
	 * @param number
	 * @return
	 */
	public static int[] ToInts(String number) {
		try {
			int[] ints = new int[number.length()];
			int i = 0;
			for (char c : number.toCharArray()) {
				int conv = ToInt(String.valueOf(c));
				ints[i++] = conv;
			}
			return ints;
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * 現在日付をint型(yyyyMMdd)の形式で取得します。
	 * @return
	 */
	public static int GetNowDateInt() {
		Calendar cal = Calendar.getInstance();
		int date = 0;
		date += cal.get(Calendar.YEAR) * 10000;
		date += (cal.get(Calendar.MONTH) + 1) * 100;
		date += cal.get(Calendar.DAY_OF_MONTH);
		return date;
	}

	/**
	 * 現在時刻をint型(HHmm)の形式で取得します。
	 * @return
	 */
	public static int GetNowTimeInt() {
		Calendar cal = Calendar.getInstance();
		int time = 0;
		time += cal.get(Calendar.HOUR_OF_DAY) * 100;
		time += cal.get(Calendar.MINUTE);
		return time;
	}

	/**
	 * 現在日時をyyyy/MM/dd HH:mmの形式で取得します。
	 * @return
	 */
	public static String GetNowDateTime() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		return sdf.format(date);
	}

	/**
	 * 現在日時をyyyy/MM/dd HH:mm:ssの形式で取得します。
	 * @return
	 */
	public static String GetNowDateTimeFull() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		return sdf.format(date);
	}

	/**
	 * 現在日付をyyyyMMddの形式で取得します。<br>
	 * int化することができます。
	 * @return
	 */
	public static String GetNowDateString() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		return sdf.format(date);
	}

	/**
	 * 現在時刻をHHmmの形式で取得します。<br>
	 * int化することができます。
	 * @return
	 */
	public static String GetNowTimeString() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
		return sdf.format(date);
	}

	/**
	 * ８桁で表された日付(yyyyMMdd)をyyyy/MM/ddの形式で取得します。
	 * @param date
	 * @return
	 */
	public static String GetDateToString(int date) {
		if(date < 10000000) {
			return "";
		}
		StringBuilder sb = new StringBuilder(String.valueOf(date));
		sb.insert(4, "/");
		sb.insert(7, "/");
		return sb.toString();
	}

	/**
	 * 時間(HHmm)をHH:mmの形式で取得します。<br>
	 * 桁数が４ではない場合、0を付与した状態で返します。
	 * @param time
	 * @return
	 */
	public static String GetTimeToString(int time) {
		StringBuilder sb = new StringBuilder(String.valueOf(time));
		if(time == 0) {
			// 0
			sb.insert(0, "00:0");
		} else if(time < 100) {
			// 15,30,45
			sb.insert(0, "00:");
		} else if(time < 1000) {
			// 100～945
			sb.insert(0, "0");
			sb.insert(2, ":");
		} else {
			// 1000～
			sb.insert(2, ":");
		}
		return sb.toString();
	}

	public static void WriteLog(String text) {
		Log.Write(text);
	}

	public static void WriteLogOfTime(String text) {
		StringBuilder sb = new StringBuilder();
		sb.append(GetNowDateTimeFull());
		sb.append("\r\n");
		sb.append(text);
		Log.Write(sb.toString());
	}

	public static void WriteLog(String messageid, String text) {
		StringBuilder sb = new StringBuilder();
		sb.append(GetNowDateTimeFull()).append(" ");
		if(messageid != null) sb.append("messageid:\"").append(messageid).append("\"");
		sb.append("\r\n");
		sb.append(text);
		Log.Write(sb.toString());
	}

	public static String GetErrorMessage(Exception ex) {
		StringBuilder sb = new StringBuilder();
		sb.append(Util.GetNowDateTime()).append("\n");
		Throwable th = ex.getCause();
		sb.append(ex.toString()).append("\n");
		if(th != null) {
			for(StackTraceElement ste : th.getStackTrace()) {
				sb.append(ste.toString()).append("\n");
			}
		} else {
			for(StackTraceElement ste : ex.getStackTrace()) {
				sb.append(ste.toString()).append("\n");
			}
		}
		return sb.toString();
	}

	/**
	 * nullまたは""である場合にtrueを返します。<br>
	 * @param str
	 * @return
	 */
	public static boolean isNullOrBlank(String str) {
		if(str == null) return true;
		if(str.equals("")) return true;
		return false;
	}
}
