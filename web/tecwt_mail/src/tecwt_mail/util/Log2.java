package tecwt_mail.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Log2 {

	private static final String BASE_PATH = "/usr/local/tomcat/webapps/logs/tecwt_mail";
	private static final String DEBUG_LOG_NAME = "debug";
	private static final String INFO_LOG_NAME = "info";
	private static final int DEFAULT_SAVE_DAY = 7;
	private static final String EXTENSION = ".log";
	private static String dirName;

    static {
    	// 現在のパスを取得
        dirName = new File("").getAbsolutePath();
        if (dirName.equals(File.separator)) {
        	// rootなら書き込みたいディレクトリまでのパスを記述
            dirName = BASE_PATH;
        } else {
        	// rootじゃないなら直下に
            dirName += File.separator + "logs/tecwt_mail";	// テスト環境用
        }
        System.out.println(dirName);
    }



    private String oname;
    private String debugLogFolderPath;
    private String infoLogFolderPath;
    private PrintStream debugLogStream;
    private PrintStream infoLogStream;
    private int saveDay;
    private boolean isOpened;

    /**
     * ログファイル作成及び日付管理の機能を持ちます。<br>
     * 引数には組織名を指定します。これはログファイルを保存する単純なフォルダ名やエラー時の識別子に使用されます。
     * @param folderName
     */
    public Log2(String oname) {
    	this.oname = oname;
    	String path = dirName + File.separator + oname + File.separator;
    	this.debugLogFolderPath = path + DEBUG_LOG_NAME;
    	this.infoLogFolderPath = path + INFO_LOG_NAME;
    	this.saveDay = DEFAULT_SAVE_DAY;
    	this.isOpened = true;
    	logRotate();
    }

    /**
     * 保存日数を指定します。デフォルトは一週間です。0以下は使用できません。
     * @param date
     */
    public void setSaveDay(int day) {
    	if(day <= 0) return;
    	logRotate();
    	this.saveDay = day;
    }

    /**
     * 現在ログを出力しているdebugログファイルのPrintStreamを取得します。
     * @return
     */
    public PrintStream getDebugLogStream() {
    	logRotate();
    	return this.debugLogStream;
    }

    /**
     * infoログファイルに書き込みます。<br>
     * titleは時刻と共に１行目に書き込みます。<br>
     * textはnullでない場合、２行目以降に書き込まれます。<br>
     * itIsIgnoreValueは無視されます。<br>
     * このメソッドは同期されます。書き込み中に別の書き込みがされることはありません。
     * @param title
     * @param text
     */
    public synchronized void write(String title, String text, boolean itIsIgnoreValue) {
    	writeText(title, text);
    }

    /**
     * infoログファイルに書き込みます。<br>
     * titleは時刻と共に１行目に書き込みます。<br>
     * exはnullでない場合、２行目以降に書き込まれます。<br>
     * このメソッドは同期されます。書き込み中に別の書き込みがされることはありません。
     * @param title
     * @param ex
     */
    public synchronized void write(String title, Exception ex) {
    	writeText(title, getStackTrace(ex));
    }

    /**
     * infoログファイルに書き込みます。<br>
     * titleは時刻と共に１行目に書き込みます。<br>
     * exはnullでない場合、2行目以降に書き込まれます。<br>
     * textはnullでない場合、ex以降に書き込まれます。<br>
     * このメソッドは同期されます。書き込み中に別の書き込みがされることはありません。
     * @param title
     * @param ex
     */
    public synchronized void write(String title, Exception ex, String text) {
    	if(text == null) {
        	writeText(title, getStackTrace(ex));
    	} else {
        	writeText(title, getStackTrace(ex) + text);
    	}
    }

    private void writeText(String title, String text) {
    	if(!logRotate()) return;
    	//書き込み
    	if(this.infoLogStream != null) {
    		Date date = new Date();
			SimpleDateFormat f2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    		String s = "["+f2.format(date)+"] "+title+" "+(text != null ? "\n" + text : "");
    		this.infoLogStream.println(s);
    		this.infoLogStream.flush();
    	}
    }

    /**
     * infoログ,debugログを開きます。<br>
     * 以後、close()が呼ばれるまでログを記録することができます。
     */
    public void open() {
    	this.isOpened = true;
    	logRotate();
    }

    /**
     * infoログ,debugログの保存を行い、その後ファイルを閉じます。PrintStream#close()<br>
     * 閉じられている間ログは一切記録されません。open()を呼ぶことで再度記録されるようになります。
     */
    public void close() {
    	this.isOpened = false;
    	if(this.debugLogStream != null) {
    		this.debugLogStream.close();
    		this.debugLogStream = null;
    	}
    	if(this.infoLogStream != null) {
    		this.infoLogStream.close();
    		this.infoLogStream = null;
    	}
    }

    private String getCurrentDateString() {
    	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"));
    	return getDateString(cal);
    }

    private String getSaveLimitDateString() {
    	Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Asia/Tokyo"));
    	cal.add(Calendar.DATE, -this.saveDay);
    	return getDateString(cal);
    }

    private static String getDateString(Calendar cal) {
    	SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
    	return f.format(cal.getTime());
    }

    private boolean logRotate() {
    	if(!this.isOpened) return false;
    	//folder
    	createLogDirs();
    	//debug log
    	logRotateDelete(this.debugLogFolderPath);
    	if(this.debugLogStream == null || !checkNowDateFileExist(this.debugLogFolderPath)) {
    		PrintStream dps = getNowDateStream(this.debugLogFolderPath);
    		if(dps != null) {
    			//if(this.debugLogStream != null) this.debugLogStream.close();閉じるタイミングは使用者側で制御する
    			this.debugLogStream = dps;
    		}
    	}
    	//info log
		logRotateDelete(this.infoLogFolderPath);
    	if(this.infoLogStream == null || !checkNowDateFileExist(this.infoLogFolderPath)) {
    		PrintStream ips = getNowDateStream(this.infoLogFolderPath);
    		if(ips != null) {
    			if(this.infoLogStream != null) this.infoLogStream.close();
    			this.infoLogStream = ips;
    		}
    	}
    	return true;
    }

    private void createLogDirs() {
        createDir(this.debugLogFolderPath);
        createDir(this.infoLogFolderPath);
    }

    private boolean createDir(String folderPath) {
        File f = new File(folderPath);
        if (!f.exists()) {
            f.mkdirs();
            return true;
        }
        return false;
    }

    private boolean logRotateDelete(String folderPath) {
    	String filename = getSaveLimitDateString() + EXTENSION;
    	File file = new File(folderPath + File.separator + filename);
		if(file.exists()) {
			file.delete();
			return true;
		}
		return false;
    }

    private PrintStream getNowDateStream(String folderPath) {
    	String filename = getCurrentDateString() + EXTENSION;
    	File file = new File(folderPath + File.separator + filename);
    	if(!file.exists()) {
			//新しい日付のファイルを作成
    		try {
        		file.createNewFile();
    		} catch(IOException ex) {
    			myErrorWrite("["+file.getPath()+"] create new file error.", ex);
    			return null;
    		}
    		//ストリームを返す
    		try {
    			PrintStream ps = new PrintStream(file);
    			return ps;
    		} catch(IOException ex) {
    			myErrorWrite("["+file.getPath()+"] get stream error.", ex);
    			return null;
    		}
    	} else {
    		//すでにある現在の日付のファイルのストリームを返す
    		try {
    			PrintStream ps = new PrintStream(file);
    			return ps;
			} catch(IOException ex) {
				myErrorWrite("["+file.getPath()+"] get now date stream error.", ex);
				return null;
			}
    	}
    }

    private boolean checkNowDateFileExist(String folderPath) {
    	String filename = getCurrentDateString() + EXTENSION;
    	File file = new File(folderPath + File.separator + filename);
    	return file.exists();
    }

	public static String getStackTrace(Exception ex) {
		if(ex == null) return "";
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		ex.printStackTrace(pw);
		pw.flush();
		return sw.toString();
	}

	private void myErrorWrite(String title, Exception ex) {
		ex.printStackTrace();
		write(
				"["+this.oname+"]["+this.getClass().getSimpleName()+"] "+title,
				getStackTrace(ex),
				true);
	}

}
