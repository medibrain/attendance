package tecwt_mail.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tecwt_mail.mail.MailManager;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGetPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGetPost(request, response);
	}

	private void doGetPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字コードを設定
		String encoding = "UTF-8";
		request.setCharacterEncoding(encoding);
		response.setCharacterEncoding(encoding);

		// セッション破棄
		HttpSession session = request.getSession();
		if(session != null) {
			session.invalidate();
		}

		String id = request.getParameter("id");
		String pass = request.getParameter("pass");
		if(id == null || pass == null) {
			// ログイン画面展開
			request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
			return;
		} else {
			// フォーム送信
			if(MailManager.login(id, pass)) {
				HttpSession s = request.getSession();
				s.setAttribute("login", true);
				response.sendRedirect("/tecwt_mail/control");
				return;
			} else {
				request.setAttribute("error", true);
				request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
				return;
			}
		}
	}

}
