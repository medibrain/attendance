package tecwt_mail.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import tecwt_mail.mail.MailManager;

/**
 * Servlet implementation class ControlServlet
 */
public class ControlServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControlServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 画面表示
		doGetPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 画面表示
		doGetPost(request, response);
	}

	private void doGetPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 文字コードを設定
		String encoding = "UTF-8";
		request.setCharacterEncoding(encoding);
		response.setCharacterEncoding(encoding);

		// ログイン確認
		HttpSession session = request.getSession();
		if(session == null || (session.getAttribute("login") != null && !(boolean)session.getAttribute("login"))) {
			response.sendRedirect("/tecwt_mail/login");
			return;
		}

		// 操作の取得
		String cmd = request.getParameter("cmd");
		if(cmd == null) {
			cmd = "";
		}

		// 操作対象の取得
		String org = request.getParameter("org");

		// 操作の結果
		boolean cmdResult = true;

		// 操作
		switch(cmd) {
		case "all-init":
			MailManager.initAllOrg();
			break;
		case "system-set-login-id":
			String loginId = request.getParameter("loginId");
			cmdResult = MailManager.setLoginId(loginId);
			break;
		case "system-set-login-pass":
			String loginPass = request.getParameter("loginPass");
			String loginPassConfirm = request.getParameter("loginPassConfirm");
			cmdResult = false;
			if(loginPass != null && !loginPass.equals("")) {
				if(loginPassConfirm != null && !loginPassConfirm.equals("")) {
					if(loginPass.equals(loginPassConfirm)) {
						cmdResult = MailManager.setLoginPass(loginPass);
					}
				}
			}
			request.setAttribute("system-set-login-pass", "system-set-login-pass");
			break;
		case "system-set-error-mailaddress":
			String mailaddress = request.getParameter("mailaddress");
			cmdResult = MailManager.setSystemErrorMailaddress(mailaddress);
		case "mailwatcher-start":
			cmdResult = MailManager.startMailWatcher();
			break;
		case "mailwatcher-stop":
			cmdResult = MailManager.stopMailWatcher();
			break;
		case "mailreceive-set-enable":
			cmdResult = false;
			if(MailManager.isEnableKey(org)) {
				String mrEnable = request.getParameter("enable");
				cmdResult = MailManager.setMailReceiverEnable(org, Boolean.valueOf(mrEnable));
			}
			break;
		case "mailreceive-all-start":
			MailManager.startAllMailReceiver();
			break;
		case "mailreceive-all-stop":
			MailManager.stopAllMailReceiver();
			break;
		case "log-start":
			MailManager.openLog();
			break;
		case "log-stop":
			MailManager.closeLog();
			break;
		case "mailreceive-start":
			cmdResult = false;
			if(MailManager.isEnableKey(org)) {
				cmdResult = MailManager.startMailReceiver(org);
			}
			break;
		case "mailreceive-stop":
			cmdResult = false;
			if(MailManager.isEnableKey(org)) {
				MailManager.stopMailReceiver(org);
				cmdResult = true;
			}
			break;
		case "timecardwatch-set-enable":
			cmdResult = false;
			if(MailManager.isEnableKey(org)) {
				String twEnable = request.getParameter("enable");
				cmdResult = MailManager.setTimecardWatcherEnable(org, Boolean.valueOf(twEnable));
			}
			break;
		case "timecardwatch-start":
			cmdResult = false;
			if(MailManager.isEnableKey(org)) {
				cmdResult = MailManager.startTimecardWatcher(org);
			}
			break;
		case "timecardwatch-stop":
			cmdResult = false;
			if(MailManager.isEnableKey(org)) {
				MailManager.stopTimecardWatcher(org);
				cmdResult = true;
			}
			break;
		case "timecardwatch-all-start":
			MailManager.startAllTimecardWatcher();
			break;
		case "timecardwatch-all-stop":
			MailManager.stopAllTimecardWatcher();
			break;
		case "timecardwatch-set-schedule":
			cmdResult = false;
			if(MailManager.isEnableKey(org)) {
				String id = request.getParameter("id");
				String m = request.getParameter("minute");
				String h = request.getParameter("hour");
				String d = request.getParameter("day");
				String M = request.getParameter("month");
				String w = request.getParameter("dayOfWeek");
				cmdResult = MailManager.setTimecardWatcherSchedule(org, id, m, h, d, M, w);
			}
			break;
		case "timecardwatch-set-schedule-enable":
			cmdResult = false;
			if(MailManager.isEnableKey(org)) {
				String scheduleId = request.getParameter("id");
				boolean enable = Boolean.valueOf(request.getParameter("enable"));
				cmdResult = MailManager.setTimecardWatcherScheduleEnable(org, scheduleId, enable);
			}
			break;
		default:
			cmdResult = false;
			break;
		}

		// コマンド実行結果
		request.setAttribute("cmdOrg", org);
		request.setAttribute("cmdResult", cmdResult);

		// エラーメッセージ
		request.setAttribute("exceptionExist", MailManager.isFuncedException());
		request.setAttribute("exceptions", MailManager.getExceptionsToHtmlText());

		// ログイン情報
		request.setAttribute("loginId", MailManager.getLoginId());
		request.setAttribute("loginPass", MailManager.getLoginPass());

		// エラー発生時送信先アドレス
		request.setAttribute("system-error-mailaddress", MailManager.getSystemErrorMailaddress());

		// メール受信監視スレッド状態記載
		request.setAttribute("watcher", MailManager.isMailWatcherIdle());

		// 全組織の管理状況記載
		List<OrgStatusHolder> osHolders = new ArrayList<OrgStatusHolder>();
		for(String key : MailManager.getAllKey()) {
			OrgStatusHolder osHolder = new OrgStatusHolder();
			osHolder.org = key;
			osHolder.oname = MailManager.getOrgName(key);
			osHolder.mailreceiveEnable = MailManager.isMailReceiverEnable(key);
			osHolder.mailreceiveIdle = MailManager.isMailReceiverIdle(key);
			osHolder.timecardwatchEnable = MailManager.isTimecardWatcherEnable(key);
			osHolder.timecardwatchIdle = MailManager.isTimecardWatcherIdle(key);
			Map<String, String[]> twSchedules = new HashMap<String, String[]>();
			Map<String, Boolean> twScheduleEnables = new HashMap<String, Boolean>();
			for(String twId : MailManager.getTimecardWatcherScheduleId(key, false)) {
				twSchedules.put(twId, MailManager.getTimecardWatcherSchedule(key, twId));
				twScheduleEnables.put(twId, MailManager.isTimecardWatcherScheduleEnable(key, twId));
			}
			osHolder.timecardwatchSchedules = twSchedules;
			osHolder.timecardwatchScheduleEnables = twScheduleEnables;
			osHolders.add(osHolder);
		}
		request.setAttribute("org-all-status", osHolders);

		// 描画
		request.getRequestDispatcher("WEB-INF/main.jsp").forward(request, response);
	}

	public class OrgStatusHolder {
		public String org;
		public String oname;
		public boolean mailreceiveEnable;
		public boolean mailreceiveIdle;
		public boolean timecardwatchEnable;
		public boolean timecardwatchIdle;
		public Map<String, String[]> timecardwatchSchedules;
		public Map<String, Boolean> timecardwatchScheduleEnables;
	}

}
