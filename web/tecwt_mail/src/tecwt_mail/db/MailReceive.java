package tecwt_mail.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tecwt_mail.db.DBConnect.SQLResult;

public class MailReceive {

	/**********
	 * カラム *
	 **********/

	private int id;
	private String messageid;
	private int oid;
	private int uid;
	private String mailadd;
	private String subject;
	private String text;
	private int receivedate;
	private int receivetime;


	/******************
	 * setter, getter *
	 ******************/

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMessageid() {
		return messageid;
	}
	public void setMessageid(String messageid) {
		this.messageid = messageid;
	}
	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getMailadd() {
		return mailadd;
	}
	public void setMailadd(String mailadd) {
		this.mailadd = mailadd;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getReceivedate() {
		return receivedate;
	}
	public void setReceivedate(int receivedate) {
		this.receivedate = receivedate;
	}
	public int getReceivetime() {
		return receivetime;
	}
	public void setReceivetime(int receivetime) {
		this.receivetime = receivetime;
	}


	/*******
	 * SQL *
	 *******/

	/**
	 * INSERTを実行します。
	 * @param mail
	 * @return
	 */
	public static Result INSERT(MailReceive mail) {

		Result result = new Result();
		result.setSuccess(false);

		// INSERT文作成
		String sql = String.format(
				"INSERT INTO mail_receive "
				+ "(messageid, oid, uid, mailadd, subject, text, receivedate, receivetime) "
				+ "VALUES ('%s', %d, %d, '%s', '%s', '%s', %d, %d)",
				mail.getMessageid(),
				mail.getOid(),
				mail.getUid(),
				mail.getMailadd(),
				mail.getSubject(),
				mail.getText(),
				mail.getReceivedate(),
				mail.getReceivetime()
				);

		try {
			// 実行
			DBConnect dbc = new DBConnect(sql, true);
			SQLResult sqlResult = dbc.execNoSELECT();

			// 成功判定
			if(sqlResult.isSuccess) {
				result.setSuccess(true);
				result.addCount(sqlResult.resultCounts.get(0));
			} else {
				result.setMessage(sqlResult.message);
			}

		} catch(SQLException ex) {
			ex.printStackTrace();
			result.setMessage(ex.getMessage());
		}

		return result;
	}

	/**
	 * DELETEします。
	 * @param id
	 * @return
	 */
	public static Result DELETE(int id) {

		Result result = new Result();
		result.setSuccess(false);

		// DELETE文作成
		String sql = String.format("DELETE FROM mail_receive WHERE id=%d", id);

		try {
			// 実行
			DBConnect dbc = new DBConnect(sql, true);
			SQLResult sqlResult = dbc.execNoSELECT();

			// 成功判定
			if(sqlResult.isSuccess) {
				result.setSuccess(true);
				result.addCount(sqlResult.resultCounts.get(0));
			} else {
				result.setMessage(sqlResult.message);
			}

		} catch(SQLException ex) {
			ex.printStackTrace();
			result.setMessage(ex.getMessage());
		}

		return result;
	}

	/**
	 * messageidに該当すればtrueを格納して返します。
	 * @param messageid
	 * @return
	 */
	public static Result matchMessageid(String messageid) {

		final Result result = new Result();
		result.setSuccess(false);

		// SELECT文作成
		String sql = String.format(
				"SELECT EXISTS (SELECT * FROM mail_receive WHERE messageid='%s')",
				messageid
				);
		try {
			// 実行
			DBConnect dbc = new DBConnect(sql, true);
			SQLResult sqlResult = dbc.execSELECT(new DBConnect.ResultCallback() {

				@Override
				public void onResult(ResultSet resultSet) throws SQLException {
					if(resultSet.next()) {
						boolean exists = resultSet.getBoolean("exists");
						result.setBool(exists);
					}
				}
			});

			// 成功判定
			if(sqlResult.isSuccess) {
				result.setSuccess(true);
			} else {
				result.setMessage(sqlResult.message);
			}

		} catch(SQLException ex) {
			ex.printStackTrace();
			result.setMessage(ex.getMessage());
		}

		return result;
	}

	public static Result FindOfMailadd(String mailadd) {
		String sql = String.format(
				"SELECT * FROM mail_receive WHERE mailadd='%s' ORDER BY receivedate DESC, receivetime DESC",
				mailadd);
		return Execute(sql);
	}

	private static Result Execute(String sql) {
		final Result result = new Result();
		result.setSuccess(false);

		try {

			// 実行
			DBConnect dbc = new DBConnect(sql, false);
			SQLResult sqlResult = dbc.execSELECT(new DBConnect.ResultCallback() {

				@Override
				public void onResult(ResultSet resultSet) throws SQLException {
					List<MailReceive> list = new ArrayList<MailReceive>();
					MailReceive mailReceive;
					while(resultSet.next()) {
						mailReceive = new MailReceive();
						mailReceive.setId(resultSet.getInt("id"));
						mailReceive.setOid(resultSet.getInt("oid"));
						mailReceive.setUid(resultSet.getInt("uid"));
						mailReceive.setMessageid(resultSet.getString("messageid"));
						mailReceive.setMailadd(resultSet.getString("mailadd"));
						mailReceive.setSubject(resultSet.getString("subject"));
						mailReceive.setText(resultSet.getString("text"));
						mailReceive.setReceivedate(resultSet.getInt("receivedate"));
						mailReceive.setReceivetime(resultSet.getInt("receivetime"));
						list.add(mailReceive);
					}
					if(list.size() > 0) {
						result.setBool(true);	// 合致するもの発見
					}
					result.setList(list);
				}
			});

			// 成功確認
			if(sqlResult.isSuccess) {
				result.setSuccess(true);
			} else {
				result.setMessage(sqlResult.message);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
			result.setMessage(ex.getMessage());
		}

		return result;
	}

}
