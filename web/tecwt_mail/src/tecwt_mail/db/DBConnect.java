package tecwt_mail.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

public class DBConnect {


	public static DataSource datasource;

	static {
		PoolProperties p = new PoolProperties();

		// 接続情報の設定
		//TODO: medi-kintai
		//p.setUrl("jdbc:postgresql://160.16.75.99:5432/tecwt");
		//p.setDriverClassName("org.postgresql.Driver");
		//p.setUsername("tecadmin");
		//p.setPassword("Tec242-Post");
		p.setUrl("jdbc:postgresql://133.167.104.74:5432/tecwt");
		p.setDriverClassName("org.postgresql.Driver");
		p.setUsername("postgres");
		p.setPassword("NiCE2018");

		// その他オプション
		p.setJmxEnabled(true);						// プールをConncectionPoolMBeanとして登録
		p.setTestWhileIdle(true);					// アイドル状態のコネクションの有効チェックを行う
		p.setMinEvictableIdleTimeMillis(3000);		// アイドル状態のコネクションの生存期間(ミリ秒)
		p.setTimeBetweenEvictionRunsMillis(30000);	// アイドル状態の接続チェックを行う間隔(ミリ秒)
		p.setTestOnBorrow(true);					// プール貸出時に接続有効チェックを行う
		p.setTestOnReturn(true);					// プール返却時に接続有効チェックを行う
		p.setValidationQuery("SELECT 1");			// このSQL文を利用して接続有効チェックを行う
		p.setValidationInterval(30000);				// 指定秒数、前回のチェックから経過しているコネクションは有効チェックする
		p.setInitialSize(5);						// プール起動時に作成されるコネクション数
		p.setMaxActive(100);						// 最大保持コネクション数
		p.setMinIdle(5);							// 最小保持コネクション数=initialSize
		p.setMaxWait(1000);							// 利用可能なコネクションがない場合に待機する時間(ミリ秒)
		p.setRemoveAbandoned(true);					// close漏れ検査
		p.setLogAbandoned(true);					// close漏れがある場合はログ出力
		p.setRemoveAbandonedTimeout(60);			// close漏れとみなすまでの時間(秒)
		p.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"
				+ "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");

		datasource = new DataSource();
		datasource.setPoolProperties(p);
	}



	private Connection conn;
	private List<String> sqlList;
	private boolean tran;

	/**
	 * 単一のSQL文を実行できます。<br>
	 * @param sql
	 * @param transaction
	 * @throws SQLException
	 */
	public DBConnect(String sql, boolean transaction) throws SQLException {
		this.tran = transaction;
		this.sqlList = new ArrayList<String>();
		this.sqlList.add(sql);
		this.open();
		if(transaction) {
			this.conn.setAutoCommit(false);
		}
	}

	/**
	 * 複数のSQL文を一度のコネクションで実行できます。<br>
	 * @param sqlList
	 * @param transaction
	 * @throws SQLException
	 */
	public DBConnect(List<String> sqlList, boolean transaction) throws SQLException {
		this.tran = transaction;
		this.sqlList = sqlList;
		this.open();
		if(transaction) {
			this.conn.setAutoCommit(false);
		}
	}

	private void open() throws SQLException {
		this.conn = datasource.getConnection();
	}

	/**
	 * 複数のSELECT文を一度のコネクションで実行できます。
	 * @param callback
	 * @return
	 */
	public SQLResult execSELECT(ResultCallback callback) {

		SQLResult result = new SQLResult();
		result.isSuccess = false;

		Statement statement;
		try {

			ResultSet resultSet;
			for(String sql : sqlList) {
				// 実行
				statement = this.conn.createStatement();
				resultSet = statement.executeQuery(sql);
				// コールバック
				if(callback != null) {
					callback.onResult(resultSet);
				}
				// 解放
				resultSet.close();
				statement.close();
			}

			// 問題なければコミット
			if(this.tran) {
				this.conn.commit();
			}

			result.isSuccess = true;

		} catch (SQLException ex) {
			// 問題あればロールバック
			if(this.tran) {
				try {
					this.conn.rollback();
				} catch (SQLException ex2) {
					// ロールバックすら無理なら何もしない
					ex2.printStackTrace();
				}
			}
			ex.printStackTrace();
			result.message = ex.getMessage();
		} finally {
			try {
				if(this.conn != null) {
					this.conn.setAutoCommit(true);
					this.conn.close();
				}
			} catch(SQLException ex) {
				ex.printStackTrace();
				result.message = ex.getMessage();
			}
		}

		return result;
	}


	/**
	 * INSERT,UPDATE,DELETEを実行します。
	 * @return
	 */
	public SQLResult execNoSELECT() {
		return executeSQL();
	}
	private SQLResult executeSQL() {

		SQLResult result = new SQLResult();
		result.isSuccess = false;
		List<Integer> resultCounts = new ArrayList<Integer>();

		Statement statement;
		try {

			// 登録されているSQL全部実行
			int count;
			for(int i=0; i<sqlList.size(); i++) {
				statement = this.conn.createStatement();
				count = statement.executeUpdate(sqlList.get(i));
				resultCounts.add(count);
				statement.close();
			}

			// 問題なければコミット
			if(this.tran) {
				this.conn.commit();
			}

			result.isSuccess = true;
			result.resultCounts = resultCounts;

		} catch (SQLException ex) {
			// 問題あればロールバック
			if(this.tran) {
				try {
					this.conn.rollback();
				} catch (SQLException ex2) {
					// ロールバックすら無理なら何もしない
					ex2.printStackTrace();
				}
			}
			ex.printStackTrace();
			result.message = ex.getMessage();
		} finally {
			try {
				if(this.conn != null) {
					this.conn.setAutoCommit(true);
					this.conn.close();
				}
			} catch(SQLException ex) {
				ex.printStackTrace();
				result.message = ex.getMessage();
			}
		}

		return result;
	}

	/**
	 * isSuccess: true=成功、false=エラー<br>
	 * message: エラー文(isSuccessがfalseの場合のみ)<br>
	 * resultCounts: SQL実行結果件数のリスト<br>
	 */
	public class SQLResult {
		public boolean isSuccess;
		public String message;
		public List<Integer> resultCounts;
	}

	/**
	 * SELECT文の実行結果を返します。
	 */
	public interface ResultCallback {
		public abstract void onResult(ResultSet resultSet) throws SQLException;
	}

}
