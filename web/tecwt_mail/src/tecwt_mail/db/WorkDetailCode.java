package tecwt_mail.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class WorkDetailCode {
	private static final String userTable = "users";
	private static final String workDetailTable = "work_detail";

	public static class MatchResult {
		public Integer workDetailID = null;
		public String[] names = null;
	}

	public static MatchResult exists(int oid, int uid, String inputName) {

		DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();

		// 全件取ってアプリ側で一致をチェック
		String sql = "SELECT work_detail.work_detail_id, work_detail.name "
			+ "FROM " + userTable + " AS users "
			+ "INNER JOIN " + workDetailTable + " AS work_detail "
			+ "ON users.department = work_detail.department_id AND work_detail.enable = TRUE "
			+ "WHERE users.oid = ? AND users.uid = ? ORDER BY work_detail.display_order";

		MatchResult result = new MatchResult();
        try {
    		List<String> list = new ArrayList<String>();

            ResultSet rs = db.execQuery(sql, Arrays.asList(oid, uid));
    		while(rs.next()) {
				String name = rs.getString("name");
				if (name.equals(inputName)) {
					result.workDetailID = rs.getInt("work_detail_id");
					return result;
				}
                list.add(name);
            }
    		result.names = list.toArray(new String[list.size()]);
    		return result;
        } catch (SQLException e) {
			e.printStackTrace();
			return result;
        } finally {
            db.close();
        }
	}

}
