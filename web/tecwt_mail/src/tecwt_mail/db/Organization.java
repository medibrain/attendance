package tecwt_mail.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tecwt_mail.db.DBConnect.SQLResult;

public class Organization {

	/**********
	 * カラム *
	 **********/

	private int oid;
	private String name;
	private boolean enable;
	private String loginid;
	private String pass;
	private String imaphost;
	private int imapport;
	private String folder;
	private String smtphost;
	private int smtpport;
	private String mailladd;
	private int idate;
	private int udate;

	/**************************
	 * カラムの setter,getter *
	 *************************/

	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isEnable() {
		return enable;
	}
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getImaphost() {
		return imaphost;
	}
	public void setImaphost(String imaphost) {
		this.imaphost = imaphost;
	}
	public int getImapport() {
		return imapport;
	}
	public void setImapport(int imapport) {
		this.imapport = imapport;
	}
	public String getFolder() {
		return folder;
	}
	public void setFolder(String folder) {
		this.folder = folder;
	}
	public String getSmtphost() {
		return smtphost;
	}
	public void setSmtphost(String smtphost) {
		this.smtphost = smtphost;
	}
	public int getSmtpport() {
		return smtpport;
	}
	public void setSmtpport(int smtpport) {
		this.smtpport = smtpport;
	}
	public String getMailladd() {
		return mailladd;
	}
	public void setMailladd(String mailladd) {
		this.mailladd = mailladd;
	}
	public int getIdate() {
		return idate;
	}
	public void setIdate(int idate) {
		this.idate = idate;
	}
	public int getUdate() {
		return udate;
	}
	public void setUdate(int udate) {
		this.udate = udate;
	}

	/*******
	 * SQL *
	 ******/

	/**
	 * oidをもとに組織を取得します。
	 */
	public static Result MatchOfOid(int oid) {
		final Result result = new Result();
		result.setSuccess(false);

		// SQL
		String sql = String.format(
				"SELECT * FROM organization WHERE oid=%d",
				oid);

		try {
			// 実行
			DBConnect dbc = new DBConnect(sql, false);
			SQLResult sqlResult = dbc.execSELECT(new DBConnect.ResultCallback() {

				@Override
				public void onResult(ResultSet resultSet) throws SQLException {
					// コールバック
					List<Organization> list = new ArrayList<Organization>();
					Organization organization;
					while(resultSet.next()) {
						organization = new Organization();
						organization.setOid(resultSet.getInt("oid"));
						organization.setName(resultSet.getString("name"));
						organization.setEnable(resultSet.getBoolean("enable"));
						organization.setLoginid(resultSet.getString("loginid"));
						organization.setPass(resultSet.getString("pass"));
						organization.setImaphost(resultSet.getString("imaphost"));
						organization.setImapport(resultSet.getInt("imapport"));
						organization.setFolder(resultSet.getString("folder"));
						organization.setSmtphost(resultSet.getString("smtphost"));
						organization.setSmtpport(resultSet.getInt("smtpport"));
						organization.setMailladd(resultSet.getString("mailladd"));
						organization.setIdate(resultSet.getInt("idate"));
						organization.setUdate(resultSet.getInt("udate"));
						list.add(organization);
					}
					if(list.size() > 0) {
						result.setBool(true);
					}
					result.setList(list);
				}
			});

			// 成功確認
			if(sqlResult.isSuccess) {
				result.setSuccess(true);
			} else {
				result.setMessage(sqlResult.message);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
			result.setMessage(ex.getMessage());
		}

		return result;
	}

}
