package tecwt_mail.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import tecwt_mail.util.Log;

// tecwt_webとtecwt_mailの両方に存在しているので修正を入れる時は両方に反映させること！

public class DBPreparedStatementConnect {
	public static class Batch {
		public String sql;
		public List<List<Object>> parameterList;
	}

	private Connection connection = null;

	public boolean open() {
		try {
			// コネクションプールの使用
			connection = DBConnect.datasource.getConnection();
			return true;
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return false;
		}
	}

	public void close() {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				Log.exceptionWrite(e);
			} finally {
				connection = null;
			}
		}
	}

	public ResultSet execQuery(String sql, List<Object> parameters) {
		if (connection == null) {
			return null;
		}

		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			setupParameter(statement, parameters);
			return statement.executeQuery();
		} catch (SQLException e) {
			Log.exceptionWrite(e);
			return null;
		}
	}

	public boolean execUpdate(String sql, List<Object> parameters) {
		if (connection == null) {
			return false;
		}

		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			setupParameter(statement, parameters);
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			Log.exceptionWrite(e);
			return false;
		}
	}

	// ※単にSQLの投入が成功したかどうかしか返さないので注意
	public boolean execBatchSQL(List<Batch> batches) {
		if (connection == null) {
			return false;
		}

		try {
			connection.setAutoCommit(false);

			for (Batch batch : batches) {
				PreparedStatement statement = connection.prepareStatement(batch.sql);

				for (List<Object> parameters : batch.parameterList) {
					setupParameter(statement, parameters);
					statement.addBatch();
				}
				statement.executeBatch();
			}

			connection.commit();

			return true;
		} catch (SQLException e) {
			Log.exceptionWrite(e);
			try {
				connection.rollback();
				return false;
			} catch (SQLException e1) {
				Log.exceptionWrite(e);
				return false;
			}
		}
	}

	private void setupParameter(PreparedStatement statement, List<Object> parameters) throws SQLException {
		for (int i = 0; i < parameters.size(); i++) {
			Object obj = parameters.get(i);
			if (obj instanceof String) {
				statement.setString(i + 1, (String)obj);
			} else if (obj instanceof Integer) {
				statement.setInt(i + 1, (Integer)obj);
			} else if (obj instanceof Boolean) {
				statement.setBoolean(i + 1, (Boolean)obj);
			}
		}
	}
}
