package tecwt_mail.db;

import java.sql.SQLException;

import tecwt_mail.db.DBConnect.SQLResult;

public class MailSend {

	/**********
	 * カラム *
	 **********/

	private int id;
	private String receivemid;
	private int oid;
	private int uid;
	private String mailadd;
	private String subject;
	private String text;
	private int senddate;
	private int sendtime;

	/******************
	 * setter, getter *
	 ******************/

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getReceivemid() {
		return receivemid;
	}
	public void setReceivemid(String receivemid) {
		this.receivemid = receivemid;
	}
	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getMailadd() {
		return mailadd;
	}
	public void setMailadd(String mailadd) {
		this.mailadd = mailadd;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getSenddate() {
		return senddate;
	}
	public void setSenddate(int senddate) {
		this.senddate = senddate;
	}
	public int getSendtime() {
		return sendtime;
	}
	public void setSendtime(int sendtime) {
		this.sendtime = sendtime;
	}

	/*******
	 * SQL *
	 *******/

	/**
	 * insert
	 * @param mailsend
	 * @return
	 */
	public static Result INSERT(MailSend mailsend) {
		Result result = new Result();
		result.setSuccess(false);

		// INSERT文作成
		String sql = String.format(
				"INSERT INTO mail_send (receivemid, oid, uid, mailadd, subject, text, senddate, sendtime) VALUES ('%s', %d, %d, '%s', '%s', '%s', %d, %d)",
				mailsend.getReceivemid(),
				mailsend.getOid(),
				mailsend.getUid(),
				mailsend.getMailadd(),
				mailsend.getSubject(),
				mailsend.getText(),
				mailsend.getSenddate(),
				mailsend.getSendtime()
				);

		try {
			// 実行
			DBConnect dbc = new DBConnect(sql, true);
			SQLResult sqlResult = dbc.execNoSELECT();

			// 成功判定
			if(sqlResult.isSuccess) {
				result.setSuccess(true);
				result.addCount(sqlResult.resultCounts.get(0));
			} else {
				result.setMessage(sqlResult.message);
			}

		} catch(SQLException ex) {
			ex.printStackTrace();
			result.setMessage(ex.getMessage());
		}

		return result;
	}

}
