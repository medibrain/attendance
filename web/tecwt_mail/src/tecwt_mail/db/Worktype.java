package tecwt_mail.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tecwt_mail.db.DBConnect.SQLResult;

public class Worktype {

	/**********
	 * カラム *
	 **********/

	private int oid;
	private int wid;
	private String name;
	private String kana;
	private boolean enable;
	private int idate;
	private int iuid;
	private int udate;
	private int uuid;

	/**************************
	 * カラムの setter,getter *
	 *************************/

	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public int getWid() {
		return wid;
	}
	public void setWid(int wid) {
		this.wid = wid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKana() {
		return kana;
	}
	public void setKana(String kana) {
		this.kana = kana;
	}
	public boolean isEnable() {
		return enable;
	}
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	public int getIdate() {
		return idate;
	}
	public void setIdate(int idate) {
		this.idate = idate;
	}
	public int getIuid() {
		return iuid;
	}
	public void setIuid(int iuid) {
		this.iuid = iuid;
	}
	public int getUdate() {
		return udate;
	}
	public void setUdate(int udate) {
		this.udate = udate;
	}
	public int getUuid() {
		return uuid;
	}
	public void setUuid(int uuid) {
		this.uuid = uuid;
	}

	/*******
	 * SQL *
	 ******/

	/**
	 * nameに合致するworktypeを取得。<br>
	 * isBool()=trueなら存在する<br>
	 * enableOtherOnly=trueの場合のみenableを条件に追記します。
	 * @param oid
	 * @param name
	 * @param enableOtherOnly
	 * @param enable
	 * @return
	 */
	public static Result MatchOfName(int oid, String name, boolean enableOtherOnly, boolean enable) {
		// enable
		String whereEnable = "";
		if(enableOtherOnly) {
			whereEnable = "AND enable="+enable;
		}

		// SQL
		String sql = String.format(
				"SELECT * FROM worktype WHERE oid=%d AND name='%s' %s",
				oid, name, whereEnable);
		return Execute(sql);
	}

	/**
	 * widからworktypeを特定。<br>
	 * isBool()=trueなら存在する<br>
	 */
	public static Result MatchOfWid(int oid, int wid) {
		// SQL
		String sql = String.format(
				"SELECT * FROM worktype WHERE oid=%d AND wid=%d",
				oid, wid);
		return Execute(sql);
	}

	/**
	 * oidからworktypeを取得。
	 */
	public static Result FindOfOid(int oid) {
		// SQL
		String sql = String.format(
				"SELECT * FROM worktype WHERE oid=%d",
				oid);
		return Execute(sql);
	}

	/**
	 * oidと勤務種別名の一文字目に該当するworktypeを取得。
	 * @param oid
	 * @param s
	 * @return
	 */
	public static Result FindOfNameFirst(int oid, String s) {
		// SQL
		String sql = "SELECT * FROM worktype WHERE oid="+oid+" AND name LIKE '"+s+"%' AND enable=true";
		return Execute(sql);
	}

	private static Result Execute(String sql) {
		final Result result = new Result();
		result.setSuccess(false);

		try {

			// 実行
			DBConnect dbc = new DBConnect(sql, false);
			SQLResult sqlResult = dbc.execSELECT(new DBConnect.ResultCallback() {

				@Override
				public void onResult(ResultSet resultSet) throws SQLException {
					List<Worktype> list = new ArrayList<Worktype>();
					Worktype worktype;
					while(resultSet.next()) {
						worktype = new Worktype();
						worktype.setOid(resultSet.getInt("oid"));
						worktype.setWid(resultSet.getInt("wid"));
						worktype.setName(resultSet.getString("name"));
						worktype.setKana(resultSet.getString("kana"));
						worktype.setEnable(resultSet.getBoolean("enable"));
						worktype.setIdate(resultSet.getInt("idate"));
						worktype.setIuid(resultSet.getInt("iuid"));
						worktype.setUdate(resultSet.getInt("udate"));
						worktype.setUuid(resultSet.getInt("uuid"));
						list.add(worktype);
					}
					if(list.size() > 0) {
						result.setBool(true);	// 合致するもの発見
					}
					result.setList(list);
				}
			});

			// 成功確認
			if(sqlResult.isSuccess) {
				result.setSuccess(true);
			} else {
				result.setMessage(sqlResult.message);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
			result.setMessage(ex.getMessage());
		}

		return result;
	}

}
