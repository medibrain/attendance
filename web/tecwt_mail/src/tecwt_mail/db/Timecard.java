package tecwt_mail.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tecwt_mail.db.DBConnect.SQLResult;

public class Timecard {

	public static final int SYUKKIN = 0;
	public static final int TAIKIN = 1;
	public static final int IDOU = 2;

	/***************
	 * カラム + α *
	 **************/

	// timecard
	protected int oid;
	protected int uid;
	protected int tdate;
	protected int times;
	protected int intime;
	protected int outtime;
	protected int wid;
	protected String note;
	protected int wcount;
	protected int rcount;
	protected int breaktime;
	protected int fare;

	protected String oidName;

	// tclog
	protected String logtype;
	protected String func;
	protected int logdate;
	protected int logtime;
	protected String latitude;
	protected String longitude;
	protected String unitid;

	/**************************
	 * カラムの setter,getter *
	 *************************/

	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public int getTdate() {
		return tdate;
	}
	public void setTdate(int tdate) {
		this.tdate = tdate;
	}
	public int getTimes() {
		return times;
	}
	public void setTimes(int times) {
		this.times = times;
	}
	public int getIntime() {
		return intime;
	}
	public void setIntime(int intime) {
		this.intime = intime;
	}
	public int getOuttime() {
		return outtime;
	}
	public void setOuttime(int outtime) {
		this.outtime = outtime;
	}
	public int getWid() {
		return wid;
	}
	public void setWid(int wid) {
		this.wid = wid;
	}
	public String getNote() {
		return this.note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public int getWcount() {
		return this.wcount;
	}
	public void setWcount(int wcount) {
		this.wcount = wcount;
	}
	public int getRcount() {
		return this.rcount;
	}
	public void setRcount(int rcount) {
		this.rcount = rcount;
	}
	public int getBreaktime() {
		return this.breaktime;
	}
	public void setBreaktime(int breaktime) {
		this.breaktime = breaktime;
	}
	public int getFare() {
		return this.fare;
	}
	public void setFare(int fare) {
		this.fare = fare;
	}

	/***************
	 * コンストラクタ *
	 **************/

	/**
	 * SELECT用
	 * @param oid
	 */
	public Timecard(int oid) {
		Result result = Organization.MatchOfOid(oid);
		if(!result.isSuccess()) {
			// エラー時は空文字で代用
			oidName = "";
		} else {
			oidName = ((Organization)result.getList().get(0)).getName();
		}
	}

	public Timecard(int oid, int uid, int tdate, int times, int intime, int outtime,
			int wid, String note, int wcount, int rcount, int breaktime, int fare) {
		this(oid);
		this.oid = oid;
		this.uid = uid;
		this.tdate = tdate;
		this.times = times;
		this.intime = intime;
		this.outtime = outtime;
		this.wid = wid;
		this.note = note;
		this.wcount = wcount;
		this.rcount = rcount;
		this.breaktime = breaktime;
		this.fare = fare;
	}

	/*******
	 * SQL *
	 ******/

	/**
	 * INSERT<br>
	 * funcがnullの場合はtclogに追加されません。
	 * @return
	 */
	protected Result insert() {
		Result result = new Result();
		result.setSuccess(false);
		// SQL
		List<String> sqls = new ArrayList<String>();
		sqls.add(getTimecardInsertSQL());	// timecard
		String tclog = getTclogInsertSQL();
		if(tclog != null) sqls.add(tclog);	// tclog

		return execSQL(sqls);
	}

	/**
	 * UPDATE<br>
	 * funcがnullの場合はtclogに追加されません。
	 * @return
	 */
	protected Result update() {
		Result result = new Result();
		result.setSuccess(false);
		// SQL
		List<String> sqls = new ArrayList<String>();
		sqls.add(getTimecardUpdateSQL());	// timecard
		String tclog = getTclogInsertSQL();
		if(tclog != null) sqls.add(tclog);	// tclog

		return execSQL(sqls);
	}

	/**
	 * リスト内にあるSQLを全て実行します。
	 * @param sqls
	 * @return
	 */
	protected Result execSQL(List<String> sqls) {
		Result result = new Result();
		result.setSuccess(false);
		try {
			// 実行
			DBConnect dbc = new DBConnect(sqls, true);
			SQLResult sqlResult = dbc.execNoSELECT();
			// 結果
			if(sqlResult.isSuccess) {
				// 成功
				result.setSuccess(true);
				result.setCounts(sqlResult.resultCounts);
			} else {
				// 失敗
				result.setMessage(sqlResult.message);
			}
		} catch (SQLException ex) {
			// エラー
			ex.printStackTrace();
			result.setMessage(ex.getMessage());
		}
		return result;
	}

	/**
	 * timecardのINSERT文
	 * @return
	 */
	protected String getTimecardInsertSQL() {
		if(intime == -2) intime = -1;
		if(outtime == -2) outtime = -1;

		String tcSQL = String.format(
				"INSERT INTO timecard"
						+ "(oid, uid, tdate, times, intime, outtime, breaktime, "
						+ "wid, note, fare, wcount, rcount) "
						+ "VALUES (%d, %d, %d, %d, %d, %d, %d, %d, '%s', %d, %d, %d);",
				oid, uid, tdate, times, intime, outtime, breaktime, wid, note, fare, wcount, rcount);

		return tcSQL;
	}

	/**
	 * timecardのUPDATE文<br>
	 * 値に-2もしくはnullが設定されているものは更新されません。
	 * @return
	 */
	protected String getTimecardUpdateSQL() {
		// SET句
		StringBuilder sb = new StringBuilder();
		if(intime != -2)	sb.append("intime=").append(intime).append(",");
		if(outtime != -2)	sb.append("outtime=").append(outtime).append(",");
		if(breaktime != -2) sb.append("breaktime=").append(breaktime).append(",");
		if(wid != -2)		sb.append("wid=").append(wid).append(",");
		if(note != null)	sb.append("note='").append(note).append("'").append(",");
		if(fare != -2)		sb.append("fare=").append(fare).append(",");
		if(wcount != -2)	sb.append("wcount=").append(wcount).append(",");
		if(rcount != -2)	sb.append("rcount=").append(rcount).append(",");

		// 最後尾のカンマを削除
		if(sb.length() > 0) {
			sb.deleteCharAt(sb.length()-1);
		}

		String tcSQL = String.format(
				"UPDATE timecard SET %s "
						+ "WHERE oid=%d AND uid=%d AND tdate=%d AND times=%d",
				sb.toString(),
				oid, uid, tdate, times);

		return tcSQL;
	}

	/**
	 * tclogのINSERT文<br>
	 * funcがnullの場合はnullを返します。
	 * @return
	 */
	protected String getTclogInsertSQL() {
		if(func == null) {
			return null;
		}
		String tclSQL = String.format(
				"INSERT INTO tclog "
						+ "(oid, uid, logtype, func, carddate, logdate, "
						+ "logtime, latitude, longitude, unitid) "
						+ "VALUES (%d, %d, '%s', '%s', %d, %d, %d, '%s', '%s', '%s');",
				oid, uid, logtype, func, tdate, logdate, logtime, latitude, longitude, unitid);

		return tclSQL;
	}

	/**
	 * tdateからタイムカードを検索します。<br>
	 * 複数行合致する場合があります。<br>
	 * oidに-1を指定すると全レコードを対象とします。<br>
	 * uidに-1を指定すると全ユーザを対象とします。<br>
	 * tdateは指定しなければなりません。
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @return
	 */
	public static Result FindOfTdate(int oid, int uid, int tdate) {
		final Result result = new Result();
		result.setSuccess(false);

		// SQL
		String sql = String.format(
				"SELECT * FROM timecard WHERE %s %s tdate=%d ORDER BY times ASC",
				oid == -1 ? "" : "oid="+oid+" AND",
				uid == -1 ? "" : "uid="+uid+" AND",
				tdate);

		try {

			// 実行
			DBConnect dbc = new DBConnect(sql, false);
			SQLResult sqlResult = dbc.execSELECT(new DBConnect.ResultCallback() {

				@Override
				public void onResult(ResultSet resultSet) throws SQLException {
					List<Timecard> list = new ArrayList<Timecard>();
					Timecard timecard;
					while(resultSet.next()) {
						timecard = new Timecard(resultSet.getInt("oid"));
						timecard.setOid(resultSet.getInt("oid"));
						timecard.setUid(resultSet.getInt("uid"));
						timecard.setTdate(resultSet.getInt("tdate"));
						timecard.setTimes(resultSet.getInt("times"));
						timecard.setIntime(resultSet.getInt("intime"));
						timecard.setOuttime(resultSet.getInt("outtime"));
						timecard.setWid(resultSet.getInt("wid"));
						timecard.setNote(resultSet.getString("note"));
						timecard.setWcount(resultSet.getInt("wcount"));
						timecard.setRcount(resultSet.getInt("rcount"));
						timecard.setBreaktime(resultSet.getInt("breaktime"));
						timecard.setFare(resultSet.getInt("fare"));
						list.add(timecard);
					}
					result.setList(list);
				}
			});

			// 成功確認
			if(sqlResult.isSuccess) {
				result.setSuccess(true);
			} else {
				result.setMessage(sqlResult.message);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
			result.setMessage(ex.getMessage());
		}

		return result;
	}

	/**
	 * tdateから打刻未完了タイムカード一覧を取得します。<br>
	 * 戻り値に含まれるgetList()メソッドで取得可能なオブジェクトはList＜Timecard.TodaysNotCompleter＞です。<br>
	 * 移動を行っている場合はtimesの値ごとに判定され、それぞれ別々にリストへ格納されます。<br>
	 * 例）以下の場合には２レコードとなります。<br>
	 * |oid|uid|tdate|times|intime|outtime|<br>
	 * |1|1|20160418|1|-1|1200|<br>
	 * |1|1|20160418|2|1200|-1|<br>
	 * @param oid
	 * @param tdate
	 * @param enable true:非凍結者のみ、false:全ユーザ対象
	 * @return
	 */
	public static Result FindOfTodaysNotCompleter(int oid, int tdate, boolean enable) {
		final Result result = new Result();
		result.setSuccess(false);

		// SQL
		String sql = String.format(
				"SELECT t.oid, t.uid, t.tdate, t.times, t.intime, t.outtime, u.name, u.mailladd, u.enable "
				+ "FROM timecard AS t LEFT OUTER JOIN users AS u ON (t.uid=u.uid) "
				+ "WHERE %s tdate=%d AND (intime=-1 or outtime=-1) %s "
				+ "ORDER BY times ASC",
				oid == -1 ? "" : "t.oid="+oid+" AND",
				tdate,
				enable ? "AND u.enable="+String.valueOf(enable) : "");

		try {

			// 実行
			DBConnect dbc = new DBConnect(sql, false);
			SQLResult sqlResult = dbc.execSELECT(new DBConnect.ResultCallback() {

				@Override
				public void onResult(ResultSet resultSet) throws SQLException {
					List<TodaysNotCompleter> list = new ArrayList<TodaysNotCompleter>();
					TodaysNotCompleter tnc;
					while(resultSet.next()) {
						tnc = new TodaysNotCompleter();
						tnc.oid = resultSet.getInt("oid");
						tnc.uid = resultSet.getInt("uid");
						tnc.tdate = resultSet.getInt("tdate");
						tnc.times = resultSet.getInt("times");
						tnc.intime = resultSet.getInt("intime");
						tnc.outtime = resultSet.getInt("outtime");
						tnc.uname = resultSet.getString("name");
						tnc.mailladd = resultSet.getString("mailladd");
						tnc.enable = resultSet.getBoolean("enable");
						list.add(tnc);
					}
					result.setList(list);
				}
			});

			// 成功確認
			if(sqlResult.isSuccess) {
				result.setSuccess(true);
			} else {
				result.setMessage(sqlResult.message);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
			result.setMessage(ex.getMessage());
		}

		return result;
	}

	public static class TodaysNotCompleter {
		public int oid;
		public int uid;
		public int tdate;
		public int times;
		public int intime;
		public int outtime;
		public String uname;
		public String mailladd;
		public boolean enable;
	}

}
