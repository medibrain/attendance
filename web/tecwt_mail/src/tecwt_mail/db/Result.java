package tecwt_mail.db;

import java.util.ArrayList;
import java.util.List;

public class Result {

	private List<?> list;
	private List<Integer> counts;
	private boolean bool;
	private boolean isSuccess;
	private String message;


	public List<?> getList() {
		return list;
	}
	public void setList(List<?> list) {
		this.list = list;
	}
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public boolean isBool() {
		return bool;
	}
	public void setBool(boolean bool) {
		this.bool = bool;
	}
	public List<Integer> getCounts() {
		return counts;
	}
	public void setCounts(List<Integer> counts) {
		this.counts = counts;
	}
	public synchronized void addCount(int count) {
		if(counts == null) {
			counts = new ArrayList<Integer>();
		}
		counts.add(count);
	}
	public int getCount(int index) {
		return counts.get(index);
	}


}
