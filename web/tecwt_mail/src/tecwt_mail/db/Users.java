package tecwt_mail.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tecwt_mail.db.DBConnect.SQLResult;

public class Users {

	/**********
	 * カラム *
	 **********/

	private int oid;
	private int uid;
	private String lname;
	private String name;
	private String kana;
	private boolean enable;
	private String pass;
	private boolean admin;
	private String mailadd;
	private int idate;
	private int iuid;
	private int udate;
	private int uuid;
	private String code;
	private boolean mailenable;


	/**************************
	 * カラムの setter,getter *
	 *************************/

	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getLname() {
		return lname;
	}
	public void setLname(String lname) {
		this.lname = lname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKana() {
		return kana;
	}
	public void setKana(String kana) {
		this.kana = kana;
	}
	public boolean isEnable() {
		return enable;
	}
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public String getMailadd() {
		return mailadd;
	}
	public void setMailadd(String mailadd) {
		this.mailadd = mailadd;
	}
	public int getIdate() {
		return idate;
	}
	public void setIdate(int idate) {
		this.idate = idate;
	}
	public int getIuid() {
		return iuid;
	}
	public void setIuid(int iuid) {
		this.iuid = iuid;
	}
	public int getUdate() {
		return udate;
	}
	public void setUdate(int udate) {
		this.udate = udate;
	}
	public int getUuid() {
		return uuid;
	}
	public void setUuid(int uuid) {
		this.uuid = uuid;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public boolean isMailenable() {
		return mailenable;
	}
	public void setMailenable(boolean mailenable) {
		this.mailenable = mailenable;
	}


	/**********************
	 * アクセサーメソッド *
	 *********************/

	/**
	 * メールアドレスからユーザを特定します。
	 * @param oid
	 * @param mailadd
	 * @param enable true:非凍結者のみ、false:全ユーザ対象
	 * @param mailenable true:メール打刻許可者のみ、false:全ユーザ対象
	 * @return
	 */
	public static Result MatchOfMail(int oid, String mailadd, boolean enable, boolean mailenable) {
		final Result result = new Result();
		result.setSuccess(false);

		// where
		String eSql = enable ? "AND enable="+String.valueOf(enable) : "";
		//String meSql = mailenable ? "AND mailenable="+String.valueOf(mailenable) : "";

		// SQL
		String sql = String.format(
				"SELECT * FROM users WHERE oid=%d AND mailladd='%s' %s",
				oid, mailadd, eSql);

		try {
			// 実行
			DBConnect dbc = new DBConnect(sql, false);
			SQLResult sqlResult = dbc.execSELECT(new DBConnect.ResultCallback() {

				@Override
				public void onResult(ResultSet resultSet) throws SQLException {
					// コールバック
					List<Users> list = new ArrayList<Users>();
					Users users;
					while(resultSet.next()) {
						users = new Users();
						users.setOid(resultSet.getInt("oid"));
						users.setUid(resultSet.getInt("uid"));
						users.setCode(resultSet.getString("code"));
						users.setLname(resultSet.getString("lname"));
						users.setName(resultSet.getString("name"));
						users.setKana(resultSet.getString("kana"));
						users.setEnable(resultSet.getBoolean("enable"));
						users.setPass(resultSet.getString("pass"));
						users.setAdmin(resultSet.getBoolean("admin"));
						users.setMailadd(resultSet.getString("mailladd"));
						users.setIdate(resultSet.getInt("idate"));
						users.setIuid(resultSet.getInt("iuid"));
						users.setUdate(resultSet.getInt("udate"));
						users.setUuid(resultSet.getInt("uuid"));
						users.setMailenable(resultSet.getBoolean("mailenable"));
						list.add(users);
					}
					if(list.size() > 0) {
						boolean exist = false;
						for(Users user : list) {
							if(user.mailenable == mailenable) {
								exist = true;
								break;
							}
						}
						result.setBool(exist);
					} else {
						result.setBool(false);
					}
					result.setList(list);
				}
			});

			// 成功確認
			if(sqlResult.isSuccess) {
				result.setSuccess(true);
			} else {
				result.setMessage(sqlResult.message);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
			result.setMessage(ex.getMessage());
		}

		return result;
	}

}
