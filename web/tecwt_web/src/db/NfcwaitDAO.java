package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import table.Nfcwait;
import util.DateTime;
import util.Util;

public class NfcwaitDAO implements ResultFormat {

	/**
	 * 全レコードを取得します。
	 * @return
	 * @throws SQLException
	 */
	public String findAll() throws SQLException {
		return this.execute(this.getSQLText(null), false);
	}

	/**
	 * 指定したoid,uidに該当するレコードを返します。<br>
	 * 組み合わせは以下の通り。<br>
	 * ① null null OK<br>
	 * ② oid  null OK<br>
	 * ③ oid  uid  OK<br>
	 * ④ null uid  NG<br>
	 * また""はエラーとなります。
	 * @param oid
	 * @param uid
	 * @return
	 * @throws SQLException
	 */
	public String findOfUid(String oid, String uid) throws SQLException {
		if(Util.isNullOrBlank(oid) && !Util.isNullOrBlank(uid)) return null;

		// WHERE
		StringBuilder where = new StringBuilder();
		if(oid != null) {
			where.append("WHERE ");
			where.append("oid=").append(oid).append(" ");
			if(uid != null) where.append("AND ").append("uid=").append(uid).append(" ");
		}

		// SQL
		String sql = this.getSQLText(where.toString());

		return this.execute(sql, false);
	}

	/**
	 * 引数のwhere句("WHERE"と末尾のスペース含む)を内蔵した完全なSQL文を作成します。<br>
	 * @param where
	 * @return
	 */
	private String getSQLText(String where) {
		StringBuilder s = new StringBuilder();
		s.append("SELECT ");
		s.append("* ");
		s.append("FROM ");
		s.append(DBinfo.TABLE_NFCWAIT).append(" ");
		if(where != null) s.append(where);

		return s.toString();
	}

	/**
	 * SELECT用SQL実行メソッド。
	 * @param sql
	 * @param transaction
	 * @return
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	private String execute(String sql, boolean transaction) throws SQLException {

		// 実行
		DBConnect db = new DBConnect(sql, transaction);
		List<Nfcwait> resultList = (List<Nfcwait>)db.ExcuteResult(this);

		// error
		if(resultList == null) {
			return null;
		}

		return this.toJSON(resultList);
	}

	/**
	 * ResultSetオブジェクトをリストに整形する。
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	@Override
	public List<Object> toList(ResultSet resultSet) throws SQLException {
		List<Object> datas = new ArrayList<Object>();
		Nfcwait nfcwait;
		while(resultSet.next()) {
			nfcwait = new Nfcwait();
			nfcwait.setOid(resultSet.getInt("oid"));
			nfcwait.setUid(resultSet.getInt("uid"));
			nfcwait.setIdate(resultSet.getInt("idate"));
			nfcwait.setIuid(resultSet.getInt("iuid"));
			datas.add(nfcwait);
		}

		return datas;
	}

	/**
	 * JSON形式に変換
	 * @param list
	 * @return
	 */
	public String toJSON(List<Nfcwait> datas) {

		// JSON形式に成型
		StringBuilder json = new StringBuilder();

		json.append("{");
		json.append("\"result\":\"success\",");
		json.append("\"datas\":[");
		for(int i=0; i<datas.size(); i++) {
			if(i > 0) {
				json.append(",");
			}
			json.append("{");
				json.append("\"oid\":\"").append(datas.get(i).getOid()).append("\",");
				json.append("\"uid\":\"").append(datas.get(i).getUid()).append("\",");
				json.append("\"idate\":\"").append(Util.toTdate(datas.get(i).getIdate())).append("\",");
				json.append("\"iuid\":\"").append(datas.get(i).getIuid()).append("\",");
			json.append("}");
		}
		json.append("]");
		json.append("}");

		return json.toString();
	}

	/**
	 * nfcwaitにレコードを追加します。<br>
	 * 引数はすべて必須項目です。<br>
	 * iuidは追加したユーザID
	 * @param oid
	 * @param uid
	 * @param iuid
	 * @return
	 * @throws SQLException
	 */
	public String insert(String oid, String uid, String iuid) throws SQLException {

		if(Util.isNullOrBlank(oid) || Util.isNullOrBlank(uid) || Util.isNullOrBlank(iuid)) return null;

		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ");
		sql.append(DBinfo.TABLE_NFCWAIT).append(" ");
		sql.append("(oid,uid,idate,iuid) ");
		sql.append("VALUES ");
		sql.append("(");
		sql.append(oid).append(",").append(uid).append(",").append(DateTime.GetTodayInt()).append(",").append(iuid);
		sql.append(")");

		return this.executeUI(sql.toString(), false);
	}

	/**
	 * nfcwaitからレコードを削除します。<br>
	 * 引数はすべて必須項目です。<br>
	 * @param oid
	 * @param uid
	 * @return
	 * @throws SQLException
	 */
	public String delete(String oid, String uid) throws SQLException {

		if(Util.isNullOrBlank(oid) || Util.isNullOrBlank(uid)) return null;

		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM ");
		sql.append(DBinfo.TABLE_NFCWAIT).append(" ");
		sql.append("WHERE ");
		sql.append("oid=").append(oid).append(" AND ");
		sql.append("uid=").append(uid);

		return this.executeUI(sql.toString(), false);
	}

	/**
	 * UPDATE,DELETE,INSERT用SQL実行メソッド。
	 * @param sql
	 * @param transaction
	 * @return
	 * @throws SQLException
	 */
	private String executeUI(String sql, boolean transaction) throws SQLException {

		// 実行
		DBConnect db = new DBConnect(sql, transaction);
		List<Integer> countList = db.ExcuteNonQueryCount();

		// error
		if(countList == null) {
			return null;
		}

		// 更新行数を合算
		int allCount = 0;
		for(int count : countList) {
			allCount += count;
		}

		return Util.toSuccessJSON(String.valueOf(allCount));
	}

}
