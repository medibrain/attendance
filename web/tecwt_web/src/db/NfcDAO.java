package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import table.Nfc;
import util.Util;

public class NfcDAO implements ResultFormat {

	/**
	 * 全レコードを返します。
	 * @return
	 * @throws SQLException
	 */
	public String findAll() throws SQLException {

		// SQL文作成
		String sql = this.getSQLText("");

		return this.execute(sql, false);
	}

	/**
	 * 以下のパターンに対応しレコードを返します。<br>
	 * ①oid -> oid検索<br>
	 * ②oid,uid -> uid検索<br>
	 * ③①②以外 -> null<br>
	 * @param oid
	 * @param uid
	 * @return
	 * @throws SQLException
	 */
	public String findOfOidOrUid(String oid, String uid) throws SQLException {

		if(Util.isNullOrBlank(oid)) return null;

		// WHERE句作成
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append("oid=").append(oid).append(" ");
		if(uid != null) where.append("AND uid=").append(uid).append(" ");

		// SQL文作成
		String sql = this.getSQLText(where.toString());

		return this.execute(sql, false);
	}

	/**
	 * 引数のwhere句("WHERE"と末尾のスペース含む)を内蔵した完全なSQL文を作成します。<br>
	 * @param where
	 * @return
	 */
	private String getSQLText(String where) {
		StringBuilder s = new StringBuilder();
		s.append("SELECT ");
		s.append("* ");
		s.append("FROM ");
		s.append(DBinfo.TABLE_NFC).append(" ");
		if(where != null) s.append(where);
		s.append("ORDER BY ");
		s.append("idate DESC");

		return s.toString();
	}

	/**
	 * SELECT用SQL実行メソッド。
	 * @param sql
	 * @param transaction
	 * @return
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	private String execute(String sql, boolean transaction) throws SQLException {

		// 実行
		DBConnect db = new DBConnect(sql, transaction);
		List<Nfc> resultList = (List<Nfc>)db.ExcuteResult(this);

		// error
		if(resultList == null) {
			return null;
		}

		return this.toJSON(resultList);
	}

	/**
	 * ResultSetオブジェクトをリストに整形する。
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	@Override
	public List<Object> toList(ResultSet resultSet) throws SQLException {
		List<Object> datas = new ArrayList<Object>();
		Nfc nfc;
		while(resultSet.next()) {
			nfc = new Nfc();
			nfc.setIdm(resultSet.getString("idm"));
			nfc.setOid(resultSet.getInt("oid"));
			nfc.setUid(resultSet.getInt("uid"));
			nfc.setNote(resultSet.getString("note"));
			nfc.setIdate(resultSet.getInt("idate"));
			nfc.setIuid(resultSet.getInt("iuid"));
			nfc.setUdate(resultSet.getInt("udate"));
			nfc.setUuid(resultSet.getInt("uuid"));
			datas.add(nfc);
		}

		return datas;
	}

	/**
	 * JSON形式に変換
	 * @param list
	 * @return
	 */
	public String toJSON(List<Nfc> datas) {

		// JSON形式に成型
		StringBuilder json = new StringBuilder();

		json.append("{");
		json.append("\"result\":\"success\",");
		json.append("\"datas\":[");
		for(int i=0; i<datas.size(); i++) {
			if(i > 0) {
				json.append(",");
			}
			json.append("{");
				json.append("\"idm\":\"").append(datas.get(i).getIdm()).append("\",");
				json.append("\"oid\":\"").append(datas.get(i).getOid()).append("\",");
				json.append("\"uid\":\"").append(datas.get(i).getUid()).append("\",");
				json.append("\"note\":\"").append(datas.get(i).getNote()).append("\",");
				json.append("\"idate\":\"").append(Util.toTdate(datas.get(i).getIdate())).append("\",");
				json.append("\"iuid\":\"").append(datas.get(i).getIuid()).append("\",");
				json.append("\"udate\":\"").append(Util.toTdate(datas.get(i).getUdate())).append("\",");
				json.append("\"uuid\":\"").append(datas.get(i).getUuid()).append("\"");
			json.append("}");
		}
		json.append("]");
		json.append("}");

		return json.toString();
	}

	/**
	 * 指定したNFCを削除します。<br>
	 * すべて必須項目です。<br>
	 * 違反があった場合はnullを返します。<br>
	 * idms.lengthが0である場合、oid,uidに関連した全NFCを削除します。
	 * @param oid
	 * @param uid
	 * @return
	 * @throws SQLException
	 */
	public String delete(String oid, String uid, String[] idms) throws SQLException {

		if(Util.isNullOrBlank(oid) || Util.isNullOrBlank(uid) || idms == null) return null;

		// WHERE句
		StringBuilder where = new StringBuilder();
		where.append("oid=").append(oid).append(" AND ");
		where.append("uid=").append(uid).append(" ");
		for(int i=0; i<idms.length; i++) {
			if(i == 0) where.append("AND (");
			if(i != 0) where.append("OR ");
			where.append("idm='").append(idms[i]).append("' ");
		}
		if(idms.length != 0) where.append(") ");

		// SQL文
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM ");
		sql.append(DBinfo.TABLE_NFC).append(" ");
		sql.append("WHERE ");
		sql.append(where.toString());

		return this.executeUI(sql.toString(), true);
	}

	/**
	 * UPDATE,INSERT用SQL実行メソッド。
	 * @param sql
	 * @param transaction
	 * @return
	 * @throws SQLException
	 */
	private String executeUI(String sql, boolean transaction) throws SQLException {

		// 実行
		DBConnect db = new DBConnect(sql, transaction);
		List<Integer> countList = db.ExcuteNonQueryCount();

		// error
		if(countList == null) {
			return null;
		}

		// 更新行数を合算
		int allCount = 0;
		for(int count : countList) {
			allCount += count;
		}

		return Util.toSuccessJSON(String.valueOf(allCount));
	}

}
