package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import table.Worktype;
import util.DateTime;
import util.Util;

public class WorktypeDAO extends BaseDAO {

	/**
	 * 全検索。
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Worktype> findAll() throws SQLException {
		return GetQueryResult(getSQLText(""), handler, Worktype.class);
	}

	/**
	 * oidに関連付けられた種別一覧。
	 * @param oid
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Worktype> findOfOid(String oid) throws SQLException {
		// WHERE句
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append(DBinfo.TABLE_WORKTYPE).append(".oid=").append(oid).append(" ");
		return GetQueryResult(getSQLText(where.toString()), handler, Worktype.class);
	}

	/**
	 * oidに関連付けられた種別一覧。
	 * @param oid
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Worktype> findOfOid(String oid, String yearCode) throws SQLException {
		// WHERE句
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append(DBinfo.TABLE_WORKTYPE).append(".oid=").append(oid).append(" ");
		where.append("AND projectcode LIKE '" + yearCode + "%' ");
		return GetQueryResult(getSQLText(where.toString()), handler, Worktype.class);
	}


	/**
	 * 指定したwidを持つレコードを返します。
	 * @param oid
	 * @param wids
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Worktype> findOfWids(String oid, List<String> wids) throws SQLException {
		// WHERE句
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append(DBinfo.TABLE_WORKTYPE).append(".oid=").append(oid).append(" ");
		if (wids != null && wids.size() != 0) {
			where.append("AND ");
			where.append("(");
			for(int i=0; i<wids.size(); i++) {
				if(i != 0) {
					where.append(" OR ");
				}
				where.append(DBinfo.TABLE_WORKTYPE).append(".wid=").append(wids.get(i));
			}
			where.append(") ");
		}
		return GetQueryResult(getSQLText(where.toString()), handler, Worktype.class);
	}

	/**
	 * enable=trueなworktypeを取得。
	 * @param oid
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Worktype> findOfEnable(String oid) throws SQLException {
		// WHERE句
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append(DBinfo.TABLE_WORKTYPE).append(".oid=").append(oid).append(" AND ");
		where.append(DBinfo.TABLE_WORKTYPE).append(".enable=true").append(" ");
		return GetQueryResult(getSQLText(where.toString()), handler, Worktype.class);
	}

	/**
	 * 日付の範囲から作成日に該当するレコード一覧を返します。<br>
	 * oidがnullの場合、nullを返します。<br>
	 * 以下に利用可能な検索条件を記述します。以下に合致しないnullがあった場合はnullを返します。<br>
	 * ①date1～<br>
	 * ②date1～date2<br>
	 * ③～date2<br>
	 * @param oid
	 * @param date1
	 * @param date2
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Worktype> findOfDateRange(String oid, String date1, String date2) throws SQLException {
		// WHERE句
		StringBuilder where = new StringBuilder();
		StringBuilder range = new StringBuilder();
		if(date1 != null) {
			if(date2 == null) {
				// ①
				range.append(DBinfo.TABLE_WORKTYPE).append(".idate>=").append(date1).append(" ");
			}
			else {
				// ②
				range.append("AND ").append(DBinfo.TABLE_WORKTYPE).append(".idate>=").append(date1).append(" ");
				range.append("AND ").append(DBinfo.TABLE_WORKTYPE).append(".idate<=").append(date2).append(" ");
			}
		}
		else {
			// ③
			range.append("AND ").append(DBinfo.TABLE_WORKTYPE).append(".idate<=").append(date2).append(" ");
		}
		where.append("WHERE ").append(DBinfo.TABLE_WORKTYPE).append(".oid=").append(oid).append(" ");
		where.append(range.toString());

		return GetQueryResult(getSQLText(where.toString()), handler, Worktype.class);
	}

	/**
	 * 指定したenable(true/false)を持つレコードを返します。<br>
	 * 引数はすべて必須です。違反した場合はnullを返します。<br>
	 * enableは便宜上Stringとしていますが、必ず"true"または"false"で指定してください。<br>
	 * それ以外はすべてnullとして扱います。<br>
	 * @param oid
	 * @param enable
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Worktype> findOfEnable(String oid, String enable) throws SQLException {
		// WHERE句
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append(DBinfo.TABLE_WORKTYPE).append(".oid=").append(oid).append(" AND ");
		where.append(DBinfo.TABLE_WORKTYPE).append(".enable=").append(enable).append(" ");

		return GetQueryResult(getSQLText(where.toString()), handler, Worktype.class);
	}

	/**
	 * 引数のwhere句("WHERE"と末尾のスペース含む)を内蔵した完全なSQL文を作成します。<br>
	 * @param where
	 * @return
	 */
	private static String getSQLText(String where) {

		// SQL文
		StringBuilder s = new StringBuilder();
		s.append("SELECT ");
		s.append("*, ");
		s.append(DBinfo.TABLE_CUSTOMER).append(".name AS customername, ");
		s.append(DBinfo.TABLE_CATEGORY).append(".name AS categoryname, ");
		s.append(DBinfo.TABLE_USERS).append(".name AS managername ");
		s.append("FROM ");
		s.append(DBinfo.TABLE_WORKTYPE).append(" ");
		s.append("LEFT OUTER JOIN ").append(DBinfo.TABLE_CUSTOMER).append(" ON ")
			.append(DBinfo.TABLE_WORKTYPE).append(".oid = ").append(DBinfo.TABLE_CUSTOMER).append(".oid AND ")
			.append(DBinfo.TABLE_WORKTYPE).append(".customer = ").append(DBinfo.TABLE_CUSTOMER).append(".id ");
		s.append("LEFT OUTER JOIN ").append(DBinfo.TABLE_CATEGORY).append(" ON ")
			.append(DBinfo.TABLE_WORKTYPE).append(".oid = ").append(DBinfo.TABLE_CATEGORY).append(".oid AND ")
			.append(DBinfo.TABLE_WORKTYPE).append(".category = ").append(DBinfo.TABLE_CATEGORY).append(".id ");
		s.append("LEFT OUTER JOIN ").append(DBinfo.TABLE_USERS).append(" ON ")
			.append(DBinfo.TABLE_WORKTYPE).append(".oid = ").append(DBinfo.TABLE_USERS).append(".oid AND ")
			.append(DBinfo.TABLE_WORKTYPE).append(".manager = ").append(DBinfo.TABLE_USERS).append(".uid ");
		if(where != null) s.append(where);
		s.append("ORDER BY ");
		s.append(DBinfo.TABLE_WORKTYPE).append(".kana ASC");

		return s.toString();
	}

	private static DBConnect.ToListHandler handler = new DBConnect.ToListHandler() {
		@SuppressWarnings("unchecked")
		@Override
		public List<Worktype> ToList(ResultSet rs) throws SQLException {
			List<Worktype> datas = new ArrayList<Worktype>();
			Worktype worktype;
			while(rs.next()) {
				worktype = new Worktype();
				worktype.setOid(rs.getInt("oid"));
				worktype.setWid(rs.getInt("wid"));
				worktype.setName(rs.getString("name"));
				worktype.setKana(rs.getString("kana"));
				worktype.setEnable(rs.getBoolean("enable"));
				worktype.setIdate(rs.getInt("idate"));
				worktype.setIuid(rs.getInt("iuid"));
				worktype.setUdate(rs.getInt("udate"));
				worktype.setUuid(rs.getInt("uuid"));
				worktype.setCategory(rs.getInt("category"));
				worktype.setCategoryname(Util.nullBlankToHyphen(rs.getString("categoryname")));
				worktype.setCustomer(rs.getInt("customer"));
				worktype.setCustomername(Util.nullBlankToHyphen(rs.getString("customername")));
				worktype.setNote(rs.getString("note"));
				worktype.setManager(rs.getInt("manager"));
				worktype.setManagername(Util.nullBlankToHyphen(rs.getString("managername")));
				worktype.setProjectcode(rs.getString("projectcode"));
				datas.add(worktype);
			}
			return datas;
		}
	};

	/**
	 * レコードを更新します。<br>
	 * oid,wid,uuidは必須項目です。<br>
	 * その他は任意ですが、nullの場合は無視されます。<br>
	 * 任意項目がすべてnullである場合はnullを返します。<br>
	 * nameもしくはkanaが""である場合はnullを返します。<br>
	 * enable,adminは便宜上Stringとしていますが、必ず"true"か"false"で指定してください。それ以外は無視されます。<br>
	 * uuidは編集者のuidです。
	 * @param oid
	 * @param wid
	 * @param enable
	 * @param name
	 * @param kana
	 * @param uuid
	 * @param customer
	 * @param category
	 * @param note
	 * @param manager
	 * @param projectcode
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult update(
			String oid, String wid, String enable, String name, String kana, String uuid,
			String customer, String category, String note, int manager, String projectcode) throws SQLException {

		SQLResult failed = new SQLResult();
		failed.Success = false;

		// 必須項目確認
		if(Util.isNullOrBlank(oid) || Util.isNullOrBlank(wid) || Util.isNullOrBlank(uuid)) return failed;
		if(name != null && name.equals("")) return failed;
		if(kana != null && kana.equals("")) return failed;
		if(enable != null && enable.equals("")) return failed;

		// SET句
		StringBuilder set = new StringBuilder();
		if(enable != null && (enable.equals("true") || enable.equals("false"))) set.append("enable='").append(enable).append("',");
		if(name != null) set.append("name='").append(Util.toSQLParse(name)).append("',");
		if(kana != null) set.append("kana='").append(Util.toSQLParse(kana)).append("',");
		if (customer != null) set.append("customer=").append(customer).append(",");
		if (category != null) set.append("category=").append(category).append(",");
		if (note != null) set.append("note='").append(note).append("',");
		set.append("manager=").append(manager).append(",");
		if (projectcode != null) set.append("projectcode='").append(projectcode).append("',");
		set.append("udate=").append(DateTime.GetTodayInt()).append(",");	// 今日の日付
		set.append("uuid=").append(uuid).append(" ");

		// SQL文作成
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE ");
		sql.append(DBinfo.TABLE_WORKTYPE).append(" ");
		sql.append("SET ");
		sql.append(set.toString());
		sql.append("WHERE ");
		sql.append("oid=").append(oid).append(" AND ");
		sql.append("wid=").append(wid);

		return ExecSQL(sql.toString());
	}

	/**
	 * worktypeを追加します。
	 * @param oid
	 * @param name
	 * @param kana
	 * @param iuid
	 * @param customer
	 * @param category
	 * @param note
	 * @param manager
	 * @param projectcode
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult insert(String oid, String name, String kana, String iuid,
			String customer, String category, String note, int manager, String projectcode) throws SQLException {

		SQLResult failed = new SQLResult();
		failed.Success = false;

		// 必須項目確認
		if(Util.isNullOrBlank(oid)) return failed;
		if(Util.isNullOrBlank(name)) return failed;
		if(Util.isNullOrBlank(kana)) return failed;
		if(Util.isNullOrBlank(iuid)) return failed;

		// SQL文
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ").append(DBinfo.TABLE_WORKTYPE).append(" ");
		sql.append("(oid,name,kana,enable,idate,iuid,udate,uuid");
		if (customer != null) sql.append(",customer");
		if (category != null) sql.append(",category");
		if (note != null) sql.append(",note");
		if (manager > 0) sql.append(",manager");
		if (projectcode != null) sql.append(",projectcode");
		sql.append(") VALUES (");
		sql.append(oid).append(",");
		sql.append("'").append(Util.toSQLParse(name)).append("',");
		sql.append("'").append(Util.toSQLParse(kana)).append("',");
		sql.append("'true',");	// 初回は全部閲覧可
		sql.append(DateTime.GetTodayInt()).append(",");
		sql.append(iuid).append(",");
		sql.append(0).append(",");
		sql.append(0).append(",");
		if (customer != null) sql.append(customer).append(",");
		if (category != null) sql.append(category).append(",");
		if (note != null) sql.append("'").append(note).append("',");
		if (manager > 0) sql.append(manager).append(",");
		if (projectcode != null) sql.append("'").append(projectcode).append("',");
		String SQL = sql.toString().substring(0, sql.length() - 1) + ")";

		return ExecSQL(SQL);
	}

}
