package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import table.Unit;
import util.Util;

public class UnitDAO implements ResultFormat {

	/**
	 * 全レコードを取得します。
	 * @return
	 * @throws SQLException
	 */
	public String findAll() throws SQLException {
		return this.execute(this.getSQLText(null), false).JSON;
	}

	/**
	 * 指定したoidを持つレコードを取得します。<br>
	 * 引数は必須です。違反した場合はnullを返します。
	 * @param oid
	 * @return
	 * @throws SQLException
	 */
	public String findOfOid(String oid) throws SQLException {
		if(Util.isNullOrBlank(oid)) return null;
		return this.execute(this.getSQLText("WHERE oid="+oid+" "), false).JSON;
	}

	/**
	 * 指定したenableを持つレコードを返します。<br>
	 * enableは必ず"true"もしくは"false"で指定してください。<br>
	 * 引数は必須です。違反した場合はnullを返します。
	 * @param oid
	 * @param enable
	 * @return
	 * @throws SQLException
	 */
	public String findOfEnable(String oid, String enable) throws SQLException {
		if(Util.isNullOrBlank(oid) || Util.isNullOrBlank(enable)) return null;
		if(!enable.equals("true") && !enable.equals("false")) return null;

		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append("oid=").append(oid).append(" AND ");
		where.append("enable=").append(enable).append(" ");

		return this.execute(this.getSQLText(where.toString()), false).JSON;
	}

	/**
	 * 指定したunitidを持つレコードを返します。<br>
	 * 引数はすべて必須です。違反した場合はnullを返します。
	 * @param oid
	 * @param unitid
	 * @return
	 * @throws SQLException
	 */
	public String findOfUnitid(String oid, String unitid) throws SQLException {

		if(Util.isNullOrBlank(oid) || Util.isNullOrBlank(unitid)) return null;

		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append("oid=").append(oid).append(" AND ");
		where.append("unitid='").append(unitid).append("' ");

		return this.execute(this.getSQLText(where.toString()), false).JSON;
	}

	/**
	 * 指定したunitidのリストに該当する全てのレコードを返します。<br>
	 * 引数は全て必須です。違反した場合はnullを返します。
	 * @param oid
	 * @param unitids
	 * @return
	 * @throws SQLException
	 */
	public UnitResult FindOfUnitids(String oid, List<String> unitids) throws SQLException {

		if(Util.isNullOrBlank(oid) || unitids == null) return null;

		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append("oid=").append(oid).append(" ");
		for (int i = 0; i < unitids.size(); i++) {
			if (i == 0) where.append("AND (");
			if (i >= 1) where.append(" OR ");
			where.append("unitid='").append(unitids.get(i)).append("'");
			if (i == unitids.size() - 1) where.append(") ");
		}

		return this.execute(this.getSQLText(where.toString()), false);
	}

	/**
	 * 日付の範囲から作成日に該当するレコード一覧を返します。<br>
	 * oidがnullの場合、nullを返します。<br>
	 * 以下に利用可能な検索条件を記述します。以下に合致しないnullがあった場合はnullを返します。<br>
	 * ①date1～<br>
	 * ②date1～date2<br>
	 * ③～date2<br>
	 * @param oid
	 * @param date1
	 * @param date2
	 * @return
	 * @throws SQLException
	 */
	public String findOfDateRange(String oid, String date1, String date2) throws SQLException {

		if(oid == null) return null;
		if(date1 == null && date2 == null) return null;

		// WHERE句
		StringBuilder where = new StringBuilder();
		StringBuilder range = new StringBuilder();
		if(date1 != null) {
			if(date2 == null) {
				// ①
				range.append("AND ").append(DBinfo.TABLE_UNIT).append(".idate>=").append(date1).append(" ");
			}
			else {
				// ②
				range.append("AND ").append(DBinfo.TABLE_UNIT).append(".idate>=").append(date1).append(" ");
				range.append("AND ").append(DBinfo.TABLE_UNIT).append(".idate<=").append(date2).append(" ");
			}
		}
		else {
			// ③
			range.append("AND ").append(DBinfo.TABLE_UNIT).append(".idate<=").append(date2).append(" ");
		}
		where.append("WHERE ");
		where.append("oid=").append(oid).append(" ");
		where.append(range.toString());

		// SQL文作成
		String sql = this.getSQLText(where.toString());

		return this.execute(sql, false).JSON;
	}

	/**
	 * 引数のwhere句("WHERE"と末尾のスペース含む)を内蔵した完全なSQL文を作成します。<br>
	 * @param where
	 * @return
	 */
	private String getSQLText(String where) {

		// SQL文
		StringBuilder s = new StringBuilder();
		s.append("SELECT ");
		s.append("* ");
		s.append("FROM ");
		s.append(DBinfo.TABLE_UNIT).append(" ");
		if(where != null) s.append(where);
		s.append("ORDER BY ");
		s.append(DBinfo.TABLE_UNIT).append(".idate DESC");

		return s.toString();
	}

	/**
	 * SELECT用SQL実行メソッド。
	 * @param sql
	 * @param transaction
	 * @return
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	private UnitResult execute(String sql, boolean transaction) throws SQLException {

		// 実行
		DBConnect db = new DBConnect(sql, transaction);
		List<Unit> resultList = (List<Unit>)db.ExcuteResult(this);

		// error
		if(resultList == null) {
			return null;
		}

		UnitResult result = new UnitResult();
		result.JSON = this.toJSON(resultList);
		result.List = resultList;

		return result;
	}

	/**
	 * ResultSetオブジェクトをリストに整形する。
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	@Override
	public List<Object> toList(ResultSet resultSet) throws SQLException {
		List<Object> datas = new ArrayList<Object>();
		Unit unit;
		while(resultSet.next()) {
			unit = new Unit();
			unit.setOid(resultSet.getInt("oid"));
			unit.setUnitid(resultSet.getString("unitid"));
			unit.setName(resultSet.getString("name"));
			unit.setEnable(resultSet.getBoolean("enable"));
			unit.setPass(resultSet.getString("pass"));
			unit.setNote(resultSet.getString("note"));
			unit.setIdate(resultSet.getInt("idate"));
			datas.add(unit);
		}

		return datas;
	}

	/**
	 * JSON形式に変換
	 * @param list
	 * @return
	 */
	public String toJSON(List<Unit> datas) {

		// JSON形式に成型
		StringBuilder json = new StringBuilder();

		json.append("{");
		json.append("\"result\":\"success\",");
		json.append("\"datas\":[");
		for(int i=0; i<datas.size(); i++) {
			if(i > 0) {
				json.append(",");
			}
			json.append("{");
				json.append("\"oid\":\"").append(datas.get(i).getOid()).append("\",");
				json.append("\"unitid\":\"").append(datas.get(i).getUnitid()).append("\",");
				json.append("\"name\":\"").append(Util.htmlEncode(datas.get(i).getName())).append("\",");
				json.append("\"enable\":\"").append(datas.get(i).isEnable()).append("\",");
				json.append("\"pass\":\"").append(Util.htmlEncode(datas.get(i).getPass())).append("\",");
				json.append("\"note\":\"").append(Util.htmlEncode(datas.get(i).getNote())).append("\",");
				json.append("\"idate\":\"").append(Util.toTdate(datas.get(i).getIdate())).append("\"");
			json.append("}");
		}
		json.append("]");
		json.append("}");

		return json.toString();
	}

	/**
	 * このメソッドはユニットを登録待機状態にするためのものです。<br>
	 * insert後、unitid = "standby"に変更。
	 * @param oid
	 * @param pass
	 * @return
	 * @throws SQLException
	 */
	public String execUnitWait(String oid, String pass) throws SQLException {

		if(Util.isNullOrBlank(oid) || Util.isNullOrBlank(pass)) return null;

		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ");
		sql.append(DBinfo.TABLE_UNIT).append(" ");
		sql.append("(oid,unitid,name,pass,enable,note) ");
		sql.append("VALUES (");
		sql.append(oid).append(",");
		sql.append("'").append("standby").append("',");
		sql.append("'").append("").append("',");
		sql.append("'").append(Util.toSQLParse(pass)).append("',");
		sql.append("'true',");	// 初回は全部閲覧可
		sql.append("'").append("").append("')");

		// 実行
		DBConnect db = new DBConnect(sql.toString(), false);
		List<Integer> countList = db.ExcuteNonQueryCount();

		// error
		if(countList == null) {
			return null;
		}

		// 更新行数を合算
		int allCount = 0;
		for(int count : countList) {
			allCount += count;
		}

		// JSON
		StringBuilder json = new StringBuilder();
		json.append("{");
		json.append("\"result\":").append("\"success\",");
		json.append("\"message\":").append("\"").append(allCount).append("\",");
		json.append("\"oid\":").append("\"").append(oid).append("\"");
		json.append("}");
		return json.toString();
	}

	/**
	 * 更新を行います。<br>
	 * oidとunitidは必須です。またその他がすべてnullの場合も違反です。nullを返します。
	 * @param oid
	 * @param unitid
	 * @param name
	 * @param pass
	 * @param enable
	 * @param note
	 * @return
	 * @throws SQLException
	 */
	public String update(String oid, String unitid, String name, String pass, String enable, String note) throws SQLException {

		if(Util.isNullOrBlank(oid) || Util.isNullOrBlank(unitid)) return null;
		if(enable != null && (!enable.equals("true") && !enable.equals("false"))) return null;
		if(Util.isNullOrBlank(name) && Util.isNullOrBlank(pass) && Util.isNullOrBlank(enable) && Util.isNullOrBlank(note)) return null;

		StringBuilder set = new StringBuilder();
		if(name != null) set.append("name='").append(name).append("',");
		if(pass != null) set.append("pass='").append(pass).append("',");
		if(enable != null) set.append("enable='").append(enable).append("',");
		if(note != null) set.append("note='").append(note).append("',");
		set.replace(set.lastIndexOf(","), set.length(), " ");	// 末尾のカンマを削除

		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE ");
		sql.append(DBinfo.TABLE_UNIT).append(" ");
		sql.append("SET ");
		sql.append(set.toString());
		sql.append("WHERE ");
		sql.append("oid=").append(oid).append(" AND ");
		sql.append("unitid='").append(unitid).append("'");

		return this.executeUI(sql.toString(), true);
	}

	/**
	 * 削除を行います。<br>
	 * 引数は必須です。違反した場合はnullを返します。
	 * @param oid
	 * @param unitid
	 * @return
	 * @throws SQLException
	 */
	public String delete(String oid, String unitid) throws SQLException {

		if(Util.isNullOrBlank(oid) || Util.isNullOrBlank(unitid)) return null;

		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM ");
		sql.append(DBinfo.TABLE_UNIT).append(" ");
		sql.append("WHERE ");
		sql.append("oid=").append(oid).append(" AND ");
		sql.append("unitid='").append(unitid).append("'");

		return this.executeUI(sql.toString(), true);
	}

	/**
	 * UPDATE,DELETE,INSERT用SQL実行メソッド。
	 * @param sql
	 * @param transaction
	 * @return
	 * @throws SQLException
	 */
	private String executeUI(String sql, boolean transaction) throws SQLException {

		// 実行
		DBConnect db = new DBConnect(sql, transaction);
		List<Integer> countList = db.ExcuteNonQueryCount();

		// error
		if(countList == null) {
			return null;
		}

		// 更新行数を合算
		int allCount = 0;
		for(int count : countList) {
			allCount += count;
		}

		return Util.toSuccessJSON(String.valueOf(allCount));
	}

	public class UnitResult {
		public String JSON;
		public List<Unit> List;
	}

}
