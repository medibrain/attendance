package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import table.Employment;

public class EmploymentDAO extends BaseDAO {

	/**
	 * 全て取得します。カナ昇順に並んでいます。
	 * @param oid
	 * @return
	 */
	public static QueryResult<Employment> GetAll(int oid) throws SQLException {
		String sql = "SELECT * FROM %s WHERE oid=%d ORDER BY kana";
		sql = String.format(sql, DBinfo.TABLE_EMPLOYMENT, oid);
		return GetQueryResult(sql, handler, Employment.class);
	}

	/**
	 * idで検索します。
	 * @param oid
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Employment> FindOfId(int oid, int id) throws SQLException {
		String sql = "SELECT * FROM %s WHERE oid=%d AND id=%d";
		sql = String.format(sql, DBinfo.TABLE_EMPLOYMENT, oid, id);
		return GetQueryResult(sql, handler, Employment.class);
	}

	/**
	 * enableで検索します。カナ昇順に並んでいます。
	 * @param oid
	 * @param enable
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Employment> FindOfEnable(int oid, boolean enable) throws SQLException {
		String sql = "SELECT * FROM %s WHERE oid=%d AND enable=%b ORDER BY kana";
		sql = String.format(sql, DBinfo.TABLE_EMPLOYMENT, oid, enable);
		return GetQueryResult(sql, handler, Employment.class);
	}

	private static DBConnect.ToListHandler handler = new DBConnect.ToListHandler() {
		@SuppressWarnings("unchecked")
		@Override
		public List<Employment> ToList(ResultSet rs) throws SQLException {
			List<Employment> datas = new ArrayList<Employment>();
			Employment employment;
			while(rs.next()) {
				employment = new Employment();
				employment.setId(rs.getInt("id"));
				employment.setOid(rs.getInt("oid"));
				employment.setName(rs.getString("name"));
				employment.setKana(rs.getString("kana"));
				employment.setEnable(rs.getBoolean("enable"));
				employment.setNote(rs.getString("note"));
				employment.setIdate(rs.getInt("idate"));
				employment.setUdate(rs.getInt("udate"));
				datas.add(employment);
			}
			return datas;
		}
	};

	/**
	 * 追加します。<br>
	 * 引数は全て必須です。値のチェックは行いません。
	 * @param oid
	 * @param name
	 * @param kana
	 * @param enable
	 * @param note
	 * @param idate
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult Insert(
			int oid, String name, String kana,
			boolean enable, String note, int idate) throws SQLException {
		String sql =
				"INSERT INTO %s (oid,name,kana,enable,note,idate) " +
				"VALUES (%d,'%s','%s',%b,'%s',%d)";
		sql = String.format(sql, DBinfo.TABLE_EMPLOYMENT, oid, name, kana, enable, note, idate);
		return ExecSQL(sql);
	}

	/**
	 * 更新します。<br>
	 * 引数がnullである場合は更新されません。
	 * @param categoryid
	 * @param name
	 * @param kana
	 * @param enable
	 * @param note
	 * @param udate
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult Update(
			int categoryid, String name, String kana,
			boolean enable, String note, int udate) throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE ").append(DBinfo.TABLE_EMPLOYMENT).append(" SET ");
		if (name != null) sql.append("name='").append(name).append("',");
		if (kana != null) sql.append("kana='").append(kana).append("',");
		sql.append("enable=").append(enable).append(",");
		if (note != null) sql.append("note='").append(note).append("',");
		sql.append("udate=").append(udate).append(" ");
		sql.append("WHERE ");
		sql.append("id=").append(categoryid);
		return ExecSQL(sql.toString());
	}

}
