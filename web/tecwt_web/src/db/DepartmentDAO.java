package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import table.Department;

public class DepartmentDAO extends BaseDAO {

	private static String getSelectFromSQL() {
		StringBuilder sr = new StringBuilder();
		sr.append("SELECT ");
		sr.append(DBinfo.TABLE_DEPARTMENT).append(".*, ");
		sr.append(DBinfo.TABLE_USERS).append(".name AS managername ");
		sr.append("FROM ");
		sr.append(DBinfo.TABLE_DEPARTMENT).append(" ");
		sr.append("LEFT OUTER JOIN ");
		sr.append(DBinfo.TABLE_USERS).append(" ON ")
			.append(DBinfo.TABLE_DEPARTMENT).append(".oid=").append(DBinfo.TABLE_USERS).append(".oid AND ")
			.append(DBinfo.TABLE_DEPARTMENT).append(".manager=").append(DBinfo.TABLE_USERS).append(".uid ");
		return sr.toString();
	}

	/**
	 * 全て取得します。カナ昇順に並んでいます。
	 * @param oid
	 * @return
	 */
	public static QueryResult<Department> GetAll(int oid) throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append(getSelectFromSQL());
		sql.append("WHERE ");
		sql.append(DBinfo.TABLE_DEPARTMENT).append(".oid=").append(oid);
		sql.append("ORDER BY kana");
		return GetQueryResult(sql.toString(), handler, Department.class);
	}

	/**
	 * enableで検索します。カナ昇順に並んでいます。
	 * @param oid
	 * @param enable
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Department> FindOfEnable(int oid, boolean enable) throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append(getSelectFromSQL());
		sql.append("WHERE ");
		sql.append(DBinfo.TABLE_DEPARTMENT).append(".oid=").append(oid).append(" AND ");
		sql.append(DBinfo.TABLE_DEPARTMENT).append(".enable=").append(enable).append(" ");
		sql.append("ORDER BY kana");
		return GetQueryResult(sql.toString(), handler, Department.class);
	}

	/**
	 * idで検索します。
	 * @param oid
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Department> FindOfId(int oid, int id) throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append(getSelectFromSQL());
		sql.append("WHERE ");
		sql.append(DBinfo.TABLE_DEPARTMENT).append(".oid=").append(oid).append(" AND ");
		sql.append(DBinfo.TABLE_DEPARTMENT).append(".id=").append(id).append(" ");
		sql.append("ORDER BY kana");
		return GetQueryResult(sql.toString(), handler, Department.class);
	}

	/**
	 * 追加します。<br>
	 * 引数は全て必須です。値のチェックは行いません。
	 * @param oid
	 * @param name
	 * @param kana
	 * @param manager
	 * @param enable
	 * @param note
	 * @param idate
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult Insert(
			int oid, String name, String kana, int manager,
			boolean enable, String note, int idate) throws SQLException {
		String sql =
				"INSERT INTO department (oid,name,kana,manager,enable,note,idate) " +
				"VALUES (%d,'%s','%s',%d,%b,'%s',%d)";
		sql = String.format(sql, oid, name, kana, manager, enable, note, idate);
		return ExecSQL(sql);
	}

	/**
	 * 更新します。<br>
	 * 引数はnull,-1の場合は更新しません。
	 * @param departmentid
	 * @param name
	 * @param kana
	 * @param manager
	 * @param enable
	 * @param note
	 * @param idate
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult Update(
			int departmentid, String name, String kana, int manager,
			boolean enable, String note, int udate) throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE department SET ");
		if (name != null) sql.append("name='").append(name).append("',");
		if (kana != null) sql.append("kana='").append(kana).append("',");
		if (manager != -1) sql.append("manager=").append(manager).append(",");
		sql.append("enable=").append(enable).append(",");
		if (note != null) sql.append("note='").append(note).append("',");
		sql.append("udate=").append(udate).append(" ");
		sql.append("WHERE ");
		sql.append("id=").append(departmentid);
		return ExecSQL(sql.toString());
	}

	private static DBConnect.ToListHandler handler = new DBConnect.ToListHandler() {
		@SuppressWarnings("unchecked")
		@Override
		public List<Department> ToList(ResultSet rs) throws SQLException {
			List<Department> datas = new ArrayList<Department>();
			Department department;
			while(rs.next()) {
				department = new Department();
				department.setId(rs.getInt("id"));
				department.setOid(rs.getInt("oid"));
				department.setName(rs.getString("name"));
				department.setKana(rs.getString("kana"));
				department.setManager(rs.getInt("manager"));
				department.setEnable(rs.getBoolean("enable"));
				department.setNote(rs.getString("note"));
				department.setIdate(rs.getInt("idate"));
				department.setUdate(rs.getInt("udate"));
				department.setManagername((rs.getString("managername")));
				datas.add(department);
			}
			return datas;
		}
	};

}
