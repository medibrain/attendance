package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import table.Tclog;
import util.Util;

public class TclogDAO implements ResultFormat {

	/**
	 * 全取得。
	 * @return
	 * @throws SQLException
	 */
	public String findAll() throws SQLException {
		return this.execute(this.getSQLText(null, null), false).JSON;
	}

	/**
	 * oidから取得。<br>
	 * oidがnullの場合は全レコードを対象とする。ただしその場合、uidもnullである場合はエラーとなる。<br>
	 * uidのみnullの場合はoidに該当するレコードを対象とする。
	 * @param oid
	 * @return
	 * @throws SQLException
	 */
	public String findOfOidOrUid(String oid, String uid) throws SQLException {

		// WHERE
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		if(oid != null) {
			where.append(DBinfo.TABLE_TCLOG).append(".oid=").append(oid).append(" ");
			if(uid != null) {
				where.append("AND ");
				where.append(DBinfo.TABLE_TCLOG).append(".uid=").append(uid).append(" ");
			}
		}

		return this.execute(this.getSQLText(oid, where.toString()), false).JSON;
	}

	/**
	 * 日付Ａから日付Ｂまでに含まれるログを検索。<br><br>
	 * oidがnullの場合は全レコードを対象とする。ただしその場合、uidもnullである場合はエラーとなる。<br><br>
	 * datesは、"date1"=日付Ａ、"date2"=日付Ｂ。<br>
	 * どちらか片方だけの場合は入っている要素位置によって区別される。<br>
	 * date1だけある場合は「それ以降の日付」を、date2だけある場合は「それまでの日付」を検索する。<br>
	 * keyが割り当てられていない、もしくは値そのものがnullの場合は無視される。<br>
	 * また上記以外のキーに設定されたものは無視される。<br>
	 * @param oid
	 * @param uid
	 * @param dates
	 * @return
	 * @throws SQLException
	 */
	public TclogResult findOfLogdateRange(String oid, String uid, HashMap<String, Integer> dates) throws SQLException {

		// WHERE
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		if(oid != null) {
			where.append(DBinfo.TABLE_TCLOG).append(".oid=").append(oid).append(" ");
			if(uid != null) {
				where.append("AND ");
				where.append(DBinfo.TABLE_TCLOG).append(".uid=").append(uid).append(" ");
			}
		}
		if(dates.containsKey("date1") && dates.get("date1") != null) {
			if(oid != null) { where.append("AND "); }
			where.append(DBinfo.TABLE_TCLOG).append(".logdate>=").append(dates.get("date1")).append(" ");
		}
		if(dates.containsKey("date2") && dates.get("date2") != null) {
			if(oid != null) { where.append("AND "); }
			where.append(DBinfo.TABLE_TCLOG).append(".logdate<=").append(dates.get("date2")).append(" ");
		}

		return this.execute(this.getSQLText(oid, where.toString()), false);
	}

	/**
	 * 指定した時刻Ａから時刻Ｂの範囲に含まれるログを検索。<br><br>
	 * oidがnullの場合は全レコードを対象とする。ただしその場合、uidもnullである場合はエラーとなる。<br><br>
	 * timesは、"time1"=時刻Ａ、"time2"=時刻Ｂ。<br>
	 * どちらか片方だけの場合は入っている要素位置によって区別される。<br>
	 * time1だけある場合は「それ以降の時刻のもの」を、time2だけある場合は「それまでの時刻のもの」を検索する。<br>
	 * keyが割り当てられていない、もしくは値そのものがnullの場合は無視される。<br>
	 * また上記以外のキーに設定されたものは無視される。<br>
	 * @param oid
	 * @param uid
	 * @param times
	 * @return
	 * @throws SQLException
	 */
	public String findOfLogtimeRange(String oid, String uid, HashMap<String, Integer> times) throws SQLException {

		// WHERE
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		if(oid != null) {
			where.append(DBinfo.TABLE_TCLOG).append(".oid=").append(oid).append(" ");
			if(uid != null) {
				where.append("AND ");
				where.append(DBinfo.TABLE_TCLOG).append(".uid=").append(uid).append(" ");
			}
		}
		if(times.containsKey("time1") && times.get("time1") != null) {
			if(oid != null) { where.append("AND "); }
			where.append(DBinfo.TABLE_TCLOG).append(".logtime>=").append(times.get("time1")).append(" ");
		}
		if(times.containsKey("time2") && times.get("time2") != null) {
			if(oid != null) { where.append("AND "); }
			where.append(DBinfo.TABLE_TCLOG).append(".logtime<=").append(times.get("time2")).append(" ");
		}

		return this.execute(this.getSQLText(oid, where.toString()), false).JSON;
	}

	/**
	 * timecardに関連付いたtclogを取得します。<br>
	 * 引数はすべて必須です。違反した場合はnullを返します。<br>
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @return
	 * @throws SQLException
	 */
	public TclogResult findOfUidAndTdate(String oid, String uid, String tdate) throws SQLException {

		// WHERE
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append(DBinfo.TABLE_TCLOG).append(".oid=").append(oid).append(" AND ");
		where.append(DBinfo.TABLE_TCLOG).append(".uid=").append(uid).append(" AND ");
		where.append(DBinfo.TABLE_TCLOG).append(".carddate=").append(tdate).append(" ");

		return this.execute(this.getSQLText(oid, where.toString()), false);
	}

	/**
	 * 完全なSQL文を返します。<br>
	 * whereには"WHERE"と末尾に" "を追加してください。<br>
	 * @param where
	 * @return
	 */
	private String getSQLText(String oid, String where) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT ");
		sql.append(DBinfo.TABLE_TCLOG).append(".*, ");
		sql.append(DBinfo.TABLE_UNIT).append(".name AS unitname ");
		sql.append("FROM ");
		sql.append(DBinfo.TABLE_TCLOG).append(" ");
		sql.append("LEFT OUTER JOIN ").append(DBinfo.TABLE_UNIT).append(" ");
		sql.append("ON ");
		sql.append("(");
		if(oid != null) sql.append(DBinfo.TABLE_TCLOG).append(".oid=").append(DBinfo.TABLE_UNIT).append(".oid").append(" AND ");
		sql.append(DBinfo.TABLE_TCLOG).append(".unitid=").append(DBinfo.TABLE_UNIT).append(".unitid");
		sql.append(") ");
		if(where != null) sql.append(where.toString());
		sql.append("ORDER BY ");
		sql.append(DBinfo.TABLE_TCLOG).append(".logid DESC");

		return sql.toString();
	}

	@SuppressWarnings("unchecked")
	private TclogResult execute(String sql, boolean transaction) throws SQLException {

		// 実行
		DBConnect db = new DBConnect(sql, transaction);
		List<Tclog> resultList = (List<Tclog>)db.ExcuteResult(this);

		// error
		if(resultList == null) {
			return null;
		}

		TclogResult result = new TclogResult();
		result.JSON = this.toJSON(resultList);
		result.List = resultList;

		return result;
	}

	/**
	 * ResultSetオブジェクトをリストに整形する。
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	@Override
	public List<Object> toList(ResultSet resultSet) throws SQLException {
		List<Object> datas = new ArrayList<Object>();
		Tclog tclog;
		while(resultSet.next()) {
			tclog = new Tclog();
			tclog.setOid(resultSet.getInt("oid"));
			tclog.setUid(resultSet.getInt("uid"));
			tclog.setLogtype(resultSet.getString("logtype"));
			tclog.setFunc(resultSet.getString("func"));
			tclog.setLogdate(resultSet.getInt("logdate"));
			tclog.setLogtime(resultSet.getInt("logtime"));
			tclog.setLatitude(resultSet.getString("latitude"));
			tclog.setLongitude(resultSet.getString("longitude"));
			tclog.setUnitid(resultSet.getString("unitid"));
			tclog.setUnitname(resultSet.getString("unitname"));
			datas.add(tclog);
		}

		return datas;
	}

	/**
	 * JSON形式に変換
	 * @param datas
	 * @return
	 */
	public String toJSON(List<Tclog> datas) {

		// JSON形式に成型
		StringBuilder json = new StringBuilder();

		json.append("{");
		json.append("\"result\":\"success\",");
		json.append("\"datas\":[");
		for(int i=0; i<datas.size(); i++) {
			if(i > 0) {
				json.append(",");
			}
			json.append("{");
				json.append("\"oid\":\"").append(datas.get(i).getOid()).append("\",");
				json.append("\"uid\":\"").append(datas.get(i).getUid()).append("\",");
				json.append("\"logtype\":\"").append(Util.jsonParse(datas.get(i).getLogtype())).append("\",");
				json.append("\"func\":\"").append(Util.htmlEncode(Util.jsonParse(datas.get(i).getFunc()))).append("\",");
				json.append("\"logdate\":\"").append(Util.toTdate(datas.get(i).getLogdate())).append("\",");
				json.append("\"carddate\":\"").append(Util.toTdate(datas.get(i).getCarddate())).append("\",");
				json.append("\"logtime\":\"").append(Util.toTime(datas.get(i).getLogtime())).append("\",");
				json.append("\"latitude\":\"").append(datas.get(i).getLatitude()).append("\",");
				json.append("\"longitude\":\"").append(datas.get(i).getLongitude()).append("\",");
				json.append("\"unitname\":\"").append(datas.get(i).getUnitname()).append("\"");
			json.append("}");
		}
		json.append("]");
		json.append("}");

		return json.toString();
	}

	/**
	 * このメソッドはtclogにINSERTするためのSQL文を提供します。<br>
	 * 必要なパラメータを引数に与えてください。
	 * @param oid
	 * @param uid
	 * @param logtype
	 * @param func
	 * @param logdate
	 * @param logtime
	 * @param latitude
	 * @param longitude
	 * @param carddate
	 * @return
	 */
	public static String toInsertSQLText(
			String oid, String uid, String logtype, String func, String logdate, String logtime, String latitude, String longitude, String carddate) {

		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ");
		sql.append(DBinfo.TABLE_TCLOG).append(" ");
		sql.append("(oid, uid, logtype, func, logdate, logtime, latitude, longitude, carddate) ");
		sql.append("VALUES (");
		sql.append(oid).append(",");
		sql.append(uid).append(",");
		sql.append("'").append(logtype).append("',");
		sql.append("'").append(Util.toSQLParse(func)).append("',");
		sql.append(logdate).append(",");
		sql.append(logtime).append(",");
		sql.append("'").append(latitude).append("',");
		sql.append("'").append(longitude).append("',");
		sql.append(carddate).append(")");

		return sql.toString();
	}

	public class TclogResult {
		public String JSON;
		public List<Tclog> List;
	}

}
