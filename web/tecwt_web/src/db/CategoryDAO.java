package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import table.Category;

public class CategoryDAO extends BaseDAO {

	/**
	 * 全て取得します。カナ昇順に並んでいます。
	 * @param oid
	 * @return
	 */
	public static QueryResult<Category> GetAll(int oid) throws SQLException {
		String sql = "SELECT * FROM %s WHERE oid=%d ORDER BY kana";
		sql = String.format(sql, DBinfo.TABLE_CATEGORY, oid);
		return GetQueryResult(sql, handler, Category.class);
	}

	/**
	 * idで検索します。
	 * @param oid
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Category> FindOfId(int oid, int id) throws SQLException {
		String sql = "SELECT * FROM %s WHERE oid=%d AND id=%d";
		sql = String.format(sql, DBinfo.TABLE_CATEGORY, oid, id);
		return GetQueryResult(sql, handler, Category.class);
	}

	/**
	 * enableで検索します。カナ昇順に並んでいます。
	 * @param oid
	 * @param enable
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Category> FindOfEnable(int oid, boolean enable) throws SQLException {
		String sql = "SELECT * FROM %s WHERE oid=%d AND enable=%b ORDER BY kana";
		sql = String.format(sql, DBinfo.TABLE_CATEGORY, oid, enable);
		return GetQueryResult(sql, handler, Category.class);
	}

	private static DBConnect.ToListHandler handler = new DBConnect.ToListHandler() {
		@SuppressWarnings("unchecked")
		@Override
		public List<Category> ToList(ResultSet rs) throws SQLException {
			List<Category> datas = new ArrayList<Category>();
			Category category;
			while(rs.next()) {
				category = new Category();
				category.setId(rs.getInt("id"));
				category.setOid(rs.getInt("oid"));
				category.setName(rs.getString("name"));
				category.setKana(rs.getString("kana"));
				category.setEnable(rs.getBoolean("enable"));
				category.setNote(rs.getString("note"));
				category.setIdate(rs.getInt("idate"));
				category.setUdate(rs.getInt("udate"));
				datas.add(category);
			}
			return datas;
		}
	};

	/**
	 * 追加します。<br>
	 * 引数は全て必須です。値のチェックは行いません。
	 * @param oid
	 * @param name
	 * @param kana
	 * @param enable
	 * @param note
	 * @param idate
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult Insert(
			int oid, String name, String kana,
			boolean enable, String note, int idate) throws SQLException {
		String sql =
				"INSERT INTO %s (oid,name,kana,enable,note,idate) " +
				"VALUES (%d,'%s','%s',%b,'%s',%d)";
		sql = String.format(sql, DBinfo.TABLE_CATEGORY, oid, name, kana, enable, note, idate);
		return ExecSQL(sql);
	}

	/**
	 * 更新します。<br>
	 * 引数がnullである場合は更新されません。
	 * @param categoryid
	 * @param name
	 * @param kana
	 * @param enable
	 * @param note
	 * @param udate
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult Update(
			int categoryid, String name, String kana,
			boolean enable, String note, int udate) throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE ").append(DBinfo.TABLE_CATEGORY).append(" SET ");
		if (name != null) sql.append("name='").append(name).append("',");
		if (kana != null) sql.append("kana='").append(kana).append("',");
		sql.append("enable=").append(enable).append(",");
		if (note != null) sql.append("note='").append(note).append("',");
		sql.append("udate=").append(udate).append(" ");
		sql.append("WHERE ");
		sql.append("id=").append(categoryid);
		return ExecSQL(sql.toString());
	}

}
