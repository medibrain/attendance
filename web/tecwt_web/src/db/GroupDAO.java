package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import table.Group;

public class GroupDAO extends BaseDAO {

	/**
	 * 全て取得します。カナ昇順に並んでいます。
	 * @param oid
	 * @return
	 */
	public static QueryResult<Group> GetAll(int oid) throws SQLException {
		String sql = "SELECT * FROM %s WHERE oid=%d ORDER BY kana";
		sql = String.format(sql, DBinfo.TABLE_GROUP, oid);
		return GetQueryResult(sql, handler, Group.class);
	}

	/**
	 * idで検索します。
	 * @param oid
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Group> FindOfId(int oid, int id) throws SQLException {
		String sql = "SELECT * FROM %s WHERE oid=%d AND id=%d";
		sql = String.format(sql, DBinfo.TABLE_GROUP, oid, id);
		return GetQueryResult(sql, handler, Group.class);
	}

	/**
	 * enableで検索します。カナ昇順に並んでいます。
	 * @param oid
	 * @param enable
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Group> FindOfEnable(int oid, boolean enable) throws SQLException {
		String sql = "SELECT * FROM %s WHERE oid=%d AND enable=%b ORDER BY kana";
		sql = String.format(sql, DBinfo.TABLE_GROUP, oid, enable);
		return GetQueryResult(sql, handler, Group.class);
	}

	/**
	 * 指定したid群にマッチするものを取得します。カナ昇順に並んでいます。
	 * @param oid
	 * @param ids
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Group> FindOfIdList(int oid, List<Integer> ids) throws SQLException {
		String sql = "SELECT * FROM %s WHERE oid=%d AND id IN %s ORDER BY kana";	//IN:候補マッチ
		sql = String.format(sql, DBinfo.TABLE_GROUP, oid, ids.toString());	//List#toString():自動正規化[,,,]
		return GetQueryResult(sql, handler, Group.class);
	}

	private static DBConnect.ToListHandler handler = new DBConnect.ToListHandler() {
		@SuppressWarnings("unchecked")
		@Override
		public List<Group> ToList(ResultSet rs) throws SQLException {
			List<Group> datas = new ArrayList<Group>();
			Group group;
			while(rs.next()) {
				group = new Group();
				group.setId(rs.getInt("id"));
				group.setOid(rs.getInt("oid"));
				group.setName(rs.getString("name"));
				group.setKana(rs.getString("kana"));
				group.setEnable(rs.getBoolean("enable"));
				group.setNote(rs.getString("note"));
				group.setIdate(rs.getInt("idate"));
				group.setUdate(rs.getInt("udate"));
				datas.add(group);
			}
			return datas;
		}
	};

	/**
	 * 追加します。<br>
	 * 引数は全て必須です。値のチェックは行いません。
	 * @param oid
	 * @param name
	 * @param kana
	 * @param enable
	 * @param note
	 * @param idate
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult Insert(
			int oid, String name, String kana,
			boolean enable, String note, int idate) throws SQLException {
		String sql =
				"INSERT INTO %s (oid,name,kana,enable,note,idate) " +
				"VALUES (%d,'%s','%s',%b,'%s',%d)";
		sql = String.format(sql, DBinfo.TABLE_GROUP, oid, name, kana, enable, note, idate);
		return ExecSQL(sql);
	}

	/**
	 * 更新します。<br>
	 * 引数がnullである場合は更新されません。
	 * @param categoryid
	 * @param name
	 * @param kana
	 * @param enable
	 * @param note
	 * @param udate
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult Update(
			int categoryid, String name, String kana,
			boolean enable, String note, int udate) throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE ").append(DBinfo.TABLE_GROUP).append(" SET ");
		if (name != null) sql.append("name='").append(name).append("',");
		if (kana != null) sql.append("kana='").append(kana).append("',");
		sql.append("enable=").append(enable).append(",");
		if (note != null) sql.append("note='").append(note).append("',");
		sql.append("udate=").append(udate).append(" ");
		sql.append("WHERE ");
		sql.append("id=").append(categoryid);
		return ExecSQL(sql.toString());
	}

}
