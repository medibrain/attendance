package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import table.Customer;

public class CustomerDAO extends BaseDAO {

	/**
	 * 全て取得します。カナ昇順に並んでいます。
	 * @param oid
	 * @return
	 */
	public static QueryResult<Customer> GetAll(int oid) throws SQLException {
		String sql = "SELECT * FROM %s WHERE oid=%d ORDER BY kana";
		sql = String.format(sql, DBinfo.TABLE_CUSTOMER, oid);
		return GetQueryResult(sql, handler, Customer.class);
	}

	/**
	 * idで検索します。
	 * @param oid
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Customer> FindOfId(int oid, int id) throws SQLException {
		String sql = "SELECT * FROM %s WHERE oid=%d AND id=%d";
		sql = String.format(sql, DBinfo.TABLE_CUSTOMER, oid, id);
		return GetQueryResult(sql, handler, Customer.class);
	}

	/**
	 * enableで検索します。カナ昇順に並んでいます。
	 * @param oid
	 * @param enable
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Customer> FindOfEnable(int oid, boolean enable) throws SQLException {
		String sql = "SELECT * FROM %s WHERE oid=%d AND enable=%b ORDER BY kana";
		sql = String.format(sql, DBinfo.TABLE_CUSTOMER, oid, enable);
		return GetQueryResult(sql, handler, Customer.class);
	}

	private static DBConnect.ToListHandler handler = new DBConnect.ToListHandler() {
		@SuppressWarnings("unchecked")
		@Override
		public List<Customer> ToList(ResultSet rs) throws SQLException {
			List<Customer> datas = new ArrayList<Customer>();
			Customer customer;
			while(rs.next()) {
				customer = new Customer();
				customer.setId(rs.getInt("id"));
				customer.setOid(rs.getInt("oid"));
				customer.setName(rs.getString("name"));
				customer.setKana(rs.getString("kana"));
				customer.setEnable(rs.getBoolean("enable"));
				customer.setNote(rs.getString("note"));
				customer.setIdate(rs.getInt("idate"));
				customer.setUdate(rs.getInt("udate"));
				datas.add(customer);
			}
			return datas;
		}
	};

	/**
	 * 追加します。<br>
	 * 引数は全て必須です。値のチェックは行いません。
	 * @param oid
	 * @param name
	 * @param kana
	 * @param enable
	 * @param note
	 * @param idate
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult Insert(
			int oid, String name, String kana,
			boolean enable, String note, int idate) throws SQLException {
		String sql =
				"INSERT INTO %s (oid,name,kana,enable,note,idate) " +
				"VALUES (%d,'%s','%s',%b,'%s',%d)";
		sql = String.format(sql, DBinfo.TABLE_CUSTOMER, oid, name, kana, enable, note, idate);
		return ExecSQL(sql);
	}

	/**
	 * 更新します。<br>
	 * 引数がnullである場合は更新されません。
	 * @param categoryid
	 * @param name
	 * @param kana
	 * @param enable
	 * @param note
	 * @param udate
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult Update(
			int categoryid, String name, String kana,
			boolean enable, String note, int udate) throws SQLException {
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE ").append(DBinfo.TABLE_CUSTOMER).append(" SET ");
		if (name != null) sql.append("name='").append(name).append("',");
		if (kana != null) sql.append("kana='").append(kana).append("',");
		sql.append("enable=").append(enable).append(",");
		if (note != null) sql.append("note='").append(note).append("',");
		sql.append("udate=").append(udate).append(" ");
		sql.append("WHERE ");
		sql.append("id=").append(categoryid);
		return ExecSQL(sql.toString());
	}

}
