package db;

public interface DBinfo {

	//TODO: medi-kintai
	public static final String URL = "jdbc:postgresql://133.167.104.74:5432/tecwt";
	//public static final String URL = "jdbc:postgresql://160.16.75.99:5432/tecwt";
	public static final String USER = "postgres";
	//public static final String USER = "tecadmin";
	public static final String PASSWORD = "NiCE2018";
	//public static final String PASSWORD = "Tec242-Post";

	public static final String TABLE_TIMECARD = "timecard";
	public static final String TABLE_ORGANIZATION = "organization";
	public static final String TABLE_USERS = "users";
	public static final String TABLE_WORKTYPE = "worktype";
	public static final String TABLE_TCLOG = "tclog";
	public static final String TABLE_LOG = "log";
	public static final String TABLE_NFC = "nfc";
	public static final String TABLE_NFCWAIT = "nfcwait";
	public static final String TABLE_UNIT = "unit";
	public static final String TABLE_DEPARTMENT = "department";
	public static final String TABLE_CATEGORY = "category";
	public static final String TABLE_CUSTOMER = "customer";
	public static final String TABLE_EMPLOYMENT = "employment";
	public static final String TABLE_WORKLOCATION = "worklocation";
	public static final String TABLE_GROUP = "usergroup";	// groupが予約語のため

	// 日毎の作業実績
	public static final String TABLE_DAILY_WORK_RESULT = "daily_work_result";
	// 作業内容マスタ
	public static final String TABLE_WORK_DETAIL = "work_detail";
	// Freee連携用
	public static final String TABLE_LINK_FREEE = "link_freee";
}
