package db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.tomcat.jdbc.pool.DataSource;	// 参考URL: https://tomcat.apache.org/tomcat-8.0-doc/api/org/apache/tomcat/jdbc/pool/DataSourceProxy.html
import org.apache.tomcat.jdbc.pool.PoolProperties;	// 参考URL: https://tomcat.apache.org/tomcat-8.0-doc/api/org/apache/tomcat/jdbc/pool/PoolProperties.html

import util.Log;



public class DBConnect {

	public static DataSource datasource;

	static {
		PoolProperties p = new PoolProperties();

		// 接続情報の設定
		p.setUrl(DBinfo.URL);
		p.setDriverClassName("org.postgresql.Driver");
		p.setUsername(DBinfo.USER);
		p.setPassword(DBinfo.PASSWORD);

		// その他オプション
		p.setJmxEnabled(true);						// プールをConncectionPoolMBeanとして登録
		p.setTestWhileIdle(true);					// アイドル状態のコネクションの有効チェックを行う
		p.setMinEvictableIdleTimeMillis(3000);		// アイドル状態のコネクションの生存期間(ミリ秒)
		p.setTimeBetweenEvictionRunsMillis(30000);	// アイドル状態の接続チェックを行う間隔(ミリ秒)
		p.setTestOnBorrow(true);					// プール貸出時に接続有効チェックを行う
		p.setTestOnReturn(true);					// プール返却時に接続有効チェックを行う
		p.setValidationQuery("SELECT 1");			// このSQL文を利用して接続有効チェックを行う
		p.setValidationInterval(30000);				// 指定秒数、前回のチェックから経過しているコネクションは有効チェックする
		p.setInitialSize(5);						// プール起動時に作成されるコネクション数
		p.setMaxActive(100);						// 最大保持コネクション数
		p.setMinIdle(5);							// 最小保持コネクション数=initialSize
		p.setMaxWait(1000);							// 利用可能なコネクションがない場合に待機する時間(ミリ秒)
		p.setRemoveAbandoned(true);					// close漏れ検査
		p.setLogAbandoned(true);					// close漏れがある場合はログ出力
		p.setRemoveAbandonedTimeout(60);			// close漏れとみなすまでの時間(秒)
		p.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"
				+ "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");

		datasource = new DataSource();
		datasource.setPoolProperties(p);
	}

	public interface ToListHandler {
		public abstract <T> List<T> ToList(ResultSet rs) throws SQLException;
	}


	private Connection conn;
	private List<String> sqlList;
	private boolean tran;

	/**
	 * 単一のSQL文を実行できます。<br>
	 * @param sql
	 * @param transaction
	 * @throws SQLException
	 */
	public DBConnect(String sql, boolean transaction) throws SQLException {
		this.tran = transaction;
		this.sqlList = new ArrayList<String>();
		this.sqlList.add(sql);
		this.open();
		if(transaction) {
			this.conn.setAutoCommit(false);
		}
	}

	/**
	 * 複数のSQL文を一度のコネクションで実行できます。<br>
	 * @param sqlList
	 * @param transaction
	 * @throws SQLException
	 */
	public DBConnect(List<String> sqlList, boolean transaction) throws SQLException {
		this.tran = transaction;
		this.sqlList = sqlList;
		this.open();
		if(transaction) {
			this.conn.setAutoCommit(false);
		}
	}

	private void open() throws SQLException {
		this.conn = datasource.getConnection();
	}

	/**
	 * 更新(UPDATE)・削除(DELETE)を行います。<br>
	 * 失敗した場合はfalseを返します。
	 * @return
	 * @throws SQLException
	 */
	public boolean ExcuteNonQuery() throws SQLException {
		Statement statement;
		try {
			// 登録されているSQL全部実行
			for(String sql : sqlList) {
				statement = this.conn.createStatement();
				statement.executeUpdate(sql);
				statement.close();
			}
			if(this.tran) {
				this.conn.commit();
			}

		} catch (SQLException ex) {
			if(this.tran) {
				this.conn.rollback();
			}
			Log.exceptionWrite(ex);
			return false;

		} finally {
			// コネクション開放
			try {
				if(this.conn != null) {
					this.conn.setAutoCommit(true);	// オートコミットを毎回trueにしておく
					this.conn.close();
				}
			} catch(SQLException ex) {}
		}

		return true;
	}

	/**
	 * 更新(UPDATE)・削除(DELETE)を行います。<br>
	 * 成功時には処理を行った行数のリストを返します。失敗時はnullを返します。<br>
	 * あくまで行数であり、どの行が実行されたのかは分かりません。<br>
	 * @return
	 * @throws SQLException
	 */
	public List<Integer> ExcuteNonQueryCount() throws SQLException {
		ArrayList<Integer> countList = new ArrayList<Integer>();
		Statement statement;
		try {
			// 登録されているSQL全部実行
			int count;
			for(String sql : sqlList) {
				statement = this.conn.createStatement();
				count = statement.executeUpdate(sql);
				countList.add(count);
				statement.close();
			}
			if(this.tran) {
				this.conn.commit();
			}

		} catch (SQLException ex) {
			if(this.tran) {
				this.conn.rollback();
			}
			Log.exceptionWrite(ex);
			ex.printStackTrace();

		} finally {
			try {
				if(this.conn != null) {
					this.conn.setAutoCommit(true);
					this.conn.close();
				}
			} catch(SQLException ex) {}
		}

		return countList;
	}

	/**
	 * 検索(SELECT)を行います。<br>
	 * 成功時には検索結果をリスト化したものを返します。<br>
	 * リスト化には引数に指定したフォーマット(toListメソッド)を使用します。<br>
	 * <br>
	 * ※注意<br>
	 * このメソッドでは登録されているSQL文のうち、第一要素（先頭）のものしか実行しません。<br>
	 * なぜならResultSetは別のstatementが実行されると前のものは消えてしまうからです。<br>
	 * ですから、このメソッドを使う場合は必ず単一のSQL文を登録するようにしてください。<br>
	 * @return
	 * @throws SQLException
	 */
	public List<?> ExcuteResult(ResultFormat format) {
		List<?> resultList = new ArrayList<>();
		Statement statement;
		try {

			// SQL実行
			statement = this.conn.createStatement();
			ResultSet resultSet = statement.executeQuery(sqlList.get(0));
			resultList = format.toList(resultSet);
			resultSet.close();
			statement.close();

			// 問題なければコミット
			if(this.tran) {
				this.conn.commit();
			}

		} catch (SQLException ex) {
			// 問題あればロールバック
			if(this.tran) {
				try {
					this.conn.rollback();
				} catch (SQLException ex2) {
					// ロールバックすら無理なら何もしない
					ex2.printStackTrace();
				}
			}
			Log.exceptionWrite(ex);
			ex.printStackTrace();

		} finally {
			try {
				if(this.conn != null) {
					this.conn.setAutoCommit(true);
					this.conn.close();
				}
			} catch(SQLException ex) {}
		}

		return resultList;
	}

	public <T> List<T> ExcuteResult(ToListHandler handler) {
		List<T> resultList = new ArrayList<>();
		Statement statement;
		try {

			// SQL実行
			statement = this.conn.createStatement();
			ResultSet resultSet = statement.executeQuery(sqlList.get(0));
			resultList = handler.ToList(resultSet);
			resultSet.close();
			statement.close();

			// 問題なければコミット
			if(this.tran) {
				this.conn.commit();
			}

		} catch (SQLException ex) {
			// 問題あればロールバック
			if(this.tran) {
				try {
					this.conn.rollback();
				} catch (SQLException ex2) {
					// ロールバックすら無理なら何もしない
					ex2.printStackTrace();
				}
			}
			Log.exceptionWrite(ex);
			ex.printStackTrace();

		} finally {
			try {
				if(this.conn != null) {
					this.conn.setAutoCommit(true);
					this.conn.close();
				}
			} catch(SQLException ex) {}
		}

		return resultList;
	}

}
