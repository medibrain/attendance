package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import table.Users;
import util.DateTime;
import util.Util;

public class UsersDAO extends BaseDAO {

	/**
	 * 全検索
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Users> FindAll() throws SQLException {
		String sql = getSQLText("");
		return GetQueryResult(sql, handler, Users.class);
	}

	/**
	 * oidからユーザ一覧を取得。
	 * @param oid
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Users> FindOfOid(String oid) throws SQLException {
		StringBuilder where = new StringBuilder();
		where.append("WHERE ").append(DBinfo.TABLE_ORGANIZATION).append(".oid=").append(oid).append(" ");
		String sql = getSQLText(where.toString());
		return GetQueryResult(sql, handler, Users.class);
	}

	/**
	 * enable=trueなユーザ一覧を取得
	 * @param oid
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Users> FindOfEnable(String oid) throws SQLException {
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append(DBinfo.TABLE_ORGANIZATION).append(".oid=").append(oid).append(" AND ");
		where.append(DBinfo.TABLE_USERS).append(".enable=true ");
		String sql = getSQLText(where.toString());
		return GetQueryResult(sql, handler, Users.class);
	}

	/**
	 * 指定したuidを持つレコードを返します。<br>
	 * 引数はすべて必須です。違反した場合はnullを返します。<br>
	 * @param oid
	 * @param uid
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Users> FindOfUid(String oid, String uid) throws SQLException {
		if(Util.isNullOrBlank(oid) || Util.isNullOrBlank(uid)) return null;
		// WHERE句
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append(DBinfo.TABLE_ORGANIZATION).append(".oid=").append(oid).append(" AND ");
		where.append(DBinfo.TABLE_USERS).append(".uid=").append(uid).append(" ");
		// SQL文
		String sql = getSQLText(where.toString());
		return GetQueryResult(sql, handler, Users.class);
	}

	/**
	 * 指定したuidを持つレコードを返します。<br>
	 * 引数はすべて必須です。違反した場合はnullを返します。
	 * @param oid
	 * @param uids
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Users> FindOfUids(String oid, List<String> uids) throws SQLException {
		if(Util.isNullOrBlank(oid) || uids == null) return null;
		// WHERE句
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append(DBinfo.TABLE_ORGANIZATION).append(".oid=").append(oid);
		if(uids.size() > 0)	where.append(" AND (");
		for(int i=0; i<uids.size(); i++) {
			if(i != 0) {
				where.append(" OR ");
			}
			where.append(DBinfo.TABLE_USERS).append(".uid=").append(uids.get(i));
		}
		if(uids.size() > 0)	where.append(") ");
		// SQL文
		String sql = getSQLText(where.toString());
		return GetQueryResult(sql, handler, Users.class);
	}

	/**
	 * 日付の範囲から作成日に該当するレコード一覧を返します。<br>
	 * oidがnullの場合、nullを返します。<br>
	 * 以下に利用可能な検索条件を記述します。以下に合致しないnullがあった場合はnullを返します。<br>
	 * ①date1～<br>
	 * ②date1～date2<br>
	 * ③～date2<br>
	 * @param oid
	 * @param date1
	 * @param date2
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Users> FindOfDateRange(String oid, String date1, String date2) throws SQLException {
		if(oid == null) return null;
		if(date1 == null && date2 == null) return null;
		// WHERE句
		StringBuilder where = new StringBuilder();
		StringBuilder range = new StringBuilder();
		if(date1 != null) {
			if(date2 == null) {
				// ①
				range.append("AND ").append(DBinfo.TABLE_USERS).append(".idate>=").append(date1).append(" ");
			}
			else {
				// ②
				range.append("AND ").append(DBinfo.TABLE_USERS).append(".idate>=").append(date1).append(" ");
				range.append("AND ").append(DBinfo.TABLE_USERS).append(".idate<=").append(date2).append(" ");
			}
		}
		else {
			// ③
			range.append("AND ").append(DBinfo.TABLE_USERS).append(".idate<=").append(date2).append(" ");
		}
		where.append("WHERE ").append(DBinfo.TABLE_ORGANIZATION).append(".oid=").append(oid).append(" ");
		where.append(range.toString());
		// SQL文作成
		String sql = getSQLText(where.toString());
		return GetQueryResult(sql, handler, Users.class);
	}

	/**
	 * 指定したcodeを持つレコードを返します。<br>
	 * 引数は必須です。""も許可されません。違反した場合はnullを返します。<br>
	 * @param oid
	 * @param code
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Users> FindOfCode(String oid, String code) throws SQLException {
		if(Util.isNullOrBlank(oid) && Util.isNullOrBlank(code)) return null;
		// WHERE句
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append(DBinfo.TABLE_ORGANIZATION).append(".oid=").append(oid).append(" AND ");
		where.append(DBinfo.TABLE_USERS).append(".code='").append(code).append("' ");
		// SQL文
		String sql = getSQLText(where.toString());
		return GetQueryResult(sql, handler, Users.class);
	}

	/**
	 * メール打刻許可のフラグで検索します。<br>
	 * mailenableはboolean値でなければなりません。<br>
	 * また値のチェックは行いません。 throws SQLException
	 * @param oid
	 * @param mailenable
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Users> FindOfMailenable(String oid, String mailenable) throws SQLException {
		if(Util.isNullOrBlank(oid) && Util.isNullOrBlank(mailenable)) return null;
		// WHERE句
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append(DBinfo.TABLE_ORGANIZATION).append(".oid=").append(oid).append(" AND ");
		where.append(DBinfo.TABLE_USERS).append(".mailenable=").append(mailenable).append(" ");
		// SQL文
		String sql = getSQLText(where.toString());
		return GetQueryResult(sql, handler, Users.class);
	}

	/**
	 * 指定したlnameを持つレコードを返します。<br>
	 * lnameはユニークであるため、返るレコード数は１行以下となります。<br>
	 * そのため、指定したlnameが使用できるかを判断する助けになります。<br>
	 * 引数はnullと""を許可しません。違反した場合はnullを返します。
	 * @param lname
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Users> FindOfLname(String oid, String lname) throws SQLException {
		if(Util.isNullOrBlank(lname)) return null;
		// WHERE句
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append(DBinfo.TABLE_ORGANIZATION).append(".oid=").append(oid).append(" AND ");
		where.append(DBinfo.TABLE_USERS).append(".lname='").append(lname).append("' ");
		// SQL文
		String sql = getSQLText(where.toString());
		return GetQueryResult(sql, handler, Users.class);
	}

	/**
	 * 指定したenable(true/false)を持つレコードを返します。<br>
	 * 引数はすべて必須です。違反した場合はnullを返します。<br>
	 * enableは便宜上Stringとしていますが、必ず"true"または"false"で指定してください。<br>
	 * それ以外はすべてnullとして扱います。<br>
	 * @param oid
	 * @param enable
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Users> FindOfEnable(String oid, String enable) throws SQLException {
		if(enable != null && !(enable.equals("true") || enable.equals("false"))) return null;
		if(Util.isNullOrBlank(oid) || Util.isNullOrBlank(enable)) return null;
		// WHERE句
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append(DBinfo.TABLE_ORGANIZATION).append(".oid=").append(oid).append(" AND ");
		where.append(DBinfo.TABLE_USERS).append(".enable=").append(enable).append(" ");
		// SQL文
		String sql = getSQLText(where.toString());
		return GetQueryResult(sql, handler, Users.class);
	}

	/**
	 * このメソッドはログイン用です。<br>
	 * 戻り値はloginプロパティ("done" or "failed")を追加したJSONとなります。<br>
	 * lnameはユニークであるため、返るレコード数は1か0となります。<br>
	 * 引数はすべて必須項目です。違反した場合はnullを返します。<br>
	 * @param oid
	 * @param lname
	 * @param pass
	 * @return
	 * @throws SQLException
	 */
	public static QueryResult<Users> ExecLogin(int oid, String lname, String pass) throws SQLException {
		if(Util.isNullOrBlank(lname) || Util.isNullOrBlank(pass)) {
			return null;
		}
		// WHERE句
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append(DBinfo.TABLE_USERS).append(".oid=").append(oid).append(" AND ");
		where.append(DBinfo.TABLE_USERS).append(".lname='").append(lname).append("' AND ");
		where.append(DBinfo.TABLE_USERS).append(".pass='").append(pass).append("' AND ");
		where.append(DBinfo.TABLE_USERS).append(".enable=").append(true).append(" ");
		// SQL文
		String sql = getSQLText(where.toString());
		// 実行
		List<Users> list = GetQueryResultList(sql, handler, Users.class);
		if (list == null) return null;
		// パラメータ
		Map<String, String> params = new HashMap<String, String>();
		params.put("login", (list.size() == 1 ? "done" : "failed"));
		//結果
		QueryResult<Users> qr = new QueryResult<Users>();
		qr.List = list;
		qr.JSON = ToJSON(list, Users.class, params);
		return qr;
	}

	/**
	 * 引数のwhere句("WHERE"と末尾のスペース含む)を内蔵した完全なSQL文を作成します。<br>
	 * usersテーブルのカラムにidmcount,onameが追加されています。
	 * @param where
	 * @return
	 */
	private static String getSQLText(String where) {

		// SQL文
		StringBuilder s = new StringBuilder();
		s.append("SELECT ");
		s.append(DBinfo.TABLE_USERS).append(".*, ");
		s.append("CASE WHEN n.count IS NULL THEN 0 ELSE n.count END AS idmcount, ");
		s.append(DBinfo.TABLE_ORGANIZATION).append(".name AS oname, ");
		s.append("CASE WHEN ").append(DBinfo.TABLE_NFCWAIT).append(".oid IS NOT NULL THEN true ELSE false END AS nfcwait, ");	// nfcwait者フラグ
		s.append(DBinfo.TABLE_DEPARTMENT).append(".name AS departmentname, ");
		s.append(DBinfo.TABLE_EMPLOYMENT).append(".name AS employmentname, ");
		s.append(DBinfo.TABLE_WORKLOCATION).append(".name AS worklocationname, ");
		s.append(DBinfo.TABLE_GROUP).append(".name AS groupname ");
		s.append("FROM ");
		s.append(DBinfo.TABLE_USERS).append(" ");
		s.append("LEFT OUTER JOIN ").append(DBinfo.TABLE_ORGANIZATION).append(" USING(oid) ");
		s.append("LEFT OUTER JOIN (SELECT oid,uid,COUNT(*) AS count FROM ").append(DBinfo.TABLE_NFC).append(" GROUP BY oid,uid) AS n USING(oid,uid) ");	// nfc所持数取得
		s.append("LEFT OUTER JOIN ").append(DBinfo.TABLE_NFCWAIT).append(" USING(oid,uid) ");
		s.append("LEFT OUTER JOIN ").append(DBinfo.TABLE_DEPARTMENT).append(" ON ")
			.append(DBinfo.TABLE_USERS).append(".oid = ").append(DBinfo.TABLE_DEPARTMENT).append(".oid AND ")
			.append(DBinfo.TABLE_USERS).append(".department = ").append(DBinfo.TABLE_DEPARTMENT).append(".id ");
		s.append("LEFT OUTER JOIN ").append(DBinfo.TABLE_EMPLOYMENT).append(" ON ")
			.append(DBinfo.TABLE_USERS).append(".oid = ").append(DBinfo.TABLE_EMPLOYMENT).append(".oid AND ")
			.append(DBinfo.TABLE_USERS).append(".employment = ").append(DBinfo.TABLE_EMPLOYMENT).append(".id ");
		s.append("LEFT OUTER JOIN ").append(DBinfo.TABLE_WORKLOCATION).append(" ON ")
			.append(DBinfo.TABLE_USERS).append(".oid = ").append(DBinfo.TABLE_WORKLOCATION).append(".oid AND ")
			.append(DBinfo.TABLE_USERS).append(".worklocation = ").append(DBinfo.TABLE_WORKLOCATION).append(".id ");
		s.append("LEFT OUTER JOIN ").append(DBinfo.TABLE_GROUP).append(" ON ")
			.append(DBinfo.TABLE_USERS).append(".oid = ").append(DBinfo.TABLE_GROUP).append(".oid AND ")
			.append(DBinfo.TABLE_USERS).append(".usergroup = ").append(DBinfo.TABLE_GROUP).append(".id ");
		if(where != null) s.append(where);
		s.append("ORDER BY ");
		s.append(DBinfo.TABLE_USERS).append(".kana ASC");

		return s.toString();
	}

	private static DBConnect.ToListHandler handler = new DBConnect.ToListHandler() {
		@SuppressWarnings("unchecked")
		@Override
		public List<Users> ToList(ResultSet rs) throws SQLException {
			List<Users> datas = new ArrayList<Users>();
			Users users;
			while(rs.next()) {
				users = new Users();
				users.setOid(rs.getInt("oid"));
				users.setUid(rs.getInt("uid"));
				users.setCode(rs.getString("code"));
				users.setLname(rs.getString("lname"));
				users.setName(rs.getString("name"));
				users.setKana(rs.getString("kana"));
				users.setEnable(rs.getBoolean("enable"));
				users.setPass(rs.getString("pass"));
				users.setAdmin(rs.getBoolean("admin"));
				users.setMailladd(rs.getString("mailladd"));
				users.setIdate(rs.getInt("idate"));
				users.setIuid(rs.getInt("iuid"));
				users.setUdate(rs.getInt("udate"));
				users.setUuid(rs.getInt("uuid"));
				users.setMailenable(rs.getBoolean("mailenable"));
				users.setOname(rs.getString("oname"));
				users.setIdmcount(rs.getString("idmcount"));
				users.setNfcwait(rs.getBoolean("nfcwait"));
				users.setDepartment(rs.getInt("department"));
				users.setDepartmentname(Util.nullBlankToHyphen(rs.getString("departmentname")));
				users.setEmployment(rs.getInt("employment"));
				users.setEmploymentname(Util.nullBlankToHyphen(rs.getString("employmentname")));
				users.setWorklocation(rs.getInt("worklocation"));
				users.setWorklocationname(Util.nullBlankToHyphen(rs.getString("worklocationname")));
				users.setGroup(rs.getInt("usergroup"));
				users.setGroupname(Util.nullBlankToHyphen(rs.getString("groupname")));
				users.setHiredate(rs.getInt("hiredate"));
				users.setPaidvacationdays(rs.getInt("paidvacationdays"));
				datas.add(users);
			}
			return datas;
		}
	};

	/**
	 * SELECT用SQL実行メソッド。
	 * @param sql
	 * @param transaction
	 * @return
	 * @throws SQLException
	 */
	/*
	@SuppressWarnings("unchecked")
	private List<Users> executeGetQueryList(String sql) throws SQLException {
		// 実行
		DBConnect db = new DBConnect(sql, transaction);
		List<Users> resultList = (List<Users>)db.ExcuteResult(this);
		// error
		if(resultList == null) {
			return null;
		}
		return resultList;
	}
	*/

	/**
	 * executeGetList()の結果をJSON化したものを返します。
	 * @param sql
	 * @param transaction
	 * @return
	 * @throws SQLException
	 */
	/*
	private String execute(String sql, boolean transaction) throws SQLException {
		return this.ToJSON(executeGetList(sql, transaction));
	}
	*/

	/**
	 * JSON形式に変換
	 * @param list
	 * @return
	 */
	/*
	@Override
	public String ToJSON(List<?> datas) {
		// JSON形式に成型
		StringBuilder json = new StringBuilder();
		json.append("{");
		json.append("\"result\":\"success\",");
		json.append("\"datas\":").append(this.GetDataArrayObjectJSON(Users.class, datas));
		json.append("}");
		return json.toString();

		json.append("\"datas\":[");

		Users user;
		for(int i=0; i<datas.size(); i++) {
			user = (Users)datas.get(i);
			if(i > 0) {
				json.append(",");
			}
			json.append("{");
				json.append("\"oid\":\"").append(user.getOid()).append("\",");
				json.append("\"uid\":\"").append(user.getUid()).append("\",");
				json.append("\"code\":\"").append(Util.htmlEncode(user.getCode())).append("\",");	// ユーザが入力できるものはエンコード
				json.append("\"lname\":\"").append(Util.htmlEncode(user.getLname())).append("\",");
				json.append("\"name\":\"").append(Util.htmlEncode(user.getName())).append("\",");
				json.append("\"kana\":\"").append(Util.htmlEncode(user.getKana())).append("\",");
				json.append("\"enable\":\"").append(user.isEnable()).append("\",");
				json.append("\"pass\":\"").append(Util.htmlEncode(user.getPass())).append("\",");
				json.append("\"admin\":\"").append(user.isAdmin()).append("\",");
				json.append("\"mailladd\":\"").append(Util.htmlEncode(user.getMailladd())).append("\",");
				json.append("\"idate\":\"").append(Util.toTdate(user.getIdate())).append("\",");
				json.append("\"iuid\":\"").append(user.getIuid()).append("\",");
				json.append("\"udate\":\"").append(Util.toTdate(user.getUdate())).append("\",");
				json.append("\"uuid\":\"").append(user.getUuid()).append("\",");
				json.append("\"mailenable\":\"").append(user.isMailenable()).append("\",");
				json.append("\"oname\":\"").append(Util.htmlEncode(user.getOname())).append("\",");
				json.append("\"idmcount\":\"").append(user.getIdmcount()).append("\",");
				json.append("\"nfcwait\":\"").append(user.isNfcwait()).append("\"");
			json.append("}");
		}
		json.append("]");
		json.append("}");

		return json.toString();
	}*/

	/**
	 * ログイン用JSON形式変換メソッドです。<br>
	 * 通常のJSONの直下にloginプロパティが追加され、"done"か"failed"が記述されます。<br>
	 * @param datas
	 * @return
	 */
	/*
	public String toLoginJSON(List<Users> datas) {
		// JSON形式に成型
		StringBuilder json = new StringBuilder();
		json.append("{");
		json.append("\"result\":\"success\",");
		if(datas.size() == 1) {
			json.append("\"login\":\"done\",");
		} else {
			json.append("\"login\":\"failed\",");
		}
		json.append("\"datas\":").append(this.GetDataArrayObjectJSON(Users.class, datas));
		json.append("}");
		return json.toString();

		json.append("\"datas\":[");
		for(int i=0; i<datas.size(); i++) {
			if(i > 0) {
				json.append(",");
			}
			json.append("{");
				json.append("\"oid\":\"").append(datas.get(i).getOid()).append("\",");
				json.append("\"uid\":\"").append(datas.get(i).getUid()).append("\",");
				json.append("\"code\":\"").append(datas.get(i).getCode()).append("\",");
				json.append("\"lname\":\"").append(Util.htmlEncode(datas.get(i).getLname())).append("\",");
				json.append("\"name\":\"").append(Util.htmlEncode(datas.get(i).getName())).append("\",");
				json.append("\"kana\":\"").append(Util.htmlEncode(datas.get(i).getKana())).append("\",");
				json.append("\"enable\":\"").append(datas.get(i).isEnable()).append("\",");
				json.append("\"pass\":\"").append(Util.htmlEncode(datas.get(i).getPass())).append("\",");
				json.append("\"admin\":\"").append(datas.get(i).isAdmin()).append("\",");
				json.append("\"mailladd\":\"").append(Util.htmlEncode(datas.get(i).getMailladd())).append("\",");
				json.append("\"idate\":\"").append(Util.toTdate(datas.get(i).getIdate())).append("\",");
				json.append("\"iuid\":\"").append(datas.get(i).getIuid()).append("\",");
				json.append("\"udate\":\"").append(Util.toTdate(datas.get(i).getUdate())).append("\",");
				json.append("\"uuid\":\"").append(datas.get(i).getUuid()).append("\",");
				json.append("\"mailenabble\":\"").append(datas.get(i).isMailenable()).append("\",");
				json.append("\"oname\":\"").append(Util.htmlEncode(datas.get(i).getOname())).append("\",");
				json.append("\"idmcount\":\"").append(datas.get(i).getIdmcount()).append("\",");
				json.append("\"nfcwait\":\"").append(datas.get(i).isNfcwait()).append("\"");
			json.append("}");
		}
		json.append("]");
		json.append("}");

		return json.toString();
	}*/

	/**
	 * レコードを更新します。<br>
	 * oid,uid,uuidは必須項目です。<br>
	 * その他は任意ですが、nullの場合は無視されます。<br>
	 * 任意項目がすべてnullである場合はnullを返します。<br>
	 * nameもしくはkanaが""である場合はnullを返します。<br>
	 * lnameもしくはpassが""である場合はnullを返します。<br>
	 * enable,admin,mailenableは便宜上Stringとしていますが、必ず"true"か"false"で指定してください。<br>
	 * それ以外は無視されます。<br>
	 * uuidは編集者のuidです。<br>
	 * @param oid
	 * @param uid
	 * @param enable
	 * @param lname
	 * @param pass
	 * @param name
	 * @param kana
	 * @param admin
	 * @param mailladd
	 * @param code
	 * @param uuid
	 * @param mailenable
	 * @param department
	 * @param employment
	 * @param worklocation
	 * @param group
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult update(
			String oid, String uid, String enable, String lname,
			String pass, String name, String kana, String admin,
			String mailladd, String code, String uuid, String mailenable,
			String department, String employment, String worklocation, String group,
			String hiredate, String paidvacationdays) throws SQLException {

		SQLResult failed = new SQLResult();
		failed.Success = false;

		// 必須項目確認
		if(Util.isNullOrBlank(oid) || Util.isNullOrBlank(uid) || Util.isNullOrBlank(uuid)) return failed;
		if(lname != null && lname.equals("")) return failed;
		if(pass != null && pass.equals("")) return failed;
		if(name != null && name.equals("")) return failed;
		if(kana != null && kana.equals("")) return failed;
		if(enable == null && lname == null && pass == null &&
				name == null && kana == null && admin == null &&
				mailladd == null && code == null && mailenable == null &&
				department == null && employment == null && worklocation == null && group == null &&
				hiredate == null && paidvacationdays == null) return failed;

		// SET句
		StringBuilder set = new StringBuilder();
		if(enable != null && (enable.equals("true") || enable.equals("false"))) set.append("enable='").append(enable).append("',");
		if(lname != null) {
			set.append("lname='").append(Util.toSQLParse(lname)).append("',");
		}
		if(pass != null) {
			set.append("pass='").append(Util.toSQLParse(pass)).append("',");
		}
		if(name != null) {
			set.append("name='").append(Util.toSQLParse(name)).append("',");
		}
		if(kana != null) {
			set.append("kana='").append(Util.toSQLParse(kana)).append("',");
		}
		if(admin != null && (admin.equals("true") || admin.equals("false"))) set.append("admin='").append(admin).append("',");
		if(mailladd != null) {
			set.append("mailladd='").append(Util.toSQLParse(mailladd)).append("',");
		}
		if(code != null) {
			set.append("code='").append(Util.toSQLParse(code)).append("',");
		}
		if(mailenable != null && ((mailenable.equals("true") || mailenable.equals("false")))) {
			set.append("mailenable=").append(mailenable).append(",");
		}
		if(department != null) {
			set.append("department=").append(department).append(",");
		}
		if(employment != null) {
			set.append("employment=").append(employment).append(",");
		}
		if(worklocation != null) {
			set.append("worklocation=").append(worklocation).append(",");
		}
		if(group != null) {
			set.append("usergroup=").append(group).append(",");
		}
		if(hiredate != null) {
			set.append("hiredate=").append(hiredate).append(",");
		}
		if(paidvacationdays != null) {
			set.append("paidvacationdays=").append(paidvacationdays).append(",");
		}
		set.append("udate=").append(DateTime.GetTodayInt()).append(",");	// 今日の日付
		set.append("uuid=").append(uuid).append(" ");

		// SQL文作成
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE ");
		sql.append(DBinfo.TABLE_USERS).append(" ");
		sql.append("SET ");
		sql.append(set.toString());
		sql.append("WHERE ");
		sql.append("oid=").append(oid).append(" AND ");
		sql.append("uid=").append(uid);

		return ExecSQL(sql.toString());
	}

	/**
	 * 指定したユーザを削除します。<br>
	 * oid,uidは必須項目です。
	 * @param oid
	 * @param uid
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult delete(String oid, String uid) throws SQLException {

		// 必須項目確認
		if(Util.isNullOrBlank(oid) || Util.isNullOrBlank(uid)) {
			SQLResult result = new SQLResult();
			result.Success = false;
			return result;
		}

		// SQL文
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM ");
		sql.append(DBinfo.TABLE_USERS).append(" ");
		sql.append("WHERE ");
		sql.append("oid=").append(oid).append(" AND ");
		sql.append("uid=").append(uid);

		return ExecSQL(sql.toString());
	}

	/**
	 * ユーザを追加します。<br>
	 * department,employment,worklocation,group以外は必須です。
	 * @param oid
	 * @param lname
	 * @param pass
	 * @param name
	 * @param kana
	 * @param admin
	 * @param code
	 * @param mailladd
	 * @param uuid
	 * @param mailenable
	 * @param department
	 * @param employment
	 * @param worklocation
	 * @param group
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult insert(
			String oid, String lname, String pass, String name, String kana, String admin,
			String code, String mailladd, String uuid, String mailenable,
			String department, String employment, String worklocation, String group,
			String hiredate, String paidvacationdays) throws SQLException {

		// 必須項目確認
		if(Util.isNullOrBlank(oid)) return null;
		if(Util.isNullOrBlank(lname)) return null;
		if(Util.isNullOrBlank(pass)) return null;
		if(Util.isNullOrBlank(name)) return null;
		if(Util.isNullOrBlank(kana)) return null;
		if(Util.isNullOrBlank(admin)) return null;
		if(Util.isNullOrBlank(code)) return null;
		if(Util.isNullOrBlank(mailladd)) return null;
		if(Util.isNullOrBlank(uuid)) return null;
		if(Util.isNullOrBlank(mailenable)) return null;

		lname.replaceAll("'", "''");	// 「'」の前に「'」を付与してPostgreSQLに対応
		pass.replaceAll("'", "''");		// 「'」の前に「'」を付与してPostgreSQLに対応
		name.replaceAll("'", "''");		// 「'」の前に「'」を付与してPostgreSQLに対応
		kana.replaceAll("'", "''");		// 「'」の前に「'」を付与してPostgreSQLに対応
		code.replaceAll("'", "''");		// 「'」の前に「'」を付与してPostgreSQLに対応
		mailladd.replaceAll("'", "''");	// 「'」の前に「'」を付与してPostgreSQLに対応

		// SQL文
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ").append(DBinfo.TABLE_USERS).append(" ");
		sql.append("(oid,lname,pass,name,kana,enable,admin,mailladd,idate,iuid,udate,uuid,code,mailenable");
		if (department != null) sql.append(",department");
		if (employment != null) sql.append(",employment");
		if (worklocation != null) sql.append(",worklocation");
		if (group != null) sql.append(",usergroup");
		if (hiredate != null) sql.append(",hiredate");
		if (paidvacationdays != null) sql.append(",paidvacationdays");
		sql.append(") VALUES (");
		sql.append(oid).append(",");
		sql.append("'").append(Util.toSQLParse(lname)).append("',");
		sql.append("'").append(Util.toSQLParse(pass)).append("',");
		sql.append("'").append(Util.toSQLParse(name)).append("',");
		sql.append("'").append(Util.toSQLParse(kana)).append("',");
		sql.append("'true',");	// 凍結状態では作成しない
		sql.append("'").append(admin).append("',");
		sql.append("'").append(Util.toSQLParse(mailladd)).append("',");
		sql.append(DateTime.GetTodayInt()).append(",");
		sql.append(uuid).append(",");
		sql.append(0).append(",");
		sql.append(0).append(",");
		sql.append("'").append(Util.toSQLParse(code)).append("',");
		sql.append("'").append(mailenable).append("',");
		if (department != null) sql.append(department).append(",");
		if (employment != null) sql.append(employment).append(",");
		if (worklocation != null) sql.append(worklocation).append(",");
		if (group != null) sql.append(group).append(",");
		if (hiredate != null) sql.append(hiredate).append(",");
		if (paidvacationdays != null) sql.append(paidvacationdays).append(",");
		String SQL = sql.toString().substring(0, sql.length() - 1) + ")";

		return ExecSQL(SQL);
	}

}














