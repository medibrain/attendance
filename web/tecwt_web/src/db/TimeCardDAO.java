package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import model.TimecardExtra;
import table.TimeCard;
import util.DateTime;
import util.Util;


public class TimeCardDAO implements ResultFormat {


	/**
	 * 全取得。Grid表示向けにtimesで分けずに一行として表します。<br>
	 * oidがnullの場合、全レコードを返します。<br>
	 * uidがnullの場合、oidに関連するレコードを返します。<br>
	 * oid,uidに値がある場合、それらに関連するレコードを返します。<br>
	 * oidがnull、uidがnullでない場合はnullを返します。<br>
	 * @return
	 * @throws SQLException
	 */
	public String findForGrid(String oid, String uid) throws SQLException {

		if(oid == null && uid != null) return null;

		// where
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append("t.times=1 ");
		if(oid != null) where.append("AND t.oid=").append(oid).append(" ");
		if(uid != null) where.append("AND t.uid=").append(uid).append(" ");

		return this.executeResult(this.getSQLText(where.toString(), "DESC"), false, oid).ToJSON(false);
	}

	/**
	 * widに関連したレコード一覧を返します。<br>
	 * uidがnullの場合、全ユーザを対象とします。<br>
	 * oidまたはwidがnullの場合、nullを返します。<br>
	 * @param oid
	 * @param uid
	 * @param wid
	 * @return
	 * @throws SQLException
	 */
	public String findOfWorktypeForGrid(String oid, String uid, String wid) throws SQLException {

		if(oid == null || wid == null) return null;

		// SQL文
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append("t.times=1 ");
		where.append("AND t.oid=").append(oid).append(" ");
		if(uid != null) where.append("AND t.uid=").append(uid).append(" ");
		where.append("AND (t.wid=").append(wid).append(" OR tt.wid=").append(wid).append(") ");	// 普通と(移)の両方で検索

		return this.executeResult(this.getSQLText(where.toString(), "DESC"), false, oid).ToJSON(false);
	}

	/**
	 * 時間の範囲からタイムカード一覧を返します。<br>
	 * oidがnullの場合、nullを返します。<br>
	 * uidがnullの場合、全ユーザを対象とします。<br>
	 * ~timeがすべてnullの場合、findForGrid(oid,uid)と同じ動作を行います。<br>
	 * 以下に利用可能な検索条件を記述します。以下に合致しないnullがあった場合はnullを返します。<br>
	 * ①intime～<br>
	 * ②intime～movetime<br>
	 * ③intime～outtime<br>
	 * ④intime～movetime～outtime<br>
	 * ⑤movetime～<br>
	 * ⑥movetime～outtime<br>
	 * ⑦～outtime<br>
	 * @param oid
	 * @param uid
	 * @param intime
	 * @param movetime
	 * @param outtime
	 * @return
	 * @throws SQLException
	 */
	public String findOfTimeRangeForGrid(String oid, String uid, String intime, String movetime, String outtime) throws SQLException {

		if(oid == null) return null;
		if(intime == null && movetime == null && outtime == null) return this.findForGrid(oid, uid);

		StringBuilder range = new StringBuilder();
		if(intime != null) {
			if(movetime == null && outtime == null) {
				// ①
				range.append("AND t.intime>=").append(intime).append(" ");
			}
			else if(outtime == null) {
				// ②
				range.append("AND t.intime>=").append(intime).append(" ");
				range.append("AND tt.intime<=").append(movetime).append(" ");
			}
			else if(movetime == null) {
				// ③
				range.append("AND CASE ");
				range.append("WHEN tt.intime IS NULL THEN (");
				range.append("t.intime>=").append(intime).append(" AND ");
				range.append("t.outtime>=").append(intime).append(" AND ");
				range.append("t.outtime<=").append(outtime).append(") ");
				range.append("ELSE (");
				range.append("t.intime>=").append(intime).append(" AND ");
				range.append("tt.outtime>=").append(intime).append(" AND ");
				range.append("tt.outtime<=").append(outtime).append(") ");
				range.append("END ");
			} else {
				// ④
				range.append("AND CASE ");
				range.append("WHEN tt.intime IS NULL THEN (");
				range.append("t.intime=").append(intime).append(" AND ");
				range.append("t.outtime=").append(outtime).append(") ");
				range.append("ELSE (");
				range.append("t.intime=").append(intime).append(" AND ");
				range.append("tt.intime=").append(movetime).append(" AND ");
				range.append("tt.outtime=").append(outtime).append(") ");
				range.append("END ");
			}
		}
		else if(movetime != null) {
			if(outtime == null) {
				// ⑤
				range.append("AND tt.intime>=").append(movetime).append(" ");
			} else {
				// ⑥
				range.append("AND tt.intime>=").append(movetime).append(" ");
				range.append("AND tt.outtime>=").append(movetime).append(" ");
				range.append("AND tt.outtime<=").append(outtime).append(" ");
			}
		}
		else  {
			// ⑦
			range.append("AND tt.outtime<=").append(outtime).append(" ");
		}

		// SQL文
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append("t.times=1 ");
		where.append("AND t.oid=").append(oid).append(" ");
		where.append(range.toString());
		if(uid != null) where.append("AND t.uid=").append(uid).append(" ");

		return this.executeResult(this.getSQLText(where.toString(), "DESC"), false, oid).ToJSON(false);
	}

	/**
	 * 日付の範囲からレコード一覧を返します。<br>
	 * oidがnullの場合、nullを返します。<br>
	 * uidがnullの場合、全ユーザを対象とします。<br>
	 * 以下に利用可能な検索条件を記述します。以下に合致しないnullがあった場合はnullを返します。<br>
	 * ①date1～<br>
	 * ②date1～date2<br>
	 * ③～date2<br>
	 * @param oid
	 * @param uid
	 * @param date1
	 * @param date2
	 * @return
	 * @throws SQLException
	 */
	public TimecardResult findOfDateRangeForGrid(String oid, String uid, String date1, String date2) throws SQLException {

		if(oid == null) return null;
		if(date1 == null && date2 == null) return null;

		StringBuilder range = new StringBuilder();
		if(date1 != null) {
			if(date2 == null) {
				// ①
				range.append("AND t.tdate>=").append(date1).append(" ");
			}
			else {
				// ②
				range.append("AND t.tdate>=").append(date1).append(" ");
				range.append("AND t.tdate<=").append(date2).append(" ");
			}
		}
		else {
			// ③
			range.append("AND t.tdate<=").append(date2).append(" ");
		}

		// SQL文
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append("t.times=1 ");
		where.append("AND t.oid=").append(oid).append(" ");
		where.append(range.toString());
		if(uid != null) where.append("AND t.uid=").append(uid).append(" ");

		return this.executeResult(this.getSQLText(where.toString(), "DESC"), false, oid);
	}

	/**
	 * uid,tdateから検索します。<br>
	 * このメソッドはレコードの重複確認にも利用できます。<br>
	 * またある日にちのタイムカードを抽出することにも利用できます。<br>
	 * uidがnullの場合は全ユーザを対象とします。tdateは必須です。
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @return
	 * @throws SQLException
	 */
	public String findOfUidAndTdateForGrid(String oid, String uid, String tdate) throws SQLException {

		if(Util.isNullOrBlank(oid) || Util.isNullOrBlank(tdate)) return null;

		// where句
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append("t.oid=").append(oid).append(" AND ");
		if(uid != null) where.append("t.uid=").append(uid).append(" AND ");
		where.append("t.tdate=").append(tdate).append(" ");

		return this.executeResult(this.getSQLText(where.toString(), "DESC"), false, oid).ToJSON(false);
	}

	/**
	 * 指定した年・月分のレコードをすべて検索します。<br>
	 * oid,yearは必須です。<br>
	 * monthがnullの場合は年単位で出力します。<br>
	 * このメソッドではレコードは日付降順で出力されます。
	 * @param oid
	 * @param uid
	 * @param year
	 * @param month
	 * @return
	 * @throws SQLException
	 */
	public List<TimeCard> findOfYearAndMonth(String oid, String uid, String year, String month) throws SQLException {
		if(Util.isNullOrBlank(oid) || Util.isNullOrBlank(year)) return null;
		// 年・月範囲
		String date;
		String y = year;
		String m = month;
		if(Util.isNullOrBlank(m)) {
			String y2 = String.valueOf(Util.toInt(y) + 1);
			y = y +"0000";		// 20150000[yyyymmhh]
			y2 = y2 + "0000";	// 20160000
			date = "t.tdate BETWEEN " + y + " AND " + y2;
		} else {
			String m2 = String.valueOf(Util.toInt(m) + 1);
			if(m.length() == 1) m = "0" + m;
			if(m2.length() == 1) m2 = "0" + m2;
			m = y + m +"00";	// 20150800
			m2 = y + m2 + "00";	// 20150900
			date = "t.tdate BETWEEN " + m + " AND " + m2;
		}
		// where句
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append("t.oid=").append(oid).append(" AND ");
		if(uid != null) where.append("t.uid=").append(uid).append(" AND ");
		where.append("t.times=1").append(" AND ");
		where.append(date).append(" ");

		return this.executeGetList(this.getSQLText(where.toString(), "ASC"), false);
	}

	/**
	 * findOfYearAndMonth()のJSONオブジェクト版です。
	 * @param oid
	 * @param year
	 * @param month
	 * @return
	 * @throws SQLException
	 */
	public String findOfYearAndMonthForGrid(String oid, String uid, String year, String month) throws SQLException {
		return toJSON(findOfYearAndMonth(oid, uid, year, month));
	}

	/**
	 * このメソッドは指定日のレコードを持つタイムカード一覧をListオブジェクトとして返します。<br>
	 * 取得したデータは別のテーブルでクエリするなどに利用できるでしょう。
	 * @param oid
	 * @param tdate
	 * @return
	 * @throws SQLException
	 */
	public TimecardResult getListOfToday(String oid, String tdate) throws SQLException {

		if(Util.isNullOrBlank(oid) || Util.isNullOrBlank(tdate)) return null;

		// where句
		StringBuilder where = new StringBuilder();
		where.append("WHERE ");
		where.append("t.oid=").append(oid).append(" AND ");
		where.append("t.tdate=").append(tdate).append(" AND ");
		where.append("t.times=1 ");

		// 実行
		//DBConnect db = new DBConnect(this.getSQLText(where.toString(), "DESC"), false);
		//List<TimeCard> resultList = (List<TimeCard>)db.ExcuteResult(this);

		// error
		//if(resultList == null) {
		//	return null;
		//}

		return this.executeResult(this.getSQLText(where.toString(), "DESC"), false, oid);
	}

	/**
	 * 完全なSELECT文を返します。<br>
	 * 引数は先頭に"WHERE"、末尾に" "を付与した条件を指定してください。<br>
	 * カラム指定は"t.カラム名"で行って下さい。<br>
	 * tdateOrderByは"ASC"もしくは"DESC"で指定してください。
	 * @param where
	 * @param tdateOrderBy
	 * @return
	 */
	/*
	private String getSQLText(String where, String tdateOrderBy) {

		// SQL文
		StringBuilder sf = new StringBuilder();
		sf.append("SELECT ");
		sf.append("t.oid AS oid, t.uid AS uid, t.tdate AS tdate, t.times AS times, t.intime AS intime, ");
		sf.append("CASE WHEN tt.intime IS NULL THEN -1 ELSE tt.intime END AS movetime, ");
		sf.append("CASE WHEN tt.outtime IS NULL THEN (CASE WHEN t.intime!=0 AND t.outtime=0 THEN -1 ELSE t.outtime END) WHEN tt.outtime=0 THEN -1 ELSE tt.outtime END AS outtime, ");	// 出勤のみは退勤を-1
		sf.append("t.wid AS wid, t.note AS note, t.wcount AS wcount, t.rcount AS rcount, ");
		sf.append("CASE WHEN tt.wid IS NULL THEN -1 ELSE tt.wid END AS wid_move, CASE WHEN tt.note IS NULL THEN '----' ELSE tt.note END AS note_move, ");
		sf.append("CASE WHEN tt.wcount IS NULL THEN -1 ELSE tt.wcount END AS wcount_move, CASE WHEN tt.rcount IS NULL THEN -1 ELSE tt.rcount END AS rcount_move, ");
		sf.append("t.breaktime AS breaktime, t.fare AS fare, o.name AS oname, u.name AS uname, w.name AS wname, ");
		sf.append("CASE WHEN move_w.name IS NULL THEN '----' ELSE move_w.name END AS wname_move ");
		sf.append("FROM timecard AS t ");
		sf.append("LEFT OUTER JOIN (SELECT * FROM timecard WHERE times=2) AS tt ON (t.oid=tt.oid AND t.uid=tt.uid AND t.tdate=tt.tdate) ");
		sf.append("LEFT OUTER JOIN organization AS o ON (t.oid=o.oid) ");
		sf.append("LEFT OUTER JOIN users AS u ON (t.oid=u.oid AND t.uid=u.uid) ");
		sf.append("LEFT OUTER JOIN worktype AS w ON (t.oid=w.oid AND t.wid=w.wid) ");
		sf.append("LEFT OUTER JOIN worktype AS move_w ON (tt.oid=move_w.oid AND tt.wid=move_w.wid) ");
		if(where != null) sf.append(where);
		sf.append("ORDER BY t.tdate ").append(tdateOrderBy);

		return sf.toString();
	}
	*/
	/**
	 * 完全なSELECT文を返します。<br>
	 * 引数は先頭に"WHERE"、末尾に" "を付与した条件を指定してください。<br>
	 * カラム指定は"t."or"tt."を先頭につけてください。<br>
	 * tdateOrderByは"ASC"もしくは"DESC"で指定してください。
	 * @param where
	 * @param tdateOrderBy
	 * @return
	 */
	private String getSQLText(String where, String tdateOrderBy) {
		StringBuilder s = new StringBuilder();
		s.append("SELECT ");
		s.append("t.*,");
		s.append("t.intime AS time1,");
		s.append("t.outtime AS time2,");
		s.append("tt.intime AS time3,");
		s.append("tt.outtime AS time4,");
		s.append("tt.wid AS wid_move,");
		s.append("tt.note AS note_move,");
		s.append("tt.wcount AS wcount_move,");
		s.append("tt.rcount AS rcount_move,");
		s.append("u.name AS uname,");
		s.append("u.kana AS ukana,");
		s.append("w.name AS wname,");
		s.append("w2.name AS wname_move ");
		s.append("FROM ");
		s.append("timecard AS t ");
		s.append("LEFT OUTER JOIN ")
			.append("(SELECT * FROM timecard WHERE times=2) AS tt ON (t.oid=tt.oid AND t.uid=tt.uid AND t.tdate=tt.tdate) ");
		s.append("LEFT OUTER JOIN ")
			.append("users AS u ON (t.oid=u.oid AND t.uid=u.uid) ");
		s.append("LEFT OUTER JOIN ")
			.append("worktype AS w ON (t.oid=w.oid AND t.wid=w.wid) ");
		s.append("LEFT OUTER JOIN ")
			.append("worktype AS w2 ON (tt.oid=w2.oid AND tt.wid=w2.wid) ");
		if(where != null) s.append(where);
		s.append("ORDER BY t.tdate ").append(tdateOrderBy).append(", ");
		s.append("time1 DESC, ukana ASC");

		return s.toString();
	}

	@SuppressWarnings("unchecked")
	private TimecardResult executeResult(String sql, boolean transaction, String oid) throws SQLException {

		// 実行
		DBConnect db = new DBConnect(sql, transaction);
		List<TimeCard> resultList = (List<TimeCard>)db.ExcuteResult(this);

		// error
		if(resultList == null) {
			return null;
		}

		return new TimecardResult(oid, resultList, this.toJSON(resultList));
	}

	@SuppressWarnings("unchecked")
	private List<TimeCard> executeGetList(String sql, boolean transaction) throws SQLException {
		// 実行
		DBConnect db = new DBConnect(sql, transaction);
		List<TimeCard> resultList = (List<TimeCard>)db.ExcuteResult(this);

		// error
		if(resultList == null) {
			return null;
		}

		return resultList;
	}

	/**
	 * ResultSetオブジェクトをデータリストに整形する処理を実装しています。<br>
	 * このメソッドはDBconnectクラスからコールバックされます。
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	@Override
	public List<Object> toList(ResultSet resultSet) throws SQLException {
		List<Object> datas = new ArrayList<Object>();
		TimeCard timecard;
		while(resultSet.next()) {
			timecard = new TimeCard();
			this.setTimeAndWorktype(resultSet, timecard);
			timecard.setOid(resultSet.getInt("oid"));
			timecard.setUid(resultSet.getInt("uid"));
			timecard.setUname(resultSet.getString("uname"));
			timecard.setUkana(resultSet.getString("ukana"));
			timecard.setTdate(resultSet.getInt("tdate"));
			timecard.setTimes(resultSet.getInt("times"));
			timecard.setBreaktime(resultSet.getInt("breaktime"));
			timecard.setFare(resultSet.getInt("fare"));
			timecard.setNote(resultSet.getString("note"));
			timecard.setWcount(resultSet.getInt("wcount"));
			timecard.setRcount(resultSet.getInt("rcount"));
			timecard.setMove_note(resultSet.getString("note_move"));
			timecard.setMove_wcount(resultSet.getInt("wcount_move"));
			timecard.setMove_rcount(resultSet.getInt("rcount_move"));
			datas.add(timecard);
		}

		return datas;
	}
	/**
	 * Grid用に時刻を成型します。<br>
	 * @param resultSet
	 * @param timecard
	 * @throws SQLException
	 */
	private void setTimeAndWorktype(ResultSet resultSet, TimeCard timecard) throws SQLException {
		int time1 = resultSet.getInt("time1");
		int time2 = resultSet.getInt("time2");
		int time3 = resultSet.getInt("time3");
		int time4 = resultSet.getInt("time4");

		// 打刻状況
		final int IN = 1;
		final int IN_OUT = 2;
		final int IN_MOVE = 3;
		final int IN_MOVE_OUT = 4;
		final int MOVE = 5;
		final int MOVE_OUT = 6;
		final int OUT = 7;
		int type;
		if(time1 != -1 && time2 == -1 && time3 == 0 && time4 == 0) {
			// 出勤のみ
			type = IN;
		}
		else if(time1 != -1 && time2 != -1 && time3 == 0 && time4 == 0) {
			// 出勤・退勤
			type = IN_OUT;
		}
		else if(time1 == -1 && time2 != -1 && time3 == 0 && time4 == 0) {
			// 退勤のみ
			type = OUT;
		}
		else if(time1 == -1 && time2 != -1 && time3 != -1 && time4 == -1) {
			// 移動のみ
			type = MOVE;
		}
		else if(time1 == -1 && time2 != -1 && time3 != -1 && time4 != -1) {
			// 移動・退勤
			type = MOVE_OUT;
		}
		else if(time1 != -1 && time2 != -1 && time3 != -1 && time4 == -1) {
			// 出勤・移動
			type = IN_MOVE;
		}
		else {
			// 出勤・移動・退勤
			type = IN_MOVE_OUT;
		}

		// 各種成型
		switch(type) {
		case IN:
			timecard.setIntime(time1);
			timecard.setMovetime(-1);
			timecard.setOuttime(-1);
			break;
		case IN_OUT:
			timecard.setIntime(time1);
			timecard.setMovetime(-1);
			timecard.setOuttime(time2);
			break;
		case IN_MOVE:
			timecard.setIntime(time1);
			timecard.setMovetime(time3);
			timecard.setOuttime(-1);
			break;
		case IN_MOVE_OUT:
			timecard.setIntime(time1);
			timecard.setMovetime(time3);
			timecard.setOuttime(time4);
			break;
		case MOVE:
			timecard.setIntime(-1);
			timecard.setMovetime(time3);
			timecard.setOuttime(-1);
			break;
		case MOVE_OUT:
			timecard.setIntime(-1);
			timecard.setMovetime(time3);
			timecard.setOuttime(time4);
			break;
		case OUT:
			timecard.setIntime(-1);
			timecard.setMovetime(-1);
			timecard.setOuttime(time2);
			break;
		}

		// widが存在しないなら「""」入れる
		int wid = resultSet.getInt("wid");
		int wid_move = resultSet.getInt("wid_move");
		if(wid == 0) {
			timecard.setWname("");
		} else {
			timecard.setWname(resultSet.getString("wname"));
		}
		if(wid_move == 0) {
			timecard.setMove_wname("");
		} else {
			timecard.setMove_wname(resultSet.getString("wname_move"));
		}
		timecard.setWid(wid);
		timecard.setMove_wid(wid_move);
	}

	/**
	 * JSON形式に変換
	 * @param datas
	 * @return
	 */
	public String toJSON(List<TimeCard> datas) {

		// JSON形式に成型
		StringBuilder json = new StringBuilder();

		json.append("{");
		json.append("\"result\":\"success\",");
		json.append("\"datas\":[");
		for(int i=0; i<datas.size(); i++) {
			if(i > 0) {
				json.append(",");
			}
			json.append("{");
			json.append("\"oid\":\"").append(datas.get(i).getOid()).append("\",");
			json.append("\"uid\":\"").append(datas.get(i).getUid()).append("\",");
			json.append("\"tdate\":\"").append(Util.toTdate(datas.get(i).getTdate())).append("\",");
			json.append("\"times\":\"").append(datas.get(i).getTimes()).append("\",");
			json.append("\"inH\":\"").append(Util.getHour(datas.get(i).getIntime())).append("\",");
			json.append("\"inM\":\"").append(Util.getMinute(datas.get(i).getIntime())).append("\",");
			json.append("\"moveH\":\"").append(Util.getHour(datas.get(i).getMovetime())).append("\",");
			json.append("\"moveM\":\"").append(Util.getMinute(datas.get(i).getMovetime())).append("\",");
			json.append("\"outH\":\"").append(Util.getHour(datas.get(i).getOuttime())).append("\",");
			json.append("\"outM\":\"").append(Util.getMinute(datas.get(i).getOuttime())).append("\",");
			json.append("\"breaktime\":\"").append(datas.get(i).getBreaktime()).append("\",");
			json.append("\"fare\":\"").append(datas.get(i).getFare()).append("\",");
			json.append("\"wid\":\"").append(datas.get(i).getWid()).append("\",");
			json.append("\"note\":\"").append(Util.htmlEncode(Util.nullToBlank(datas.get(i).getNote()))).append("\",");	// ユーザが自由に決められる箇所はエンコードしておく
			json.append("\"wcount\":\"").append(datas.get(i).getWcount()).append("\",");
			json.append("\"rcount\":\"").append(datas.get(i).getRcount()).append("\",");
			json.append("\"wid_move\":\"").append(Util.minusOneToHyphen(datas.get(i).getMove_wid())).append("\",");
			json.append("\"note_move\":\"").append(Util.htmlEncode(Util.nullToBlank(datas.get(i).getMove_note()))).append("\",");
			json.append("\"wcount_move\":\"").append(Util.minusOneToHyphen(datas.get(i).getMove_wcount())).append("\",");
			json.append("\"rcount_move\":\"").append(Util.minusOneToHyphen(datas.get(i).getMove_rcount())).append("\",");
			json.append("\"oname\":\"").append(Util.htmlEncode(datas.get(i).getOname())).append("\",");
			json.append("\"uname\":\"").append(Util.htmlEncode(datas.get(i).getUname())).append("\",");
			json.append("\"ukana\":\"").append(Util.htmlEncode(datas.get(i).getUkana())).append("\",");
			json.append("\"wname\":\"").append(Util.htmlEncode(datas.get(i).getWname())).append("\",");
			json.append("\"wname_move\":\"").append(Util.htmlEncode(datas.get(i).getMove_wname())).append("\",");
			json.append("\"dayofweek\":\"").append(DateTime.GetDayOfWeek((datas.get(i).getTdate()))).append("\"");
			json.append("}");
		}
		json.append("]");
		json.append("}");

		return json.toString();
	}



	/**
	 * レコードをアップデートします。<br>
	 * ↓memo↓<br>
	 * 移動を行っている場合と行っていない場合とでtimes1のouttimeの意味合いが変化するので注意してください。<br>
	 * @param defaultUid
	 * @param defaultTdate
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @param intime
	 * @param movetime
	 * @param outtime
	 * @param wid
	 * @param note
	 * @param wcount
	 * @param rcount
	 * @param wid_move
	 * @param note_move
	 * @param wcount_move
	 * @param rcount_move
	 * @param breaktime
	 * @param fare
	 * @param uname
	 * @param wname
	 * @param wname_move
	 * @param isDefaultMove
	 * @return
	 * @throws SQLException
	 */
	public String updateForGrid2(
			String defaultUid, String defaultTdate,
			String oid, String uid, String tdate,
			String intime, String movetime, String outtime,
			String wid, String note, String wcount, String rcount,
			String wid_move, String note_move, String wcount_move, String rcount_move,
			String breaktime, String fare,
			String uname, String wname, String wname_move,
			boolean isDefaultMove, boolean isKeepMove	// 更新前からtimes2レコードがあるか否か、times2レコードを維持するか否か
			) throws SQLException {

		// 実行SQL文リスト
		List<String> sqlList = new ArrayList<String>();

		// uid変更確認
		String masterUid = defaultUid;
		if(uid != null && uid.equals("")) masterUid = uid;

		// tdate変更確認
		String masterTdate = defaultTdate;
		if(tdate != null && !tdate.equals("")) masterTdate = tdate;

		// 更新日付と時間
		String udate = DateTime.GetTodayStr();
		String utime = DateTime.GetNowHHMMStr();

		// 変更箇所取得
		Changes changes = this.getChangedColumns(
				defaultUid, defaultTdate, oid, uid, tdate,
				intime, movetime, outtime, wid, note, wcount, rcount, wid_move, note_move, wcount_move, rcount_move, breaktime, fare, uname, wname, wname_move);
		// 移動の有無
		boolean isNeedMove = false;
		if(movetime != null || wid_move != null || note_move != null || wcount_move != null || rcount_move != null) {
			isNeedMove = true;
		}

		// 移動解除
		boolean isTimes2Delete = false;
		if(isDefaultMove && !isKeepMove) {
			// 退勤まで押されているならtimes1のouttimeにtimes2のものを移植
			if(changes.isDefaultInMoveOut) {
				sqlList.add(this.getUpdateFullText(
						oid, defaultUid, uid, defaultTdate, tdate, "1", null, String.valueOf(changes.times2Outtime), null, null, null, null, null, null));
			}
			// times2レコード削除
			sqlList.add(this.toDELETE_SQLFullText(oid, defaultUid, defaultTdate, "2"));	// times2削除
			sqlList.add(TclogDAO.toInsertSQLText(
					oid, masterUid, "WEB 管理", "移動取消", udate, utime, "0.0", "0.0", masterTdate));
			isTimes2Delete = true;
		}

		// 移動追加
		if(!isDefaultMove && isNeedMove) {
			// times1項目に変更がなければ、times1レコードのouttimeをmovetimeと同じものに更新(あった場合は一緒に変更するのでOK)
			if(!changes.isTimes1Changed) {
				sqlList.add(this.getUpdateFullText(
						oid, defaultUid, uid, defaultTdate, tdate, "1", null, movetime, null, null, null, null, null, null));
			}
			// 移動する前の状態が出勤＆退勤の状態なら、times2のouttimeにtimes1のouttimeを入れる
			String masterOuttime = outtime;
			if(changes.isDefaultInOut) {
				masterOuttime = String.valueOf(changes.times1Outtime);
			}
			// times2レコード追加
			sqlList.add(this.toINSERT_SQLFullText2(oid, masterUid, masterTdate, "2", movetime, masterOuttime, wid_move, note_move, wcount_move, rcount_move, null, null));	// times2追加
			// ログに記載
			sqlList.add(TclogDAO.toInsertSQLText(
					oid, masterUid, "WEB 管理",
					"移動追加,"+this.getTclogFuncOfTimes2Insert(movetime, outtime, wname_move, note_move, wcount_move, rcount_move),
					udate, utime, "0.0", "0.0", masterTdate));
		}

		// 更新SQL文作成
		if(changes.isTimes1Changed) {
			if(isDefaultMove && isKeepMove) {
				// times2が既に存在し、times1項目を変更する場合
				String in = (intime != null && intime.equals("----")) ? "-1" : Util.nullBlankHyphenToNull(intime);
				String move = (movetime != null && movetime.equals("----")) ? "-1" : Util.nullBlankHyphenToNull(movetime);
				sqlList.add(this.getUpdateFullText(
						oid, defaultUid, uid, defaultTdate, tdate, "1", in, move, wid, note, wcount, rcount, breaktime, fare));
			} else if(!isDefaultMove && isNeedMove) {
				// times2が存在せず、movetimeを変更(times2追加)する場合
				String in = (intime != null && intime.equals("----")) ? "-1" : Util.nullBlankHyphenToNull(intime);
				String move = (movetime != null && movetime.equals("----")) ? "-1" : Util.nullBlankHyphenToNull(movetime);
				sqlList.add(this.getUpdateFullText(
						oid, defaultUid, uid, defaultTdate, tdate, "1", in, move, wid, note, wcount, rcount, breaktime, fare));	// times1のouttimeの意味が変わる
			} else {
				// times2が存在せず、times1項目を変更する場合
				String in = (intime != null && intime.equals("----")) ? "-1" : Util.nullBlankHyphenToNull(intime);
				String out = (outtime != null && outtime.equals("----")) ? "-1" : Util.nullBlankHyphenToNull(outtime);
				sqlList.add(this.getUpdateFullText(
						oid, defaultUid, uid, defaultTdate, tdate, "1", in, out, wid, note, wcount, rcount, breaktime, fare));
			}
		}
		if(!isTimes2Delete && isDefaultMove && changes.isTimes2Changed) {
			// times2の削除予定がなく、times2が既に存在し、times2項目を変更する場合（存在しない場合はinsertで対応）
			String move = (movetime != null && movetime.equals("----")) ? "-1" : Util.nullBlankHyphenToNull(movetime);
			String out = (outtime != null && outtime.equals("----")) ? "-1" : Util.nullBlankHyphenToNull(outtime);
			sqlList.add(this.getUpdateFullText(
					oid, defaultUid, uid, defaultTdate, tdate, "2", move, out, wid_move, note_move, wcount_move, rcount_move, null, null));
		}

		// tclog更新用SQL文作成
		if(changes.func != null && !changes.func.equals("")) {	// times1変更無かつtimes2追加の場合「""」になるので省く
			sqlList.add(TclogDAO.toInsertSQLText(
					oid, masterUid, "WEB 管理", changes.func, udate, utime, "0.0", "0.0", masterTdate));
		}

		return this.executeQuery(sqlList, true);
	}


	/**
	 * 更新するためのSQL文を返します。<br>
	 * 変更点が存在するかは判定していません。<br>
	 * @param oid
	 * @param defaultUid
	 * @param uid
	 * @param defaultTdate
	 * @param tdate
	 * @param times
	 * @param wid
	 * @param note
	 * @param wcount
	 * @param rcount
	 * @param breaktime
	 * @param fare
	 * @return
	 */
	private String getUpdateFullText(String oid, String defaultUid, String uid, String defaultTdate, String tdate, String times,
			String intime, String outtime, String wid, String note, String wcount, String rcount, String breaktime, String fare) {

		StringBuilder s = new StringBuilder();
		s.append("UPDATE ").append(DBinfo.TABLE_TIMECARD).append(" ");
		s.append("SET ");
		if(!Util.isNullOrBlank(uid)) s.append("uid=").append(uid).append(", ");
		if(!Util.isNullOrBlank(tdate)) s.append("tdate=").append(tdate).append(", ");
		if(!Util.isNullOrBlank(intime)) s.append("intime=").append(intime).append(", ");
		if(!Util.isNullOrBlank(outtime)) s.append("outtime=").append(outtime).append(", ");
		if(!Util.isNullOrBlank(wid)) s.append("wid=").append(wid).append(", ");
		if(note != null) s.append("note='").append(Util.toSQLParse(note)).append("', ");	// 'は''にしてエスケープ
		if(!Util.isNullOrBlank(wcount)) s.append("wcount=").append(wcount).append(", ");
		if(!Util.isNullOrBlank(rcount)) s.append("rcount=").append(rcount).append(", ");
		if(!Util.isNullOrBlank(breaktime)) s.append("breaktime=").append(breaktime).append(", ");
		if(!Util.isNullOrBlank(fare)) s.append("fare=").append(fare).append(", ");
		s.replace(s.lastIndexOf(","), s.length(), " ");	// 末尾の","を削除
		s.append("WHERE ");
		s.append("oid=").append(oid).append(" AND ");
		s.append("uid=").append(defaultUid).append(" AND ");
		s.append("tdate=").append(defaultTdate).append(" AND ");
		s.append("times=").append(times);

		return s.toString();
	}

	/**
	 * 変更されている箇所をテキストで取得できます。<br>
	 * テキストは「項目：変更前->変更後,」の形式で記述します。<br>
	 * 返却するChangesインスタンスには、取得したテキストとレコードが変更されているかを判定するフラグを格納しています。
	 * @param defaultUid
	 * @param defaultTdate
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @param intime
	 * @param movetime
	 * @param outtime
	 * @param wid
	 * @param note
	 * @param wcount
	 * @param rcount
	 * @param wid_move
	 * @param note_move
	 * @param wcount_move
	 * @param rcount_move
	 * @param breaktime
	 * @param fare
	 * @param uname
	 * @param wname
	 * @param wname_move
	 * @return
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	private Changes getChangedColumns(
			String defaultUid, String defaultTdate,
			String oid, String uid, String tdate,
			String intime, String movetime, String outtime,
			String wid, String note, String wcount, String rcount,
			String wid_move, String note_move, String wcount_move, String rcount_move,
			String breaktime, String fare,
			String uname, String wname, String wname_move
			) throws SQLException {

		// 返却用インスタンス
		Changes changes = new Changes();

		// レコード取得
		DBConnect db = new DBConnect(this.getSQLText("WHERE t.oid="+oid+" AND t.uid="+defaultUid+" AND t.tdate="+defaultTdate+" ", "ASC"), false);
		List<TimeCard> resultList = (List<TimeCard>)db.ExcuteResult(this);
		if(resultList == null || resultList.isEmpty() || resultList.size() > 2) {
			return null;
		}

		// 変更値取得
		boolean isDefaultMove = resultList.size() == 2;
		StringBuilder func1 = new StringBuilder();
		StringBuilder func2 = new StringBuilder();
		TimeCard tc;
		for(int i=0; i<resultList.size(); i++) {
			tc = resultList.get(i);

			if(tc.getTimes() == 1) {
				// times1
				if(uid != null && !String.valueOf(tc.getUid()).equals(uid)) func1.append("名前：").append(tc.getUname()).append(">>").append(uname).append(",");
				if(tdate != null && !String.valueOf(tc.getTdate()).equals(tdate)) func1.append("日付：").append(tc.getTdate()).append(">>").append(tdate).append(",");
				if(intime != null && !String.valueOf(tc.getIntime()).equals(intime)) func1.append("出勤：").append(tc.getIntime()).append(">>").append(intime).append(",");
				if(outtime != null && !String.valueOf(tc.getOuttime()).equals(outtime)) {
					if(!isDefaultMove) {
						func1.append("退勤：").append(tc.getOuttime()).append(">>").append(outtime).append(",");	// isTimes1Changedフラグを立てないようにする
					}
				}
				if(wid != null && !String.valueOf(tc.getWid()).equals(wid)) func1.append("勤務種別：").append(tc.getWname()).append(">>").append(wname).append(",");
				if(note != null && !tc.getNote().equals(note)) func1.append("業務内容：").append(tc.getNote()).append(">>").append(note).append(",");
				if(wcount != null && !String.valueOf(tc.getWcount()).equals(wcount)) func1.append("処理枚数：").append(tc.getWcount()).append(">>").append(wcount).append(",");
				if(rcount != null && !String.valueOf(tc.getRcount()).equals(rcount)) func1.append("申出枚数：").append(tc.getRcount()).append(">>").append(rcount).append(",");
				if(breaktime != null && !String.valueOf(tc.getBreaktime()).equals(breaktime)) func1.append("休憩時間：").append(tc.getBreaktime()).append(">>").append(breaktime).append(",");
				if(fare != null && !String.valueOf(tc.getFare()).equals(fare)) func1.append("交通費：").append(tc.getFare()).append(">>").append(fare).append(",");

				// 変更前から出勤と退勤が押されている場合は値抽出
				if(tc.getIntime() != -1 && tc.getOuttime() != -1) {
					changes.isDefaultInOut = true;
					changes.times1Outtime = tc.getOuttime();
				}

				// 変更箇所があったことを通知
				if(func1.length() > 0) changes.isTimes1Changed = true;
			}
			else if(tc.getTimes() == 2) {
				// times2
				if(movetime != null && !String.valueOf(tc.getIntime()).equals(movetime)) func2.append("移動：").append(tc.getMovetime()).append(">>").append(movetime).append(",");
				if(outtime != null && !String.valueOf(tc.getOuttime()).equals(outtime)) {
					func2.append("退勤：").append(tc.getOuttime()).append(">>").append(outtime).append(",");
				}
				if(wid_move != null && !String.valueOf(tc.getMove_wid()).equals(wid_move)) func2.append("(移)勤務種別：").append(tc.getMove_wname()).append(">>").append(wname_move).append(",");
				if(note_move != null && !tc.getMove_note().equals(note_move)) func2.append("(移)業務内容：").append(tc.getMove_note()).append(">>").append(note_move).append(",");
				if(wcount_move != null && !String.valueOf(tc.getMove_wcount()).equals(wcount_move)) func2.append("(移)処理枚数：").append(tc.getMove_wcount()).append(">>").append(wcount_move).append(",");
				if(rcount_move != null && !String.valueOf(tc.getMove_rcount()).equals(rcount_move)) func2.append("(移)申出枚数：").append(tc.getMove_rcount()).append(">>").append(rcount_move).append(",");

				// 変更前から出勤と退勤が押されている場合は値抽出
				if(tc.getIntime() != -1 && tc.getOuttime() != -1) {
					changes.isDefaultInMoveOut = true;
					changes.times2Outtime = tc.getOuttime();
				}

				// 変更箇所があったことを通知
				if(func2.length() > 0) changes.isTimes2Changed = true;
			}
		}
		changes.func = func1.toString() + func2.toString();

		return changes;
	}

	private class Changes {
		public boolean isTimes1Changed;
		public boolean isTimes2Changed;
		public String func;
		public boolean isDefaultInOut;		// 変更前が出勤＆退勤
		public int times1Outtime;
		public boolean isDefaultInMoveOut;	// 変更前が出勤＆移動＆退勤
		public int times2Outtime;
	}

	/**
	 * times1を追加する場合にtclogテーブルへ記録するためのfuncテキストを取得します。
	 * @param intime
	 * @param outtime
	 * @param wname
	 * @param note
	 * @param wcount
	 * @param rcount
	 * @return
	 */
	private String getTclogFuncOfTimes1Insert(String intime, String outtime, String uname, String wname, String note, String wcount, String rcount) {
		StringBuilder s = new StringBuilder();
		if(!Util.isNullOrBlank(intime)) s.append("出勤：>>").append(intime).append(",");
		if(!Util.isNullOrBlank(outtime)) s.append("退勤：>>").append(outtime).append(",");
		if(!Util.isNullOrBlank(uname)) s.append("名前：>>").append(uname).append(",");
		if(!Util.isNullOrBlank(wname)) s.append("勤務種別：>>").append(wname).append(",");
		if(!Util.isNullOrBlank(note)) s.append("業務内容：>>").append(note).append(",");
		if(!Util.isNullOrBlank(wcount)) s.append("処理枚数：>>").append(wcount).append(",");
		if(!Util.isNullOrBlank(rcount)) s.append("申出枚数：>>").append(rcount).append(",");
		return s.toString();
	}

	/**
	 * times2を追加する場合にtclogテーブルへ記録するためのfuncテキストを取得します。
	 * @param movetime
	 * @param outtime
	 * @param wname_move
	 * @param note_move
	 * @param wcount_move
	 * @param rcount_move
	 * @return
	 */
	private String getTclogFuncOfTimes2Insert(String movetime, String outtime, String wname_move, String note_move, String wcount_move, String rcount_move) {
		StringBuilder s = new StringBuilder();
		if(!Util.isNullOrBlank(movetime)) s.append("移動：>>").append(movetime).append(",");
		if(!Util.isNullOrBlank(outtime)) s.append("退勤：>>").append(outtime).append(",");
		if(!Util.isNullOrBlank(wname_move)) s.append("(移)勤務種別：>>").append(wname_move).append(",");
		if(!Util.isNullOrBlank(note_move)) s.append("(移)業務内容：>>").append(note_move).append(",");
		if(!Util.isNullOrBlank(wcount_move)) s.append("(移)処理枚数：>>").append(wcount_move).append(",");
		if(!Util.isNullOrBlank(rcount_move)) s.append("(移)申出枚数：>>").append(rcount_move).append(",");
		return s.toString();
	}

	/**
	 * 指定したレコードを物理削除します。<br>
	 * 引数にnullが存在する場合は処理を行わず、エラーメッセージを返します。
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @param times
	 * @return
	 * @throws SQLException
	 */
	public String deleteForGrid(String oid, String uid, String tdate, boolean defaultIsMove) throws SQLException {
		if(oid == null || uid == null || tdate == null) return Util.toErrorJSON("データ削除の為に必要な値を取得できませんでした。[CODE:TCD001]");

		String date = DateTime.GetTodayStr();
		String time = DateTime.GetNowHHMMStr();

		List<String> sqlList = new ArrayList<String>();

		// timecard DELETE
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM ");
		sql.append(DBinfo.TABLE_TIMECARD).append(" ");
		sql.append("WHERE ");
		sql.append("oid=").append(oid).append(" AND ");
		sql.append("uid=").append(uid).append(" AND ");
		sql.append("tdate=").append(tdate).append(" AND ");
		sql.append("times=").append(1);	// times1
		sqlList.add(sql.toString());

		// 作業内容も削除
		String delete = "DELETE FROM " + DBinfo.TABLE_DAILY_WORK_RESULT + " "
			+ "WHERE oid=" + oid + " AND uid=" + uid+ " AND tdate=" + tdate + ";";
		sqlList.add(delete);

		// tclog INSERT
		sqlList.add(TclogDAO.toInsertSQLText(
				oid, uid, "WEB 管理", "タイムカード削除", date, time, "0.0", "0.0", tdate));

		// 移動を挟んでいる場合はtimes2も削除する
		if(defaultIsMove) {
			StringBuilder sql2 = new StringBuilder();
			sql2.append("DELETE FROM ");
			sql2.append(DBinfo.TABLE_TIMECARD).append(" ");
			sql2.append("WHERE ");
			sql2.append("oid=").append(oid).append(" AND ");
			sql2.append("uid=").append(uid).append(" AND ");
			sql2.append("tdate=").append(tdate).append(" AND ");
			sql2.append("times=").append(2);	// times2
			sqlList.add(sql2.toString());

			sqlList.add(TclogDAO.toInsertSQLText(
					oid, uid, "WEB 管理", "(移)タイムカード削除", date, time, "0.0", "0.0", tdate));
		}

		return this.executeQuery(sqlList, true);
	}

	/**
	 * レコードを追加します。<br>
	 * params1,2で有効なキーは以下の通りです。<br>
	 * [oid,uid,intime,outtime,wid,note,wcount,rcount,breaktime,fare]<br>
	 * nullの値は無視され、DBで設定されているデフォルト値が使用されますが、以下の値は必ずセットしてください。<br>
	 * [oid,uid,tdate,intime]<br>
	 * 上記の値が不正であった場合はエラーとなります。
	 * @param params1
	 * @param params2
	 * @return
	 * @throws SQLException
	 */
	public String insertForGrid(String oid, String uid, String tdate, Map<String, String> params1, Map<String, String> params2, String uname, String wname, String wname_move) throws SQLException {

		// 更新日付時刻
		String date = DateTime.GetTodayStr();
		String time = DateTime.GetNowHHMMStr();

		List<String> sqlList = new ArrayList<String>();

		// パラメータ抽出
		String intime = params1.get("intime");
		String outtime = params1.get("outtime");
		String wid = params1.get("wid");
		String note = params1.get("note");
		String wcount = params1.get("wcount");
		String rcount = params1.get("rcount");
		String breaktime = params1.get("breaktime");
		String fare = params1.get("fare");

		// レコード追加
		sqlList.add(this.toINSERT_SQLFullText2(oid, uid, tdate, "1", intime, outtime, wid, note, wcount, rcount, breaktime, fare));
		String func1 = this.getTclogFuncOfTimes1Insert(intime, outtime, uname, wname, note, wcount, rcount);

		// 移動を挟んでいる場合はtimes2を追加する
		String func2 = "";
		if(params2 != null) {
			String intime_move = params2.get("intime");
			String outtime_move = params2.get("intime");
			String wid_move = params2.get("wid");
			String note_move = params2.get("note");
			String wcount_move = params2.get("wcount");
			String rcount_move = params2.get("rcount");
			sqlList.add(this.toINSERT_SQLFullText2(oid, uid, tdate, "2", intime_move, outtime_move, wid_move, note_move, wcount_move, rcount_move, null, null));
			func2 = this.getTclogFuncOfTimes2Insert(intime_move, outtime_move, wname_move, note_move, wcount_move, rcount_move);
		}

		// ログに記載
		String func = "タイムカード追加," + func1 + func2;
		sqlList.add(TclogDAO.toInsertSQLText(oid, uid, "WEB 管理", func, date, time,"0.0", "0.0", tdate));

		return this.executeQuery(sqlList, true);
	}

	/**
	 * 与えられた値を元に完全なDELETE文を作成します。<br>
	 * nullチェックは行わない点に注意してください。<br>
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @param times
	 * @return
	 */
	private String toDELETE_SQLFullText(String oid, String uid, String tdate, String times) {
		StringBuilder sql = new StringBuilder();
		sql.append("DELETE FROM ");
		sql.append(DBinfo.TABLE_TIMECARD).append(" ");
		sql.append("WHERE ");
		sql.append("oid=").append(oid).append(" AND ");
		sql.append("uid=").append(uid).append(" AND ");
		sql.append("tdate=").append(tdate).append(" AND ");
		sql.append("times=").append(times);
		return sql.toString();
	}

	/**
	 * 完全なinsert文を取得します。<br>
	 * oid,uid,tdate,times,intimeは必須です。
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @param times
	 * @param intime
	 * @param outtime
	 * @param wid
	 * @param note
	 * @param wcount
	 * @param rcount
	 * @param breaktime
	 * @param fare
	 * @return
	 */
	private String toINSERT_SQLFullText2(
			String oid, String uid, String tdate, String times,
			String intime, String outtime, String wid, String note, String wcount, String rcount, String breaktime, String fare) {

		StringBuilder s = new StringBuilder();
		s.append("INSERT INTO ").append(DBinfo.TABLE_TIMECARD).append(" ");
		s.append("(");
		s.append("oid, uid, tdate, times, intime, outtime, ");
		s.append("wid, ");	// widはnull非許可でデフォルト値なしなので、その場合は0を入れる
		if(!Util.isNullOrBlank(note)) s.append("note, ");
		if(!Util.isNullOrBlank(wcount)) s.append("wcount, ");
		if(!Util.isNullOrBlank(rcount)) s.append("rcount, ");
		if(!Util.isNullOrBlank(breaktime)) s.append("breaktime, ");
		if(!Util.isNullOrBlank(fare)) s.append("fare, ");
		s.replace(s.lastIndexOf(","), s.length(), " ");	// 末尾の","を削除
		s.append(") ");
		s.append("VALUES ");
		s.append("(");
		s.append(oid).append(", ");
		s.append(uid).append(", ");
		s.append(tdate).append(", ");
		s.append(times).append(", ");
		s.append(intime).append(", ");
		if(Util.isNullOrBlank(outtime)) {
			s.append(-1).append(", ");
		} else {
			s.append(outtime).append(", ");
		}
		if(!Util.isNullOrBlank(wid)) {
			s.append(wid).append(", ");
		} else {
			s.append(0).append(", ");
		}
		if(!Util.isNullOrBlank(note)) s.append("'").append(Util.toSQLParse(note)).append("', ");	// 'は''にしてエスケープ
		if(!Util.isNullOrBlank(wcount)) s.append(wcount).append(", ");
		if(!Util.isNullOrBlank(rcount)) s.append(rcount).append(", ");
		if(!Util.isNullOrBlank(breaktime)) s.append(breaktime).append(", ");
		if(!Util.isNullOrBlank(fare)) s.append(fare).append(", ");
		s.replace(s.lastIndexOf(","), s.length(), " ");	// 末尾の","を削除
		s.append(")");

		return s.toString();
	}

	/**
	 * 更新・削除実行メソッド。<br>
	 * @param sqlList
	 * @param transaction
	 * @return
	 * @throws SQLException
	 */
	private String executeQuery(List<String> sqlList, boolean transaction) throws SQLException {

		// 実行
		DBConnect db = new DBConnect(sqlList, transaction);
		List<Integer> countList = db.ExcuteNonQueryCount();

		// error
		if(countList == null) {
			return null;
		}

		// 更新行数を合算
		int allCount = 0;
		for(int count : countList) {
			allCount += count;
		}

		// tclogの分はカウントしない
		allCount = allCount / 2;	// timecard更新行数=tclog挿入行数

		return Util.toSuccessJSON(String.valueOf(allCount));
	}

	public class TimecardResult {
		public List<TimeCard> List;
		private TimecardExtra extra;
		public TimecardResult(String oid, List<TimeCard> list, String json) {
			List = list;
			extra = new TimecardExtra(oid, list, json);
		}
		public String ToJSON(boolean setUnitName) {
			return extra.ToJSON(setUnitName);
		}
	}

}
