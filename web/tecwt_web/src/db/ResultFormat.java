package db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public interface ResultFormat {

	/**
	 * 検索結果(ResultSet)をリスト形式に変換します。
	 * @param resultSet
	 * @return
	 */
	public abstract List<Object> toList(ResultSet resultSet) throws SQLException;

}
