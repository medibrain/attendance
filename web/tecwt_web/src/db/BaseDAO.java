package db;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import table.Property;
import util.Log;
import util.Util;

public abstract class BaseDAO {

	private static Map<Class<?>, Map<String, FieldHolder>> holderMap;
	static {
		holderMap = new HashMap<Class<?>, Map<String, FieldHolder>>();
	}

	/**
	 * クエリ結果のリストのみを取得します。
	 * @param sql
	 * @param handler
	 * @param cls
	 * @return
	 * @throws SQLException
	 */
	public static <T> List<T> GetQueryResultList(
			String sql, DBConnect.ToListHandler handler, Class<T> cls)
					throws SQLException {
		DBConnect db = new DBConnect(sql, false);
		List<T> list = db.ExcuteResult(handler);
		return list;
	}

	/**
	 * クエリ結果を取得します。<br>
	 * 引数のClassは結果レコードを格納するテーブルクラスである必要があります。
	 * @param sql
	 * @param handler
	 * @param cls
	 * @return
	 * @throws SQLException
	 */
	public static <T> QueryResult<T> GetQueryResult(
			String sql, DBConnect.ToListHandler handler, Class<T> cls)
					throws SQLException {
		DBConnect db = new DBConnect(sql, false);
		List<T> list = db.ExcuteResult(handler);
		QueryResult<T> result = new QueryResult<T>();
		result.List = list;
		result.JSON = ToJSON(list, cls, null);
		return result;
	}

	/**
	 * クエリ結果を取得します。<br>
	 * paramsには付加的な値をJSONに設定できます。<br>
	 * 引数のClassは結果レコードを格納するテーブルクラスである必要があります。
	 * @param sql
	 * @param handler
	 * @param cls
	 * @param params
	 * @return
	 * @throws SQLException
	 */
	public static <T> QueryResult<T> GetQueryResult(
			String sql, DBConnect.ToListHandler handler, Class<T> cls, Map<String, String> params)
					throws SQLException {
		DBConnect db = new DBConnect(sql, false);
		List<T> list = db.ExcuteResult(handler);
		QueryResult<T> result = new QueryResult<T>();
		result.List = list;
		result.JSON = ToJSON(list, cls, params);
		return result;
	}

	/**
	 * INSERT, UPDATE, DELETEのSQLを実行します。<br>
	 * SQLResult#ExecCountは実行された行数を返します。
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult ExecSQL(String sql) throws SQLException {
		DBConnect db = new DBConnect(sql, false);
		return execSQL(db);
	}

	/**
	 * INSERT, UPDATE, DELETEの複数のSQL文を実行します。<br>
	 * SQLResult#ExecCountは実行された合計行数を返します。
	 * @param sqls
	 * @param transaction
	 * @return
	 * @throws SQLException
	 */
	public static SQLResult ExecSQLs(List<String> sqls, boolean transaction) throws SQLException {
		DBConnect db = new DBConnect(sqls, true);
		return execSQL(db);
	}

	private static SQLResult execSQL(DBConnect db) throws SQLException {
		List<Integer> execList = db.ExcuteNonQueryCount();
		SQLResult result = new SQLResult();
		if (execList == null || execList.size() == 0) {
			result.Success = false;
			return result;
		}
		result.Success = true;
		result.ExecCount = execList.size();
		return result;
	}

	/**
	 * リストをJSON化します。<br>
	 * paramsには付加的な値をJSONに設定できます。
	 * @param list
	 * @param cls
	 * @param params
	 * @return
	 */
	public static <T> String ToJSON(List<T> list, Class<T> cls, Map<String, String> params) {
		StringBuilder json = new StringBuilder();
		json.append("{");
		json.append("\"result\":\"success\",");
		if (params != null && params.size() > 0) {
			for(Map.Entry<String, String> e : params.entrySet()) {
				json.append("\"").append(e.getKey()).append("\"");
				json.append(":");
				json.append("\"").append(Util.htmlEncode(e.getValue())).append("\",");
			}
		}
		json.append("\"datas\":").append(getDataArrayObjectJSON(cls, list));
		json.append("}");
		return json.toString();
	}

	private static <T> String getDataArrayObjectJSON(Class<T> cls, List<T> datas) {
		if (datas == null) return null;
		StringBuilder json = new StringBuilder();
		try {
			Map<String, FieldHolder> getters = getGetterMethodsOfClass(cls);
			if (getters == null) return null;
			//ゲッターメソッドを全て呼び出し、JSON化する
			T obj;
			String name;
			FieldHolder holder;
			int dataCount = 1;
			for(int i=0; i<datas.size(); i++) {
				obj = (T)datas.get(i);
				json.append("{");
				int nameCount = 1;
				for (Map.Entry<String, FieldHolder> e : getters.entrySet()) {
					name = e.getKey().toLowerCase();
					holder = e.getValue();
					json.append("\"").append(name).append("\"");
					json.append(":");
					json.append("\"").append(getValue(holder, obj)).append("\"");
					if (nameCount != getters.size()) {
						json.append(",");
					}
					nameCount++;
				}
				json.append("}");
				if (dataCount != datas.size()) {
					json.append(",");
				}
				dataCount++;
			}
			return "[" + json.toString() + "]";
		} catch (Exception ex) {
			ex.printStackTrace();
			Log.Write(ex.getStackTrace().toString());
			return null;
		}
	}

	private static String getValue(FieldHolder holder, Object object) {
		try {
			Property.ValueTypes type = holder.Property.ValueType;
			if (type == Property.ValueTypes.INT_DATE) {
				int valInt = (int)holder.Method.invoke(object);
				return Util.toTdate(valInt);
			}
			else if (type == Property.ValueTypes.STR) {
				String valStr = (String)holder.Method.invoke(object);
				return Util.htmlEncode(valStr);
			}
			else {
				String val = String.valueOf(holder.Method.invoke(object));
				return val;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return "";
		}
	}

	private static <T> Map<String, FieldHolder> getGetterMethodsOfClass(Class<T> cls) {
		if (holderMap.containsKey(cls)) {
			return holderMap.get(cls);
		}
		Map<String, FieldHolder> getters = new HashMap<String, FieldHolder>();
		try {
			Field[] fields = cls.getDeclaredFields();
			String fName;
			Method getter;
			Property property;
			Object obj = cls.newInstance();
			for (Field f : fields) {
				try {
					fName = f.getName();
					getter = getGetter(cls, fName);
					if (getter != null) {
						property = (Property)f.get(obj);
						getters.put(fName, new FieldHolder(property, getter));
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					continue;
				}
			}
			holderMap.put(cls, getters);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return getters;
	}

	private static <T> Method getGetter(Class<T> cls, String fieldName) {
		String getterName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
		String isGetterName = "is" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
		try {
			Method getter = cls.getDeclaredMethod(getterName, new Class[]{});
			return getter;
		} catch (Exception ex) {
			try {
				Method iser = cls.getDeclaredMethod(isGetterName, new Class[]{});
				return iser;
			} catch (Exception ex2) {}
			return null;
		}
	}


	public static class QueryResult<T> {
		public List<T> List;
		public String JSON;
	}

	public static class SQLResult {
		public boolean Success;
		public int ExecCount;
	}

	public static class FieldHolder {
		public Property Property;
		public Method Method;
		public FieldHolder (Property property, Method method) {
			Property = property;
			Method = method;
		}
	}

}
