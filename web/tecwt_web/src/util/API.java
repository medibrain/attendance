package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

// API呼び出し用にHttpURLConnectionをラップしたクラス
public class API {
	/**
	 * GET通信を行う
	 * @param url
	 * @param bearer OAuthなどの認証情報がある場合に付与
	 * @return 通信結果の文字列。失敗時はnull
	 */
	public static String get(URL url, String bearer) {
		HttpURLConnection connection = null;
		try {
			connection = (HttpURLConnection)url.openConnection();
			if (bearer != null) {
				connection.setRequestProperty("Authorization", "Bearer " + bearer);
			}

			StringBuilder stringBuilder = new StringBuilder();
			InputStreamReader input = new InputStreamReader(connection.getInputStream(), "UTF-8");
			BufferedReader reader = new BufferedReader(input);
			String line = null;
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line).append("\n");
			}
			return stringBuilder.toString();
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	/**
	 * POST通信を行う
	 * @param url
	 * @param parameter bodyに設定するデータ
	 * @param bearer OAuthなどの認証情報がある場合に付与
	 * @return 通信結果の文字列。失敗時はnull
	 */
	public static String post(URL url, String parameter, String bearer) {
		HttpURLConnection connection = null;
		try {
			byte[] payload = parameter.getBytes();

			connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);
			connection.setRequestProperty("Content-Length", String.valueOf(payload.length));
			if (bearer != null) {
				connection.setRequestProperty("Authorization", "Bearer " + bearer);
			}
			connection.getOutputStream().write(payload);
			connection.getOutputStream().flush();

			StringBuilder stringBuilder = new StringBuilder();
			InputStreamReader input = new InputStreamReader(connection.getInputStream());
			BufferedReader reader = new BufferedReader(input);
			String line = null;
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line).append("\n");
			}
			return stringBuilder.toString();
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
	}

	/**
	 * JSON文字列をmap形式に変換する
	 * @param json 変換したいJSON文字列
	 * @return 変換結果
	 */
	public static Map<String, String> jsonStringToMap(String json) {
		ObjectMapper mapper = new ObjectMapper();
		TypeReference<HashMap<String, String>> reference = new TypeReference<HashMap<String, String>>() {};
		try {
			return mapper.readValue(json, reference);
		} catch (IOException e) {
			Log.exceptionWrite(e);
			return new HashMap<String, String>();
		}
	}

	/**
	 * mapをクエリ文字列に変換する
	 * @param map
	 * @return "?"から始まるクエリ文字列
	 */
	public static String mapToQueryString(Map<String, String> map) {
		StringBuilder stringBuilder = new StringBuilder();
		String prefix = "?";
		for (String key : map.keySet()) {
			String value = map.get(key);
			stringBuilder.append(prefix).append(key).append("=").append(value);
			prefix = "&";
	    }
		return stringBuilder.toString();
	}
}
