package util;

// とりあえずここに改修分だけまとめていく
// 定数名などはほぼそのまま移管する
public enum ErrorMessage {
    // unit_11("oid(組織ID)が判別できませんでした"),
    // unit_12("端末IDが取得できませんでした"),
    unit_13("この端末は登録されていません"),
    unit_14("この端末からの入力は受け付けられません"),
    unit_15("ログインに失敗しました"),
    unit_ex("端末のログイン処理でエラーが発生しました"),

    timecard_out01("IDm(NFCタグID)が判別できませんでした"),
    // timecard_out02("読込まれたIDmは登録されていません"),
    timecard_out02_1("読込まれたIDmは登録されていないか無効です"),
    // timecard_out05("業務種別が判別できませんでした"),
    // timecard_out06("指定された業務種別は無効です"),
    timecard_out07("緯度が指定されていません"),
    timecard_out08("経度が指定されていません"),
    timecard_out09("端末IDが判別できませんでした"),
    timecard_out10("退勤時間が判別できませんでした"),
    timecard_out11("タイムカードの日付が本日ではありません"),
    timecard_out12("指定された退勤時間は15分単位ではありません"),
    timecard_out13("指定された退勤時間が不明です"),
    // timecard_out17("交通費が判別できませんでした"),
    // timecard_out18("休憩時間が判別できませんでした"),
    timecard_out19("指定された休憩時間は15分単位ではありません"),
    timecard_out20("退勤記録でエラーが発生しました"),
    timecard_out21_1("退勤時間が出勤時間を下回ることはできません。時間を確認して下さい"),
    timecard_out21_2("現在時刻より後の退勤時間は指定できません"),
    timecard_outex("退勤記録でエラーが発生しました");

    private final String message;

    private ErrorMessage(final String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }
}
