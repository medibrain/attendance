/*
 * [15/07/27]
 * 松本さんから頂戴したソースコード「tecwt」内にあったものをコピペしたものです。
 * 記述者：久保
 */

package util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

public class Log {

    static String dirName;	// ログ置き場
    static File file;		// 通常ログの書き込み場所

    static {
    	// 現在のパスを取得
        dirName = new File("").getAbsolutePath();
        if (dirName.equals(File.separator)) {
        	// rootなら書き込みたいディレクトリまでのパスを記述
        	//TODO: medi-kintai
            //dirName = "/usr/local/tomcat8/webapps/logs/tecwt_web";
            dirName = "/usr/local/tomcat/webapps/logs/tecwt_web";
        } else {
        	// rootじゃないなら直下に
            dirName += File.separator + "logs/tecwt_web";	// テスト環境用
        }
        // 通常ログの記載場所
        file = new File(dirName + File.separator + "log.log");
    }

    static void createLogDirs() {
        File f = new File(dirName);
        if (!f.exists()) {
            f.mkdirs();
        }
    }

    static boolean createLogFile(File file) throws IOException {
    	if(!file.exists()) {
    		return file.createNewFile();
    	}
    	return true;
    }

    /**
     * エラー以外のログを書き込みます。<br>
     * すべて同じ場所に追記されます。
     * @param str
     */
    public static void Write(String str) {
        createLogDirs();
        try {
        	if(!createLogFile(file)) return;
            FileWriter filewriter = new FileWriter(file, true);
            filewriter.write(str + "\r\n");
            filewriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * エラーログを書き込みます。<br>
     * ファイルは日付ごとにまとめて記載されます。
     * @param e
     */
    public static void exceptionWrite(Exception e) {
        createLogDirs();
        String fn = dirName + File.separator + "error" + DateTime.GetTodayStr() + ".log";
        File errorFile = new File(fn);
        try {
        	if(!createLogFile(errorFile)) return;
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String trace = sw.toString();
            sw.close();
            pw.close();

            FileWriter filewriter = new FileWriter(errorFile, true);
            filewriter.write(DateTime.GetNowStr());
            filewriter.write(trace + "\r\n");

            filewriter.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
