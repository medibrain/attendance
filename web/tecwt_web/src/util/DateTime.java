package util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTime {

	/**
	 * 時間間隔をhhmmのintで取得します
	 *
	 * @param inTime
	 *            開始時刻 hhmm
	 * @param outTime
	 *            終了時刻 hhmm
	 * @param breakMin
	 *            休憩時間分 [m]
	 * @return
	 */
	public static int GetTimeSpan(int inTime, int outTime, int breakMin) {
		int inm = (inTime / 100 * 60) + (inTime % 100);
		int outm = (outTime / 100 * 60) + (outTime % 100);
		int m = outm - inm - breakMin;

		return m / 60 * 100 + m % 60;
	}
	/**
	 * 時間間隔をhhmmのintで取得します
	 *
	 * @param inTime
	 *            開始時刻 hhmm
	 * @param outTime
	 *            終了時刻 hhmm
	 * @param breakMin
	 *            休憩時間分 [m]
	 * @return
	 */
	public static int GetTimeSpan(String inTime, String outTime, String breakMin) {
		try {
			int in = Integer.parseInt(inTime);
			int out = Integer.parseInt(outTime);
			int brk = Integer.parseInt(breakMin);
			return GetTimeSpan(in, out, brk);
		} catch (Exception ex) {
			return -1;
		}
	}

	/**
	 * int6桁で表された2つの日付の日数差を取得します。失敗した場合、nullが返ります。
	 *
	 * @param from
	 * @param to
	 * @return
	 */
	public static Integer GetDateSpan(Calendar from, Calendar to) {
		try {
			// 日付をlong値に変換
			long dateTimeTo = to.getTime().getTime();
			long dateTimeFrom = from.getTime().getTime();
			// 秒数を取得
			long dayDiff = (dateTimeTo - dateTimeFrom) / (1000 * 60 * 60 * 24);
			return (int) dayDiff;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 今日の日付をyyyyMMddのintで取得します
	 *
	 * @return
	 */
	public static int GetTodayInt() {
		Calendar c = Calendar.getInstance();
		int y = c.get(Calendar.YEAR); // 年を取得
		int m = c.get(Calendar.MONTH) + 1; // 月を取得(※1)
		int d = c.get(Calendar.DATE); // 日を取得
		return y * 10000 + m * 100 + d;
	}

	/**
	 * 現在時刻をhhmmssのintで取得します
	 *
	 * @return
	 */
	public static int GetNowInt() {
		Calendar c = Calendar.getInstance();
		int h = c.get(Calendar.HOUR_OF_DAY); // 時を取得
		int m = c.get(Calendar.MINUTE) + 1; // 分を取得(※1)
		int s = c.get(Calendar.SECOND); // 秒を取得
		return h * 10000 + m * 100 + s;
	}

	/**
	 * 現在時刻をhhmmのintで取得します
	 *
	 * @return
	 */
	public static int GetNowHHMMInt() {
		Calendar c = Calendar.getInstance();
		int h = c.get(Calendar.HOUR_OF_DAY); // 時を取得
		int m = c.get(Calendar.MINUTE) + 1; // 分を取得(※1)
		return h * 100 + m;
	}

	/**
	 * 現在時刻をhhmmのstringで取得します
	 *
	 * @return
	 */
	public static String GetNowHHMMStr() {
		Calendar c = Calendar.getInstance();
		int h = c.get(Calendar.HOUR_OF_DAY); // 時を取得
		int m = c.get(Calendar.MINUTE) + 1; // 分を取得(※1)
		return String.valueOf(h * 100 + m);
	}

	/**
	 * 指定したDate型の日付をyyyyMMddのintで取得します
	 *
	 * @param dt
	 * @return
	 */
	public static int DateToIntDate(Date dt) {
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		int y = c.get(Calendar.YEAR); // 年を取得
		int m = c.get(Calendar.MONTH) + 1; // 月を取得(※1)
		int d = c.get(Calendar.DATE); // 日を取得
		return y * 10000 + m * 100 + d;
	}

	/**
	 * 指定したDate型の時刻をhhmmssのintで取得します
	 *
	 * @param dt
	 * @return
	 */
	public static int DateToIntTime(Date dt) {
		Calendar c = Calendar.getInstance();
		c.setTime(dt);
		int h = c.get(Calendar.HOUR); // 時を取得
		int m = c.get(Calendar.MINUTE); // 分を取得(※1)
		int s = c.get(Calendar.SECOND); // 秒を取得
		return h * 10000 + m * 100 + s;
	}

	/**
	 * yyyyMMddの数字をCalendarに変換します。失敗した場合、nullを返します。
	 *
	 * @param yyyyMMdd
	 * @return
	 */
	public static Calendar IntToCalendar(int yyyyMMdd) {
		Calendar c = Calendar.getInstance();
		try {
			c.set(yyyyMMdd / 10000, (yyyyMMdd / 100 % 100) - 1, yyyyMMdd % 100,
					0, 0, 0);
			c.set(Calendar.MILLISECOND, 0);
			c.setLenient(false);
			c.getTime();
			return c;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * 今日の0:00のDateを取得します。
	 *
	 * @return
	 */
	public static Calendar Today() {
		Calendar c = Calendar.getInstance();
		setDay0(c);
		return c;
	}

	private static void setDay0(Calendar c) {
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
	}

	/**
	 * 現在時刻を文字列 yyyy/MM/dd HH:mm で取得します
	 *
	 * @return
	 */
	public static String GetNowStr() {
		// 現在日時を取得する
		Calendar c = Calendar.getInstance();

		// フォーマットパターンを指定して表示する
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		return sdf.format(c.getTime());
	}

	/**
	 * 現在日付を文字列 yyyyMMdd で取得します
	 *
	 * @return
	 */
	public static String GetTodayStr() {
		// 現在日時を取得する
		Calendar c = Calendar.getInstance();

		// フォーマットパターンを指定して表示する
		SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd");
		return f.format(c.getTime());
	}

	/**
	 * 指定した年月日の曜日を取得します。("曜日"は付きません)
	 * @param yyyyMMdd
	 * @return
	 */
	public static String GetDayOfWeek(int yyyyMMdd) {
		Calendar c = Calendar.getInstance();

		// 分割
		int yyyy = yyyyMMdd / 10000;
		int MM = yyyyMMdd / 100 - (yyyy * 100);
		int dd = yyyyMMdd - ((yyyy * 10000) + (MM * 100));

		// 曜日取得
		c.set(yyyy, MM-1, dd);
		String[] dayofweek = {"日","月","火","水","木","金","土"};
		return dayofweek[c.get(Calendar.DAY_OF_WEEK) - 1];
	}

	/**
	 * int8桁で表された日付をy/M/dの文字列で返します。
	 *
	 * @param date
	 * @return
	 */
	public static String IntDateToString(int date) {
		return String.format("%1$04d/%2$02d/%3$02d", date / 10000,
				date / 100 % 100, date % 100);
	}

	/**
	 * int8桁で表された日付をM/dの文字列で返します。
	 *
	 * @param date
	 * @return
	 */
	public static String IntDayToString(int date) {
		return String.format("%1$02d/%2$02d", date / 100 % 100, date % 100);
	}

	/**
	 * int4桁で表された時間を、hh:mmの文字列で返します。<br>
	 * 負の値が指定された場合は空文字列を返します。
	 * @param time
	 * @return
	 */
	public static String IntTimeToString(int time) {
		if (time < 0) return "";
		return String.format("%1$02d:%2$02d", time / 100, time % 100);
	}

	/**
	 * yyyyMMddまたはy/M/dの文字列をint8桁の情報に変換します。 失敗した場合、0が返ります。
	 *
	 * @param str
	 * @return
	 */
	public static int GetDateInt(String str) {
		char[] cs = Common.toHalf(str.trim()).toCharArray();
		StringBuilder sb = new StringBuilder();
		boolean isBS = true;
		for (int i = 0; i < cs.length; i++) {
			if (cs[i] < '0' || cs[i] > '9') {
				if (isBS) {
					continue;
				}
				isBS = true;
				sb.append('/');
			} else {
				isBS = false;
				sb.append(cs[i]);
			}
		}

		str = sb.toString();
		String[] ss = str.split("/");
		if (ss.length < 3) {
			if (ss.length > 1 || ss[0].length() != 8) {
				return 0;
			}
			ss = new String[3];
			ss[0] = str.substring(0, 4);
			ss[1] = str.substring(5, 6);
			ss[2] = str.substring(7, 8);
		}

		int y = Common.tryParse(ss[0]);
		int m = Common.tryParse(ss[1]);
		int d = Common.tryParse(ss[2]);

		Calendar c = Calendar.getInstance();
		try {
			c.setLenient(true);
			c.set(y, m, d);
		} catch (Exception e) {
			return 0;
		}

		return y * 10000 + m * 100 + d;
	}

	/**
	 * yyyyMM または y/M の文字列をint6桁の情報に変換します。 失敗した場合、0が返ります。
	 *
	 * @param str
	 * @return
	 */
	public static int GetMonthInt(String str) {
		char[] cs = Common.toHalf(str.trim()).toCharArray();
		StringBuilder sb = new StringBuilder();
		boolean isBS = true;
		for (int i = 0; i < cs.length; i++) {
			if (cs[i] < '0' || cs[i] > '9') {
				if (isBS) {
					continue;
				}
				isBS = true;
				sb.append('/');
			} else {
				isBS = false;
				sb.append(cs[i]);
			}
		}

		str = sb.toString();
		String[] ss = str.split("/");
		if (ss.length < 2) {
			if (ss.length > 1 || ss[0].length() != 6) {
				return 0;
			}
			ss = new String[2];
			ss[0] = str.substring(0, 4);
			ss[1] = str.substring(5, 6);
		}

		int y = Common.tryParse(ss[0]);
		int m = Common.tryParse(ss[1]);

		if (y < 1900 || y > 2100 || m > 12 || m < 1) {
			return 0;
		}
		return y * 100 + m;
	}

	public static int GetTimeInt(String str) {
		String hourStr = "";
		String minStr = "";

		int hour;
		int min;

		// 半角変換
		str = Common.toHalf(str);
		int i = 0;

		// 時間部分を取得
		for (; i < str.length(); i++) {
			if ("0123456789".indexOf(str.charAt(i)) >= 0) {
				hourStr += str.charAt(i);
			}
			if ("/:-時".indexOf(str.charAt(i)) >= 0) {
				break;
			}
			if (hourStr.length() == 2) {
				break;
			}
		}

		i++;

		// 分部分を取得
		for (; i < str.length(); i++) {
			if ("0123456789".indexOf(str.charAt(i)) >= 0) {
				minStr += str.charAt(i);
			}
		}

		// 数値に変換
		try {
			hour = Integer.parseInt(hourStr);
			if (minStr.isEmpty()) {
				min = 0;
			} else {
				min = Integer.parseInt(minStr);
			}
		} catch (Exception e) {
			return 0;
		}

		// 午後時間であれば　時間+12
		if (str.contains("後") || str.contains("P") || str.contains("p")) {
			hour += 12;
		}

		if (hour > 32) {
			return 0;
		}
		if (min >= 60) {
			return 0;
		}

		// 4桁の数字
		return hour * 100 + min;
	}

	public static int PlusMinutes(int baseTime, int plusMinutes) {
		int h = baseTime / 100;
		int m = baseTime % 100;
		m += plusMinutes;

		while (m < 0 || m >= 60) {
			if (m < 0) {
				m = m + 60;
				h = h - 1;
			} else if (m >= 60) {
				m = m - 60;
				h = h + 1;
			}
		}

		return h * 100 + m;
	}

	/**
	 * 整数以上のint型の日付（８桁）をスラッシュ区切りの日付形式に変換します。<br>
	 * 変換できない場合は"----/--/--"を返します。
	 * @param dateValue
	 * @return
	 */
	public static String ToDateString(int dateValue) {
		String val = String.valueOf(dateValue);
		if (val.length() != 8) return "----/--/--";
		StringBuilder sr = new StringBuilder();
		sr.append(val.substring(0, 4)).append("/");
		sr.append(val.substring(4, 6)).append("/");
		sr.append(val.substring(6));
		return sr.toString();
	}
}
