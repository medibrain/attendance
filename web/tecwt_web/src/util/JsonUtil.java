package util;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;


public class JsonUtil {

	/**
	 * JSON文字列を指定クラス型に変換します。<br>
	 * 失敗した場合はnullを返します。
	 * @param cls
	 * @param json
	 * @return
	 */
	public static <T> T ToObject(String json, Class<T> cls) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return (T)mapper.readValue(json, cls);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	/**
	 * トップレベルが配列なJSON文字列を指定クラス型に変換します。<br>
	 * 失敗した場合はnullを返します。
	 * @param json
	 * @param cls
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> ToObjectList(String json, Class<T> cls) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			CollectionType ct = mapper.getTypeFactory().constructCollectionType(List.class, cls);
			return (List<T>)mapper.readValue(json, ct);
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	/**
	 * インスタンスをJSON文字列化します。
	 * @param obj
	 * @return
	 */
	public static String ToJSON(Object obj) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
			return json;
		} catch (Exception ex) {
			return null;
		}
	}

}
