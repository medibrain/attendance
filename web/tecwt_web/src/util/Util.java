package util;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



public class Util {

	public static String APP_NAME = "tecwt_web";
	private static String[] htmlEncodeChars = {"&", "\"", "<", ">"};
	private static String[] htmlEncodedChars = {"&amp;", "&quot;", "&lt;", "&gt;"};

	/**
	 * セッションの生存を確認し、切れていた場合はセッションエラーのJSONを返します。<br>
	 * セッションが生存していた場合はsuccessのJSONを返します。<br>
	 * @param session
	 * @param response
	 * @return
	 * @throws IOException
	 */
	public static String execIsSessionTimeoutForAjax(HttpSession session, HttpServletResponse response) throws IOException {

		if(session == null) {
			// そもそもセッションがない
			return toSessionTimeoutJSON("セッションが切れています。再度ログインし直して下さい。");
		}

		String session_oid = (String)session.getAttribute("oid");
		if(session_oid == null) {
			// セッション切れ
			return toSessionTimeoutJSON("セッションが切れています。再度ログインし直して下さい。");
		}

		// セッション生存
		String soid = (String)session.getAttribute("oid");
		String suid = (String)session.getAttribute("uid");
		return toSessionSuccessJSON("セッションは有効です。", soid, suid);
	}

	/**
	 * タブの閲覧権限があるかどうかを判定します。<br>
	 * adminが"true"である場合のみtrueを返します。
	 * @param admin
	 * @return
	 */
	public static boolean execIsAdmin(String admin) {
		if(admin == null) return false;
		if(admin.equals("true")) return true;
		return false;
	}

	/**
	 * エラーメッセージ付きJSONを作成する。
	 * @param message
	 * @return
	 */
	public static String toErrorJSON(String message) {
		StringBuilder json = new StringBuilder();
		json.append("{");
		json.append("\"result\":").append("\"error\",");
		json.append("\"message\":").append("\"").append(message).append("\"");
		json.append("}");
		return json.toString();
	}

	/**
	 * 成功メッセージ付きJSONを作成する。
	 * @param message
	 * @return
	 */
	public static String toSuccessJSON(String message) {
		StringBuilder json = new StringBuilder();
		json.append("{");
		json.append("\"result\":").append("\"success\",");
		json.append("\"message\":").append("\"").append(message).append("\"");
		json.append("}");
		return json.toString();
	}

	/**
	 * セッション生存メッセージ付きJSONを作成する。<br>
	 * Ajax用に組織ID(soid)とユーザID(suid)も付与されています。
	 * @param message
	 * @return
	 */
	public static String toSessionSuccessJSON(String message, String soid, String suid) {
		StringBuilder json = new StringBuilder();
		json.append("{");
		json.append("\"result\":").append("\"success\",");
		json.append("\"message\":").append("\"").append(message).append("\",");
		json.append("\"soid\":").append("\"").append(soid).append("\",");
		json.append("\"suid\":").append("\"").append(suid).append("\"");
		json.append("}");
		return json.toString();
	}

	/**
	 * セッション切れメッセージ付きJSONを作成する。
	 * @param message
	 * @return
	 */
	public static String toSessionTimeoutJSON(String message) {
		StringBuilder json = new StringBuilder();
		json.append("{");
		json.append("\"result\":").append("\"session_timeout\",");
		json.append("\"message\":").append("\"").append(message).append("\"");
		json.append("}");
		return json.toString();
	}

	/**
	 * int型の日付(20150701)を日付形式(2015/07/01)に変換
	 * @param tdate
	 * @return
	 */
	public static String toTdate(int tdate) {
		String strTdate = String.valueOf(tdate);
		if(strTdate.length() == 8) {
			StringBuilder builder = new StringBuilder(strTdate);
			builder.insert(4, "/");	// 年区切り
			builder.insert(7, "/");	// 月区切り("/"追加分だけシフト)
			strTdate = builder.toString();
		}
		return strTdate;
	}

	/**
	 * int型の時間(1015)を時・分(10・15)に分割し、時だけを取得。<br>
	 * 3桁（時部分が1桁）の場合は、先頭に0を一つ追加してから処理を行います。
	 * 2桁（時部分が0で分部分が2桁）の場合は、先頭に0を二つ追加してから処理を行います。
	 * 0（00:00）の場合は、先頭に0を三つ追加してから処理を行います。
	 * -1（存在しないとき）は、"----"にしてから処理を行います。<br>
	 * それ以外はそのまま返します。
	 * @param time
	 * @return
	 */
	public static String getHour(int time) {
		String strTime = String.valueOf(time);
		if(time == 0) {
			strTime = "0000";
			return strTime.substring(0, 2);
		}
		else if(time == -1) {
			strTime = "----";
			return strTime.substring(2, 4);
		}
		else if(strTime.length() == 4) {
			return strTime.substring(0, 2);
		}
		else if(strTime.length() == 3) {
			strTime = "0" + strTime;
			return strTime.substring(0, 2);
		}
		else if(strTime.length() == 2) {
			strTime = "00" + strTime;
			return strTime.substring(0, 2);
		}

		return strTime;
	}

	/**
	 * int型の時間(1015)を時・分(10・15)に分割し、分だけを取得。<br>
	 * 3桁（時部分が1桁）の場合は、先頭に0を一つ追加してから処理を行います。
	 * 2桁（時部分が0で分部分が2桁）の場合は、先頭に0を二つ追加してから処理を行います。
	 * 0（00:00）の場合は、先頭に0を三つ追加してから処理を行います。
	 * -1（存在しないとき）は、"----"にしてから処理を行います。<br>
	 * それ以外はそのまま返します。
	 * @param time
	 * @return
	 */
	public static String getMinute(int time) {
		String strTime = String.valueOf(time);
		if(time == 0) {
			strTime = "0000";
			return strTime.substring(2, 4);
		}
		else if(time == -1) {
			strTime = "----";
			return strTime.substring(2, 4);
		}
		else if(strTime.length() == 4) {
			return strTime.substring(2, 4);
		}
		else if(strTime.length() == 3) {
			strTime = "0" + strTime;
			return strTime.substring(2, 4);
		}
		else if(strTime.length() == 2) {
			strTime = "00" + strTime;
			return strTime.substring(0, 2);
		}
		return strTime;
	}

	/**
	 * 日付(yyyy/MM/dd)をint型(yyyyMMdd)に変換。<br>
	 * nullの場合は-1を返します。
	 * @param tdate
	 * @return
	 * @throws NumberFormatException
	 */
	public static int convertTdate(String tdate) throws NumberFormatException {
		if(tdate == null) return -1;
		int intTdate = Integer.parseInt(replaceTdate(tdate));
		return intTdate;
	}

	/**
	 * 日付(yyyy/MM/dd)をString型(yyyyMMdd)に変換。<br>
	 * nullの場合はnullを返します。
	 * @param tdate
	 * @return
	 */
	public static String replaceTdate(String tdate) {
		if(tdate == null) return null;
		String replece = tdate.replaceAll("/", "");
		return replece;
	}

	/**
	 * 時間(hhmm)をint型に変換。<br>
	 * nullの場合は-1を返します。
	 * @param time
	 * @return
	 * @throws NumberFormatException
	 */
	public static int convertTime(String time) throws NumberFormatException {
		if(time == null) return -1;
		int intTdate = Integer.parseInt(time);
		return intTdate;
	}

	/**
	 * JSON用にエスケープ処理を行う。
	 * @param text
	 * @return
	 */
	public static String jsonParse(String text) {
		if(text == null || text.equals("")) {
			return text;
		}

		// 改行コード「\n」の先頭に「\」を追加
		text = text.replaceAll("\\n", "\\\\n");

		// 改行コード「\r」の先頭に「\」を追加
		text = text.replaceAll("\\r", "\\\\n");

		// ダブルクォーテーション「"」の先頭に「\」を追加
		text = text.replaceAll("\"", "\\\"");

		// シングルクォーテーション「'」の先頭に「\」を追加
		text = text.replaceAll("\"", "\\\"");

		// コロン「:」の先頭に「\」を追加
		text = text.replaceAll(":", "\\:");

		// セミコロン「;」の先頭に「\」を追加
		text = text.replaceAll(";", "\\;");

		return text;
	}

	/**
	 * エラーかどうかを判断します。<br>
	 * nullの場合はtrueを返します。
	 * @param json
	 * @return
	 */
	public static boolean isError(String json) {
		if(json == null) return true;
		return json.contains("\"result\":\"error\"");
	}

	/**
	 * nullであれば「""」を返します。
	 * @param value
	 * @return
	 */
	public static String nullToBlank(String value) {
		return (value == null) ? "" : value;
	}

	/**
	 * -1であれば「----」を返します。
	 * @param value
	 * @return
	 */
	public static String minusOneToHyphen(int value) {
		return (value == -1) ? "----":String.valueOf(value);
	}

	/**
	 * nullまたは""であれば「----」を返します。<br>
	 * それ以外はそのまま返します。
	 * @param value
	 * @return
	 */
	public static String nullBlankToHyphen(String value) {
		return (value == null || value.equals("")) ? "----" : value;
	}

	/**
	 * null,"",--,----であればnullを返します。<br>
	 * @param value
	 * @return
	 */
	public static String nullBlankHyphenToNull(String value) {
		return (value == null || value.equals("") || value.equals("--") || value.equals("----")) ? null : value;
	}

	/**
	 * HTMLエンコードを行います。<br>
	 * nullの場合はnullを返します。
	 * @param str
	 * @return
	 */
	public static String htmlEncode(String str) {
		if(str == null) return null;
		String result = str;
		for(int i=0; i<htmlEncodeChars.length; i++) {
			result = result.replaceAll(htmlEncodeChars[i], htmlEncodedChars[i]);
		}
		return result;
	}

	/**
	 * HTMLデコードを行います。<br>
	 * nullの場合はnullを返します。
	 * @param str
	 * @return
	 */
	public static String htmlDecode(String str) {
		if(str == null) return null;
		String result = str;
		for(int i=0; i<htmlEncodedChars.length; i++) {
			result = result.replaceAll(htmlEncodedChars[i], htmlEncodeChars[i]);
		}
		return result;
	}

	/**
	 * 改行コード(\n,\r\n,\t)を削除します。<br>
	 * nullの場合はnullを返します。
	 * @param str
	 * @return
	 */
	public static String replaceCRLF(String str) {
		if(str == null) return null;
		String replace = str;
		replace = replace.replaceAll("\r\n", "");
		replace = replace.replaceAll("\n", "");
		replace = replace.replaceAll("\t", "");
		return replace;
	}

	/**
	 * nullと""の場合はnullを返します。
	 * @param str
	 * @return
	 */
	public static String nullBlankCheck(String str) {
		if(str == null) return null;
		if(str.equals("")) return null;
		return str;
	}

	/**
	 * nullまたは""である場合にtrueを返します。<br>
	 * @param str
	 * @return
	 */
	public static boolean isNullOrBlank(String str) {
		if(str == null) return true;
		if(str.equals("")) return true;
		return false;
	}

	/**
	 * nullまたは""または"--","----"である場合にtrueを返します。<br>
	 * @param str
	 * @return
	 */
	public static boolean isNullOrBlankOrHyphen(String str) {
		if(str == null) return true;
		if(str.equals("")) return true;
		if(str.equals("--")) return true;
		if(str.equals("----")) return true;
		return false;
	}

	/**
	 * JSON文字列からオブジェクト内に入っている値を取得します。<br>
	 * 指定したキーが入っていない場合はnullを返します。<br>
	 * オブジェクト内にさらにオブジェクトや配列が入っているものは取得できません。<br>
	 * @param json
	 * @param keyword
	 * @return
	 */
	public static String jsonSubstring(String json, String key) {
		if(json.indexOf(key) == -1) return null;

		int start = json.indexOf(key) + key.length();
		int end = json.indexOf(",", start);
		if(end == -1) {
			// 以降に「,」が登場せず、末尾が「}」となっているなら末尾を除く
			if(json.lastIndexOf("}") == json.length()) {
				end = json.length() - 1;
			}
		}
		String result = json.substring(start, end).replaceAll("\"", "");	// 「"」は含めない
		return result;
	}

	/**
	 * Util#toTime(String)を参照。<br>
	 * ただしtimeが負の値もしくは5桁以上の場合はnullを返します。<br>
	 * @param time
	 * @return
	 */
	public static String toTime(int time) {
		if(time < 0) return null;
		String strTime = String.valueOf(time);
		if(strTime.length() > 4) return null;
		return toTime(strTime);
	}
	/**
	 * 0,45,100,1215などを時間(HH:MM)に成型します。<br>
	 * -> 00:00,00:45,01:00,12:45<br>
	 * ただしtimeが数値でない、もしくは負の値、もしくは5桁以上の場合はnullを返します。<br>
	 * @param str
	 * @return
	 */
	public static String toTime(String time) {
		if(time == null) return null;
		int intTime = 0;
		try {
			intTime = Integer.parseInt(time);
			if(intTime < 0) return null;
		} catch (NumberFormatException ex) {
			return null;
		}
		if(time.length() > 4) return null;
		StringBuilder sb = new StringBuilder(time);
		if(time.length() == 3) {
			sb.insert(0, "0");	// 先頭に0付加
		}
		if(time.length() == 2) {
			sb.insert(0, "00");	// 先頭に00付加
		}
		if(time.length() == 1) {
			sb.insert(0, "000");// 先頭に000付加
		}

		return sb.insert(2, ":").toString();
	}

	/**
	 * SQL文内で「'」を文字列として扱う場合用にエスケープを行います。
	 * @param str
	 * @return
	 */
	public static String toSQLParse(String str) {
		if(str == null) return null;
		return str.replaceAll("'", "''");
	}

	/**
	 * 数値文字列を数値(int)に変換します。<br>
	 * 変換できなかった場合は-1を返します。
	 * @param number
	 * @return
	 */
	public static int toInt(String number) {
		try {
			return Integer.parseInt(number);
		} catch (NumberFormatException ex) {
			return -1;
		}
	}

	/**
	 * 文字列を数値(Integer)に変換します。<br>
	 * 文字列がnullや変換できなかった場合はnullを返します。
	 * @param text
	 * @return
	 */
	public static Integer toNullableInteger(String text) {
		try {
			return Integer.parseInt(text);
		} catch (Exception ex) {
			return null;
		}
	}

	/**
	 * 真偽値文字列をbooleanに変換します。<br>
	 * 失敗した場合はfalseを返します。
	 * @param booleanValue
	 * @return
	 */
	public static boolean toBoolean(String booleanValue) {
		return Boolean.parseBoolean(booleanValue);
	}

	/**
	 * 日付文字列（"/","-"区切り）をintに変換します。yyyy/MM-dd => yyyyMMdd<br>
	 * 失敗した場合は-1を返します。
	 * @param dateIntValue
	 * @return
	 */
	public static int toDateInt(String dateIntValue) {
		if (dateIntValue == null) return -1;
		String tmp = dateIntValue;
		tmp = tmp.replace("/", "");
		tmp = tmp.replace("-", "");
		return toInt(tmp);
	}

	/**
	 * 「"」を「""」でエスケープします。
	 * @param str
	 * @param nullBlank true : nullの場合に空文字列を返します。
	 * @return
	 */
	public static String doubleQuoteEscape(String str, boolean nullBlank) {
		if(str == null) {
			return nullBlank ? "" : null;
		}
		return str.replaceAll("\"", "\"\"");
	}

	/**
	 * URLのorganization部分を取得する
	 * @param servPath
	 * @return
	 */
	public static String getOrgString(String servPath) {
		String[] paths = getPaths(servPath);
		if(paths == null) {
			return null;
		}
		String org = paths[1];
		if(org == null || org.equals("")) {
			return null;
		}
		return org;
	}
	/**
	 * URLのservlet部分を取得する
	 * @param servPath
	 * @return
	 */
	public static String getServString(String servPath) {
		String[] paths = getPaths(servPath);
		if(paths == null) {
			return null;
		}
		String serv = paths[2];
		if(serv == null || serv.equals("")) {
			return null;
		}
		return serv;
	}
	/**
	 * 下記の場合における[etc]を取得する<br>
	 * http://example.com/example/[organization]/[servlet]/[etc]
	 * @param servPath
	 * @return
	 */
	public static String getServEtcString(String servPath) {
		if(servPath == null) {
			return null;
		}
		String[] paths = servPath.split("/", -1);
		if(paths == null || paths.length < 4) {
			return null;
		}
		String iServ = paths[3];
		if(iServ == null || iServ.equals("")) {
			return null;
		}
		return iServ;
	}
	private static String[] getPaths(String servPath) {
		if(servPath == null) {
			return null;
		}
		String[] paths = servPath.split("/", -1);
		if(paths == null || paths.length < 3) {
			return null;
		}
		return paths;
	}

}
