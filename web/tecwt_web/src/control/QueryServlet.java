package control;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.QueryLogic;

/**
 * Servlet implementation class QueryServlet
 */
public class QueryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public QueryServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// 返却はJSON
		response.setContentType("application/json;charset=UTF-8");

		// キャッシュ無効(IEはここでも記述が必要)
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);

		// JSON貼り付け用
		PrintWriter out = response.getWriter();

		// セッション確認済みのリクエストからoid,uidを取得
		HttpSession session = request.getSession(false);
		String session_oid = (String)session.getAttribute("oid");
		String session_uid = (String)session.getAttribute("uid");

		// 各種分岐
		String action = request.getParameter("action");
		if(action == null) return;

		/*********************************
		 * 業務種別
		 *********************************/
		else if(action.equals("get_worktypes")) {
			String param = request.getParameter("year");
			Integer year = null;
			if (param != null) {
				try {
					year = Integer.parseInt(param);
				} catch(Exception e) {
					// 無視してOK
				}
			}

			if (request.getParameter("department") != null) {
				out.write(QueryLogic.getWorktypesForDepartment(session_oid));
			} else {
				// 一覧
				out.write(QueryLogic.getWorktypes(session_oid, year));
			}
		}
		else if(action.equals("get_worktypes_enable")) {
			// 選択可能種別一覧
			out.write(QueryLogic.getWorktypesOfEnable(session_oid));
		}
		else if(action.equals("query_worktype_daterange")) {
			// 日付範囲検索
			String date1 = request.getParameter("date1");
			String date2 = request.getParameter("date2");
			out.write(QueryLogic.getWorktypeOfDateRange(session_oid, date1, date2));
		}
		else if(action.equals("query_worktype_enable")) {
			// 管理状況検索
			String enable = request.getParameter("enable");
			out.write(QueryLogic.getWorktypeOfEnable(session_oid, enable));
		}
		else if(action.equals("query_worktype_today")) {
			// 本日稼動中検索
			out.write(QueryLogic.getWorktypeOfToday(session_oid));
		}
		else if(action.equals("query_worktype_wid")) {
			// 勤務種別ID検索
			String wid = request.getParameter("wid");
			out.write(QueryLogic.getWorktypeOfWid(session_oid, wid));
		}

		/*********************************
		 * ユーザ
		 *********************************/
		else if(action.equals("get_users")) {
			// 一覧
			out.write(QueryLogic.getUsers(session_oid));
		}
		else if(action.equals("get_users_enable")) {
			// 選択可能者一覧
			out.write(QueryLogic.getUsersOfEnable(session_oid));
		}
		else if(action.equals("query_users_daterange")) {
			// 日付範囲検索
			String date1 = request.getParameter("date1");
			String date2 = request.getParameter("date2");
			out.write(QueryLogic.getUsersOfDateRange(session_oid, date1, date2));
		}
		else if(action.equals("query_users_code")) {
			// 社員番号検索
			String oid = request.getParameter("oid");
			if(oid == null || oid.equals("")) {
				oid = session_oid;	// パラメータにoidが付属していない場合はセッションから取得
			}
			String code = request.getParameter("code");
			out.write(QueryLogic.getUsersOfCode(oid, code));
		}
		else if(action.equals("query_users_lname")) {
			// ログインID検索
			String lname = request.getParameter("lname");
			out.write(QueryLogic.getUsersOfLname(session_oid, lname));
		}
		else if(action.equals("query_users_enable")) {
			// 管理状況検索
			String enable = request.getParameter("enable");
			out.write(QueryLogic.getUsersOfEnable(session_oid, enable));
		}
		else if(action.equals("query_users_mailenable")) {
			// メール打刻許可検索
			String mailenable = request.getParameter("mailenable");
			out.write(QueryLogic.getUsersOfMailenable(session_oid, mailenable));
		}
		else if(action.equals("query_users_uid")) {
			// ユーザID検索
			String uid = request.getParameter("uid");
			out.write(QueryLogic.getUsersOfUid(session_oid, uid));
		}
		else if(action.equals("query_users_today")) {
			// 本日出勤検索
			out.write(QueryLogic.getUsersOfToday(session_oid));
		}

		/*********************************
		 * タイムカード
		 *********************************/
		else if(action.equals("get_timecards")) {
			// 全検索(マイページからの場合は１ユーザ分)
			String uid = request.getParameter("uid");
			out.write(QueryLogic.getTimeCards(session_oid, uid));
		}
		else if(action.equals("query_timecard_user")) {
			// ユーザ検索
			String uid = request.getParameter("uid");
			out.write(QueryLogic.getTimeCards(session_oid, uid));
		}
		else if(action.equals("query_timecard_worktype")) {
			// 業務種別検索
			String uid = request.getParameter("uid");
			String wid = request.getParameter("wid");
			out.write(QueryLogic.getTimeCardOfWorktype(session_oid, uid, wid));
		}
		else if(action.equals("query_timecard_timerange")) {
			// 時間範囲検索
			String uid = request.getParameter("uid");
			String inH = request.getParameter("inH");
			String inM = request.getParameter("inM");
			String moveH = request.getParameter("moveH");
			String moveM = request.getParameter("moveM");
			String outH = request.getParameter("outH");
			String outM = request.getParameter("outM");
			out.write(QueryLogic.getTimeCardOfTimeRangeForGrid(session_oid, uid, inH, inM, moveH, moveM, outH, outM));
		}
		else if(action.equals("query_timecard_daterange")) {
			// 日付範囲検索
			String uid = request.getParameter("uid");
			String date1 = request.getParameter("date1");
			String date2 = request.getParameter("date2");
			out.write(QueryLogic.getTimeCardOfDateRangeForGrid(session_oid, uid, date1, date2));
		}
		else if(action.equals("query_timecard_today")) {
			// 本日出勤検索
			out.write(QueryLogic.getTimeCardOfNowDateForGrid(session_oid));
		}
		else if(action.equals("query_timecard_overlap")) {
			// 重複検索
			String uid = request.getParameter("uid");
			String tdate = request.getParameter("tdate");
			out.write(QueryLogic.getTimeCardOfUidAndTdateForGrid(session_oid, uid, tdate));
		}
		else if(action.equals("query_timecard_yearmonth")) {
			// 年・月検索
			String uid = request.getParameter("uid");
			String year = request.getParameter("year");
			String month = request.getParameter("month");
			out.write(QueryLogic.getTimeCardOfYearAndMonthForGrid(session_oid, uid, year, month));
		}
		else if(action.equals("query_timecard_nowmonth")) {
			// 今月検索(全ユーザ)
			out.write(QueryLogic.getTimeCardOfNowMonthForGrid(session_oid, null));
		}
		else if(action.equals("query_timecard_nowmonth_my")) {
			// 今月検索(1ユーザ)
			out.write(QueryLogic.getTimeCardOfNowMonthForGrid(session_oid, session_uid));
		}
		else if(action.equals("query_timecard_lastmonth")) {
			// 先月検索(全ユーザ)
			out.write(QueryLogic.getTimeCardOfLastMonthForGrid(session_oid, null));
		}
		else if(action.equals("query_timecard_lastmonth_my")) {
			// 先月検索(1ユーザ)
			out.write(QueryLogic.getTimeCardOfLastMonthForGrid(session_oid, session_uid));
		}
		else if(action.equals("query_timecard_nextmonth")) {
			// 来月検索(全ユーザ)
			out.write(QueryLogic.getTimeCardOfNextMonthForGrid(session_oid, null));
		}
		else if(action.equals("query_timecard_nextmonth_my")) {
			// 来月検索(1ユーザ)
			out.write(QueryLogic.getTimeCardOfNextMonthForGrid(session_oid, session_uid));
		}
		else if(action.equals("query_timecard_tdate")) {
			// ユーザ・日付検索
			String uid = request.getParameter("uid");
			String tdate = request.getParameter("tdate");
			out.write(QueryLogic.getTimeCardOfUidAndTdate(session_oid, uid, tdate));
		}

		/*********************************
		 * NFC
		 *********************************/
		else if(action.equals("get_nfcs")) {
			// uid検索
			String uid = request.getParameter("uid");
			out.write(QueryLogic.getNfcOfUid(session_oid, uid));
		}

		/*********************************
		 * 詳細ログ
		 *********************************/
		else if(action.equals("get_tclogs")) {
			// 一覧
			String uid = request.getParameter("uid");
			String tdate = request.getParameter("tdate");
			out.write(QueryLogic.getTclogOfUidAndTdate(session_oid, uid, tdate));
		}

		/*********************************
		 * 端末
		 *********************************/
		else if(action.equals("get_units")) {
			// 一覧
			out.write(QueryLogic.getUnits(session_oid));
		}
		else if(action.equals("query_unit_unitid")) {
			// 端末ID検索
			String unitid = request.getParameter("unitid");
			out.write(QueryLogic.getUnitOfUnitid(session_oid, unitid));
		}
		else if(action.equals("query_unit_enable")) {
			// 有効無効検索
			String enable = request.getParameter("enable");
			out.write(QueryLogic.getUnitOfEnable(session_oid, enable));
		}
		else if(action.equals("query_unit_daterange")) {
			// 日付範囲検索
			String date1 = request.getParameter("date1");
			String date2 = request.getParameter("date2");
			out.write(QueryLogic.getUnitOfDateRange(session_oid, date1, date2));
		}

		/*********************************
		 * 部署
		 *********************************/
		else if(action.equals("get_departments")) {
			// 一覧
			out.write(QueryLogic.getDepartments(session_oid));
		}
		else if(action.equals("query_department_id")) {
			// ID検索
			String id = request.getParameter("id");
			out.write(QueryLogic.getDepartmentOfId(session_oid, id));
		}
		else if(action.equals("query_department_enable")) {
			// 有効無効
			String enable = request.getParameter("enable");
			out.write(QueryLogic.getDepartmentOfEnable(session_oid, enable));
		}

		/*********************************
		 * カテゴリー
		 *********************************/
		else if(action.equals("get_categorys")) {
			// 一覧
			out.write(QueryLogic.getCategorys(session_oid));
		}
		else if(action.equals("query_category_id")) {
			// ID検索
			String id = request.getParameter("id");
			out.write(QueryLogic.getCategoryOfId(session_oid, id));
		}
		else if(action.equals("query_category_enable")) {
			// 有効無効
			String enable = request.getParameter("enable");
			out.write(QueryLogic.getCategoryOfEnable(session_oid, enable));
		}

		/*********************************
		 * 客先
		 *********************************/
		else if(action.equals("get_customers")) {
			// 一覧
			out.write(QueryLogic.getCustomers(session_oid));
		}
		else if(action.equals("query_customer_id")) {
			// ID検索
			String id = request.getParameter("id");
			out.write(QueryLogic.getCustomerOfId(session_oid, id));
		}
		else if(action.equals("query_customer_enable")) {
			// 有効無効
			String enable = request.getParameter("enable");
			out.write(QueryLogic.getCustomerOfEnable(session_oid, enable));
		}

		/*********************************
		 * 雇用形態
		 *********************************/
		else if(action.equals("get_employments")) {
			// 一覧
			out.write(QueryLogic.getEmployments(session_oid));
		}
		else if(action.equals("query_employment_id")) {
			// ID検索
			String id = request.getParameter("id");
			out.write(QueryLogic.getEmploymentOfId(session_oid, id));
		}
		else if(action.equals("query_employment_enable")) {
			// 有効無効
			String enable = request.getParameter("enable");
			out.write(QueryLogic.getEmploymentOfEnable(session_oid, enable));
		}

		/*********************************
		 * 勤務地
		 *********************************/
		else if(action.equals("get_worklocations")) {
			// 一覧
			out.write(QueryLogic.getWorklocations(session_oid));
		}
		else if(action.equals("query_worklocation_id")) {
			// ID検索
			String id = request.getParameter("id");
			out.write(QueryLogic.getWorklocationOfId(session_oid, id));
		}
		else if(action.equals("query_worklocation_enable")) {
			// 有効無効
			String enable = request.getParameter("enable");
			out.write(QueryLogic.getWorklocationOfEnable(session_oid, enable));
		}

		/*********************************
		 * グループ
		 *********************************/
		else if(action.equals("get_groups")) {
			// 一覧
			out.write(QueryLogic.getGroups(session_oid));
		}
		else if(action.equals("query_group_id")) {
			// ID検索
			String id = request.getParameter("id");
			out.write(QueryLogic.getGroupOfId(session_oid, id));
		}
		else if(action.equals("query_group_enable")) {
			// 有効無効
			String enable = request.getParameter("enable");
			out.write(QueryLogic.getGroupOfEnable(session_oid, enable));
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
