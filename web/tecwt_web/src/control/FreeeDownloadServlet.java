package control;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.sql.SQLException;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.FreeeDownload;
import util.Log;
import util.Util;

public class FreeeDownloadServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		String session_admin = (String)session.getAttribute("admin");
		if (!Util.execIsAdmin(session_admin)) {
			// adminじゃないのに来た場合は強制的にマイページへ飛ばす
			String orgUri = Util.getOrgString(request.getServletPath());
			response.sendRedirect("/" + Util.APP_NAME + "/" + orgUri + "/history?action=my");
			return;
		}

		String yearStr = request.getParameter("year");
		String monthStr = request.getParameter("month");
		if (Util.isNullOrBlank(yearStr) || Util.isNullOrBlank(monthStr)) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}

		String oid = (String)session.getAttribute("oid");

		// CSVデータ取得に時間がかかるので非同期で
		AsyncContext asyncContext = request.startAsync(request, response);
		// タイムアウト時間は長めに設定
		asyncContext.setTimeout(1000 * 60 * 10);
		asyncContext.start(new Runnable() {
			@Override
			public void run() {
				try (PrintWriter writer = asyncContext.getResponse().getWriter()) {
					HttpServletResponse response = (HttpServletResponse)asyncContext.getResponse();
					int year = Integer.parseInt(yearStr);
					int month = Integer.parseInt(monthStr);
					String filename = "月毎従業員別_時間_給与_" + year + "_" + month + ".csv";

					response.setContentType("text/csv;charset=utf-8");
					String encodedFilename = URLEncoder.encode(filename, "utf-8");
					response.setHeader("Content-Disposition", "attachment;filename=\"" + encodedFilename + "\"");
					FreeeDownload.outputWorkAndPayrollCSV(writer, oid, year, month);
				} catch (IOException | SQLException e) {
					Log.exceptionWrite(e);
				} finally {
					asyncContext.complete();
	            }
			}
		});
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
	}
}
