package control;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import db.DBPreparedStatementConnect;
import util.API;
import util.DateTime;
import util.Log;
import util.Util;

/**
 * Servlet implementation class WorktypeServlet
 */
public class ShiftCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public class Data {
		public String name;
		public int oid;
		public int uid;
		public int tdate;
		public int shift;
		public int intime;
	}
	public class Result {
		public Data[] list;
	}

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShiftCheckServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");

		HttpSession session = request.getSession(false);
		String session_admin = (String)session.getAttribute("admin");
		if (!Util.execIsAdmin(session_admin)) {
			// adminじゃないのに来た場合はエラー
			response.getWriter().write(Util.toErrorJSON("権限エラー"));
			return;
		}

		DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();

		String sql =
				"SELECT users.name, timecard.oid, timecard.uid, tdate, COALESCE(shift_time.intime, 900) AS shift, timecard.intime " +
				"FROM timecard " +
				"LEFT OUTER JOIN shift_time ON timecard.oid = shift_time.oid AND timecard.uid = shift_time.uid " +
				"JOIN users ON timecard.oid = users.oid AND timecard.uid = users.uid " +
				"WHERE tdate = ? " +
				"AND timecard.intime > 0 AND timecard.intime < COALESCE(shift_time.intime, 900) AND timecard.intime <= 1000 " +
				"ORDER BY tdate DESC, timecard.intime DESC, users.kana ASC;";
        try {
    		List<Data> list = new ArrayList<Data>();

    		ResultSet rs = db.execQuery(sql, Arrays.asList(DateTime.GetTodayInt()));
    		while(rs != null && rs.next()) {
    			Data data = new Data();
    			data.oid = rs.getInt("oid");
    			data.uid = rs.getInt("uid");
    			data.tdate = rs.getInt("tdate");
    			data.shift = rs.getInt("shift");
    			data.intime = rs.getInt("intime");
    			data.name = rs.getString("name");
                list.add(data);
            }

    		Result result = new Result();
    		result.list = list.toArray(new Data[list.size()]);
    		ObjectMapper mapper = new ObjectMapper();
			response.getWriter().write(mapper.writeValueAsString(result));
        } catch (SQLException | NumberFormatException | JsonProcessingException  e) {
            Log.exceptionWrite(e);
            response.getWriter().write(Util.toErrorJSON("予期せぬエラー"));
        } finally {
            db.close();
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");

		HttpSession session = request.getSession(false);
		String session_admin = (String)session.getAttribute("admin");
		if (!Util.execIsAdmin(session_admin)) {
			// adminじゃないのに来た場合はエラー
			response.getWriter().write(Util.toErrorJSON("権限エラー"));
			return;
		}

		try {
			BufferedReader buffer = new BufferedReader(request.getReader());
			String json = buffer.readLine();

			Map<String, String> map = API.jsonStringToMap(json);

			String update = "UPDATE timecard SET intime = ? "
					+ "WHERE oid = ? AND uid = ? AND tdate = ? AND times = 1;";

			DBPreparedStatementConnect db = new DBPreparedStatementConnect();
			db.open();

			boolean success = db.execUpdate(update,
					Arrays.asList(Integer.parseInt(map.get("intime")), Integer.parseInt(map.get("oid")),
							Integer.parseInt(map.get("uid")), Integer.parseInt(map.get("tdate"))));
			db.close();
			String jsonString = success ? Util.toSuccessJSON("") : Util.toErrorJSON("更新（保存）エラー");
			response.getWriter().write(jsonString);
		} catch(Exception e) {
			Log.exceptionWrite(e);
			response.getWriter().write(Util.toErrorJSON("予期せぬエラー"));
		}
	}

}
