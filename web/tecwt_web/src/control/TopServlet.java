package control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.QueryLogic;
import util.Util;

/**
 * Servlet implementation class TopServlet
 */
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public TopServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGetPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGetPost(request, response);
	}

	private void doGetPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// セッション取得
		HttpSession session = request.getSession(false);
		// サイト閲覧権限取得
		String session_admin = (String)session.getAttribute("admin");
		// フォワード
		if(Util.execIsAdmin(session_admin)) {
			String json = QueryLogic.getTimeCardOfNowDateForGrid((String)session.getAttribute("oid"));
			request.setAttribute("workin", json);
			request.getRequestDispatcher("../WEB-INF/top.jsp").forward(request, response);
		} else {
			// adminじゃないのに来た場合は強制的にマイページへ飛ばす
			String orgUri = Util.getOrgString(request.getServletPath());
			response.sendRedirect("/" + Util.APP_NAME + "/" + orgUri + "/history?action=my");
		}
	}

}
