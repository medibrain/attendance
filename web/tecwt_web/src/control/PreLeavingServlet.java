package control;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.PreLeavingRequest;
import model.PreLeavingRequest.WorklistResult;
import organization.Initialize;
import organization.Organization;
import util.ErrorMessage;
import util.Log;
import util.Util;


public class PreLeavingServlet extends HttpServlet {
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");

		try {
			BufferedReader buffer = new BufferedReader(request.getReader());
			String json = buffer.readLine();

			ObjectMapper mapper = new ObjectMapper();
	        PreLeavingRequest req = mapper.readValue(json, PreLeavingRequest.class);
			// 毎回ログインさせる
			ErrorMessage error = req.login();
			if (error != null) {
				response.getWriter().write(Util.toErrorJSON(error.getMessage()));
				return;
			}

			// セッションを作成
			String orgUri = Util.getOrgString(request.getServletPath());
			Organization org = Initialize.getOrganization(orgUri);
			HttpSession session = request.getSession(true);
			session.setAttribute("oid", req.oid);
			session.setAttribute("unitid", req.unitid);
			session.setAttribute("admin", "false");
			session.setAttribute("organization", orgUri);
			session.setAttribute("orrgname", org.name);
			session.setAttribute("session", "true");
			// タイムアウト時間
			session.setMaxInactiveInterval(604800);	// 一週間

			// ログ記録
			req.saveLog("CreateSessionID:" + session.getId());

			// 退勤打刻時はデフォルトで当日の日付
			WorklistResult result = req.loadWorkList();
			if (result.data == null) {
				response.getWriter().write(Util.toErrorJSON(result.error.getMessage()));
				return;
			}

			String jsonString = mapper.writeValueAsString(result.data);
			response.getWriter().write(jsonString);
		} catch(JsonParseException e) {
			Log.exceptionWrite(e);
			response.getWriter().write(Util.toErrorJSON("パラメータ不正"));
		} catch(Exception e) {
			Log.exceptionWrite(e);
			response.getWriter().write(Util.toErrorJSON("予期せぬエラー"));
		}
	}

}
