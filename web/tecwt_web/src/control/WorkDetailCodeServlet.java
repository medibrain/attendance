package control;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.WorkDetailCode;
import util.Log;
import util.Util;


public class WorkDetailCodeServlet extends HttpServlet {
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		Integer oid = getOid(request);
		if (oid == null) {
			response.sendError(HttpServletResponse.	SC_BAD_REQUEST);
			return;
		}

		Integer department = Util.toNullableInteger(request.getParameter("department"));
		if (department == null) {
			response.sendError(HttpServletResponse.	SC_BAD_REQUEST);
			return;
		}

		WorkDetailCode result = WorkDetailCode.makeFromDB(oid, department);

		ObjectMapper mapper = new ObjectMapper();

		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		try {
			String json = mapper.writeValueAsString(result);
			response.getWriter().write(json);
		} catch (JsonProcessingException e) {
			Log.exceptionWrite(e);
			response.getWriter().write(Util.toErrorJSON("予期せぬエラー"));
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");

		Integer oid = getOid(request);
		if (oid == null) {
			response.sendError(HttpServletResponse.	SC_BAD_REQUEST);
			return;
		}

		try {
			BufferedReader buffer = new BufferedReader(request.getReader());
			String json = buffer.readLine();

			ObjectMapper mapper = new ObjectMapper();
			WorkDetailCode data = mapper.readValue(json, WorkDetailCode.class);

			boolean result = data.updateDB(oid);

			String jsonString = result ? Util.toSuccessJSON("OK") : Util.toErrorJSON("更新エラー");
			response.getWriter().write(jsonString);
		} catch(JsonParseException e) {
			Log.exceptionWrite(e);
			response.getWriter().write(Util.toErrorJSON("パラメータ不正"));
		} catch(Exception e) {
			Log.exceptionWrite(e);
			response.getWriter().write(Util.toErrorJSON("予期せぬエラー"));
		}
	}

	private Integer getOid(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		return (session != null) ? Util.toNullableInteger((String)session.getAttribute("oid")) : null;
	}
}
