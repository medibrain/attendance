package control;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.WorklistEditRequest;
import model.WorklistEditResponse;
import model.WorklistEditResponse.WorkDetailList;
import util.Log;
import util.Util;

public class WorklistEditServlet extends HttpServlet {
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");

		HttpSession session = request.getSession(false);
		if (session == null) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		String oid = (String)session.getAttribute("oid");

		String jsonString = getWorkDetailList(oid);
		response.getWriter().write(jsonString);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");

		try {
			BufferedReader buffer = new BufferedReader(request.getReader());
			String json = buffer.readLine();

			ObjectMapper mapper = new ObjectMapper();
			WorklistEditRequest work = mapper.readValue(json, WorklistEditRequest.class);

			boolean result = work.updateDB();

			if (!result) {
				response.getWriter().write(Util.toErrorJSON("予期せぬエラー"));
				return;
			}
			response.getWriter().write(Util.toSuccessJSON("OK"));
		} catch(JsonParseException e) {
			Log.exceptionWrite(e);
			response.getWriter().write(Util.toErrorJSON("パラメータ不正"));
		} catch(Exception e) {
			Log.exceptionWrite(e);
			response.getWriter().write(Util.toErrorJSON("予期せぬエラー"));
		}
	}

	private String getWorkDetailList(String oidString) {
		Integer oid = Util.toNullableInteger(oidString);

		if (oid == null) {
			return Util.toErrorJSON("パラメータ不正");
		}
		WorkDetailList result = WorklistEditResponse.makeDetailListFromDB(oid);

        if (result == null) {
            return Util.toErrorJSON("パラメータ不正");
        }

		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(result);
		} catch (JsonProcessingException e) {
			Log.exceptionWrite(e);
            return Util.toErrorJSON("予期せぬエラー");
		}
	}
}
