package control;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.QueryLogic;
import organization.Initialize;
import organization.Organization;
import util.Util;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// セッション確認
		HttpSession session = request.getSession(false);
		if (session != null) {
			String s = (String)session.getAttribute("session");
			if (s != null && s.equals("true")) {
				// あれば進む
				String admin = (String)session.getAttribute("admin");
				if (admin.equals("true")) {
					// 管理者はトップ画面
					request.getRequestDispatcher("top").forward(request, response);
				} else {
					// 一般はマイページ
					request.getRequestDispatcher("history?action=my").forward(request, response);
				}
				return;
			} else {
				// なければ初期化
				session.invalidate();
			}
		}

		// ポスト用のorgzanition文字列をJSPに渡す
		String orgUri =  Util.getOrgString(request.getServletPath());
		request.setAttribute("organization", orgUri);
		request.setAttribute("orgname", Initialize.getOrganization(orgUri).name);

		// ログインページへ
		// 「../」で組織名部分(organization)部分を無いことにする
		request.getRequestDispatcher("../WEB-INF/login.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// organizationの取得
		String orgUri = Util.getOrgString(request.getServletPath());
		Organization org = Initialize.getOrganization(orgUri);

		// パラメータ取得
		String lname = request.getParameter("lname");
		String pass = request.getParameter("pass");
		String save = request.getParameter("save");

		// 返却はJSON
		response.setContentType("application/json;charset=UTF-8");

		// JSON貼り付け用
		PrintWriter out = response.getWriter();

		// ログイン
		String json = QueryLogic.execLogin(org.oid, lname, pass);
		if(!Util.isError(json)) {
			QueryLogic.LoginResult result = QueryLogic.isLogin(json);
			if(result.isLogin) {
				// セッションを作成
				HttpSession session = request.getSession(true);
				session.setAttribute("oid", result.oid);
				session.setAttribute("uid", result.uid);
				session.setAttribute("admin", result.admin);
				session.setAttribute("organization", orgUri);
				session.setAttribute("orrgname", org.name);
				session.setAttribute("session", "true");

				// タイムアウト時間
				if(save.equals("true")) {
					session.setMaxInactiveInterval(604800);	// 一週間
				} else {
					session.setMaxInactiveInterval(14400);	// 4時間（14400秒）
				}
			}
		}
		out.write(json);
	}

}
