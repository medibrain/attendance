package control;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import organization.Initialize;
import util.Util;

/**
 * Servlet Filter implementation class Filter
 */
public class Filter implements javax.servlet.Filter {

    /**
     * Default constructor.
     */
    public Filter() {}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		// 文字コードを設定
		String encoding = "UTF-8";
		request.setCharacterEncoding(encoding);
		response.setCharacterEncoding(encoding);

		// Http化
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;

		// orgUri,servUriの取得 (http://example.com/example/orgUri/servUri)
		String orgUri = Util.getOrgString(req.getServletPath());
		String servUri = Util.getServString(req.getServletPath());

		// サーブレット判定
		if(servUri == null || servUri.equals("login")) {
			// ログイン
		} else if(servUri.equals("logout")) {
			// ログアウト
		} else if(servUri.equals("info")) {
			// info
		} else if(servUri.equals("query")) {
			// 検索
			if(isSessionDisabled(req)) {
				returnSessionDisabled(res);	// リダイレクトではなくJSONで返す
				return;
			}
			if(isNoMatchOrg(req, orgUri)) {
				returnOrganizationNoMatch(res);
				return;
			}
		} else if(servUri.equals("update")) {
			// アップデート
			if(isSessionDisabled(req)) {
				returnSessionDisabled(res);	// リダイレクトではなくJSONで返す
				return;
			}
			if(isNoMatchOrg(req, orgUri)) {
				returnOrganizationNoMatch(res);
				return;
			}
		} else if(servUri.equals("preleaving") || servUri.equals("leaving")) {
			// アプリのAPI用なのでここでのセッションチェックなし
		} else if(servUri.equals("workdetailcode") || servUri.equals("worklistedit")) {
			// 作業内容コードのAPI
			if(isSessionDisabled(req)) {
				returnSessionDisabled(res);	// リダイレクトではなくJSONで返す
				return;
			}
			if(isNoMatchOrg(req, orgUri)) {
				returnOrganizationNoMatch(res);
				return;
			}
		} else {
			// その他
			if(isSessionDisabled(req)) {
				// タイムアウト画面へリダイレクト
				res.sendRedirect("/"+Util.APP_NAME+"/"+orgUri+"/info/timeout");
				return;
			}
			if(isNoMatchOrg(req, orgUri)) {
				res.sendRedirect("/"+Util.APP_NAME+"/"+orgUri+"/info/error?message=type:org");
				return;
			}
		}

		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

	private boolean isSessionDisabled(HttpServletRequest req) {
		HttpSession session = req.getSession(false);
		if(session == null || session.getAttribute("oid") == null) {
			return true;
		}
		return false;
	}

	/**
	 * 指定した組織が登録されており、かつセッション情報と合致しているか確認
	 * @param req
	 * @param orgUri
	 * @return
	 */
	private boolean isNoMatchOrg(HttpServletRequest req, String orgUri) {
		// 登録情報との照合
		if(Initialize.getOrganization(orgUri) == null) {
			return true;
		}
		// セッション情報との照合
		HttpSession session = req.getSession(false);
		String sOrgUri = (String)session.getAttribute("organization");
		if(sOrgUri == null || !sOrgUri.equals(orgUri)) {
			return true;
		}
		return false;
	}

	/**
	 * セッション切れのJSONをレスポンスとして返します。
	 * @param res
	 * @throws IOException
	 */
	private void returnSessionDisabled(HttpServletResponse res) throws IOException {
		// 返却はJSON
		res.setContentType("application/json;charset=UTF-8");

		// キャッシュ無効(IEはここでも記述が必要)
		res.setHeader("Pragma", "no-cache");
		res.setHeader("Cache-Control", "no-cache");
		res.setDateHeader("Expires", 0);

		// JSON作成
		String json = "{\"result\":\"session_timeout\"}";

		// JSON返却
		PrintWriter out = res.getWriter();
		out.write(json);
	}

	private void returnOrganizationNoMatch(HttpServletResponse res) throws IOException {
		// 返却はJSON
		res.setContentType("application/json;charset=UTF-8");

		// キャッシュ無効(IEはここでも記述が必要)
		res.setHeader("Pragma", "no-cache");
		res.setHeader("Cache-Control", "no-cache");
		res.setDateHeader("Expires", 0);

		// JSON作成
		String json =
				"{"
				+ "\"result\":\"error\", "
				+ "\"message\":\"type:org\""
				+ "}";

		// JSON返却
		PrintWriter out = res.getWriter();
		out.write(json);
	}

}
