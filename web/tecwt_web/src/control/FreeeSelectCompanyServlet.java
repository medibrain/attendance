package control;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.FreeeAPI;
import model.FreeeAuthAPI;
import util.API;
import util.Log;
import util.Util;

// Freee連携時の事業所選択画面用
public class FreeeSelectCompanyServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");

		HttpSession session = request.getSession(false);
		String session_admin = (String)session.getAttribute("admin");
		if (!Util.execIsAdmin(session_admin)) {
			// adminじゃないのに来た場合はエラー
			response.getWriter().write(Util.toErrorJSON("権限エラー"));
			return;
		}

		FreeeAPI.MeResponse result = FreeeAPI.getCompany();
		ObjectMapper mapper = new ObjectMapper();
		try {
			response.getWriter().write(mapper.writeValueAsString(result));
		} catch (JsonProcessingException e) {
			Log.exceptionWrite(e);
			response.getWriter().write(Util.toErrorJSON("予期せぬエラー"));
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");

		HttpSession session = request.getSession(false);
		String session_admin = (String)session.getAttribute("admin");
		if (!Util.execIsAdmin(session_admin)) {
			// adminじゃないのに来た場合はエラー
			response.getWriter().write(Util.toErrorJSON("権限エラー"));
			return;
		}

		try {
			BufferedReader buffer = new BufferedReader(request.getReader());
			String json = buffer.readLine();

			Map<String, String> map = API.jsonStringToMap(json);
			int id = Integer.parseInt(map.get("id"));
			boolean result = FreeeAuthAPI.setCompany(id);
			String jsonString = result ? Util.toSuccessJSON("設定完了") : Util.toErrorJSON("設定エラー");
			response.getWriter().write(jsonString);
		} catch(Exception e) {
			Log.exceptionWrite(e);
			response.getWriter().write(Util.toErrorJSON("予期せぬエラー"));
		}
	}
}
