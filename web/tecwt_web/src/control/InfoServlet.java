package control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.Util;

/**
 * Servlet implementation class BasicServlet
 */
public class InfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public InfoServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGetPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGetPost(request, response);
	}

	private void doGetPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// orgUri,servUriの取得 http://example.com/example/[orgUri]/info/[servUri]
		String servPath = request.getServletPath();	// 上記の /[orgUri]/info まで取得
		String pathInfo = request.getPathInfo();	// 上記の /[servUri] を取得
		String path = servPath + pathInfo;			// /[orgUri]/info/[servUri] に成形
		String orgUri = Util.getOrgString(path);
		String servUri = Util.getServEtcString(path);
		if(servUri == null) {
			return;
		}
		// 分岐
		if(servUri.equals("timeout")) {
			request.setAttribute("organization", orgUri);
			request.getRequestDispatcher("../../WEB-INF/timeout.jsp").forward(request, response);	// パスが一つ分多いので二階層上がる
			return;
		} else if(servUri.equals("error")) {
			String message = request.getParameter("error_message");
			request.setAttribute("message", message);
			request.setAttribute("organization", orgUri);
			request.getRequestDispatcher("../../WEB-INF/error.jsp").forward(request, response);		// パスが一つ分多いので二階層上がる
			return;
		} else {
			return;
		}
	}

}
