package control;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.FreeeAuthAPI;
import model.FreeeAuthAPI.ClientAuthorize;
import model.FreeeAuthAPI.LinkInfo;
import util.API;
import util.Log;
import util.Util;

// OAuth認証用
public class FreeeTokenServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// コールバックURLとトークン強制更新の兼用URL
		String code = request.getParameter("code");
		String refresh = request.getParameter("refresh");

		HttpSession session = request.getSession(false);
		String session_admin = (String)session.getAttribute("admin");
		if (!Util.execIsAdmin(session_admin)) {
			// adminじゃないのに来た場合はエラー
			response.getWriter().write(Util.toErrorJSON("権限エラー"));
			return;
		}

		if (Util.isNullOrBlank(code) || session == null) {
			if (refresh != null) {
				response.setContentType("application/json; charset=UTF-8");
				response.setHeader("Cache-Control", "no-cache");

				// refreshの指定があればトークン更新実行
				if (FreeeAuthAPI.refreshTokenAndSave() == null) {
					response.getWriter().write((Util.toErrorJSON("リフレッシュ失敗！！（再連携をしてください）")));
				} else {
					response.getWriter().write((Util.toSuccessJSON("リフレッシュOK")));
				}
				return;
			}
			response.sendRedirect("top");
		} else {
			// コードの指定あり＝Freee側からコールバックURLを呼び出された時
			// 初回の連携処理（続き）
			LinkInfo link = FreeeAuthAPI.getLinkInfo();
			if (link == null || link.client == null) {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);
				return;
			}

			boolean result = FreeeAuthAPI.retrieveTokenAndSave(link.client, code);
			if (result) {
				// トークン取得処理が成功すれば事業所選択へ
				response.sendRedirect("freeeselectcompany");
			} else {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		}
	}

	// Freee側の連携情報の設定・更新用
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");

		HttpSession session = request.getSession(false);
		String session_admin = (String)session.getAttribute("admin");
		if (!Util.execIsAdmin(session_admin)) {
			// adminじゃないのに来た場合はエラー
			response.getWriter().write(Util.toErrorJSON("権限エラー"));
			return;
		}

		try {
			BufferedReader buffer = new BufferedReader(request.getReader());
			String json = buffer.readLine();

			ClientAuthorize data = new ClientAuthorize();
			Map<String, String> map = API.jsonStringToMap(json);
			if (map.get("relink") != null) {
				// 再連携の指定がある場合はサーバ内の連携情報を使う
				LinkInfo link = FreeeAuthAPI.getActiveLinkInfo();
				if (link == null || link.client == null) {
					response.getWriter().write(Util.toErrorJSON("連携情報なし。「連携開始」をしてください"));
					return;
				}
				data = link.client;
			} else {
				// クライアントから連携情報が入力されてきた場合（初回）
				data.id = map.get("id");
				data.secret = map.get("secret");
				data.url = map.get("url");
			}

			// 連携処理開始（既存のデータは削除される）
			String url = FreeeAuthAPI.generateAuthorizeURL(data);
			String jsonString;
			if (Util.isNullOrBlank(url)) {
				jsonString = Util.toErrorJSON("更新エラー");
			} else {
				// Freee側の認証画面のURLを返す
				boolean result = FreeeAuthAPI.clearAndStartAuthorize(data);
				jsonString = result ? Util.toSuccessJSON(url) : Util.toErrorJSON("更新（保存）エラー");
			}
			response.getWriter().write(jsonString);
		} catch(Exception e) {
			Log.exceptionWrite(e);
			response.getWriter().write(Util.toErrorJSON("予期せぬエラー"));
		}
	}

}
