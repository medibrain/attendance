package control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import util.Util;

/**
 * Servlet implementation class HistoryServlet
 */
public class HistoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public HistoryServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGo(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGo(request, response);
	}

	/**
	 * 1.セッション確認<br>
	 * 2.セッションスコープ操作<br>
	 * 3.フォワード分岐<br>
	 * @throws ServletException
	 * @throws IOException
	 */
	private void doGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// セッション取得
		HttpSession session = request.getSession(false);

		// セッションから権限を取得してレスポンスに記載
		String session_admin = (String)session.getAttribute("admin");
		request.setAttribute("admin", session_admin);

		// セッションからuidを取得
		String session_uid = (String)session.getAttribute("uid");

		// 分岐
		String action = request.getParameter("action");
		if(action.equals("my")) {
			request.setAttribute("uid", session_uid);
			request.getRequestDispatcher("../WEB-INF/my-history.jsp").forward(request, response);
		} else if(action.equals("business")) {
			if(Util.execIsAdmin(session_admin)) {
				request.getRequestDispatcher("../WEB-INF/business-history.jsp").forward(request, response);
			} else {
				// adminじゃないのに来た場合は強制的にマイページへ飛ばす(URL表示の都合上リダイレクト)
				String orgUri = Util.getOrgString(request.getServletPath());
				response.sendRedirect("/" + Util.APP_NAME + "/" + orgUri + "/history?action=my");
			}
		}
	}

}
