package control;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.WorklistEditResponse;
import model.WorklistRequest;
import model.WorklistRequest.Result;
import util.Log;
import util.Util;

public class WorklistServlet extends HttpServlet {
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");

		HttpSession session = request.getSession(false);
		if (session == null) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		String oid = (String)session.getAttribute("oid");
		String uid = request.getParameter("uid");

		boolean isAdmin = Util.execIsAdmin((String)session.getAttribute("admin"));
		if (!isAdmin && !uid.equals((String)session.getAttribute("uid"))) {
			// 管理者以外は自分のみ
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
			return;
		}

		String jsonString = getWorklist(request.getParameter("tdate"), oid, uid, isAdmin);
		response.getWriter().write(jsonString);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		response.setHeader("Cache-Control", "no-cache");

		try {
			BufferedReader buffer = new BufferedReader(request.getReader());
			String json = buffer.readLine();

			ObjectMapper mapper = new ObjectMapper();
	        WorklistRequest work = mapper.readValue(json, WorklistRequest.class);

			Result result = work.execLeavingWorkByNFC();

			if (result.error != null) {
				response.getWriter().write(Util.toErrorJSON(result.error.getMessage()));
				return;
			}

			String jsonString = mapper.writeValueAsString(result.data);
			response.getWriter().write(jsonString);
		} catch(JsonParseException e) {
			Log.exceptionWrite(e);
			response.getWriter().write(Util.toErrorJSON("パラメータ不正"));
		} catch(Exception e) {
			Log.exceptionWrite(e);
			response.getWriter().write(Util.toErrorJSON("予期せぬエラー"));
		}
	}

	private String getWorklist(String dateString, String oidString, String uidString, boolean isAdmin) {
		Integer date = Util.toNullableInteger(dateString);
		if (date == null) {
			return Util.toErrorJSON("パラメータ不正");
		}

		Integer oid = Util.toNullableInteger(oidString);
		Integer uid = Util.toNullableInteger(uidString);

		if (oid == null && uid == null) {
			return Util.toErrorJSON("パラメータ不正");
		}
		WorklistEditResponse result = WorklistEditResponse.makeFromDB(date, oid, uid, isAdmin);

        if (result == null) {
            return Util.toErrorJSON("パラメータ不正");    // 該当ユーザなし
        } else if (result.intime < 0) {
			return Util.toErrorJSON("出勤がされていない為、登録できません");
        }

		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(result);
		} catch (JsonProcessingException e) {
			Log.exceptionWrite(e);
            return Util.toErrorJSON("予期せぬエラー");
		}
	}
}
