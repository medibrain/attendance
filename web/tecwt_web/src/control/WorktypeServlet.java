package control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class WorktypeServlet
 */
public class WorktypeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public WorktypeServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGo(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGo(request, response);
	}

	/**
	 * 1.セッション確認<br>
	 * 2.セッションスコープ操作<br>
	 * 3.フォワード<br>
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	private void doGo(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// セッション取得
		HttpSession session = request.getSession(false);

		// セッションから権限を取得してレスポンスに記載
		String session_admin = (String)session.getAttribute("admin");
		request.setAttribute("admin", session_admin);

		// フォワード
		request.getRequestDispatcher("../WEB-INF/worktype.jsp").forward(request, response);
	}

}
