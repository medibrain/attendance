package control;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import organization.Initialize;
import organization.Organization;
import util.Log;
import util.Util;

/**
 * Servlet implementation class DownloadServlet
 */
public class DownloadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DownloadServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGetPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGetPost(request, response);
	}

	private void doGetPost(HttpServletRequest request, HttpServletResponse response) {
		String servPath = request.getServletPath();
		String pathInfo = request.getPathInfo();
		String path = servPath + pathInfo;
		String etcUri = Util.getServEtcString(path);
		try {
			// ダウンロードする対象の分岐
			switch(etcUri) {
			case "csv":
				// CSV
				doCSVDownload(request, response);
				break;
			case "android":
				// APK
				doAPKDownload(response);
				break;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			Log.Write(ex.toString());
		}
	}

	public void doCSVDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String ENCODE = "UTF-8";
		String target = request.getParameter("target");
		String dlfalse = request.getParameter("dl");
		String filename = request.getParameter("filename");
		// ダウンロードする中身
		String csv;
		if(target.equals("timecard")) {
			// timecard
			csv = doTimecardCSVDownload(request, response);
		} else if(target.equals("timecard_incomestatement")) {
			// timecard_incomestatement
			csv = doIncomeStatementCSVDownload(request, response);
		} else if(target.equals("users")) {
			// users
			csv = doUsersCSVDownload(request, response);
		} else if(target.equals("worktype")) {
			// worktype
			csv = doWorktypeCSVDownload(request, response);
		} else if(target.equals("unit")) {
			// unit
			csv = doUnitCSVDownload(request, response);
		} else if (target.equals("department")) {
			// department
			csv = doDepartmentCSVDownload(request, response);
		} else if (target.equals("employment")) {
			// employment
			csv = doEmploymentCSVDownload(request, response);
		} else if (target.equals("worklocation")) {
			// worklocation
			csv = doWorklocationCSVDownload(request, response);
		} else if (target.equals("group")) {
			// group
			csv = doGroupCSVDownload(request, response);
		} else if (target.equals("customer")) {
			// customer
			csv = doCustomerCSVDownload(request, response);
		} else if (target.equals("category")) {
			// category
			csv = doCategoryCSVDownload(request, response);
		} else if (target.equals("worklist")) {
			// 作業内容
			csv = doWorkListCSVDownload(request, response);
		} else {
			return;
		}
		// レスポンス
		if(dlfalse != null && dlfalse.equals("false")) {
			response.setContentType("text/html; charset=UTF-8");
			csv = csv.replaceAll("\n", "<br>");	// HTMLに表示するので改行コードを変更
			//HTML化
			String html = "<html><body>"+csv+"</body></html>";
			PrintWriter pw = response.getWriter();
			pw.write(html);
			pw.flush();
		} else {
			String utfFileName = URLEncoder.encode(filename, ENCODE);//ファイル名はこっちじゃないとダメ
			String utfCSV = new String(csv.getBytes(ENCODE), ENCODE);
			//response.setContentType("application/force-download; charset="+ENCODE);
			response.setCharacterEncoding(ENCODE);
			response.setContentType("application/force-download");
			response.setHeader("Content-Disposition", "attachment; filename=\""+utfFileName+".csv\"");
			byte[] bom = new byte[]{(byte)0xef, (byte)0xbb, (byte)0xbf};	// Excel対応のため先頭にBOMを追記
			ServletOutputStream outs = response.getOutputStream();
			outs.write(bom);
			outs.write(utfCSV.getBytes(ENCODE));
			outs.flush();
		}
	}

	private String doTimecardCSVDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// パラメータの取得
		String orgUri = Util.getOrgString(request.getServletPath());
		String showDataJSON = request.getParameter("showDatas");
		String yearStr = request.getParameter("year");
		String monthStr = request.getParameter("month");
		String uidStr = request.getParameter("myuid");
		String unfinishedableStr = request.getParameter("unfinishable");
		int year = Util.toInt(yearStr);
		int month = Util.toInt(monthStr);
		int uid = Util.toInt(uidStr);
		boolean unfinishedable = Boolean.valueOf(unfinishedableStr);
		// 該当する組織インスタンスの取得
		Organization org = Initialize.getOrganization(orgUri);
		// CSV
		return org.getTimecardCSV(showDataJSON, year, month, uid, unfinishedable);
	}

	private String doWorkListCSVDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// パラメータの取得
		String orgUri = Util.getOrgString(request.getServletPath());
		String yearStr = request.getParameter("year");
		String monthStr = request.getParameter("month");
		int year = Util.toInt(yearStr);
		int month = Util.toInt(monthStr);

		Organization org = Initialize.getOrganization(orgUri);
		// CSV
		return org.getWorkListCSV(year, month);
	}

	private String doIncomeStatementCSVDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// パラメータの取得
		String orgUri = Util.getOrgString(request.getServletPath());
		String showDataJSON = request.getParameter("showDatas");
		String yearStr = request.getParameter("year");
		String monthStr = request.getParameter("month");
		String uidStr = request.getParameter("myuid");
		String unfinishedableStr = request.getParameter("unfinishable");
		int year = Util.toInt(yearStr);
		int month = Util.toInt(monthStr);
		int uid = Util.toInt(uidStr);
		boolean unfinishedable = Boolean.valueOf(unfinishedableStr);
		// 該当する組織インスタンスの取得
		Organization org = Initialize.getOrganization(orgUri);
		// CSV
		return org.getIncomeStatementCSV(showDataJSON, year, month, uid, unfinishedable);
	}

	private String doUsersCSVDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// パラメータの取得
		String orgUri = Util.getOrgString(request.getServletPath());
		String showDataJSON = request.getParameter("showDatas");
		// 該当する組織インスタンスの取得
		Organization org = Initialize.getOrganization(orgUri);
		// CSV
		return org.getUsersCSV(showDataJSON);
	}

	private String doWorktypeCSVDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// パラメータの取得
		String orgUri = Util.getOrgString(request.getServletPath());
		String showDataJSON = request.getParameter("showDatas");
		// 該当する組織インスタンスの取得
		Organization org = Initialize.getOrganization(orgUri);
		// CSV
		return org.getWorktypeCSV(showDataJSON);
	}

	private String doUnitCSVDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// パラメータの取得
		String orgUri = Util.getOrgString(request.getServletPath());
		String showDataJSON = request.getParameter("showDatas");
		// 該当する組織インスタンスの取得
		Organization org = Initialize.getOrganization(orgUri);
		// CSV
		return org.getUnitCSV(showDataJSON);
	}

	private String doDepartmentCSVDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// パラメータの取得
		String orgUri = Util.getOrgString(request.getServletPath());
		String showDataJSON = request.getParameter("showDatas");
		// 該当する組織インスタンスの取得
		Organization org = Initialize.getOrganization(orgUri);
		// CSV
		return org.getDepartmentCSV(showDataJSON);
	}

	private String doEmploymentCSVDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// パラメータの取得
		String orgUri = Util.getOrgString(request.getServletPath());
		String showDataJSON = request.getParameter("showDatas");
		// 該当する組織インスタンスの取得
		Organization org = Initialize.getOrganization(orgUri);
		// CSV
		return org.getEmploymentCSV(showDataJSON);
	}

	private String doWorklocationCSVDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// パラメータの取得
		String orgUri = Util.getOrgString(request.getServletPath());
		String showDataJSON = request.getParameter("showDatas");
		// 該当する組織インスタンスの取得
		Organization org = Initialize.getOrganization(orgUri);
		// CSV
		return org.getWorklocationCSV(showDataJSON);
	}

	private String doGroupCSVDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// パラメータの取得
		String orgUri = Util.getOrgString(request.getServletPath());
		String showDataJSON = request.getParameter("showDatas");
		// 該当する組織インスタンスの取得
		Organization org = Initialize.getOrganization(orgUri);
		// CSV
		return org.getGroupCSV(showDataJSON);
	}

	private String doCustomerCSVDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// パラメータの取得
		String orgUri = Util.getOrgString(request.getServletPath());
		String showDataJSON = request.getParameter("showDatas");
		// 該当する組織インスタンスの取得
		Organization org = Initialize.getOrganization(orgUri);
		// CSV
		return org.getCustomerCSV(showDataJSON);
	}

	private String doCategoryCSVDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// パラメータの取得
		String orgUri = Util.getOrgString(request.getServletPath());
		String showDataJSON = request.getParameter("showDatas");
		// 該当する組織インスタンスの取得
		Organization org = Initialize.getOrganization(orgUri);
		// CSV
		return org.getCategoryCSV(showDataJSON);
	}

	public void doAPKDownload(HttpServletResponse response) throws IOException {
		// ダウンロード処理
		String path = this.getServletContext().getRealPath("/") + "/WEB-INF/files/jp.co.tecwt.medi_techno_service.apk";
		File file = new File(path);
		BufferedInputStream in = null;
		BufferedOutputStream out = null;
		try {
			//HTTPヘッダの出力
			response.setContentType("application/octet-stream");
			response.setHeader("Content-disposition", "attachment; filename=\"" + file.getName() + "\"");
			response.setContentLength((int)file.length());
			in = new BufferedInputStream(new FileInputStream(file));
			out = new BufferedOutputStream(response.getOutputStream());
			byte buf[]=new byte[1024];
			int len;
			while((len=in.read(buf))!=-1){
				out.write(buf,0,len);
			}
		} catch (Exception e) {
			//ファイルダウンロード用のHTTPヘッダをリセットします。
			response.reset();
			response.sendError(HttpURLConnection.HTTP_INTERNAL_ERROR , e.toString());
		} finally {
			if (in != null) {
			in.close();
			}
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}

}
