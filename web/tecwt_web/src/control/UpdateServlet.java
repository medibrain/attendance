package control;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.UpdateLogic;

/**
 * Servlet implementation class UpdateServlet
 */
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// 返却はJSON
		response.setContentType("application/json;charset=UTF-8");

		// キャッシュ無効(IEはここでも記述が必要)
		response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);

        PrintWriter out = response.getWriter();

        // セッション確認済みのリクエストからoidとuid(操作主)を取得
        HttpSession session = request.getSession(false);
        String session_oid = (String)session.getAttribute("oid");
        String session_uid = (String)session.getAttribute("uid");

        // 値の取得
		HashMap<String, String> params = new HashMap<String, String>();
		Enumeration<String> names = request.getParameterNames();
		String name;
		while (names.hasMoreElements()){
			name = names.nextElement();
			params.put(name, request.getParameter(name));
		}

		// action分岐
		String action = request.getParameter("action");
		if(action == null) {
			return;

		/*******************
		 *  タイムカード
		 *******************/
		} else if(action.equals("update_timecard")) {
			// 更新
			out.write(UpdateLogic.execTimeCardUpdate2(params));
		} else if(action.equals("delete_timecard")) {
			// 削除
			out.write(UpdateLogic.execTimeCardDelete(params));
		} else if(action.equals("insert_timecard")) {
			// 追加
			out.write(UpdateLogic.execTimeCardInsert(session_oid, params));

		/*******************
		 *  ユーザ
		 *******************/
		} else if(action.equals("update_users")) {
			// 更新
			out.write(UpdateLogic.execUsersUpdate(params, session_uid));
		} else if(action.equals("delete_users")) {
			// 削除
			String oid = request.getParameter("oid");
			String uid = request.getParameter("uid");
			out.write(UpdateLogic.execUsersDelete(oid, uid));
		} else if(action.equals("insert_users")) {
			// 追加
			out.write(UpdateLogic.execUsersInsert(session_oid, session_uid, params));

		/*******************
		 * NFC(IDM)
		 *******************/
		} else if(action.equals("delete_nfc")) {
			// 削除
			out.write(UpdateLogic.execNfcDelete(params));

		/*******************
		 * NFCwait(IDM)
		 *******************/
		} else if(action.equals("delete_nfcwait")) {
			// 削除
			out.write(UpdateLogic.execNfcwaitDelete(params));
		} else if(action.equals("insert_nfcwait")) {
			// 追加
			out.write(UpdateLogic.execNfcwaitInsert(params, session_uid));

		/*******************
		 * 業務種別
		 *******************/
		} else if(action.equals("update_worktype")) {
			// 更新
			out.write(UpdateLogic.execWorktypeUpdate(params, session_uid));
		} else if(action.equals("insert_worktype")) {
			// 追加
			out.write(UpdateLogic.execWorktypeInsert(session_oid, session_uid, params));

		/*******************
		 * 端末
		 *******************/
		} else if(action.equals("insert_unit")) {
			// 追加
			out.write(UpdateLogic.execUnitInsert(session_oid, params));
		} else if(action.equals("update_unit")) {
			// 更新
			out.write(UpdateLogic.execUnitUpdate(session_oid, params));
		} else if(action.equals("delete_unit")) {
			// 削除
			out.write(UpdateLogic.execUnitDelete(session_oid, params));

		/*******************
		 * 部署
		 *******************/
		} else if (action.equals("insert_department")) {
			// 追加
			out.write(UpdateLogic.execDepartmentInsert(session_oid, params));
		} else if (action.equals("update_department")) {
			// 更新
			out.write(UpdateLogic.execDepartmentUpdate(params));

		/*******************
		 * カテゴリー
		 *******************/
		} else if (action.equals("insert_category")) {
			// 追加
			out.write(UpdateLogic.execCategoryInsert(session_oid, params));
		} else if (action.equals("update_category")) {
			// 更新
			out.write(UpdateLogic.execCategoryUpdate(params));

		/*******************
		 * 客先
		 *******************/
		} else if (action.equals("insert_customer")) {
			// 追加
			out.write(UpdateLogic.execCustomerInsert(session_oid, params));
		} else if (action.equals("update_customer")) {
			// 更新
			out.write(UpdateLogic.execCustomerUpdate(params));

		/*******************
		 * 雇用形態
		 *******************/
		} else if (action.equals("insert_employment")) {
			// 追加
			out.write(UpdateLogic.execEmploymentInsert(session_oid, params));
		} else if (action.equals("update_employment")) {
			// 更新
			out.write(UpdateLogic.execEmploymentUpdate(params));

		/*******************
		 * 勤務地
		 *******************/
		} else if (action.equals("insert_worklocation")) {
			// 追加
			out.write(UpdateLogic.execWorklocationInsert(session_oid, params));
		} else if (action.equals("update_worklocation")) {
			// 更新
			out.write(UpdateLogic.execWorklocationUpdate(params));

		/*******************
		 * グループ
		 *******************/
		} else if (action.equals("insert_group")) {
			// 追加
			out.write(UpdateLogic.execGroupInsert(session_oid, params));
		} else if (action.equals("update_group")) {
			// 更新
			out.write(UpdateLogic.execGroupUpdate(params));
		}
	}

}
