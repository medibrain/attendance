package table;

import util.Util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Worktype {

	// table
	public Property oid = new Property(Property.ValueTypes.INT, -1);
	public Property wid = new Property(Property.ValueTypes.INT, -1);
	public Property name = new Property(Property.ValueTypes.STR, null);
	public Property kana = new Property(Property.ValueTypes.STR, null);
	public Property enable = new Property(Property.ValueTypes.BOOL, false);
	public Property idate = new Property(Property.ValueTypes.INT_DATE, -1);
	public Property iuid = new Property(Property.ValueTypes.INT, -1);
	public Property udate = new Property(Property.ValueTypes.INT_DATE, -1);
	public Property uuid = new Property(Property.ValueTypes.INT, -1);
	public Property category = new Property(Property.ValueTypes.INT, -1);
	public Property customer = new Property(Property.ValueTypes.INT, -1);
	public Property note = new Property(Property.ValueTypes.STR, null);
	public Property manager = new Property(Property.ValueTypes.INT, -1);
	public Property projectcode = new Property(Property.ValueTypes.STR, null);
	// others
	public Property oname = new Property(Property.ValueTypes.STR, null);
	public Property categoryname = new Property(Property.ValueTypes.STR, null);
	public Property customername = new Property(Property.ValueTypes.STR, null);
	public Property managername = new Property(Property.ValueTypes.STR, null);
	// json
	public String valid;
	public String idatetext;
	public String udatetext;

	public Worktype(){}

	@JsonCreator
	public Worktype(
			@JsonProperty("oid") String oid,
			@JsonProperty("wid") String wid,
			@JsonProperty("name") String name,
			@JsonProperty("kana") String kana,
			@JsonProperty("enable") String enable,
			@JsonProperty("iuid") String iuid,
			@JsonProperty("uuid") String uuid,
			@JsonProperty("category") String category,
			@JsonProperty("customer") String customer,
			@JsonProperty("note") String note,
			@JsonProperty("manager") String manager,
			@JsonProperty("projectcode") String projectcode,
			@JsonProperty("categoryname") String categoryname,
			@JsonProperty("customername") String customername,
			@JsonProperty("managername") String managername,
			@JsonProperty("valid") String valid,
			@JsonProperty("idatetext") String idatetext,
			@JsonProperty("udatetext") String udatetext) {

		setOid(Util.toInt(oid));
		setWid(Util.toInt(wid));
		setName(name);
		setKana(kana);
		setEnable(Util.toBoolean(enable));
		setIuid(Util.toInt(iuid));
		setUuid(Util.toInt(uuid));
		setCategory(Util.toInt(category));
		setCustomer(Util.toInt(customer));
		setNote(note);
		setManager(Util.toInt(manager));
		setProjectcode(projectcode);
		setCategoryname(categoryname);
		setCustomername(customername);
		setManagername(managername);

		this.valid = valid;
		this.idatetext = idatetext;
		this.udatetext = udatetext;
	}

	public int getOid() {
		return oid.IntValue;
	}
	public void setOid(int oid) {
		this.oid.IntValue = oid;
	}
	public int getWid() {
		return wid.IntValue;
	}
	public void setWid(int wid) {
		this.wid.IntValue = wid;
	}
	public String getName() {
		return name.StringValue;
	}
	public void setName(String name) {
		this.name.StringValue= name;
	}
	public String getKana() {
		return kana.StringValue;
	}
	public void setKana(String kana) {
		this.kana.StringValue = kana;
	}
	public boolean isEnable() {
		return enable.BooleanValue;
	}
	public void setEnable(boolean enable) {
		this.enable.BooleanValue = enable;
	}
	public int getIdate() {
		return idate.IntValue;
	}
	public void setIdate(int idate) {
		this.idate.IntValue = idate;
	}
	public int getIuid() {
		return iuid.IntValue;
	}
	public void setIuid(int iuid) {
		this.iuid.IntValue = iuid;
	}
	public int getUdate() {
		return udate.IntValue;
	}
	public void setUdate(int udate) {
		this.udate.IntValue= udate;
	}
	public int getUuid() {
		return uuid.IntValue;
	}
	public void setUuid(int uuid) {
		this.uuid.IntValue = uuid;
	}
	public String getOname() {
		return oname.StringValue;
	}
	public void setOname(String oname) {
		this.oname.StringValue = oname;
	}
	public int getCategory() {
		return category.IntValue;
	}
	public void setCategory(int category) {
		this.category.IntValue = category;
	}
	public int getCustomer() {
		return customer.IntValue;
	}
	public void setCustomer(int customer) {
		this.customer.IntValue = customer;
	}
	public String getNote() {
		return note.StringValue;
	}
	public void setNote(String note) {
		this.note.StringValue = note;
	}
	public int getManager() {
		return manager.IntValue;
	}
	public void setManager(int manager) {
		this.manager.IntValue = manager;
	}
	public String getProjectcode() {
		return projectcode.StringValue;
	}
	public void setProjectcode(String projectcode) {
		this.projectcode.StringValue = projectcode;
	}
	public String getCategoryname() {
		return categoryname.StringValue;
	}
	public void setCategoryname(String categoryname) {
		this.categoryname.StringValue = categoryname;
	}
	public String getCustomername() {
		return customername.StringValue;
	}
	public void setCustomername(String customername) {
		this.customername.StringValue = customername;
	}
	public String getManagername() {
		return managername.StringValue;
	}
	public void setManagername(String managername) {
		this.managername.StringValue = managername;
	}

}
