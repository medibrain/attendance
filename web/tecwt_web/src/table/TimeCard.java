package table;


public class TimeCard {

	private int oid;
	private int uid;
	private int tdate;
	private int times;
	private int intime;
	private int outtime;
	private int breaktime;
	private int wid;
	private String note;
	private int fare;
	private int wcount;
	private int rcount;
	private String oname;
	private String uname;
	private String ukana;
	private String wname;
	private String dayoftheweek;

	private int movetime;
	private int move_wid;
	private String move_note;
	private int move_wcount;
	private int move_rcount;
	private String move_wname;

	public String getDayoftheweek() {
		return dayoftheweek;
	}
	public void setDayoftheweek(String dayoftheweek) {
		this.dayoftheweek = dayoftheweek;
	}
	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public int getTdate() {
		return tdate;
	}
	public void setTdate(int tdate) {
		this.tdate = tdate;
	}
	public int getTimes() {
		return times;
	}
	public void setTimes(int times) {
		this.times = times;
	}
	public int getIntime() {
		return intime;
	}
	public void setIntime(int intime) {
		this.intime = intime;
	}
	public int getOuttime() {
		return outtime;
	}
	public void setOuttime(int outtime) {
		this.outtime = outtime;
	}
	public int getBreaktime() {
		return breaktime;
	}
	public void setBreaktime(int breaktime) {
		this.breaktime = breaktime;
	}
	public int getWid() {
		return wid;
	}
	public void setWid(int wid) {
		this.wid = wid;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public int getFare() {
		return fare;
	}
	public void setFare(int fare) {
		this.fare = fare;
	}
	public int getWcount() {
		return wcount;
	}
	public void setWcount(int wcount) {
		this.wcount = wcount;
	}
	public int getRcount() {
		return rcount;
	}
	public void setRcount(int rcount) {
		this.rcount = rcount;
	}
	public String getOname() {
		return oname;
	}
	public void setOname(String oname) {
		this.oname = oname;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}
	public String getUkana() {
		return ukana;
	}
	public void setUkana(String ukana) {
		this.ukana = ukana;
	}
	public String getWname() {
		return wname;
	}
	public void setWname(String wname) {
		this.wname = wname;
	}
	public int getMovetime() {
		return movetime;
	}
	public void setMovetime(int movetime) {
		this.movetime = movetime;
	}
	public int getMove_wid() {
		return move_wid;
	}
	public void setMove_wid(int move_wid) {
		this.move_wid = move_wid;
	}
	public String getMove_note() {
		return move_note;
	}
	public void setMove_note(String move_note) {
		this.move_note = move_note;
	}
	public int getMove_wcount() {
		return move_wcount;
	}
	public void setMove_wcount(int move_wcount) {
		this.move_wcount = move_wcount;
	}
	public int getMove_rcount() {
		return move_rcount;
	}
	public void setMove_rcount(int move_rcount) {
		this.move_rcount = move_rcount;
	}
	public String getMove_wname() {
		return move_wname;
	}
	public void setMove_wname(String move_wname) {
		this.move_wname = move_wname;
	}


}
