package table;

import util.Util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Users {

	// table
	public Property oid = new Property(Property.ValueTypes.INT, -1);
	public Property uid = new Property(Property.ValueTypes.INT, -1);
	public Property code = new Property(Property.ValueTypes.STR, null);
	public Property lname = new Property(Property.ValueTypes.STR, null);
	public Property name = new Property(Property.ValueTypes.STR, null);
	public Property kana = new Property(Property.ValueTypes.STR, null);
	public Property enable = new Property(Property.ValueTypes.BOOL, false);
	public Property pass = new Property(Property.ValueTypes.STR, null);
	public Property admin = new Property(Property.ValueTypes.BOOL, false);
	public Property mailladd = new Property(Property.ValueTypes.STR, null);
	public Property idate = new Property(Property.ValueTypes.INT_DATE, -1);
	public Property iuid = new Property(Property.ValueTypes.INT, -1);
	public Property udate = new Property(Property.ValueTypes.INT_DATE, -1);
	public Property uuid = new Property(Property.ValueTypes.INT, -1);
	public Property mailenable = new Property(Property.ValueTypes.BOOL, false);
	public Property department = new Property(Property.ValueTypes.INT, -1);
	public Property employment = new Property(Property.ValueTypes.INT, -1);
	public Property worklocation = new Property(Property.ValueTypes.INT, -1);
	public Property group = new Property(Property.ValueTypes.INT, -1);
	public Property hiredate = new Property(Property.ValueTypes.INT_DATE, 0);
	public Property paidvacationdays = new Property(Property.ValueTypes.INT, 0);
	// other
	public Property idmcount = new Property(Property.ValueTypes.STR, null);
	public Property oname = new Property(Property.ValueTypes.STR, null);
	public Property nfcwait = new Property(Property.ValueTypes.BOOL, false);
	public Property departmentname = new Property(Property.ValueTypes.STR, null);
	public Property employmentname = new Property(Property.ValueTypes.STR, null);
	public Property worklocationname = new Property(Property.ValueTypes.STR, null);
	public Property groupname = new Property(Property.ValueTypes.STR, null);
	// json
	public String admintext;
	public String mailenabletext;
	public String issignupidm;
	public String isloginok;
	public String hiredatetext;
	public String idatetext;
	public String udatetext;

	public Users(){}

	@JsonCreator
	public Users(
			@JsonProperty("oid") String oid,
			@JsonProperty("uid") String uid,
			@JsonProperty("code") String code,
			@JsonProperty("lname") String lname,
			@JsonProperty("name") String name,
			@JsonProperty("kana") String kana,
			@JsonProperty("enable") String enable,
			@JsonProperty("pass") String pass,
			@JsonProperty("default_admin") String admin,
			@JsonProperty("mailladd") String mailladd,
			@JsonProperty("iuid") String iuid,
			@JsonProperty("uuid") String uuid,
			@JsonProperty("default_mailenable") String mailenable,
			@JsonProperty("departmentname") String departmentname,
			@JsonProperty("employmentname") String employmentname,
			@JsonProperty("worklocationname") String worklocationname,
			@JsonProperty("groupname") String groupname,
			@JsonProperty("idmcount") String idmcount,
			@JsonProperty("paidvacationdays") String paidvacationdays,
			@JsonProperty("admintext") String admintext,
			@JsonProperty("mailenabletext") String mailenabletext,
			@JsonProperty("issignupidm") String issignupidm,
			@JsonProperty("isloginok") String isloginok,
			@JsonProperty("hiredatetext") String hiredatetext,
			@JsonProperty("idatetext") String idatetext,
			@JsonProperty("udatetext") String udatetext) {

		setOid(Util.toInt(oid));
		setUid(Util.toInt(uid));
		setCode(code);
		setLname(lname);
		setName(name);
		setKana(kana);
		setEnable(Util.toBoolean(enable));
		setPass(pass);
		setAdmin(Util.toBoolean(admin));
		setMailladd(mailladd);
		setIuid(Util.toInt(iuid));
		setUuid(Util.toInt(uuid));
		setMailenable(Util.toBoolean(mailenable));
		setDepartmentname(departmentname);
		setEmploymentname(employmentname);
		setWorklocationname(worklocationname);
		setGroupname(groupname);
		setIdmcount(idmcount);
		setPaidvacationdays(Util.toInt(paidvacationdays));

		this.admintext = admintext;
		this.mailenabletext = mailenabletext;
		this.issignupidm = issignupidm;
		this.isloginok = isloginok;
		this.hiredatetext = hiredatetext;
		this.idatetext = idatetext;
		this.udatetext = udatetext;
	}

	public boolean isNfcwait() {
		return nfcwait.BooleanValue;
	}
	public void setNfcwait(boolean nfcwait) {
		this.nfcwait.BooleanValue = nfcwait;
	}
	public String getOname() {
		return oname.StringValue;
	}
	public void setOname(String oname) {
		this.oname.StringValue = oname;
	}
	public String getIdmcount() {
		return idmcount.StringValue;
	}
	public void setIdmcount(String idmcount) {
		this.idmcount.StringValue = idmcount;
	}
	public int getOid() {
		return oid.IntValue;
	}
	public void setOid(int oid) {
		this.oid.IntValue = oid;
	}
	public int getUid() {
		return uid.IntValue;
	}
	public void setUid(int uid) {
		this.uid.IntValue = uid;
	}
	public String getCode() {
		return code.StringValue;
	}
	public void setCode(String code) {
		this.code.StringValue = code;
	}
	public String getLname() {
		return lname.StringValue;
	}
	public void setLname(String lname) {
		this.lname.StringValue = lname;
	}
	public String getName() {
		return name.StringValue;
	}
	public void setName(String name) {
		this.name.StringValue = name;
	}
	public String getKana() {
		return kana.StringValue;
	}
	public void setKana(String kana) {
		this.kana.StringValue= kana;
	}
	public boolean isEnable() {
		return enable.BooleanValue;
	}
	public void setEnable(boolean enable) {
		this.enable.BooleanValue = enable;
	}
	public String getPass() {
		return pass.StringValue;
	}
	public void setPass(String pass) {
		this.pass.StringValue= pass;
	}
	public boolean isAdmin() {
		return admin.BooleanValue;
	}
	public void setAdmin(boolean admin) {
		this.admin.BooleanValue = admin;
	}
	public String getMailladd() {
		return mailladd.StringValue;
	}
	public void setMailladd(String mailladd) {
		this.mailladd.StringValue = mailladd;
	}
	public int getIdate() {
		return idate.IntValue;
	}
	public void setIdate(int idate) {
		this.idate.IntValue = idate;
	}
	public int getIuid() {
		return iuid.IntValue;
	}
	public void setIuid(int iuid) {
		this.iuid.IntValue = iuid;
	}
	public int getUdate() {
		return udate.IntValue;
	}
	public void setUdate(int udate) {
		this.udate.IntValue = udate;
	}
	public int getUuid() {
		return uuid.IntValue;
	}
	public void setUuid(int uuid) {
		this.uuid.IntValue = uuid;
	}
	public boolean isMailenable() {
		return mailenable.BooleanValue;
	}
	public void setMailenable(boolean mailenable) {
		this.mailenable.BooleanValue = mailenable;
	}
	public int getDepartment() {
		return department.IntValue;
	}
	public void setDepartment(int department) {
		this.department.IntValue = department;
	}
	public String getDepartmentname() {
		return departmentname.StringValue;
	}
	public void setDepartmentname(String departmentname) {
		this.departmentname.StringValue = departmentname;
	}
	public int getEmployment() {
		return employment.IntValue;
	}
	public void setEmployment(int employment) {
		this.employment.IntValue = employment;
	}
	public String getEmploymentname() {
		return employmentname.StringValue;
	}
	public void setEmploymentname(String employmentname) {
		this.employmentname.StringValue = employmentname;
	}
	public int getWorklocation() {
		return worklocation.IntValue;
	}
	public void setWorklocation(int worklocation) {
		this.worklocation.IntValue = worklocation;
	}
	public String getWorklocationname() {
		return worklocationname.StringValue;
	}
	public void setWorklocationname(String worklocationname) {
		this.worklocationname.StringValue = worklocationname;
	}
	public int getGroup() {
		return group.IntValue;
	}
	public void setGroup(int group) {
		this.group.IntValue = group;
	}
	public String getGroupname() {
		return groupname.StringValue;
	}
	public void setGroupname(String groupname) {
		this.groupname.StringValue = groupname;
	}
	public int getHiredate() {
		return hiredate.IntValue;
	}
	public void setHiredate(int hiredate) {
		this.hiredate.IntValue = hiredate;
	}
	public int getPaidvacationdays() {
		return paidvacationdays.IntValue;
	}
	public void setPaidvacationdays(int paidvacationdays) {
		this.paidvacationdays.IntValue = paidvacationdays;
	}

}
