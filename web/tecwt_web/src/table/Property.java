package table;

public class Property {

	public enum ValueTypes {
		INT,
		INT_DATE,
		STR,
		BOOL,
	}

	public ValueTypes ValueType;
	public int IntValue;
	public int DefaultIntValue;
	public String StringValue;
	public String DefaultStringValue;
	public boolean BooleanValue;
	public boolean DefaultBooleanValue;

	public Property(ValueTypes ValueType, int DefaultValue) {
		this.ValueType = ValueType;
		this.DefaultIntValue = DefaultValue;
	}

	public Property(ValueTypes ValueType, String DefaultValue) {
		this.ValueType = ValueType;
		this.DefaultStringValue = DefaultValue;
	}

	public Property(ValueTypes ValueType, boolean DefaultValue) {
		this.ValueType = ValueType;
		this.DefaultBooleanValue = DefaultValue;
	}

}
