package table;


public class Nfc {

	private String idm;
	private int oid;
	private int uid;
	private String note;
	private int idate;
	private int iuid;
	private int udate;
	private int uuid;


	public String getIdm() {
		return idm;
	}
	public void setIdm(String idm) {
		this.idm = idm;
	}
	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public int getIdate() {
		return idate;
	}
	public void setIdate(int idate) {
		this.idate = idate;
	}
	public int getIuid() {
		return iuid;
	}
	public void setIuid(int iuid) {
		this.iuid = iuid;
	}
	public int getUdate() {
		return udate;
	}
	public void setUdate(int udate) {
		this.udate = udate;
	}
	public int getUuid() {
		return uuid;
	}
	public void setUuid(int uuid) {
		this.uuid = uuid;
	}

}
