package table;

import util.Util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Customer {

	// table
	public Property id = new Property(Property.ValueTypes.INT, -1);
	public Property oid = new Property(Property.ValueTypes.INT, -1);
	public Property name = new Property(Property.ValueTypes.STR, null);
	public Property kana = new Property(Property.ValueTypes.STR, null);
	public Property enable = new Property(Property.ValueTypes.BOOL, false);
	public Property note = new Property(Property.ValueTypes.STR, null);
	public Property idate = new Property(Property.ValueTypes.INT_DATE, -1);
	public Property udate = new Property(Property.ValueTypes.INT_DATE, -1);
	// json
	public String valid;

	public Customer(){}

	@JsonCreator
	private Customer(
			@JsonProperty("customerid") String id,
			@JsonProperty("oid") String oid,
			@JsonProperty("name") String name,
			@JsonProperty("kana") String kana,
			@JsonProperty("enable") String enable,
			@JsonProperty("note") String note,
			@JsonProperty("idate") String idate,
			@JsonProperty("udate") String udate,
			@JsonProperty("valid") String valid) {

		setId(Util.toInt(id));
		setOid(Util.toInt(oid));
		setName(name);
		setKana(kana);
		setEnable(Util.toBoolean(enable));
		setNote(note);
		setIdate(Util.toDateInt(idate));
		setUdate(Util.toDateInt(udate));

		this.valid = valid;
	}

	public int getId() {
		return id.IntValue;
	}
	public void setId(int id) {
		this.id.IntValue = id;
	}
	public int getOid() {
		return oid.IntValue;
	}
	public void setOid(int oid) {
		this.oid.IntValue = oid;
	}
	public String getName() {
		return name.StringValue;
	}
	public void setName(String name) {
		this.name.StringValue = name;
	}
	public String getKana() {
		return kana.StringValue;
	}
	public void setKana(String kana) {
		this.kana.StringValue = kana;
	}
	public boolean isEnable() {
		return enable.BooleanValue;
	}
	public void setEnable(boolean enable) {
		this.enable.BooleanValue = enable;
	}
	public String getNote() {
		return note.StringValue;
	}
	public void setNote(String note) {
		this.note.StringValue = note;
	}
	public int getIdate() {
		return idate.IntValue;
	}
	public void setIdate(int idate) {
		this.idate.IntValue = idate;
	}
	public int getUdate() {
		return udate.IntValue;
	}
	public void setUdate(int udate) {
		this.udate.IntValue = udate;
	}

}
