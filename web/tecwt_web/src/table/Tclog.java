package table;


public class Tclog {

	private int oid;
	private int uid;
	private String logtype;
	private String func;
	private int logdate;
	private int carddate;
	private int logtime;
	private String latitude;
	private String longitude;
	private String unitid;
	private String oname;
	private String uname;
	private String unitname;


	public String getUnitname() {
		return unitname;
	}
	public void setUnitname(String unitname) {
		this.unitname = unitname;
	}
	public int getCarddate() {
		return carddate;
	}
	public void setCarddate(int carddate) {
		this.carddate = carddate;
	}
	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getLogtype() {
		return logtype;
	}
	public void setLogtype(String logtype) {
		this.logtype = logtype;
	}
	public String getFunc() {
		return func;
	}
	public void setFunc(String func) {
		this.func = func;
	}
	public int getLogdate() {
		return logdate;
	}
	public void setLogdate(int logdate) {
		this.logdate = logdate;
	}
	public int getLogtime() {
		return logtime;
	}
	public void setLogtime(int logtime) {
		this.logtime = logtime;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getUnitid() {
		return unitid;
	}
	public void setUnitid(String unitid) {
		this.unitid = unitid;
	}
	public String getOname() {
		return oname;
	}
	public void setOname(String oname) {
		this.oname = oname;
	}
	public String getUname() {
		return uname;
	}
	public void setUname(String uname) {
		this.uname = uname;
	}

}
