package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import db.DBPreparedStatementConnect;
import db.DBPreparedStatementConnect.Batch;
import db.DBinfo;
import util.Log;

public class WorkDetailCode {
	public static class Data {
	    public int detailID;
	    public String name;
	    public boolean enable;
	    public int order;
	}

	public int departmentID;
	public Data[] codes;

	private static final String workDetailTable = DBinfo.TABLE_WORK_DETAIL;

	public static WorkDetailCode makeFromDB(int oid, int department) {
		DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();

		String sql = "SELECT work_detail_id, name, enable, display_order "
			+ "FROM " + workDetailTable + " "
			+ "WHERE oid = ? AND department_id = ? "
			+ "ORDER BY display_order;";

        try {
    		List<Data> codes = new ArrayList<Data>();

            ResultSet rs = db.execQuery(sql, Arrays.asList(oid, department));
    		while(rs.next()) {
            	Data code = new Data();
            	code.detailID = rs.getInt("work_detail_id");
                code.name = rs.getString("name");
                code.enable = rs.getBoolean("enable");
                code.order = rs.getInt("display_order");

                codes.add(code);
            }

    		WorkDetailCode result = new WorkDetailCode();
    		result.departmentID = department;
    		result.codes = codes.toArray(new Data[codes.size()]);
    		return result;
        } catch (SQLException | NumberFormatException e) {
            Log.exceptionWrite(e);
            return null;
        } finally {
            db.close();
        }
	}

	public boolean updateDB(int oid) {
        DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();

		List<Batch> batches = new ArrayList<Batch>();
		String insert = "INSERT INTO " + workDetailTable + " "
			+ "(oid, department_id, name, enable, display_order, update_at) "
			+ "VALUES (?, ?, ?, ?, ?, null)";
		String update = "UPDATE " + workDetailTable + " "
			+ "SET name = ?, enable = ?, display_order = ?, update_at = now() "
			+ "WHERE oid = ? AND department_id = ? AND work_detail_id = ?";

		List<List<Object>> insertParams = new ArrayList<List<Object>>();
		List<List<Object>> updateParams = new ArrayList<List<Object>>();

		for (Data code : codes) {
			if (code.detailID < 0) {
				// 新規
				insertParams.add(Arrays.asList(oid, departmentID, code.name, code.enable, code.order));
			} else {
				// 既存（変更なくても全部上書き）
				updateParams.add(Arrays.asList(code.name, code.enable, code.order, oid, departmentID, code.detailID));
			}
		}

		if (!insertParams.isEmpty()) {
			Batch batch = new Batch();
			batch.sql = insert;
			batch.parameterList = insertParams;
			batches.add(batch);
		}

		if (!updateParams.isEmpty()) {
			Batch batch = new Batch();
			batch.sql = update;
			batch.parameterList = updateParams;
			batches.add(batch);
		}
        boolean success = db.execBatchSQL(batches);
        db.close();
        return success;
	}
}
