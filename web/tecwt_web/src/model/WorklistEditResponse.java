package model;

import java.sql.SQLException;

import db.DBPreparedStatementConnect;
import util.DateTime;
import util.Log;

// WorklistResponseの編集版
public class WorklistEditResponse extends WorklistResponse {
	public static class WorkDetailList {
		public ProjectGroup[] allWorkDetails;
	}

	// 編集可能かどうか（＝管理者）
	public boolean canEdit;
	// 当月分かどうか
	public boolean isThisMonth;

	public static WorklistEditResponse makeFromDB(int date, int oid, int uid, boolean isAdmin) {
		WorklistResponse response = WorklistResponse.makeFromDB(date, oid, uid, false, isAdmin);

		if (response == null) {
			return null;
		}

		WorklistEditResponse result = new WorklistEditResponse();
		result.oid = response.oid;
		result.uid = response.uid;
		result.tdate = response.tdate;
		result.name = response.name;
		result.projects = response.projects;
		result.worktypes = response.worktypes;
		result.intime = response.intime;
		result.outtime = response.outtime;
		result.breaktime = response.breaktime;
		result.fare = response.fare;
	    result.works = response.works;
		result.allProjects = response.allProjects;
		result.canEdit = isAdmin;		// 管理者はいつでも変更可
		result.allWorkDetails = response.allWorkDetails;

		int month = DateTime.GetTodayInt() / 100;
		int target = result.tdate / 100;
		result.isThisMonth = (month == target);
		return result;
	}

	public static WorkDetailList makeDetailListFromDB(int oid) {
		DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();

		WorkDetailList result = new WorkDetailList();
		try {
			result.allWorkDetails = queryAllWorkDetails(db, oid);
			return result;
		} catch (SQLException e) {
			Log.exceptionWrite(e);
			return null;
		} finally {
			db.close();
		}
	}
}
