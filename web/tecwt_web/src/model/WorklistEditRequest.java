package model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import db.DBPreparedStatementConnect;
import db.DBPreparedStatementConnect.Batch;
import db.DBinfo;

// WorklistRequestの編集版
// （運用の都合上、作業内容変更時は勤務種別のみタイムカード側にも反映させる）
public class WorklistEditRequest {
	public static class Work {
		public int wid;
		public int detailID;
		public int minute;
    }

    public int tdate;

	public int oid;
	public int uid;

    public Work[] works;

    private static final String dailyTable = DBinfo.TABLE_DAILY_WORK_RESULT;
	private static final String timecardTable = DBinfo.TABLE_TIMECARD;

    public boolean updateDB() {
        DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();

        List<Batch> batches = new ArrayList<Batch>();

        // 更新は削除→挿入の順
        batches.add(makeDailyDeleteBatch());
        batches.add(makeDailyInsertBatch());
        batches.add(makeTimecardBatch());

        boolean result = db.execBatchSQL(batches);

        db.close();

        return result;
    }

    private Batch makeDailyDeleteBatch() {
        String sql = "DELETE FROM " + dailyTable + " "
            + "WHERE oid = ? AND uid = ? AND tdate = ?;";
        Batch batch = new Batch();
        batch.sql = sql;
        batch.parameterList = Arrays.asList(Arrays.asList(oid, uid, tdate));
        return batch;
    }

    private Batch makeDailyInsertBatch() {
        String sql = "INSERT INTO " + dailyTable + " "
            + "(oid, uid, tdate, wid, work_detail_id, work_minute) "
            + "VALUES (?, ?, ?, ?, ?, ?);";
        Batch batch = new Batch();
        List<List<Object>> parameters = new ArrayList<List<Object>>();
        for (Work work : works) {
            parameters.add(
                Arrays.asList(oid, uid, tdate, work.wid, work.detailID, work.minute)
            );
        }
        batch.sql = sql;
        batch.parameterList = parameters;
        return batch;
    }

    // タイムカードの勤務種別だけ変更する
    private Batch makeTimecardBatch() {
        String sql = "UPDATE " + timecardTable + " SET wid = ? "
        	+ "WHERE oid = ? AND uid = ? AND tdate = ? AND times = ?;";

        int wid = works.length > 0 ? works[0].wid : 0;
        int times = WorklistRequest.WORKING_NORMAL_TIMES_VALUE;     // 移動が無くなったので1固定
        List<Object> param = Arrays.asList(wid, oid, uid, tdate, times);

        Batch batch = new Batch();
        batch.sql = sql;
        batch.parameterList = Arrays.asList(param);
        return batch;
    }

}
