package model;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import util.DateTime;
import util.Log;
import util.Util;
import db.BaseDAO;
import db.CategoryDAO;
import db.CustomerDAO;
import db.DepartmentDAO;
import db.EmploymentDAO;
import db.GroupDAO;
import db.NfcDAO;
import db.NfcwaitDAO;
import db.TimeCardDAO;
import db.UnitDAO;
import db.UsersDAO;
import db.WorklocationDAO;
import db.WorktypeDAO;

public class UpdateLogic {

	/**
	 * タイムカードを更新します。
	 * @param recordParams
	 * @return
	 */
	public static String execTimeCardUpdate2(Map<String, String> recordParams) {
		String json = "";
		try {

			// oid
			String oid = recordParams.get("oid");

			// uid
			String defaultUid = recordParams.get("default_uid");	// 変更前のuid

			// tdate
			String defaultTdate = Util.replaceTdate(recordParams.get("default_tdate"));	// 変更前のtdate

			// パラメータ取得
			String[] change = recordParams.get("change").split(",", 0);
			String uid = null;
			String uname = null;
			String tdate = null;
			String intime = null;
			String movetime = null;
			String outtime = null;
			String wid = null;
			String wname = null;
			String note = null;
			String wcount = null;
			String rcount = null;
			String wid_move = null;
			String wname_move = null;
			String note_move = null;
			String wcount_move = null;
			String rcount_move = null;
			String breaktime = null;
			String fare = null;

			// 変更前の状態
			boolean isDefaultMove = Boolean.valueOf(recordParams.get("default_ismove"));
			// 移動を保持するか否か
			boolean isKeepMove = true;

			String name;
			for(int i=0; i<change.length; i++) {
				name = change[i];

				if(name.equals("uid")) {
					uid = recordParams.get("uid");
					uname = recordParams.get("uname");
				}
				else if(name.equals("tdate")) {
					tdate = Util.replaceTdate(recordParams.get("tdate"));
				}
				else if(name.equals("intime")) {
					String inH = recordParams.get("intime_h");
					String inM = recordParams.get("intime_m");
					intime = inH + inM;
				}
				else if(name.equals("movetime")) {
					String moveH = recordParams.get("movetime_h");
					String moveM = recordParams.get("movetime_m");
					movetime = moveH + moveM;
					if(movetime != null && movetime.equals("----")) {
						isKeepMove = false;
					}
				}
				else if(name.equals("outtime")) {
					String outH = recordParams.get("outtime_h");
					String outM = recordParams.get("outtime_m");
					outtime = outH + outM;
				}
				else if(name.equals("wid")) {
					wid = recordParams.get("wid");
					wname = recordParams.get("wname");
				}
				else if(name.equals("note")) note = recordParams.get("note");
				else if(name.equals("wcount")) wcount = recordParams.get("wcount");
				else if(name.equals("rcount")) rcount = recordParams.get("rcount");
				else if(name.equals("wid_move")) {
					wid_move = recordParams.get("wid_move");
					wname_move = recordParams.get("wname_move");
				}
				else if(name.equals("note_move")) note_move = recordParams.get("note_move");
				else if(name.equals("wcount_move")) wcount_move = recordParams.get("wcount_move");
				else if(name.equals("rcount_move")) rcount_move = recordParams.get("rcount_move");
				else if(name.equals("breaktime")) breaktime = recordParams.get("breaktime");
				else if(name.equals("fare")) fare = recordParams.get("fare");
			}

			// 実行
			TimeCardDAO dao = new TimeCardDAO();
			json = dao.updateForGrid2(
					defaultUid, defaultTdate,
					oid, uid, tdate,
					intime, movetime, outtime, wid, note, wcount, rcount,
					wid_move, note_move, wcount_move, rcount_move,
					breaktime, fare,
					uname, wname, wname_move, isDefaultMove, isKeepMove);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL001]");
		} catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL002]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * タイムカードを削除します。
	 * @param recordParams
	 * @return
	 */
	public static String execTimeCardDelete(Map<String, String> recordParams) {
		String json = "";
		try {

			// 値の取得
			String oid = Util.nullBlankCheck(recordParams.get("oid"));
			String default_uid = Util.nullBlankCheck(recordParams.get("default_uid"));
			String default_tdate = Util.nullBlankCheck(Util.replaceTdate(recordParams.get("default_tdate")));

			// 変更前の状態
			boolean defaultIsMove = Boolean.valueOf(recordParams.get("default_ismove"));

			// 実行
			TimeCardDAO dao = new TimeCardDAO();
			json = dao.deleteForGrid(oid, default_uid, default_tdate, defaultIsMove);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL003]");
		} catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL004]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * タイムカードを追加します。
	 * @param recordParams
	 * @return
	 */
	public static String execTimeCardInsert(String oid, Map<String, String> recordParams) {
		String json = "";
		try {

			// times=2が必要か判定
			boolean isTimes2Need = recordParams.get("ismove").equals("true");

			Map<String, String> params1 = new HashMap<String, String>();
			Map<String, String> params2 = null;

			// 値の取得
			String OID = Util.nullBlankHyphenToNull(oid);
			String UID = Util.nullBlankHyphenToNull(recordParams.get("uid"));
			String TDATE = Util.replaceTdate(Util.nullBlankHyphenToNull(recordParams.get("tdate")));
			String WID = Util.nullBlankHyphenToNull(recordParams.get("wid"));
			String NOTE = Util.replaceCRLF(recordParams.get("note"));
			String WCOUNT = Util.nullBlankHyphenToNull(recordParams.get("wcount"));
			String RCOUNT = Util.nullBlankHyphenToNull(recordParams.get("rcount"));
			String WID_M = Util.nullBlankHyphenToNull(recordParams.get("wid_move"));
			String NOTE_M = Util.replaceCRLF(recordParams.get("note_move"));
			String WCOUNT_M = Util.nullBlankHyphenToNull(recordParams.get("wcount_move"));
			String RCOUNT_M = Util.nullBlankHyphenToNull(recordParams.get("rcount_move"));
			String BREAKTIME = Util.nullBlankHyphenToNull(recordParams.get("breaktime"));
			String FARE = Util.nullBlankHyphenToNull(recordParams.get("fare"));
			String UNAME = Util.nullBlankHyphenToNull(recordParams.get("uname"));
			String WNAME = Util.nullBlankHyphenToNull(recordParams.get("wname"));
			String WNAME_M = Util.nullBlankHyphenToNull(recordParams.get("wname_move"));

			// 時間成型
			String INTIME_HOUR = Util.nullBlankHyphenToNull(recordParams.get("intime_h"));
			String INTIME_MINUTE = Util.nullBlankHyphenToNull(recordParams.get("intime_m"));
			String MOVETIME_HOUR = Util.nullBlankHyphenToNull(recordParams.get("movetime_h"));
			String MOVETIME_MINUTE = Util.nullBlankHyphenToNull(recordParams.get("movetime_m"));
			String OUTTIME_HOUR = Util.nullBlankHyphenToNull(recordParams.get("outtime_h"));
			String OUTTIME_MINUTE = Util.nullBlankHyphenToNull(recordParams.get("outtime_m"));
			String INTIME = null;
			String MOVETIME = null;
			String OUTTIME = null;
			if(!Util.isNullOrBlankOrHyphen(INTIME_HOUR) && !Util.isNullOrBlankOrHyphen(INTIME_MINUTE)) {
				INTIME = INTIME_HOUR + INTIME_MINUTE;
			}
			if(!Util.isNullOrBlankOrHyphen(MOVETIME_HOUR) && !Util.isNullOrBlankOrHyphen(MOVETIME_MINUTE)) {
				MOVETIME = MOVETIME_HOUR + MOVETIME_MINUTE;
			}
			if(!Util.isNullOrBlankOrHyphen(OUTTIME_HOUR) && !Util.isNullOrBlankOrHyphen(OUTTIME_MINUTE)) {
				OUTTIME = OUTTIME_HOUR + OUTTIME_MINUTE;
			}

			// 値格納
			params1.put("intime", INTIME);
			if(isTimes2Need) {
				params1.put("outtime", MOVETIME);
			} else {
				params1.put("outtime", OUTTIME);
			}
			params1.put("wid", WID);
			params1.put("note", NOTE);
			params1.put("wcount", WCOUNT);
			params1.put("rcount", RCOUNT);
			params1.put("breaktime", BREAKTIME);
			params1.put("fare", FARE);
			if(isTimes2Need) {
				params2 = new HashMap<String, String>();
				params2.put("intime", MOVETIME);
				params2.put("outtime", OUTTIME);
				params2.put("wid", WID_M);
				params2.put("note", NOTE_M);
				params2.put("wcount", WCOUNT_M);
				params2.put("rcount", RCOUNT_M);
			}

			// 実行
			TimeCardDAO dao = new TimeCardDAO();
			json = dao.insertForGrid(OID, UID, TDATE, params1, params2, UNAME, WNAME, WNAME_M);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL005]");
		} catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL006]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * ユーザ情報を更新します。<br>
	 * uuidは現在ページを操作しているユーザのIDです。
	 * @param recordParams
	 * @param uuid
	 * @return
	 */
	public static String execUsersUpdate(Map<String, String> recordParams, String uuid) {
		String json = "";
		try {

			// パラメータ取得
			String OID = Util.nullBlankCheck(recordParams.get("oid"));
			String UID = Util.nullBlankCheck(recordParams.get("uid"));
			String ENABLE = null;
			String LNAME = null;
			String PASS = null;
			String NAME = null;
			String KANA = null;
			String ADMIN = null;
			String MAILLADD = null;
			String CODE = null;
			String MAILENABLE = null;
			String DEPARTMENT = null;
			String EMPLOYMENT = null;
			String WORKLOCATION = null;
			String GROUP = null;
			String HIREDATE = null;
			String PAIDVACATIONDAYS = null;
			String[] change = recordParams.get("change").split(",", 0);	// 変更項目
			String name;
			for(int i=0; i<change.length; i++) {
				name = change[i];
				if(name.equals("enable")) ENABLE = recordParams.get("enable");
				if(name.equals("lname")) LNAME = recordParams.get("lname");
				if(name.equals("pass")) PASS = recordParams.get("pass");
				if(name.equals("name")) NAME = recordParams.get("name");
				if(name.equals("kana")) KANA = recordParams.get("kana");
				if(name.equals("admin")) ADMIN = recordParams.get("admin");
				if(name.equals("mailladd")) MAILLADD = recordParams.get("mailladd");
				if(name.equals("code")) CODE = recordParams.get("code");
				if(name.equals("mailenable")) MAILENABLE = recordParams.get("mailenable");
				if(name.equals("department")) DEPARTMENT = Util.nullBlankHyphenToNull(recordParams.get("department"));
				if(name.equals("employment")) EMPLOYMENT = Util.nullBlankHyphenToNull(recordParams.get("employment"));
				if(name.equals("worklocation")) WORKLOCATION = Util.nullBlankHyphenToNull(recordParams.get("worklocation"));
				if(name.equals("group")) GROUP = Util.nullBlankHyphenToNull(recordParams.get("group"));
				if(name.equals("hiredate")) HIREDATE = Util.nullBlankHyphenToNull(Util.replaceTdate(recordParams.get("hiredate")));
				if(name.equals("paidvacationdays")) PAIDVACATIONDAYS = Util.nullBlankHyphenToNull(recordParams.get("paidvacationdays"));
			}

			// 実行
			BaseDAO.SQLResult result = UsersDAO.update(
					OID, UID, ENABLE, LNAME, PASS,
					NAME, KANA, ADMIN, MAILLADD, CODE, uuid, MAILENABLE,
					DEPARTMENT, EMPLOYMENT, WORKLOCATION, GROUP, HIREDATE, PAIDVACATIONDAYS);
			if (result.Success) {
				json = Util.toSuccessJSON(String.valueOf(result.ExecCount));
			} else {
				json = errorCheck(null);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL007]");
		} catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL008]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * ユーザを削除します。
	 * @param oid
	 * @param uid
	 * @return
	 */
	public static String execUsersDelete(String oid, String uid) {
		String json = "";
		try {

			// 実行
			BaseDAO.SQLResult result = UsersDAO.delete(oid, uid);
			if (result.Success) {
				json = Util.toSuccessJSON(String.valueOf(result.ExecCount));
			} else {
				json = errorCheck(null);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL009]");
		} catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL010]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * ユーザを追加します。<br>
	 * uuidは操作主のuidです。
	 * @param oid
	 * @param uuid
	 * @param recordParams
	 * @return
	 */
	public static String execUsersInsert(String oid, String uuid, Map<String, String> recordParams) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);
			String LNAME = Util.nullBlankCheck(recordParams.get("lname"));
			String PASS = Util.nullBlankCheck(recordParams.get("pass"));
			String NAME = Util.nullBlankCheck(recordParams.get("name"));
			String KANA = Util.nullBlankCheck(recordParams.get("kana"));
			String ADMIN = Util.nullBlankCheck(recordParams.get("admin"));
			String CODE = Util.nullBlankCheck(recordParams.get("code"));
			String MAILLADD = Util.nullBlankCheck(recordParams.get("mailladd"));
			String UUID = Util.nullBlankCheck(uuid);
			String MAILENABLE = Util.nullBlankHyphenToNull(recordParams.get("mailenable"));
			String DEPARTMENT = Util.nullBlankHyphenToNull(recordParams.get("department"));
			String EMPLOYMENT = Util.nullBlankHyphenToNull(recordParams.get("employment"));
			String WORKLOCATION = Util.nullBlankHyphenToNull(recordParams.get("worklocation"));
			String GROUP = Util.nullBlankHyphenToNull(recordParams.get("group"));
			String HIREDATE = Util.nullBlankHyphenToNull(Util.replaceTdate(recordParams.get("hiredate")));
			String PAIDVACATIONDAYS = Util.nullBlankHyphenToNull(recordParams.get("paidvacationdays"));

			// 実行
			BaseDAO.SQLResult result = UsersDAO.insert(
					OID, LNAME, PASS, NAME, KANA, ADMIN, CODE, MAILLADD, UUID, MAILENABLE,
					DEPARTMENT, EMPLOYMENT, WORKLOCATION, GROUP, HIREDATE, PAIDVACATIONDAYS);
			if (result.Success) {
				json = Util.toSuccessJSON(String.valueOf(result.ExecCount));
			} else {
				json = errorCheck(null);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL009]");
		} catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL010]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * NFCを削除します。
	 * @param recordParams
	 * @return
	 */
	public static String execNfcDelete(Map<String, String> recordParams) {
		String json = "";
		try {

			// パラメータ取得
			String OID = Util.nullBlankCheck(recordParams.get("oid"));
			String UID = Util.nullBlankCheck(recordParams.get("uid"));
			String deleteidm = recordParams.get("delete-idm");
			String[] IDMS = null;
			if(deleteidm != null) IDMS = deleteidm.split(",", 0);

			// 実行
			NfcDAO dao = new NfcDAO();
			json = dao.delete(OID, UID, IDMS);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL011]");
		} catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL012]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * IDM待機状態を解除します。
	 * @param recordParams
	 * @return
	 */
	public static String execNfcwaitDelete(Map<String, String> recordParams) {
		String json = "";
		try {

			// パラメータ取得
			String OID = Util.nullBlankCheck(recordParams.get("oid"));
			String UID = Util.nullBlankCheck(recordParams.get("uid"));

			// 実行
			NfcwaitDAO dao = new NfcwaitDAO();
			json = dao.delete(OID, UID);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL013]");
		} catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL014]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * IDM待機状態にします。<br>
	 * iuidは操作主のuidです。
	 * @param recordParams
	 * @param iuid
	 * @return
	 */
	public static String execNfcwaitInsert(Map<String, String> recordParams, String iuid) {
		String json = "";
		try {

			// パラメータ取得
			String OID = Util.nullBlankCheck(recordParams.get("oid"));
			String UID = Util.nullBlankCheck(recordParams.get("uid"));
			String IUID = Util.nullBlankCheck(iuid);

			// 実行
			NfcwaitDAO dao = new NfcwaitDAO();
			json = dao.insert(OID, UID, IUID);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL015]");
		} catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL016]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 業務種別を更新します。<br>
	 * uuidは更新を行ったユーザのIDとなります。
	 * @param recordParams
	 * @param uuid
	 * @return
	 */
	public static String execWorktypeUpdate(Map<String, String> recordParams, String uuid) {
		String json = "";
		try {

			// パラメータ取得
			String OID = Util.nullBlankCheck(recordParams.get("oid"));
			String WID = Util.nullBlankCheck(recordParams.get("wid"));
			String ENABLE = null;
			String NAME = null;
			String KANA = null;
			String CUSTOMER = null;
			String CATEGORY = null;
			String NOTE = null;
			String MANAGER = null;
			String PROJECTCODE = null;
			String[] change = recordParams.get("change").split(",", 0);	// 変更項目
			String name;
			for(int i=0; i<change.length; i++) {
				name = change[i];
				if(name.equals("enable")) ENABLE = recordParams.get("enable");
				if(name.equals("name")) NAME = recordParams.get("name");
				if(name.equals("kana")) KANA = recordParams.get("kana");
				if(name.equals("customer")) CUSTOMER = Util.nullBlankHyphenToNull(recordParams.get("customer"));
				if(name.equals("category")) CATEGORY = Util.nullBlankHyphenToNull(recordParams.get("category"));
				if(name.equals("note")) NOTE = recordParams.get("note");
				if(name.equals("manager")) MANAGER = Util.nullBlankHyphenToNull(recordParams.get("manager"));
				if(name.equals("projectcode")) PROJECTCODE = recordParams.get("projectcode");
			}

			// 実行
			BaseDAO.SQLResult result = WorktypeDAO.update(
					OID, WID, ENABLE, NAME, KANA, uuid, CUSTOMER, CATEGORY, NOTE, Util.toInt(MANAGER), PROJECTCODE);
			if (result.Success) {
				json = Util.toSuccessJSON(String.valueOf(result.ExecCount));
			} else {
				json = errorCheck(null);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL017]");
		} catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL018]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 業務種別を追加します。<br>
	 * uuidは操作主のuidです。
	 * @param oid
	 * @param iuid
	 * @param recordParams
	 * @return
	 */
	public static String execWorktypeInsert(String oid, String iuid, Map<String, String> recordParams) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);
			String NAME = Util.nullBlankCheck(recordParams.get("name"));
			String KANA = Util.nullBlankCheck(recordParams.get("kana"));
			String IUID = Util.nullBlankCheck(iuid);
			String CUSTOMER = Util.nullBlankHyphenToNull(recordParams.get("customer"));
			String CATEGORY = Util.nullBlankHyphenToNull(recordParams.get("category"));
			String NOTE = Util.nullBlankHyphenToNull(recordParams.get("note"));
			String MANAGER = Util.nullBlankHyphenToNull(recordParams.get("manager"));
			String PROJECTCODE = Util.nullBlankCheck(recordParams.get("projectcode"));

			// 実行
			BaseDAO.SQLResult result = WorktypeDAO.insert(OID, NAME, KANA, IUID, CUSTOMER, CATEGORY, NOTE, Util.toInt(MANAGER), PROJECTCODE);
			if (result.Success) {
				json = Util.toSuccessJSON(String.valueOf(result.ExecCount));
			} else {
				json = errorCheck(null);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL019]");
		} catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL020]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 端末受付待機状態にします。<br>
	 * このメソッドはあくまで待機状態です。実際に登録するには専用アプリが必要です。
	 * @param oid
	 * @param recordParams
	 * @return
	 */
	public static String execUnitInsert(String oid, Map<String, String> recordParams) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);
			String PASS = Util.nullBlankCheck(recordParams.get("pass"));

			UnitDAO dao = new UnitDAO();
			json = dao.execUnitWait(OID, PASS);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL021]");
		} catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL022]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 端末情報を更新します。<br>
	 * @param oid
	 * @param recordParams
	 * @return
	 */
	public static String execUnitUpdate(String oid, Map<String, String> recordParams) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);
			String UNIDID = Util.nullBlankCheck(recordParams.get("unitid"));
			String NAME = null;
			String PASS = null;
			String ENABLE = null;
			String NOTE = null;

			String[] change = recordParams.get("change").split(",", 0);	// 変更項目
			String name;
			for(int i=0; i<change.length; i++) {
				name = change[i];

				if(name.equals("name")) NAME = recordParams.get("name");
				if(name.equals("pass")) PASS = recordParams.get("pass");
				if(name.equals("enable")) ENABLE = recordParams.get("enable");
				if(name.equals("note")) {
					NOTE = Util.replaceCRLF(recordParams.get("note"));	// 改行は許可しない
				}
			}

			UnitDAO dao = new UnitDAO();
			json = dao.update(OID, UNIDID, NAME, PASS, ENABLE, NOTE);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL023]");
		} catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL024]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 端末情報を削除します。<br>
	 * 通常は使用しないで下さい。待機状態のものを削除する場合のみ利用するようにして下さい。
	 * @param oid
	 * @param recordParams
	 * @return
	 */
	public static String execUnitDelete(String oid, Map<String, String> recordParams) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);
			String UNITID = Util.nullBlankCheck(recordParams.get("unitid"));

			UnitDAO dao = new UnitDAO();
			json = dao.delete(OID, UNITID);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL025]");
		} catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL026]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 部署を追加します。
	 * @param oid
	 * @param recordParams
	 * @return
	 */
	public static String execDepartmentInsert(String oid, Map<String, String> recordParams) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			String NAME = Util.nullBlankCheck(recordParams.get("name"));
			String KANA = Util.nullBlankCheck(recordParams.get("kana"));
			int MANAGER = Util.toInt(recordParams.get("manager"));
			boolean ENABLE = true;
			String NOTE = Util.replaceCRLF(recordParams.get("note"));
			int IDATE = DateTime.GetTodayInt();

			json = errorCheck(null);
			if (OID == -1 || NAME == null || KANA == null || MANAGER == -1 || NOTE == null) {
				return json;
			}

			BaseDAO.SQLResult sr = DepartmentDAO.Insert(OID, NAME, KANA, MANAGER, ENABLE, NOTE, IDATE);
			if (sr.Success) json = Util.toSuccessJSON(String.valueOf(sr.ExecCount));
		}
		catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL027]");
		}
		catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL028]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 部署を更新します。
	 * @param recordParams
	 * @return
	 */
	public static String execDepartmentUpdate(Map<String, String> recordParams) {
		String json = "";
		try {
			int DEPARTMENTID = Util.toInt(recordParams.get("departmentid"));
			String NAME = Util.nullToBlank(recordParams.get("name"));
			String KANA = Util.nullToBlank(recordParams.get("kana"));
			int MANAGER = Util.toInt(recordParams.get("manager"));
			boolean ENABLE = Util.toBoolean(recordParams.get("enable"));
			String NOTE = recordParams.get("note");
			int UDATE = DateTime.GetTodayInt();

			BaseDAO.SQLResult sr = DepartmentDAO.Update(DEPARTMENTID, NAME, KANA, MANAGER, ENABLE, NOTE, UDATE);
			if (sr.Success) {
				json = Util.toSuccessJSON(String.valueOf(sr.ExecCount));
			} else {
				json = errorCheck(null);
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL029]");
		}
		catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL030]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * カテゴリーを追加します。
	 * @param oid
	 * @param recordParams
	 * @return
	 */
	public static String execCategoryInsert(String oid, Map<String, String> recordParams) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			String NAME = Util.nullBlankCheck(recordParams.get("name"));
			String KANA = Util.nullBlankCheck(recordParams.get("kana"));
			boolean ENABLE = true;
			String NOTE = Util.replaceCRLF(recordParams.get("note"));
			int IDATE = DateTime.GetTodayInt();

			json = errorCheck(null);
			if (OID == -1 || NAME == null || KANA == null || NOTE == null) {
				return json;
			}

			BaseDAO.SQLResult sr = CategoryDAO.Insert(OID, NAME, KANA, ENABLE, NOTE, IDATE);
			if (sr.Success) json = Util.toSuccessJSON(String.valueOf(sr.ExecCount));
		}
		catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL031]");
		}
		catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL032]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * カテゴリーを更新します。
	 * @param recordParams
	 * @return
	 */
	public static String execCategoryUpdate(Map<String, String> recordParams) {
		String json = "";
		try {
			int CATEGORYID = Util.toInt(recordParams.get("categoryid"));
			String NAME = Util.nullToBlank(recordParams.get("name"));
			String KANA = Util.nullToBlank(recordParams.get("kana"));
			boolean ENABLE = Util.toBoolean(recordParams.get("enable"));
			String NOTE = recordParams.get("note");
			int UDATE = DateTime.GetTodayInt();

			BaseDAO.SQLResult sr = CategoryDAO.Update(CATEGORYID, NAME, KANA, ENABLE, NOTE, UDATE);
			if (sr.Success) {
				json = Util.toSuccessJSON(String.valueOf(sr.ExecCount));
			} else {
				json = errorCheck(null);
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL033]");
		}
		catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL034]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 客先を追加します。
	 * @param oid
	 * @param recordParams
	 * @return
	 */
	public static String execCustomerInsert(String oid, Map<String, String> recordParams) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			String NAME = Util.nullBlankCheck(recordParams.get("name"));
			String KANA = Util.nullBlankCheck(recordParams.get("kana"));
			boolean ENABLE = true;
			String NOTE = Util.replaceCRLF(recordParams.get("note"));
			int IDATE = DateTime.GetTodayInt();

			json = errorCheck(null);
			if (OID == -1 || NAME == null || KANA == null || NOTE == null) {
				return json;
			}

			BaseDAO.SQLResult sr = CustomerDAO.Insert(OID, NAME, KANA, ENABLE, NOTE, IDATE);
			if (sr.Success) json = Util.toSuccessJSON(String.valueOf(sr.ExecCount));
		}
		catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL035]");
		}
		catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL036]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 客先を更新します。
	 * @param recordParams
	 * @return
	 */
	public static String execCustomerUpdate(Map<String, String> recordParams) {
		String json = "";
		try {
			int CUSTOMERID = Util.toInt(recordParams.get("customerid"));
			String NAME = Util.nullToBlank(recordParams.get("name"));
			String KANA = Util.nullToBlank(recordParams.get("kana"));
			boolean ENABLE = Util.toBoolean(recordParams.get("enable"));
			String NOTE = recordParams.get("note");
			int UDATE = DateTime.GetTodayInt();

			BaseDAO.SQLResult sr = CustomerDAO.Update(CUSTOMERID, NAME, KANA, ENABLE, NOTE, UDATE);
			if (sr.Success) {
				json = Util.toSuccessJSON(String.valueOf(sr.ExecCount));
			} else {
				json = errorCheck(null);
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL037]");
		}
		catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL038]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 雇用形態を追加します。
	 * @param oid
	 * @param recordParams
	 * @return
	 */
	public static String execEmploymentInsert(String oid, Map<String, String> recordParams) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			String NAME = Util.nullBlankCheck(recordParams.get("name"));
			String KANA = Util.nullBlankCheck(recordParams.get("kana"));
			boolean ENABLE = true;
			String NOTE = Util.replaceCRLF(recordParams.get("note"));
			int IDATE = DateTime.GetTodayInt();

			json = errorCheck(null);
			if (OID == -1 || NAME == null || KANA == null || NOTE == null) {
				return json;
			}

			BaseDAO.SQLResult sr = EmploymentDAO.Insert(OID, NAME, KANA, ENABLE, NOTE, IDATE);
			if (sr.Success) json = Util.toSuccessJSON(String.valueOf(sr.ExecCount));
		}
		catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL039]");
		}
		catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL040]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 雇用形態を更新します。
	 * @param recordParams
	 * @return
	 */
	public static String execEmploymentUpdate(Map<String, String> recordParams) {
		String json = "";
		try {
			int EMPLOYMENTID = Util.toInt(recordParams.get("employmentid"));
			String NAME = Util.nullToBlank(recordParams.get("name"));
			String KANA = Util.nullToBlank(recordParams.get("kana"));
			boolean ENABLE = Util.toBoolean(recordParams.get("enable"));
			String NOTE = recordParams.get("note");
			int UDATE = DateTime.GetTodayInt();

			BaseDAO.SQLResult sr = EmploymentDAO.Update(EMPLOYMENTID, NAME, KANA, ENABLE, NOTE, UDATE);
			if (sr.Success) {
				json = Util.toSuccessJSON(String.valueOf(sr.ExecCount));
			} else {
				json = errorCheck(null);
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL041]");
		}
		catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL042]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 勤務地を追加します。
	 * @param oid
	 * @param recordParams
	 * @return
	 */
	public static String execWorklocationInsert(String oid, Map<String, String> recordParams) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			String NAME = Util.nullBlankCheck(recordParams.get("name"));
			String KANA = Util.nullBlankCheck(recordParams.get("kana"));
			boolean ENABLE = true;
			String NOTE = Util.replaceCRLF(recordParams.get("note"));
			int IDATE = DateTime.GetTodayInt();

			json = errorCheck(null);
			if (OID == -1 || NAME == null || KANA == null || NOTE == null) {
				return json;
			}

			BaseDAO.SQLResult sr = WorklocationDAO.Insert(OID, NAME, KANA, ENABLE, NOTE, IDATE);
			if (sr.Success) json = Util.toSuccessJSON(String.valueOf(sr.ExecCount));
		}
		catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL043]");
		}
		catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL044]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 勤務地を更新します。
	 * @param recordParams
	 * @return
	 */
	public static String execWorklocationUpdate(Map<String, String> recordParams) {
		String json = "";
		try {
			int WORKLOCATIONID = Util.toInt(recordParams.get("worklocationid"));
			String NAME = Util.nullToBlank(recordParams.get("name"));
			String KANA = Util.nullToBlank(recordParams.get("kana"));
			boolean ENABLE = Util.toBoolean(recordParams.get("enable"));
			String NOTE = recordParams.get("note");
			int UDATE = DateTime.GetTodayInt();

			BaseDAO.SQLResult sr = WorklocationDAO.Update(WORKLOCATIONID, NAME, KANA, ENABLE, NOTE, UDATE);
			if (sr.Success) {
				json = Util.toSuccessJSON(String.valueOf(sr.ExecCount));
			} else {
				json = errorCheck(null);
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL045]");
		}
		catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL046]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * グループを追加します。
	 * @param oid
	 * @param recordParams
	 * @return
	 */
	public static String execGroupInsert(String oid, Map<String, String> recordParams) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			String NAME = Util.nullBlankCheck(recordParams.get("name"));
			String KANA = Util.nullBlankCheck(recordParams.get("kana"));
			boolean ENABLE = true;
			String NOTE = Util.replaceCRLF(recordParams.get("note"));
			int IDATE = DateTime.GetTodayInt();

			json = errorCheck(null);
			if (OID == -1 || NAME == null || KANA == null || NOTE == null) {
				return json;
			}

			BaseDAO.SQLResult sr = GroupDAO.Insert(OID, NAME, KANA, ENABLE, NOTE, IDATE);
			if (sr.Success) json = Util.toSuccessJSON(String.valueOf(sr.ExecCount));
		}
		catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL047]");
		}
		catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL048]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * グループを更新します。
	 * @param recordParams
	 * @return
	 */
	public static String execGroupUpdate(Map<String, String> recordParams) {
		String json = "";
		try {
			int GROUPID = Util.toInt(recordParams.get("groupid"));
			String NAME = Util.nullToBlank(recordParams.get("name"));
			String KANA = Util.nullToBlank(recordParams.get("kana"));
			boolean ENABLE = Util.toBoolean(recordParams.get("enable"));
			String NOTE = recordParams.get("note");
			int UDATE = DateTime.GetTodayInt();

			BaseDAO.SQLResult sr = GroupDAO.Update(GROUPID, NAME, KANA, ENABLE, NOTE, UDATE);
			if (sr.Success) {
				json = Util.toSuccessJSON(String.valueOf(sr.ExecCount));
			} else {
				json = errorCheck(null);
			}
		}
		catch (SQLException ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL049]");
		}
		catch(Exception ex) {
			ex.printStackTrace();Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:UL050]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * nullの場合はJSON形式のエラーメッセージを返します。
	 * @param json
	 * @return
	 */
	private static String errorCheck(String json) {
		if(json == null) {
			return Util.toErrorJSON("必要なデータを取得できませんでした。もしくはデータ解析中にエラーが発生しました。[CODE:UL]");
		}
		return json;
	}

}
