package model;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.FreeeAuthAPI.LinkInfo;
import util.API;
import util.Log;

// Freeeの人事労務APIに対応
public class FreeeAPI {
	static final String API_URL = "https://api.freee.co.jp/hr/";

	static final String ENDPOINT_ME = "api/v1/users/me";
	static final String ENDPOINT_EMPLOYEES = "api/v1/employees";
	static final String ENDPOINT_WORKRECORD = "work_record_summaries";
	static final String ENDPOINT_PAYROLL = "api/v1/salaries/employee_payroll_statements";

	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class Company {
		public int id;
		public String name;
	}

	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class MeResponse {
		public int id;
		public Company[] companies;
	}

	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class Employee {
		public int id;
		public String num;
	}

	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class EmployeeResponse {
		public int total_count;
		public Employee[] employees;
	}

	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class WorkRecordSummary {
		public int total_work_mins;
	}

	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class PayrollStatement {
		public int employee_id;
		public String employee_num;
		public String total_taxable_payment_amount;		// 課税対象支給額
		public DeductionPair[] deductions;
	}

	public static class DeductionPair {
		public String name;
		public String amount;
	}

	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class PayrollStatementListResponse {
		public PayrollStatement[] employee_payroll_statements;
	}

	@JsonIgnoreProperties(ignoreUnknown=true)
	public static class PayrollStatementResponse {
		public PayrollStatement employee_payroll_statement;
	}

	// me
	public static MeResponse getCompany() {
		try {
			URL url = new URL(API_URL + ENDPOINT_ME);
			LinkInfo link = FreeeAuthAPI.getActiveLinkInfo();
			String json = API.get(url, link.token.access);
			ObjectMapper mapper = new ObjectMapper();
			MeResponse data = mapper.readValue(json, MeResponse.class);
			return data;
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		}
	}

	// employee
	public static Employee[] getEmployeeList(int year, int month) {
		try {
			LinkInfo link = FreeeAuthAPI.getActiveLinkInfo();

			int page = 0;
			int per = 100;
			int total = 0;

			List<Employee> list = new ArrayList<Employee>();
			do {
				page += 1;
				EmployeeResponse result = getEmployeeList(link.token.access, link.company, year, month, page, per);
				for (Employee obj : result.employees) {
					list.add(obj);
				}
				total = result.total_count;
			} while (page * per < total);

			return list.toArray(new Employee[0]);
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		}
	}

	private static EmployeeResponse getEmployeeList(String token, int company, int year, int month, int page, int per) throws Exception {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("company_id", String.valueOf(company));
		map.put("year", String.valueOf(year));
		map.put("month", String.valueOf(month));
		map.put("per", String.valueOf(per));
		map.put("page", String.valueOf(page));

		String param = API.mapToQueryString(map);
		URL url = new URL(API_URL + ENDPOINT_EMPLOYEES + param);
		String json = API.get(url, token);
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, EmployeeResponse.class);
	}

	// work_record_summaries
	public static WorkRecordSummary getWorkRecordSummary(int year, int month, int employee) {
		try {
			LinkInfo link = FreeeAuthAPI.getActiveLinkInfo();
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("company_id", String.valueOf(link.company));
			String param = API.mapToQueryString(map);

			String endpoint = API_URL + ENDPOINT_EMPLOYEES + "/" + String.valueOf(employee)
				 + "/" + ENDPOINT_WORKRECORD + "/" + String.valueOf(year) + "/" + String.valueOf(month);

			URL url = new URL(endpoint + param);
			String json = API.get(url, link.token.access);
			ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(json, WorkRecordSummary.class);
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		}
	}

	// employee_payroll_statements
	public static PayrollStatement getPayrollStatement(int year, int month, int employee) {
		try {
			LinkInfo link = FreeeAuthAPI.getActiveLinkInfo();

			HashMap<String, String> map = new HashMap<String, String>();
			map.put("company_id", String.valueOf(link.company));
			map.put("year", String.valueOf(year));
			map.put("month", String.valueOf(month));

			String param = API.mapToQueryString(map);
			URL url = new URL(API_URL + ENDPOINT_PAYROLL + "/" + String.valueOf(employee) + param);
			String json = API.get(url, link.token.access);
			ObjectMapper mapper = new ObjectMapper();
			PayrollStatementResponse result = mapper.readValue(json, PayrollStatementResponse.class);
			return result.employee_payroll_statement;
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		}
	}

	// employee_payroll_statements(一覧)
	public static PayrollStatement[] getPayrollStatementList(int year, int month) {
		try {
			LinkInfo link = FreeeAuthAPI.getActiveLinkInfo();

			int page = 0;
			int per = 100;

			List<PayrollStatement> list = new ArrayList<PayrollStatement>();
			while (true) {
				page += 1;
				PayrollStatementListResponse result = getPayrollStatementList(link.token.access, link.company, year, month, page, per);

				if (result.employee_payroll_statements.length == 0) {
					break;
				}

				for (PayrollStatement obj : result.employee_payroll_statements) {
					list.add(obj);
				}
			}

			return list.toArray(new PayrollStatement[0]);
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		}
	}

	private static PayrollStatementListResponse getPayrollStatementList(String token, int company, int year, int month, int page, int per) throws Exception {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("company_id", String.valueOf(company));
		map.put("year", String.valueOf(year));
		map.put("month", String.valueOf(month));
		map.put("per", String.valueOf(per));
		map.put("page", String.valueOf(page));

		String param = API.mapToQueryString(map);
		URL url = new URL(API_URL + ENDPOINT_PAYROLL + param);
		String json = API.get(url, token);
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, PayrollStatementListResponse.class);
	}

}
