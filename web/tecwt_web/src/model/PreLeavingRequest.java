package model;

import java.sql.ResultSet;
import java.util.Arrays;

import db.DBPreparedStatementConnect;
import db.DBinfo;
import util.DateTime;
import util.ErrorMessage;
import util.Log;
import util.Util;

// NFC退勤打刻専用
// （NFC読み取り時に送信）
public class PreLeavingRequest {
    public static class WorklistResult {
        public WorklistResponse data;
        public ErrorMessage error;
    }

    public int oid;
    public String unitid;
    public String pass;

    public String idm;

    private static final String unitTable = DBinfo.TABLE_UNIT;
    private static final String logTable = DBinfo.TABLE_LOG;

    public ErrorMessage login() {
        DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();

        String sql = "SELECT name, pass, enable FROM " + unitTable + " as unit "
            + "WHERE oid = ? AND unitid = ?;";
        ResultSet rs = db.execQuery(sql, Arrays.asList(oid, unitid));

        try {
            if (rs.next()) {
			    if (!rs.getBoolean("enable")) {
			        return ErrorMessage.unit_14;
			    }

			    if (!rs.getString("pass").equals(pass)) {
			        return ErrorMessage.unit_15;
			    }
			} else {
			    return ErrorMessage.unit_13;
			}
            return null;
        } catch (Exception e) {
			Log.exceptionWrite(e);
			return ErrorMessage.unit_ex;
		} finally {
			db.close();
		}
    }

    public void saveLog(String note) {
		DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();

        String sql = "INSERT INTO " + logTable + " "
            + "(note, idate, itime) VALUES(?, ?, ?)";
        // ここはエラー処理不要
        db.execUpdate(sql,
        	Arrays.asList(note, DateTime.GetTodayInt(), DateTime.GetNowInt()));
        db.close();
    }

    public WorklistResult loadWorkList() {
        WorklistResult result = new WorklistResult();

        if (Util.isNullOrBlank(idm)) {
            result.error = ErrorMessage.timecard_out01;
            return result;
        }

        // 退勤打刻時はデフォルトで当日の日付
        int tdate = DateTime.GetTodayInt();
        WorklistResponse worklist = WorklistResponse.makeFromDB(tdate, idm);

        if (worklist == null) {
            result.error = ErrorMessage.timecard_out02_1;
            return result;
        }

        result.data = worklist;
        return result;
    }
}
