package model;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import table.Category;
import table.Customer;
import table.Department;
import table.Employment;
import table.Group;
import table.TimeCard;
import table.Users;
import table.Worklocation;
import table.Worktype;
import util.DateTime;
import util.Log;
import util.Util;
import db.BaseDAO;
import db.CategoryDAO;
import db.CustomerDAO;
import db.DepartmentDAO;
import db.EmploymentDAO;
import db.GroupDAO;
import db.NfcDAO;
import db.TclogDAO;
import db.TclogDAO.TclogResult;
import db.TimeCardDAO;
import db.TimeCardDAO.TimecardResult;
import db.UnitDAO;
import db.UsersDAO;
import db.WorklocationDAO;
import db.WorktypeDAO;

public class QueryLogic {

	/**
	 * タイムカード一覧を取得します。<br>
	 * @param oid
	 * @param uid
	 * @return
	 */
	public static String getTimeCards(String oid, String uid) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);
			String UID = Util.nullBlankCheck(uid);

			TimeCardDAO dao = new TimeCardDAO();
			json = dao.findForGrid(OID, UID);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL001]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL002]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 業務種別一覧を取得します。<br>
	 * @param oid
	 * @return
	 */
	public static String getWorktypes(String oid, Integer year) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);
			BaseDAO.QueryResult<Worktype> result;

			if (year != null) {
				int y2 = (DateTime.GetTodayInt() / 10000) % 1000;
				y2 += year;
				result = WorktypeDAO.findOfOid(OID, String.valueOf(y2));
			} else {
				result = WorktypeDAO.findOfOid(OID);
			}

			json = errorCheck(result.JSON);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL003]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL004]<br>" + ex.getMessage());
		}

		return json;
	}

	public static String getWorktypesForDepartment(String oid) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);
			BaseDAO.QueryResult<Worktype> result;
			result = WorktypeDAO.findOfOid(OID, "99");

			json = errorCheck(result.JSON);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL003]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL004]<br>" + ex.getMessage());
		}

		return json;
	}


	/**
	 * ユーザ一覧を取得します。<br>
	 * @param oid
	 * @return
	 */
	public static String getUsers(String oid) {
		String json = "";
		try {
			String OID = Util.nullBlankCheck(oid);
			BaseDAO.QueryResult<Users> qr = UsersDAO.FindOfOid(OID);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL005]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL006]<br>" + ex.getMessage());
		}
		return json;
	}

	/**
	 * 登録日時からユーザ一覧を取得します。
	 * @param oid
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static String getUsersOfDateRange(String oid, String date1, String date2) {
		String json = "";
		try {
			String OID = Util.nullBlankCheck(oid);

			String repDate1 = null;
			String repDate2 = null;
			if(date1 != null) repDate1 = Util.replaceTdate(Util.nullBlankCheck(date1));
			if(date2 != null) repDate2 = Util.replaceTdate(Util.nullBlankCheck(date2));

			BaseDAO.QueryResult<Users> qr = UsersDAO.FindOfDateRange(OID, repDate1, repDate2);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL005]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL006]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 業務種別からタイムカードを検索します。
	 * @param oid
	 * @param uid
	 * @param wid
	 * @return
	 */
	public static String getTimeCardOfWorktype(String oid, String uid, String wid) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);
			String UID = Util.nullBlankCheck(uid);
			String WID = Util.nullBlankCheck(wid);

			TimeCardDAO dao = new TimeCardDAO();
			json = dao.findOfWorktypeForGrid(OID, UID, WID);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL007]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL008]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 時間の範囲からタイムカードを検索します。
	 * @param oid
	 * @param uid
	 * @param inH
	 * @param inM
	 * @param moveH
	 * @param moveM
	 * @param outH
	 * @param outM
	 * @return
	 */
	public static String getTimeCardOfTimeRangeForGrid(
			String oid, String uid,
			String inH, String inM,
			String moveH, String moveM,
			String outH, String outM) {
		String json = "";
		try {

			String intime = null;
			String movetime = null;
			String outtime = null;
			if(inH != null && inM != null) {
				if(!inH.equals("--") && !inM.equals("--")) {
					intime = inH + inM;
				}
			}
			if(moveH != null && moveM != null) {
				if(!moveH.equals("--") && !moveM.equals("--")) {
					movetime = moveH + moveM;
				}
			}
			if(outH != null && outM != null) {
				if(!outH.equals("--") && !outM.equals("--")) {
					outtime = outH + outM;
				}
			}

			TimeCardDAO dao = new TimeCardDAO();
			json = dao.findOfTimeRangeForGrid(oid, uid, intime, movetime, outtime);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL009]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL010]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 日付の範囲からタイムカードを検索します。
	 * @param oid
	 * @param uid
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static String getTimeCardOfDateRangeForGrid(String oid, String uid, String date1, String date2) {
		String json = "";
		try {

			String repDate1 = null;
			String repDate2 = null;
			if(date1 != null) repDate1 = Util.replaceTdate(Util.nullBlankCheck(date1));
			if(date2 != null) repDate2 = Util.replaceTdate(Util.nullBlankCheck(date2));

			TimeCardDAO dao = new TimeCardDAO();
			TimecardResult result = dao.findOfDateRangeForGrid(oid, uid, repDate1, repDate2);
			json = result.ToJSON(false);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL011]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL012]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * ユーザが登録しているNFC一覧を取得します。
	 * @param oid
	 * @param uid
	 * @return
	 */
	public static String getNfcOfUid(String oid, String uid) {
		String json = "";
		try {

			NfcDAO dao = new NfcDAO();
			json = dao.findOfOidOrUid(oid, uid);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL013]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL014]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 指定した社員番号を持つユーザを検索します。
	 * @param code
	 * @return
	 */
	public static String getUsersOfCode(String oid, String code) {
		String json = "";
		try {
			String OID = Util.nullBlankCheck(oid);
			String CODE = Util.nullBlankCheck(code);
			BaseDAO.QueryResult<Users> qr = UsersDAO.FindOfCode(OID, CODE);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL015]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL016]<br>" + ex.getMessage());
		}
		return json;
	}

	/**
	 * 指定したログインIDを持つユーザを検索します。
	 * @param oid
	 * @param lname
	 * @return
	 */
	public static String getUsersOfLname(String oid, String lname) {
		String json = "";
		try {
			String OID = Util.nullBlankCheck(oid);
			String LNAME = Util.nullBlankCheck(lname);
			BaseDAO.QueryResult<Users> qr = UsersDAO.FindOfLname(OID, LNAME);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL017]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL018]<br>" + ex.getMessage());
		}
		return json;
	}

	/**
	 * 指定したユーザ状態を持つユーザを検索します。
	 * @param oid
	 * @param enable
	 * @return
	 */
	public static String getUsersOfEnable(String oid, String enable) {
		String json = "";
		try {
			String OID = Util.nullBlankCheck(oid);
			String ENABLE = Util.nullBlankCheck(enable);
			BaseDAO.QueryResult<Users> qr = UsersDAO.FindOfEnable(OID, ENABLE);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL019]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL020]<br>" + ex.getMessage());
		}
		return json;
	}

	/**
	 * ユーザIDからユーザ情報を取得します。<br>
	 * @param oid
	 * @param uid
	 * @return
	 */
	public static String getUsersOfUid(String oid, String uid) {
		String json = "";
		try {
			String OID = Util.nullBlankCheck(oid);
			String UID = Util.nullBlankCheck(uid);
			BaseDAO.QueryResult<Users> qr = UsersDAO.FindOfUid(OID, UID);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL021]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL022]<br>" + ex.getMessage());
		}
		return json;
	}

	/**
	 * ログインを行います。<br>
	 * 指定したログインIDとパスを持つユーザが存在するかを確認します。<br>
	 * 存在した場合はユーザ情報を返し、存在しない場合はlengthが0になります。<br>
	 * @param oid
	 * @param lname
	 * @param pass
	 * @return
	 */
	public static String execLogin(int oid, String lname, String pass) {
		String json = "";
		try {
			String LNAME = Util.nullBlankCheck(lname);
			String PASS = Util.nullBlankCheck(pass);
			BaseDAO.QueryResult<Users> qr = UsersDAO.ExecLogin(oid, LNAME, PASS);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL023]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL024]<br>" + ex.getMessage());
		}
		return json;
	}

	/**
	 * ログインできているかを判定します。<br>
	 * 引数のjsonはexecLogin()の結果を指定してください。<br>
	 * 戻り値はLoginResultオブジェクトです。<br>
	 * ログイン結果と、ログインできた場合にはoid,uid,そして管理者権限を有するかを判断するadminが入っています。
	 * @param json
	 * @return
	 */
	public static LoginResult isLogin(String json) {

		// ログイン結果とoid,uidを格納するオブジェクト
		LoginResult result = new LoginResult();

		if(json == null) {
			result.isLogin = false;
			return result;
		}

		// JSONからoid,uid,adminを抜き出す
		if(json.contains("\"login\":\"done\"")) {
			result.isLogin = true;
			String tagOid = "\"oid\":";
			String tagUid = "\"uid\":";
			String tagAdmin = "\"admin\":";
			result.oid = Util.jsonSubstring(json, tagOid);
			result.uid = Util.jsonSubstring(json, tagUid);
			result.admin = Util.jsonSubstring(json, tagAdmin);
			return result;
		}

		result.isLogin = false;
		return result;
	}
	public static class LoginResult {
		public boolean isLogin;
		public String oid;
		public String uid;
		public String admin;
	}

	/**
	 * 作成日付の範囲から業務種別一覧を検索します。
	 * @param oid
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static String getWorktypeOfDateRange(String oid, String date1, String date2) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);

			String repDate1 = null;
			String repDate2 = null;
			if(date1 != null) repDate1 = Util.replaceTdate(Util.nullBlankCheck(date1));
			if(date2 != null) repDate2 = Util.replaceTdate(Util.nullBlankCheck(date2));

			BaseDAO.QueryResult<Worktype> qr = WorktypeDAO.findOfDateRange(OID, repDate1, repDate2);
			json = errorCheck(qr.JSON);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL025]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL026]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 指定したユーザ状態を持つユーザを検索します。
	 * @param oid
	 * @param enable
	 * @return
	 */
	public static String getWorktypeOfEnable(String oid, String enable) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);
			String ENABLE = Util.nullBlankCheck(enable);

			BaseDAO.QueryResult<Worktype> qr = WorktypeDAO.findOfEnable(OID, ENABLE);
			json = errorCheck(qr.JSON);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL027]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL028]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 閲覧可能な(凍結していない)ユーザを取得します。
	 * @param oid
	 * @return
	 */
	public static String getUsersOfEnable(String oid) {
		String json = "";
		try {
			String OID = Util.nullBlankCheck(oid);
			BaseDAO.QueryResult<Users> qr = UsersDAO.FindOfEnable(OID);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL029]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL030]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 閲覧可能な(凍結していない)業務種別を取得します。
	 * @param oid
	 * @return
	 */
	public static String getWorktypesOfEnable(String oid) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);

			BaseDAO.QueryResult<Worktype> qr = WorktypeDAO.findOfEnable(OID);
			json = errorCheck(qr.JSON);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL031]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL032]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * ユーザIDと日付からタイムカードを特定します。<br>
	 * 取得できるレコードは最大で２となります。<br>
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @return
	 */
	public static String getTimeCardOfUidAndTdateForGrid(String oid, String uid, String tdate) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);
			String UID = Util.nullBlankCheck(uid);
			String TDATE = Util.replaceTdate(Util.nullBlankCheck(tdate));

			TimeCardDAO dao = new TimeCardDAO();
			json = dao.findOfUidAndTdateForGrid(OID, UID, TDATE);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL033]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL034]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 単一のタイムカードに関連付いたログ一覧を取得します。<br>
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @return
	 */
	public static String getTclogOfUidAndTdate(String oid, String uid, String tdate) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);
			String UID = Util.nullBlankCheck(uid);
			String TDATE = Util.replaceTdate(Util.nullBlankCheck(tdate));

			TclogDAO dao = new TclogDAO();
			TclogResult result = dao.findOfUidAndTdate(OID, UID, TDATE);
			json = result.JSON;

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL035]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL036]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 本日タイムカードが押されているタイムカード一覧を取得します。
	 * @param oid
	 * @return
	 */
	public static String getTimeCardOfNowDateForGrid(String oid) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);
			String NOWDATE = DateTime.GetTodayStr();

			TimeCardDAO dao = new TimeCardDAO();
			TimecardResult result = dao.findOfDateRangeForGrid(OID, null, NOWDATE, NOWDATE);
			json = result.ToJSON(true);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL037]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL038]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 本日タイムカードが押されているユーザ一覧を取得します。
	 * @param oid
	 * @return
	 */
	public static String getUsersOfToday(String oid) {
		String json = "";
		try {
			String OID = Util.nullBlankCheck(oid);
			String NOWDATE = DateTime.GetTodayStr();
			// 本日打刻タイムカード取得
			TimeCardDAO dao = new TimeCardDAO();
			TimecardResult result = dao.getListOfToday(OID, NOWDATE);
			List<TimeCard> list = result.List;
			// 出勤者いなければすぐ返す
			if(list != null && list.size() == 0) {
				return UsersDAO.ToJSON(new ArrayList<Users>(), Users.class, null);
			}
			// ユーザIDのみ抽出
			List<String> users = new ArrayList<String>();
			for(TimeCard tc : list) {
				users.add(String.valueOf(tc.getUid()));
			}
			// ユーザ情報取得
			BaseDAO.QueryResult<Users> qr = UsersDAO.FindOfUids(oid, users);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL039]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL040]<br>" + ex.getMessage());
		}
		return json;
	}

	/**
	 * 本日付けのタイムカードに記載されている業務種別一覧を取得します。
	 * @param oid
	 * @return
	 */
	public static String getWorktypeOfToday(String oid) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);
			String NOWDATE = DateTime.GetTodayStr();

			// 本日打刻タイムカード取得
			TimeCardDAO dao = new TimeCardDAO();
			TimecardResult result = dao.getListOfToday(OID, NOWDATE);
			List<TimeCard> list = result.List;

			// 出勤者いなければすぐ返す
			if(list != null && list.size() == 0) {
				return BaseDAO.ToJSON(new ArrayList<Worktype>(), Worktype.class, null);
			}

			// 業務種別IDのみ抽出
			List<String> worktypes = new ArrayList<String>();
			for(TimeCard tc : list) {
				worktypes.add(String.valueOf(tc.getWid()));
				if(tc.getMove_wid() != 0) {
					worktypes.add(String.valueOf(tc.getMove_wid()));
				}
			}

			// 業務種別情報取得
			BaseDAO.QueryResult<Worktype> qr = WorktypeDAO.findOfWids(oid, worktypes);
			json = errorCheck(qr.JSON);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL041]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL042]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 管理端末一覧を取得します。
	 * @param oid
	 * @return
	 */
	public static String getUnits(String oid) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);

			UnitDAO dao = new UnitDAO();
			json = dao.findOfOid(OID);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL043]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL044]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 指定した端末IDを持つ端末情報を取得します。<br>
	 * このメソッドは重複確認にも利用できるでしょう。
	 * @param oid
	 * @param unitid
	 * @return
	 */
	public static String getUnitOfUnitid(String oid, String unitid) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);
			String UNITID = Util.nullBlankCheck(unitid);

			UnitDAO dao = new UnitDAO();
			json = dao.findOfUnitid(OID, UNITID);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL045]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL046]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 指定した管理状況にある端末一覧を取得します。
	 * @param oid
	 * @param enable
	 * @return
	 */
	public static String getUnitOfEnable(String oid, String enable) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);
			String ENABLE = Util.nullBlankCheck(enable);

			UnitDAO dao = new UnitDAO();
			json = dao.findOfEnable(OID, ENABLE);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL047]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL048]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 登録日時から端末一覧を取得します。
	 * @param oid
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static String getUnitOfDateRange(String oid, String date1, String date2) {
		String json = "";
		try {

			String OID = Util.nullBlankCheck(oid);

			String repDate1 = null;
			String repDate2 = null;
			if(date1 != null) repDate1 = Util.replaceTdate(Util.nullBlankCheck(date1));
			if(date2 != null) repDate2 = Util.replaceTdate(Util.nullBlankCheck(date2));

			UnitDAO dao = new UnitDAO();
			json = dao.findOfDateRange(OID, repDate1, repDate2);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL049]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL050]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 年・月からタイムカードを取得します。<br>
	 * このメソッドは「2015年8月分のタイムカード一覧」などに使用できるでしょう。
	 * @param oid
	 * @param year
	 * @param month
	 * @return
	 */
	public static String getTimeCardOfYearAndMonthForGrid(String oid, String uid, String year, String month) {
		String json = "";
		try {

			String OID = Util.nullBlankHyphenToNull(oid);
			String UID = Util.nullBlankHyphenToNull(uid);
			String YEAR = Util.nullBlankHyphenToNull(year);
			String MONTH = Util.nullBlankHyphenToNull(month);

			TimeCardDAO dao = new TimeCardDAO();
			json = dao.findOfYearAndMonthForGrid(OID, UID, YEAR, MONTH);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL051]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL052]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 今月のタイムカード一覧を取得します。<br>
	 * @param oid
	 * @param uid
	 * @return
	 */
	public static String getTimeCardOfNowMonthForGrid(String oid, String uid) {
		String json = "";
		try {

			// 今月～来月の算出
			Calendar calendar = Calendar.getInstance();
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH) + 1;
			String nowyear = String.valueOf(calendar.get(Calendar.YEAR));
			String nowmonth = String.valueOf(month);
			String nextyear;
			String nextmonth;
			if(month == 12) {
				nextyear = String.valueOf(year + 1);
				nextmonth = "1";
			} else {
				nextyear = String.valueOf(year);
				nextmonth = String.valueOf(month + 1);
			}

			// 0埋め
			if(nowmonth.length() == 1) nowmonth = "0" + nowmonth;
			if(nextmonth.length() == 1) nextmonth = "0" + nextmonth;

			String OID = Util.nullBlankHyphenToNull(oid);
			String UID = Util.nullBlankHyphenToNull(uid);
			String DATE1 = nowyear + nowmonth + "00";
			String DATE2 = nextyear + nextmonth + "00";

			TimeCardDAO dao = new TimeCardDAO();
			TimecardResult result = dao.findOfDateRangeForGrid(OID, UID, DATE1, DATE2);
			json = result.ToJSON(false);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL053]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL054]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * ユーザと日付からタイムカードを取得します。
	 * @param oid
	 * @param uid
	 * @param tdate
	 * @return
	 */
	public static String getTimeCardOfUidAndTdate(String oid, String uid, String tdate) {
		String json = "";
		try {

			String OID = Util.nullBlankHyphenToNull(oid);
			String UID = Util.nullBlankHyphenToNull(uid);
			String TDATE = tdate != null ? tdate.replaceAll("/", "") : null;

			TimeCardDAO dao = new TimeCardDAO();
			json = dao.findOfUidAndTdateForGrid(OID, UID, TDATE);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL055]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL056]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * widから勤務種別を取得します。
	 * @param oid
	 * @param wid
	 * @return
	 */
	public static String getWorktypeOfWid(String oid, String wid) {
		String json = "";
		try {

			String OID = Util.nullBlankHyphenToNull(oid);
			String WID = Util.nullBlankHyphenToNull(wid);

			List<String> wids = new ArrayList<String>();
			wids.add(WID);
			BaseDAO.QueryResult<Worktype> qr = WorktypeDAO.findOfWids(OID, wids);
			json = errorCheck(qr.JSON);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL057]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL058]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * mailenableからユーザ一覧を取得します。
	 * @param oid
	 * @param mailenable true or false
	 * @return
	 */
	public static String getUsersOfMailenable(String oid, String mailenable) {
		String json = "";
		try {
			String OID = Util.nullBlankHyphenToNull(oid);
			String MAILENABLE = Util.nullBlankHyphenToNull(mailenable);
			BaseDAO.QueryResult<Users> qr = UsersDAO.FindOfMailenable(OID, MAILENABLE);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL059]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL060]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 部署一覧を取得します。
	 * @param oid
	 * @return
	 */
	public static String getDepartments(String oid) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			BaseDAO.QueryResult<Department> qr = DepartmentDAO.GetAll(OID);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL061]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL062]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 指定したIDを持つ部署情報を取得します。
	 * @param oid
	 * @param id
	 * @return
	 */
	public static String getDepartmentOfId(String oid, String id) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			int ID = Util.toInt(id);
			BaseDAO.QueryResult<Department> qr = DepartmentDAO.FindOfId(OID, ID);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL063]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL064]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 指定した管理状況にある部署一覧を取得します。
	 * @param oid
	 * @param enable
	 * @return
	 */
	public static String getDepartmentOfEnable(String oid, String enable) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			boolean ENABLE = Util.toBoolean(enable);
			BaseDAO.QueryResult<Department> qr = DepartmentDAO.FindOfEnable(OID, ENABLE);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL065]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL066]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * カテゴリー一覧を取得します。
	 * @param oid
	 * @return
	 */
	public static String getCategorys(String oid) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			BaseDAO.QueryResult<Category> qr = CategoryDAO.GetAll(OID);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL067]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL068]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 指定したIDを持つカテゴリー情報を取得します。
	 * @param oid
	 * @param id
	 * @return
	 */
	public static String getCategoryOfId(String oid, String id) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			int ID = Util.toInt(id);
			BaseDAO.QueryResult<Category> qr = CategoryDAO.FindOfId(OID, ID);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL069]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL070]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 指定した管理状況にあるカテゴリー一覧を取得します。
	 * @param oid
	 * @param enable
	 * @return
	 */
	public static String getCategoryOfEnable(String oid, String enable) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			boolean ENABLE = Util.toBoolean(enable);
			BaseDAO.QueryResult<Category> qr = CategoryDAO.FindOfEnable(OID, ENABLE);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL071]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL072]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 客先一覧を取得します。
	 * @param oid
	 * @return
	 */
	public static String getCustomers(String oid) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			BaseDAO.QueryResult<Customer> qr = CustomerDAO.GetAll(OID);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL073]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL074]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 指定したIDを持つ客先情報を取得します。
	 * @param oid
	 * @param id
	 * @return
	 */
	public static String getCustomerOfId(String oid, String id) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			int ID = Util.toInt(id);
			BaseDAO.QueryResult<Customer> qr = CustomerDAO.FindOfId(OID, ID);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL075]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL076]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 指定した管理状況にある客先一覧を取得します。
	 * @param oid
	 * @param enable
	 * @return
	 */
	public static String getCustomerOfEnable(String oid, String enable) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			boolean ENABLE = Util.toBoolean(enable);
			BaseDAO.QueryResult<Customer> qr = CustomerDAO.FindOfEnable(OID, ENABLE);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL077]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL078]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 雇用形態一覧を取得します。
	 * @param oid
	 * @return
	 */
	public static String getEmployments(String oid) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			BaseDAO.QueryResult<Employment> qr = EmploymentDAO.GetAll(OID);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL079]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL080]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 指定したIDを持つ雇用形態情報を取得します。
	 * @param oid
	 * @param id
	 * @return
	 */
	public static String getEmploymentOfId(String oid, String id) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			int ID = Util.toInt(id);
			BaseDAO.QueryResult<Employment> qr = EmploymentDAO.FindOfId(OID, ID);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL081]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL082]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 指定した管理状況にある雇用形態一覧を取得します。
	 * @param oid
	 * @param enable
	 * @return
	 */
	public static String getEmploymentOfEnable(String oid, String enable) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			boolean ENABLE = Util.toBoolean(enable);
			BaseDAO.QueryResult<Employment> qr = EmploymentDAO.FindOfEnable(OID, ENABLE);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL083]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL084]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 勤務地一覧を取得します。
	 * @param oid
	 * @return
	 */
	public static String getWorklocations(String oid) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			BaseDAO.QueryResult<Worklocation> qr = WorklocationDAO.GetAll(OID);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL085]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL086]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 指定したIDを持つ勤務地情報を取得します。
	 * @param oid
	 * @param id
	 * @return
	 */
	public static String getWorklocationOfId(String oid, String id) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			int ID = Util.toInt(id);
			BaseDAO.QueryResult<Worklocation> qr = WorklocationDAO.FindOfId(OID, ID);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL087]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL088]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 指定した管理状況にある勤務地一覧を取得します。
	 * @param oid
	 * @param enable
	 * @return
	 */
	public static String getWorklocationOfEnable(String oid, String enable) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			boolean ENABLE = Util.toBoolean(enable);
			BaseDAO.QueryResult<Worklocation> qr = WorklocationDAO.FindOfEnable(OID, ENABLE);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL089]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL090]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * グループ一覧を取得します。
	 * @param oid
	 * @return
	 */
	public static String getGroups(String oid) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			BaseDAO.QueryResult<Group> qr = GroupDAO.GetAll(OID);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL091]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL092]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 指定したIDを持つグループ情報を取得します。
	 * @param oid
	 * @param id
	 * @return
	 */
	public static String getGroupOfId(String oid, String id) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			int ID = Util.toInt(id);
			BaseDAO.QueryResult<Group> qr = GroupDAO.FindOfId(OID, ID);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL093]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL094]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 指定した管理状況にあるグループ一覧を取得します。
	 * @param oid
	 * @param enable
	 * @return
	 */
	public static String getGroupOfEnable(String oid, String enable) {
		String json = "";
		try {
			int OID = Util.toInt(oid);
			boolean ENABLE = Util.toBoolean(enable);
			BaseDAO.QueryResult<Group> qr = GroupDAO.FindOfEnable(OID, ENABLE);
			json = errorCheck(qr.JSON);
		}
		catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL095]");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL096]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 先月のタイムカード一覧を取得します。<br>
	 * @param oid
	 * @param uid
	 * @return
	 */
	public static String getTimeCardOfLastMonthForGrid(String oid, String uid) {
		String json = "";
		try {
			// 先月の算出
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DATE, 1);
			String nowYear = String.format("%04d", calendar.get(Calendar.YEAR));
			String nowMonth = String.format("%02d", calendar.get(Calendar.MONTH) + 1);
			calendar.add(Calendar.MONTH, -1);	//先月
			String lastYear = String.format("%04d", calendar.get(Calendar.YEAR));
			String lastMonth = String.format("%02d", calendar.get(Calendar.MONTH) + 1);

			String OID = Util.nullBlankHyphenToNull(oid);
			String UID = Util.nullBlankHyphenToNull(uid);
			String DATE1 = lastYear + lastMonth + "00";
			String DATE2 = nowYear + nowMonth + "00";

			TimeCardDAO dao = new TimeCardDAO();
			TimecardResult result = dao.findOfDateRangeForGrid(OID, UID, DATE1, DATE2);
			json = result.ToJSON(false);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL097]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL098]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * 来月のタイムカード一覧を取得します。<br>
	 * @param oid
	 * @param uid
	 * @return
	 */
	public static String getTimeCardOfNextMonthForGrid(String oid, String uid) {
		String json = "";
		try {
			// 先月の算出
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.DATE, 1);
			calendar.add(Calendar.MONTH, 1);	//来月
			String nextYear = String.format("%04d", calendar.get(Calendar.YEAR));
			String nextMonth = String.format("%02d", calendar.get(Calendar.MONTH) + 1);
			calendar.add(Calendar.MONTH, 1);	//その翌月
			String nextnextYear = String.format("%04d", calendar.get(Calendar.YEAR));
			String nextnextMonth = String.format("%02d", calendar.get(Calendar.MONTH) + 1);

			String OID = Util.nullBlankHyphenToNull(oid);
			String UID = Util.nullBlankHyphenToNull(uid);
			String DATE1 = nextYear + nextMonth + "00";
			String DATE2 = nextnextYear + nextnextMonth + "00";

			TimeCardDAO dao = new TimeCardDAO();
			TimecardResult result = dao.findOfDateRangeForGrid(OID, UID, DATE1, DATE2);
			json = result.ToJSON(false);

			json = errorCheck(json);

		} catch (SQLException ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("データ交信中にエラーが発生した為、処理を中断しました。[CODE:QL099]");
		} catch(Exception ex) {
			ex.printStackTrace();
			Log.exceptionWrite(ex);
			return Util.toErrorJSON("通信中にエラーが発生した為、データを元の状態に戻しました。[CODE:QL100]<br>" + ex.getMessage());
		}

		return json;
	}

	/**
	 * nullの場合はJSON形式のエラーメッセージを返します。
	 * @param json
	 * @return
	 */
	private static String errorCheck(String json) {
		if(json == null) {
			return Util.toErrorJSON("データ所得中にエラーが発生しました。[CODE:QL]");
		}
		return json;
	}

}
