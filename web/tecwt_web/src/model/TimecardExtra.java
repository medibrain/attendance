package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import table.Tclog;
import table.TimeCard;
import table.Unit;
import util.DateTime;
import util.Util;
import db.TclogDAO;
import db.TclogDAO.TclogResult;
import db.UnitDAO;
import db.UnitDAO.UnitResult;

public class TimecardExtra {

	public String Oid;
	public String JSON;
	public List<TimeCard> TimecardList;
	public Map<TimeCard, Tclog> TclogMap;
	public Map<String, Unit> UnitMap;

	public TimecardExtra(String oid, List<TimeCard> timecardList, String defaultJSON) {
		Oid = oid;
		TimecardList = timecardList;
		JSON = defaultJSON;
	}

	/**
	 * setUnitName:<br>
	 * true=端末名(unitname)を付与します。<br>
	 * false=最初のJSONをそのまま返します。
	 * @return
	 */
	public String ToJSON(boolean setUnitName) {
		if (setUnitName) {
			if (!setUnitNames()) return JSON;
		}
		if (TimecardList == null || TclogMap == null) {
			return JSON;
		}

		// JSON形式に成型
		StringBuilder json = new StringBuilder();

		json.append("{");
		json.append("\"result\":\"success\",");
		json.append("\"datas\":[");
		TimeCard tc;
		for(int i=0; i<TimecardList.size(); i++) {
			tc = TimecardList.get(i);
			if(i > 0) {
				json.append(",");
			}
			json.append("{");
			json.append("\"oid\":\"").append(tc.getOid()).append("\",");
			json.append("\"uid\":\"").append(tc.getUid()).append("\",");
			json.append("\"tdate\":\"").append(Util.toTdate(tc.getTdate())).append("\",");
			json.append("\"times\":\"").append(tc.getTimes()).append("\",");
			json.append("\"inH\":\"").append(Util.getHour(tc.getIntime())).append("\",");
			json.append("\"inM\":\"").append(Util.getMinute(tc.getIntime())).append("\",");
			json.append("\"moveH\":\"").append(Util.getHour(tc.getMovetime())).append("\",");
			json.append("\"moveM\":\"").append(Util.getMinute(tc.getMovetime())).append("\",");
			json.append("\"outH\":\"").append(Util.getHour(tc.getOuttime())).append("\",");
			json.append("\"outM\":\"").append(Util.getMinute(tc.getOuttime())).append("\",");
			json.append("\"breaktime\":\"").append(tc.getBreaktime()).append("\",");
			json.append("\"fare\":\"").append(tc.getFare()).append("\",");
			json.append("\"wid\":\"").append(tc.getWid()).append("\",");
			json.append("\"note\":\"").append(Util.htmlEncode(Util.nullToBlank(tc.getNote()))).append("\",");	// ユーザが自由に決められる箇所はエンコードしておく
			json.append("\"wcount\":\"").append(tc.getWcount()).append("\",");
			json.append("\"rcount\":\"").append(tc.getRcount()).append("\",");
			json.append("\"wid_move\":\"").append(Util.minusOneToHyphen(tc.getMove_wid())).append("\",");
			json.append("\"note_move\":\"").append(Util.htmlEncode(Util.nullToBlank(tc.getMove_note()))).append("\",");
			json.append("\"wcount_move\":\"").append(Util.minusOneToHyphen(tc.getMove_wcount())).append("\",");
			json.append("\"rcount_move\":\"").append(Util.minusOneToHyphen(tc.getMove_rcount())).append("\",");
			json.append("\"oname\":\"").append(Util.htmlEncode(tc.getOname())).append("\",");
			json.append("\"uname\":\"").append(Util.htmlEncode(tc.getUname())).append("\",");
			json.append("\"ukana\":\"").append(Util.htmlEncode(tc.getUkana())).append("\",");
			json.append("\"wname\":\"").append(Util.htmlEncode(tc.getWname())).append("\",");
			json.append("\"wname_move\":\"").append(Util.htmlEncode(tc.getMove_wname())).append("\",");
			json.append("\"dayofweek\":\"").append(DateTime.GetDayOfWeek((tc.getTdate()))).append("\",");
			json.append("\"unitname\":\"").append(this.getUnitName(tc)).append("\"");
			json.append("}");
		}
		json.append("]");
		json.append("}");

		return json.toString();
	}

	private boolean setUnitNames() {
		TclogMap = new HashMap<TimeCard, Tclog>();
		// タイムカードに該当するログ（端末ID）を取得する
		TclogDAO dao = new TclogDAO();
		TclogResult tcr;
		Tclog hpTclog;
		String unitid;
		Map<String, String> uidMap = new HashMap<String, String>();
		for (TimeCard tc : TimecardList) {
			try {
				tcr = dao.findOfUidAndTdate(
						String.valueOf(tc.getOid()),
						String.valueOf(tc.getUid()),
						String.valueOf(tc.getTdate()));
				hpTclog = tcr.List.get(0);//最新レコード
				unitid = hpTclog.getUnitid();
				if (!Util.isNullOrBlank(unitid)) {
					if (!uidMap.containsKey(unitid)) {
						uidMap.put(unitid, unitid);
					}
				}
				TclogMap.put(tc, hpTclog);
			} catch (Exception ex) {
				continue;
			}
		}
		List<String> uidList = new ArrayList<String>();
		for (String uid : uidMap.values()) {
			uidList.add(uid);
		}

		// 端末情報を取得する
		UnitDAO udao = new UnitDAO();
		try {
			UnitResult uResult = udao.FindOfUnitids(Oid, uidList);
			UnitMap = new HashMap<String, Unit>();
			for (Unit unit : uResult.List) {
				UnitMap.put(unit.getUnitid(), unit);
			}
		} catch (Exception ex) {
			return false;
		}

		return true;
	}

	private String getUnitName(TimeCard tc) {
		if (TclogMap.containsKey(tc)) {
			Tclog tclog = TclogMap.get(tc);
			if (UnitMap.containsKey(tclog.getUnitid())) {
				Unit unit = UnitMap.get(tclog.getUnitid());
				return unit.getNote();
			} else {
				return getUnitNameFromTclog(tclog);
			}
		}
		return "<不明な打刻>";
	}

	/**
	 * 端末名を取得します。<br>
	 * 出勤情報のものを優先し、次に退勤、WEBとなります。
	 * @param tc
	 * @return
	 */
	private String getUnitNameFromTclog(Tclog tclog) {
		//まずは出勤情報
		if (tclog.getLogtype().equals("メール 出勤報告")) {
			return "メール";
		}
		//出勤情報がなければ退勤情報で
		if (tclog.getLogtype().equals("メール 退勤報告")) {
			return "メール";
		}
		//退勤情報もなければWEB情報で
		if (tclog.getLogtype().equals("WEB 管理")) {
			return "WEB管理";
		}
		return "<不明な打刻>";
	}

}
