package model;

import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.HashMap;

import db.BaseDAO.QueryResult;
import db.EmploymentDAO;
import db.UsersDAO;
import model.FreeeAPI.DeductionPair;
import model.FreeeAPI.PayrollStatement;
import model.FreeeAPI.WorkRecordSummary;
import table.Employment;
import table.Users;

// FreeeのデータをCSV形式でダウンロードする用
public class FreeeDownload {
	// 月毎の総勤務時間と給与を出力
	public static void outputWorkAndPayrollCSV(PrintWriter writer, String oid, int year, int month) throws SQLException {
		// 社員形態の取得
		HashMap<String, String> types = new HashMap<String, String>();
		QueryResult<Employment> result = EmploymentDAO.GetAll(Integer.parseInt(oid));
		for (Employment item : result.List) {
			types.put(String.valueOf(item.id.IntValue), item.getName());
		}


		String header = "従業員番号,社員区分,総勤務時間,課税対象支給額,健康保険料,介護保険料,厚生年金保険料,雇用保険料,交通費精算\n";
		writer.print(header);
		writer.flush();

		// 給与の一覧を取得
		PayrollStatement[] list = FreeeAPI.getPayrollStatementList(year, month);
		for (PayrollStatement pay : list) {
			Users user = UsersDAO.FindOfCode(oid, pay.employee_num).List.get(0);
			String type = types.get(String.valueOf(user.employment.IntValue));

			// 各従業員ごとの勤務時間を取得
			WorkRecordSummary record = FreeeAPI.getWorkRecordSummary(year, month, pay.employee_id);
			if (record == null) {
				continue;
			}
			String kenkoHoken = "";
			String kaigoHoken = "";
			String nenkinHoken = "";
			String koyoHoken = "";
			String kotsuhi = "";

			for (DeductionPair pair : pay.deductions) {
				switch (pair.name) {
				case "健康保険料":
					kenkoHoken = pair.amount;
					break;
				case "介護保険料":
					kaigoHoken = pair.amount;
					break;
				case "厚生年金保険料":
					nenkinHoken = pair.amount;
					break;
				case "雇用保険料":
					koyoHoken = pair.amount;
					break;
				case "交通費精算":
					kotsuhi = pair.amount;
					break;
				}
			}


			String line = pay.employee_num + "," + type + "," + record.total_work_mins + "," + pay.total_taxable_payment_amount
					 + "," + kenkoHoken + "," + kaigoHoken + "," + nenkinHoken + "," + koyoHoken + "," + kotsuhi
					 + "\n";
			writer.print(line);
			writer.flush();
		}
	}
}
