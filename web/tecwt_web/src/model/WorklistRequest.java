package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import db.DBPreparedStatementConnect;
import db.DBPreparedStatementConnect.Batch;
import db.DBinfo;
import util.DateTime;
import util.ErrorMessage;
import util.Log;
import util.Util;

// tecwt_webとtecwt_mailの両方に存在しているので修正を入れる時は両方に反映させること！

public class WorklistRequest {
    public static class Result {
        public Response data;
        public ErrorMessage error;
    }

    // 登録結果
    public static class Response {
        public int oid;
        public int uid;

        public int tdate;
        public String name;

        public int intime;
        public int outtime;
        public int breaktime;
        public int fare;
    }

	public static class Work {
		public int wid;
		public int detailID;
		public int minute;
    }

    private static enum From {
        WEB("WEB管理"), NFC_OUT("NFC 退勤報告"), MAIL_OUT("メール 退勤報告");

        private final String name;

        private From(final String name) {
            this.name = name;
        }
    }

    public int tdate;

	public int oid;
	public int uid;

	public int intime;
	public int outtime;
	public int breaktime;
	public int fare;

    public Work[] works;

    // 端末（NFC）打刻用
	public String latitude;
	public String longitude;
	public String unitid;

    private static final String dailyTable = DBinfo.TABLE_DAILY_WORK_RESULT;
    private static final String logTable = DBinfo.TABLE_TCLOG;
    private static final String userTable = DBinfo.TABLE_USERS;
	private static final String timecardTable = DBinfo.TABLE_TIMECARD;

    private static final int WORKING_TIME_UNIT_MIN = 15;        // 勤務時間の最低単位（分）
    public static final int WORKING_NORMAL_TIMES_VALUE = 1;    // 通常出勤の「times」の値
    private static final String TIMECARD_DEFAULT_LOCATION = "0.0";

    private Batch makeDailyDeleteBatch() {
        String sql = "DELETE FROM " + dailyTable + " "
            + "WHERE oid = ? AND uid = ? AND tdate = ?;";
        Batch batch = new Batch();
        batch.sql = sql;
        batch.parameterList = Arrays.asList(Arrays.asList(oid, uid, tdate));
        return batch;
    }

    private Batch makeDailyInsertBatch() {
        String sql = "INSERT INTO " + dailyTable + " "
            + "(oid, uid, tdate, wid, work_detail_id, work_minute) "
            + "VALUES (?, ?, ?, ?, ?, ?);";
        Batch batch = new Batch();
        List<List<Object>> parameters = new ArrayList<List<Object>>();
        for (Work work : works) {
            parameters.add(
                Arrays.asList(oid, uid, tdate, work.wid, work.detailID, work.minute)
            );
        }
        batch.sql = sql;
        batch.parameterList = parameters;
        return batch;
    }

    private ErrorMessage checkError() {
        if (Util.isNullOrBlank(latitude)) {
            return ErrorMessage.timecard_out07;
        }

        if (Util.isNullOrBlank(longitude)) {
            return ErrorMessage.timecard_out08;
        }

        if (Util.isNullOrBlank(unitid)) {
            return ErrorMessage.timecard_out09;
        }

        Calendar cal = DateTime.IntToCalendar(tdate);
        if (cal == null) {
            return ErrorMessage.timecard_out10;
        }

        if (tdate != DateTime.GetTodayInt()) {
            return ErrorMessage.timecard_out11;
        }

        int outH = outtime / 100;
        int outM = outtime % 100;
        if (outH > 24 || outH < 0) {
            return ErrorMessage.timecard_out13;
        }
        if ((outM % WORKING_TIME_UNIT_MIN) != 0) {
            return ErrorMessage.timecard_out12;
        }
        if (intime > 0 && intime >= outtime) {
            return ErrorMessage.timecard_out21_1;
        }

        if (outtime > DateTime.GetNowHHMMInt()) {
            return ErrorMessage.timecard_out21_2;
        }

        if ((breaktime % WORKING_TIME_UNIT_MIN) != 0) {
            return ErrorMessage.timecard_out19;
        }

        return null;    // エラーなし
    }

    // NFCで退勤処理がされた場合
    public Result execLeavingWorkByNFC() {
        Result result = new Result();
        ErrorMessage error = checkError();
        if (error != null) {
            result.error = error;
            return result;
        }

        DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();

        List<Batch> batches = new ArrayList<Batch>();

        // タイムカード更新（UPSERT）
        batches.add(makeTimecardBatch());
        // タイムカード用ログ更新
        batches.add(makeLogBatch(From.NFC_OUT));

        // 勤務内容の更新は削除→挿入の順
        batches.add(makeDailyDeleteBatch());
        batches.add(makeDailyInsertBatch());

        boolean success = db.execBatchSQL(batches);

        db.close();

        if (!success) {
            result.error = ErrorMessage.timecard_outex;
            return result;
        }

        Response data = queryResult();
        if (data == null) {
            result.error = ErrorMessage.timecard_outex;
            return result;
        }
        result.data = data;
        return result;    // 正常終了
    }

    // メールで退勤処理がされた場合
    public Result execLeavingWorkByMail() {
        Result result = new Result();
        // メールの場合は呼び出し元でチェック済

        DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();

        List<Batch> batches = new ArrayList<Batch>();

        // タイムカード更新（UPSERT）
        batches.add(makeTimecardBatch());
        // タイムカード用ログ更新
        batches.add(makeLogBatch(From.MAIL_OUT));

        // 勤務内容の更新は削除→挿入の順
        batches.add(makeDailyDeleteBatch());
        batches.add(makeDailyInsertBatch());

        boolean success = db.execBatchSQL(batches);

        db.close();

        if (!success) {
            result.error = ErrorMessage.timecard_outex;
            return result;
        }

        Response data = queryResult();
        if (data == null) {
            result.error = ErrorMessage.timecard_outex;
            return result;
        }
        result.data = data;
        return result;    // 正常終了
    }


    private Batch makeLogBatch(From from) {
        String sql = "INSERT INTO " + logTable + " "
            + "(oid, uid, logtype, func, carddate, logdate, logtime, latitude, longitude, unitid) "
            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        int wid = works.length > 0 ? works[0].wid : 0;
        int detailID = works.length > 0 ? works[0].detailID : 0;

        DecimalFormat df = new DecimalFormat("00:00");
        String func = "退勤時刻:" + df.format(outtime) + ","
            + "業務種別:" + wid + ","
            + "業務内容:" + detailID + ","
            + "休憩時間:" + breaktime + ","
            + "交通費:" + fare;

        List<Object> param = Arrays.asList(oid, uid, from.name, func,
            tdate, DateTime.GetTodayInt(), DateTime.GetNowHHMMInt(),
            latitude == null ? TIMECARD_DEFAULT_LOCATION : latitude,
    		longitude == null ? TIMECARD_DEFAULT_LOCATION : longitude,
			unitid == null ? "" : unitid);

        Batch batch = new Batch();
        batch.sql = sql;
        batch.parameterList = Arrays.asList(param);
        return batch;
    }

    private Batch makeTimecardBatch() {
        String sql = "WITH upsert AS( UPDATE " + timecardTable + " "
            + "SET intime = ?, outtime = ?, breaktime = ?, wid = ?, note = ?, "
            + "fare = ?, wcount = ?, rcount = ? "
            + "WHERE oid = ? AND uid = ? AND tdate = ? AND times = ? RETURNING 1 ) "
            + "INSERT INTO " + timecardTable + "( "
            + "oid, uid, tdate, times, intime, outtime, breaktime, wid, note, fare, wcount, rcount "
            + ") SELECT ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? "
            + "WHERE NOT EXISTS( SELECT 1 FROM upsert );";

        int wid = works.length > 0 ? works[0].wid : 0;
        String note = "";	// 利用しない
        int wcount = 0;		// 利用しない
        int rcount = 0;		// 利用しない
        int times = WORKING_NORMAL_TIMES_VALUE;     // 移動が無くなったので1固定

        List<Object> param = Arrays.asList(
            // UPDATE用(SET)
            intime, outtime, breaktime, wid, note, fare, wcount, rcount,
            // UPDATE用(WHERE)
            oid, uid, tdate, times,
            // INSERT用
            oid, uid, tdate, times, intime, outtime, breaktime,
            wid, note, fare, wcount, rcount
        );

        Batch batch = new Batch();
        batch.sql = sql;
        batch.parameterList = Arrays.asList(param);
        return batch;
    }

    private Response queryResult() {
        DBPreparedStatementConnect db = new DBPreparedStatementConnect();
        db.open();

        String sql = "SELECT users.oid, users.uid, users.name, timecard.tdate, "
            + "timecard.intime, timecard.outtime, timecard.breaktime, timecard.fare "
            + "FROM " + userTable + " AS users "
            + "LEFT OUTER JOIN " + timecardTable + " AS timecard "
            + "ON users.oid = timecard.oid AND users.uid = timecard.uid "
            + "AND timecard.tdate = ? "
            + "WHERE users.oid = ? AND users.uid = ? ";
        try {
        	Response result = new Response();
            ResultSet rs = db.execQuery(sql, Arrays.asList(tdate, oid, uid));
            if (rs != null && rs.next()) {
                result.oid = rs.getInt("oid");
                result.uid = rs.getInt("uid");
                result.name = rs.getString("name");
                result.intime = rs.getInt("intime");
                result.outtime = rs.getInt("outtime");
                result.breaktime = rs.getInt("breaktime");
                result.fare = rs.getInt("fare");
                result.tdate = rs.getInt("tdate");
                return result;
            } else {
                return null;
            }
        } catch (SQLException | NumberFormatException e) {
            Log.exceptionWrite(e);
            return null;
        } finally {
            db.close();
        }

    }
}
