package model;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;

import db.DBPreparedStatementConnect;
import db.DBinfo;
import util.API;
import util.Log;
import util.Util;

// FreeeのOAuth認証用のAPIに対応
public class FreeeAuthAPI {
	static final String ACCOUNT_URL = "https://accounts.secure.freee.co.jp/public_api/";

	static final String ENDPOINT_AUTH = "authorize";
	static final String ENDPOINT_TOKEN = "token";

	private static final String linkFreeelTable = DBinfo.TABLE_LINK_FREEE;

	// クライアントのアプリの認証情報
	public static class ClientAuthorize {
		// クライアントID（アプリID）
		public String id;
		// クライアントシークレット
		public String secret;
		// コールバックURL
		public String url;
	}

	// トークン情報
	public static class Token {
		// アクセストークン
		public String access;
		// リフレッシュトークン
		public String refresh;
		// 有効期限（残り時間）
		public int expires;

		// トークンが有効かどうか（checkExpire: 残り時間もチェックするかどうか）
		public boolean isValid(boolean checkExpire) {
			if (Util.isNullOrBlank(access) || Util.isNullOrBlank(refresh)) {
				return false;
			}
			if (checkExpire && expires <= 0) {
				return false;
			}
			return true;
		}
	}

	// 連携情報
	public static class LinkInfo {
		public ClientAuthorize client;
		public Token token;
		// 事業所ID
		public int company;
	}

	// トークンの有効期限の残り時間が↓を下回ったら更新する（デフォルト設定）
	static int defaultRefreshRemainTime = 60 * 10;

	// 認証用URL生成
	public static String generateAuthorizeURL(ClientAuthorize client) {
		try {
			return ACCOUNT_URL + ENDPOINT_AUTH
				+ "?" + "client_id=" + client.id
				+ "&" + "redirect_uri=" + URLEncoder.encode(client.url, "utf-8")
				+ "&" + "response_type=code";
		} catch (UnsupportedEncodingException e) {
			return null;
		}
	}

	// 現在のクライアント認証情報を全て削除し新しい認証開始
	public static boolean clearAndStartAuthorize(ClientAuthorize client) {
		DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();

		String clear = "DELETE FROM " + linkFreeelTable + ";";
		db.execUpdate(clear, Arrays.asList());

		String insert = "INSERT INTO " + linkFreeelTable + " "
			+ "(client_id, client_secret, redirect_uri) "
			+ "VALUES(?, ?, ?)";
		boolean success = db.execUpdate(insert,
			Arrays.asList(client.id, client.secret, client.url));
		db.close();
		return success;
	}

	// トークン取得（連携先から）
	public static boolean retrieveTokenAndSave(ClientAuthorize client, String authorizationCode) {
		try {
			URL url = new URL(ACCOUNT_URL + ENDPOINT_TOKEN);
			String parameter ="code=" + URLEncoder.encode(authorizationCode, "utf-8")
				+ "&client_id=" + URLEncoder.encode(client.id, "utf-8")
				+ "&client_secret=" + URLEncoder.encode(client.secret, "utf-8")
				+ "&redirect_uri=" + URLEncoder.encode(client.url, "utf-8")
				+ "&grant_type=authorization_code";

			String result = API.post(url, parameter, null);
			if (result == null) {
				return false;
			}

			Map<String, String> map = API.jsonStringToMap(result);
			Token token = new Token();
			token.access = map.get("access_token");
			token.refresh = map.get("refresh_token");
			token.expires = Integer.parseInt(map.get("expires_in"));

			if (!token.isValid(false)) {
				return false;
			}

			return updateToken(token, client);
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return false;
		}
	}

	private static Timestamp tokenExpire(Token token) {
		long expire = Calendar.getInstance().getTimeInMillis() + token.expires * 1000;
		return new Timestamp(expire);
	}

	// トークン更新
	private static boolean updateToken(Token token, ClientAuthorize client) {
		String update = "UPDATE " + linkFreeelTable + " "
			+ "SET access_token = ?, refresh_token = ?, expire = ? "
			+ "WHERE client_id = ?;";

		DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();

		boolean success = db.execUpdate(update,
			Arrays.asList(token.access, token.refresh, tokenExpire(token), client.id));
		db.close();
		return success;
	}

	// 連携情報取得
	public static LinkInfo getLinkInfo() {
		DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();

		String sql = "SELECT access_token, refresh_token, expire, client_id, client_secret, redirect_uri, company_id FROM "
			 + linkFreeelTable + " ORDER BY expire DESC;";
		ResultSet rs = db.execQuery(sql, Arrays.asList());
		try {
			if (rs == null || !rs.next()) {
				return null;
			}

			ClientAuthorize client = new ClientAuthorize();
			client.id = rs.getString("client_id");
			client.secret = rs.getString("client_secret");
			client.url = rs.getString("redirect_uri");

			Token token = new Token();
			token.access = rs.getString("access_token");
			token.refresh = rs.getString("refresh_token");
			Timestamp timestamp = rs.getTimestamp("expire");
			if (timestamp != null) {
				long now = Calendar.getInstance().getTimeInMillis();
				token.expires = (int)((timestamp.getTime() - now) / 1000);
			} else {
				token.expires = 0;
			}
			LinkInfo result = new LinkInfo();
			result.client = client;
			result.token = token;

			result.company = rs.getString("company_id") == null ? -1 : rs.getInt("company_id");
			return result;
		} catch (SQLException e) {
			return null;
		} finally {
			db.close();
		}
	}

	// 有効な連携情報を返す
	public static LinkInfo getActiveLinkInfo() {
		LinkInfo link = getLinkInfo();
		if (link == null || link.token == null) {
			return null;
		}
		if (link.token.expires < defaultRefreshRemainTime) {
			Token newToken = refreshTokenAndSave(link.token, link.client);
			link.token = newToken;
		}
		return link;
	}

	// 事業所IDを設定する
	public static boolean setCompany(int id) {
		DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();
		// 全件アップデート
		String update = "UPDATE " + linkFreeelTable + " SET company_id = ? ";
		boolean success = db.execUpdate(update, Arrays.asList(id));
		db.close();
		return success;
	}

	// トークンの更新と保存
	public static Token refreshTokenAndSave() {
		LinkInfo info = getLinkInfo();
		if (info == null) {
			return null;
		}
		return refreshTokenAndSave(info.token, info.client);
	}

	private static Token refreshTokenAndSave(Token token, ClientAuthorize client) {
		try {
			URL url = new URL(ACCOUNT_URL + ENDPOINT_TOKEN);
			String parameter ="client_id=" + URLEncoder.encode(client.id, "utf-8")
				+ "&client_secret=" + URLEncoder.encode(client.secret, "utf-8")
				+ "&refresh_token=" + URLEncoder.encode(token.refresh, "utf-8")
				+ "&grant_type=refresh_token";

			String result = API.post(url, parameter, null);
			if (result == null) {
				return null;
			}

			Map<String, String> map = API.jsonStringToMap(result);
			Token newToken = new Token();
			newToken.access = map.get("access_token");
			newToken.refresh = map.get("refresh_token");
			newToken.expires = Integer.parseInt(map.get("expires_in"));

			if (!newToken.isValid(false)) {
				return null;
			}

			if (!updateToken(newToken, client)) {
				return null;
			}

			return newToken;
		} catch (Exception e) {
			Log.exceptionWrite(e);
			return null;
		}
	}
}
