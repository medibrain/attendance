package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import db.DBPreparedStatementConnect;
import db.DBinfo;
import util.Log;
import util.Util;


public class WorklistResponse {
	public static class ProjectGroup {
		public String head;
		public IDPair[] list;
	}

	public static class IDPair {
		public String id;
		public String name;
	}

	public static class Work {
		public int wid;
		public int detailID;
		public int minute;

		public String workName;
		public String detailName;
	}

	public int oid;
	public int uid;

	public int tdate;

	public String name;
	public String[] projects;
	public IDPair[] worktypes;

	public int intime;
	public int outtime;
	public int breaktime;
	public int fare;

    public Work[] works;

	public ProjectGroup[] allProjects;
	// 部署ごと作業内容一覧
	public ProjectGroup[] allWorkDetails;

	private static final String dailyTable = DBinfo.TABLE_DAILY_WORK_RESULT;
	private static final String userTable = DBinfo.TABLE_USERS;
	private static final String timecardTable = DBinfo.TABLE_TIMECARD;
	private static final String departmentTable = DBinfo.TABLE_DEPARTMENT;
	private static final String workDetailTable = DBinfo.TABLE_WORK_DETAIL;
	private static final String workTypeTable = DBinfo.TABLE_WORKTYPE;
	private static final String nfcTable = DBinfo.TABLE_NFC;
	private static final String projectLimitCount = "5";

	protected static WorklistResponse makeFromDB(int date, int oid, int uid, boolean onlyActiveUser, boolean includeNoProjectCode) {
		DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();

		WorklistResponse result;
		try {
			result = queryUser(db, date, oid, uid, onlyActiveUser);
			if (result == null) {
				return null;
			}

			result.projects = queryProjects(db, oid, uid);
			result.allProjects = queryAllProjects(db, oid, includeNoProjectCode);
			result.allWorkDetails = queryAllWorkDetails(db, oid);
			result.works = queryDaily(db, oid, uid, date);
			result.tdate = date;
			return result;
		} catch (SQLException e) {
			Log.exceptionWrite(e);
			return null;
		} finally {
			db.close();
		}
	}

	public static WorklistResponse makeFromDB(int date, String idm) {
		DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();

		WorklistResponse result;
		try {
			result = queryUser(db, date, idm);
			if (result == null) {
				return null;
			}
			result.projects = queryProjects(db, result.oid, result.uid);
			result.allProjects = queryAllProjects(db, result.oid, false);	// 有給などはなし
			result.allWorkDetails = queryAllWorkDetails(db, result.oid);
			result.works = queryDaily(db, result.oid, result.uid, null);	// 直近
			result.tdate = date;
			return result;
		} catch (SQLException | NumberFormatException e) {
			Log.exceptionWrite(e);
			return null;
		} finally {
			db.close();
		}
	}

	private static WorklistResponse queryUser(DBPreparedStatementConnect db, int date, int oid, int uid, boolean onlyActiveUser) throws SQLException {
		// 指定されたユーザの出勤データと作業内容コードを取得
		String sql = "SELECT users.oid, users.uid, users.name, "
			+ "timecard.intime, timecard.outtime, timecard.breaktime, timecard.fare, "
			+ "department.id as department_id, "
			+ "work_detail.work_detail_id, work_detail.display_order, work_detail.name as work_name "
			+ "FROM " + userTable + " AS users "
			+ "LEFT OUTER JOIN " + timecardTable + " AS timecard "
			+ "ON users.oid = timecard.oid AND users.uid = timecard.uid "
			+ "AND timecard.tdate = ? "
			+ "INNER JOIN " + departmentTable + " AS department "
			+ "ON users.oid = department.oid AND users.department = department.id "
			+ "LEFT OUTER JOIN " + workDetailTable + " as work_detail "
			+ "ON work_detail.oid = department.oid "
			+ "AND work_detail.department_id = department.id "
			+ "AND work_detail.enable = true "
			+ "WHERE users.oid = ? AND users.uid = ? "
			+ (onlyActiveUser ? "AND users.enable = true " : "")
			+ "ORDER BY work_detail.display_order;";

		return queryParseUser(db, sql, Arrays.asList(date, oid, uid));
	}

	private static WorklistResponse queryUser(DBPreparedStatementConnect db, int date, String idm) throws SQLException {
		// 指定されたユーザの出勤データと作業内容コードを取得
		String sql = "SELECT users.oid, users.uid, users.name, "
			+ "timecard.intime, timecard.outtime, timecard.breaktime, timecard.fare, "
			+ "department.id as department_id, "
			+ "work_detail.work_detail_id, work_detail.display_order, work_detail.name as work_name "
			+ "FROM " + nfcTable + " AS nfc "
			+ "INNER JOIN " + userTable + " AS users "
			+ "ON nfc.oid = users.oid AND nfc.uid = users.uid "
			+ "LEFT OUTER JOIN " + timecardTable + " AS timecard "
			+ "ON users.oid = timecard.oid AND users.uid = timecard.uid "
			+ "AND timecard.tdate = ? "
			+ "INNER JOIN " + departmentTable + " AS department "
			+ "ON users.oid = department.oid AND users.department = department.id "
			+ "LEFT OUTER JOIN " + workDetailTable + " as work_detail "
			+ "ON work_detail.oid = department.oid "
			+ "AND work_detail.department_id = department.id "
			+ "AND work_detail.enable = true "
			+ "WHERE nfc.idm = ? AND users.enable = true "
			+ "ORDER BY work_detail.display_order;";

		return queryParseUser(db, sql, Arrays.asList(date, idm));
	}

	private static WorklistResponse queryParseUser(DBPreparedStatementConnect db, String sql, List<Object> parameters) throws SQLException {
		ResultSet rs = db.execQuery(sql, parameters);
		if (rs == null) {
			return null;
		}

		WorklistResponse result = new WorklistResponse();
		List<IDPair> codes = new ArrayList<IDPair>();

		while(rs.next()) {
			result.oid = rs.getInt("oid");
			result.uid = rs.getInt("uid");
			result.name = rs.getString("name");
			result.intime = rs.getInt("intime");
			if (rs.wasNull()) {
				result.intime = -1;
			}
			result.outtime = rs.getInt("outtime");
			if (rs.wasNull()) {
				result.outtime = -1;
			}
			result.breaktime = rs.getInt("breaktime");
			result.fare = rs.getInt("fare");

			IDPair code = new IDPair();
			code.id = rs.getString("work_detail_id");
			if (!rs.wasNull()) {
				code.name = rs.getString("work_name");
				codes.add(code);
			}
		}

		if (result.name == null) {
			return null;	// 該当ユーザ存在せず
		}

		result.worktypes = codes.toArray(new IDPair[codes.size()]);

		return result;
	}

	private static String[] queryProjects(DBPreparedStatementConnect db, int oid, int uid) throws SQLException {
		// 指定されたユーザの直近のよく使うコード取得
		String sql = "SELECT worktype.wid FROM " + workTypeTable + " AS worktype "
			+ "INNER JOIN ( SELECT history.wid FROM " + dailyTable + " AS history "
			+ "WHERE ctid = ( SELECT ctid FROM " + dailyTable + " AS history2 "
			+ "WHERE history.wid = history2.wid AND history2.oid = ? AND history2.uid = ? "
			+ "ORDER BY history2.tdate DESC LIMIT 1 ) ORDER BY tdate "
			+ "DESC LIMIT " + projectLimitCount + " ) AS result "
			+ "ON worktype.wid = result.wid WHERE worktype.enable = true";

		ResultSet rs = db.execQuery(sql, Arrays.asList(oid, uid));
		if (rs == null) {
			return new String[0];
		}

		List<String> codes = new ArrayList<String>();

		while(rs.next()) {
			codes.add(rs.getString("wid"));
		}
		return codes.toArray(new String[codes.size()]);
	}

	private static ProjectGroup[] queryAllProjects(DBPreparedStatementConnect db, int oid, boolean includeNoProjectCode) throws SQLException {
		// 勤務種別一覧取得
		String sql = "SELECT wid, name, kana, projectcode "
			+ "FROM " + workTypeTable + " "
			+ "WHERE oid = ? AND enable = true ORDER BY kana;";

		ResultSet rs = db.execQuery(sql, Arrays.asList(oid));
		if (rs == null) {
			return new ProjectGroup[0];
		}

		List<ProjectGroup> list = new ArrayList<>();
		String nowHead = "";
		List<IDPair> codes = new ArrayList<>();

		while(rs.next()) {
			String id = rs.getString("wid");
			// FIXME: 半休等の改良の時まで一旦暫定対応で入れておく
			// includeNoProjectCode: プロジェクトコード未付与＝有給などの管理者以外が選択しないものの想定
			if (!includeNoProjectCode && Util.isNullOrBlank(rs.getString("projectcode"))) {
				continue;
			}

			String head = rs.getString("kana").substring(0, 1);
			if (!nowHead.equals(head)) {
				if (codes.size() > 0) {
					ProjectGroup group = new ProjectGroup();
					group.head = nowHead;
					group.list = codes.toArray(new IDPair[codes.size()]);
					list.add(group);
				}

				codes = new ArrayList<>();
				nowHead = head;
			}

			IDPair code = new IDPair();
			code.id = id;
			code.name = rs.getString("name");
			codes.add(code);
		}

		if (codes.size() > 0) {
			ProjectGroup group = new ProjectGroup();
			group.head = nowHead;
			group.list = codes.toArray(new IDPair[codes.size()]);
			list.add(group);
		}

		return list.toArray(new ProjectGroup[list.size()]);
	}

	private static Work[] queryDaily(DBPreparedStatementConnect db, int oid, int uid, Integer date) throws SQLException {
		ResultSet rs;

		if (date == null) {
			// 直近のデータを取ってくる
			String sql = "SELECT daily_work_result.wid, daily_work_result.work_detail_id, daily_work_result.work_minute, "
				+ "worktype.name AS work_name, work_detail.name AS detail_name "
				+ "FROM " + dailyTable + " AS daily_work_result "
				+ "INNER JOIN " + workTypeTable + " AS worktype "
				+ "ON worktype.oid = daily_work_result.oid AND worktype.wid = daily_work_result.wid "
				+ "INNER JOIN " + workDetailTable + " AS work_detail "
				+ "ON work_detail.oid = daily_work_result.oid AND work_detail.work_detail_id = daily_work_result.work_detail_id "
				+ "WHERE daily_work_result.oid = ? AND daily_work_result.uid = ? "
				+ "AND daily_work_result.tdate = (SELECT MAX(tdate) FROM daily_work_result WHERE oid = ? AND uid = ?);";

			rs = db.execQuery(sql, Arrays.asList(oid, uid, oid, uid));
		} else {
			String sql = "SELECT daily_work_result.wid, daily_work_result.work_detail_id, daily_work_result.work_minute, "
				+ "worktype.name AS work_name, work_detail.name AS detail_name "
				+ "FROM " + dailyTable + " AS daily_work_result "
				+ "INNER JOIN " + workTypeTable + " AS worktype "
				+ "ON worktype.oid = daily_work_result.oid AND worktype.wid = daily_work_result.wid "
				+ "INNER JOIN " + workDetailTable + " AS work_detail "
				+ "ON work_detail.oid = daily_work_result.oid AND work_detail.work_detail_id = daily_work_result.work_detail_id "
				+ "WHERE daily_work_result.oid = ? AND daily_work_result.uid = ? AND daily_work_result.tdate = ?;";

			rs = db.execQuery(sql, Arrays.asList(oid, uid, date));
		}

		if (rs == null) {
			return new Work[0];
		}

		List<Work> works = new ArrayList<Work>();
		while(rs.next()) {
			Work work = new Work();
			work.wid = rs.getInt("wid");
			work.detailID = rs.getInt("work_detail_id");
			work.minute = rs.getInt("work_minute");
			work.workName = rs.getString("work_name");
			work.detailName = rs.getString("detail_name");
			works.add(work);
		}
		return works.toArray(new Work[works.size()]);
	}

	protected static ProjectGroup[] queryAllWorkDetails(DBPreparedStatementConnect db, int oid) throws SQLException {
		String sql = "SELECT  department.name AS department, work_detail.work_detail_id, work_detail.name AS name "
				+ "FROM " + departmentTable + " AS department "
				+ "INNER JOIN " + workDetailTable + " AS work_detail "
				+ "ON department.oid = work_detail.oid AND department.id = work_detail.department_id "
				+ "WHERE department.oid = ? ORDER BY department.id, work_detail.display_order";

		ResultSet rs = db.execQuery(sql, Arrays.asList(oid));
		if (rs == null) {
			return new ProjectGroup[0];
		}

		List<ProjectGroup> list = new ArrayList<>();
		String nowHead = "";
		List<IDPair> codes = new ArrayList<>();

		while(rs.next()) {
			String head = rs.getString("department");
			if (!nowHead.equals(head)) {
				if (codes.size() > 0) {
					ProjectGroup group = new ProjectGroup();
					group.head = nowHead;
					group.list = codes.toArray(new IDPair[codes.size()]);
					list.add(group);
				}

				codes = new ArrayList<>();
				nowHead = head;
			}

			IDPair code = new IDPair();
			code.id = rs.getString("work_detail_id");
			code.name = rs.getString("name");
			codes.add(code);
		}

		if (codes.size() > 0) {
			ProjectGroup group = new ProjectGroup();
			group.head = nowHead;
			group.list = codes.toArray(new IDPair[codes.size()]);
			list.add(group);
		}

		return list.toArray(new ProjectGroup[list.size()]);
	}
}
