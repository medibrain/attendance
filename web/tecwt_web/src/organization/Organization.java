package organization;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import db.BaseDAO;
import db.DBPreparedStatementConnect;
import db.DBinfo;
import db.TimeCardDAO;
import db.UsersDAO;
import db.WorktypeDAO;
import table.Category;
import table.Customer;
import table.Department;
import table.Employment;
import table.Group;
import table.Users;
import table.Worklocation;
import table.Worktype;
import util.DateTime;
import util.JsonUtil;
import util.Util;

public abstract class Organization {

	public String uri;
	public int oid;
	public String name;

	/**
	 * --出力形式--<br>
	 * SID,氏名,日付,出勤時刻,退勤時刻,休憩時間,勤務種別,業務内容,交通費,処理数,申出数
	 */
	protected static final String[] ROWS_TIMECARD_1 = {
		"SID","氏名","日付",
		"出勤時刻","退勤時刻","休憩時間",
		"勤務種別","業務内容","交通費","処理数","申出数"
	};
	/**
	 * --出力形式--<br>
	 * 社員番号,氏名,日付,出勤時刻,退勤時刻,休憩時間,勤務種別,業務内容,交通費,処理数,申出数,移動時刻,勤務種別２,業務内容２,処理数２,申出数２
	 */
	protected static final String[] ROWS_TIMECARD_2 = {
		"社員番号","氏名","日付",
		"出勤時刻","退勤時刻","休憩時間",
		"勤務種別","業務内容","交通費","処理数","申出数",
		"移動時刻","勤務種別2","業務内容2","処理数2","申出数2"
	};
	/**
	 * --出力形式--<br>
	 * 日付,社員番号,氏名,グループ,勤務種別,プロジェクトコード,客先,カテゴリー,業務内容,打刻区分,出勤時刻,退勤時刻,休憩時間,実働時間,交通費
	 */
	protected static final String[] HEADERS_INCOMESTATEMENT = {
		"日付","社員番号","氏名","グループ","勤務種別","プロジェクトコード","客先","カテゴリー","業務内容","打刻区分","出勤時刻","退勤時刻","休憩時間","実働時間","交通費"
	};
	/**
	 * --作業内容　出力形式--<br>
	 */
	private static final String[] ROWS_WORKLIST = {
		"日付", "社員番号", "氏名", "グループ", "部署", "勤務種別", "プロジェクトコード",  "業務内容", "業務内容ID", "作業時間", "交通費"
	};
	/**
	 * --出力形式--<br>
	 * "氏名","カナ","雇用形態","勤務地","部署","社員番号","社員証登録数",<br>
	 * "メールアドレス","メール打刻","グループ","登録日","サイト閲覧権限","登録状況",<br>
	 * "サイト閲覧権限","登録状況","登録日","最終更新日"
	 */
	protected static final String[] ROWS_USERS_1 = {
		"氏名","カナ","雇用形態","勤務地","部署","社員番号","社員証登録数",
		"メールアドレス","メール打刻","グループ","雇用開始日","有休残日数",
		"サイト閲覧権限","登録状況","登録日","最終更新日"
	};
	/**
	 * --出力形式--<br>
	 * "勤務種別名","カナ","客先","カテゴリー","メモ","登録状況","登録日","最終更新日"
	 */
	protected static final String[] ROWS_WORKTYPE_1 = {
		"勤務種別名","カナ","プロジェクトコード","客先","カテゴリー","責任者","メモ","登録状況","登録日","最終更新日"
	};
	/**
	 * --出力形式--<br>
	 * "端末名","端末ID","登録日","登録状況","メモ","登録用パスワード"
	 */
	protected static final String[] ROWS_UNIT_1 = {
		"端末名","端末ID","登録日","登録状況","メモ","登録用パスワード"
	};
	/**
	 * --出力形式--<br>
	 * "部署名","カナ","責任者","登録状況","メモ","登録日","最終更新日"
	 */
	protected static final String[] ROWS_DEPARTMENT_1 = {
		"部署名","カナ","責任者","登録状況","メモ","登録日","最終更新日"
	};
	/**
	 * --出力形式--<br>
	 * "雇用形態名","カナ","登録状況","メモ","登録日","最終更新日"
	 */
	protected static final String[] ROWS_EMPLOYMENT_1 = {
		"雇用形態名","カナ","登録状況","メモ","登録日","最終更新日"
	};
	/**
	 * --出力形式--<br>
	 * "勤務地名","カナ","登録状況","メモ","登録日","最終更新日"
	 */
	protected static final String[] ROWS_WORKLOCATION_1 = {
		"勤務先名","カナ","登録状況","メモ","登録日","最終更新日"
	};
	/**
	 * --出力形式--<br>
	 * "グループ名","カナ","登録状況","メモ","登録日","最終更新日"
	 */
	protected static final String[] ROWS_GROUP_1 = {
		"グループ名","カナ","登録状況","メモ","登録日","最終更新日"
	};
	/**
	 * --出力形式--<br>
	 * "客先名","カナ","登録状況","メモ","登録日","最終更新日"
	 */
	protected static final String[] ROWS_CUSTOMER_1 = {
		"客先名","カナ","登録状況","メモ","登録日","最終更新日"
	};
	/**
	 * --出力形式--<br>
	 * "カテゴリー名","カナ","登録状況","メモ","登録日","最終更新日"
	 */
	protected static final String[] ROWS_CATEGORY_1 = {
		"カテゴリー名","カナ","登録状況","メモ","登録日","最終更新日"
	};

	/**
	 * コンストラクタ
	 * @param uri
	 * @param oid
	 */
	public Organization(String uri, int oid, String name) {
		this.uri = uri;
		this.oid = oid;
		this.name = name;
	}

	/**
	 * タイムカードのCSV文字列を返します。<br>
	 * 本文字列をレスポンスに流すことができます。
	 * @param showDataJSON JSON文字列。not null(表示行をCSV化)、null(年月でCSV化)
	 * @param year 年
	 * @param month 月
	 * @param uid -1(全ユーザ対象),
	 * @param unfinishedable true(未完打刻でも出力), false(未完打刻は出力しない)
	 * @return
	 */
	public abstract String getTimecardCSV(String showDataJSON, int year, int month, int uid, boolean unfinishedable);
	/**
	 * タイムカードの損益計算用CSV文字列を返します。<br>
	 * 本文字列をレスポンスに流すことができます。
	 * @param showDataJSON JSON文字列。not null(表示行をCSV化)、null(年月でCSV化)
	 * @param year 年
	 * @param month 月
	 * @param uid -1(全ユーザ対象),
	 * @param unfinishedable true(未完打刻でも出力), false(未完打刻は出力しない)
	 * @return
	 */
	public abstract String getIncomeStatementCSV(String showDataJSON, int year, int month, int uid, boolean unfinishedable);
	/**
	 * ユーザ一覧のCSV文字列を返します。<br>
	 * 本文字列をレスポンスに流すことができます。
	 * @param showDataJSON
	 * @return
	 */
	public abstract String getUsersCSV(String showDataJSON);
	/**
	 * 勤務種別一覧のCSV文字列を返します。<br>
	 * 本文字列をレスポンスに流すことができます。
	 * @param showDataJSON
	 * @return
	 */
	public abstract String getWorktypeCSV(String showDataJSON);
	/**
	 * 端末一覧のCSV文字列を返します。<br>
	 * 本文字列をレスポンスに流すことができます。
	 * @param showDataJSON
	 * @return
	 */
	public abstract String getUnitCSV(String showDataJSON);
	/**
	 * 部署一覧のCSV文字列を返します。<br>
	 * 本文字列をレスポンスに流すことができます。
	 * @param showDataJSON
	 * @return
	 */
	public abstract String getDepartmentCSV(String showDataJSON);
	/**
	 * 雇用形態一覧のCSV文字列を返します。<br>
	 * 本文字列をレスポンスに流すことができます。
	 * @param showDataJSON
	 * @return
	 */
	public abstract String getEmploymentCSV(String showDataJSON);
	/**
	 * 勤務地一覧のCSV文字列を返します。<br>
	 * 本文字列をレスポンスに流すことができます。
	 * @param showDataJSON
	 * @return
	 */
	public abstract String getWorklocationCSV(String showDataJSON);
	/**
	 * グループ一覧のCSV文字列を返します。<br>
	 * 本文字列をレスポンスに流すことができます。
	 * @param showDataJSON
	 * @return
	 */
	public abstract String getGroupCSV(String showDataJSON);
	/**
	 * 客先一覧のCSV文字列を返します。<br>
	 * 本文字列をレスポンスに流すことができます。
	 * @param showDataJSON
	 * @return
	 */
	public abstract String getCustomerCSV(String showDataJSON);
	/**
	 * カテゴリー一覧のCSV文字列を返します。<br>
	 * 本文字列をレスポンスに流すことができます。
	 * @param showDataJSON
	 * @return
	 */
	public abstract String getCategoryCSV(String showDataJSON);

/**********************
 * Timecard
 **********************/
	/**
	 * 抽象メソッドgetCSVの基本形式です。形式内容(ヘッダ)は以下を参照してください。<br>
	 * 出力形式に指定がない場合はgetCSV内からこのメソッドを呼び出してください。<br>
	 * このメソッドは移動を一切考慮しません。移動を考慮する場合はgetCSVBase2,getSCVBase3を利用して下さい。<br>
	 * <br>
	 * SID,氏名,日付,出勤時刻,退勤時刻,休憩時間,勤務種別,業務内容,交通費,処理数,申出数<br>
	 * @param showDataJSON
	 * @param year
	 * @param month
	 * @param uid
	 * @param unfinishedable
	 * @return
	 */
	protected String getTimecardCSVBase1(String showDataJSON, int year, int month, int uid, boolean unfinishedable) {
		// JSON解析
		List<DtoTimeCard> dtos = this.toTimecardList(showDataJSON, year, month, oid, uid);
		if(dtos == null) {
			return null;
		}
		// 社員番号解析
		List<String> tcUserList = new ArrayList<String>();	// 出力範囲内のユーザIDリスト
		for(DtoTimeCard dto : dtos) {
			if(!tcUserList.contains(dto.uid)) {
				tcUserList.add(dto.uid);
			}
 		}
		Map<Integer, String> codeList = new HashMap<Integer, String>();	// 社員番号リスト
		try {
			BaseDAO.QueryResult<Users> qr = UsersDAO.FindOfUids(String.valueOf(oid), tcUserList);
			Users user;
			for (Object obj : qr.List) {
				user = (Users)obj;
				codeList.put(user.getUid(), user.getCode());
			}
		} catch (Exception ex) {
			return null;
		}
		// CSVベース作成
		StringBuilder csv = new StringBuilder();
		for(String row : ROWS_TIMECARD_1) {
			csv.append(row).append(",");
		}
		csv.replace(csv.length()-1, csv.length(), "\n");
		// 値を入れていく
		for(DtoTimeCard dto : dtos) {
			if(unfinishedable && isTimecardUnfinished(dto)) continue;
			// SID(社員番号)
			csv.append("\"").append(codeList.get(Util.toInt(dto.uid))).append("\",");
			// 氏名
			csv.append("\"").append(Util.doubleQuoteEscape(dto.uname, true)).append("\",");
			// 日付
			csv.append("\"").append(dto.tdate).append("\",");
			// 出勤時刻
			csv.append("\"").append(dto.inH).append(":").append(dto.inM).append("\",");
			// 退勤時刻
			csv.append("\"").append(dto.outH).append(":").append(dto.outM).append("\",");
			// 休憩時間
			csv.append("\"").append(dto.breaktime).append("\",");
			// 勤務種別
			csv.append("\"").append(Util.doubleQuoteEscape(dto.wname, true)).append("\",");
			// 業務内容
			csv.append("\"").append(Util.doubleQuoteEscape(dto.note, true)).append("\",");
			// 交通費
			csv.append("\"").append(dto.fare).append("\",");
			// 処理数
			csv.append("\"").append(dto.wcount).append("\",");
			// 申出数
			csv.append("\"").append(dto.rcount).append("\",");
			// 末尾のカンマを削除しつつ改行
			csv.replace(csv.length()-1, csv.length(), "\n");
		}

		return csv.toString();
	}

	/**
	 * 抽象メソッドgetCSVの基本形式です。形式内容は以下を参照してください。<br>
	 * 出力形式に指定がない場合はgetCSV内からこのメソッドを呼び出してください。<br>
	 * <br>
	 * SID,氏名,日付,出勤時刻,退勤時刻,休憩時間,勤務種別,業務内容,交通費,処理数,申出数,移動時刻,勤務種別２,業務内容２,処理数２,申出数２<br>
	 * @param showDataJSON
	 * @param year
	 * @param month
	 * @param uid
	 * @param unfinishedable
	 * @return
	 */
	protected String getTimecardCSVBase2(String showDataJSON, int year, int month, int uid, boolean unfinishedable) {
		// JSON解析
		List<DtoTimeCard> dtos = this.toTimecardList(showDataJSON, year, month, oid, uid);
		if(dtos == null) {
			return null;
		}
		// 社員番号解析
		List<String> tcUserList = new ArrayList<String>();	// 出力範囲内のユーザIDリスト
		for(DtoTimeCard dto : dtos) {
			if(!tcUserList.contains(dto.uid)) {
				tcUserList.add(dto.uid);
			}
 		}
		Map<Integer, String> codeList = new HashMap<Integer, String>();	// 社員番号リスト
		try {
			BaseDAO.QueryResult<Users> qr = UsersDAO.FindOfUids(String.valueOf(oid), tcUserList);
			Users user;
			for (Object obj : qr.List) {
				user = (Users)obj;
				codeList.put(user.getUid(), user.getCode());
			}
		} catch (Exception ex) {
			return null;
		}
		// CSVベース作成
		StringBuilder csv = new StringBuilder();
		for(String row : ROWS_TIMECARD_2) {
			csv.append(row).append(",");
		}
		csv.replace(csv.length()-1, csv.length(), "\n");
		// 値を入れていく
		for(DtoTimeCard dto : dtos) {
			if(unfinishedable && isTimecardUnfinished(dto)) continue;
			// SID(社員番号)
			csv.append("\"").append(codeList.get(Util.toInt(dto.uid))).append("\",");
			// 氏名
			csv.append("\"").append(Util.doubleQuoteEscape(dto.uname, true)).append("\",");
			// 日付
			csv.append("\"").append(dto.tdate).append("\",");
			// 出勤時刻
			csv.append("\"").append(dto.inH).append(":").append(dto.inM).append("\",");
			// 退勤時刻
			csv.append("\"").append(dto.outH).append(":").append(dto.outM).append("\",");
			// 休憩時間
			csv.append("\"").append(dto.breaktime).append("\",");
			// 勤務種別
			csv.append("\"").append(Util.doubleQuoteEscape(dto.wname, true)).append("\",");
			// 業務内容
			csv.append("\"").append(Util.doubleQuoteEscape(dto.note, true)).append("\",");
			// 交通費
			csv.append("\"").append(dto.fare).append("\",");
			// 処理数
			csv.append("\"").append(dto.wcount).append("\",");
			// 申出数
			csv.append("\"").append(dto.rcount).append("\",");
			// 移動時刻
			csv.append("\"").append(dto.moveH).append(":").append(dto.moveM).append("\",");
			// 勤務種別２
			csv.append("\"").append(Util.doubleQuoteEscape(dto.wname_move, true)).append("\",");
			// 業務内容２
			csv.append("\"").append(Util.doubleQuoteEscape(dto.note_move, true)).append("\",");
			// 処理数２
			csv.append("\"").append(dto.wcount_move).append("\",");
			// 申出数２
			csv.append("\"").append(dto.rcount_move).append("\",");
			// 末尾のカンマを削除しつつ改行
			csv.replace(csv.length()-1, csv.length(), "\n");
		}

		return csv.toString();
	}

	/**
	 * 抽象メソッドgetCSVの基本形式です。形式内容(ヘッダ)は以下を参照してください。<br>
	 * 出力形式に指定がない場合はgetCSV内からこのメソッドを呼び出してください。<br>
	 * このメソッドの出力形式はgetCSVBase1と同じですが、移動を考慮した結果が含まれます。<br>
	 * 具体的には業務時間が長い方が優先されます。 詳細はgetWorkContents()を確認して下さい。<br>
	 * <br>
	 * SID,氏名,日付,出勤時刻,退勤時刻,休憩時間,勤務種別,業務内容,交通費,処理数,申出数<br>
	 * @param showDataJSON
	 * @param year
	 * @param month
	 * @param uid
	 * @param unfinishedable
	 * @return
	 */
	protected String getTimecardCSVBase3(String showDataJSON, int year, int month, int uid, boolean unfinishedable) {
		// JSON解析
		List<DtoTimeCard> dtos = this.toTimecardList(showDataJSON, year, month, oid, uid);
		if(dtos == null) {
			return null;
		}
		// 社員番号解析
		List<String> tcUserList = new ArrayList<String>();	// 出力範囲内のユーザIDリスト
		for(DtoTimeCard dto : dtos) {
			if(!tcUserList.contains(dto.uid)) {
				tcUserList.add(dto.uid);
			}
 		}
		Map<Integer, String> codeList = new HashMap<Integer, String>();	// 社員番号リスト
		try {
			BaseDAO.QueryResult<Users> qr = UsersDAO.FindOfUids(String.valueOf(oid), tcUserList);
			Users user;
			for (Object obj : qr.List) {
				user = (Users)obj;
				codeList.put(user.getUid(), user.getCode());
			}
		} catch (Exception ex) {
			return null;
		}
		// CSVベース作成
		StringBuilder csv = new StringBuilder();
		for(String row : ROWS_TIMECARD_1) {
			csv.append(row).append(",");
		}
		csv.replace(csv.length()-1, csv.length(), "\n");
		// 値を入れていく
		TimecardWorkContents wc;
		for(DtoTimeCard dto : dtos) {
			if(unfinishedable && isTimecardUnfinished(dto)) continue;
			wc = getTimecardWorkContents(dto);
			// SID(社員番号)
			csv.append("\"").append(codeList.get(Util.toInt(dto.uid))).append("\",");
			// 氏名
			csv.append("\"").append(Util.doubleQuoteEscape(dto.uname, true)).append("\",");
			// 日付
			csv.append("\"").append(dto.tdate).append("\",");
			// 出勤時刻
			csv.append("\"").append(dto.inH).append(":").append(dto.inM).append("\",");
			// 退勤時刻
			csv.append("\"").append(dto.outH).append(":").append(dto.outM).append("\",");
			// 休憩時間
			csv.append("\"").append(dto.breaktime).append("\",");
			// 勤務種別
			csv.append("\"").append(Util.doubleQuoteEscape(wc.wname, true)).append("\",");
			// 業務内容
			csv.append("\"").append(Util.doubleQuoteEscape(wc.note, true)).append("\",");
			// 交通費
			csv.append("\"").append(dto.fare).append("\",");
			// 処理数
			csv.append("\"").append(wc.wcount).append("\",");
			// 申出数
			csv.append("\"").append(wc.rcount).append("\",");
			// 末尾のカンマを削除しつつ改行
			csv.replace(csv.length()-1, csv.length(), "\n");
		}

		return csv.toString();
	}

	/**
	 * 抽象メソッドgetIncomeStatementCSVの基本形式です。形式内容(ヘッダ)は以下を参照してください。<br>
	 * 出力形式に指定がない場合はgetIncomeStatementCSV内からこのメソッドを呼び出してください。<br>
	 * このデータには移動分も含まれます。同日同ユーザが２行以上出現する場合があります。<br>
	 * <br>
	 * 日付,社員番号,氏名,グループ,勤務種別,プロジェクトコード,客先,カテゴリー,業務内容,打刻区分,出勤時刻,退勤時刻,休憩時間,実働時間,交通費
	 * @param showDataJSON
	 * @param year
	 * @param month
	 * @param uid
	 * @param unfinishedable
	 * @return
	 */
	protected String getIncomeStatementCSVBase1(String showDataJSON, int year, int month, int uid, boolean unfinishedable) {
		// JSON解析
		List<DtoTimeCard> dtos = this.toTimecardList(showDataJSON, year, month, oid, uid);
		if(dtos == null) {
			return null;
		}

		// ユーザマップ作成（グループも含まれる）
		List<String> tcUserList = new ArrayList<String>();	// 出力範囲内のユーザIDリスト
		for(DtoTimeCard dto : dtos) {
			if(!tcUserList.contains(dto.uid)) {
				tcUserList.add(dto.uid);
			}
 		}
		Map<String, Users> userMap = new HashMap<String, Users>();	//ここでIDをStringにしておく
		try {
			BaseDAO.QueryResult<Users> qr = UsersDAO.FindOfUids(String.valueOf(oid), tcUserList);
			Users user;
			for (Object obj : qr.List) {
				user = (Users)obj;
				userMap.put(String.valueOf(user.getUid()), user);
			}
		} catch (Exception ex) {
			return null;
		}

		// 勤務種別マップ作成（客先、カテゴリーも含まれる）
		List<String> tcWorktypeList = new ArrayList<String>();	// 出力範囲内の勤務種別IDリスト
		for(DtoTimeCard dto : dtos) {
			if(!Util.isNullOrBlank(dto.wid) && !tcWorktypeList.contains(dto.wid)) {
				tcWorktypeList.add(dto.wid);
			}
			if(!Util.isNullOrBlank(dto.wid_move) && !tcWorktypeList.contains(dto.wid_move)) {
				tcWorktypeList.add(dto.wid_move);
			}
 		}
		Map<String, Worktype> worktypeMap = new HashMap<String, Worktype>();	//ここでIDをStringにしておく
		try {
			BaseDAO.QueryResult<Worktype> qr = WorktypeDAO.findOfWids(String.valueOf(oid), tcWorktypeList);
			Worktype worktype;
			for (Object obj : qr.List) {
				worktype = (Worktype)obj;
				worktypeMap.put(String.valueOf(worktype.getWid()), worktype);
			}
		} catch (Exception ex) {
			return null;
		}

		// CSVベース作成
		StringBuilder csv = new StringBuilder();
		for(String header : HEADERS_INCOMESTATEMENT) {
			csv.append(header).append(",");
		}
		csv.replace(csv.length()-1, csv.length(), "\n");	//末尾カンマ削除+改行挿入
		// 値を入れていく
		Users us;
		Worktype wt1;
		Worktype wt2;
		double ratio;
		int workTime;
		int workTime1;
		int workTime2;
		int breaktime;
		int breaktime1;
		int breaktime2;
		int fare;
		int fare1;
		int fare2;
		for(DtoTimeCard dto : dtos) {
			if(unfinishedable && isTimecardUnfinished(dto)) continue;

			//出勤・退勤のみ
			if (Util.isNullOrBlankOrHyphen(dto.wname_move)) {
				us = userMap.get(dto.uid);
				wt1 = worktypeMap.get(dto.wid);
				csv.append("\"").append(dto.tdate).append("\",");	//日付
				csv.append("\"").append(Util.doubleQuoteEscape(us != null ? Util.nullBlankHyphenToNull(us.getCode()) : "", true)).append("\",");	//社員番号
				csv.append("\"").append(Util.doubleQuoteEscape(dto.uname, true)).append("\",");	//氏名
				csv.append("\"").append(Util.doubleQuoteEscape(us != null ? Util.nullBlankHyphenToNull(us.getGroupname()) : "", true)).append("\",");	//グループ
				csv.append("\"").append(Util.doubleQuoteEscape(dto.wname, true)).append("\",");	//勤務種別
				csv.append("\"").append(Util.doubleQuoteEscape(wt1 != null ? Util.nullBlankHyphenToNull(wt1.getProjectcode()) : "", true)).append("\",");	//プロジェクトコード
				csv.append("\"").append(Util.doubleQuoteEscape(wt1 != null ? Util.nullBlankHyphenToNull(wt1.getCustomername()) : "", true)).append("\",");	//客先
				csv.append("\"").append(Util.doubleQuoteEscape(wt1 != null ? Util.nullBlankHyphenToNull(wt1.getCategoryname()) : "", true)).append("\",");	//カテゴリー
				csv.append("\"").append(Util.doubleQuoteEscape(dto.note, true)).append("\",");	//業務内容
				csv.append("\"").append("出退").append("\",");	//打刻区分
				if (!Util.isNullOrBlankOrHyphen(dto.inH) && !Util.isNullOrBlankOrHyphen(dto.outH)) {
					csv.append("\"").append(dto.inH).append(":").append(dto.inM).append("\",");	//出勤時刻
					csv.append("\"").append(dto.outH).append(":").append(dto.outM).append("\",");	//退勤時刻
					workTime = DateTime.GetTimeSpan(dto.inH + dto.inM, dto.outH + dto.outM, dto.breaktime);
				} else {
					csv.append("\"\",");	//出勤時刻
					csv.append("\"\",");	//退勤時刻
					workTime = -1;
				}
				csv.append("\"").append(dto.breaktime).append("\",");	//休憩時間
				csv.append("\"").append(DateTime.IntTimeToString(workTime)).append("\",");	//実働時間
				csv.append("\"").append(dto.fare).append("\",");	//交通費
				csv.replace(csv.length()-1, csv.length(), "\n");
			}
			//移動込み
			else {
				//各種別の実働時間を算出
				workTime = DateTime.GetTimeSpan(dto.inH + dto.inM, dto.outH + dto.outM, "0");	//全体時間
				workTime1 = DateTime.GetTimeSpan(dto.inH + dto.inM, dto.moveH + dto.moveM, "0");	//移動まで
				workTime2 = DateTime.GetTimeSpan(dto.moveH + dto.moveM, dto.outH + dto.outM, "0");	//移動から最後まで
				//時間割合を算出
				ratio = (double)workTime1 / (double)workTime;
				//各種別の休憩時間を割合算出(15分刻み化)
				breaktime = Util.toInt(dto.breaktime);
				breaktime1 = (int)(breaktime * ratio);
				breaktime2 = 0;
				if (breaktime1 % 15 == 0) {
					breaktime2 = breaktime - breaktime1;
				} else {
					//7以下は0
					if (breaktime1 <= 7) {
						breaktime1 = 0;
						breaktime2 = breaktime;
					//以降15刻みで近い方にする
					} else {
						for (int i = 0; ; i++) {	//無限回
							//15刻みの中間数字(0-[7]-15-[22]-30-[37]-45)の間にあるかどうかで判定
							//例）23 => 7~22[NG],22~37[OK] => 30
							if (i*15 + 7 <= breaktime1 && breaktime1 <= i*15 + 7 + 15) {
								breaktime1 = (i + 1) * 15;
								breaktime2 = breaktime - breaktime1;
								break;
							}
						}
					}
				}
				//各種別の交通費を割合算出
				fare = Util.toInt(dto.fare);
				fare1 = (int)Math.ceil((fare * ratio) - 0.5d);	//五捨五超入(0.5引いてから切り上げ)
				fare2 = fare - fare1;

				us = userMap.get(dto.uid);
				wt1 = worktypeMap.get(dto.wid);
				wt2 = worktypeMap.get(dto.wid_move);
				//出退レコード
				csv.append("\"").append(dto.tdate).append("\",");	//日付
				csv.append("\"").append(Util.doubleQuoteEscape(us != null ? Util.nullBlankHyphenToNull(us.getCode()) : "", true)).append("\",");	//社員番号
				csv.append("\"").append(Util.doubleQuoteEscape(dto.uname, true)).append("\",");	//氏名
				csv.append("\"").append(Util.doubleQuoteEscape(us != null ? Util.nullBlankHyphenToNull(us.getGroupname()) : "", true)).append("\",");	//グループ
				csv.append("\"").append(Util.doubleQuoteEscape(dto.wname, true)).append("\",");	//勤務種別
				csv.append("\"").append(Util.doubleQuoteEscape(wt1 != null ? Util.nullBlankHyphenToNull(wt1.getProjectcode()) : "", true)).append("\",");	//プロジェクトコード
				csv.append("\"").append(Util.doubleQuoteEscape(wt1 != null ? Util.nullBlankHyphenToNull(wt1.getCustomername()) : "", true)).append("\",");	//客先
				csv.append("\"").append(Util.doubleQuoteEscape(wt1 != null ? Util.nullBlankHyphenToNull(wt1.getCategoryname()) : "", true)).append("\",");	//カテゴリー
				csv.append("\"").append(Util.doubleQuoteEscape(dto.note, true)).append("\",");	//業務内容
				csv.append("\"").append("出退").append("\",");	//打刻区分
				if (!Util.isNullOrBlankOrHyphen(dto.inH) && !Util.isNullOrBlankOrHyphen(dto.moveH)) {
					csv.append("\"").append(dto.inH).append(":").append(dto.inM).append("\",");	//出勤時刻
					csv.append("\"").append(dto.moveH).append(":").append(dto.moveM).append("\",");	//退勤時刻
				} else {
					csv.append("\"\",");	//出勤時刻
					csv.append("\"\",");	//退勤時刻
				}
				csv.append("\"").append(breaktime1).append("\",");	//休憩時間
				csv.append("\"").append(DateTime.IntTimeToString(workTime1)).append("\",");	//実働時間
				csv.append("\"").append(fare1).append("\",");	//交通費
				csv.replace(csv.length()-1, csv.length(), "\n");
				//移動レコード
				csv.append("\"").append(dto.tdate).append("\",");	//日付
				csv.append("\"").append(Util.doubleQuoteEscape(us != null ? Util.nullBlankHyphenToNull(us.getCode()) : "", true)).append("\",");	//社員番号
				csv.append("\"").append(Util.doubleQuoteEscape(dto.uname, true)).append("\",");	//氏名
				csv.append("\"").append(Util.doubleQuoteEscape(us != null ? Util.nullBlankHyphenToNull(us.getGroupname()) : "", true)).append("\",");	//グループ
				csv.append("\"").append(Util.doubleQuoteEscape(dto.wname_move, true)).append("\",");	//勤務種別
				csv.append("\"").append(Util.doubleQuoteEscape(wt2 != null ? Util.nullBlankHyphenToNull(wt2.getProjectcode()) : "", true)).append("\",");	//プロジェクトコード
				csv.append("\"").append(Util.doubleQuoteEscape(wt2 != null ? Util.nullBlankHyphenToNull(wt2.getCustomername()) : "", true)).append("\",");	//客先
				csv.append("\"").append(Util.doubleQuoteEscape(wt2 != null ? Util.nullBlankHyphenToNull(wt2.getCategoryname()) : "", true)).append("\",");	//カテゴリー
				csv.append("\"").append(Util.doubleQuoteEscape(dto.note_move, true)).append("\",");	//業務内容
				csv.append("\"").append("移動").append("\",");	//打刻区分
				if (!Util.isNullOrBlankOrHyphen(dto.moveH) && !Util.isNullOrBlankOrHyphen(dto.outH)) {
					csv.append("\"").append(dto.moveH).append(":").append(dto.moveM).append("\",");	//出勤時刻
					csv.append("\"").append(dto.outH).append(":").append(dto.outM).append("\",");	//退勤時刻
				} else {
					csv.append("\"\",");	//出勤時刻
					csv.append("\"\",");	//退勤時刻
				}
				csv.append("\"").append(breaktime2).append("\",");	//休憩時間
				csv.append("\"").append(DateTime.IntTimeToString(workTime2)).append("\",");	//実働時間
				csv.append("\"").append(fare2).append("\",");	//交通費
				csv.replace(csv.length()-1, csv.length(), "\n");
			}
		}

		return csv.toString();
	}

	/**
	 * タイムカードJSON解析したリストを返します。
	 * @param showDataJSON
	 * @param year
	 * @param month
	 * @param oid
	 * @param uid
	 * @return
	 */
	protected List<DtoTimeCard> toTimecardList(String showDataJSON, int year, int month, int oid, int uid) {
		List<DtoTimeCard> dtos;
		if(showDataJSON != null) {
			// 表示行のみ
			try {
				// JSON -> class
				ObjectMapper mapper = new ObjectMapper();
				DtoTimeCardRoot root = mapper.readValue(showDataJSON, DtoTimeCardRoot.class);
				dtos = root.datas;
			} catch (IOException ex) {
				ex.printStackTrace();
				return null;
			}

		} else {
			// 年月検索
			try {
				TimeCardDAO tcd = new TimeCardDAO();
				String sUid = null;
				if(uid != -1) sUid = String.valueOf(uid);
				String ymJSON = tcd.findOfYearAndMonthForGrid(
						String.valueOf(oid), sUid, String.valueOf(year), String.valueOf(month));
				// JSON -> class
				ObjectMapper mapper = new ObjectMapper();
				DtoTimeCardRoot root = mapper.readValue(ymJSON, DtoTimeCardRoot.class);
				dtos = root.datas;
			} catch (SQLException ex) {
				ex.printStackTrace();
				return null;
			} catch (IOException ex) {
				ex.printStackTrace();
				return null;
			}
		}
		return dtos;
	}

	/**
	 * 作業内容のCSV出力
	 * <br>
	 * ※専用処理なのでべた書き
	 * "日付", "社員番号", "氏名", "グループ", "部署", "勤務種別", "プロジェクトコード",  "業務内容", "業務内容ID", "作業時間", "交通費(時間配分)"<br>
	 * @param year
	 * @param month
	 * @return
	 */
	public String getWorkListCSV(int year, int month) {
		DBPreparedStatementConnect db = new DBPreparedStatementConnect();
		db.open();
		String sql = "SELECT daily_work_result.tdate, users.code, users.name, usergroup.name AS usergroup, "
			+ "department.name AS department,worktype.name AS worktype, worktype.projectcode, "
			+ "daily_work_result.work_detail_id, work_detail.name AS work_detail, daily_work_result.work_minute, "
			+ "(SELECT SUM(temp.work_minute) AS totaltime FROM daily_work_result AS temp "
			+ "WHERE daily_work_result.oid = temp.oid AND daily_work_result.tdate = temp.tdate AND daily_work_result.uid = temp.uid), "
			+ "(SELECT SUM(timecard.fare) AS fare FROM timecard AS timecard "
			+ "WHERE daily_work_result.oid = timecard.oid AND daily_work_result.tdate = timecard.tdate AND daily_work_result.uid = timecard.uid) "
			+ "FROM " + DBinfo.TABLE_DAILY_WORK_RESULT + " AS daily_work_result "
			+ "INNER JOIN " + DBinfo.TABLE_USERS + " AS users "
			+ "ON users.oid = daily_work_result.oid AND users.uid = daily_work_result.uid "
			+ "INNER JOIN " + DBinfo.TABLE_GROUP + " AS usergroup "
			+ "ON users.oid = usergroup.oid AND users.usergroup = usergroup.id "
			+ "INNER JOIN " + DBinfo.TABLE_DEPARTMENT + " AS department "
			+ "ON users.oid = department.oid AND department.id = users.department "
			+ "INNER JOIN " + DBinfo.TABLE_WORKTYPE + " AS worktype "
			+ "ON users.oid = worktype.oid AND worktype.wid = daily_work_result.wid "
			+ "INNER JOIN " + DBinfo.TABLE_WORK_DETAIL + " AS work_detail "
			+ "ON users.oid = work_detail.oid AND work_detail.work_detail_id = daily_work_result.work_detail_id "
			+ "WHERE daily_work_result.oid = ? AND daily_work_result.tdate BETWEEN ? AND ? ORDER BY tdate, code;";

		StringBuilder csv = new StringBuilder();
		// CSVベース作成
		for(String row : ROWS_WORKLIST) {
			csv.append(row).append(",");
		}
		csv.replace(csv.length()-1, csv.length(), "\n");

		int from = year * 10000 + month * 100;
		int to = from + 99;
		String[] cols = { "tdate" , "code", "name", "usergroup", "department", "worktype", "projectcode",
				"work_detail", "work_detail_id", "work_minute" };
		try {
			ResultSet rs = db.execQuery(sql, Arrays.asList(oid, from, to));

			if (rs == null) {
				return csv.toString();
			}

			while(rs.next()) {
				for (String name : cols) {
					csv.append("\"").append(rs.getString(name)).append("\",");
				}
				// 交通費はその日の作業時間に応じて配分。小数点以下切り捨て
				double total = rs.getDouble("totaltime");
				double fare = rs.getDouble("work_minute") / total * rs.getDouble("fare");
				csv.append("\"").append((int)Math.floor(fare)).append("\"\n");
			}
			return csv.toString();
		} catch (SQLException e) {
			e.printStackTrace();
			return csv.toString();
		} finally {
			db.close();
		}
	}

	/**
	 * 出勤と退勤が行われている場合はfalseを返します。
	 * @param dto
	 * @return
	 */
	protected boolean isTimecardUnfinished(DtoTimeCard dto) {
		if(dto == null) {
			return true;
		}
		if(Util.isNullOrBlankOrHyphen(dto.inH)) {
			return true;
		}
		if(Util.isNullOrBlankOrHyphen(dto.outH)) {
			return true;
		}
		return false;
	}

	/**
	 * 業務時間が長い方の内容を取得します。<br>
	 * 未出勤の場合は移動を行っていれば移動の内容を優先します。<br>
	 * 出勤のみの場合は全て空文字となります。
	 * @param dto
	 * @return
	 */
	protected TimecardWorkContents getTimecardWorkContents(DtoTimeCard dto) {
		TimecardWorkContents wc = new TimecardWorkContents();
		if(dto == null) {
			return null;
		}
		boolean noin = Util.isNullOrBlankOrHyphen(dto.inH);
		boolean nomove = Util.isNullOrBlankOrHyphen(dto.moveH);
		boolean noout = Util.isNullOrBlankOrHyphen(dto.outH);
		if(!noin && !nomove && !noout) {
			// 業務時間が長い方を取る
			int in = Util.toInt(dto.inH + dto.inM);
			int move = Util.toInt(dto.moveH + dto.moveM);
			int out = Util.toInt(dto.outH + dto.outM);
			if((out - move) > (move - in)) {
				wc.wname = dto.wname_move;
				wc.note = dto.note_move;
				wc.wcount = dto.wcount_move;
				wc.rcount = dto.rcount_move;
			} else {
				wc.wname = dto.wname;
				wc.note = dto.note;
				wc.wcount = dto.wcount;
				wc.rcount = dto.rcount;
			}
		} else if(!nomove) {
			// 未出勤では長い方を取れないので移動の内容を優先
			wc.wname = dto.wname;
			wc.note = dto.note;
			wc.wcount = dto.wcount;
			wc.rcount = dto.rcount;
		} else if(!noout) {
			// 退勤の内容
			wc.wname = dto.wname;
			wc.note = dto.note;
			wc.wcount = dto.wcount;
			wc.rcount = dto.rcount;
		} else {
			// 出勤のみの場合はすべて空文字
			wc.wname = "";
			wc.note = "";
			wc.wcount = "";
			wc.rcount = "";
		}
		return wc;
	}
	protected class TimecardWorkContents {
		String wname;
		String note;
		String wcount;
		String rcount;
	}

/**********************
 * Users
 **********************/

	protected String getUsersCSVBase1(String showDataJSON) {
		// JSON解析
		if (showDataJSON == null) return null;
		List<Users> list = JsonUtil.ToObjectList(showDataJSON, Users.class);
		if(list == null) {
			return null;
		}
		// CSVベース作成
		StringBuilder csv = new StringBuilder();
		for(String row : ROWS_USERS_1) {
			csv.append(row).append(",");
		}
		ReplaceEndComma(csv);
		// 値を入れていく
		for(Users item : list) {
			csv.append(ToCSVField(item.getName()));
			csv.append(ToCSVField(item.getKana()));
			csv.append(ToCSVField(item.getEmploymentname()));
			csv.append(ToCSVField(item.getWorklocationname()));
			csv.append(ToCSVField(item.getDepartmentname()));
			csv.append(ToCSVField(item.getCode()));
			csv.append(ToCSVField(item.issignupidm));
			csv.append(ToCSVField(item.getMailladd()));
			csv.append(ToCSVField(item.mailenabletext));
			csv.append(ToCSVField(item.getGroupname()));
			csv.append(ToCSVField(item.hiredatetext));
			csv.append(ToCSVField(item.getPaidvacationdays()));
			csv.append(ToCSVField(item.admintext));
			csv.append(ToCSVField(item.isloginok));
			csv.append(ToCSVField(item.idatetext));
			csv.append(ToCSVField(item.udatetext));
			// 末尾のカンマを削除しつつ改行
			ReplaceEndComma(csv);
		}
		return csv.toString();
	}

/**********************
 * Worktype
 **********************/

	protected String getWorktypeCSVBase1(String showDataJSON) {
		// JSON解析
		if (showDataJSON == null) return null;
		List<Worktype> list = JsonUtil.ToObjectList(showDataJSON, Worktype.class);
		if(list == null) {
			return null;
		}
		// CSVベース作成
		StringBuilder csv = new StringBuilder();
		for(String row : ROWS_WORKTYPE_1) {
			csv.append(row).append(",");
		}
		ReplaceEndComma(csv);
		// 値を入れていく
		for(Worktype item : list) {
			csv.append(ToCSVField(item.getName()));
			csv.append(ToCSVField(item.getKana()));
			csv.append(ToCSVField(item.getProjectcode()));
			csv.append(ToCSVField(item.getCustomername()));
			csv.append(ToCSVField(item.getCategoryname()));
			csv.append(ToCSVField(item.getManagername()));
			csv.append(ToCSVField(item.getNote()));
			csv.append(ToCSVField(item.valid));
			csv.append(ToCSVField(item.idatetext));
			csv.append(ToCSVField(item.udatetext));
			// 末尾のカンマを削除しつつ改行
			ReplaceEndComma(csv);
		}
		return csv.toString();
	}
/*
	protected String getWorktypeCSVBase1(String showDataJSON) {
		// JSON解析
		List<DtoWorktype> dtos = this.toWorktypeList(showDataJSON);
		if(dtos == null) {
			return null;
		}
		// CSVベース作成
		StringBuilder csv = new StringBuilder();
		for(String row : ROWS_WORKTYPE_1) {
			csv.append(row).append(",");
		}
		csv.replace(csv.length()-1, csv.length(), "\n");
		// 値を入れていく
		for(DtoWorktype dto : dtos) {
			// 勤務種別名
			csv.append("\"").append(Util.doubleQuoteEscape(dto.name)).append("\",");
			// カナ
			csv.append("\"").append(Util.doubleQuoteEscape(dto.kana)).append("\",");
			// 登録日
			csv.append("\"").append(Util.doubleQuoteEscape(dto.idate)).append("\",");
			// 登録状況
			csv.append("\"").append(Util.doubleQuoteEscape(dto.valid)).append("\",");
			// 末尾のカンマを削除しつつ改行
			csv.replace(csv.length()-1, csv.length(), "\n");
		}

		return csv.toString();
	}*/
	/**
	 * 勤務種別一覧JSONを解析した結果を返します。
	 * @param showDataJSON
	 * @return
	 */
	protected List<DtoWorktype> toWorktypeList(String showDataJSON) {
		List<DtoWorktype> dtos;
		if(showDataJSON == null) {
			return null;
		}
		try {
			// JSON -> class
			ObjectMapper mapper = new ObjectMapper();
			dtos = mapper.readValue(showDataJSON, new TypeReference<List<DtoWorktype>>(){});
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}

		return dtos;
	}

/**********************
 * Unit
 **********************/

	protected String getUnitCSVBase1(String showDataJSON) {
		// JSON解析
		List<DtoUnit> dtos = this.toUnitList(showDataJSON);
		if(dtos == null) {
			return null;
		}
		// CSVベース作成
		StringBuilder csv = new StringBuilder();
		for(String row : ROWS_UNIT_1) {
			csv.append(row).append(",");
		}
		csv.replace(csv.length()-1, csv.length(), "\n");
		// 値を入れていく
		for(DtoUnit dto : dtos) {
			// 端末名
			csv.append("\"").append(Util.doubleQuoteEscape(dto.name, true)).append("\",");
			// 端末ID
			csv.append("\"").append(Util.doubleQuoteEscape(dto.unitviewid, true)).append("\",");
			// 登録日
			csv.append("\"").append(Util.doubleQuoteEscape(dto.idate, true)).append("\",");
			// 登録状況
			csv.append("\"").append(Util.doubleQuoteEscape(dto.valid, true)).append("\",");
			// メモ
			csv.append("\"").append(Util.doubleQuoteEscape(dto.note, true)).append("\",");
			// 登録用パスワード
			csv.append("\"").append(Util.doubleQuoteEscape(dto.pass, true)).append("\",");
			// 末尾のカンマを削除しつつ改行
			csv.replace(csv.length()-1, csv.length(), "\n");
		}

		return csv.toString();
	}
	/**
	 * 勤務種別一覧JSONを解析した結果を返します。
	 * @param showDataJSON
	 * @return
	 */
	protected List<DtoUnit> toUnitList(String showDataJSON) {
		List<DtoUnit> dtos;
		if(showDataJSON == null) {
			return null;
		}
		try {
			// JSON -> class
			ObjectMapper mapper = new ObjectMapper();
			dtos = mapper.readValue(showDataJSON, new TypeReference<List<DtoUnit>>(){});
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}

		return dtos;
	}

/**********************
 * Department
 **********************/

	protected String getDepartmentCSVBase1(String showDataJSON) {
		// JSON解析
		if (showDataJSON == null) return null;
		List<Department> list = JsonUtil.ToObjectList(showDataJSON, Department.class);
		if(list == null) {
			return null;
		}
		// ユーザ一覧取得
		BaseDAO.QueryResult<Users> ur;
		try {
			ur = UsersDAO.FindOfOid(String.valueOf(this.oid));
			if (ur == null) return null;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
		final List<Users> users = ur.List;
		// ユーザ検索ラムダ（未発見時ブランク返す）
		Function<Integer, String> findUser = (Integer manager) -> {
			for (Users user : users) {
				if (manager == user.getUid()) return user.getName();
			}
			return "";
		};
		// CSVベース作成
		StringBuilder csv = new StringBuilder();
		for(String row : ROWS_DEPARTMENT_1) {
			csv.append(row).append(",");
		}
		ReplaceEndComma(csv);
		// 値を入れていく
		for(Department item : list) {
			csv.append(ToCSVField(item.getName()));
			csv.append(ToCSVField(item.getKana()));
			csv.append(ToCSVField(findUser.apply(item.getManager())));
			csv.append(ToCSVField(item.valid));
			csv.append(ToCSVField(item.getNote()));
			csv.append(ToCSVField(DateTime.ToDateString(item.getIdate())));
			csv.append(ToCSVField(DateTime.ToDateString(item.getUdate())));
			// 末尾のカンマを削除しつつ改行
			ReplaceEndComma(csv);
		}
		return csv.toString();
	}

/**********************
 * Employment
 **********************/

	protected String getEmploymentCSVBase1(String showDataJSON) {
		// JSON解析
		if (showDataJSON == null) return null;
		List<Employment> list = JsonUtil.ToObjectList(showDataJSON, Employment.class);
		if(list == null) {
			return null;
		}
		// CSVベース作成
		StringBuilder csv = new StringBuilder();
		for(String row : ROWS_EMPLOYMENT_1) {
			csv.append(row).append(",");
		}
		ReplaceEndComma(csv);
		// 値を入れていく
		for(Employment item : list) {
			csv.append(ToCSVField(item.getName()));
			csv.append(ToCSVField(item.getKana()));
			csv.append(ToCSVField(item.valid));
			csv.append(ToCSVField(item.getNote()));
			csv.append(ToCSVField(DateTime.ToDateString(item.getIdate())));
			csv.append(ToCSVField(DateTime.ToDateString(item.getUdate())));
			// 末尾のカンマを削除しつつ改行
			ReplaceEndComma(csv);
		}
		return csv.toString();
	}

/**********************
 * Worklocation
 **********************/

	protected String getWorklocationCSVBase1(String showDataJSON) {
		// JSON解析
		if (showDataJSON == null) return null;
		List<Worklocation> list = JsonUtil.ToObjectList(showDataJSON, Worklocation.class);
		if(list == null) {
			return null;
		}
		// CSVベース作成
		StringBuilder csv = new StringBuilder();
		for(String row : ROWS_WORKLOCATION_1) {
			csv.append(row).append(",");
		}
		ReplaceEndComma(csv);
		// 値を入れていく
		for(Worklocation item : list) {
			csv.append(ToCSVField(item.getName()));
			csv.append(ToCSVField(item.getKana()));
			csv.append(ToCSVField(item.valid));
			csv.append(ToCSVField(item.getNote()));
			csv.append(ToCSVField(DateTime.ToDateString(item.getIdate())));
			csv.append(ToCSVField(DateTime.ToDateString(item.getUdate())));
			// 末尾のカンマを削除しつつ改行
			ReplaceEndComma(csv);
		}
		return csv.toString();
	}

/**********************
 * Group
 **********************/

	protected String getGroupCSVBase1(String showDataJSON) {
		// JSON解析
		if (showDataJSON == null) return null;
		List<Group> list = JsonUtil.ToObjectList(showDataJSON, Group.class);
		if(list == null) {
			return null;
		}
		// CSVベース作成
		StringBuilder csv = new StringBuilder();
		for(String row : ROWS_GROUP_1) {
			csv.append(row).append(",");
		}
		ReplaceEndComma(csv);
		// 値を入れていく
		for(Group item : list) {
			csv.append(ToCSVField(item.getName()));
			csv.append(ToCSVField(item.getKana()));
			csv.append(ToCSVField(item.valid));
			csv.append(ToCSVField(item.getNote()));
			csv.append(ToCSVField(DateTime.ToDateString(item.getIdate())));
			csv.append(ToCSVField(DateTime.ToDateString(item.getUdate())));
			// 末尾のカンマを削除しつつ改行
			ReplaceEndComma(csv);
		}
		return csv.toString();
	}

/**********************
 * Customer
 **********************/

	protected String getCustomerCSVBase1(String showDataJSON) {
		// JSON解析
		if (showDataJSON == null) return null;
		List<Customer> list = JsonUtil.ToObjectList(showDataJSON, Customer.class);
		if(list == null) {
			return null;
		}
		// CSVベース作成
		StringBuilder csv = new StringBuilder();
		for(String row : ROWS_CUSTOMER_1) {
			csv.append(row).append(",");
		}
		ReplaceEndComma(csv);
		// 値を入れていく
		for(Customer item : list) {
			csv.append(ToCSVField(item.getName()));
			csv.append(ToCSVField(item.getKana()));
			csv.append(ToCSVField(item.valid));
			csv.append(ToCSVField(item.getNote()));
			csv.append(ToCSVField(DateTime.ToDateString(item.getIdate())));
			csv.append(ToCSVField(DateTime.ToDateString(item.getUdate())));
			// 末尾のカンマを削除しつつ改行
			ReplaceEndComma(csv);
		}
		return csv.toString();
	}

/**********************
 * Category
 **********************/

	protected String getCategoryCSVBase1(String showDataJSON) {
		// JSON解析
		if (showDataJSON == null) return null;
		List<Category> list = JsonUtil.ToObjectList(showDataJSON, Category.class);
		if(list == null) {
			return null;
		}
		// CSVベース作成
		StringBuilder csv = new StringBuilder();
		for(String row : ROWS_CATEGORY_1) {
			csv.append(row).append(",");
		}
		ReplaceEndComma(csv);
		// 値を入れていく
		for(Category item : list) {
			csv.append(ToCSVField(item.getName()));
			csv.append(ToCSVField(item.getKana()));
			csv.append(ToCSVField(item.valid));
			csv.append(ToCSVField(item.getNote()));
			csv.append(ToCSVField(DateTime.ToDateString(item.getIdate())));
			csv.append(ToCSVField(DateTime.ToDateString(item.getUdate())));
			// 末尾のカンマを削除しつつ改行
			ReplaceEndComma(csv);
		}
		return csv.toString();
	}



	/**
	 * 指定値をダブルクォーテーションで囲み、末尾にカンマを追加した文字列を返します。（ "value", ）<br>
	 * 値にダブルクォーテーションが含まれている場合、それをUtil.doubleQuoteEscape()でエスケープします。
	 * @param value
	 * @return
	 */
	protected String ToCSVField(String value) {
		return String.format("\"%s\",", Util.doubleQuoteEscape(value, true));
	}

	/**
	 * 指定値をダブルクォーテーションで囲み、末尾にカンマを追加した文字列を返します。（ "value", ）
	 * @param value
	 * @return
	 */
	protected String ToCSVField(int value) {
		return String.format("\"%d\",", value);
	}

	/**
	 * 指定値をダブルクォーテーションで囲み、末尾にカンマを追加した文字列を返します。（ "value", ）
	 * @param value
	 * @return
	 */
	protected String ToCSVField(boolean value) {
		return String.format("\"%b\",", value);
	}

	/**
	 * 末尾のカンマを改行コードに変換します。
	 * @param csv
	 * @return
	 */
	protected StringBuilder ReplaceEndComma(StringBuilder csv) {
		if (csv == null) return null;
		return csv.replace(csv.length()-1, csv.length(), "\n");
	}

}
