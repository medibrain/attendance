package organization;

public class DtoTimeCard {
	public String id;
	public String oname;
	public String uname;
	public String ukana;
	public String tdate;
	public String inH;
	public String inM;
	public String moveH;
	public String moveM;
	public String outH;
	public String outM;
	public String wname;
	public String note;
	public String wcount;
	public String rcount;
	public String wname_move;
	public String note_move;
	public String wcount_move;
	public String rcount_move;
	public String breaktime;
	public String fare;
	public String oid;
	public String uid;
	public String times;
	public String wid;
	public String wid_move;
	public String dayofweek;
}
