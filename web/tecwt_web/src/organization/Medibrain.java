package organization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import db.BaseDAO;
import db.UsersDAO;
import db.WorktypeDAO;
import table.Users;
import table.Worktype;
import util.DateTime;
import util.Util;

public class Medibrain extends Organization {

	protected static final String[] HEADERS_INCOMESTATEMENT = {
		"日付","社員番号","氏名","グループ","勤務種別","プロジェクトコード","客先","カテゴリー","業務内容","打刻区分","出勤時刻","退勤時刻","休憩時間","実働時間","交通費","WID","処理数","申出数"
	};

	public Medibrain(String uri, int oid, String name) {
		super(uri, oid, name);
	}

	@Override
	public String getTimecardCSV(String showDataJSON, int year, int month, int uid, boolean unfinishedable) {
		// JSON解析
		List<DtoTimeCard> dtos = this.toTimecardList(showDataJSON, year, month, this.oid, uid);
		// 社員番号解析
		List<String> tcUserList = new ArrayList<String>();	// 出力範囲内のユーザIDリスト
		for(DtoTimeCard dto : dtos) {
			if(!tcUserList.contains(dto.uid)) {
				tcUserList.add(dto.uid);
			}
 		}
		Map<Integer, String> codeList = new HashMap<Integer, String>();	// 社員番号リスト
		try {
			BaseDAO.QueryResult<Users> qr = UsersDAO.FindOfUids(String.valueOf(this.oid), tcUserList);
			Users user;
			for(Object obj : qr.List) {
				user = (Users)obj;
				codeList.put(user.getUid(), user.getCode());
			}
		} catch (Exception ex) {
			return null;
		}
		// CSVベース作成
		StringBuilder csv = new StringBuilder();
		for(String row : ROWS_TIMECARD_2) {
			csv.append(row).append(",");
		}
		csv.replace(csv.length()-1, csv.length(), "\n");
		// 値を入れていく
		TimecardWorkContents wc;
		for(DtoTimeCard dto : dtos) {
			if(unfinishedable && this.isTimecardUnfinished(dto)) continue;
			wc = getTimecardWorkContents(dto);
			// 社員番号
			String code = codeList.get(Util.toInt(dto.uid));
			csv.append("\"").append(code).append("\",");
			// 氏名
			csv.append("\"").append(Util.doubleQuoteEscape(dto.uname, true)).append("\",");
			// 日付
			csv.append("\"").append(dto.tdate).append("\",");
			// 出勤時刻
			csv.append("\"").append(dto.inH).append(":").append(dto.inM).append("\",");
			// 退勤時刻
			csv.append("\"").append(dto.outH).append(":").append(dto.outM).append("\",");
			// 休憩時間
			csv.append("\"").append(dto.breaktime).append("\",");
			// 勤務種別
			csv.append("\"").append(Util.doubleQuoteEscape(wc.wname, true)).append("\",");
			// 業務内容
			csv.append("\"").append(Util.doubleQuoteEscape(wc.note, true)).append("\",");
			// 交通費
			csv.append("\"").append(dto.fare).append("\",");
			// 処理数
			csv.append("\"").append(wc.wcount).append("\",");
			// 申出数
			csv.append("\"").append(wc.rcount).append("\",");

			// 末尾のカンマを削除しつつ改行
			csv.replace(csv.length()-1, csv.length(), "\n");
		}

		return csv.toString();
	}

	@Override
	public String getIncomeStatementCSV(String showDataJSON, int year, int month, int uid, boolean unfinishedable) {
		// JSON解析
		List<DtoTimeCard> dtos = this.toTimecardList(showDataJSON, year, month, oid, uid);
		if(dtos == null) {
			return null;
		}

		// ユーザマップ作成（グループも含まれる）
		List<String> tcUserList = new ArrayList<String>();	// 出力範囲内のユーザIDリスト
		for(DtoTimeCard dto : dtos) {
			if(!tcUserList.contains(dto.uid)) {
				tcUserList.add(dto.uid);
			}
 		}
		Map<String, Users> userMap = new HashMap<String, Users>();	//ここでIDをStringにしておく
		try {
			BaseDAO.QueryResult<Users> qr = UsersDAO.FindOfUids(String.valueOf(oid), tcUserList);
			Users user;
			for (Object obj : qr.List) {
				user = (Users)obj;
				userMap.put(String.valueOf(user.getUid()), user);
			}
		} catch (Exception ex) {
			return null;
		}

		// 勤務種別マップ作成（客先、カテゴリーも含まれる）
		List<String> tcWorktypeList = new ArrayList<String>();	// 出力範囲内の勤務種別IDリスト
		for(DtoTimeCard dto : dtos) {
			if(!Util.isNullOrBlank(dto.wid) && !tcWorktypeList.contains(dto.wid)) {
				tcWorktypeList.add(dto.wid);
			}
			if(!Util.isNullOrBlank(dto.wid_move) && !tcWorktypeList.contains(dto.wid_move)) {
				tcWorktypeList.add(dto.wid_move);
			}
 		}
		Map<String, Worktype> worktypeMap = new HashMap<String, Worktype>();	//ここでIDをStringにしておく
		try {
			BaseDAO.QueryResult<Worktype> qr = WorktypeDAO.findOfWids(String.valueOf(oid), tcWorktypeList);
			Worktype worktype;
			for (Object obj : qr.List) {
				worktype = (Worktype)obj;
				worktypeMap.put(String.valueOf(worktype.getWid()), worktype);
			}
		} catch (Exception ex) {
			return null;
		}

		// CSVベース作成
		StringBuilder csv = new StringBuilder();
		for(String header : HEADERS_INCOMESTATEMENT) {
			csv.append(header).append(",");
		}
		csv.replace(csv.length()-1, csv.length(), "\n");	//末尾カンマ削除+改行挿入
		// 値を入れていく
		Users us;
		Worktype wt1;
		Worktype wt2;
		double ratio;
		int workTime;
		int workTime1;
		int workTime2;
		int breaktime;
		int breaktime1;
		int breaktime2;
		int fare;
		int fare1;
		int fare2;
		for(DtoTimeCard dto : dtos) {
			if(unfinishedable && isTimecardUnfinished(dto)) continue;

			//出勤・退勤のみ
			if (Util.isNullOrBlankOrHyphen(dto.wname_move)) {
				us = userMap.get(dto.uid);
				wt1 = worktypeMap.get(dto.wid);
				csv.append("\"").append(dto.tdate).append("\",");	//日付
				csv.append("\"").append(Util.doubleQuoteEscape(us != null ? Util.nullBlankHyphenToNull(us.getCode()) : "", true)).append("\",");	//社員番号
				csv.append("\"").append(Util.doubleQuoteEscape(dto.uname, true)).append("\",");	//氏名
				csv.append("\"").append(Util.doubleQuoteEscape(us != null ? Util.nullBlankHyphenToNull(us.getGroupname()) : "", true)).append("\",");	//グループ
				csv.append("\"").append(Util.doubleQuoteEscape(dto.wname, true)).append("\",");	//勤務種別
				csv.append("\"").append(Util.doubleQuoteEscape(wt1 != null ? Util.nullBlankHyphenToNull(wt1.getProjectcode()) : "", true)).append("\",");	//プロジェクトコード
				csv.append("\"").append(Util.doubleQuoteEscape(wt1 != null ? Util.nullBlankHyphenToNull(wt1.getCustomername()) : "", true)).append("\",");	//客先
				csv.append("\"").append(Util.doubleQuoteEscape(wt1 != null ? Util.nullBlankHyphenToNull(wt1.getCategoryname()) : "", true)).append("\",");	//カテゴリー
				csv.append("\"").append(Util.doubleQuoteEscape(dto.note, true)).append("\",");	//業務内容
				csv.append("\"").append("出退").append("\",");	//打刻区分
				if (!Util.isNullOrBlankOrHyphen(dto.inH) && !Util.isNullOrBlankOrHyphen(dto.outH)) {
					csv.append("\"").append(dto.inH).append(":").append(dto.inM).append("\",");	//出勤時刻
					csv.append("\"").append(dto.outH).append(":").append(dto.outM).append("\",");	//退勤時刻
					workTime = DateTime.GetTimeSpan(dto.inH + dto.inM, dto.outH + dto.outM, dto.breaktime);
				} else {
					csv.append("\"\",");	//出勤時刻
					csv.append("\"\",");	//退勤時刻
					workTime = -1;
				}
				csv.append("\"").append(dto.breaktime).append("\",");	//休憩時間
				csv.append("\"").append(DateTime.IntTimeToString(workTime)).append("\",");	//実働時間
				csv.append("\"").append(dto.fare).append("\",");	//交通費
				csv.append("\"").append(dto.wid).append("\",");	//勤務種別ID
				csv.append("\"").append(dto.wcount).append("\",");	//処理数
				csv.append("\"").append(dto.rcount).append("\",");	//申出数
				csv.replace(csv.length()-1, csv.length(), "\n");
			}
			//移動込み
			else {
				//各種別の実働時間を算出
				workTime = DateTime.GetTimeSpan(dto.inH + dto.inM, dto.outH + dto.outM, "0");	//全体時間
				workTime1 = DateTime.GetTimeSpan(dto.inH + dto.inM, dto.moveH + dto.moveM, "0");	//移動まで
				workTime2 = DateTime.GetTimeSpan(dto.moveH + dto.moveM, dto.outH + dto.outM, "0");	//移動から最後まで
				//時間割合を算出
				ratio = (double)workTime1 / (double)workTime;
				//各種別の休憩時間を割合算出(15分刻み化)
				breaktime = Util.toInt(dto.breaktime);
				breaktime1 = (int)(breaktime * ratio);
				breaktime2 = 0;
				if (breaktime1 % 15 == 0) {
					breaktime2 = breaktime - breaktime1;
				} else {
					//7以下は0
					if (breaktime1 <= 7) {
						breaktime1 = 0;
						breaktime2 = breaktime;
					//以降15刻みで近い方にする
					} else {
						for (int i = 0; ; i++) {	//無限回
							//15刻みの中間数字(0-[7]-15-[22]-30-[37]-45)の間にあるかどうかで判定
							//例）23 => 7~22[NG],22~37[OK] => 30
							if (i*15 + 7 <= breaktime1 && breaktime1 <= i*15 + 7 + 15) {
								breaktime1 = (i + 1) * 15;
								breaktime2 = breaktime - breaktime1;
								break;
							}
						}
					}
				}
				//各種別の交通費を割合算出
				fare = Util.toInt(dto.fare);
				fare1 = (int)Math.ceil((fare * ratio) - 0.5d);	//五捨五超入(0.5引いてから切り上げ)
				fare2 = fare - fare1;

				us = userMap.get(dto.uid);
				wt1 = worktypeMap.get(dto.wid);
				wt2 = worktypeMap.get(dto.wid_move);
				//出退レコード
				csv.append("\"").append(dto.tdate).append("\",");	//日付
				csv.append("\"").append(Util.doubleQuoteEscape(us != null ? Util.nullBlankHyphenToNull(us.getCode()) : "", true)).append("\",");	//社員番号
				csv.append("\"").append(Util.doubleQuoteEscape(dto.uname, true)).append("\",");	//氏名
				csv.append("\"").append(Util.doubleQuoteEscape(us != null ? Util.nullBlankHyphenToNull(us.getGroupname()) : "", true)).append("\",");	//グループ
				csv.append("\"").append(Util.doubleQuoteEscape(dto.wname, true)).append("\",");	//勤務種別
				csv.append("\"").append(Util.doubleQuoteEscape(wt1 != null ? Util.nullBlankHyphenToNull(wt1.getProjectcode()) : "", true)).append("\",");	//プロジェクトコード
				csv.append("\"").append(Util.doubleQuoteEscape(wt1 != null ? Util.nullBlankHyphenToNull(wt1.getCustomername()) : "", true)).append("\",");	//客先
				csv.append("\"").append(Util.doubleQuoteEscape(wt1 != null ? Util.nullBlankHyphenToNull(wt1.getCategoryname()) : "", true)).append("\",");	//カテゴリー
				csv.append("\"").append(Util.doubleQuoteEscape(dto.note, true)).append("\",");	//業務内容
				csv.append("\"").append("出退").append("\",");	//打刻区分
				if (!Util.isNullOrBlankOrHyphen(dto.inH) && !Util.isNullOrBlankOrHyphen(dto.moveH)) {
					csv.append("\"").append(dto.inH).append(":").append(dto.inM).append("\",");	//出勤時刻
					csv.append("\"").append(dto.moveH).append(":").append(dto.moveM).append("\",");	//退勤時刻
				} else {
					csv.append("\"\",");	//出勤時刻
					csv.append("\"\",");	//退勤時刻
				}
				csv.append("\"").append(breaktime1).append("\",");	//休憩時間
				csv.append("\"").append(DateTime.IntTimeToString(workTime1)).append("\",");	//実働時間
				csv.append("\"").append(fare1).append("\",");	//交通費
				csv.append("\"").append(dto.wid).append("\",");	//勤務種別ID
				csv.append("\"").append(dto.wcount).append("\",");	//処理数
				csv.append("\"").append(dto.rcount).append("\",");	//申出数
				csv.replace(csv.length()-1, csv.length(), "\n");
				//移動レコード
				csv.append("\"").append(dto.tdate).append("\",");	//日付
				csv.append("\"").append(Util.doubleQuoteEscape(us != null ? Util.nullBlankHyphenToNull(us.getCode()) : "", true)).append("\",");	//社員番号
				csv.append("\"").append(Util.doubleQuoteEscape(dto.uname, true)).append("\",");	//氏名
				csv.append("\"").append(Util.doubleQuoteEscape(us != null ? Util.nullBlankHyphenToNull(us.getGroupname()) : "", true)).append("\",");	//グループ
				csv.append("\"").append(Util.doubleQuoteEscape(dto.wname_move, true)).append("\",");	//勤務種別
				csv.append("\"").append(Util.doubleQuoteEscape(wt2 != null ? Util.nullBlankHyphenToNull(wt2.getProjectcode()) : "", true)).append("\",");	//プロジェクトコード
				csv.append("\"").append(Util.doubleQuoteEscape(wt2 != null ? Util.nullBlankHyphenToNull(wt2.getCustomername()) : "", true)).append("\",");	//客先
				csv.append("\"").append(Util.doubleQuoteEscape(wt2 != null ? Util.nullBlankHyphenToNull(wt2.getCategoryname()) : "", true)).append("\",");	//カテゴリー
				csv.append("\"").append(Util.doubleQuoteEscape(dto.note_move, true)).append("\",");	//業務内容
				csv.append("\"").append("移動").append("\",");	//打刻区分
				if (!Util.isNullOrBlankOrHyphen(dto.moveH) && !Util.isNullOrBlankOrHyphen(dto.outH)) {
					csv.append("\"").append(dto.moveH).append(":").append(dto.moveM).append("\",");	//出勤時刻
					csv.append("\"").append(dto.outH).append(":").append(dto.outM).append("\",");	//退勤時刻
				} else {
					csv.append("\"\",");	//出勤時刻
					csv.append("\"\",");	//退勤時刻
				}
				csv.append("\"").append(breaktime2).append("\",");	//休憩時間
				csv.append("\"").append(DateTime.IntTimeToString(workTime2)).append("\",");	//実働時間
				csv.append("\"").append(fare2).append("\",");	//交通費
				csv.append("\"").append(dto.wid_move).append("\",");	//勤務種別ID
				csv.append("\"").append(dto.wcount_move).append("\",");	//処理数
				csv.append("\"").append(dto.rcount_move).append("\",");	//申出数
				csv.replace(csv.length()-1, csv.length(), "\n");
			}
		}

		return csv.toString();
	}

	@Override
	public String getUsersCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getUsersCSVBase1(showDataJSON);
	}

	@Override
	public String getWorktypeCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getWorktypeCSVBase1(showDataJSON);
	}

	@Override
	public String getUnitCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getUnitCSVBase1(showDataJSON);
	}

	@Override
	public String getDepartmentCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getDepartmentCSVBase1(showDataJSON);
	}

	@Override
	public String getEmploymentCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getEmploymentCSVBase1(showDataJSON);
	}

	@Override
	public String getWorklocationCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getWorklocationCSVBase1(showDataJSON);
	}

	@Override
	public String getGroupCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getGroupCSVBase1(showDataJSON);
	}

	@Override
	public String getCustomerCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getCustomerCSVBase1(showDataJSON);
	}

	@Override
	public String getCategoryCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getCategoryCSVBase1(showDataJSON);
	}

}
