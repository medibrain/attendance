package organization;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Initialize implements ServletContextListener {

	private static List<Organization> oList;
	private static final String XML_PATH = "/WEB-INF/organization/organization.xml";

	/**
	 * WEBアプリケーション起動時に呼ばれる
	 */
	@Override
	public void contextInitialized(ServletContextEvent event) {
		// ルートまでのパス
		ServletContext context = event.getServletContext();
		String path = context.getRealPath(XML_PATH);
		// 各Organization初期化
		oList = initOrganizationInstance(path);
	}
	@Override
	public void contextDestroyed(ServletContextEvent event) {}

	/**
	 * 各Organizationインスタンス生成
	 * @param servletPath
	 * @return
	 */
	private static List<Organization> initOrganizationInstance(String realPath) {
		List<Organization> list = new ArrayList<Organization>();
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = factory.newDocumentBuilder();
			Document document = documentBuilder.parse(realPath);
			// organizations(root)
			Element root = document.getDocumentElement();
			NodeList rootChildren = root.getChildNodes();
			for(int i=0; i < rootChildren.getLength(); i++) {
				Node node = rootChildren.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					// organization
					Element element = (Element)node;
					if (element.getNodeName().equals("organization")) {
						NodeList organizationChildren = node.getChildNodes();
						String uri = null;
						int oid = -1;
						String orgClass = null;
						String orgName = element.getAttribute("name");
						for (int j=0; j < organizationChildren.getLength(); j++) {
							Node organizationNode = organizationChildren.item(j);
							if(organizationNode.getNodeType() == Node.ELEMENT_NODE) {
								if(organizationNode.getNodeName().equals("uri")) {
									// uri
									uri = organizationNode.getTextContent();
								} else if(organizationNode.getNodeName().equals("oid")) {
									// oid
									oid = Integer.parseInt(organizationNode.getTextContent());
								} else if(organizationNode.getNodeName().equals("class")) {
									// class
									orgClass = organizationNode.getTextContent();
								}
							}
						}
						// 各Organizationインスタンスを生成
						Class<?> clazz = Class.forName(orgClass);
						Constructor<?> constructor = clazz.getConstructor(String.class, int.class, String.class);
						Organization org = (Organization)constructor.newInstance(uri, oid, orgName);
						list.add(org);
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
		return list;
	}

	/**
	 * URLのorganization部分から該当するインスタンスを取得
	 * @param orgUri
	 * @return
	 */
	public static Organization getOrganization(String orgUri) {
		if(orgUri == null || orgUri.equals("")) {
			return null;
		}
		for(Organization org : oList) {
			if(org.uri.equals(orgUri)) {
				return org;
			}
		}
		return null;
	}
	/**
	 * oidから該当するインスタンスを取得
	 * @param orgUri
	 * @return
	 */
	public static Organization getOrganization(int oid) {
		for(Organization org : oList) {
			if(org.oid == oid) {
				return org;
			}
		}
		return null;
	}

}
