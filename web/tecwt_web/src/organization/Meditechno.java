package organization;



public class Meditechno extends Organization {

	public Meditechno(String uri, int oid, String name) {
		super(uri, oid, name);
	}

	@Override
	public String getTimecardCSV(String showDataJSON, int year, int month, int uid, boolean unfinishedable) {
		// 標準形式でOK
		return super.getTimecardCSVBase3(showDataJSON, year, month, uid, unfinishedable);
	}

	@Override
	public String getIncomeStatementCSV(String showDataJSON, int year, int month, int uid, boolean unfinishedable) {
		// 標準形式でOK
		return super.getIncomeStatementCSVBase1(showDataJSON, year, month, uid, unfinishedable);
	}

	@Override
	public String getUsersCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getUsersCSVBase1(showDataJSON);
	}

	@Override
	public String getWorktypeCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getWorktypeCSVBase1(showDataJSON);
	}

	@Override
	public String getUnitCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getUnitCSVBase1(showDataJSON);
	}

	@Override
	public String getDepartmentCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getDepartmentCSVBase1(showDataJSON);
	}

	@Override
	public String getEmploymentCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getEmploymentCSVBase1(showDataJSON);
	}

	@Override
	public String getWorklocationCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getWorklocationCSVBase1(showDataJSON);
	}

	@Override
	public String getGroupCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getGroupCSVBase1(showDataJSON);
	}

	@Override
	public String getCustomerCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getCustomerCSVBase1(showDataJSON);
	}

	@Override
	public String getCategoryCSV(String showDataJSON) {
		// 標準形式でOK
		return super.getCategoryCSVBase1(showDataJSON);
	}

}
