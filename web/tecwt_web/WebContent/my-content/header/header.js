/**
 * HEADER & TAB 部分のJavaScriptを定義します。
 */

// ヘッダー部分の項目を変更します。
function headerInit(tab) {

	// ログアウト先を指定
	$('#logout').attr("href", "logout");

	// タブ色変化
	if(tab == "my") {
		$('#tab_1').addClass('active');
	}
	else if(tab == "business") {
		$('#tab_2').addClass('active');
	}
	else if(tab == "department") {
		$('#tab_3').addClass('active');
		$('#tab_3_1').addClass('active');
	}
	else if(tab == "worklocation") {
		$('#tab_3').addClass('active');
		$('#tab_3_2').addClass('active');
	}
	else if(tab == "users") {
		$('#tab_3').addClass('active');
		$('#tab_3_3').addClass('active');
	}
	else if(tab == "employment") {
		$('#tab_3').addClass('active');
		$('#tab_3_4').addClass('active');
	}
	else if(tab == "group") {
		$('#tab_3').addClass('active');
		$('#tab_3_5').addClass('active');
	}
	else if(tab == "customer") {
		$('#tab_4').addClass('active');
		$('#tab_4_1').addClass('active');
	}
	else if(tab == "category") {
		$('#tab_4').addClass('active');
		$('#tab_4_2').addClass('active');
	}
	else if(tab == "worktype") {
		$('#tab_4').addClass('active');
		$('#tab_4_3').addClass('active');
		$('#tab_6').addClass('active');
	}
	else if(tab == "unit") {
		$('#tab_5').addClass('active');
	}
	else if(tab == "workdetaillist") {
		$('#tab_7').addClass('active');
	}
}

// ページロードされると実行
$(function () {

	// 時間表示
	$("#headerBox_datetime").text(new Date($.now()).toLocaleString())
	setInterval(function () {
		$("#headerBox_datetime").text(new Date($.now()).toLocaleString())
	}, 1000);

	//ロード or スクロールされると実行
	$(window).on('load scroll', function(){

		//ヘッダーの高さ分(80px)スクロールするとTAB,MENUにfixedを追加
		if ($(window).scrollTop() > 80) {
			$('.nav').addClass('nav_fixed');
			$('.MENU').addClass('menu_fixed');
		} else {
			//80px以下だとfixedクラスを削除
			$('.nav').removeClass('nav_fixed');
			$('.MENU').removeClass('menu_fixed');
		}
	});

	// タブクリック時の処理
	$('.tab li').click(function() {

		// タブ色を変更する
	    $('.tab li').removeClass('active');
	    $(this).addClass('active');

	    // イベントハンドラ
	    var tab = $(this).attr("id");
	    if(tab == "tab_0") {
	    	postForward("top", "");
	    }
	    else if(tab == "tab_1") {
	    	postForward("history?action=my", "");
	    }
	    else if(tab == "tab_2") {
	    	postForward("history?action=business", "");
	    }
	    else if(tab == "tab_3_1") {
	    	postForward("department", "");
	    }
	    else if(tab == "tab_3_2") {
	    	postForward("worklocation", "");
	    }
	    else if(tab == "tab_3_3") {
	    	postForward("users", "");
	    }
	    else if(tab == "tab_3_4") {
	    	postForward("employment", "");
	    }
	    else if(tab == "tab_3_5") {
	    	postForward("group", "");
	    }
	    else if(tab == "tab_4_1") {
	    	postForward("customer", "");
	    }
	    else if(tab == "tab_4_2") {
	    	postForward("category", "");
	    }
	    else if(tab == "tab_4_3") {
	    	postForward("worktype", "");
	    }
	    else if(tab == "tab_5") {
	    	postForward("unit", "");
	    }
	    else if(tab == "tab_6") {
	    	postForward("worktype", "");
	    }
	    else if(tab == "tab_7") {
	    	postForward("workdetaillist", "");
	    }

		return true;
    });

	// 指定したURLへPOST(submit)します。
	function postForward(url, data) {
		var $form = $('<form/>', {'action': url, 'method': 'post'});
        for(var key in data) {
                $form.append($('<input/>', {'type': 'hidden', 'name': key, 'value': data[key]}));
        }
        $form.appendTo(document.body);
        $form.submit();
	}

	var width = (window.innerWidth / 10) * 9;
	var tabWidth = (width / 6) - 10;
	$('.tab_li').css("width", tabWidth);
	$('.tab_li_parent').css("width", tabWidth);


	// マージン変更が必要なものを更新
	var height = $("#header_tab").height();	// タブ自体の高さ（隠れたulが存在する）
	$('.GRID').css("margin-top", height+"px");

});