function topInit() {
	// 出勤者一覧
	setWorkin();
	// 更新ボタンイベントハンドラ
	$("#reload").click(setWorkin);
	// 出勤者一覧を５分間隔で更新する
	setInterval("setWorkin()", 300000);
}

function setWorkin() {
	// 初期化
	$("#workin-list").empty();
	$("#workin-base .empty").remove();
	// 出勤者取得
	$.getJSON("query?action=query_timecard_today", "", function(json, status) {
		if(isSessionTimeout(json)) return;	// セッション切れ
		if(json.result == "success") {
			closeLoadingDialog();	// 待機ダイアログ閉じる
			var datas = json.datas;
			var base = $("#workin-list");
			if(datas.length == 0) {
				// 空メッセージ出力だけ
				base.after($("<div>").addClass("empty").text("出勤者なし"));
				return;
			}
			// 時刻が降順なので昇順に変換
			var intime;
			var name;
			var beforeTime = "";
			var times = [];
			var workinDatas = [];
			var outedCount = 0;
			for(var i=0; i<datas.length; i++) {
				intime = toTime(datas[i].inH, datas[i].inM);
				name = datas[i].uname;
				if(beforeTime != intime) {
					times.push(intime);
				}
				beforeTime = intime;
				if(isOuted(datas[i].outH, datas[i].outM)) {
					outedCount++;
				}
				workinDatas.push(
					{
						intime:intime,
						name:name,
						outH:datas[i].outH,
						outM:datas[i].outM,
						unitname:datas[i].unitname
					});
			}
			// 時刻と氏名
			var isFirst;
			var outtime;
			var tr;
			var td1;
			var td2;
			var td3;
			var td4;
			// 出勤者数
			tr = $("<tr>");
			var workinText = "出勤： " + datas.length + " 人" + "　　退勤済み： <span style='color:red'>" + outedCount + "</span> 人";
			td1 = $("<td>").attr("colspan", "4").addClass("rightTop").html($("<span>").html(workinText));
			tr.append(td1);
			base.append(tr);
			// データ
			for(var i=times.length-1; i>=0; i--) {	// 時刻は若い方が上
				// border
				tr = $("<tr>");
				td1 = $("<td>").attr("colspan", "4").html($("<hr>").addClass("border"));
				tr.append(td1);
				base.append(tr);
				// 出勤者一覧
				isFirst = true;
				for(var j=0; j<workinDatas.length; j++) {	// 名前順はそのまま維持
					if(times[i] == workinDatas[j].intime) {
						tr = $("<tr>");
						if(isFirst) {
							// 各時刻トップバッターのみ時刻表記
							td1 = $("<td>").addClass("left").text(times[i] + " 〜");
							isFirst = false;
						} else {
							td1 = $("<td>").addClass("left").text("");
						}
						if(isOuted(workinDatas[j].outH, workinDatas[j].outM)) {
							// 退勤済みの場合はCSS変えて時刻も付け加える
							td2 = $("<td>").addClass("centerOut").text(workinDatas[j].name);
							outtime = toTime(workinDatas[j].outH, workinDatas[j].outM);
							td3 = $("<td>").addClass("right").text("［" +outtime + "］");
							td4 = $("<td>").addClass("rightRightOut").text(workinDatas[j].unitname);
						} else {
							td2 = $("<td>").addClass("center").text(workinDatas[j].name);
							td3 = $("<td>").addClass("right").text("");
							td4 = $("<td>").addClass("rightRight").text(workinDatas[j].unitname);
						}
						tr.append(td1);
						tr.append(td2);
						tr.append(td3);
						tr.append(td4);
						base.append(tr);
					}
				}
			}
			// border
			tr = $("<tr>");
			td1 = $("<td>").attr("colspan", "4").html($("<hr>").addClass("border"));
			tr.append(td1);
			base.append(tr);
		} else {
			closeLoadingDialog();
		}
	});
}

/**
 * 09,00を9:00、10,00は10:00という具合に成型します。
 * @param hh
 * @param mm
 * @returns {String}
 */
function toTime(hh, mm) {
	var h;
	if(hh.lastIndexOf("0", 0) == 0) {
		h = hh.slice(1);
	} else {
		h = hh;
	}
	return h + ":" + mm;
}
/**
 * 退勤が打刻されているかを返します。
 * @param outH
 * @param outM
 * @returns {Boolean}
 */
function isOuted(outH, outM) {
	if(outH.indexOf("-") == -1) {
		return true;
	}
	if(outM.indexOf("-") == -1) {
		return true;
	}
	return false;
}