/**
 * 全ページから使用されるグローバル関数を定義
 */

/**
 * 結果の状態を確認します。戻り値がfalseの場合は正常です。<br>
 * エラーの場合はエラーページへ、セッション切れの場合はタイムアウトページへ遷移します。<br>
 * @param json
 * @returns {Boolean}
 */
function isErrorFunction(json) {
	if(json == null || json.result == "error") {
		$('<form/>', {action: "info/error", method: 'post'})
			.append($('<input/>', {type: 'hidden', name: 'error_message', value: json.message}))
			.appendTo(document.body)
			.submit();
		return true;
	}
	if(isSessionTimeout(json)) {
		return true;
	}
	return false;
}
/**
 * セッションの状態を確認します。戻り値がfalseの場合は正常です。<br>
 * trueの場合はタイムアウトページへ遷移します。<br>
 * @param json
 * @returns {Boolean}
 */
function isSessionTimeout(json) {
	if(json.result == "session_timeout") {
		$('<form/>', {action: "info/timeout", method: 'post'})
			.appendTo(document.body)
			.submit();
		return true;
	}
	return false;
}

/**
 * グリッドの幅がmin-widthより大きければoverflow-yプロパティを削除する<br>
 * 平常時横スクロール対策
 */
function gridWidthToFit() {
	var gridBaseW = $("#grid-base").width();
	var gridMinW = $("#grid").css("min-width");
	gridMinW = gridMinW.substring(0, gridMinW.indexOf("px"));
	if(gridBaseW > gridMinW) {
		$("#grid-base").css("overflow", "visible");
	}
}

/**
 * グローバル変数data配列を、引数のjsonで更新する。
 * @param json
 * @param status
 */
function dataLoad(json, status) {
	if(isErrorFunction(json)) return;
	// 初期化
	data = [];
	dataView.refresh();
	// 値挿入
	var datas = json.datas;
	insertData(datas);		// グリッドに合わせてデータを成型するグローバル関数insertDataを宣言する必要があります。
	// 描画更新
	dataView.beginUpdate();
	dataView.setItems(data_grid);
	dataView.endUpdate();
	grid.invalidate();
	// 待機ダイアログ閉じる
	closeLoadingDialog();
}

/**
 * 待機ダイアログを表示します。
 * @param message
 */
function showLoadingDialog(message) {
	// 待機ダイアログ
	$("#loading_dialog").html("<center><img src='../my-content/images/loading.gif'/></center>");
	$("#loading_dialog").dialog({
		title: message,
		modal: true,
		closeOnEscape: false,
		open: function(event, ui){
			$(".ui-dialog-titlebar-close").hide();
		},
		width: 80,
		height: 140
	});
}

/**
 * 待機ダイアログを閉じます。
 */
function closeLoadingDialog() {
	$("#loading_dialog").dialog('close');
}

/**
 * HTMLエンコードを行います。
 * @param text
 * @returns
 */
function htmlEncode(text) {
	return $('<div/>').text(text).html();
}

/**
 * HTMLデコードを行います。
 * @param value
 */
function htmlDecode(value){
	return $('<div/>').html(value).text();
}

/**
 * 改行コードを削除します。置換は行いません。
 * @param value
 */
function deleteCRLF(value) {
	if(value == null) return value;
	var replace = value;
	replace = replace.replace(/\r?\n/g,"");
	replace = replace.replace(/\n/g, "");
	return replace;
}

/**
 * 日付検索に必要な値が入力されているかを判定します。<br>
 * 入力されていない場合はダイアログを表示します。
 * @returns {Boolean}
 */
function isDateRangeBlank() {
	var date1 = $("#tdate_range1").val();
	var date2 = $("#tdate_range2").val();
	if(date1 == "" && date2 == "") {
		window.alert("日付が入力されていません。");
		return true;
	}
	return false;
}

/**
 * 年・月検索に必要な値が入力されているかを判定します。<br>
 * 入力されていない場合はダイアログを表示します。
 * @returns {Boolean}
 */
function isYearMonthBlank() {
	var year = $("#history_tdate_year").val();
	var month = $("#history_tdate_month").val();
	if(year == "--") {
		window.alert("年が指定されていません。年のみ、または年・月の両方を入力してください。");
		return true;
	}
	return false;
}

/**
 * 現在表示されている列を配列で取得します。
 */
function getShowRecords() {
	var queryData = [];
	var r;
	for(var key in data_grid) {	// data_grid=各ページのグローバル変数に指定されている操作用の配列。dataはDBから取得した生データなので触らない
		r = data_grid[key];
		if(filter(r)) {	// 表示行のみ取得	各種.js参照
			queryData.push(r);
		}
	}
	return queryData;
}

/**
 * enterで検索開始。サブミットは行われません。
 * @param event
 */
function doSearch(event) {
	if(event.keyCode == 13) {
		var target = event.data.target;
		if(target == "worktype") {
			searchTextWorktype(event);
		} else if(target == "users") {
			searchTextUsers(event);
		} else if(target == "department") {
			searchTextDepartment(event);
		}
		return false;
	}
}
/**
 * ユーザ文字列検索
 * @param event
 */
function searchTextUsers(event) {
	var box = event.data.box;
	var text = event.data.text;
	var message = event.data.message;
	var act = event.data.act;
	var users = event.data.users;
	if(act == "reset") {
		if(message) message.empty();
		text.val("");
		box.empty();
		for(var i=0; i<users.length; i++) {
			box.append($("<option>").val(users[i].uid).html(users[i].name));
		}
		return;
	}
	var str = text.val();
	// 検索結果を反映
	var result = searchNameAndKana(users, str);
	if(message) message.empty();
	if(message) message.text("ヒット "+result.length+"件");
	if(result.length != 0) {
		box.empty();
		for(var i=0; i<result.length; i++) {
			box.append($("<option>").val(result[i].uid).html(result[i].name));
		}
	}
}
/**
 * 勤務種別文字列検索
 * @param event
 */
function searchTextWorktype(event) {
	var box = event.data.box;
	var text = event.data.text;
	var message = event.data.message;
	var act = event.data.act;
	var worktypes = event.data.worktypes;
	var enableonly = event.data.enableonly;
	var str = text.val();
	var enable = enableonly.prop("checked");
	if(act == "reset") {
		if(message) message.empty();
		text.val("");
		box.empty();
		message.empty();
		box.append($("<option>").val("0").html("----"));	// 未選択許可
		for(var i=0; i<worktypes.length; i++) {
			box.append($("<option>").val(worktypes[i].wid).html(worktypes[i].name));
		}
		return;
	}
	if(str == "" && enable == false) {
		if(message) message.empty();
		text.val("");
		box.empty();
		message.empty();
		box.append($("<option>").val("0").html("----"));	// 未選択許可
		for(var i=0; i<worktypes.length; i++) {
			box.append($("<option>").val(worktypes[i].wid).html(worktypes[i].name));
		}
		return;
	}
	// 検索結果を反映
	var result = searchNameAndKana(worktypes, str, enable);
	if(message) message.empty();
	if(message) message.text("ヒット "+result.length+"件");
	if(result.length != 0) {
		box.empty();
		for(var i=0; i<result.length; i++) {
			box.append($("<option>").val(result[i].wid).html(result[i].name));
		}
	}
}
/**
 * 部署文字列検索
 * @param event
 */
function searchTextDepartment(event) {
	var box = event.data.box;
	var text = event.data.text;
	var message = event.data.message;
	var act = event.data.act;
	var departments = event.data.departments;
	if(act == "reset") {
		if(message) message.empty();
		text.val("");
		box.empty();
		message.empty();
		for(var i=0; i<departments.length; i++) {
			box.append($("<option>").val(departments[i].id).html(departments[i].name));
		}
		return;
	}
	var str = text.val();
	// 検索結果を反映
	var result = searchNameAndKana(departments, str);
	if(message) message.empty();
	if(message) message.text("ヒット "+result.length+"件");
	if(result.length != 0) {
		box.empty();
		for(var i=0; i<result.length; i++) {
			box.append($("<option>").val(result[i].id).html(result[i].name));
		}
	}
}

/**
 * 引数に渡したオブジェクト配列の中から指定した文字列を含むものを取得します。<br>
 * この関数は必ず配列を返します。(要素数０の可能性あり)
 * @param str
 * @returns
 */
function searchNameAndKana(jsonArray, str, enableonly) {
	var result = [];
	var katakana = toKatakana(str);
	for(var i=0; i<jsonArray.length; i++) {
		if(str == "") {//空文字の場合はEnableのみ確認
			if(enableonly) {
				if(jsonArray[i].enable == "true") {
					result.push(jsonArray[i]);
				}
			}
			continue;
		}
		if(jsonArray[i].name.indexOf(str) != -1) {
			if(enableonly) {
				if(jsonArray[i].enable == "true") {
					result.push(jsonArray[i]);
				}
			} else{
				result.push(jsonArray[i]);
			}
			continue;
		}
		if(jsonArray[i].kana.indexOf(katakana) != -1) {	// ひらがなはカタカナにして検索
			if(enableonly) {
				if(jsonArray[i].enable == true) {
					result.push(jsonArray[i]);
				}
			} else{
				result.push(jsonArray[i]);
			}
			continue;
		}
	}
	return result;
}

/**
 * ひらがなをカタカナに変換します。
 * @param hiragana
 * @returns
 */
function toKatakana(hiragana) {
	return hiragana.replace(/[\u3041-\u3096]/g, function(match) {
		var chr = match.charCodeAt(0) + 0x60;
		return String.fromCharCode(chr);
	});
}