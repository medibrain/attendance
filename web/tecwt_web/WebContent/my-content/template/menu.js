/**
 * メニューの開閉イベントに関する処理を定義します。<br>
 */

$(function () {

	// 検索メニューのヘッダー部分をクリック
	$("#queryHeader").on("click", function () {
		door($(this), $("#queryMenu"));	// 内容を開閉
	});
	// 追加メニュー
	$("#insertHeader").on("click", function () {
		door($(this), $("#insertMenu"));
	});
	// ユーザメニュー
	$("#myHeader").on("click", function () {
		door($(this), $("#myMenu"));
	});
	// CSVメニュー
	$("#csvHeader").on("click", function () {
		door($(this), $("#csvMenu"));
	});
	// freee連携メニュー
	$("#freeeHeader").on("click", function () {
		door($(this), $("#freeeMenu"));
	});
	// アプリメニュー
	$("#appHeader").on("click", function () {
		door($(this), $("#appMenu"));
	});
	function door(header, menu) {
		if(menu.is(":hidden")) {
			header.find(".plus").html("-");
			menu.show();
		} else {
			header.find(".plus").html("+");
			menu.hide();
		}
	}
});
/**
 * マイページ・過去勤怠タブのメニュー初期化
 */
function myhistoryMENUInit() {
	if($("#queryHeader") != null) {
		$("#queryHeader").find(".plus").html("+");
		$("#queryMenu").hide();
	}
	if($("#myHeader") != null) {
		$("#myHeader").find(".plus").html("-");
		$("#myMenu").show();
	}
	if($("#csvHeader") != null) {
		$("#csvHeader").find(".plus").html("-");
		$("#csvMenu").show();
	}
}
/**
 * 勤怠表タブのメニュー初期化
 */
function businessMENUInit() {
	if($("#queryHeader") != null) {
		$("#queryHeader").find(".plus").html("+");
		$("#queryMenu").hide();
	}
	if($("#insertHeader") != null) {
		$("#insertHeader").find(".plus").html("-");
		$("#insertMenu").show();
	}
	if($("#csvHeader") != null) {
		$("#csvHeader").find(".plus").html("-");
		$("#csvMenu").show();
	}
}
/**
 * ユーザタブのメニュー初期化
 */
function usersMENUInit() {
	if($("#queryHeader") != null) {
		$("#queryHeader").find(".plus").html("+");
		$("#queryMenu").hide();
	}
	if($("#insertHeader") != null) {
		$("#insertHeader").find(".plus").html("-");
		$("#insertMenu").show();
	}
	if($("#csvHeader") != null) {
		$("#csvHeader").find(".plus").html("-");
		$("#csvMenu").show();
	}
}
/**
 * 勤務種別タブのメニュー初期化
 */
function worktypeMENUInit() {
	if($("#queryHeader") != null) {
		$("#queryHeader").find(".plus").html("+");
		$("#queryMenu").hide();
	}
	if($("#insertHeader") != null) {
		$("#insertHeader").find(".plus").html("-");
		$("#insertMenu").show();
	}
	if($("#csvHeader") != null) {
		$("#csvHeader").find(".plus").html("-");
		$("#csvMenu").show();
	}
}
/**
 * 端末タブのメニュー初期化
 */
function unitMENUInit() {
	// [検索][新規登録]メニューのみ初回表示
	if($("#queryHeader") != null) {
		$("#queryHeader").find(".plus").html("+");
		$("#queryMenu").hide();
	}
	if($("#insertHeader") != null) {
		$("#insertHeader").find(".plus").html("-");
		$("#insertMenu").show();
	}
	if($("#appHeader") != null) {
		$("#appHeader").find(".plus").html("-");
		$("#appMenu").show();
	}
	if($("#csvHeader") != null) {
		$("#csvHeader").find(".plus").html("+");
		$("#csvMenu").hide();
	}
}
/**
 * 部署タブのメニュー初期化
 */
function departmentMENUInit() {
	// [検索][新規登録]メニューのみ初回表示
	if($("#queryHeader") != null) {
		$("#queryHeader").find(".plus").html("-");
		$("#queryMenu").show();
	}
	if($("#insertHeader") != null) {
		$("#insertHeader").find(".plus").html("-");
		$("#insertMenu").show();
	}
	if($("#csvHeader") != null) {
		$("#csvHeader").find(".plus").html("-");
		$("#csvMenu").show();
	}
}
/**
 * カテゴリータブのメニュー初期化
 */
function categoryMENUInit() {
	// [検索][新規登録]メニューのみ初回表示
	if($("#queryHeader") != null) {
		$("#queryHeader").find(".plus").html("-");
		$("#queryMenu").show();
	}
	if($("#insertHeader") != null) {
		$("#insertHeader").find(".plus").html("-");
		$("#insertMenu").show();
	}
	if($("#csvHeader") != null) {
		$("#csvHeader").find(".plus").html("-");
		$("#csvMenu").show();
	}
}
/**
 * 客先タブのメニュー初期化
 */
function customerMENUInit() {
	// [検索][新規登録]メニューのみ初回表示
	if($("#queryHeader") != null) {
		$("#queryHeader").find(".plus").html("-");
		$("#queryMenu").show();
	}
	if($("#insertHeader") != null) {
		$("#insertHeader").find(".plus").html("-");
		$("#insertMenu").show();
	}
	if($("#csvHeader") != null) {
		$("#csvHeader").find(".plus").html("-");
		$("#csvMenu").show();
	}
}
/**
 * 雇用形態タブのメニュー初期化
 */
function employmentMENUInit() {
	// [検索][新規登録]メニューのみ初回表示
	if($("#queryHeader") != null) {
		$("#queryHeader").find(".plus").html("-");
		$("#queryMenu").show();
	}
	if($("#insertHeader") != null) {
		$("#insertHeader").find(".plus").html("-");
		$("#insertMenu").show();
	}
	if($("#csvHeader") != null) {
		$("#csvHeader").find(".plus").html("-");
		$("#csvMenu").show();
	}
}
/**
 * 勤務地タブのメニュー初期化
 */
function worklocationMENUInit() {
	// [検索][新規登録]メニューのみ初回表示
	if($("#queryHeader") != null) {
		$("#queryHeader").find(".plus").html("-");
		$("#queryMenu").show();
	}
	if($("#insertHeader") != null) {
		$("#insertHeader").find(".plus").html("-");
		$("#insertMenu").show();
	}
	if($("#csvHeader") != null) {
		$("#csvHeader").find(".plus").html("-");
		$("#csvMenu").show();
	}
}
/**
 * グループタブのメニュー初期化
 */
function groupMENUInit() {
	// [検索][新規登録]メニューのみ初回表示
	if($("#queryHeader") != null) {
		$("#queryHeader").find(".plus").html("-");
		$("#queryMenu").show();
	}
	if($("#insertHeader") != null) {
		$("#insertHeader").find(".plus").html("-");
		$("#insertMenu").show();
	}
	if($("#csvHeader") != null) {
		$("#csvHeader").find(".plus").html("-");
		$("#csvMenu").show();
	}
}
/**
 * メニュー全表示
 */
function allHeaderShow() {
	if($("#queryHeader") != null) {
		$("#queryHeader").find(".plus").html("-");
		$("#queryMenu").show();
	}
	if($("#insertHeader") != null) {
		$("#insertHeader").find(".plus").html("-");
		$("#insertMenu").show();
	}
	if($("#myHeader") != null) {
		$("#myHeader").find(".plus").html("-");
		$("#myMenu").show();
	}
	if($("#csvHeader") != null) {
		$("#csvHeader").find(".plus").html("-");
		$("#csvMenu").show();
	}
}