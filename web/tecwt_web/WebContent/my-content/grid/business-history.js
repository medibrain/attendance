/**
 * My勤怠表についての処理を記述します。
 *
 * 参考URL
 * http://mleibman.github.io/SlickGrid/examples/example-header-row.html
 * https://github.com/mleibman/SlickGrid/wiki/DataView
 * https://github.com/hnakamur/slickgrid_example/blob/master/js/example.js
 */


// グローバル変数
var uid;


function historyInit(uid) {
    $.ajaxSetup({ cache: false });
	this.uid = uid;
	dataInit(false);	// util_history.js参照
	queryInit();
	timecardInsertInit();	// button_timecard_insert.js参照

	// Freeeダウンロード
	setupFreeeDownload();

	function queryInit() {
		$( "#query_tdate_yearmonth_button" ).on("click", queryYearMonth);
		$( "#history_query_user_button" ).on("click", queryUser);
		$( "#history_query_user_narrowing_button" ).on("click", narrowingUser);
		$( "#history_query_worktype_button" ).on("click", queryWorktype);
		$( "#history_query_worktype_narrowing_button" ).on("click", narrowingWorktype);
		$( "#query_tdate_range_button" ).on("click", queryDateRange);
		$( "#query_tdate_range_narrowing_button" ).on("click", narrowingDateRange);	// util_history.js参照
		$( "#query_tdate_range_narrowing_button" ).css("visibility", "visible");
		$( "#query_tdate_range_reset_button").on("click", resetDateRange);
		$( "#query_tdate_range_reset_button").css("visibility", "visible");
		$( "#query_tdate_nowmonth_button" ).on("click", queryNowMonth);
		$( "#query_tdate_nowmonth_button" ).css("visibility", "visible");
		$( "#query_tdate_lastmonth_button" ).on("click", queryLastMonth);
		$( "#query_tdate_lastmonth_button" ).css("visibility", "visible");
		$( "#query_tdate_nextmonth_button" ).on("click", queryNextMonth);
		$( "#query_tdate_nextmonth_button" ).css("visibility", "visible");
		$( "#query_tdate_today_button" ).on("click", queryToday);
		$( "#query_tdate_today_button" ).css("visibility", "visible");
		$( "#query_tdate_today_button" ).val("本日打刻");
		$( "#history_query_time_range_button" ).on("click", queryTimeRange);
		$( "#history_query_time_narrowing_button" ).on("click", narrowingTimeRange);
		$( "#history_query_time_reset_button" ).on("click", resetTimeRange);
		$( "#query_showall_button" ).on("click", queryShowAll);
	}

	/**
	 * [ユーザ]検索の処理
	 */
	function queryUser() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// form内容取出
		var text = $( "#history_query_user" ).serialize();
		// Ajax通信
		$.getJSON("query?action=query_timecard_user", text, dataLoad);
	}
	/**
	 * [ユーザ]絞込みの処理
	 */
	function narrowingUser() {
		showLoadingDialog("絞込み中...");

		var queryData = getShowRecords();	// util.js参照

		var resultData = [];
		var user = $("#history_user").val();
		for(var key in queryData) {
			if(user == queryData[key].uid) {
				resultData.push(queryData[key]);
			}
		}
		data_grid = resultData;

		// 描画更新
		dataView.beginUpdate();
		dataView.setItems(data_grid);
		dataView.endUpdate();
		grid.invalidate();

		closeLoadingDialog();
	}
	/**
	 * [業務種別]検索の処理
	 */
	function queryWorktype() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// form内容取出
		var text = $( "#history_query_worktype_range" ).serialize();
		// Ajax通信
		$.getJSON("query?action=query_timecard_worktype", text, dataLoad);
	}
	/**
	 * [業務種別]絞込みの処理
	 */
	function narrowingWorktype() {
		showLoadingDialog("絞込み中...");

		var queryData = getShowRecords();	// util.js参照

		var resultData = [];
		var worktype = $("#history_worktype").val();
		for(var key in queryData) {
			if(worktype == queryData[key].wid || worktype == queryData[key].wid_move) {
				resultData.push(queryData[key]);
			}
		}
		data_grid = resultData;

		// 描画更新
		dataView.beginUpdate();
		dataView.setItems(data_grid);
		dataView.endUpdate();
		grid.invalidate();

		closeLoadingDialog();
	}
	/**
	 * [年・月]検索の処理
	 */
	function queryYearMonth() {
		if(isYearMonthBlank()) return;	// util.js参照
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// form内容取出
		var text = $( "#query_year-month" ).serialize();
		// Ajax通信
		$.getJSON("query?action=query_timecard_yearmonth", text, dataLoad);
	}
	/**
	 * [日付間]検索の処理
	 */
	function queryDateRange() {
		// 入力チェック
		if(isDateRangeBlank()) return;
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// form内容取出
		var text = $( "#query_tdate_range" ).serialize();
		// Ajax通信
		$.getJSON("query?action=query_timecard_daterange", text, dataLoad);
	}
	/**
	 * [本日打刻済]検索の処理
	 */
	function queryToday() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// Ajax通信
		$.getJSON("query?action=query_timecard_today", "", dataLoad);
	}
	/**
	 * [時分間]検索の処理
	 */
	function queryTimeRange() {
		var inH = $( "#history_intime_h" ).val();
		var inM = $( "#history_intime_m" ).val();
		var moveH = $( "#history_movetime_h" ).val();
		var moveM = $( "#history_movetime_m" ).val();
		var outH = $( "#history_outtime_h" ).val();
		var outM = $( "#history_outtime_m" ).val();
		if(inH == "--" && inM == "--" && moveH == "--" && moveM == "--" && outH == "--" && outM == "--") {
			return window.alert("時間が選択されていません。");
		}
		if((inH == "--" && inM != "--") || (inH != "--" && inM == "--")) {
			return window.alert("出勤の値が不正です。\n[時][分]を正しく選択してください。");
		}
		if((moveH == "--" && moveM != "--") || (moveH != "--" && moveM == "--")) {
			return window.alert("移動の値が不正です。\n[時][分]を正しく選択してください。");
		}
		if((outH == "--" && outM != "--") || (outH != "--" && outM == "--")) {
			return window.alert("退勤の値が不正です。\n[時][分]を正しく選択してください。");
		}
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// form内容取出
		var text = $( "#history_query_time_range" ).serialize();
		// Ajax通信
		$.getJSON("query?action=query_timecard_timerange", text, dataLoad);
	}
	/**
	 * [今月分]検索の処理
	 */
	function queryNowMonth() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// Ajax通信
		$.getJSON("query?action=query_timecard_nowmonth", "", dataLoad);
	}
	/**
	 * [先月分]検索の処理
	 */
	function queryLastMonth() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// Ajax通信
		$.getJSON("query?action=query_timecard_lastmonth", "", dataLoad);
	}
	/**
	 * [来月分]検索の処理
	 */
	function queryNextMonth() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// Ajax通信
		$.getJSON("query?action=query_timecard_nextmonth", "", dataLoad);
	}
	/**
	 * [全]検索の処理
	 */
	function queryShowAll() {
		// データ再取得
		getData(false, true);	// util-hisotry.js参照
	}
}

/**
 * 選択初期化
 */
function queryReset() {
	$('#history_query_worktype_range').find('option').attr("selected",false);
	$('#query_tdate_range').find('input[type=\"text\"]').val("");
	$('#history_query_time_range').find('option').attr("selected",false);
}

function setupFreeeDownload() {
	// 年月取得
	var $year = $("#freee_download_year");
	var $month = $("#freee_download_month");
	$year.empty();
	$month.empty();

	var date = new Date();
	var underYear = 2019;
	var thisYear = date.getFullYear();
	for(var i = thisYear + 1; i >= underYear; i--) {
		$year.append($("<option>").val(i).html(i));
	}
	for(var i = 1; i <= 12; i++) {
		$month.append($("<option>").val(i).html(i));
	}
	$year.val(thisYear);
	$month.val(date.getMonth() + 1);

	$("#freee_download_button").off("click").on("click", clickFreeeDownload);
}

function clickFreeeDownload() {
	var year = $("#freee_download_year").val();
	var month = $("#freee_download_month").val();

	if (confirm("Freeeeデータのダウンロードを開始しますか？\n※時間がかかるのでご注意ください。また、出勤/退勤打刻の多い時間帯は避けてください。")) {
		location.href = "./freeedownload?year=" + year + "&month=" + month;
	}
}