/**
 * ユーザ情報更新のためのグローバル関数を定義します。
 */


/**
 * ユーザ情報更新ボタンモデル
 * @param row
 * @param cell
 * @param value
 * @param columnDef
 * @param dataContext
 * @returns
 */
function worktypeUpdateFormatter(row, cell, value, columnDef, dataContext) {
	// どの行がクリックされたか判定するためにbuttonのvalueにidを紐付けておく
	return "<button class='gridSelectButton' value='"+dataContext.id+"' onClick=worktypeUpdateHandleClick(event)>編集・管理</button>";
}

/**
 * ユーザ情報更新ボタンイベントハンドラ
 * @param e
 * @returns
 */
function worktypeUpdateHandleClick(e) {

	// nullになる場合があったので対応
	if(e.target.value == null) {
		return;
	}
	// どのbuttonがクリックされたかを判断し、該当するdataを取得
	var rowData = data.filter(function(item, index) {
		if(item.id == e.target.value) return true;
	})[0];

	// フォーム内容リセット
	$('#dialog-form_worktype_update_form').find('input[type=\"text\"]').val("");
	$('#dialog-form_worktype_update_form').find('option').attr("selected",false);
	$('#dialog-form_worktype_update_form').find('textarea').val("");

	// 各種フォーム
	var name = $( "#dialog-form_worktype_update_name" );
	var kana = $( "#dialog-form_worktype_update_kana" );
	var projectcode = $( "#dialog-form_worktype_update_projectcode" );
	var customer = $( "#dialog-form_worktype_update_customer" );
	var category = $( "#dialog-form_worktype_update_category" );
	var manager = $( "#dialog-form_worktype_update_manager" );
	var note = $( "#dialog-form_worktype_update_note" );

	// ↓↓ 値を入れていく ↓↓

	// 一覧取得
	getCustomers(rowData.customer);
	getCategorys(rowData.category);
	getUsers(rowData.manager);

	// name
	name.val(rowData.name);

	// kana
	kana.val(rowData.kana);

	//projectcode
	projectcode.val(rowData.projectcode);

	// note
	note.val(rowData.note);

	// oid (hidden)
	$("#dialog-form_worktype_update_oid").val(rowData.oid);

	// wid (hidden)
	$("#dialog-form_worktype_update_wid").val(rowData.wid);

	// enable (hidden)
	$("#dialog-form_worktype_update_enable").val(rowData.enable);

	// formダイアログ
	var dialog = $( "#dialog-form_worktype_update" ).dialog({
		autoOpen: false,
		maxHeight: $(window).height(),	// 表示領域の高さまで
		width: 500,
		minHeight: 300,
		modal: true,
		buttons: getButtons(rowData.enable == "true"),
		close: function () {
			// 毎回初期化
			$()
		}
	});

	dialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();	// アクションをキャンセル
	});

	dialog.dialog( "open" );

	/**
	 * 状態に応じて表示ボタンを変える
	 */
	function getButtons(enable) {
		var buttons;
		if(enable) {
			return buttons = {
				"確定":ok,
				"凍結する":validDisable,
				"キャンセル": cancel
			}
		} else {
			return buttons = {
				"確定":ok,
				"凍結を解除する":validEnable,
				"キャンセル": cancel
			}
		}
	}

	/**
	 * [確定]ボタン処理
	 */
	function ok() {

		// 改行コードを削除
		var $note = $("#dialog-form_worktype_update_note");
		$note.val(deleteCRLF($note.val()));	// deleteCRLF(): util.js参照

		// 変更箇所確認
		var change = "";
		if(rowData.name != name.val()) change += "name,";
		if(rowData.kana != kana.val()) change += "kana,";
		if(rowData.projectcode != projectcode.val()) change += "projectcode,";
		if(rowData.customer != customer.val()) change += "customer,";
		if(rowData.category != category.val()) change += "category,";
		if(rowData.manager != manager.val()) change += "manager,";
		if(rowData.note != note.val()) change += "note,";
		// 未変更は許可しない
		if(change == "") {
			return window.alert("いずれも変更されていません。");
		}

		// disabledな部品は送信されないので解除する
		$("#dialog-form_worktype_update_form :disabled").prop("disabled", false);
		// 変更箇所一覧を追加
		$( "#dialog-form_worktype_update_change" ).val(change);

		// 引数に渡す万能データ
		var datas = {
			change:change,
			rowData:rowData,
			name:name.val(),
			kana:kana.val(),
			functions:[update],	// 処理の順番を設定
			functionCount:0,	// 現在処理中のインデックス番号を設定
		};

		// functionsの第一要素から順に実行していく
		datas.functions[datas.functionCount](datas);
	}

	/**
	 * [この業務種別を凍結する]ボタン処理
	 */
	function validDisable() {
		var result = window.confirm(
			"本当に凍結してもよろしいですか？\n" +
			"※凍結された業務種別はタイムカードを打刻する際に指定することができなくなります。");
		if(result) {
			var change = "enable,";
			$( "#dialog-form_worktype_update_change" ).val(change);
			$( "#dialog-form_worktype_update_enable" ).val("false");
			update();
		} else {
			return;
		}
	}

	/**
	 * [この業務種別の凍結を解除する]ボタン処理
	 */
	function validEnable() {
		var result = window.confirm(
			"本当に凍結を解除してもよろしいですか？\n" +
			"※凍結を解除するとタイムカードを打刻する際に指定することができるようになります。");
		if(result) {
			var change = "enable,";
			$( "#dialog-form_worktype_update_change" ).val(change);
			$( "#dialog-form_worktype_update_enable" ).val("true");
			update();
		} else {
			return;
		}
	}

	/**
	 * [キャンセル]ボタン処理
	 */
	function cancel() {
		dialog.dialog( "close" );
	}

	/**
	 * 客先一覧取得
	 */
	function getCustomers(customer) {
		$("#dialog-form_worktype_update_customer").empty();
		$.getJSON("query?action=get_customers", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var customers = json.datas;
				// 一覧作成
				$("#dialog-form_worktype_update_customer").append($("<option>").val("-1").html("----"));
				for(var i=0; i<customers.length; i++) {
					$("#dialog-form_worktype_update_customer").append($("<option>").val(customers[i].id)
							.html(customers[i].note + " " + customers[i].name));
				}
				$("#dialog-form_worktype_update_customer").val(customer);
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * カテゴリー一覧取得
	 */
	function getCategorys(category) {
		$("#dialog-form_worktype_update_category").empty();
		$.getJSON("query?action=get_categorys", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var categorys = json.datas;
				// 一覧作成
				$("#dialog-form_worktype_update_category").append($("<option>").val("-1").html("----"));
				for(var i=0; i<categorys.length; i++) {
					$("#dialog-form_worktype_update_category").append($("<option>").val(categorys[i].id).html(categorys[i].name));
				}
				$("#dialog-form_worktype_update_category").val(category);
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * ユーザ一覧取得
	 */
	function getUsers(manager) {
		$("#dialog-form_worktype_update_manager").empty();
		showLoadingDialog("データを取得中...");	// 待機ダイアログ表示
		$.getJSON("query?action=get_users", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var users = json.datas;
				// ユーザ文字列検索イベントハンドラ登録 util.js参照
				var uBox = $("#dialog-form_worktype_update_manager");
				var uText = $("#dialog-form_worktype_update_search_users_text");
				var uMessage = $("#dialog-form_worktype_update_search_users_message");
				uText.keydown({box:uBox, text:uText, message:uMessage, act:"search", target:"users", users:users}, doSearch);
				$("#dialog-form_worktype_update_search_users_button")
					.click({box:uBox, text:uText, message:uMessage, act:"search", users:users}, searchTextUsers);
				$("#dialog-form_worktype_update_search_users_button_reset")
					.click({box:uBox, text:uText, message:uMessage, act:"reset", users:users}, searchTextUsers);
				// 一覧作成
				$("#dialog-form_worktype_update_manager").append($("<option>").val("-1").html("----"));
				for(var i=0; i<users.length; i++) {
					$("#dialog-form_worktype_update_manager").append($("<option>").val(users[i].uid).html(users[i].name));
				}
				$("#dialog-form_worktype_update_manager").val(manager);
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * UPDATE処理
	 */
	function update() {
		// formダイアログ閉じる
		dialog.dialog( "close" );

		// 待機ダイアログ表示
		showLoadingDialog("データを更新中...");

		// Ajax通信
		$.ajax({
			type: "POST",
			url: "update?action=update_worktype",
			data: $( "#dialog-form_worktype_update_form" ).serialize()
		}).done(function (json, status, xhr) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 成功
			if(json.result == "success") {
				$("#update_complete_dialog").html("業務種別情報を更新しました。");
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
			}
			var params = "wid="+rowData.wid;
			// 更新したタイムカードの情報を取得
			$.getJSON("query?action=query_worktype_wid", params, function(json, status) {
				if(isErrorFunction(json)) return;
				var uRecord = json.datas[0];	// １行のはず
				// 表の内容を更新
				for(var i=0; i<data.length; i++) {
					if(data[i].wid == rowData.wid) {
						data[i] = toRecord(data[i].id, uRecord);	// 更新
						for(var j=0; j<data_grid.length; j++) {
							if(data_grid[j].id == data[i].id) {
								data_grid[j] = toRecord(data_grid[j].id, uRecord);	// 管理用のも更新
								break;
							}
						}
						break;
					}
				}
				// 描画更新
				dataView.beginUpdate();
				dataView.setItems(data_grid);
				dataView.endUpdate();
				grid.invalidate();
			});
			ajaxFinishFunction();
		}).fail(function (json, status, error) {
			// 失敗
			$("#update_complete_dialog").html(
					"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
			ajaxFinishFunction();
		});
	}

	/**
	 * Ajax通信成功時
	 */
	function ajaxFinishFunction() {
		closeLoadingDialog();	// 待機ダイアログ閉じる
		// 通信結果ダイアログ
		$("#update_complete_dialog").dialog({
			title: "通信結果",
			modal: true,
			buttons: {
				"OK": function() {
					//allClear();	// 全体更新
					$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
				}
			},
			maxWidth: 500,
			maxHeight: 500,
			resizable: false
		});
	}
}