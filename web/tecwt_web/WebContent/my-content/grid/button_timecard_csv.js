/**
 * CSVダウンロード	ボタンの処理を定義。
 */
$(function () {
	$("#csv_button").on("click", csvOutput);
});

/**
 * ユーザ追加メソッド
 */
function csvOutput() {

	// フォーム内容リセット
	$('#dialog-form_csv_form').find('input[type=\"text\"]').val("");
	$('#dialog-form_csv_form').find('input[type=\"checkbox\"]').prop("checked", false);
	$('#dialog-form_csv_type1').prop('checked', true);

	var $isViewList = $('#dialog-form_csv_isviewlist');
	var $date = $('#dialog-form_csv_date');
	var $types = $('input:radio[name=dialog-form_csv_type]');

	// イベント登録
	$isViewList.change(function() {
		if($(this).is(":checked")) {
			$date.hide();
		} else {
			$date.show();
		}
	});
	$types.change(function() {
		if ($(this).val() == $('#dialog-form_csv_type2').val()) {
			$("#dialog-form_csv_istimeexist").prop('checked', true);	//損益用は未完打刻デフォルト拒否
		}
		if ($(this).val() == $('#dialog-form_csv_type3').val()) {
			// 作業内容は年月指定強制＆未完打刻無視
			$('#dialog-form_csv_isviewlist').prop('checked', false).prop('disabled', true);
			$('#dialog-form_csv_istimeexist').prop('checked', false).prop('disabled', true);
			$date.show();
		} else {
			$('#dialog-form_csv_isviewlist').prop('disabled', false);
			$('#dialog-form_csv_istimeexist').prop('disabled', false);
		}
	});

	// デフォルトは現在表示しているもののみ
	$isViewList.prop("checked", true).change();

	// ファイル名は指定
	var date = "Shuttai";
	var now = new Date();
	var y = now.getFullYear();
	if(y < 1900) y = y + 1900;	// 3桁で取得した場合用
	date = date + y;
	var filename = date;
	var $filename = $("#dialog-form_csv_filename");
	$filename.val(filename);

	// 年月を挿入
	var $year = $("#dialog-form_csv_year");
	var $month = $("#dialog-form_csv_month");
	$year.empty();
	$month.empty();
	$year.append($("<option>").val("--").html("--"));
	$month.append($("<option>").val("--").html("--"));
	var underYear = 2000;
	for(var i=y; i>=underYear; i--) {
		$year.append($("<option>").val(i).html(i));
	}
	for(var i=1; i<=12; i++) {
		$month.append($("<option>").val(i).html(i));
	}

	// formダイアログ
	var dialog = $( "#dialog-form_csv" ).dialog({
		autoOpen: false,
		maxHeight: $(window).height(),	// 表示領域の高さまで
		width: 500,
		minHeight: 300,
		modal: true,
		buttons: {
			"ダウンロードリンクを作成する": function () {

				// 自分で年・月を選択するかどうか
				var isViewList = $isViewList.is(":checked");

				// 年月確認
				if(!isViewList) {
					if($year.val() == "--" && $month.val() == "--") {
						return window.alert("年・月が選択されていません。");
					}
					if($year.val() == "--" && $month.val() != "--") {
						return window.alert("月のみで出力することはできません。");
					}
				}

				// ファイル名禁則文字確認
				if($filename.val() == "") {
					return window.alert("ファイル名を入力して下さい。");
				}
				if($filename.val().match(/[\\\/:\*\?\"\<\>\|]/gi)) {
					return window.alert("ファイル名に「\ / : * ? \" < > |」は使用できません。");
				}

				// 出勤のみ、移動のみ、退勤のみの許可非許可
				var isTimeExist = $("#dialog-form_csv_istimeexist").is(":checked");

				var dtype = $('input:radio[name=dialog-form_csv_type]:checked').val();

				var datas = {
						isShowDataOnly: isViewList,	// 表示行列のみの出力可否
						showDatas: getShowRecords(),	// 表示行列
						year: $year.val(),			// 検索：年
						month: $month.val(),		// 検索：月
						myuid: isMy ? uid : null,	// ユーザ
						unfinishable: isTimeExist,	// 打刻未完了行の可否
						filename: $filename.val(),	// ファイル名
						type: dtype,				// 出力タイプ
						functions: [makeLink],		// 実行関数リスト
						functionCount: 0,			// 実行中関数インデックス
				}

				// functionsの第一要素から順に実行していく
				datas.functions[datas.functionCount](datas);

				dialog.dialog("close");
			},
			"キャンセル": function () {
				dialog.dialog( "close" );
			}
		},
		close: function () {
			// 毎回初期化
		}
	});

	dialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();	// アクションをキャンセル
	});

	dialog.dialog( "open" );

	function makeLink(datas) {
		// ダウンロードLinkのフォーム
		var $dlf = $("#download-form");				// ダウンロード用
		var $dlff = $("#download-form_dlfalse");	// HTML展開用
		// 送信する値の追加
		if(datas.isShowDataOnly) {
			// 表示行列のみの場合
			var data1 = {
					type: 'hidden',
					name: 'showDatas',
					value: "{\"datas\":"+JSON.stringify(datas.showDatas)+"}"
				};
			var data2 = {
					type: 'hidden',
					name: 'unfinishable',
					value: datas.unfinishable
				};
			var data3 = {
					type: 'hidden',
					name: 'filename',
					value: datas.filename
				};
			var data4 = {
					type: 'hidden',
					name: 'target',
					value: datas.type
				};
			$("<input>", data1).appendTo($dlf);
			$("<input>", data1).appendTo($dlff);
			$("<input>", data2).appendTo($dlf);
			$("<input>", data2).appendTo($dlff);
			$("<input>", data3).appendTo($dlf);
			$("<input>", data3).appendTo($dlff);
			$("<input>", data4).appendTo($dlf);
			$("<input>", data4).appendTo($dlff);
		} else {
			// 年月検索の場合
			var data1 = {
					type: 'hidden',
					name: 'year',
					value: datas.year
				};
			var data2 = {
					type: 'hidden',
					name: 'month',
					value: datas.month
				};
			var data3 = {
					type: 'hidden',
					name: 'unfinishable',
					value: datas.unfinishable
				};
			var data4 = {
					type: 'hidden',
					name: 'myuid',
					value: datas.myuid
				};
			var data5 = {
					type: 'hidden',
					name: 'filename',
					value: datas.filename
				};
			var data6 = {
					type: 'hidden',
					name: 'target',
					value: datas.type
				};
			$("<input>", data1).appendTo($dlf);
			$("<input>", data1).appendTo($dlff);
			$("<input>", data2).appendTo($dlf);
			$("<input>", data2).appendTo($dlff);
			$("<input>", data3).appendTo($dlf);
			$("<input>", data3).appendTo($dlff);
			$("<input>", data4).appendTo($dlf);
			$("<input>", data4).appendTo($dlff);
			$("<input>", data5).appendTo($dlf);
			$("<input>", data5).appendTo($dlff);
			$("<input>", data6).appendTo($dlf);
			$("<input>", data6).appendTo($dlff);
		}

		// ダウンロード用ダイアログ作成
		var dlDialog = $( "#dialog_download-link" ).dialog({
			autoOpen: false,
			maxHeight: $(window).height(),	// 表示領域の高さまで
			width: 500,
			minHeight: 300,
			modal: true,
			buttons: {
				"キャンセル": function () {
					dlDialog.dialog( "close" );
				}
			},
			close: function() {
				// リンク削除
				$("#download-form").empty();
				$("#download-form_dlfalse").empty();
			}
		});
		dlDialog.find( "form" ).on( "submit", function( event ) {
			event.preventDefault();	// アクションをキャンセル
		});

		dlDialog.dialog( "open" );
	}

	/**
     * Ajax通信成功時
     */
    function ajaxFinishFunction() {
    	closeLoadingDialog();	// 待機ダイアログ閉じる
		// 通信結果ダイアログ
		$("#update_complete_dialog").dialog({
			title: "通信結果",
			modal: true,
			buttons: {
				"OK": function() {
					allClear();	// 全体更新
					$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
				}
			},
			maxWidth: 500,
			maxHeight: 500,
			resizable: false
		});
    }
}

