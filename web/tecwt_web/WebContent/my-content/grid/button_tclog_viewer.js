/**
 * ログ閲覧のためのグローバル関数を定義します。
 */


/**
 * ログ閲覧ボタンモデル
 * @param row
 * @param cell
 * @param value
 * @param columnDef
 * @param dataContext
 * @returns
 */
function tclogViewerFormatter(row, cell, value, columnDef, dataContext) {
	// どの行がクリックされたか判定するためにbuttonのvalueにidを紐付けておく
	return "<button class='gridSelectButton' value='"+dataContext.id+"' onClick=tclogViewerHandleClick(event)>ログ</button>";
}

/**
 * ユーザ情報更新ボタンイベントハンドラ
 * @param e
 * @returns
 */
function tclogViewerHandleClick(e) {

	// nullになる場合があったので対応
	if(e.target.value == null) {
		return;
	}
	// どのbuttonがクリックされたかを判断し、該当するdataを取得
	var rowData = data.filter(function(item, index) {
		if(item.id == e.target.value) return true;
	})[0];

	// ログ取得
	var uid = rowData.uid;
	var tdate = rowData.tdate;
	getTclog(uid, tdate);

	// formダイアログ
	var dialog = $( "#dialog-div_tclog_viewer" ).dialog({
		title: rowData.tdate + "　" + rowData.uname + "　タイムカード詳細ログ",
		autoOpen: false,
		width: 750,
		height: 500,
		maxWidth: $(window).width(),	// 表示領域の幅まで
		maxHeight: $(window).height(),	// 表示領域の高さまで
		modal: true,
		buttons: {
			"閉じる": function() {
				dialog.dialog( "close" );
			}
		},
		close: function () {
			// 毎回初期化
		}
	});

	dialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();	// アクションをキャンセル
	});

	dialog.dialog( "open" );

	/**
	 * ログ取得
	 */
	function getTclog(uid, tdate) {
		// フォーム内容リセット
		var table = $('#dialog-div_tclog_viewer_table');
		var tbody = $("#dialog-div_tclog_viewer_table_tbody");
		tbody.find("tr").remove();

		// 取得
		showLoadingDialog("データを取得中...");	// 待機ダイアログ表示
		$.getJSON("query?action=get_tclogs", {uid:uid,tdate:tdate}, function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				tbody.append($("<tr></tr>"));
				var tclogs = json.datas;
				var tclog;
				var href;
				var ll;
				var after;
				for(var i=0; i<tclogs.length; i++) {
					tclog = tclogs[i];
					ll = tclog.latitude+","+tclog.longitude;
					href = '<a href="http://maps.google.co.jp/maps/?ll='+ll+'&q='+ll+'&z=17" target="_blank">'+ll+'</a>';	// GPS
					after = $("<tr/>");
					after.append($("<td/>").html(tclog.logtype).addClass("dialog-div_table_logtype"));
					after.append($("<td/>").html(tclog.logdate).addClass("dialog-div_table_logdate"));
					after.append($("<td/>").html(tclog.logtime).addClass("dialog-div_table_logtime"));
					after.append($("<td/>").html(tclog.func).addClass("dialog-div_table_func"));
					after.append($("<td/>").html(href).addClass("dialog-div_table_ll"));
					after.append($("<td/>").html(tclog.unitname).addClass("dialog-div_table_unitname"));
					// 行の追加
					if(tbody.children("tr").length) {
						tbody.children("tr:last").after(after);
					} else {
						tbody.children("tr:first").after(after);
					}
				}
			} else {
				$("#update_complete_dialog").html("<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * Ajax通信成功時
	 */
	function ajaxFinishFunction() {
		closeLoadingDialog();	// 待機ダイアログ閉じる
		// 通信結果ダイアログ
		$("#update_complete_dialog").dialog({
			title: "通信結果",
			modal: true,
			buttons: {
				"OK": function() {
					allClear();	// 全体更新
					$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
				}
			},
			maxWidth: 500,
			maxHeight: 500,
			resizable: false
		});
	}
}