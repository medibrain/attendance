/**
 * アプリダウンロード処理を記述。
 */

$(function () {
	$("#app_button").on("click", appOutput);
});

/**
 * ダウンロード
 */
function appOutput(e) {

	// ダイアログでキャンセル確認(IE対応のため)
	var confirm = window.confirm(
			"APKファイル（Androidアプリをインストールする為のデータ）をダウンロードします。\n" +
			"\n" +
			"※Android端末から直接ダウンロードする場合は、ダウンロード成功後ステータスバーからファイルをタップし、表示される画面にてインストールを行って下さい。\n" +
			"\n" +
			"ダウンロードを開始してもよろしいですか？");
	if(!confirm) {
		e.preventDefault();
	}

	// ダウンロードリンクはapp.htmlのhref参照
}