/**
 * My勤怠表についての処理を記述します。
 *
 * 参考URL
 * http://mleibman.github.io/SlickGrid/examples/example-header-row.html
 * https://github.com/mleibman/SlickGrid/wiki/DataView
 * https://github.com/hnakamur/slickgrid_example/blob/master/js/example.js
 */


// グローバル変数
var myData;
var uid;


function historyInit(uid) {
	$.ajaxSetup({ cache: false });

	this.uid = uid;
	dataInit(true);	// util_history.js参照
	queryInit();
	myUpdateInit();	// button_my_update.js参照

	function queryInit() {
		$( "#query_tdate_yearmonth_button" ).on("click", queryYearMonth);
		$( "#query_tdate_range_button" ).on("click", queryDateRange);
		$( "#query_tdate_range_narrowing_button" ).on("click", narrowingDateRange);	// util_history.js参照
		$( "#query_tdate_range_narrowing_button" ).css("visibility", "visible");
		$( "#query_tdate_range_reset_button").on("click", resetDateRange);
		$( "#query_tdate_range_reset_button").css("visibility", "visible");
		$( "#query_tdate_nowmonth_button" ).on("click", queryNowMonth);
		$( "#query_tdate_nowmonth_button" ).css("visibility", "visible");
		$( "#query_tdate_lastmonth_button" ).on("click", queryLastMonth);
		$( "#query_tdate_lastmonth_button" ).css("visibility", "visible");
		$( "#query_tdate_nextmonth_button" ).on("click", queryNextMonth);
		$( "#query_tdate_nextmonth_button" ).css("visibility", "visible");
		$( "#history_query_time_range_button" ).on("click", queryTimeRange);
		$( "#history_query_time_narrowing_button" ).on("click", narrowingTimeRange);
		$( "#history_query_time_reset_button" ).on("click", resetTimeRange);
		$( "#query_showall_button" ).on("click", queryShowAll);
	}

	/**
	 * [年・月]検索の処理
	 */
	function queryYearMonth() {
		if(isYearMonthBlank()) return;	// util.js参照
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// form内容取出
		var text = $( "#query_year-month" ).serialize() + "&uid=" + uid;
		// Ajax通信
		$.getJSON("query?action=query_timecard_yearmonth", text, dataLoad);
	}
	/**
	 * [日付間]検索の処理
	 */
	function queryDateRange() {
		// 入力チェック
		if(isDateRangeBlank()) return;	// util.js参照
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// form内容取出
		var text = $( "#query_tdate_range" ).serialize() + "&uid=" + uid;
		// Ajax通信
		$.getJSON("query?action=query_timecard_daterange", text, dataLoad);
	}
	/**
	 * [今月分]検索の処理
	 */
	function queryNowMonth() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// Ajax通信
		$.getJSON("query?action=query_timecard_nowmonth_my", "", dataLoad);
	}
	/**
	 * [先月分]検索の処理
	 */
	function queryLastMonth() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// Ajax通信
		$.getJSON("query?action=query_timecard_lastmonth_my", "", dataLoad);
	}
	/**
	 * [来月分]検索の処理
	 */
	function queryNextMonth() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// Ajax通信
		$.getJSON("query?action=query_timecard_nextmonth_my", "", dataLoad);
	}
	/**
	 * [時分間]検索の処理
	 */
	function queryTimeRange() {
		var inH = $( "#history_intime_h" ).val();
		var inM = $( "#history_intime_m" ).val();
		var moveH = $( "#history_movetime_h" ).val();
		var moveM = $( "#history_movetime_m" ).val();
		var outH = $( "#history_outtime_h" ).val();
		var outM = $( "#history_outtime_m" ).val();
		if(inH == "--" && inM == "--" && moveH == "--" && moveM == "--" && outH == "--" && outM == "--") {
			return window.alert("時間が選択されていません。");
		}
		if((inH == "--" && inM != "--") || (inH != "--" && inM == "--")) {
			return window.alert("出勤の値が不正です。\n[時][分]を正しく選択してください。");
		}
		if((moveH == "--" && moveM != "--") || (moveH != "--" && moveM == "--")) {
			return window.alert("移動の値が不正です。\n[時][分]を正しく選択してください。");
		}
		if((outH == "--" && outM != "--") || (outH != "--" && outM == "--")) {
			return window.alert("退勤の値が不正です。\n[時][分]を正しく選択してください。");
		}
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// form内容取出
		var text = $( "#history_query_time_range" ).serialize() + "&uid=" + uid;
		// Ajax通信
		$.getJSON("query?action=query_timecard_timerange", text, dataLoad);
	}
	/**
	 * [全]検索の処理
	 */
	function queryShowAll() {
		// データ再取得
		getData(true, true);	// util-hisotry.js参照
	}
}

/**
 * 選択初期化
 */
function queryReset() {
	$('#query_tdate_range').find('input[type=\"text\"]').val("");
	$('#history_query_time_range').find('option').attr("selected",false);
}