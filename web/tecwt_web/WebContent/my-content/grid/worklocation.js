/**
 * カテゴリー一覧表示についての処理を記述します。
 *
 * 参考URL
 * http://mleibman.github.io/SlickGrid/examples/example-header-row.html
 * https://github.com/mleibman/SlickGrid/wiki/DataView
 * https://github.com/hnakamur/slickgrid_example/blob/master/js/example.js
 */

/**
 * グローバル変数
 */
// Grid
var dataView;
var grid;
var data = [];
var data_grid = [];
var columns;
var options;
var pager;


function worklocationInit() {
	dataInit();
	queryInit();
	worklocationInsertInit();	// button_worklocation_insert.js参照

	function dataInit() {
		// 表示項目
		columns = [
				{id:"id", name:"", field:"id", width: 40, sortable: true},
				{id:"name", name:"部署名", field:"name", width: 170, sortable: true},
				{id:"kana", name:"カナ", field:"kana", width: 170, sortable: true},
				{id:"valid", name:"管理状況", field:"valid", width: 60, sortable: true},
				{id:"note", name:"memo", field:"note", width: 170, sortable: true},
				{id:"idate", name:"登録日", field:"idate", width: 80, minWidth: 50, sortable: true},
				{id:"udate", name:"更新日", field:"udate", width: 80, minWidth: 50, sortable: true},
				{id:"worklocation_btn", name:"", field:"worklocation_btn", width: 65, sortable: false, formatter: worklocationUpdateFormatter}	// button_cateogry_update.js参照
		];

		// 全体オプション
		options = {
			multiColumnSort: true,
			showHeaderRow: true,
			headerRowHeight: 30,			// ヘッダーの高さ
			rowHeight: 22,					// 行の高さ
			autoHeight: true,				// 表示行数分Gridの高さを伸ばす
			enableColumnReorder: false,		// 列の変更を無効
			forceFitColumns: true,			// 余っている場合は余分を等しく分配してくれる
			explicitInitialization: true,	// textareaを入れる
			enableTextSelectionOnCells:true	// text選択許可
		};

		// データ取得
		showLoadingDialog("データを取得中...");	// 待機ダイアログ表示
		$.getJSON("query?action=get_worklocations", "", function(json, status) {

			// error check
			if(isErrorFunction(json)) return;

			// 初期化
			data = [];
			data_grid = [];
			dataView.refresh();

			// 値挿入
			var datas = json.datas;
			insertData(datas);

			// 描画更新
			dataView.beginUpdate();
			dataView.setItems(data_grid);
			dataView.endUpdate();
			grid.invalidate();

			// 待機ダイアログ閉じる
			closeLoadingDialog();
		});

		// 検索用カラムID
		var nextCid = data.length + 1;

		// インスタンス化
		dataView = new Slick.Data.DataView();
		grid = new Slick.Grid("#grid", dataView, columns, options);
		grid.setSelectionModel(new Slick.RowSelectionModel());

		// ソート
		grid.onSort.subscribe(function (e, args) {
			var cols = args.sortCols;
			// 比較する
			dataView.sort(function (dataRow1, dataRow2) {
				if(cols.length == 0) {
					return;
				}
				var field = cols[0].sortCol.field;
				var sign = cols[0].sortAsc ? 1 : -1;
				var func;
				if(field == "id") {
					// intで計算
					func = function(value1, value2) {
						try {
							value1 = parseInt(value1);
							value2 = parseInt(value2);
							return (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
						} catch (e) {
							return 0;
						}
					}
				} else {
					// unicodeで計算
					func = function(value1, value2) {
						return (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
					}
				}
				var value1;
				var value2;
				var result;
				for (var i = 0, l = cols.length; i < l; i++) {
					value1 = dataRow1[field];
					value2 = dataRow2[field];
					result = func(value1, value2);
					if (result != 0) {
						return result;
					}
				}
				return 0;
			});
		});

		// 検索用入力欄を作成
		grid.onHeaderRowCellRendered.subscribe(function (e, args) {
			// 親ノード
			var cell = $(args.node);
			cell.css("height", "100%");
			// id列には「検索」の文字
			if (args.column.id === "id") {
				cell.css("text-align", "center");
				cell.html("検索");
				return;
			}
			// 検索テキストクリア用のボタンを配置
			if(args.column.id === "department_btn") {
				cell.css("text-align", "center");
				cell.html("<input type='button' value='×' onclick='clickTextClear()'>");
				return;
			}
			// その他のボタン列には何もしない
			else if (args.column.id.indexOf("_btn") != -1) {
				return;
			}
			// <input type="text">
			cell.empty();
			$("<input>")
				.attr("type", "text")
				.attr("placeholder", "入力")
				.data("columnId", args.column.id)	// columnIdという名前でカラムにIDを紐付けておく
				.val(columnFilters[args.column.id])
				.appendTo(cell);
		});

		// ヒットする行のみを表示
		function updateFilters() {
			var columnId = $(this).data("columnId");	// ID取り出し
			if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				dataView.refresh();
			}
		}
		$(grid.getHeaderRow()).japaneseInputChange('input[type=text]', updateFilters);

		// 表を作成
		grid.init();

		// カラムの表示位置が変更された場合のイベントハンドラ
		dataView.onRowCountChanged.subscribe(function (e, args) {
			grid.updateRowCount();
			grid.render();
		});

		// カラムに関する描画更新イベントハンドラ
		dataView.onRowsChanged.subscribe(function (e, args) {
			grid.invalidateRows(args.rows);
			grid.render();
		});

		// DataView初期化
		dataView.beginUpdate();
		dataView.setItems(data_grid);	// デフォルトで"id"カラムを行識別子としている。第二引数で指定可能
		dataView.setFilter(filter);
		dataView.endUpdate();

		// ページ分け機能
		pager = new Slick.Controls.Pager(dataView, grid, $("#timecard_pager"));
	}

	function queryInit() {
		$( "#query_enable_button" ).on("click", queryEnable);
		$( "#query_showall_button" ).on("click", queryShowAll);
	}

	/**
	 * [管理状況]検索の処理
	 */
	function queryEnable() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// form内容取出
		var text = $( "#query_enable" ).serialize();
		// Ajax通信
		$.getJSON("query?action=query_worklocation_enable", text, dataLoad);
	}
	/**
	 * [全]検索の処理
	 */
	function queryShowAll() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// Ajax通信
		$.getJSON("query?action=get_worklocations", "", dataLoad);
	}
}

//検索文字をクリア
function clickTextClear() {
	$('.slick-headerrow-columns').find('input[type="text"]').val("");
}

/**
 * フィルタリング
 */
var columnFilters = {};
function filter(item) {
	var c;
	var field
	var val;
	// 全カラムを検査
	for (var columnId in columnFilters) {
		// 検索文字列が入力されているかをチェック
		if (columnId !== undefined && columnFilters[columnId] !== "") {
			c = grid.getColumns()[grid.getColumnIndex(columnId)];
			field = c.field;
			val = item[field];
			if (val === undefined || val.indexOf(columnFilters[columnId]) === -1) {	// 一致する部分がなければfalse
				return false;
			}
		}
	}
	return true;
}

/**
 * グローバル変数dataを引数datasで更新する
 */
function insertData(datas) {
	for(var i=0; i<datas.length; i++) {
		data[i] = toRecord(i+1, datas[i]);
	}
	data_grid = data.concat();	// グリッドで操作する用の配列を複製
}

function toRecord(id, insertData) {
	var record = {
			id:id,
			name:htmlDecode(insertData.name),	// Gridに挿入される際にエンコードされるので一旦戻す
			kana:htmlDecode(insertData.kana),
			valid:(insertData.enable == "true") ? "----":"凍結中",
			note:htmlDecode(insertData.note),
			idate:insertData.idate,
			udate:insertData.udate,
			worklocationid:htmlDecode(insertData.id),
			oid:insertData.oid,
			enable:insertData.enable
	};
	return record;
}

/**
 * 検索メニューを初期化します。
 */
function allClear() {
	$( "#query_showall_button" ).click();	// 強制全表示
	queryReset();		// 検索メニューリセット
}

/**
 * 選択初期化
 */
function queryReset() {
//	$('#query_tdate_range').find('input[type=\"text\"]').val("");
}