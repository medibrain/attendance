/**
 * My勤怠表についての処理を記述します。
 *
 * 参考URL
 * http://mleibman.github.io/SlickGrid/examples/example-header-row.html
 * https://github.com/mleibman/SlickGrid/wiki/DataView
 * https://github.com/hnakamur/slickgrid_example/blob/master/js/example.js
 */

/**
 * グローバル変数
 */
// Grid
var dataView;
var grid;
var data = [];
var data_grid = [];
var columns;
var options;
var pager;

var is_admin = false;

function worktypeInit(admin_flag) {
	is_admin = admin_flag;
	dataInit();
	queryInit();
	worktypeInsertInit();	// button_worktype_insert.js参照

	function dataInit() {
		// 表示項目
		if (is_admin) {
			columns = [
				{id:"id", name:"", field:"id", width: 40, sortable: true},
				{id:"name", name:"勤務種別名", field:"name", width: 100, sortable: true},
				{id:"kana", name:"カナ", field:"kana", width: 80, sortable: true},
				{id:"projectcode", name:"ﾌﾟﾛｼﾞｪｸﾄｺｰﾄﾞ", field:"projectcode", width: 70, sortable: true},
				{id:"customername", name:"客先", field:"customername", width: 80, sortable: true},
				{id:"categoryname", name:"カテゴリー", field:"categoryname", width: 80, sortable: true},
				{id:"managername", name:"責任者", field:"managername", width: 60, sortable: true},
				{id:"note", name:"メモ", field:"note", width: 80, sortable: true},
				{id:"valid", name:"管理状況", field:"valid", width: 60, sortable: true},
				{id:"idatetext", name:"登録日", field:"idate", width: 80, minWidth: 70, sortable: true},
				{id:"udatetext", name:"更新日", field:"udate", width: 80, minWidth: 70, sortable: true},
				{id:"worktype_btn", name:"", field:"worktype_btn", width: 65, sortable: false, formatter: worktypeUpdateFormatter}	// button_worktype_update.js参照
			];
		} else {
			columns = [
				{id:"name", name:"勤務種別名", field:"name", width: 100, sortable: true},
				{id:"kana", name:"カナ", field:"kana", width: 80, sortable: true},
				{id:"projectcode", name:"ﾌﾟﾛｼﾞｪｸﾄｺｰﾄﾞ", field:"projectcode", width: 70, sortable: true},
				{id:"customername", name:"客先", field:"customername", width: 80, sortable: true},
				{id:"categoryname", name:"カテゴリー", field:"categoryname", width: 80, sortable: true},
				{id:"managername", name:"責任者", field:"managername", width: 60, sortable: true},
				{id:"note", name:"メモ", field:"note", width: 80, sortable: true},
			];
		}

		// 全体オプション
		options = {
			multiColumnSort: true,
			showHeaderRow: true,
			headerRowHeight: 30,			// ヘッダーの高さ
			rowHeight: 22,					// 行の高さ
			autoHeight: true,				// 表示行数分Gridの高さを伸ばす
			enableColumnReorder: false,		// 列の変更を無効
			forceFitColumns: true,			// 余っている場合は余分を等しく分配してくれる
			explicitInitialization: true,	// textareaを入れる
			enableTextSelectionOnCells:true	// text選択許可
		};

		// データ取得
		showLoadingDialog("データを取得中...");	// 待機ダイアログ表示
		// 初期表示は有効なもののみ表示
		$.getJSON("query?action=query_worktype_enable&enable=true", "", function(json, status) {

			// error check
			if(isErrorFunction(json)) return;

			// 初期化
			data = [];
			data_grid = [];
			dataView.refresh();

			// 値挿入
			var datas = json.datas;
			insertData(datas);

			// 描画更新
			dataView.beginUpdate();
			dataView.setItems(data_grid);
			dataView.endUpdate();
			grid.invalidate();

			// 待機ダイアログ閉じる
			closeLoadingDialog();
		});

		// インスタンス化
		dataView = new Slick.Data.DataView();
		grid = new Slick.Grid("#grid", dataView, columns, options);
		grid.setSelectionModel(new Slick.RowSelectionModel());

		// ソート
		grid.onSort.subscribe(function (e, args) {
			var cols = args.sortCols;
			// 比較する
			dataView.sort(function (dataRow1, dataRow2) {
				if(cols.length == 0) {
					return;
				}
				var field = cols[0].sortCol.field;
				var sign = cols[0].sortAsc ? 1 : -1;
				var func;
				if(field == "id") {
					// intで計算
					func = function(value1, value2) {
						try {
							value1 = parseInt(value1);
							value2 = parseInt(value2);
							return (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
						} catch (e) {
							return 0;
						}
					}
				} else {
					// unicodeで計算
					func = function(value1, value2) {
						return (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
					}
				}
				var value1;
				var value2;
				var result;
				for (var i = 0, l = cols.length; i < l; i++) {
					value1 = dataRow1[field];
					value2 = dataRow2[field];
					result = func(value1, value2);
					if (result != 0) {
						return result;
					}
				}
				return 0;
			});
		});

		// 検索用入力欄を作成
		grid.onHeaderRowCellRendered.subscribe(function (e, args) {
			// 親ノード
			var cell = $(args.node);
			cell.css("height", "100%");
			// id列には「検索」の文字
			if (args.column.id === "id") {
				cell.css("text-align", "center");
				cell.html("検索");
				return;
			}
			// 検索テキストクリア用のボタンを配置
			if(args.column.id === "worktype_btn") {
				cell.css("text-align", "center");
				cell.html("<input type='button' value='×' onclick='clickTextClear()'>");
				return;
			}
			// その他のボタン列には何もしない
			else if (args.column.id.indexOf("_btn") != -1) {
				return;
			}
			// <input type="text">
			cell.empty();
			$("<input>")
				.attr("type", "text")
				.attr("placeholder", "入力")
				.data("columnId", args.column.id)	// columnIdという名前でカラムにIDを紐付けておく
				.val(columnFilters[args.column.id])
				.appendTo(cell);
		});

		// ヒットする行のみを表示
		function updateFilters() {
			var columnId = $(this).data("columnId");	// ID取り出し
			if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				dataView.refresh();
			}
		}
		$(grid.getHeaderRow()).japaneseInputChange('input[type=text]', updateFilters);

		// 表を作成
		grid.init();

		// カラムの表示位置が変更された場合のイベントハンドラ
		dataView.onRowCountChanged.subscribe(function (e, args) {
			grid.updateRowCount();
			grid.render();
		});

		// カラムに関する描画更新イベントハンドラ
		dataView.onRowsChanged.subscribe(function (e, args) {
			grid.invalidateRows(args.rows);
			grid.render();
		});

		// DataView初期化
		dataView.beginUpdate();
		dataView.setItems(data_grid);	// デフォルトで"id"カラムを行識別子としている。第二引数で指定可能
		dataView.setFilter(filter);
		dataView.endUpdate();

		// ページ分け機能
		pager = new Slick.Controls.Pager(dataView, grid, $("#timecard_pager"));

		// カレンダー登録
		$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
		$( "#tdate_range1" ).datepicker();
		$( "#tdate_range1" ).datepicker("option", "showOn", 'both');
		$( "#tdate_range1" ).datepicker("option", "buttonImageOnly", true);
		$( "#tdate_range1" ).datepicker("option", "buttonImage", '../images/calendar.gif');
		$( "#tdate_range2" ).datepicker();
		$( "#tdate_range2" ).datepicker("option", "showOn", 'both');
		$( "#tdate_range2" ).datepicker("option", "buttonImageOnly", true);
		$( "#tdate_range2" ).datepicker("option", "buttonImage", '../images/calendar.gif');
	}

	function queryInit() {
		$( "#query_tdate_range_button" ).on("click", queryDateRange);
		$( "#query_enable_button" ).on("click", queryEnable);
		$( "#query_showall_button" ).on("click", queryShowAll);
		$( "#query_tdate_today_button" ).on("click", queryToday);
		$( "#query_tdate_today_button" ).css("visibility", "visible");
		$( "#query_tdate_today_button" ).val("本日利用");

		$( "#query_pjcode_showall_button" ).on("click", queryShowAll);
		$( "#query_pjcode_thisyear_button" ).on("click", queryShowThisYear);
		$( "#query_pjcode_lastyear_button" ).on("click", queryShowLastYear);
		$( "#query_pjcode_nextyear_button" ).on("click", queryShowNextYear);
		$( "#query_pjcode_department_button" ).on("click", queryShowDepartment);
	}

	/**
	 * [日付間]検索の処理
	 */
	function queryDateRange() {
		// 入力チェック
		if(isDateRangeBlank()) return;
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// form内容取出
		var text = $( "#query_tdate_range" ).serialize();
		// Ajax通信
		$.getJSON("query?action=query_worktype_daterange", text, dataLoad);
	}
	/**
	 * [本日稼動中]検索の処理
	 */
	function queryToday() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// Ajax通信
		$.getJSON("query?action=query_worktype_today", "", dataLoad);
	}
	/**
	 * [ユーザに対する管理状況]検索の処理
	 */
	function queryEnable() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// form内容取出
		var text = $( "#query_enable" ).serialize();
		// Ajax通信
		$.getJSON("query?action=query_worktype_enable", text, dataLoad);
	}
	/**
	 * [全]検索の処理
	 */
	function queryShowAll() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// Ajax通信
		$.getJSON("query?action=get_worktypes", "", dataLoad);
	}

	/**
	 * [今年度]検索の処理
	 */
	function queryShowThisYear() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// Ajax通信
		$.getJSON("query?action=get_worktypes&year=0", "", dataLoad);
	}

	/**
	 * [昨年度]検索の処理
	 */
	function queryShowLastYear() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// Ajax通信
		$.getJSON("query?action=get_worktypes&year=-1", "", dataLoad);
	}

	/**
	 * [来年度]検索の処理
	 */
	function queryShowNextYear() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// Ajax通信
		$.getJSON("query?action=get_worktypes&year=1", "", dataLoad);
	}

	function queryShowDepartment() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// Ajax通信
		$.getJSON("query?action=get_worktypes&department=1", "", dataLoad);
	}
}

//検索文字をクリア
function clickTextClear() {
	$('.slick-headerrow-columns').find('input[type="text"]').val("");
}

/**
 * フィルタリング
 */
var columnFilters = {};
function filter(item) {
	if (!is_admin && item.valid === "凍結中") {
		return false;
	}

	var c;
	var field
	var val;
	// 全カラムを検査
	for (var columnId in columnFilters) {
		// 検索文字列が入力されているかをチェック
		if (columnId !== undefined && columnFilters[columnId] !== "") {
			c = grid.getColumns()[grid.getColumnIndex(columnId)];
			field = c.field;
			val = item[field];
			if (val === undefined || val.indexOf(columnFilters[columnId]) === -1) {	// 一致する部分がなければfalse
				return false;
			}
		}
	}
	return true;
}

/**
 * グローバル変数dataを引数datasで更新する
 */
function insertData(datas) {
	var idm;
	for(var i=0; i<datas.length; i++) {
		data[i] = toRecord(i+1, datas[i]);
	}
	data_grid = data.concat();	// グリッドで操作する用の配列を複製
}

function toRecord(id, insertData) {
	var cusname = insertData.customername == "" ? "----" : htmlDecode(insertData.customername);
	var catname = insertData.categoryname == "" ? "----" : htmlDecode(insertData.categoryname);
	var manname = insertData.managername == "" ? "----" : htmlDecode(insertData.managername);
	var record = {
			id:id,
			name:htmlDecode(insertData.name),	// Gridに挿入される際にエンコードされるので一旦戻す
			kana:htmlDecode(insertData.kana),
			customername:cusname,
			categoryname:catname,
			managername:manname,
			projectcode:htmlDecode(insertData.projectcode),
			note:htmlDecode(insertData.note),
			valid:(insertData.enable == "true") ? "----":"凍結中",
			idatetext:insertData.idate,
			udatetext:insertData.udate,
			oid:insertData.oid,
			wid:insertData.wid,
			enable:insertData.enable,
			customer:insertData.customer,
			category:insertData.category,
			manager:insertData.manager
	};
	return record;
}

/**
 * 検索メニューを初期化します。
 */
function allClear() {
	$( "#query_showall_button" ).click();	// 強制全表示
	queryReset();		// 検索メニューリセット
}

/**
 * 選択初期化
 */
function queryReset() {
	$('#query_tdate_range').find('input[type=\"text\"]').val("");
}