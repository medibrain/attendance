/**
 * ユーザ追加ボタンの処理を定義。
 */

function usersInsertInit() {
	$("#insert_user_button").on("click", usersInsert);
}

/**
 * ユーザ追加メソッド
 */
function usersInsert() {

	// フォーム内容リセット
	$('#dialog-form_users_insert_form').find('input[type=\"text\"]').val("");
	$('#dialog-form_users_insert_form').find('input[type=\"password\"]').val("");
	$('#dialog-form_users_insert_form').find('option').attr("selected",false);

	// 一覧セット
	setDatepicker();
	getDepartments();
	getEmployments();
	getWorklocations();
	getGroups();

	// 初期値
	$("#dialog-form_users_insert_paidvacationdays").val("0");

	$("#dialog-form_users_insert_admin").val("false");	// 初期は一般ユーザ

	// formダイアログ
	var dialog = $( "#dialog-form_users_insert" ).dialog({
		autoOpen: false,
		maxHeight: $(window).height(),	// 表示領域の高さまで
		width: 500,
		minHeight: 300,
		modal: true,
		buttons: {
			"このユーザを追加する": function () {

				// 入力確認
				var lname = $("#dialog-form_users_insert_lname").val();
				if (lname == "") return window.alert("ログインIDが入力されていません。");
				var pass = $("#dialog-form_users_insert_pass").val();
				if (pass == "") return window.alert("ログインパスが入力されていません。");
				var name = $("#dialog-form_users_insert_name").val();
				if (name == "") return window.alert("名前が入力されていません。");
				var kana = $("#dialog-form_users_insert_kana").val();
				if (kana == "") return window.alert("カナが入力されていません。");
				var code = $("#dialog-form_users_insert_code").val();
				if (code == "") return window.alert("社員番号が入力されていません。");
				var admin = $("#dialog-form_users_insert_admin").val();
				var mailladd = $("#dialog-form_users_insert_mailladd").val();
				if (mailladd == "") return window.alert("メールアドレスが入力されていません。");
				var mailenable = $("#dialog-form_users_insert_mailenable").val();
				var paidvacationdays = $("#dialog-form_users_insert_paidvacationdays").val();
				if (!$.isNumeric(paidvacationdays)) return window.alert("有休残日数が数字ではありません。");
				if (paidvacationdays < 0) return window.alert("有休残日数は0以上の数値である必要があります。");

				var lname_confirm = $("#dialog-form_users_insert_lname_confirm").val();
				var pass_confirm = $("#dialog-form_users_insert_pass_confirm").val();
				if (lname != lname_confirm) return window.alert("ログインIDが違います。ログインIDを再入力してください。");
				if (pass != pass_confirm) return window.alert("ログインパスが違います。ログインパスを再入力してください。");

				// 引数に渡す万能データ
				var datas = {
					lname:lname,
					pass:pass,
					name:name,
					kana:kana,
					code:code,
					admin:admin,
					mailladd:mailladd,
					functions:[checkLname, checkCode, insert],	// 処理の順番を設定
					functionCount:0,							// 現在処理中のインデックス番号を設定
				};

				// functionsの第一要素から順に実行していく
				datas.functions[datas.functionCount](datas);
			},
			"キャンセル": function () {
				dialog.dialog( "close" );
			}
		},
		close: function () {
			// 毎回初期化
		}
	});

	dialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();	// アクションをキャンセル
	});

	dialog.dialog( "open" );


	/**
	 * カレンダー設定
	 */
	function setDatepicker() {
		$("#dialog-form_users_insert_hiredate").datepicker();
		$("#dialog-form_users_insert_hiredate").datepicker("option", "showOn", 'both');
		$("#dialog-form_users_insert_hiredate").datepicker("option", "buttonImageOnly", true);
		$("#dialog-form_users_insert_hiredate").datepicker("option", "buttonImage", '../images/calendar.gif');
	}

	/**
	 * 部署一覧取得
	 */
	function getDepartments() {
		$("#dialog-form_users_insert_department").empty();
		$.getJSON("query?action=get_departments", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var departments = json.datas;
				// 一覧作成
				$("#dialog-form_users_insert_department").append($("<option>").val("-1").html("----"));
				for(var i=0; i<departments.length; i++) {
					$("#dialog-form_users_insert_department").append($("<option>").val(departments[i].id).html(departments[i].name));
				}
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * 雇用形態一覧取得
	 */
	function getEmployments() {
		$("#dialog-form_users_insert_employment").empty();
		$.getJSON("query?action=get_employments", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var employments = json.datas;
				// 一覧作成
				$("#dialog-form_users_insert_employment").append($("<option>").val("-1").html("----"));
				for(var i=0; i<employments.length; i++) {
					$("#dialog-form_users_insert_employment").append($("<option>").val(employments[i].id).html(employments[i].name));
				}
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * 勤務地一覧取得
	 */
	function getWorklocations() {
		$("#dialog-form_users_insert_worklocation").empty();
		$.getJSON("query?action=get_worklocations", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var worklocations = json.datas;
				// 一覧作成
				$("#dialog-form_users_insert_worklocation").append($("<option>").val("-1").html("----"));
				for(var i=0; i<worklocations.length; i++) {
					$("#dialog-form_users_insert_worklocation").append($("<option>").val(worklocations[i].id).html(worklocations[i].name));
				}
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * グループ一覧取得
	 */
	function getGroups() {
		$("#dialog-form_users_insert_group").empty();
		$.getJSON("query?action=get_groups", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var groups = json.datas;
				// 一覧作成
				$("#dialog-form_users_insert_group").append($("<option>").val("-1").html("----"));
				for(var i=0; i<groups.length; i++) {
					$("#dialog-form_users_insert_group").append($("<option>").val(groups[i].id).html(groups[i].name));
				}
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * ログインID有効確認
	 */
	function checkLname(datas) {
		var lname = datas.lname;
		datas.functionCount = datas.functionCount + 1;	// カウントを進める
		var nextFunction = datas.functions[datas.functionCount];
		showLoadingDialog("データを照合中...");	// 待機ダイアログ表示
		$.getJSON("query?action=query_users_lname", {lname:lname}, function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				if(json.datas.length > 0) {
					return window.alert("このログインIDは使用できません。");
				}
				if(nextFunction != null) {
					nextFunction(datas);	// 次の関数を実行
				}
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * 社員番号重複確認
	 */
	function checkCode(datas) {
		var code = datas.code;
		datas.functionCount = datas.functionCount + 1;	// カウントを進める
		var nextFunction = datas.functions[datas.functionCount];
		showLoadingDialog("データを照合中...");	// 待機ダイアログ表示
		// Ajax
		$.getJSON("query?action=query_users_code", {code:code}, function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				if(json.datas.length > 0) {
					var agree =  window.confirm(
							"この社員番号は既に使用されています。\n同一の社員番号が存在することになりますが、本当によろしいですか？");
					if(!agree) {
						return;	// 同意しない場合はキャンセル
					}
				}
				if(nextFunction != null) {
					nextFunction(datas);	// 次の関数を実行
				}
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	function insert() {
		// formダイアログ閉じる
		dialog.dialog("close");

		// 待機ダイアログ表示
		showLoadingDialog("データを作成中...");

		// Ajax通信
		$.ajax({
			type: "POST",
			url: "update?action=insert_users",
			data: $( "#dialog-form_users_insert_form" ).serialize()
		}).done(function (json, status, xhr) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 成功
			if(json.result == "success") {
				$("#update_complete_dialog").html("ユーザを追加しました。");
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
			}
			ajaxFinishFunction();
		}).fail(function (json, status, error) {
			// 失敗
			$("#update_complete_dialog").html(
					"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
			ajaxFinishFunction();
		});
	}

	/**
	 * Ajax通信成功時
	 */
	function ajaxFinishFunction() {
		closeLoadingDialog();	// 待機ダイアログ閉じる
		// 通信結果ダイアログ
		$("#update_complete_dialog").dialog({
			title: "通信結果",
			modal: true,
			buttons: {
				"OK": function() {
					allClear();	// 全体更新
					$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
				}
			},
			maxWidth: 500,
			maxHeight: 500,
			resizable: false
		});
	}
}

