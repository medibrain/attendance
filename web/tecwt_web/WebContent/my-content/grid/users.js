/**
 * My勤怠表についての処理を記述します。
 *
 * 参考URL
 * http://mleibman.github.io/SlickGrid/examples/example-header-row.html
 * https://github.com/mleibman/SlickGrid/wiki/DataView
 * https://github.com/hnakamur/slickgrid_example/blob/master/js/example.js
 */

/**
 * グローバル変数
 */
// Grid
var dataView;
var grid;
var data = [];
var data_grid = [];
var columns;
var options;
var pager;


function usersInit() {
	dataInit();
	queryInit();
	usersInsertInit();	// button_users_insert.js参照

	function dataInit() {
		// 表示項目
		columns = [
					{id:"id", name:"", field:"id", width: 40, sortable: true},
					{id:"name", name:"名前", field:"name", width: 85, sortable: true},
					{id:"kana", name:"カナ", field:"kana", width: 160, sortable: true},
					{id:"employmentname", name:"雇用形態", field:"employmentname", width: 120, sortable: true},
					{id:"worklocationname", name:"勤務地", field:"worklocationname", width: 130, sortable: true},
					{id:"departmentname", name:"部署", field:"departmentname", width: 90, sortable: true},
					{id:"code", name:"社員番号", field:"code", width: 70, sortable: true},
					{id:"issignupidm", name:"識別IDM", field:"issignupidm", width: 70, minWidth: 60, sortable: true},
					{id:"mailladd", name:"ﾒｰﾙｱﾄﾞﾚｽ", field:"mailladd", width: 315, sortable: true},
					{id:"mailenabletext", name:"ﾒｰﾙ打刻", field:"mailenabletext", width: 65, sortable: true},
					{id:"groupname", name:"グループ", field:"groupname", width: 100, sortable: true},
					{id:"hiredatetext", name:"雇用開始日", field:"hiredatetext", width: 80, sortable: true},
					{id:"paidvacationdays", name:"残有休", field:"paidvacationdays", width: 55, sortable: true},
					{id:"admintext", name:"権限", field:"admintext", width: 50, sortable: true},
					{id:"isloginok", name:"管理", field:"isloginok", width: 50, sortable: true},
					{id:"idatetext", name:"登録日", field:"idatetext", width: 80, minWidth: 70, sortable: true},
					{id:"udatetext", name:"更新日", field:"udatetext", width: 80, minWidth: 70, sortable: true},
					{id:"users_btn", name:"", field:"users_btn", width: 40, sortable: false, formatter: userUpdateFormatter},	// button_users_update.js参照
					{id:"idm_btn", name:"", field:"idm_btn", width: 50, sortable: false, formatter: idmUpdateFormatter},	// button_idm_update.js参照
			];

		// 全体オプション
		options = {
			multiColumnSort: true,
			showHeaderRow: true,
			headerRowHeight: 30,				// ヘッダーの高さ
			rowHeight: 22,						// 行の高さ
			autoHeight: true,					// 表示行数分Gridの高さを伸ばす
			enableColumnReorder: false,			// 列の変更を無効
			forceFitColumns: false,				// 情報量が多いのでスクロール許可
			explicitInitialization: true,		// textareaを入れる
			enableTextSelectionOnCells:true		// text選択許可
		};

		// データ取得
		showLoadingDialog("データを取得中...");	// 待機ダイアログ表示
		$.getJSON("query?action=get_users", "", function(json, status) {

			// error check
			if(isErrorFunction(json)) return;

			// 初期化
			data = [];
			data_grid = [];
			dataView.refresh();

			// 値挿入
			var datas = json.datas;
			insertData(datas);

			// 描画更新
			dataView.beginUpdate();
			dataView.setItems(data_grid);
			dataView.endUpdate();
			grid.invalidate();

			// 待機ダイアログ閉じる
			closeLoadingDialog();
		});

		// 検索用カラムID
		var nextCid = data.length + 1;

		// インスタンス化
		dataView = new Slick.Data.DataView();
		grid = new Slick.Grid("#grid", dataView, columns, options);
		grid.setSelectionModel(new Slick.RowSelectionModel());

		// ソート
		grid.onSort.subscribe(function (e, args) {
			var cols = args.sortCols;
			// 比較する
			dataView.sort(function (dataRow1, dataRow2) {
				if(cols.length == 0) {
					return;
				}
				var field = cols[0].sortCol.field;
				var sign = cols[0].sortAsc ? 1 : -1;
				var func;
				if(field == "id") {
					// intで計算
					func = function(value1, value2) {
						try {
							value1 = parseInt(value1);
							value2 = parseInt(value2);
							return (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
						} catch (e) {
							return 0;
						}
					}
				} else {
					// unicodeで計算
					func = function(value1, value2) {
						return (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
					}
				}
				var value1;
				var value2;
				var result;
				for (var i = 0, l = cols.length; i < l; i++) {
					value1 = dataRow1[field];
					value2 = dataRow2[field];
					result = func(value1, value2);
					if (result != 0) {
						return result;
					}
				}
				return 0;
			});
		});

		// 検索用入力欄を作成
		grid.onHeaderRowCellRendered.subscribe(function (e, args) {
			// 親ノード
			var cell = $(args.node);
			cell.css("height", "100%");
			// id列には「検索」の文字
			if (args.column.id === "id") {
				cell.css("text-align", "center");
				cell.html("検索");
				return;
			}
			// 検索テキストクリア用のボタンを配置
			if(args.column.id === "idm_btn") {
				cell.css("text-align", "center");
				cell.html("<input type='button' value='×' onclick='clickTextClear()'>");
				return;
			}
			// その他のボタン列には何もしない
			else if (args.column.id.indexOf("_btn") != -1) {
				return;
			}
			// <input type="text">
			cell.empty();
			$("<input>")
				.attr("type", "text")
				.attr("placeholder", "入力")
				.data("columnId", args.column.id)	// columnIdという名前でカラムにIDを紐付けておく
				.val(columnFilters[args.column.id])
				.appendTo(cell);
		});

		// ヒットする行のみを表示
		function updateFilters() {
			var columnId = $(this).data("columnId");	// ID取り出し
			if (columnId != null) {
				columnFilters[columnId] = $.trim($(this).val());
				dataView.refresh();
			}
		}
		$(grid.getHeaderRow()).japaneseInputChange('input[type=text]', updateFilters);

		// 表を作成
		grid.init();

		// カラムの表示位置が変更された場合のイベントハンドラ
		dataView.onRowCountChanged.subscribe(function (e, args) {
			grid.updateRowCount();
			grid.render();
		});

		// カラムに関する描画更新イベントハンドラ
		dataView.onRowsChanged.subscribe(function (e, args) {
			grid.invalidateRows(args.rows);
			grid.render();
		});

		// DataView初期化
		dataView.beginUpdate();
		dataView.setItems(data_grid);	// デフォルトで"id"カラムを行識別子としている。第二引数で指定可能
		dataView.setFilter(filter);
		dataView.endUpdate();

		// ページ分け機能
		pager = new Slick.Controls.Pager(dataView, grid, $("#timecard_pager"));

		// カレンダー登録
		$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
		$( "#tdate_range1" ).datepicker();
		$( "#tdate_range1" ).datepicker("option", "showOn", 'both');
		$( "#tdate_range1" ).datepicker("option", "buttonImageOnly", true);
		$( "#tdate_range1" ).datepicker("option", "buttonImage", '../images/calendar.gif');
		$( "#tdate_range2" ).datepicker();
		$( "#tdate_range2" ).datepicker("option", "showOn", 'both');
		$( "#tdate_range2" ).datepicker("option", "buttonImageOnly", true);
		$( "#tdate_range2" ).datepicker("option", "buttonImage", '../images/calendar.gif');
	}

	function queryInit() {
		$( "#query_tdate_range_button" ).on("click", queryDateRange);
		$( "#query_enable_button" ).on("click", queryEnable);
		$( "#query_mailenable_button" ).on("click", queryMailenable);
		$( "#query_showall_button" ).on("click", queryShowAll);
		$( "#query_tdate_today_button" ).on("click", queryToday);
		$( "#query_tdate_today_button" ).css("visibility", "visible");
		$( "#query_tdate_today_button" ).val("本日出勤");
	}

	/**
	 * [日付間]検索の処理
	 */
	function queryDateRange() {
		// 入力チェック
		if(isDateRangeBlank()) return;
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// form内容取出
		var text = $( "#query_tdate_range" ).serialize();
		// Ajax通信
		$.getJSON("query?action=query_users_daterange", text, dataLoad);
	}
	/**
	 * [本日出勤]検索の処理
	 */
	function queryToday() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// Ajax通信
		$.getJSON("query?action=query_users_today", "", dataLoad);
	}
	/**
	 * [ユーザに対する管理状況]検索の処理
	 */
	function queryEnable() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// form内容取出
		var text = $( "#query_enable" ).serialize();
		// Ajax通信
		$.getJSON("query?action=query_users_enable", text, dataLoad);
	}
	/**
	 * [ユーザに対するメール打刻許可]検索の処理
	 */
	function queryMailenable() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// form内容取出
		var text = $( "#query_mailenable" ).serialize();
		// Ajax通信
		$.getJSON("query?action=query_users_mailenable", text, dataLoad);
	}
	/**
	 * [全]検索の処理
	 */
	function queryShowAll() {
		// 待機ダイアログ表示
		showLoadingDialog("データを取得中...");
		// Ajax通信
		$.getJSON("query?action=get_users", "", dataLoad);
	}
}

//検索文字をクリア
function clickTextClear() {
	$('.slick-headerrow-columns').find('input[type="text"]').val("");
}

/**
 * フィルタリング
 */
var columnFilters = {};
function filter(item) {
	var c;
	var field
	var val;
	// 全カラムを検査
	for (var columnId in columnFilters) {
		// 検索文字列が入力されているかをチェック
		if (columnId !== undefined && columnFilters[columnId] !== "") {
			c = grid.getColumns()[grid.getColumnIndex(columnId)];
			field = c.field;
			val = item[field];
			if (val === undefined || val.indexOf(columnFilters[columnId]) === -1) {	// 一致する部分がなければfalse
				return false;
			}
		}
	}
	return true;
}

/**
 * グローバル変数dataを引数datasで更新する
 */
function insertData(datas) {
	var idm;
	for(var i=0; i<datas.length; i++) {
		data[i] = toRecord(i+1, datas[i]);
	}
	data_grid = data.concat();	// グリッドで操作する用の配列を複製
}

/**
 * 単一のタイムカードから表示用に加工したオブジェクトを返します
 * @param id
 * @param insertData
 * @returns {___anonymous8422_9072}
 */
function toRecord(id, insertData) {
	// 社員証IDMカラムの表示
	var idm;
	if(insertData.nfcwait == "true") idm = "受付中";
	else if(insertData.idmcount == 0) idm = "未登録";
	else idm = "登録数["+insertData.idmcount+"]";
	var dn = insertData.departmentname == "" ? "----" : htmlDecode(insertData.departmentname);
	var wln = insertData.worklocationname == "" ? "----" : htmlDecode(insertData.worklocationname);
	var en = insertData.employmentname == "" ? "----" : htmlDecode(insertData.employmentname);
	var gn = insertData.groupname == "" ? "----" : htmlDecode(insertData.groupname);
	var record = {
			id:id,
			name:htmlDecode(insertData.name),	// Grid挿入時にエンコードされるので一旦戻す
			kana:htmlDecode(insertData.kana),
			departmentname:dn,
			worklocationname:wln,
			employmentname:en,
			groupname:gn,
			hiredatetext:insertData.hiredate,
			paidvacationdays:insertData.paidvacationdays,
			admintext:(insertData.admin == "true") ? "管理者" : "一般",
			code:htmlDecode(insertData.code),
			mailladd:htmlDecode(insertData.mailladd),
			mailenabletext:(insertData.mailenable == "true") ? "許可" : "----",
			issignupidm:idm,
			isloginok:(insertData.enable == "true") ? "----":"凍結中",
			idatetext:insertData.idate,
			udatetext:insertData.udate,
			oid:insertData.oid,
			uid:insertData.uid,
			lname:htmlDecode(insertData.lname),
			pass:htmlDecode(insertData.pass),
			idmcount:insertData.idmcount,
			default_admin:insertData.admin,
			nfcwait:insertData.nfcwait,
			enable:insertData.enable,
			default_mailenable:insertData.mailenable,
			department:insertData.department,
			employment:insertData.employment,
			worklocation:insertData.worklocation,
			group:insertData.group
	};
	return record;
}

/**
 * 検索メニューを初期化します。
 */
function allClear() {
	$( "#query_showall_button" ).click();	// 強制全表示
	queryReset();		// 検索メニューリセット
}

/**
 * 選択初期化
 */
function queryReset() {
	$('#query_tdate_range').find('input[type=\"text\"]').val("");
}

/**
 * 表上の特定のユーザ情報を更新します。
 * @param uid
 */
function updateRecord(uid) {
	var params = "uid="+uid;
	// 更新したタイムカードの情報を取得
	$.getJSON("query?action=query_users_uid", params, function(json, status) {
		if(isErrorFunction(json)) return;
		var uRecord = json.datas[0];	// １行のはず
		// 表の内容を更新
		for(var i=0; i<data.length; i++) {
			if(data[i].uid == uid) {
				data[i] = toRecord(data[i].id, uRecord);	// 更新
				for(var j=0; j<data_grid.length; j++) {
					if(data_grid[j].id == data[i].id) {
						data_grid[j] = toRecord(data_grid[j].id, uRecord);	// 管理用のも更新
						break;
					}
				}
				break;
			}
		}
		// 描画更新
		dataView.beginUpdate();
		dataView.setItems(data_grid);
		dataView.endUpdate();
		grid.invalidate();
	});
}