/**
 * 端末情報更新のためのグローバル関数を定義します。
 */


/**
 * 端末情報更新ボタンモデル
 * @param row
 * @param cell
 * @param value
 * @param columnDef
 * @param dataContext
 * @returns
 */
function unitUpdateFormatter(row, cell, value, columnDef, dataContext) {
	// どの行がクリックされたか判定するためにbuttonのvalueにidを紐付けておく
	return "<button class='gridSelectButton' value='"+dataContext.id+"' onClick=unitUpdateHandleClick(event)>編集・管理</button>";
}

/**
 * ユーザ情報更新ボタンイベントハンドラ
 * @param e
 * @returns
 */
function unitUpdateHandleClick(e) {

	// nullになる場合があったので対応
	if(e.target.value == null) {
		return;
	}
	// どのbuttonがクリックされたかを判断し、該当するdataを取得
	var rowData = data.filter(function(item, index) {
		if(item.id == e.target.value) return true;
	})[0];

	// 各種フォーム
	var name = $( "#dialog-form_unit_update_name" );
	var pass = $( "#dialog-form_unit_update_pass" );
	var note = $( "#dialog-form_unit_update_note" );

	// ↓↓ 値を入れていく ↓↓

	// name
	name.val(rowData.name);

	// pass
	pass.val(rowData.pass);

	// oid
	$("#dialog-form_unit_update_oid").val(rowData.oid);
	$("#dialog-form_unit_update_oid").attr("disabled", true);

	// note
	note.val(rowData.note);

	// enable (hidden)
	$("#dialog-form_unit_update_enable").val(rowData.enable);

	// unitid (hidden)
	$("#dialog-form_unit_update_unitid").val(rowData.unitid);

	var standby = false;
	if(rowData.unitid == "standby") {
		// 受付中なら端末名と管理状況は触らせない
		name.prop("disabled", true);
		standby = true;
	} else {
		// 受付完了してるならパスは触らせない
		pass.prop("disabled", true);
	}

	// formダイアログ
	var dialog = $( "#dialog-form_unit_update" ).dialog({
		autoOpen: false,
		maxHeight: $(window).height(),	// 表示領域の高さまで
		width: 500,
		minHeight: 300,
		modal: true,
		buttons: getButtons(rowData.enable == "true", standby),
		close: function () {
			// 毎回初期化
			$('#dialog-form_unit_update_form').find('input[type=\"text\"]').val("");
			$("#dialog-form_unit_update_form :disabled").prop("disabled", false);
		}
	});

	dialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();	// アクションをキャンセル
	});

	dialog.dialog( "open" );

	/**
	 * 状態に応じて表示ボタンを変える
	 */
	function getButtons(enable, standby) {
		var buttons;
		if(standby) {
			return buttons = {
				"確定":ok,
				"受付状態を解除する":waitDelete,
				"キャンセル": cancel
			}
		}
		if(enable) {
			return buttons = {
				"確定":ok,
				"凍結する":validDisable,
				"キャンセル": cancel
			}
		} else {
			return buttons = {
				"確定":ok,
				"凍結を解除する":validEnable,
				"キャンセル": cancel
			}
		}
	}

	/**
	 * [確定]ボタン処理
	 */
	function ok() {

		// 変更箇所確認
		var change = "";
		if(rowData.name != name.val()) change += "name,";
		if(rowData.pass != pass.val()) change += "pass,";
		if(rowData.note != note.val()) change += "note,";
		// 未変更は許可しない
		if(change == "") {
			return window.alert("いずれも変更されていません。");
		}

		// disabledな部品は送信されないので解除する
		$("#dialog-form_unit_update_form :disabled").prop("disabled", false);
		// 変更箇所一覧を追加
		$( "#dialog-form_unit_update_change" ).val(change);

		// 改行コードを削除
		var $note = $("#dialog-form_unit_update_note");
		$note.val(deleteCRLF($note.val()));	// deleteCRLF(): util.js参照

		// 引数に渡す万能データ
		var datas = {
			change:change,
			rowData:rowData,
			serialize:$( "#dialog-form_unit_update_form" ).serialize(),
			functions:[update],	// 処理の順番を設定
			functionCount:0,	// 現在処理中のインデックス番号を設定
		};
		// functionsの第一要素から順に実行していく
		datas.functions[datas.functionCount](datas);
	}

	/**
	 * [この端末の受付状態を解除する]ボタン処理
	 */
	function waitDelete() {
		var result = window.confirm(
				"本当に解除してもよろしいですか？\n" +
				"※解除を行うと受付枠から削除され、設定したパスワードも解除されます。");
		if(!result) {
			return;
		}

		// disabledな部品は送信されないので解除する
		$("#dialog-form_unit_update_form :disabled").prop("disabled", false);

		// formダイアログ閉じる
		dialog.dialog( "close" );

		// 待機ダイアログ表示
		showLoadingDialog("データを更新中...");

		// Ajax通信
		$.ajax({
			type: "POST",
			url: "update?action=delete_unit",
			data: $( "#dialog-form_unit_update_form" ).serialize()
		}).done(function (json, status, xhr) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 成功
			if(json.result == "success") {
				$("#update_complete_dialog").html("端末待機状態を解除しました。");
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
			}
			deleteRecord(rowData.unitid);
			ajaxFinishFunction();
		}).fail(function (json, status, error) {
			// 失敗
			$("#update_complete_dialog").html(
					"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
			ajaxFinishFunction();
		});
	}

	/**
	 * [この端末を凍結する]ボタン処理
	 */
	function validDisable() {
		var result = window.confirm(
			"本当に凍結してもよろしいですか？\n" +
			"※凍結された端末では打刻・ユーザ認証などが行えなくなります。");
		if(result) {
			var change = "enable,";
			$( "#dialog-form_unit_update_change" ).val(change);
			$( "#dialog-form_unit_update_enable" ).val("false");
			var datas = {
					serialize:$( "#dialog-form_unit_update_form" ).serialize(),
				};
			update(datas);
		} else {
			return;
		}
	}

	/**
	 * [この端末の凍結を解除する]ボタン処理
	 */
	function validEnable() {
		var result = window.confirm(
			"本当に凍結を解除してもよろしいですか？\n" +
			"※凍結を解除すると、その端末から打刻・ユーザ認証などが行えるようになります。");
		if(result) {
			var change = "enable,";
			$( "#dialog-form_unit_update_change" ).val(change);
			$( "#dialog-form_unit_update_enable" ).val("true");
			var datas = {
					serialize:$( "#dialog-form_unit_update_form" ).serialize(),
				};
			update(datas);
		} else {
			return;
		}
	}

	/**
	 * [キャンセル]ボタン処理
	 */
	function cancel() {
		dialog.dialog( "close" );
	}

	/**
	 * UPDATE処理
	 */
	function update(datas) {

		// formダイアログ閉じる
		dialog.dialog( "close" );

		// 待機ダイアログ表示
		showLoadingDialog("データを更新中...");

		// Ajax通信
		$.ajax({
			type: "POST",
			url: "update?action=update_unit",
			data: datas.serialize
		}).done(function (json, status, xhr) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 成功
			if(json.result == "success") {
				$("#update_complete_dialog").html("端末情報を更新しました。");
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
			}
			updateRecord(rowData.unitid);
			ajaxFinishFunction();
		}).fail(function (json, status, error) {
			// 失敗
			$("#update_complete_dialog").html(
					"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
			ajaxFinishFunction();
		});
	}

	/**
	 * 表上の特定の端末情報を更新します。
	 */
	function updateRecord(unitid) {
		var params = "unitid="+unitid;
		// 更新したタイムカードの情報を取得
		$.getJSON("query?action=query_unit_unitid", params, function(json, status) {
			if(isErrorFunction(json)) return;
			var uRecord = json.datas[0];	// １行のはず
			// 表の内容を更新
			for(var i=0; i<data.length; i++) {
				if(data[i].unitid == unitid) {
					data[i] = toRecord(data[i].id, uRecord);	// 更新
					for(var j=0; j<data_grid.length; j++) {
						if(data_grid[j].id == data[i].id) {
							data_grid[j] = toRecord(data_grid[j].id, uRecord);	// 管理用のも更新
							break;
						}
					}
					break;
				}
			}
			// 描画更新
			dataView.beginUpdate();
			dataView.setItems(data_grid);
			dataView.endUpdate();
			grid.invalidate();
		});
	}
	/**
	 * 表上の特定の端末情報を削除します。
	 * @param uid
	 * @param tdate
	 */
	function deleteRecord(unitid) {
		// 表の内容を更新
		for(var i=0; i<data.length; i++) {
			if(data[i].unitid == unitid) {
				for(var j=0; j<data_grid.length; j++) {
					if(data_grid[j].id == data[i].id) {
						data_grid.splice(j, 1);	// 管理用を先に削除
						break;
					}
				}
				data.splice(i, 1);	// 削除
				break;
			}
		}
		// 描画更新
		dataView.beginUpdate();
		dataView.setItems(data_grid);
		dataView.endUpdate();
		grid.invalidate();
	}

	/**
	 * Ajax通信成功時
	 */
	function ajaxFinishFunction() {
		closeLoadingDialog();	// 待機ダイアログ閉じる
		// 通信結果ダイアログ
		$("#update_complete_dialog").dialog({
			title: "通信結果",
			modal: true,
			buttons: {
				"OK": function() {
					//allClear();	// 全体更新
					$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
				}
			},
			maxWidth: 500,
			maxHeight: 500,
			resizable: false
		});
	}
}