/**
 * ユーザ追加ボタンの処理を定義。
 */

function worktypeInsertInit() {
	$("#insert_worktype_button").on("click", worktypeInsert);
}

/**
 * ユーザ追加メソッド
 */
function worktypeInsert() {

	// フォーム内容リセット
	$('#dialog-form_worktype_insert_form').find('input[type=\"text\"]').val("");
	$('#dialog-form_worktype_insert_form').find('option').attr("selected",false);
	$('#dialog-form_worktype_insert_form').find('textarea').val("");

	// 一覧取得
	getCustomers();
	getCategorys();
	getUsers();

	// formダイアログ
	var dialog = $( "#dialog-form_worktype_insert" ).dialog({
		autoOpen: false,
		maxHeight: $(window).height(),	// 表示領域の高さまで
		width: 500,
		minHeight: 300,
		modal: true,
		buttons: {
			"確定": function () {

				// 入力確認
				var name = $("#dialog-form_worktype_insert_name").val();
				if(name == "") return window.alert("名前が入力されていません。");
				var kana = $("#dialog-form_worktype_insert_kana").val();
				if(kana == "") return window.alert("カナが入力されていません。");

				// 改行コードを削除
				var $note = $("#dialog-form_worktype_insert_note");
				$note.val(deleteCRLF($note.val()));	// deleteCRLF(): util.js参照

				// 引数に渡す万能データ
				var datas = {
					name:name,
					kana:kana,
					functions:[insert],	// 処理の順番を設定
					functionCount:0,	// 現在処理中のインデックス番号を設定
				};

				// functionsの第一要素から順に実行していく
				datas.functions[datas.functionCount](datas);
			},
			"キャンセル": function () {
				dialog.dialog( "close" );
			}
		},
		close: function () {
			// 毎回初期化
		}
	});

	dialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();	// アクションをキャンセル
	});

	dialog.dialog( "open" );


	/**
	 * 客先一覧取得
	 */
	function getCustomers() {
		$("#dialog-form_worktype_insert_customer").empty();
		$.getJSON("query?action=get_customers", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var customers = json.datas;
				// 一覧作成
				$("#dialog-form_worktype_insert_customer").append($("<option>").val("-1").html("----"));
				for(var i=0; i<customers.length; i++) {
					$("#dialog-form_worktype_insert_customer").append($("<option>").val(customers[i].id)
							.html(customers[i].note + " " + customers[i].name));
				}
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * カテゴリー一覧取得
	 */
	function getCategorys() {
		$("#dialog-form_worktype_insert_category").empty();
		$.getJSON("query?action=get_categorys", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var categorys = json.datas;
				// 一覧作成
				$("#dialog-form_worktype_insert_category").append($("<option>").val("-1").html("----"));
				for(var i=0; i<categorys.length; i++) {
					$("#dialog-form_worktype_insert_category").append($("<option>").val(categorys[i].id).html(categorys[i].name));
				}
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * ユーザ一覧取得
	 */
	function getUsers() {
		$("#dialog-form_worktype_insert_manager").empty();
		showLoadingDialog("データを取得中...");	// 待機ダイアログ表示
		$.getJSON("query?action=get_users", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var users = json.datas;
				// ユーザ文字列検索イベントハンドラ登録 util.js参照
				var uBox = $("#dialog-form_worktype_insert_manager");
				var uText = $("#dialog-form_worktype_insert_search_users_text");
				var uMessage = $("#dialog-form_worktype_insert_search_users_message");
				uText.keydown({box:uBox, text:uText, message:uMessage, act:"search", target:"users", users:users}, doSearch);
				$("#dialog-form_worktype_insert_search_users_button")
					.click({box:uBox, text:uText, message:uMessage, act:"search", users:users}, searchTextUsers);
				$("#dialog-form_worktype_insert_search_users_button_reset")
					.click({box:uBox, text:uText, message:uMessage, act:"reset", users:users}, searchTextUsers);
				// 一覧作成
				$("#dialog-form_worktype_insert_manager").append($("<option>").val("-1").html("----"));
				for(var i=0; i<users.length; i++) {
					$("#dialog-form_worktype_insert_manager").append($("<option>").val(users[i].uid).html(users[i].name));
				}
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	function insert() {
		// formダイアログ閉じる
		dialog.dialog("close");

		// 待機ダイアログ表示
		showLoadingDialog("データを作成中...");

		// Ajax通信
		$.ajax({
			type: "POST",
			url: "update?action=insert_worktype",
			data: $( "#dialog-form_worktype_insert_form" ).serialize()
		}).done(function (json, status, xhr) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 成功
			if(json.result == "success") {
				$("#update_complete_dialog").html("業務種別を追加しました。");
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
			}
			$.getJSON("query?action=query_worktype_enable&enable=true", "", function(json, status) {
				if(isErrorFunction(json)) return;
				// 初期化
				data = [];
				data_grid = [];
				dataView.refresh();

				// 値挿入
				var datas = json.datas;
				insertData(datas);

				// 描画更新
				dataView.beginUpdate();
				dataView.setItems(data_grid);
				dataView.endUpdate();
				grid.invalidate();
			});

			ajaxFinishFunction();
		}).fail(function (json, status, error) {
			// 失敗
			$("#update_complete_dialog").html(
					"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
			ajaxFinishFunction();
		});
	}

	/**
	 * Ajax通信成功時
	 */
	function ajaxFinishFunction() {
		closeLoadingDialog();	// 待機ダイアログ閉じる
		// 通信結果ダイアログ
		$("#update_complete_dialog").dialog({
			title: "通信結果",
			modal: true,
			buttons: {
				"OK": function() {
					allClear();	// 全体更新
					$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
				}
			},
			maxWidth: 500,
			maxHeight: 500,
			resizable: false
		});
	}
}

