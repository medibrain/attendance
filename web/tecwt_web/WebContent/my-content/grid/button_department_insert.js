/**
 * 追加ボタンの処理を定義。
 */

function departmentInsertInit() {
	$("#insert_department_button").on("click", departmentInsert);
}

/**
 * 追加メソッド
 */
function departmentInsert() {

	// フォーム内容リセット
	$('#dialog-form_department_insert_form').find('input[type=\"text\"]').val("");
	$('#dialog-form_department_insert_form').find('textarea').val("");
	$('#dialog-form_department_insert_form').find('option').attr("selected",false);

	// 選択肢の作成
	getUsers();

	// formダイアログ
	var dialog = $( "#dialog-form_department_insert" ).dialog({
		autoOpen: false,
		maxHeight: $(window).height(),	// 表示領域の高さまで
		width: 500,
		minHeight: 300,
		modal: true,
		buttons: {
			"確定": function () {

				// 入力確認
				var name = $("#dialog-form_department_insert_name").val();
				if(name == "") return window.alert("名前が入力されていません。");
				var kana = $("#dialog-form_department_insert_kana").val();
				if(kana == "") return window.alert("カナが入力されていません。");
				var manager = $("#dialog-form_department_insert_manager").val();
				if(manager == "") return window.alert("責任者が選択されていません。");
				var $note = $("#dialog-form_department_insert_note");
				$note.val(deleteCRLF($note.val()));	// 改行コード削除　util.js参照

				// 引数に渡す万能データ
				var datas = {
					functions:[insert],	// 処理の順番を設定
					functionCount:0,	// 現在処理中のインデックス番号を設定
				};

				// functionsの第一要素から順に実行していく
				datas.functions[datas.functionCount](datas);
			},
			"キャンセル": function () {
				dialog.dialog( "close" );
			}
		},
		close: function () {
			// 毎回初期化
		}
	});

	dialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();	// アクションをキャンセル
	});

	dialog.dialog( "open" );

	/**
	 * ユーザ一覧取得
	 */
	function getUsers() {
		$("#dialog-form_department_insert_manager").empty();
		showLoadingDialog("データを取得中...");	// 待機ダイアログ表示
		$.getJSON("query?action=get_users", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var users = json.datas;
				// ユーザ文字列検索イベントハンドラ登録 util.js参照
				var uBox = $("#dialog-form_department_insert_manager");
				var uText = $("#dialog-form_department_insert_search_users_text");
				var uMessage = $("#dialog-form_department_insert_search_users_message");
				uText.keydown({box:uBox, text:uText, message:uMessage, act:"search", target:"users", users:users}, doSearch);
				$("#dialog-form_department_insert_search_users_button")
					.click({box:uBox, text:uText, message:uMessage, act:"search", users:users}, searchTextUsers);
				$("#dialog-form_department_insert_search_users_button_reset")
					.click({box:uBox, text:uText, message:uMessage, act:"reset", users:users}, searchTextUsers);
				// 一覧作成
				for(var i=0; i<users.length; i++) {
					$("#dialog-form_department_insert_manager").append($("<option>").val(users[i].uid).html(users[i].name));
				}
				$("#dialog-form_department_insert_manager").val(users[0].uid).change();
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * 登録処理
	 */
	function insert() {
		// formダイアログ閉じる
		dialog.dialog("close");

		// 待機ダイアログ表示
		showLoadingDialog("データを作成中...");

		// Ajax通信
		$.ajax({
			type: "POST",
			url: "update?action=insert_department",
			data: $( "#dialog-form_department_insert_form" ).serialize()
		}).done(function (json, status, xhr) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 成功
			if(json.result == "success") {
				$("#update_complete_dialog").html("部署を追加しました。");
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
			}
			ajaxFinishFunction();
		}).fail(function (json, status, error) {
			// 失敗
			$("#update_complete_dialog").html(
					"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
			ajaxFinishFunction();
		});
	}

	/**
	 * Ajax通信成功時
	 */
	function ajaxFinishFunction() {
		closeLoadingDialog();	// 待機ダイアログ閉じる
		// 通信結果ダイアログ
		$("#update_complete_dialog").dialog({
			title: "通信結果",
			modal: true,
			buttons: {
				"OK": function() {
					allClear();	// 全体更新
					$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
				}
			},
			maxWidth: 500,
			maxHeight: 500,
			resizable: false
		});
	}
}

