/**
 * タイムカード更新処理を記述
 */


/**
 * タイムカード更新ボタンモデル
 * @param row
 * @param cell
 * @param value
 * @param columnDef
 * @param dataContext
 * @returns
 */
function timecardUpdateFormatter(row, cell, value, columnDef, dataContext) {
	// どの行がクリックされたか判定するためにbuttonのvalueにidを紐付けておく
	return "<button class='gridSelectButton' value='"+dataContext.id+"' onClick=timecardUpdateHandleClick(event)>編集</button>";
}

/**
 * タイムカード更新メソッド
 */
function timecardUpdateHandleClick(e) {

	// フォーム内容リセット
	$('#dialog-form_timecard_update_form').find('input[type=\"text\"]').val("");
	$('#dialog-form_timecard_update_form').find('textarea').val("");
	$('#dialog-form_timecard_update_form').find('option').attr("selected",false);

	// どのbuttonがクリックされたかを判断し、該当するdataを取得
	var rowData = data.filter(function(item, index) {
		if(item.id == e.target.value) return true;
	})[0];

	// 選択肢作成
	setDatepicker();
	getUsers();
	getWorktypes();
	setTimes();

	// 各種フォーム
	var oid = $( "#dialog-form_timecard_update_oid" );
	var uid = $( "#dialog-form_timecard_update_uid" );
	var default_uid = $( "#dialog-form_timecard_update_default_uid" );
	var tdate = $( "#dialog-form_timecard_update_tdate" );
	var default_tdate = $( "#dialog-form_timecard_update_default_tdate" );
	var times = $( "#dialog-form_timecard_update_times" );
	var inH = $( "#dialog-form_timecard_update_intime_h" );
	var inM = $( "#dialog-form_timecard_update_intime_m" );
	var moveH = $( "#dialog-form_timecard_update_movetime_h" );
	var moveM = $( "#dialog-form_timecard_update_movetime_m" );
	var outH = $( "#dialog-form_timecard_update_outtime_h" );
	var outM = $( "#dialog-form_timecard_update_outtime_m" );
	var wid = $( "#dialog-form_timecard_update_wid" );
	var note = $( "#dialog-form_timecard_update_note" );
	var wcount = $( "#dialog-form_timecard_update_wcount" );
	var rcount = $( "#dialog-form_timecard_update_rcount" );
	var wid_move = $( "#dialog-form_timecard_update_wid_move" );
	var note_move = $( "#dialog-form_timecard_update_note_move" );
	var wcount_move = $( "#dialog-form_timecard_update_wcount_move" );
	var rcount_move = $( "#dialog-form_timecard_update_rcount_move" );
	var breaktime = $( "#dialog-form_timecard_update_breaktime" );
	var fare = $( "#dialog-form_timecard_update_fare");

	// 移動有無
	var defaultIsMove = $( "#dialog-form_timecard_update_default_ismove" );
	if(rowData.moveH != "--" || rowData.moveM != "--" || rowData.wid_move != 0 || rowData.note_move != "" || rowData.wcount_move != 0 || rowData.rcount_move != 0) {
		defaultIsMove.val("true");
	} else {
		defaultIsMove.val("false");
	}

	// 凍結を除くチェック変更時イベントハンドラ
	$("#dialog-form_timecard_update_search_worktype_enable").change(function () {
		getWorktypes();
	});
	$("#dialog-form_timecard_update_search_worktype_move_enable").change(function () {
		getWorktypes();
	});
	// 仮入力ボタン押下時は勤務種別を仮入力にする
	$("#dialog-form_timecard_update_temporary").on("click", function() {
		var obj = $("#dialog-form_timecard_update_wid").children();
	    for( var i=0; i<obj.length; i++ ){
	    	if(obj.eq(i).text() === "仮入力") {
	    		$("#dialog-form_timecard_update_wid").val(obj.eq(i).val());
	    		break;
	    	}
	    }
    });
	// 名前変更時イベントハンドラ
	uid.change(function () {
		$("#dialog-form_timecard_update_uname").val($("#dialog-form_timecard_update_uid option:selected").text());
	});

	// 業務種別変更時イベントハンドラ
	wid.change(function () {
		$("#dialog-form_timecard_update_wname").val($("#dialog-form_timecard_update_wid option:selected").text());
	})
	wid_move.change(function () {
		$("#dialog-form_timecard_update_wname_move").val($("#dialog-form_timecard_update_wid_move option:selected").text());
	});

	// 移動時刻変更時イベントハンドラ
	$("#dialog-form_timecard_update_movetime_h").change(function () {
		var moveM = $("#dialog-form_timecard_update_movetime_m");
		if(moveM.val() == "--") {
			moveM.val("00").change();	// 分を00にしてあげる
		}
	});

	// 退勤時刻変更時イベントハンドラ
	$("#dialog-form_timecard_update_outtime_h").change(function () {
		var outM = $("#dialog-form_timecard_update_outtime_m");
		if(outM.val() == "--") {
			outM.val("00").change();	// 分を00にしてあげる
		}
	});

	// ↓↓ 値を入れていく ↓↓

	// oid (hidden)
	oid.val(rowData.oid);

	// uid
	uid.val(rowData.uid);

	// default_uid (hidden)
	default_uid.val(rowData.uid);

	// tdate
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	tdate.datepicker();
	tdate.datepicker("option", "showOn", 'both');
	tdate.datepicker("option", "buttonImageOnly", true);
	tdate.datepicker("option", "buttonImage", '../images/calendar.gif');
	tdate.val(rowData.tdate);

	// default_tdate (hidden)
	default_tdate.val(rowData.tdate);

	// times (hidden)
	times.val(rowData.times);

	// time
	inH.val(rowData.inH);
	inM.val(rowData.inM);
	moveH.val(rowData.moveH);
	moveM.val(rowData.moveM);
	outH.val(rowData.outH)
	outM.val(rowData.outM)

	// wid
	wid.val(rowData.wid);

	// note
	note.val(rowData.note);

	// wcount
	wcount.val(rowData.wcount);

	// rcount
	rcount.val(rowData.rcount);

	// wid_move
	wid_move.val(rowData.wid_move);

	// note_move
	note_move.val(rowData.note_move);

	// wcount_move
	wcount_move.val(rowData.wcount_move);

	// rcount_move
	rcount_move.val(rowData.rcount_move);

	// breaktime
	breaktime.val(rowData.breaktime);

	// fare
	fare.val(rowData.fare);

	// uname (hidden)
	$("#dialog-form_timecard_update_uname").val(htmlEncode(rowData.uname));

	// wname, wname_move (hidden)
	$("#dialog-form_timecard_update_wname").val(htmlEncode(rowData.wname));
	$("#dialog-form_timecard_update_wname_move").val(htmlEncode(rowData.wname_move));

	// formダイアログ
	var dialog = $( "#dialog-form_timecard_update" ).dialog({
		autoOpen: false,
		maxHeight: $(window).height(),	// 表示領域の高さまで
		width: 500,
		minHeight: 300,
		modal: true,
		buttons: {
			"確定": function () {

				// time
				if(moveH.val() == "--" && moveM.val() != "--") {
					// movetimeのどちらかに未選択のものがあれば
					return window.alert("移動の値が不正です。");
				}
				if(moveH.val() != "--" && moveM.val() == "--") {
					// movetimeのどちらかに未選択のものがあれば
					return window.alert("移動の値が不正です。");
				}
				if(outH.val() == "--" && outM.val() != "--"){
					// outtimeのどちらかに未選択のものがあれば
					return window.alert("退勤の値が不正です。");
				}
				if(outH.val() != "--" && outM.val() == "--"){
					// outtimeのどちらかに未選択のものがあれば
					return window.alert("退勤の値が不正です。");
				}
				if(moveH.val() != "--" && moveM.val() != "--") {
					if(inH.val() > moveH.val()) {
						return window.alert("出勤を移動より遅くすることはできません。");
					}
					if(inH.val() == moveH.val() && inM.val() > moveM.val()) {
						return window.alert("出勤を移動より遅くすることはできません。");
					}
				}
				if(outH.val() != "--" && outM.val() != "--") {
					if(inH.val() > outH.val()) {
						return window.alert("出勤を退勤より遅くすることはできません。");
					}
					if(inH.val() == outH.val() && inM.val() > outM.val()) {
						return window.alert("出勤を退勤より遅くすることはできません。");
					}
				}
				if(moveH.val() != "--" && moveM.val() != "--" && outH.val() != "--" && outM.val() != "--") {
					if(moveH.val() > outH.val()) {
						return window.alert("移動を退勤より遅くすることはできません。");
					}
					if(moveH.val() == outH.val() && moveM.val() > outM.val()) {
						return window.alert("移動を退勤より遅くすることはできません。");
					}
				}

				// worktype
				if((outH.val() != "--" && outM.val() != "--") || (moveH.val() != "--" && moveM.val() != "--")) {
					if(wid.val() == "0") {
						return window.alert("勤務種別を選択して下さい。");
					}
				}
				if((outH.val() != "--" && outM.val() != "--") && (moveH.val() != "--" && moveM.val() != "--")) {
					if(wid_move.val() == "0") {
						return window.alert("(移)勤務種別を選択して下さい。");
					}
				}

				// note, note_move
				if(note.val() != "" && note.val().length > 50) return window.alert("業務内容  は50文字以内で入力して下さい。");
				if(note_move.val() != "" && note_move.val().length > 50) return window.alert("(移)業務内容  は50文字以内で入力して下さい。");

				// count
				if(wcount.val() == "") wcount.val(0);	// 無記入は0とする
				if(rcount.val() == "") rcount.val(0);
				if(wcount_move.val() == "") wcount_move.val(0);
				if(rcount_move.val() == "") rcount_move.val(0);
				if(!$.isNumeric(wcount.val()) || wcount.val() < 0) return window.alert("処理数  は0以上の整数のみ記入できます。");
				if(!$.isNumeric(rcount.val()) || rcount.val() < 0) return window.alert("申出数  は0以上の整数のみ記入できます。");
				if(wcount_move.val() != "" && !$.isNumeric(wcount_move.val()) || wcount_move.val() < 0) return window.alert("(移)処理数  の値が無効です。0以上の整数のみ記入できます。");
				if(rcount_move.val() != "" && !$.isNumeric(rcount_move.val()) || rcount_move.val() < 0) return window.alert("(移)申出数  の値が無効です。0以上の整数のみ記入できます。");

				// breaktime, fare
				if(breaktime.val() == "") breaktime.val(0);
				if(fare.val() == "") fare.val(0);
				if(breaktime.val() != "" &&  !$.isNumeric(breaktime.val()) || breaktime.val() < 0) {
					return window.alert("休憩  は0以上の整数のみ記入できます。");
				}
				if(breaktime.val() != "" && breaktime.val() % 15 != 0) {
					return window.alert("休憩  は0もしくは15分刻みで入力して下さい。");
				}
				if(fare.val() != "" && !$.isNumeric(fare.val()) || fare.val() < 0) return window.alert("交通費  は0以上の整数のみ記入できます。");

				// 変更箇所確認
				var change = "";
				if(rowData.uid != uid.val()) change += "uid,";
				if(rowData.tdate != tdate.val()) change += "tdate,";
				if(rowData.inH != inH.val() || rowData.inM != inM.val()) change += "intime,";
				if(rowData.moveH != moveH.val() || rowData.moveM != moveM.val()) change += "movetime,";
				if(rowData.outH != outH.val() || rowData.outM != outM.val()) change += "outtime,";
				if(rowData.wid != wid.val()) change += "wid,";
				if(rowData.note != note.val()) change += "note,";
				if(rowData.wcount != wcount.val()) change += "wcount,";
				if(rowData.rcount != rcount.val()) change += "rcount,";
				if(rowData.wid_move != wid_move.val()) change += "wid_move,";
				if(rowData.note_move != note_move.val()) change += "note_move,";
				if(rowData.wcount_move != wcount_move.val()) change += "wcount_move,";
				if(rowData.rcount_move != rcount_move.val()) change += "rcount_move,";
				if(rowData.breaktime != breaktime.val()) change += "breaktime,";
				if(rowData.fare != fare.val()) change += "fare,";

				// 未変更は許可しない
				if(change == "") {
					return window.alert("いずれも変更されていません。");
				}

				// 移動に関連する項目が変更された場合は移動時刻チェックする
				if(change.indexOf("_move") != -1) {
					// ただしデフォルト値に戻されている(移動キャンセルの意思がある)場合は無視する
					if(wid_move.val() == "0" && note_move.val() == "" && wcount_move.val() == 0 && rcount_move.val() == 0) {}
					else if(moveH.val() == "--" || moveM.val() == "--") {
						return window.alert("移動を開始した時間を選択して下さい。");
					}
				}

				// disabledな部品は送信されないので解除する
				$("#dialog-form_timecard_update_form :disabled").prop("disabled", false);
				// 変更箇所一覧を追加
				$( "#dialog-form_timecard_update_change" ).val(change);

				// 改行コードを削除
				var $note = $("#dialog-form_timecard_update_note");
				var $note_move = $("#dialog-form_timecard_update_note_move");
				$note.val(deleteCRLF($note.val()));	// deleteCRLF(): util.js参照
				$note_move.val(deleteCRLF($note_move.val()));

				// 引数に渡すデータ
				var datas = {
					change:change,
					uid:uid.val(),
					tdate:tdate.val(),
					functions:[checkOverlapTimecard, update],	// 処理の順番を設定
					functionCount:0,							// 現在処理中のインデックス番号を設定
				};

				// functionsの第一要素から順に実行していく
				datas.functions[datas.functionCount](datas);
			},
			"このタイムカードを削除する": function () {
				if(window.confirm("本当にこのタイムカードを削除してもよろしいですか？\n※削除したタイムカードは元に戻すことができません。")) {
					timecardDelete();
				}
			},
			"キャンセル": function () {
				dialog.dialog( "close" );
			}
		},
		close: function () {
			// イベントハンドラは毎回破棄する
			uid.unbind();
			wid.unbind();
			wid_move.unbind();
		}
	});

	dialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();	// アクションをキャンセル
	});

	dialog.dialog( "open" );

	/**
	 * カレンダー設定
	 */
	function setDatepicker() {
		$("#dialog-form_timecard_update_tdate").datepicker();
		$("#dialog-form_timecard_update_tdate").datepicker("option", "showOn", 'both');
		$("#dialog-form_timecard_update_tdate").datepicker("option", "buttonImageOnly", true);
		$("#dialog-form_timecard_update_tdate").datepicker("option", "buttonImage", '../images/calendar.gif');
	}

	/**
	 * ユーザ一覧取得
	 */
	function getUsers() {
		$("#dialog-form_timecard_update_uid").empty();
		showLoadingDialog("データを取得中...");	// 待機ダイアログ表示
		$.getJSON("query?action=get_users", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var users = json.datas;
				// ユーザ文字列検索イベントハンドラ登録 util.js参照
				var uBox = $("#dialog-form_timecard_update_uid");
				var uText = $("#dialog-form_timecard_update_search_users_text");
				var uMessage = $("#dialog-form_timecard_update_search_users_message");
				uText.keydown({box:uBox, text:uText, message:uMessage, act:"search", target:"users", users:users}, doSearch);
				$("#dialog-form_timecard_update_search_users_button")
					.click({box:uBox, text:uText, message:uMessage, act:"search", users:users}, searchTextUsers);	// util.js参照
				$("#dialog-form_timecard_update_search_users_button_reset")
					.click({box:uBox, text:uText, message:uMessage, act:"reset", users:users}, searchTextUsers);
				// 一覧作成
				for(var i=0; i<users.length; i++) {
					if (users[i].enable == "true") {
						$("#dialog-form_timecard_update_uid").append($("<option>").val(users[i].uid).html(users[i].name));
					}
				}
				uid.val(rowData.uid);	// 一覧表ができてから改めて設定する
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * 業務種別一覧取得
	 */
	function getWorktypes() {
		$("#dialog-form_timecard_update_wid").empty();
		$("#dialog-form_timecard_update_wid_move").empty();
		showLoadingDialog("データを取得中...");	// 待機ダイアログ表示
		$.getJSON("query?action=get_worktypes", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var worktypes = json.datas;
				// 勤務種別文字列検索イベントハンドラ登録 util.js参照
				var wBox = $("#dialog-form_timecard_update_wid");
				var wText = $("#dialog-form_timecard_update_search_worktype_text");
				var wMessage = $("#dialog-form_timecard_update_search_worktype_message");
				var wEnable = $("#dialog-form_timecard_update_search_worktype_enable");
				wText.keydown({box:wBox, text:wText, message:wMessage, act:"search", target:"worktype", worktypes:worktypes, enableonly:wEnable}, doSearch);
				$("#dialog-form_timecard_update_search_worktype_button")
					.click({box:wBox, text:wText, message:wMessage, act:"search", worktypes:worktypes, enableonly:wEnable}, searchTextWorktype);
				$("#dialog-form_timecard_update_search_worktype_button_reset")
					.click({box:wBox, text:wText, message:wMessage, act:"reset", worktypes:worktypes, enableonly:wEnable}, searchTextWorktype);
				// (移)勤務種別文字列検索イベントハンドラ登録 util.js参照
				var wBoxMove = $("#dialog-form_timecard_update_wid_move");
				var wTextMove = $("#dialog-form_timecard_update_search_worktype_move_text");
				var wMessageMove = $("#dialog-form_timecard_update_search_worktype_move_message");
				var wEnableMove = $("#dialog-form_timecard_update_search_worktype_move_enable");
				wTextMove.keydown({box:wBoxMove, text:wTextMove, message:wMessageMove, act:"search", target:"worktype", worktypes:worktypes, enableonly:wEnableMove}, doSearch);
				$("#dialog-form_timecard_update_search_worktype_move_button")
					.click({box:wBoxMove, text:wTextMove, message:wMessageMove, act:"search", worktypes:worktypes, enableonly:wEnableMove}, searchTextWorktype);
				$("#dialog-form_timecard_update_search_worktype_move_button_reset")
					.click({box:wBoxMove, text:wTextMove, message:wMessageMove, act:"reset", worktypes:worktypes, enableonly:wEnableMove}, searchTextWorktype);
				// 一覧作成
				$("#dialog-form_timecard_update_wid").append($("<option>").val("0").html("----"));
				$("#dialog-form_timecard_update_wid_move").append($("<option>").val("0").html("----"));
				for(var i=0; i<worktypes.length; i++) {
					if (!wEnable.prop("checked") || worktypes[i].enable === "true") {
						$("#dialog-form_timecard_update_wid").append($("<option>").val(worktypes[i].wid).html(worktypes[i].name));
					}
					if (!wEnableMove.prop("checked") || worktypes[i].enable === "true") {
						$("#dialog-form_timecard_update_wid_move").append($("<option>").val(worktypes[i].wid).html(worktypes[i].name));
					}
				}
				wid.val(rowData.wid);	// 一覧表ができてから改めて設定する
				wid_move.val(rowData.wid_move);
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * 時間設定
	 */
	function setTimes() {
		/*
		 * H
		 */
		var inH = $("#dialog-form_timecard_update_intime_h");
		var moveH = $("#dialog-form_timecard_update_movetime_h");
		var outH = $("#dialog-form_timecard_update_outtime_h");
		inH.empty();
		moveH.empty();
		outH.empty();

		// "--"を許可する
		//inH.append($("<option>").val("--").html("--"));
		moveH.append($("<option>").val("--").html("--"));
		outH.append($("<option>").val("--").html("--"));

		var tmp;
		for(var i=0; i<=23; i++) {
			tmp = i < 10 ? ("0"+i) : i;
			inH.append($("<option>").val(tmp).html(tmp));
			moveH.append($("<option>").val(tmp).html(tmp));
			outH.append($("<option>").val(tmp).html(tmp));
		}

		//inH.val("--");
		moveH.val("--");
		outH.val("--");

		/*
		 * M
		 */
		var inM = $("#dialog-form_timecard_update_intime_m");
		var moveM = $("#dialog-form_timecard_update_movetime_m");
		var outM = $("#dialog-form_timecard_update_outtime_m");
		inM.empty();
		moveM.empty();
		outM.empty();

		// "--"を許可する
		//inM.append($("<option>").val("--").html("--"));
		moveM.append($("<option>").val("--").html("--"));
		outM.append($("<option>").val("--").html("--"));

		var tmp;
		for(var i=0; i<=3; i++) {
			tmp = i == 0 ? ("0"+i) : (i * 15);
			inM.append($("<option>").val(tmp).html(tmp));
			moveM.append($("<option>").val(tmp).html(tmp));
			outM.append($("<option>").val(tmp).html(tmp));
		}

		//inM.val("--");
		moveM.val("--");
		outM.val("--");
	}

	/**
	 * 同じタイムカードが既に存在しないか確認
	 */
	function checkOverlapTimecard(datas) {
		var uid = datas.uid;
		var tdate = datas.tdate;
		datas.functionCount = datas.functionCount + 1;	// カウントを進める
		var nextFunction = datas.functions[datas.functionCount];
		var change = datas.change;
		if(change.indexOf("uid") == -1 && change.indexOf("tdate") == -1) {
			if(nextFunction != null) {
				nextFunction(datas);	// 次の関数を実行
				return;
			}
		}
		showLoadingDialog("データを照合中...");	// 待機ダイアログ表示
		$.getJSON("query?action=query_timecard_overlap", {uid:uid,tdate:tdate}, function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				if(json.datas.length > 0) {
					return window.alert("このタイムカードは既に存在しています。ユーザもしくは日付を再度確認してください。");
				}
				if(nextFunction != null) {
					nextFunction(datas);	// 次の関数を実行
				}
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * UPDATE処理
	 */
	function update(datas) {
		var uid = datas.uid;
		var tdate = datas.tdate;

		// formダイアログ閉じる
		dialog.dialog( "close" );

		// 待機ダイアログ表示
		showLoadingDialog("データを更新中...");

		// Ajax通信
		$.ajax({
			type: "POST",
			url: "update?action=update_timecard",
			data: $( "#dialog-form_timecard_update_form" ).serialize()
		}).done(function (json, status, xhr) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 成功
			if(json.result == "success") {
				$("#update_complete_dialog").html("タイムカードを更新しました。");
			} else {
				$("#update_complete_dialog").html("<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
			}
			// 更新
			updateRecord(default_uid.val(), default_tdate.val(), uid, tdate);
			// 完了処理
			ajaxFinishFunction();
		}).fail(function (json, status, error) {
			// 失敗
			$("#update_complete_dialog").html(
					"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
			ajaxFinishFunction();
		});
	}

	/**
	 * DELETE処理
	 */
	function timecardDelete() {
		// formダイアログ閉じる
		dialog.dialog( "close" );

		// 待機ダイアログ
		showLoadingDialog("データを更新中...");

		// Ajax通信
		$.ajax({
			type: "POST",
			url: "update?action=delete_timecard",
			data: "oid="+oid.val()+"&default_uid="+default_uid.val()+"&default_tdate="+default_tdate.val()+"&default_ismove="+defaultIsMove.val()
		}).done(function (json, status, xhr) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 成功
			if(json.result == "success") {
				$("#update_complete_dialog").html("タイムカードを削除しました。");
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
			}
			// 更新
			deleteRecord(default_uid.val(), default_tdate.val());
			// 完了処理
			ajaxFinishFunction();
		}).fail(function (json, status, error) {
			// 失敗
			$("#update_complete_dialog").html(
					"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
			ajaxFinishFunction();
		});
	}

	/**
	 * 表上の特定のタイムカードを更新します。
	 * @param defaultUid
	 * @param defaultTdate
	 * @param updateUid
	 * @param updateTdate
	 */
	function updateRecord(defaultUid, defaultTdate, updateUid, updateTdate) {
		var params = "uid="+updateUid+"&tdate="+updateTdate;
		// 更新したタイムカードの情報を取得
		$.getJSON("query?action=query_timecard_tdate", params, function(json, status) {
			if(isErrorFunction(json)) return;
			var uRecord;
			if(json.datas.length == 1) {
				uRecord = json.datas[0];
			} else if(json.datas.length > 1) {
				uRecord = json.datas[1];
			} else {
				return;
			}
			// 表の内容を更新
			for(var i=0; i<data.length; i++) {
				if(data[i].uid == defaultUid && data[i].tdate == defaultTdate) {
					data[i] = toRecord(data[i].id, uRecord);	// 更新
					for(var j=0; j<data_grid.length; j++) {
						if(data_grid[j].id == data[i].id) {
							data_grid[j] = toRecord(data_grid[j].id, uRecord);	// 管理用のも更新
							break;
						}
					}
					break;
				}
			}
			// 描画更新
			dataView.beginUpdate();
			dataView.setItems(data_grid);
			dataView.endUpdate();
			grid.invalidate();
		});
	}
	/**
	 * 表上の特定のタイムカードを削除します。
	 * @param uid
	 * @param tdate
	 */
	function deleteRecord(uid, tdate) {
		// 表の内容を更新
		for(var i=0; i<data.length; i++) {
			if(data[i].uid == uid && data[i].tdate == tdate) {
				for(var j=0; j<data_grid.length; j++) {
					if(data_grid[j].id == data[i].id) {
						data_grid.splice(j, 1);	// 管理用を先に削除
						break;
					}
				}
				data.splice(i, 1);	// 削除
				break;
			}
		}
		// 描画更新
		dataView.beginUpdate();
		dataView.setItems(data_grid);
		dataView.endUpdate();
		grid.invalidate();
	}

	/**
     * Ajax通信成功時
     */
    function ajaxFinishFunction() {
    	closeLoadingDialog();	// 待機ダイアログ閉じる
		// 通信結果ダイアログ
		$("#update_complete_dialog").dialog({
			title: "通信結果",
			modal: true,
			buttons: {
				"OK": function() {
					//allClearForAdmin();	// 全体更新
					$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
				}
			},
			maxWidth: 500,
			maxHeight: 500,
			resizable: false
		});
    }
}