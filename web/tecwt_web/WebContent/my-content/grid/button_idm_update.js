/**
 * IDM削除のためのグローバル関数を定義します。
 */


/**
 * IDM削除用ボタンモデル
 * @param row
 * @param cell
 * @param value
 * @param columnDef
 * @param dataContext
 * @returns
 */
function idmUpdateFormatter(row, cell, value, columnDef, dataContext) {
	// どの行がクリックされたか判定するためにbuttonのvalueにidを紐付けておく
	return "<button class='gridSelectButton' value='"+dataContext.id+"' onClick=idmUpdateHandleClick(event)>社員証</button>";
}

/**
 * IDM削除用ボタンイベントハンドラ
 * @param e
 * @param args
 */
function idmUpdateHandleClick(e) {

	// nullになる場合があったので対応
	if(e.target.value == null) {
		return;
	}
	// どのbuttonがクリックされたかを判断し、該当するdataを取得
	var rowData = data.filter(function(item, index) {
		if(item.id == e.target.value) return true;
	})[0];

	// タイトルに名前記載
	$("#name").text("名前："+rowData.name);

	/*
	 * IDMが登録されている場合、Ajax通信でその一覧を取得する
	 */
	// まず空っぽにする
	$("#idm-checkboxs-div").empty();
	if(rowData.idmcount > 0) {
		showLoadingDialog("データを更新中...");	// 待機ダイアログ表示
		var params = {action:"get_nfcs", uid:rowData.uid};
		// Ajax
		$.getJSON("query", params, function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 取得できなかった場合
			if(json.result != "success") {
				// 通信結果ダイアログ
				$("#update_complete_dialog").html("<span style='color:#ff0000;'>通信エラーが発生しました。</span><br>再度操作を行って下さい。");
				$("#update_complete_dialog").dialog({
					title: "データ取得時エラー",
					modal: true,
					buttons: {
						"OK": function() {
							$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
						}
					},
					maxWidth: 500,
					maxHeight: 500,
					resizable: false
				});
				closeLoadingDialog();	// 待機ダイアログ閉じる
				return;
			}

			var datas = json.datas;
			var $div;
			var $input;
			var $label;
			for(var i=0; i<datas.length; i++) {
				$div = $('<div class="idm-record-div">');
				$input = $('<input type="checkbox" />').val(datas[i].idm).attr("checked", false);
				$label = $('<label />').text(" 登録"+(i+1)+": "+datas[i].idm+"  ["+datas[i].idate+"]...(MEMO) "+datas[i].note+"");
				$input.appendTo($div);
				$label.appendTo($div);
				$div.appendTo("#idm-checkboxs-div");
			}
			closeLoadingDialog();	// 待機ダイアログ閉じる
		});
	} else {
		var text;
		if(rowData.enable == "true") {
			text = "<span style='color:#FF0000;'>識別IDMが登録されていません。</span><br>" +
				"下部の [登録受付状態にする] を選択し、専用アプリがインストールされたタブレットから登録を行って下さい。";
		} else {
			text = "<span style='color:#FF0000;'>このユーザは凍結されています。</span><br>" +
				"凍結されたユーザはIDMの削除は行えますが、新規IDMの登録を行うことはできません。登録を行う場合は凍結を解除してください。";
		}
		var $label = $('<label style="font-size:15px;"/>').html(text);
		$label.appendTo("#idm-checkboxs-div");
	}

	// oid (hidden)
	$("#dialog-form_idm_oid").val(rowData.oid);

	// uid (hidden)
	$("#dialog-form_idm_uid").val(rowData.uid);

	// formダイアログ
	var dialog = $( "#dialog-form_idm" ).dialog({
		autoOpen: false,
		maxHeight: $(window).height(),	// 表示領域の高さまで
		width: 500,
		minHeight: 300,
		modal: true,
		buttons: getButtons(rowData.nfcwait == "true", rowData.enable == "true"),
		close: function () {
			// 毎回初期化
		}
	});

	dialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();	// アクションをキャンセル
	});

	dialog.dialog( "open" );

	/**
	 * IDM登録受付状態に応じたボタンを返す。
	 */
	function getButtons(isNfcwait, enable) {
		var buttons;
		if(!isNfcwait) {
			if(rowData.idmcount > 0) {
				if(enable) {
					// NFC未待機、IDM所持、未凍結
					return buttons = {"登録受付状態にする": idmRegistTrue, "選択したIDMを削除する": idmDelete, "閉じる": close}
				} else {
					// NFC未待機、IDM所持、凍結
					return buttons = {"選択したIDMを削除する": idmDelete,  "閉じる": close}
				}
			} else {
				if(enable) {
					// NFC未待機、IDM未所持、未凍結
					return buttons = {"登録受付状態にする": idmRegistTrue,  "閉じる": close}
				} else {
					// NFC未待機、IDM未所持、凍結
					return buttons = { "閉じる": close}
				}
			}
		} else {
			if(rowData.idmcount > 0) {
				if(enable) {
					// NFC待機、IDM所持、未凍結
					return buttons = {"登録受付を解除する": idmRegistFalse, "選択したIDMを削除する": idmDelete,  "閉じる": close}
				} else {
					// NFC待機、IDM所持、凍結
					return buttons = {"登録受付を解除する": idmRegistFalse, "選択したIDMを削除する": idmDelete,  "閉じる": close}
				}

			} else {
				if(enable) {
					// NFC待機、IDM未所持、未凍結
					return buttons = {"登録受付を解除する": idmRegistFalse,  "閉じる": close};
				} else {
					// NFC待機、IDM未所持、凍結
					return buttons = {"登録受付を解除する": idmRegistFalse,  "閉じる": close};
				}
			}
		}
	}

	/**
	 * IDM登録受付 開始
	 */
	function idmRegistTrue() {
		var result = window.confirm(
				"このユーザの識別IDMを登録受付状態にします。" +
				"受付期限はありませんが、別のユーザによって誤登録されてしまう可能性があるため、" +
				"可能な限り早めに登録を行って下さい。\n" +
				"登録は専用アプリがインストールされたタブレットをご利用下さい。\n" +
				"[トップ] -> [管理設定] -> [ID登録]\n" +
				"\n" +
				"受付状態にしてもよろしいですか。");
		if(!result) return;

		// formダイアログ閉じる
		dialog.dialog("close");

		// 送信
		$.ajax({
			type: "POST",
			url: "update?action=insert_nfcwait",
			data: "oid="+rowData.oid+"&uid="+rowData.uid
		}).done(function (json, status, xhr) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 成功
			if(json.result == "success") {
				$("#update_complete_dialog").html("識別IDMを登録受付状態にしました。");
			} else {
				$("#update_complete_dialog").html("<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
			}
			updateRecord(rowData.uid);
			ajaxFinishFunction();
		}).fail(function (json, status, error) {
			// 失敗
			$("#update_complete_dialog").html("<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
			ajaxFinishFunction();
		});
	}

	/**
	 * IDM登録受付 解除
	 */
	function idmRegistFalse() {

		// formダイアログ閉じる
		dialog.dialog("close");

		// 送信
		$.ajax({
			type: "POST",
			url: "update?action=delete_nfcwait",
			data: "oid="+rowData.oid+"&uid="+rowData.uid
		}).done(function (json, status, xhr) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 成功
			if(json.result == "success") {
				$("#update_complete_dialog").html("識別IDMの登録受付を解除しました。");
			} else {
				$("#update_complete_dialog").html("<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
			}
			updateRecord(rowData.uid);
			ajaxFinishFunction();
		}).fail(function (json, status, error) {
			// 失敗
			$("#update_complete_dialog").html("<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
			ajaxFinishFunction();
		});
	}

	/**
	 * 登録されたIDMを削除する
	 */
	function idmDelete() {
		// 削除箇所確認
		var deleteidm = "";
		var checks = $("input[type='checkbox']:checked");
		for(var i=0; i<checks.length; i++) {
			deleteidm += checks[i].value + ",";
		}
		// 未削除は許可しない
		if(deleteidm == "") {
			return window.alert("いずれも選択されていません。");
		}
		// disabledな部品は送信されないので解除する
		$("#dialog-form_idm_form :disabled").prop("disabled", false);
		// 変更箇所一覧を追加
		$( "#dialog-form_idm_delete-idm" ).val(deleteidm);

		// formダイアログ閉じる
		dialog.dialog( "close" );

		// 待機ダイアログ表示
		showLoadingDialog("データを更新中...");

		// Ajax通信
		$.ajax({
			type: "POST",
			url: "update?action=delete_nfc",
			data: $( "#dialog-form_idm_form" ).serialize()
		}).done(function (json, status, xhr) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 成功
			if(json.result == "success") {
				$("#update_complete_dialog").html("社員識別IDM情報を更新しました。");
			} else {
				$("#update_complete_dialog").html("<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
			}
			updateRecord(rowData.uid);
			ajaxFinishFunction();
		}).fail(function (json, status, error) {
			// 失敗
			$("#update_complete_dialog").html("<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
			ajaxFinishFunction();
		});
	}

	/**
	 * ダイアログを閉じる
	 */
	function close() {
		dialog.dialog( "close" );
	}

	/**
	 * Ajax通信成功時
	 */
	function ajaxFinishFunction() {
		closeLoadingDialog();	// 待機ダイアログ閉じる
		// 通信結果ダイアログ
		$("#update_complete_dialog").dialog({
			title: "通信結果",
			modal: true,
			buttons: {
				"OK": function() {
					//allClear();	// 全体更新
					$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
				}
			},
			maxWidth: 500,
			maxHeight: 500,
			resizable: false
		});
	}
}