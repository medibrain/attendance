/**
 * My勤怠表、勤怠表 編集・削除タグから利用されるグローバル関数を定義
 */


/**
 * グローバル変数
 */
// Grid
var dataView;
var grid;
var data = [];
var data_grid = [];
var columns;
var options;
var pager;
// select
var worktypes;
var users;
// util
var isMy;


/**
 * グリッドを初期化し、データの取得を行った後に完璧なものとします。<br>
 * 引数のisMyは一般ユーザかそうでないかを判定するのに使います。<br>
 */
function dataInit(isMy) {
	// マイページか否か
	if(isMy) this.isMy = true;
	else this.isMy = false;
	// 表示項目
	columns = [
			{id:"id", name:"", field:"id", width: 40, sortable: true},
			{id:"uname", name:"名前", field:"uname", width: 100, minWidth: 75, sortable: true},
			{id:"tdate", name:"日付", field:"tdate", width: 100, minWidth: 75, sortable: true},
			{id:"dayofweek", name:"曜", field:"dayofweek", minWidth: 25, sortable: true},
			{id:"intime", name:"出勤", field:"intime", minWidth: 50, sortable: true, formatter: IntimeNumericRangeFormatter},
			{id:"movetime", name:"移動", field:"movetime", minWidth: 50, sortable: true, formatter: MovetimeNumericRangeFormatter},
			{id:"outtime", name:"退勤", field:"outtime", minWidth: 50, sortable: true, formatter: OuttimeNumericRangeFormatter},
			{id:"wname", name:"勤務種別", field:"wname", minWidth: 90, sortable: true},
			{id:"note", name:"業務内容", field:"note", minWidth: 65, sortable: true},
			{id:"wcount", name:"処理", field:"wcount", minWidth: 35, sortable: true},
			{id:"rcount", name:"申出", field:"rcount", minWidth: 35, sortable: true},
			{id:"wname_move", name:"(移)勤務種別", field:"wname_move", minWidth: 50, sortable: true},
			{id:"note_move", name:"(移)業務内容", field:"note_move", minWidth: 50, sortable: true},
			{id:"wcount_move", name:"(移)処理", field:"wcount_move", minWidth: 35, sortable: true},
			{id:"rcount_move", name:"(移)申出", field:"rcount_move", minWidth: 35, sortable: true},
			{id:"breaktime", name:"休憩", field:"breaktime", minWidth: 35, sortable: true},
			{id:"fare", name:"交通費", field:"fare", minWidth: 35, sortable: true},
	];

	// 全体オプション
	options = {
		multiColumnSort: true,
		showHeaderRow: true,
		headerRowHeight: 30,			// ヘッダーの高さ
		rowHeight: 22,					// 行の高さ
		autoHeight: true,				// 表示行数分Gridの高さを伸ばす
		enableColumnReorder: false,		// 列の変更を無効
		forceFitColumns: true,			// 余っている場合は余分を等しく分配してくれる
		explicitInitialization: true,	// textareaを入れる
		enableTextSelectionOnCells:true	// text選択許可
	};

	// データ取得
	getData(isMy, false);

	// 検索用カラムID
	var nextCid = data.length + 1;

	// インスタンス化
	dataView = new Slick.Data.DataView();
	grid = new Slick.Grid("#grid", dataView, columns, options);
	grid.setSelectionModel(new Slick.RowSelectionModel());

	// ソート
	grid.onSort.subscribe(function (e, args) {
		var cols = args.sortCols;
		// 比較する
		dataView.sort(function (dataRow1, dataRow2) {
			if(cols.length == 0) {
				return;
			}
			var field = cols[0].sortCol.field;
			var sign = cols[0].sortAsc ? 1 : -1;
			var func;
			if(field == "id" || field == "wcount"
					|| field == "rcount" || field == "wcount_move"
						|| field == "rcount_move" || field == "breaktime" || field == "fare") {
				// intで計算
				func = function(value1, value2) {
					try {
						value1 = parseInt(value1);
						value2 = parseInt(value2);
						return (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
					} catch (e) {
						return 0;
					}
				}
			} else {
				// unicodeで計算
				func = function(value1, value2) {
					return (value1 == value2 ? 0 : (value1 > value2 ? 1 : -1)) * sign;
				}
			}
			var value1;
			var value2;
			var result;
			for (var i = 0, l = cols.length; i < l; i++) {
				value1 = dataRow1[field];
				value2 = dataRow2[field];
				result = func(value1, value2);
				if (result != 0) {
					return result;
				}
			}
			return 0;
		});
	});

	// 検索用入力欄を作成
	grid.onHeaderRowCellRendered.subscribe(function (e, args) {
		// 親ノード
		var cell = $(args.node);
		cell.css("height", "100%");
		// id列には「検索」の文字
		if (args.column.id === "id") {
			cell.css("text-align", "center");
			cell.html("検索");
			return;
		}
		// 検索テキストクリア用のボタンを配置
		if(args.column.id === "tclog_btn") {
			cell.css("text-align", "center");
			cell.html("<input type='button' value='×' onclick='clickTextClear()'>");
			return;
		}
		// その他のボタン列には何もしない
		else if (args.column.id.indexOf("_btn") != -1) {
			return;
		}
		// <input type="text">
		cell.empty();
		$("<input>")
			.attr("type", "text")
//			.attr("placeholder", "入力")
			.data("columnId", args.column.id)	// columnIdという名前でカラムにIDを紐付けておく
			.val(columnFilters[args.column.id])
			.appendTo(cell);
	});

	// 検索欄に入力されている部分のみで集める
	function updateFilters() {
		var columnId = $(this).data("columnId");	// ID取り出し
		if (columnId != null) {
			columnFilters[columnId] = $.trim($(this).val());
			dataView.refresh();
		}
	}
	$(grid.getHeaderRow()).japaneseInputChange('input[type=text]', updateFilters);

	if(!isMy) {
		// [編集]ボタン列追加
		var timecard_btn = {
			id:"timecard_btn",
			name:"",
			field:"timecard_btn",
			width: 40,
			sortable: false,
			formatter: timecardUpdateFormatter
		}
		columns.push(timecard_btn);

		// [作業内容]ボタン列追加
		var work_btn = {
			id:"work_btn",
			name:"",
			field:"work_btn",
			width: 60,
			sortable: false,
			formatter: timecardWorkUpdateFormatter
		}
		columns.push(work_btn);
		//columns.splice(1, 0, timecard_btn);	// 左に持ってくる場合に
	}

	// [ログ確認]ボタン列追加
	var tclog_btn = {
			id:"tclog_btn",
			name:"",
			field:"tclog_btn",
			width: 40,
			sortable: false,
			formatter: tclogViewerFormatter
		}
	columns.push(tclog_btn);

	// 表を作成
	grid.init();

	// カラムの表示位置が変更された場合のイベントハンドラ
	dataView.onRowCountChanged.subscribe(function (e, args) {
		grid.updateRowCount();
		grid.render();
	});

	// カラムに関する描画更新イベントハンドラ
	dataView.onRowsChanged.subscribe(function (e, args) {
		grid.invalidateRows(args.rows);
		grid.render();
	});

	// DataView初期化
	dataView.beginUpdate();
	dataView.setItems(data_grid);	// デフォルトで"id"カラムを行識別子としている。第二引数で指定可能
	dataView.setFilter(filter);
	dataView.endUpdate();

	// ページ分け機能
	pager = new Slick.Controls.Pager(dataView, grid, $("#timecard_pager"));

	// ページトップへスクロールするリンク
	$("#pager-top-scroll").click(function() {
		var userAgent = window.navigator.userAgent.toLowerCase();	// safari判定
		$(userAgent.indexOf('safari') ? 'body' : 'html').animate({scrollTop: 0}, 500, 'swing');
		return false;
	});
}

// 検索文字をクリア
function clickTextClear() {
	$('.slick-headerrow-columns').find('input[type="text"]').val("");
}

// 検索文字列で検索
var columnFilters = {};
function filter(item) {	// CSV出力時にも使用する
	var c;
	var field;
	var val;
	// 全カラムを検査
	for (var columnId in columnFilters) {
		// 検索文字列が入力されているかをチェック
		if (columnId !== undefined && columnFilters[columnId] !== "") {
			c = grid.getColumns()[grid.getColumnIndex(columnId)];
			field = c.field;
			if(field == "intime" || field == "movetime" || field == "outtime") {
				var length = field.length;
				var name = name = field.slice(0, field.indexOf("time"));	// **timeの"**"部分を抽出
				val = item[name + "H"] + item[name + "M"];	// 時分4桁の文字列を作成
			} else {
				val = item[field];
			}
			if (val === undefined || val.indexOf(columnFilters[columnId]) === -1) {	// 一致する部分がなければfalse
				return false;
			}
		}
	}
	return true;
}

/**
 * タイムカードを取得します。
 * @param isMy true:一般ユーザ, false:管理者
 * @param isAll true:全件表示, false:今月分
 */
function getData(isMy, isAll) {
	showLoadingDialog("データを取得中...");	// 待機ダイアログ表示

	// 一般ユーザの場合
	if(isMy) {
		// ユーザデータの取得
		var uText = "uid=" + uid;
		$.getJSON("query?action=query_users_uid", uText, function(json, status) {
			if(isErrorFunction(json)) return;
			myData = json.datas[0];	// myDataはmy-history.jsに定義されているグローバル変数
			$("#my_code").html(myData.code);	// update_my.html参照
			$("#my_name").html(myData.name);
			$("#my_kana").html(myData.kana);
			$("#my_mailladd").html(myData.mailladd);
		});
		// タイムカードの取得
		if(isAll) {
			var tcText = "uid=" + uid;
			$.getJSON("query?action=get_timecards", tcText, function(json, status) {
				response(json);
			});
		} else {
			// タイムカードの取得
			var tcText = "uid=" + uid;
			$.getJSON("query?action=query_timecard_nowmonth_my", tcText, function(json, status) {
				response(json);
			});
		}
		return;
	}

	// 管理者の場合
	// [全件表示]の処理
	if(isAll) {
		// タイムカードの取得
		$.getJSON("query?action=get_timecards", "", function(json, status) {
			response(json);
		});
	}
	// 初回時の処理
	else {
		// タイムカードの取得
		$.getJSON("query?action=query_timecard_nowmonth", "", function(json, status) {
			response(json);
		});
	}

	function response(json) {
		// error check
		if(isErrorFunction(json)) return;
		if (location.search.indexOf("action=my") !== -1 && !isAll) {
			columns = [
				{id:"id", name:"", field:"id", width: 40, sortable: true},
				{id:"uname", name:"名前", field:"uname", width: 100, minWidth: 75, sortable: true},
				{id:"tdate", name:"日付", field:"tdate", width: 100, minWidth: 75, sortable: true},
				{id:"dayofweek", name:"曜", field:"dayofweek", minWidth: 25, sortable: true},
				{id:"intime", name:"出勤", field:"intime", minWidth: 50, sortable: true, formatter: IntimeNumericRangeFormatter},
				{id:"outtime", name:"退勤", field:"outtime", minWidth: 50, sortable: true, formatter: OuttimeNumericRangeFormatter},
				{id:"wname", name:"勤務種別", field:"wname", minWidth: 90, sortable: true},
				{id:"work_btn", name:"作業内容", field:"work_btn", minWidth: 60, sortable: true, formatter: timecardWorkUpdateFormatter },
				{id:"breaktime", name:"休憩", field:"breaktime", minWidth: 35, sortable: true},
				{id:"fare", name:"交通費", field:"fare", minWidth: 35, sortable: true},
			];
			grid.setColumns(columns);
		}

		// 初期化
		data = [];
		data_grid = [];
		dataView.refresh();

		// 値挿入
		var datas = json.datas;
		insertData(datas);

		// 描画更新
		dataView.beginUpdate();
		dataView.setItems(data_grid);
		dataView.endUpdate();
		grid.invalidate();

		// select系を初期化
		selectDataInit(isMy);	// これは初回しか行わない処理

		// 待機ダイアログ閉じる
		closeLoadingDialog();
	}
}

/**
 * グローバル変数data配列を、引数に指定したデータで更新します。
 */
function insertData(datas) {
	for(var i=0; i<datas.length; i++) {
		data[i] = toRecord(i+1, datas[i]);
	}
	data_grid = data.concat();	// グリッドで操作する用の配列を複製

	// 100recordを超える場合はページャー使用
	if(data.length > 100) {
		$("span.slick-pager-settings-expanded").children("a[data='100']").click();
	}
}

/**
 * 単一のタイムカードから表示用に加工したオブジェクトを返します
 * @param id
 * @param insertData
 * @returns {___anonymous8484_9532}
 */
function toRecord(id, insertData) {
	var record = {
			id:id,
			uname:htmlDecode(insertData.uname),			// Gridに挿入される際に再度エンコードされるのでここで一度元に戻す
			ukana:htmlDecode(insertData.ukana),			// Gridに挿入される際に再度エンコードされるのでここで一度元に戻す
			tdate:insertData.tdate,
			inH:insertData.inH,
			inM:insertData.inM,
			moveH:insertData.moveH,
			moveM:insertData.moveM,
			outH:insertData.outH,
			outM:insertData.outM,
			wname:htmlDecode(insertData.wname),			// Gridに挿入される際に再度エンコードされるのでここで一度元に戻す
			note:htmlDecode(insertData.note),			// Gridに挿入される際に再度エンコードされるのでここで一度元に戻す
			wcount:insertData.wcount,
			rcount:insertData.rcount,
			wname_move:htmlDecode(insertData.wname_move),// Gridに挿入される際に再度エンコードされるのでここで一度元に戻す
			note_move:htmlDecode(insertData.note_move),	// Gridに挿入される際に再度エンコードされるのでここで一度元に戻す
			wcount_move:insertData.wcount_move,
			rcount_move:insertData.rcount_move,
			breaktime:insertData.breaktime,
			fare:insertData.fare,
			oid:insertData.oid,
			uid:insertData.uid,
			times:insertData.times,
			wid:insertData.wid,
			wid_move:insertData.wid_move,
			dayofweek:insertData.dayofweek
	};
	return record;
}

/**
 * 選択肢を初期化します。<br>
 * 引数のisMyは一般ユーザかを判定するのに使います。<br>
 */
function selectDataInit(isMy) {
	if(!isMy) {
		// 業務種別一覧取得
		$.getJSON("query?action=get_worktypes", "", function (json, status) {
			if(isErrorFunction(json)) return;
			worktypes = json.datas;	// グローバル変数へ
			// 勤務種別文字列検索イベントハンドラ登録 util.js参照
			var wBox = $("#history_worktype");
			var wText = $("#query_search_worktype_text");
			var wMessage = $("#query_search_worktype_message");
			wText.keydown({box:wBox, text:wText, message:wMessage, act:"search", target:"worktype", worktypes:worktypes}, doSearch);
			$("#query_search_worktype_button")
				.click({box:wBox, text:wText, message:wMessage, act:"search", worktypes:worktypes}, searchTextWorktype);
			$("#query_search_worktype_button_reset")
				.click({box:wBox, text:wText, message:wMessage, act:"reset", worktypes:worktypes}, searchTextWorktype);
			// 一覧作成
			var worktype = $( "#history_worktype" );
			worktype.empty();	// 初期化
			for(var i=0; i<worktypes.length; i++) {
				worktype.append($("<option>").val(worktypes[i].wid).html(worktypes[i].name));
			}
		});
		// ユーザ一覧取得
		$.getJSON("query?action=get_users", "", function (json, status) {
			if(isErrorFunction(json)) return;
			users = json.datas;	// グローバル変数へ
			// ユーザ文字列検索イベントハンドラ登録 util.js参照
			var uBox = $("#history_user");
			var uText = $("#query_search_users_text");
			var uMessage = $("#query_search_users_message");
			uText.keydown({box:uBox, text:uText, message:uMessage, act:"search", target:"users", users:users}, doSearch);
			$("#query_search_users_button")
				.click({box:uBox, text:uText, message:uMessage, act:"search", users:users}, searchTextUsers);
			$("#query_search_users_button_reset")
				.click({box:uBox, text:uText, message:uMessage, act:"reset", users:users}, searchTextUsers);
			// 一覧作成
			var user = $("#history_user");
			user.empty();	// 初期化
			for(var i=0; i<users.length; i++) {
				user.append($("<option>").val(users[i].uid).html(users[i].name));
			}
		});
	}

	// カレンダー登録
	$.datepicker.setDefaults( $.datepicker.regional[ "ja" ] );
	$( "#tdate_range1" ).datepicker();
	$( "#tdate_range1" ).datepicker("option", "showOn", 'both');
	$( "#tdate_range1" ).datepicker("option", "buttonImageOnly", true);
	$( "#tdate_range1" ).datepicker("option", "buttonImage", '../images/calendar.gif');
	$( "#tdate_range2" ).datepicker();
	$( "#tdate_range2" ).datepicker("option", "showOn", 'both');
	$( "#tdate_range2" ).datepicker("option", "buttonImageOnly", true);
	$( "#tdate_range2" ).datepicker("option", "buttonImage", '../images/calendar.gif');

	// 年月取得
	var $year = $("#history_tdate_year");
	var $month = $("#history_tdate_month");
	$year.empty();
	$month.empty();
	$year.append($("<option>").val("--").html("--"));
	$month.append($("<option>").val("--").html("--"));
	var underYear = 2010;
	var y = new Date().getFullYear() + 1;	//来年分も表示
	for(var i=y; i>=underYear; i--) {
		$year.append($("<option>").val(i).html(i));
	}
	for(var i=1; i<=12; i++) {
		$month.append($("<option>").val(i).html(i));
	}

	// 時間一覧取得
	wantAppendTimeHSelects();
	wantAppendTimeMSelects();
}

function wantAppendTimeHSelects() {
	var uhInH = $( "#history_intime_h" );
	var uhMoveH = $( "#history_movetime_h" );
	var uhOutH = $( "#history_outtime_h" );
	uhInH.empty();
	uhMoveH.empty();
	uhOutH.empty();

	// "--"を許可する
	uhInH.append($("<option>").val("--").html("--"));
	uhMoveH.append($("<option>").val("--").html("--"));
	uhOutH.append($("<option>").val("--").html("--"));

	var tmp;
	for(var i=0; i<=23; i++) {
		tmp = i < 10 ? ("0"+i) : i;
		uhInH.append($("<option>").val(tmp).html(tmp));
		uhMoveH.append($("<option>").val(tmp).html(tmp));
		uhOutH.append($("<option>").val(tmp).html(tmp));
	}

	uhInH.val("--");
	uhMoveH.val("--");
	uhOutH.val("--");
}

function wantAppendTimeMSelects() {
	var uhInM = $( "#history_intime_m" );
	var uhMoveM = $( "#history_movetime_m" );
	var uhOutM = $( "#history_outtime_m" );
	uhInM.empty();
	uhMoveM.empty();
	uhOutM.empty();

	// outだけは"--"を許可する
	uhInM.append($("<option>").val("--").html("--"));
	uhMoveM.append($("<option>").val("--").html("--"));
	uhOutM.append($("<option>").val("--").html("--"));

	for(var i=0; i<=3; i++) {
		tmp = i == 0 ? ("0"+i) : (i * 15);
		uhInM.append($("<option>").val(tmp).html(tmp));
		uhMoveM.append($("<option>").val(tmp).html(tmp));
		uhOutM.append($("<option>").val(tmp).html(tmp));
	}

	uhInM.val("--");
	uhMoveM.val("--");
	uhOutM.val("--");
}

/**
 * 表示列を日付範囲で絞込み
 */
function narrowingDateRange() {
	showLoadingDialog("絞込み中...");

	var queryData = getShowRecords();	// util.js参照

	// 表示行から条件で絞込む
	var resultData = [];
	var tdate1 = $("#tdate_range1").val();
	var tdate2 = $("#tdate_range2").val();
	if(tdate1 == "" && tdate2 == "") return window.alert("日付が選択されていません。");
	if(tdate1 == "") {
		tdate1 = 0;
	} else {
		tdate1 = Number(tdate1.replace(/\//g, ""));	// /を削除
	}
	if(tdate2 == "") {
		tdate2 = 99999999;	// 9999/99/99 最大値
	} else {
		tdate2 = Number(tdate2.replace(/\//g, ""));
	}
	var tdate;
	for(var key in queryData) {
		tdate = Number(queryData[key].tdate.replace(/\//g, ""));
		if(tdate1 <= tdate && tdate <= tdate2) {
			resultData.push(queryData[key]);
		}
	}
	data_grid = resultData;

	// 描画更新
	dataView.beginUpdate();
	dataView.setItems(data_grid);
	dataView.endUpdate();
	grid.invalidate();

	closeLoadingDialog();
}
function resetDateRange() {
	$("#tdate_range1").val("");
	$("#tdate_range2").val("");
}

/**
 * 表示列を時間範囲で絞込み
 */
function narrowingTimeRange() {
	showLoadingDialog("絞込み中...");

	var queryData = getShowRecords();	// util.js参照

	// 入力確認
	var inH = $( "#history_intime_h" ).val();
	var inM = $( "#history_intime_m" ).val();
	var moveH = $( "#history_movetime_h" ).val();
	var moveM = $( "#history_movetime_m" ).val();
	var outH = $( "#history_outtime_h" ).val();
	var outM = $( "#history_outtime_m" ).val();
	if(inH == "--" && inM == "--" && moveH == "--" && moveM == "--" && outH == "--" && outM == "--") {
		return window.alert("時間が選択されていません。");
	}
	if((inH == "--" && inM != "--") || (inH != "--" && inM == "--")) {
		return window.alert("出勤の値が不正です。\n[時][分]を正しく選択してください。");
	}
	if((moveH == "--" && moveM != "--") || (moveH != "--" && moveM == "--")) {
		return window.alert("移動の値が不正です。\n[時][分]を正しく選択してください。");
	}
	if((outH == "--" && outM != "--") || (outH != "--" && outM == "--")) {
		return window.alert("退勤の値が不正です。\n[時][分]を正しく選択してください。");
	}

	// 時間取得
	var INTIME = inH + inM;
	var MOVETIME = moveH + moveM;
	var OUTTIME = outH + outM;
	if(INTIME.indexOf("-") != -1) INTIME = -1;
	else INTIME = Number(INTIME);
	if(MOVETIME.indexOf("-") != -1) MOVETIME = -1;
	else MOVETIME = Number(MOVETIME);
	if(OUTTIME.indexOf("-") != -1) OUTTIME = -1;
	else OUTTIME = Number(OUTTIME);

	// 絞込方法確定
	var isIn = false;		// 1
	var isInMo = false;		// 2
	var isInMoOu = false;	// 3
	var isInOu = false;		// 4
	var isMo = false;		// 5
	var isMoOu = false;		// 6
	var isOu = false;		// 7
	if(INTIME == -1) {
		if(MOVETIME == -1) isOu = true;	// 7
		else {
			if(OUTTIME == -1) isMo = true;	// 5
			else isMoOu = true;	// 6
		}
	} else {
		if(MOVETIME == -1) {
			if(OUTTIME == -1) isIn = true;	// 1
			else isInOu = true;	// 4
		} else {
			if(OUTTIME == -1) isInMo = true;	// 2
			else isInMoOu = true;	// 3
		}
	}

	// 絞込
	var resultData = [];
	var intime;
	var movetime;
	var outtime;
	var r;
	for(var key in queryData) {
		r = queryData[key];
		intime = r.inH + r.inM;
		movetime = r.moveH + r.moveM;
		outtime = r.outH + r.outM;
		if(intime.indexOf("-") != -1) intime = -1;
		else intime = Number(intime);
		if(movetime.indexOf("-") != -1) movetime = -1;
		else movetime = Number(movetime);
		if(outtime.indexOf("-") != -1) outtime = -1;
		else outtime = Number(outtime);

		     if(isIn && INTIME <= intime) resultData.push(r);
		else if(isInMo && INTIME <= intime && movetime <= MOVETIME) resultData.push(r);
		else if(isInMoOu && INTIME == intime && movetime == MOVETIME && outtime == OUTTIME) resultData.push(r);
		else if(isInOu && INTIME <= intime && outtime <= OUTTIME) resultData.push(r);
		else if(isMo && MOVETIME <= movetime) resultData.push(r);
		else if(isMoOu && MOVETIME <= movetime && outtime <= OUTTIME) resultData.push(r);
		else if(isOu && outtime <= OUTTIME) resultData.push(r);
	}
	data_grid = resultData;

	// 描画更新
	dataView.beginUpdate();
	dataView.setItems(data_grid);
	dataView.endUpdate();
	grid.invalidate();

	closeLoadingDialog();
}
function resetTimeRange() {
	$( "#history_intime_h" ).val("--");
	$( "#history_intime_m" ).val("--");
	$( "#history_movetime_h" ).val("--");
	$( "#history_movetime_m" ).val("--");
	$( "#history_outtime_h" ).val("--");
	$( "#history_outtime_m" ).val("--");
}

/**
 * Gridへの表示フォーマット(intime)
 */
function IntimeNumericRangeFormatter(row, cell, value, columnDef, dataContext) {
	return dataContext.inH + " : " + dataContext.inM;
}
/**
 * Gridへの表示フォーマット(movetime)
 */
function MovetimeNumericRangeFormatter(row, cell, value, columnDef, dataContext) {
	return dataContext.moveH + " : " + dataContext.moveM;
}
/**
 * Gridへの表示フォーマット(outtime)
 */
function OuttimeNumericRangeFormatter(row, cell, value, columnDef, dataContext) {
	return dataContext.outH + " : " + dataContext.outM;
}

/**
 * 作業内容更新ボタンモデル
 * @param row
 * @param cell
 * @param value
 * @param columnDef
 * @param dataContext
 * @returns
 */
function timecardWorkUpdateFormatter(row, cell, value, columnDef, dataContext) {
    // どの行がクリックされたか判定するためにbuttonのvalueにidを紐付けておく
    return "<button class='gridSelectButton' value='"+dataContext.id+"' onClick=workUpdateHandleClick(event)>作業内容</button>";
}

/**
 * 作業内容更新メソッド
 */
function workUpdateHandleClick(e) {
    // どのbuttonがクリックされたかを判断し、該当するdataを取得
    var rowData = data.filter(function(item, index) {
        if(item.id == e.target.value) return true;
    })[0];

    location.href = "./works?tdate=" + rowData.tdate.split("/").join("") + "&uid=" + rowData.uid;
}

/**
 * 検索メニューを初期化します。
 */
function allClear() {
	$( "#query_showall_button" ).click();	// 強制全表示
	queryReset();		// 検索メニューリセット
}
function allClearForAdmin() {
	getData(false, false);	// 今月分表示
	queryReset();
}