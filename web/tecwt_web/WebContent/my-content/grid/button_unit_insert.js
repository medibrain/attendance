/**
 * ユーザ追加ボタンの処理を定義。
 */

function unitInsertInit() {
	$("#insert_unit_button").on("click", unitInsert);
}

/**
 * ユーザ追加メソッド
 */
function unitInsert() {

	// フォーム内容リセット
	$('#dialog-form_unit_insert_form').find('input[type=\"text\"]').val("");

	// formダイアログ
	var dialog = $( "#dialog-form_unit_insert" ).dialog({
		autoOpen: false,
		maxHeight: $(window).height(),	// 表示領域の高さまで
		width: 500,
		minHeight: 300,
		modal: true,
		buttons: {
			"確定": function () {

				// 入力確認
				var pass = $("#dialog-form_unit_insert_pass").val();
				if(pass == "") return window.alert("パスが入力されていません。");

				// 引数に渡す万能データ
				var datas = {
					functions:[standbyOverlap, insert],	// 処理の順番を設定
					functionCount:0,	// 現在処理中のインデックス番号を設定
				};

				// functionsの第一要素から順に実行していく
				datas.functions[datas.functionCount](datas);
			},
			"キャンセル": function () {
				dialog.dialog( "close" );
			}
		},
		close: function () {
			// 毎回初期化
		}
	});

	dialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();	// アクションをキャンセル
	});

	dialog.dialog( "open" );

	/**
	 * すでに登録待機状態になっているものがないかチェック
	 */
	function standbyOverlap(datas) {
		var unitid = "standby";
		datas.functionCount = datas.functionCount + 1;	// カウントを進める
		var nextFunction = datas.functions[datas.functionCount];
		showLoadingDialog("データを照合中...");	// 待機ダイアログ表示
		$.getJSON("query?action=query_unit_unitid", {unitid:unitid}, function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				if(json.datas.length > 0) {
					dialog.dialog("close");	// formダイアログ閉じる
					$("#update_complete_dialog").html(
							"<span style='color:#ff0000;'>すでに待機状態となっているものがあります。</span><br>" +
							"登録を完了させてから再度追加を行って下さい。<br>" +
							"※パスワードを変更する場合は表右側の[編集]ボタンを押して編集画面を開いて下さい。");
					ajaxFinishFunction();
					return;
				}
				if(nextFunction != null) {
					nextFunction(datas);	// 次の関数を実行
				}
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * 登録処理
	 */
	function insert() {
		// formダイアログ閉じる
		dialog.dialog("close");

		// 待機ダイアログ表示
		showLoadingDialog("データを作成中...");

		// Ajax通信
		$.ajax({
			type: "POST",
			url: "update?action=insert_unit",
			data: $( "#dialog-form_unit_insert_form" ).serialize()
		}).done(function (json, status, xhr) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 成功
			if(json.result == "success") {
				$("#update_complete_dialog").html(
						"端末受付枠を追加しました。<br>" +
						"<br>" +
						"専用アプリを起動し、初回起動時に表示される画面にて設定したパスワードを入力してください。<br>" +
						"また「組織ID」には半角数字の「<span style='color:#ff0000;'>" + json.oid +"</span>」を入力してください。<br>" +
						"<br>" +
						"端末の登録に成功すると、以降は認証なしでアプリを使用することができるようになります。<br>" +
						"<br>" +
						"※端末登録に成功した場合でも、データの反映には時間がかかる場合があります。（その場合、受付枠を追加することはできません）");
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
			}
			ajaxFinishFunction();
		}).fail(function (json, status, error) {
			// 失敗
			$("#update_complete_dialog").html(
					"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
			ajaxFinishFunction();
		});
	}

	/**
	 * Ajax通信成功時
	 */
	function ajaxFinishFunction() {
		closeLoadingDialog();	// 待機ダイアログ閉じる
		// 通信結果ダイアログ
		$("#update_complete_dialog").dialog({
			title: "通信結果",
			modal: true,
			buttons: {
				"OK": function() {
					allClear();	// 全体更新
					$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
				}
			},
			maxWidth: 500,
			maxHeight: 500,
			resizable: false
		});
	}
}

