/**
 * タイムカード作成ボタンの処理を定義。
 */

function timecardInsertInit() {
	$("#insert_timecard_button").on("click", timecardInsert);
}

/**
 * 有給チェックボックス
 * @param check
 */
function clickVacation(check) {
	if(check) {
		$("#dialog-form_timecard_insert_intime_h").val("00");
		$("#dialog-form_timecard_insert_intime_m").val("00");
		$("#dialog-form_timecard_insert_movetime_h").val("--");
		$("#dialog-form_timecard_insert_movetime_m").val("--");
		$("#dialog-form_timecard_insert_outtime_h").val("00");
		$("#dialog-form_timecard_insert_outtime_m").val("00");
		var obj = $("#dialog-form_timecard_insert_wid").children();
		var wname;
	    for( var i=0; i<obj.length; i++ ){
	    	wname = obj.eq(i).text();
	    	if(wname == "有給" || wname == "有休") {
	    		$("#dialog-form_timecard_insert_wid").val(obj.eq(i).val());
	    		break;
	    	}
	    }
	} else {
		$("#dialog-form_timecard_insert_intime_h").val("00");
		$("#dialog-form_timecard_insert_intime_m").val("00");
		$("#dialog-form_timecard_insert_movetime_h").val("--");
		$("#dialog-form_timecard_insert_movetime_m").val("--");
		$("#dialog-form_timecard_insert_outtime_h").val("--");
		$("#dialog-form_timecard_insert_outtime_m").val("--");
		$("#dialog-form_timecard_insert_wid").val("0");
	}
}

/**
 * タイムカード作成メソッド
 */
function timecardInsert(isContinue, DATE, INH, INM, MOVEH, MOVEM, OUTH, OUTM, WID, isVacation) {

	var cont = (
			isContinue == null ||
			DATE == null ||
			INH == null ||
			INM == null ||
			MOVEH == null ||
			MOVEM == null ||
			OUTH == null ||
			OUTM == null ||
			WID == null ||
			isVacation == null)
			? false : true;

	// フォーム内容リセット
	$('#dialog-form_timecard_insert_form').find('input[type=\"text\"]').val("");
	$('#dialog-form_timecard_insert_form').find('textarea').val("");
	$('#dialog-form_timecard_insert_form').find('option').attr("selected",false);

	// 選択肢作成
	setDatepicker(DATE);
	getUsers();
	getWorktypes(WID);
	setTimes(INH, INM, MOVEH, MOVEM, OUTH, OUTM);

	// 凍結を除くチェック変更時イベントハンドラ
	$("#dialog-form_timecard_insert_search_worktype_enable").change(function () {
		getWorktypes(WID);
	});
	$("#dialog-form_timecard_insert_search_worktype_move_enable").change(function () {
		getWorktypes(WID);
	});
	// 仮入力ボタン押下時は勤務種別を仮入力にする
	$("#dialog-form_timecard_insert_temporary").on("click", function() {
		var obj = $("#dialog-form_timecard_insert_wid").children();
	    for( var i=0; i<obj.length; i++ ){
	    	if(obj.eq(i).text() === "仮入力") {
	    		$("#dialog-form_timecard_insert_wid").val(obj.eq(i).val());
	    		break;
	    	}
	    }
    });

	// 名前変更時イベントハンドラ
	$("#dialog-form_timecard_insert_uid").change(function () {
		$("#dialog-form_timecard_insert_uname").val($("#dialog-form_timecard_insert_uid option:selected").text());
	});

	// 業務種別変更時イベントハンドラ
	$("#dialog-form_timecard_insert_wid").change(function () {
		$("#dialog-form_timecard_insert_wname").val($("#dialog-form_timecard_insert_wid option:selected").text());
	})
	$("#dialog-form_timecard_insert_wid_move").change(function () {
		$("#dialog-form_timecard_insert_wname_move").val($("#dialog-form_timecard_insert_wid_move option:selected").text());
	});

	// 移動時刻変更時イベントハンドラ
	$("#dialog-form_timecard_insert_movetime_h").change(function () {
		var moveM = $("#dialog-form_timecard_insert_movetime_m");
		if(moveM.val() == "--") {
			moveM.val("00").change();	// 分を00にしてあげる
		}
	});

	// 退勤時刻変更時イベントハンドラ
	$("#dialog-form_timecard_insert_outtime_h").change(function () {
		var outM = $("#dialog-form_timecard_insert_outtime_m");
		if(outM.val() == "--") {
			outM.val("00").change();	// 分を00にしてあげる
		}
	});

	// 連続登録なら初期値設定
	if(cont) {
		if(isVacation) {
			$("#dialog-form_timecard_insert_vacation").prop("checked", true);
		} else {
			$("#dialog-form_timecard_insert_vacation").prop("checked", false);
		}
	} else {
		$("#dialog-form_timecard_insert_vacation").prop("checked", false);
	}

	// 初期値
	$("#dialog-form_timecard_insert_breaktime").val("0");
	$("#dialog-form_timecard_insert_fare").val("0");
	$("#dialog-form_timecard_insert_wcount").val("0");
	$("#dialog-form_timecard_insert_rcount").val("0");

	// formダイアログ
	var dialog = $( "#dialog-form_timecard_insert" ).dialog({
		autoOpen: false,
		maxHeight: $(window).height(),	// 表示領域の高さまで
		width: 500,
		minHeight: 300,
		modal: true,
		buttons: {
			"確定": function () {

				/*
				 * 入力確認
				 */
				// 名前
				var uid = $("#dialog-form_timecard_insert_uid").val();
				if(uid == "" || uid == "----") return window.alert("名前が選択されていません。");
				// 日付
				var tdate = $("#dialog-form_timecard_insert_tdate").val();
				if(tdate == "") return window.alert("日付が選択されていません。");
				// 時間
				var intime = $("#dialog-form_timecard_insert_intime_h").val() + $("#dialog-form_timecard_insert_intime_m").val();
				var movetime = $("#dialog-form_timecard_insert_movetime_h").val() + $("#dialog-form_timecard_insert_movetime_m").val();
				var outtime = $("#dialog-form_timecard_insert_outtime_h").val() + $("#dialog-form_timecard_insert_outtime_m").val();
				var isIn = false;			// パターン① 出勤のみ
				var isInMove = false;		// パターン② 出勤と移動
				var isInMoveOut = false;	// パターン③ 出勤と移動と退勤
				var isInOut = false;		// パターン④ 出勤と退勤
				if(intime == "----") return window.alert("出勤が入力されていません。");
				if(intime.indexOf("-") != -1) return window.alert("出勤の値が不正です。");
				if(movetime.indexOf("-") == -1) {
					if(outtime == "----" ) {
						// ②
						isInMove = true;
					} else {
						if(outtime.indexOf("-") == -1) {
							// ③
							isInMoveOut = true;
						} else {
							// outtimeの値が不正
							return window.alert("退勤の値が不正です。");
						}
					}
				} else {
					if(movetime != "----") return window.alert("移動の値が不正です。");
					if(outtime == "----" ) {
						// ①
						isIn = true;
					} else {
						if(outtime.indexOf("-") == -1) {
							// ④
							isInOut = true;
						} else {
							// outtimeの値が不正
							return window.alert("退勤の値が不正です。");
						}
					}
				}
				// その他項目 (時間の入力パターンによって必須項目が変化)
				var ismove = $("#dialog-form_timecard_insert_ismove");
				if(isIn) {
					// ①
					if(check(false ,true ,false,false,false,true ,false,false,false,false)) return;
					ismove.val("false");
				} else if(isInMove) {
					// ②
					if(Number(intime) > Number(movetime)) return window.alert("出勤を移動より遅くすることはできません。");
					if(check(true ,false,true ,true ,false ,true ,false,false,false,false)) return;
					ismove.val("true");
				} else if(isInMoveOut) {
					// ③
					if(Number(intime) > Number(movetime)) return window.alert("出勤を移動より遅くすることはできません。");
					if(Number(movetime) > Number(outtime)) return window.alert("移動を退勤より遅くすることはできません。");
					if(check(true ,false,true ,true ,true ,false,true ,true ,true ,true )) return;
					ismove.val("true");
				} else if(isInOut) {
					// ④
					if(Number(intime) > Number(outtime)) return window.alert("出勤を退勤より遅くすることはできません。");
					if(check(true ,false,true ,true ,false,true ,false,false,true ,true )) return;
					ismove.val("false");
				} else {
					return window.alert("出勤、移動、退勤が正しく入力されていません。");
				}

				/**
				 * 引数<br>
				 * true:打刻パターンにおける必須項目<br>
				 * false:打刻パターンにおける存在してはいけない項目<br>
				 * ただしnote,noteMは、存在してはいけない(true),任意(false)で指定する。<br>
				 * 戻り値がtrueである場合は登録キャンセル。falseは進行。
				 */
				function check(wid, note, wcount, rcount, widM, noteM, wcountM, rcountM, breaktime, fare) {
					if(checkWid(wid, false)) return true;
					if(note) {
						if(checkNoExistNote(false)) return true;
					}
					if(checkWcount(wcount, false)) return true;
					if(checkRcount(rcount, false)) return true;
					if(checkWid(widM, true)) return true;
					if(noteM) {
						if(checkNoExistNote(true)) return true;
					}
					if(checkWcount(wcountM, true)) return true;
					if(checkRcount(rcountM, true)) return true;
					if(checkBreaktime(breaktime)) return true;
					if(checkFare(fare)) return true;
				}
				function checkWid(exist, ismove) {
					var wid;
					var text;
					if(ismove) {
						wid = $("#dialog-form_timecard_insert_wid_move");
						text = "(移)勤務種別";
					} else {
						wid = $("#dialog-form_timecard_insert_wid");
						text = "勤務種別";
					}
					if(exist) {
						if(wid.val() == "" || wid.val() == "0") {
							window.alert(text+"が選択されていません。");
							return true;
						}
						return false;
					} else {
						if(wid.val() != "" && wid.val() != "0") {
							var result = window.confirm(text+"は入力できません。\n入力した内容は破棄されますがよろしいですか？");
							if(result) {
								wid.val("0");
								return false;
							}
							return true;
						}
						return false;
					}
				}
				function checkNoExistNote(ismove) {
					var note;
					var text;
					if(ismove) {
						note = $("#dialog-form_timecard_insert_note_move");
						text = "(移)業務内容";
					} else {
						note = $("#dialog-form_timecard_insert_note");
						text = "業務内容";
					}
					if(note.val() != "") {
						var result = window.confirm(text+"は入力できません。\n入力した内容は破棄されますがよろしいですか？");
						if(result) {
							note.val("");
							return false
						}
						return true;
					}
					return false;
				}
				function checkWcount(exist, ismove) {
					var wcount;
					var text;
					if(ismove) {
						wcount = $("#dialog-form_timecard_insert_wcount_move");
						text = "(移)処理数";
					} else {
						wcount = $("#dialog-form_timecard_insert_wcount");
						text = "処理数";
					}
					if(exist) {
						if(wcount.val() == "") {
							window.alert(text+"が入力されていません。");
							return true;
						}
						if(!$.isNumeric(wcount.val()) || wcount.val() < 0) {
							window.alert(text+"の値が不正です。");
							return true;
						}
						return false;
					} else {
						if(wcount.val() != "" && wcount.val() != "0") {
							var result = window.confirm(text+"は入力できません。\n入力した内容は破棄されますがよろしいですか？");
							if(result) {
								wcount.val("0");
								return false;
							}
							return true;
						}
						return false;
					}
				}
				function checkRcount(exist, ismove) {
					var rcount;
					var text;
					if(ismove) {
						rcount = $("#dialog-form_timecard_insert_rcount_move");
						text = "(移)申出数";
					} else {
						rcount = $("#dialog-form_timecard_insert_rcount");
						text = "申出数";
					}
					if(exist) {
						if(rcount.val() == "") {
							window.alert(text+"が入力されていません。");
							return true;
						}
						if(!$.isNumeric(rcount.val()) || rcount.val() < 0) {
							window.alert(text+"の値が不正です。");
							return true;
						}
						return false;
					} else {
						if(rcount.val() != "" && rcount.val() != "0") {
							var result = window.confirm(text+"は入力できません。\n入力した内容は破棄されますがよろしいですか？");
							if(result) {
								rcount.val("0");
								return false;
							}
							return true;
						}
						return false;
					}
				}
				function checkBreaktime(exist) {
					var breaktime = $("#dialog-form_timecard_insert_breaktime");
					if(exist) {
						if(breaktime.val() == "") {
							window.alert("休憩時間が入力されていません。");
							return true;
						}
						if(!$.isNumeric(breaktime.val()) || breaktime.val() < 0) {
							window.alert("休憩時間の値が不正です。");
							return true;
						}
						if(breaktime.val() % 15 != 0) {
							window.alert("休憩時間 は0もしくは15分刻みで入力して下さい。");
							return true;
						}
						return false;
					} else {
						if(breaktime.val() != "" && breaktime.val() != "0") {
							var result = window.confirm("休憩時間は入力できません。\n入力した内容は破棄されますがよろしいですか？");
							if(result) {
								breaktime.val("0");
								return false;
							}
							return true;
						}
						return false;
					}
				}
				function checkFare(exist) {
					var fare = $("#dialog-form_timecard_insert_fare");
					if(exist) {
						if(fare.val() == "") {
							window.alert("交通費が入力されていません。");
							return true;
						}
						if(!$.isNumeric(fare.val()) || fare.val() < 0) {
							window.alert("交通費の値が不正です。");
							return true;
						}
						return false;
					} else {
						if(fare.val() != "" && fare.val() != "0") {
							var result = window.confirm("交通費は入力できません。\n入力した内容は破棄されますがよろしいですか？");
							if(result) {
								fare.val("0");
								return false;
							}
							return true;
						}
						return false;
					}
				}

				// 改行コードを削除
				var $note = $("#dialog-form_timecard_insert_note");
				var $note_move = $("#dialog-form_timecard_insert_note_move");
				$note.val(deleteCRLF($note.val()));	// deleteCRLF(): util.js参照
				$note_move.val(deleteCRLF($note_move.val()));

				// 引数に渡すデータ
				var datas = {
					uid:uid,
					tdate:tdate,
					functions:[checkOverlapTimecard, insert],	// 処理の順番を設定
					functionCount:0,							// 現在処理中のインデックス番号を設定
				};

				// functionsの第一要素から順に実行していく
				datas.functions[datas.functionCount](datas);
			},
			"キャンセル": function () {
				dialog.dialog( "close" );
			}
		},
		close: function () {
			// 毎回初期化
		}
	});

	dialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();	// アクションをキャンセル
	});

	dialog.dialog( "open" );

	/**
	 * カレンダー設定
	 */
	function setDatepicker(date) {
		$("#dialog-form_timecard_insert_tdate").datepicker();
		$("#dialog-form_timecard_insert_tdate").datepicker("option", "showOn", 'both');
		$("#dialog-form_timecard_insert_tdate").datepicker("option", "buttonImageOnly", true);
		$("#dialog-form_timecard_insert_tdate").datepicker("option", "buttonImage", '../images/calendar.gif');
		if(date != null) {
			$("#dialog-form_timecard_insert_tdate").val(date);
		}
	}

	/**
	 * ユーザ一覧取得
	 */
	function getUsers() {
		$("#dialog-form_timecard_insert_uid").empty();
		showLoadingDialog("データを取得中...");	// 待機ダイアログ表示
		$.getJSON("query?action=get_users", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var users = json.datas;
				// ユーザ文字列検索イベントハンドラ登録 util.js参照
				var uBox = $("#dialog-form_timecard_insert_uid");
				var uText = $("#dialog-form_timecard_insert_search_users_text");
				var uMessage = $("#dialog-form_timecard_insert_search_users_message");
				uText.keydown({box:uBox, text:uText, message:uMessage, act:"search", target:"users", users:users}, doSearch);
				$("#dialog-form_timecard_insert_search_users_button")
					.click({box:uBox, text:uText, message:uMessage, act:"search", users:users}, searchTextUsers);
				$("#dialog-form_timecard_insert_search_users_button_reset")
					.click({box:uBox, text:uText, message:uMessage, act:"reset", users:users}, searchTextUsers);
				// 一覧作成
				for(var i=0; i<users.length; i++) {
					if (users[i].enable == "true"){
						$("#dialog-form_timecard_insert_uid").append($("<option>").val(users[i].uid).html(users[i].name));
					}
				}
				$("#dialog-form_timecard_insert_uid").val(users[0].uid).change();
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * 勤務種別一覧取得
	 */
	function getWorktypes(cWid) {
		$("#dialog-form_timecard_insert_wid").empty();
		$("#dialog-form_timecard_insert_wid_move").empty();
		showLoadingDialog("データを取得中...");	// 待機ダイアログ表示
		$.getJSON("query?action=get_worktypes", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var worktypes = json.datas;
				// 勤務種別文字列検索イベントハンドラ登録 util.js参照
				var wBox = $("#dialog-form_timecard_insert_wid");
				var wText = $("#dialog-form_timecard_insert_search_worktype_text");
				var wMessage = $("#dialog-form_timecard_insert_search_worktype_message");
				var wEnable = $("#dialog-form_timecard_insert_search_worktype_enable");
				wText.keydown({box:wBox, text:wText, message:wMessage, act:"search", target:"worktype", worktypes:worktypes, enableonly:wEnable}, doSearch);
				$("#dialog-form_timecard_insert_search_worktype_button")
					.click({box:wBox, text:wText, message:wMessage, act:"search", worktypes:worktypes, enableonly:wEnable}, searchTextWorktype);//util.js
				$("#dialog-form_timecard_insert_search_worktype_button_reset")
					.click({box:wBox, text:wText, message:wMessage, act:"reset", worktypes:worktypes, enableonly:wEnable}, searchTextWorktype);
				// (移)勤務種別文字列検索イベントハンドラ登録 util.js参照
				var wBoxMove = $("#dialog-form_timecard_insert_wid_move");
				var wTextMove = $("#dialog-form_timecard_insert_search_worktype_move_text");
				var wMessageMove = $("#dialog-form_timecard_insert_search_worktype_move_message");
				var wEnableMove = $("#dialog-form_timecard_insert_search_worktype_move_enable");
				wTextMove.keydown({box:wBoxMove, text:wTextMove, message:wMessageMove, act:"search", target:"worktype", worktypes:worktypes, enableonly:wEnableMove}, doSearch);
				$("#dialog-form_timecard_insert_search_worktype_move_button")
					.click({box:wBoxMove, text:wTextMove, message:wMessageMove, act:"search", worktypes:worktypes, enableonly:wEnableMove}, searchTextWorktype);
				$("#dialog-form_timecard_insert_search_worktype_move_button_reset")
					.click({box:wBoxMove, text:wTextMove, message:wMessageMove, act:"reset", worktypes:worktypes, enableonly:wEnableMove}, searchTextWorktype);
				// 一覧作成
				$("#dialog-form_timecard_insert_wid").append($("<option>").val("0").html("----"));	// 勤務種別は無選択可
				$("#dialog-form_timecard_insert_wid_move").append($("<option>").val("0").html("----"));	// (移)勤務種別は無選択可
				for(var i=0; i<worktypes.length; i++) {
					if (!wEnable.prop("checked") || worktypes[i].enable === "true") {
						$("#dialog-form_timecard_insert_wid").append($("<option>").val(worktypes[i].wid).html(worktypes[i].name));
					}
					if (!wEnableMove.prop("checked") || worktypes[i].enable === "true") {
						$("#dialog-form_timecard_insert_wid_move").append($("<option>").val(worktypes[i].wid).html(worktypes[i].name));
					}
				}
				// 連続登録用
				if(cWid != null) {
					$("#dialog-form_timecard_insert_wid").val(cWid);
				}
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * 時間設定
	 */
	function setTimes(cInH, cInM, cMoveH, cMoveM, cOutH, cOutM) {
		/*
		 * H
		 */
		var inH = $("#dialog-form_timecard_insert_intime_h");
		var moveH = $("#dialog-form_timecard_insert_movetime_h");
		var outH = $("#dialog-form_timecard_insert_outtime_h");
		inH.empty();
		moveH.empty();
		outH.empty();

		// "--"を許可する
		//inH.append($("<option>").val("--").html("--"));
		moveH.append($("<option>").val("--").html("--"));
		outH.append($("<option>").val("--").html("--"));

		var tmp;
		for(var i=0; i<=23; i++) {
			tmp = i < 10 ? ("0"+i) : i;
			inH.append($("<option>").val(tmp).html(tmp));
			moveH.append($("<option>").val(tmp).html(tmp));
			outH.append($("<option>").val(tmp).html(tmp));
		}

		//inH.val("--");
		moveH.val("--");
		outH.val("--");

		/*
		 * M
		 */
		var inM = $("#dialog-form_timecard_insert_intime_m");
		var moveM = $("#dialog-form_timecard_insert_movetime_m");
		var outM = $("#dialog-form_timecard_insert_outtime_m");
		inM.empty();
		moveM.empty();
		outM.empty();

		// "--"を許可する
		//inM.append($("<option>").val("--").html("--"));
		moveM.append($("<option>").val("--").html("--"));
		outM.append($("<option>").val("--").html("--"));

		var tmp;
		for(var i=0; i<=3; i++) {
			tmp = i == 0 ? ("0"+i) : (i * 15);
			inM.append($("<option>").val(tmp).html(tmp));
			moveM.append($("<option>").val(tmp).html(tmp));
			outM.append($("<option>").val(tmp).html(tmp));
		}

		//inM.val("--");
		moveM.val("--");
		outM.val("--");

		// 連続登録用
		if(cInH != null && cInM != null &&
			cMoveH != null && cMoveM != null &&
			cOutH != null && cOutM != null) {
			inH.val(cInH);
			inM.val(cInM);
			moveH.val(cMoveH);
			moveM.val(cMoveM);
			outH.val(cOutH);
			outM.val(cOutM);
		}
	}

	/**
	 * 同じタイムカードが既に存在しないか確認
	 */
	function checkOverlapTimecard(datas) {
		var uid = datas.uid;
		var tdate = datas.tdate;
		datas.functionCount = datas.functionCount + 1;	// カウントを進める
		var nextFunction = datas.functions[datas.functionCount];
		showLoadingDialog("データを照合中...");	// 待機ダイアログ表示
		$.getJSON("query?action=query_timecard_overlap", {uid:uid,tdate:tdate}, function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				if(json.datas.length > 0) {
					return window.alert(
						"このタイムカードは既に存在しています。ユーザもしくは日付を再度確認してください。\n" +
						"編集を行いたい場合は該当するタイムカードを勤怠表から検索し、右側の[編集]ボタンを押すことで編集画面を開くことができます。");
				}
				if(nextFunction != null) {
					nextFunction(datas);	// 次の関数を実行
				}
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	function insert() {
		// formダイアログ閉じる
		dialog.dialog("close");

		// 待機ダイアログ表示
		showLoadingDialog("データを作成中...");

		// Ajax通信
		$.ajax({
			type: "POST",
			url: "update?action=insert_timecard",
			data: $( "#dialog-form_timecard_insert_form" ).serialize()
		}).done(function (json, status, xhr) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 成功
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				// 通信結果ダイアログ
				$("#update_complete_dialog").html("タイムカードを追加しました。<br>続けて追加しますか？");
				$("#update_complete_dialog").dialog({
					title: "通信結果",
					modal: true,
					buttons: {
						"はい": function() {
							allClearForAdmin();	// 全体更新
							$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
							// 再度フォームを表示する
							var date = $("#dialog-form_timecard_insert_tdate").val();
							var inH = $("#dialog-form_timecard_insert_intime_h").val();
							var inM = $("#dialog-form_timecard_insert_intime_m").val();
							var moveH = $("#dialog-form_timecard_insert_movetime_h").val();
							var moveM = $("#dialog-form_timecard_insert_movetime_m").val();
							var outH = $("#dialog-form_timecard_insert_outtime_h").val();
							var outM = $("#dialog-form_timecard_insert_outtime_m").val();
							var wid = $("#dialog-form_timecard_insert_wid").val();
							var isVacation = $("#dialog-form_timecard_insert_vacation").prop("checked");
							timecardInsert(true, date, inH, inM, moveH, moveM, outH, outM, wid, isVacation);
						},
						"いいえ": function() {
							allClearForAdmin();	// 全体更新
							$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
						}
					},
					maxWidth: 500,
					maxHeight: 500,
					resizable: false
				});
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		}).fail(function (json, status, error) {
			// 失敗
			$("#update_complete_dialog").html(
					"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
			ajaxFinishFunction();
		});
	}

	/**
	 * Ajax通信成功時
	 */
	function ajaxFinishFunction() {
		closeLoadingDialog();	// 待機ダイアログ閉じる
		// 通信結果ダイアログ
		$("#update_complete_dialog").dialog({
			title: "通信結果",
			modal: true,
			buttons: {
				"OK": function() {
					allClearForAdmin();	// 全体更新
					$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
				}
			},
			maxWidth: 500,
			maxHeight: 500,
			resizable: false
		});
	}
}

