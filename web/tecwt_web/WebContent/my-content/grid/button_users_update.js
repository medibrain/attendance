/**
 * ユーザ情報更新のためのグローバル関数を定義します。
 */


/**
 * ユーザ情報更新ボタンモデル
 * @param row
 * @param cell
 * @param value
 * @param columnDef
 * @param dataContext
 * @returns
 */
function userUpdateFormatter(row, cell, value, columnDef, dataContext) {
	// どの行がクリックされたか判定するためにbuttonのvalueにidを紐付けておく
	return "<button class='gridSelectButton' value='"+dataContext.id+"' onClick=usersUpdateHandleClick(event)>編集</button>";
}

/**
 * ユーザ情報更新ボタンイベントハンドラ
 * @param e
 * @returns
 */
function usersUpdateHandleClick(e) {

	// nullになる場合があったので対応
	if(e.target.value == null) {
		return;
	}
	// どのbuttonがクリックされたかを判断し、該当するdataを取得
	var rowData = data.filter(function(item, index) {
		if(item.id == e.target.value) return true;
	})[0];

	// フォーム内容リセット
	$('#dialog-form_users_update_form').find('input[type=\"text\"]').val("");
	$('#dialog-form_users_update_form').find('input[type=\"password\"]').val("");
	$('#dialog-form_users_update_form').find('option').attr("selected",false);

	// 各種フォーム
	var lname = $( "#dialog-form_users_update_lname" );
	var pass = $( "#dialog-form_users_update_pass" );
	var name = $( "#dialog-form_users_update_name" );
	var kana = $( "#dialog-form_users_update_kana" );
	var code = $( "#dialog-form_users_update_code" );
	var admin = $( "#dialog-form_users_update_admin" );
	var mailladd = $( "#dialog-form_users_update_mailladd" );
	var mailenable = $( "#dialog-form_users_update_mailenable" );
	var department = $( "#dialog-form_users_update_department" );
	var employment = $( "#dialog-form_users_update_employment" );
	var worklocation = $( "#dialog-form_users_update_worklocation" );
	var group = $( "#dialog-form_users_update_group" );
	var hiredate = $( "#dialog-form_users_update_hiredate" );
	var paidvacationdays = $( "#dialog-form_users_update_paidvacationdays" );

	// 一覧作成と値反映
	setDatepicker(rowData.hiredatetext);
	getDepartments(rowData.department);
	getEmployments(rowData.employment);
	getWorklocations(rowData.worklocation);
	getGroups(rowData.group);

	// ↓↓ 値を入れていく ↓↓

	// lname
	lname.val(rowData.lname);

	// pass
	pass.val(rowData.pass);

	// name
	name.val(rowData.name);

	// kana
	kana.val(rowData.kana);

	// code
	code.val(rowData.code);

	// admin
	admin.val(rowData.default_admin);	// rowData.adminには表示専用の文字列が入っているのでこっちを使う

	// mailladd
	mailladd.val(rowData.mailladd);

	// mailenable
	mailenable.val(rowData.default_mailenable);	// rowData.mailenableには表示専用の文字列が入っているのでこっちを使う

	// paidvacationdays
	paidvacationdays.val(rowData.paidvacationdays);

	// oid (hidden)
	$("#dialog-form_users_update_oid").val(rowData.oid);

	// uid (hidden)
	$("#dialog-form_users_update_uid").val(rowData.uid);

	// enable (hidden)
	$("#dialog-form_users_update_enable").val(rowData.enable);

	// formダイアログ
	var dialog = $( "#dialog-form_users_update" ).dialog({
		autoOpen: false,
		maxHeight: $(window).height(),	// 表示領域の高さまで
		width: 500,
		minHeight: 300,
		modal: true,
		buttons: getButtons(rowData.enable == "true"),
		close: function () {
			// 毎回初期化
			$()
		}
	});

	dialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();	// アクションをキャンセル
	});

	dialog.dialog( "open" );

	/**
	 * 状態に応じて表示ボタンを変える
	 */
	function getButtons(enable) {
		var buttons;
		if(enable) {
			return buttons = {
				"確定":ok,
				"凍結する":loginDisable,
				"キャンセル": cancel
			}
		} else {
			return buttons = {
				"確定":ok,
				"凍結を解除する":loginEnable,
				"キャンセル": cancel
			}
		}
	}

	/**
	 * [確定]ボタン処理
	 */
	function ok() {

		// 値確認
		var pvd = $("#dialog-form_users_update_paidvacationdays").val();
		if (!$.isNumeric(pvd)) return window.alert("有休残日数が数字ではありません。");
		if (pvd < 0) return window.alert("有休残日数は0以上の数値である必要があります。");

		// 変更箇所確認
		var change = "";
		if(rowData.lname != lname.val()) change += "lname,";
		if(rowData.pass != pass.val()) change += "pass,";
		if(rowData.name != name.val()) change += "name,";
		if(rowData.kana != kana.val()) change += "kana,";
		if(rowData.code != code.val()) change += "code,";
		if(rowData.default_admin != admin.val()) change += "admin,";
		if(rowData.mailladd != mailladd.val()) change += "mailladd,";
		if(rowData.default_mailenable != mailenable.val()) change += "mailenable,";
		if(rowData.department != department.val()) change += "department,";
		if(rowData.employment != employment.val()) change += "employment,";
		if(rowData.worklocation != worklocation.val()) change += "worklocation,";
		if(rowData.group != group.val()) change += "group,";
		if(rowData.hiredate != hiredate.val()) change += "hiredate,";
		if(rowData.paidvacationdays != pvd) change += "paidvacationdays,";
		// 未変更は許可しない
		if(change == "") {
			return window.alert("いずれも変更されていません。");
		}

		// IDパス入力確認
		if(change.indexOf("lname") != -1) {
			if(lname.val() != $("#dialog-form_users_update_lname_confirm").val()) return window.alert("ログインIDが違います。ログインIDを再入力してください。");
		}
		if(change.indexOf("pass") != -1) {
			if(pass.val() != $("#dialog-form_users_update_pass_confirm").val()) return window.alert("ログインパスが違います。ログインパスを再入力してください。");
		}

		// disabledな部品は送信されないので解除する
		$("#dialog-form_users_update_form :disabled").prop("disabled", false);
		// 変更箇所一覧を追加
		$( "#dialog-form_users_update_change" ).val(change);

		// lnameが変更されている場合はユニーク確認
		var functions;
		if(change.indexOf("lname") != -1) {
			functions = [checkLname, checkCode, update];
		} else {
			functions = [checkCode, update];
		}

		// 引数に渡す万能データ
		var datas = {
			change:change,
			rowData:rowData,
			lname:lname.val(),
			pass:pass.val(),
			name:name.val(),
			kana:kana.val(),
			code:code.val(),
			admin:admin.val(),
			mailladd:mailladd.val(),
			functions:functions,	// 処理の順番を設定
			functionCount:0,		// 現在処理中のインデックス番号を設定
		};

		// functionsの第一要素から順に実行していく
		datas.functions[datas.functionCount](datas);
	}

	/**
	 * [このユーザを凍結する]ボタン処理
	 */
	function loginDisable() {
		var result = window.confirm(
				"本当に凍結してもよろしいですか？\n" +
				"※凍結されたユーザはログイン及び出勤・退勤などの打刻を行うことができなくなります。")
		if(result) {
			var change = "enable,";
			$( "#dialog-form_users_update_change" ).val(change);
			$( "#dialog-form_users_update_enable" ).val("false");
			update();
		} else {
			return;
		}
	}

	/**
	 * [このユーザの凍結を解除する]ボタン処理
	 */
	function loginEnable() {
		var result = window.confirm(
				"本当に凍結を解除してもよろしいですか？\n" +
				"※凍結を解除するとログインや出勤・退勤などの打刻を行うことが可能になります。");
		if(result) {
			var change = "enable,";
			$( "#dialog-form_users_update_change" ).val(change);
			$( "#dialog-form_users_update_enable" ).val("true");
			update();
		} else {
			return;
		}
	}

	/**
	 * [キャンセル]ボタン処理
	 */
	function cancel() {
		dialog.dialog( "close" );
	}

	/**
	 * カレンダー設定
	 */
	function setDatepicker(date) {
		$("#dialog-form_users_update_hiredate").datepicker();
		$("#dialog-form_users_udpate_hiredate").datepicker("option", "showOn", 'both');
		$("#dialog-form_users_update_hiredate").datepicker("option", "buttonImageOnly", true);
		$("#dialog-form_users_update_hiredate").datepicker("option", "buttonImage", '../images/calendar.gif');
		if (date.indexOf("/") != -1) $("#dialog-form_users_update_hiredate").val(date);
	}

	/**
	 * 部署一覧取得
	 */
	function getDepartments(departmentid) {
		$("#dialog-form_users_update_department").empty();
		$.getJSON("query?action=get_departments", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var departments = json.datas;
				// 一覧作成
				$("#dialog-form_users_update_department").append($("<option>").val("-1").html("----"));
				for(var i=0; i<departments.length; i++) {
					$("#dialog-form_users_update_department").append($("<option>").val(departments[i].id).html(departments[i].name));
				}
				$("#dialog-form_users_update_department").val(departmentid);
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * 雇用形態一覧取得
	 */
	function getEmployments(employmentid) {
		$("#dialog-form_users_update_employment").empty();
		$.getJSON("query?action=get_employments", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var employments = json.datas;
				// 一覧作成
				$("#dialog-form_users_update_employment").append($("<option>").val("-1").html("----"));
				for(var i=0; i<employments.length; i++) {
					$("#dialog-form_users_update_employment").append($("<option>").val(employments[i].id).html(employments[i].name));
				}
				$("#dialog-form_users_update_employment").val(employmentid);
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * 勤務地一覧取得
	 */
	function getWorklocations(worklocationid) {
		$("#dialog-form_users_update_worklocation").empty();
		$.getJSON("query?action=get_worklocations", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var worklocations = json.datas;
				// 一覧作成
				$("#dialog-form_users_update_worklocation").append($("<option>").val("-1").html("----"));
				for(var i=0; i<worklocations.length; i++) {
					$("#dialog-form_users_update_worklocation").append($("<option>").val(worklocations[i].id).html(worklocations[i].name));
				}
				$("#dialog-form_users_update_worklocation").val(worklocationid);
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	/**
	 * グループ一覧取得
	 */
	function getGroups(groupid) {
		$("#dialog-form_users_update_group").empty();
		$.getJSON("query?action=get_groups", "", function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				var groups = json.datas;
				// 一覧作成
				$("#dialog-form_users_update_group").append($("<option>").val("-1").html("----"));
				for(var i=0; i<groups.length; i++) {
					$("#dialog-form_users_update_group").append($("<option>").val(groups[i].id).html(groups[i].name));
				}
				$("#dialog-form_users_update_group").val(groupid);
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}


	/**
	 * ログインID有効確認
	 */
	function checkLname(datas) {
		var change = datas.change;
		var lname = datas.lname;
		datas.functionCount = datas.functionCount + 1;	// カウントを進める
		var nextFunction = datas.functions[datas.functionCount];
		if(change.indexOf("lname") != -1) {
			showLoadingDialog("データを照合中...");	// 待機ダイアログ表示
			$.getJSON("query?action=query_users_lname", {lname:lname}, function(json, status) {
				if(isSessionTimeout(json)) return;	// セッション切れ
				if(json.result == "success") {
					closeLoadingDialog();	// 待機ダイアログ閉じる
					if(json.datas.length > 0) {
						return window.alert("このログインIDは使用できません。");
					}
					if(nextFunction != null) {
						nextFunction(datas);	// 次の関数を実行
					}
				} else {
					$("#update_complete_dialog").html(
							"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
					ajaxFinishFunction();
				}
			});
		} else {
			if(nextFunction != null) {
				nextFunction(datas);	// 次の関数を実行
			}
		}
	}

	/**
	 * 社員番号重複確認
	 */
	function checkCode(datas) {
		var change = datas.change;
		var oid = datas.rowData.oid;
		var code = datas.code;
		datas.functionCount = datas.functionCount + 1;	// カウントを進める
		var nextFunction = datas.functions[datas.functionCount];
		if(change.indexOf("code") != -1) {
			showLoadingDialog("データを照合中...");	// 待機ダイアログ表示
			// Ajax
			$.getJSON("query?action=query_users_code", {oid:oid, code:code}, function(json, status) {
				if(isSessionTimeout(json)) return;	// セッション切れ
				if(json.result == "success") {
					closeLoadingDialog();	// 待機ダイアログ閉じる
					if(json.datas.length > 0) {
						var agree =  window.confirm(
								"この社員番号は既に使用されています。\n同一の社員番号が存在することになりますが、本当によろしいですか？");
						if(!agree) {
							return;	// 同意しない場合はキャンセル
						}
					}
					if(nextFunction != null) {
						nextFunction(datas);	// 次の関数を実行
					}
				} else {
					$("#update_complete_dialog").html(
							"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
					ajaxFinishFunction();
				}
			});
		} else {
			if(nextFunction != null) {
				nextFunction(datas);	// 次の関数を実行
			}
		}
	}

	/**
	 * UPDATE処理
	 */
	function update() {
		// formダイアログ閉じる
		dialog.dialog( "close" );

		// 待機ダイアログ表示
		showLoadingDialog("データを更新中...");

		// Ajax通信
		$.ajax({
			type: "POST",
			url: "update?action=update_users",
			data: $( "#dialog-form_users_update_form" ).serialize()
		}).done(function (json, status, xhr) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 成功
			if(json.result == "success") {
				$("#update_complete_dialog").html("ユーザ情報を更新しました。");
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
			}
			updateRecord(rowData.uid);
			ajaxFinishFunction();
		}).fail(function (json, status, error) {
			// 失敗
			$("#update_complete_dialog").html(
					"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
			ajaxFinishFunction();
		});
	}

	/**
	 * Ajax通信成功時
	 */
	function ajaxFinishFunction() {
		closeLoadingDialog();	// 待機ダイアログ閉じる
		// 通信結果ダイアログ
		$("#update_complete_dialog").dialog({
			title: "通信結果",
			modal: true,
			buttons: {
				"OK": function() {
					//allClear();	// 全体更新
					$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
				}
			},
			maxWidth: 500,
			maxHeight: 500,
			resizable: false
		});
	}
}