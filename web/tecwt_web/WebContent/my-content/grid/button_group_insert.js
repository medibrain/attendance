/**
 * 追加ボタンの処理を定義。
 */

function groupInsertInit() {
	$("#insert_group_button").on("click", groupInsert);
}

/**
 * 追加メソッド
 */
function groupInsert() {

	// フォーム内容リセット
	$('#dialog-form_group_insert_form').find('input[type=\"text\"]').val("");
	$('#dialog-form_group_insert_form').find('textarea').val("");

	// formダイアログ
	var dialog = $( "#dialog-form_group_insert" ).dialog({
		autoOpen: false,
		maxHeight: $(window).height(),	// 表示領域の高さまで
		width: 500,
		minHeight: 300,
		modal: true,
		buttons: {
			"確定": function () {

				// 入力確認
				var name = $("#dialog-form_group_insert_name").val();
				if(name == "") return window.alert("名前が入力されていません。");
				var kana = $("#dialog-form_group_insert_kana").val();
				if(kana == "") return window.alert("カナが入力されていません。");
				var $note = $("#dialog-form_group_insert_note");
				$note.val(deleteCRLF($note.val()));	// 改行コード削除　util.js参照

				// 引数に渡す万能データ
				var datas = {
					functions:[insert],	// 処理の順番を設定
					functionCount:0,	// 現在処理中のインデックス番号を設定
				};

				// functionsの第一要素から順に実行していく
				datas.functions[datas.functionCount](datas);
			},
			"キャンセル": function () {
				dialog.dialog( "close" );
			}
		},
		close: function () {
			// 毎回初期化
		}
	});

	dialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();	// アクションをキャンセル
	});

	dialog.dialog( "open" );

	/**
	 * 登録処理
	 */
	function insert() {
		// formダイアログ閉じる
		dialog.dialog("close");

		// 待機ダイアログ表示
		showLoadingDialog("データを作成中...");

		// Ajax通信
		$.ajax({
			type: "POST",
			url: "update?action=insert_group",
			data: $( "#dialog-form_group_insert_form" ).serialize()
		}).done(function (json, status, xhr) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 成功
			if(json.result == "success") {
				$("#update_complete_dialog").html("グループを追加しました。");
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
			}
			ajaxFinishFunction();
		}).fail(function (json, status, error) {
			// 失敗
			$("#update_complete_dialog").html(
					"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
			ajaxFinishFunction();
		});
	}

	/**
	 * Ajax通信成功時
	 */
	function ajaxFinishFunction() {
		closeLoadingDialog();	// 待機ダイアログ閉じる
		// 通信結果ダイアログ
		$("#update_complete_dialog").dialog({
			title: "通信結果",
			modal: true,
			buttons: {
				"OK": function() {
					allClear();	// 全体更新
					$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
				}
			},
			maxWidth: 500,
			maxHeight: 500,
			resizable: false
		});
	}
}

