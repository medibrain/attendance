/**
 * CSVダウンロード	ボタンの処理を定義。
 */
$(function () {
	$("#csv_button").on("click", csvOutput);
});

/**
 * ユーザ追加メソッド
 */
function csvOutput() {

	// ファイル名は指定
	var date = "customer";
	var now = new Date();
	var y = now.getFullYear();
	if(y < 1900) y = y + 1900;	// 3桁で取得した場合用
	var m = now.getMonth() + 1;
	if(m < 10) m = "0" + m;
	var d = now.getDate();
	if(d < 10) d = "0" + d;
	var filename = date + y + m + d;

	// ダウンロードLinkのフォーム
	var $dlf = $("#download-form");				// ダウンロード用
	var $dlff = $("#download-form_dlfalse");	// HTML展開用

	// パラメータ
	var data1 = {
			type: 'hidden',
			name: 'showDatas',
			value: JSON.stringify(getShowRecords())
		};
	var data2 = {
			type: 'hidden',
			name: 'filename',
			value: filename
		};
	var data3 = {
			type: 'hidden',
			name: 'target',
			value: 'customer'
		};
	$("<input>", data1).appendTo($dlf);
	$("<input>", data1).appendTo($dlff);
	$("<input>", data2).appendTo($dlf);
	$("<input>", data2).appendTo($dlff);
	$("<input>", data3).appendTo($dlf);
	$("<input>", data3).appendTo($dlff);

	// ダウンロード用ダイアログ作成
	var dlDialog = $( "#dialog_download-link" ).dialog({
		autoOpen: false,
		maxHeight: $(window).height(),	// 表示領域の高さまで
		width: 500,
		minHeight: 300,
		modal: true,
		buttons: {
			"キャンセル": function () {
				dlDialog.dialog( "close" );
			}
		},
		close: function() {
			// リンク削除
			$("#download-form").empty();
			$("#download-form_dlfalse").empty();
		}
	});
	dlDialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();	// アクションをキャンセル
	});

	dlDialog.dialog( "open" );
}

