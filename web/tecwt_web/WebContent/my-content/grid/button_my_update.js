/**
 * マイユーザ情報変更ボタンの処理を定義。
 */

function myUpdateInit() {
	$("#update_my_button").on("click", myUpdate);
}

/**
 * マイユーザ情報変更メソッド
 */
function myUpdate() {

	// フォーム内容リセット
	$('#dialog-form_my_update_form').find('input[type=\"text\"]').val("");
	$('#dialog-form_my_update_form').find('input[type=\"password\"]').val("");

	// ↓↓ 値を入れていく ↓↓

	// name
	$("#dialog-form_my_update_name").val(myData.name);	// myDataはmy-history.js参照のグローバル変数

	// lname
	$("#dialog-form_my_update_lname").val(myData.lname);

	// pass
	$("#dialog-form_my_update_pass").val(myData.pass);

	// oid (hidden)
	$("#dialog-form_my_update_oid").val(myData.oid);

	// uid (hidden)
	$("#dialog-form_my_update_uid").val(myData.uid);

	// formダイアログ
	var dialog = $( "#dialog-form_my_update" ).dialog({
		autoOpen: false,
		maxHeight: $(window).height(),	// 表示領域の高さまで
		width: 500,
		minHeight: 300,
		modal: true,
		buttons: {
			"ユーザ情報を変更する": function () {

				// 入力確認
				var name = $("#dialog-form_my_update_name").val();
				var lname = $("#dialog-form_my_update_lname").val();
				var pass = $("#dialog-form_my_update_pass").val();

				// 変更箇所確認
				var change = "";
				if(name != "" && myData.name != name) change += "name,";
				if(lname != "" && myData.lname != lname) change += "lname,";
				if(pass != "" && myData.pass != pass) change += "pass,";
				var lname_confirm = $("#dialog-form_my_update_lname_confirm").val();
				var pass_confirm = $("#dialog-form_my_update_pass_confirm").val();
				if(change.indexOf("lname") != -1 && lname != lname_confirm) return window.alert("ログインIDが違います。ログインIDを再入力してください。");
				if(change.indexOf("pass") != -1 && pass != pass_confirm) return window.alert("ログインパスが違います。ログインパスを再入力してください。");

				// 未変更は許可しない
				if(change == "") {
					return window.alert("いずれも変更されていません。");
				}
				// 変更箇所一覧を追加
				$( "#dialog-form_my_update_change" ).val(change);

				// lnameが変更されている場合はユニーク確認
				var functions;
				if(change.indexOf("lname") != -1) {
					functions = [checkLname, update];
				} else {
					functions = [update];
				}

				// 引数に渡す万能データ
				var datas = {
					lname:lname,
					pass:pass,
					functions:functions,	// 処理の順番を設定
					functionCount:0,		// 現在処理中のインデックス番号を設定
				};

				// functionsの第一要素から順に実行していく
				datas.functions[datas.functionCount](datas);
			},
			"キャンセル": function () {
				dialog.dialog( "close" );
			}
		},
		close: function () {
			// 毎回初期化
		}
	});

	dialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();	// アクションをキャンセル
	});

	dialog.dialog( "open" );

	/**
	 * ログインID有効確認
	 */
	function checkLname(datas) {
		var lname = datas.lname;
		datas.functionCount = datas.functionCount + 1;	// カウントを進める
		var nextFunction = datas.functions[datas.functionCount];
		showLoadingDialog("データを照合中...");	// 待機ダイアログ表示
		$.getJSON("query?action=query_users_lname", {lname:lname}, function(json, status) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			if(json.result == "success") {
				closeLoadingDialog();	// 待機ダイアログ閉じる
				if(json.datas.length > 0) {
					return window.alert("このログインIDは使用できません。");
				}
				if(nextFunction != null) {
					nextFunction(datas);	// 次の関数を実行
				}
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
				ajaxFinishFunction();
			}
		});
	}

	function update() {
		// formダイアログ閉じる
		dialog.dialog("close");

		// 待機ダイアログ表示
		showLoadingDialog("データを作成中...");
		// Ajax通信
		$.ajax({
			type: "POST",
			url: "update?action=update_users",
			data: $( "#dialog-form_my_update_form" ).serialize()
		}).done(function (json, status, xhr) {
			if(isSessionTimeout(json)) return;	// セッション切れ
			// 成功
			if(json.result == "success") {
				$("#update_complete_dialog").html("ユーザ情報を変更しました。");
			} else {
				$("#update_complete_dialog").html(
						"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[success]<br>変更内容は破棄されます。再度操作を行って下さい。");
			}
			ajaxFinishFunction();
		}).fail(function (json, status, error) {
			// 失敗
			$("#update_complete_dialog").html(
					"<span style='color:#ff0000;'>通信エラーが発生しました。</span>[error]<br>変更内容は破棄されます。再度操作を行って下さい。");
			ajaxFinishFunction();
		});
	}

	/**
	 * Ajax通信成功時
	 */
	function ajaxFinishFunction() {
		closeLoadingDialog();	// 待機ダイアログ閉じる
		// 通信結果ダイアログ
		$("#update_complete_dialog").dialog({
			title: "通信結果",
			modal: true,
			buttons: {
				"OK": function() {
					allClear();	// 全体更新
					$("#update_complete_dialog").dialog('close');	// 通信結果ダイアログ閉じる
				}
			},
			maxWidth: 500,
			maxHeight: 500,
			resizable: false
		});
	}
}

