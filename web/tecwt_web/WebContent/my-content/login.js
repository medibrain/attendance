/**
 * ログインに関する処理を記述します。
 */


function loginInit() {
	// イベントハンドラ
	$("#login_button").on( "click", login);
}

/**
 * password入力後のエンターキーでサブミットする
 */
function doSubmit(code) {
	// enter
	if(code == 13) {
		login();
	}
}

/**
 * ログイン処理<br>
 * クリック自体はキャンセルしてAjaxでログインを行う
 */
function login() {

	var lname = $("#lname").val();
	var pass = $("#pass").val();

	if(lname == "") {
		$("#login-failed").text("ログインIDを入力して下さい。");
		return false;
	}
	if(pass == "") {
		$("#login-failed").text("ログインパスを入力して下さい。");
		return false;
	}

	showLoadingDialog("データを照合中です...");

	// パラメータ作成
	var save = "&save=";
	if($("#save").is(":checked")) {
		save = save + "true";
	} else {
		save = save + "false";
	}
	var params = "lname="+$("#lname").val() + "&pass="+$("#pass").val() + save;

	// データ照合
	$.ajax({
		type: "POST",
		url: "login",
		data: params
	}).done(function (json, status, xhr) {
		// メッセージ欄に記載
		if(json.result == "error") {
			$("#login-failed").text("データの照合中にエラーが発生しました。再度操作を行って下さい。");
		}
		else if(json.login == "done") {
			if(json.datas[0].admin) {
				// トップページへ遷移
				$('<form/>', {action: "top", method: 'post'})
				.append($('<input/>', {type: 'hidden', name: 'error_message'}))
				.appendTo(document.body)
				.submit();
			} else {
				// マイページへ遷移
				$('<form/>', {action: "history?action=my", method: 'post'})
				.append($('<input/>', {type: 'hidden', name: 'error_message'}))
				.appendTo(document.body)
				.submit();
			}
		}
		else {
			$("#login-failed").text("ログインIDまたはログインパスが違います。");
		}
		closeLoadingDialog();
	}).fail(function (json, status, error) {
		$("#login-failed").text("データの照合中にエラーが発生しました。再度操作を行って下さい。");
		closeLoadingDialog();
	});

	return false;
}