<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>タイムアウト</title>
<!-- ファビコン -->
<link rel="shortcut icon" href="../../my-content/images/meditechno_logo.jpg">
<!-- キャッシュ無効 -->
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<!-- CSS -->
<link rel="stylesheet" href="../../css/ui-lightness/jquery-ui-1.10.3.custom.css"/>
<link rel="stylesheet" href="../../my-content/header/header.css"/>
<link rel="stylesheet" href="../../my-content/template/template.css"/>
</head>
<body>
<div class="main">
	<div class="main-logo">
		<img src="../../my-content/images/header_image.png" style="width:100%;">
	</div>
	<div class="main-dot">
		<div class="main-form-base">
			<div class="message">
セッションの有効期限が切れています。<br>
お手数ですが再度ログインを行って下さい。
			</div>
			<a href="../login">ログインページへ  >></a>
		</div>
	</div>
</div>
</body>
</html>