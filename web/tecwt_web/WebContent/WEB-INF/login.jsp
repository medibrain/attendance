<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>勤怠管理-tecwt.com ログインページ</title>
<!-- ファビコン -->
<link rel="shortcut icon" href="../my-content/images/meditechno_logo.jpg">
<!-- キャッシュ無効 -->
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<!-- CSS -->
<link rel="stylesheet" href="../css/ui-lightness/jquery-ui-1.10.3.custom.css"/>
<link rel="stylesheet" href="../my-content/header/header.css"/>
<link rel="stylesheet" href="../my-content/template/template.css"/>
</head>
<body>
<div class="main">
	<div class="main-logo">
		<img src="../my-content/images/header_image.png" style="width:100%;">
	</div>
	<div class="main-dot">
		<div class="main-form-base">
			<div class="login-form">
				<div class="login-failed" id="login-failed"></div>
				<div class="loginFormItem">
					<label class="loginFormItem-label" for="lname">ログインID</label><br>
					<input class="loginFormItem-input" type="text" id="lname">
				</div>
				<div class="loginFormItem">
					<label class="loginFormItem-label" for="pass">パスワード</label><br>
					<input class="loginFormItem-input" type="password" id="pass" onkeypress="doSubmit(event.keyCode);">
				</div>
			</div>
			<div class="login-button">
				<label class="loginFormItem-save"><input type="checkbox" id="save">ログイン情報を記憶</label>
				<input class="loginFormItem-login" type="button" id="login_button" value="ログイン">
			</div>
		</div>
	</div>
	<div class="orgname"><b></b>　様</div>
</div>

<!-- DIALOG -->
<div id="loading_dialog"></div>
<div id="update_complete_dialog"></div>

<!-- JAVASCRIPT -->
<script src="../js/jquery-1.10.2.js"></script>
<script src="../js/jquery-ui-1.10.3.custom.js"></script>

<script src="../my-content/util.js"></script>
<script src="../my-content/login.js"></script>

<script type="text/javascript">
$(function () {
	var name = "<%= request.getAttribute("orgname") %>";
	$(".orgname b").html(name);
	var org = "<%= request.getAttribute("organization") %>";
	loginInit(org);	// login.js参照
});
</script>
</body>
</html>