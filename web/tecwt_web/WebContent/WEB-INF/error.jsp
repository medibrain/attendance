<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>エラー</title>
<!-- ファビコン -->
<link rel="shortcut icon" href="../../my-content/images/meditechno_logo.jpg">
<!-- キャッシュ無効 -->
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<!-- CSS -->
<link rel="stylesheet" href="../../css/ui-lightness/jquery-ui-1.10.3.custom.css"/>
<link rel="stylesheet" href="../../my-content/header/header.css"/>
<link rel="stylesheet" href="../../my-content/template/template.css"/>
</head>
<body>
<div class="main">
	<div class="main-logo">
		<img src="../../my-content/images/header_image.png" style="width:100%;">
	</div>
	<div class="main-dot">
		<div class="main-form-base">
			<div class="message">
処理中にエラーが発生したため中断しました。<br>
お手数ですが再度操作を行って下さい。<br>
解決しない場合は少し時間を空けてから再度接続し直して下さい。
			</div>
			<div class="error-contents">
				<b>エラー内容：</b><br>
				<br>
<% request.setCharacterEncoding("UTF-8"); %>
<% String message = request.getParameter("message"); %>
<% boolean isTypeOrg = false; %>
<% if(message != null && message.startsWith("type:org")) {
	isTypeOrg = true;
	message = "指定した組織が登録されていないか、もしくは組織情報が間違っています。URLを確認して下さい。<br>"
			+ "また別のブラウザやタグからもログインしている場合は、一度ログアウトを行ってから再度接続し直して下さい。";
}%>
<%= message %>
			</div>
			<% if(isTypeOrg) { %>
				<a href="../logout">ログインページへ  >></a>
			<% } else {%>
				<a href="../top">トップページへ  >></a>
			<% } %>
		</div>
	</div>
</div>
</body>
</html>