<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>トップページ</title>
<!-- ファビコン -->
<link rel="shortcut icon" href="../my-content/images/meditechno_logo.jpg">
<!-- キャッシュ無効 -->
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<!-- CSS -->
<link rel="stylesheet" href="../css/ui-lightness/jquery-ui-1.10.3.custom.css"/>
<link rel="stylesheet" href="../my-content/header/header.css"/>
<link rel="stylesheet" href="../my-content/template/template.css"/>
</head>
<body>
<!-- HEADER -->
<header>
<div class="headerBox">
	<div id="headerBox_image">
		<img src="../my-content/images/header_image.png" height=80>
	</div>
	<div class="headerBox_datetime" id="headerBox_datetime"></div>
	<a id="logout" href="logout">ログアウト</a>
</div>
<hr size="2" color="#888888" style="width:100%;margin:0;">
</header>
<!-- MAIN -->
<div class="topMain">
	<div class="topMenu">
		<table class="topMenuTable">
			<tr>
				<td><input class="menuInput_Timecard" type="button" onclick="location.href='history?action=business'" value="勤怠表"></td>
				<td class="tdWorkin" rowspan="11"><jsp:include page="workin.jsp"></jsp:include></td>
			</tr>
			<tr>
				<td><input class="menuInput_Users" type="button" onclick="location.href='department'" value="社員：部署"></td>
			</tr>
			<tr>
				<td><input class="menuInput_Users" type="button" onclick="location.href='worklocation'" value="社員：勤務地"></td>
			</tr>
			<tr>
				<td><input class="menuInput_Users" type="button" onclick="location.href='users'" value="社員：ユーザ"></td>
			</tr>
			<tr>
				<td><input class="menuInput_Users" type="button" onclick="location.href='employment'" value="社員：雇用形態"></td>
			</tr>
			<tr>
				<td><input class="menuInput_Users" type="button" onclick="location.href='group'" value="社員：グループ"></td>
			</tr>
			<tr>
				<td><input class="menuInput_Worktype" type="button" onclick="location.href='customer'" value="勤務種別：客先"></td>
			</tr>
			<tr>
				<td><input class="menuInput_Worktype" type="button" onclick="location.href='category'" value="勤務種別：カテゴリー"></td>
			</tr>
			<tr>
				<td><input class="menuInput_Worktype" type="button" onclick="location.href='worktype'" value="勤務種別：種別"></td>
			</tr>
			<tr>
				<td><input class="menuInput_Unit" type="button" onclick="location.href='unit'" value="端末・アプリ"></td>
			</tr>
			<tr>
				<td><input class="menuInput_Timecard" type="button" onclick="location.href='history?action=my'" value="マイページ"></td>
			</tr>
		</table>
	</div>
</div>

<!-- JAVASCRIPT -->
<script src="../js/jquery-1.10.2.js"></script>
<script src="../js/jquery-ui-1.10.3.custom.js"></script>

<script src="../my-content/util.js"></script>
<script src="../my-content/header/header-top.js"></script>
<script src="../my-content/top.js"></script>

<script type="text/javascript">
$(function () {
	$("html").css("min-height", "100%");
	$("body").css("min-height", "100%");
	topInit();
});
</script>
</body>
</html>