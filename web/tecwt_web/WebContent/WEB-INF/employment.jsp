<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員：雇用形態</title>
<!-- ファビコン -->
<link rel="shortcut icon" href="../my-content/images/meditechno_logo.jpg">
<!-- キャッシュ無効 -->
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<!-- JAVASCRIPT -->
<script src="../js/jquery-1.10.2.js"></script>
<script src="../js/jquery-ui-1.10.3.custom.js"></script>
<script src="../js/jquery.event.drag-2.2.js"></script>
<script src="../js/jquery.ui.datepicker-ja.js"></script>
<!-- CSS -->
<link rel="stylesheet" href="../js/slick.grid.css"/>
<link rel="stylesheet" href="../js/slick.pager.css" type="text/css"/>
<link rel="stylesheet" href="../css/ui-lightness/jquery-ui-1.10.3.custom.css"/>
<link rel="stylesheet" href="../my-content/header/header.css"/>
<link rel="stylesheet" href="../my-content/template/template.css"/>
</head>
<body>
<!-- HEADER -->
<jsp:include page="../WEB-INF/include/header.jsp"></jsp:include>
<!-- CONTENTS -->
<div class="CONTENTS">
	<div class="clearfix">
	<div class="MENU">
		<!-- 検索メニュー -->
		<div class="queryMenu">
			<jsp:include page="include/query_header.jsp"></jsp:include>
			<div id="queryMenu" class="menuFrame">
				<!-- 全件表示 & 本日 -->
				<jsp:include page="include/query_all-or-today.jsp"></jsp:include>
				<!-- 雇用形態に関する管理状況検索 -->
				<jsp:include page="include/query_enable.jsp"></jsp:include>
			</div>
		</div>
		<!-- 雇用形態登録 -->
		<div class="insertMenu">
			<jsp:include page="include/insert_header.jsp"></jsp:include>
			<div id="insertMenu" class="menuFrame">
				<!-- 雇用形態を追加 -->
				<jsp:include page="include/insert_employment.jsp"></jsp:include>
			</div>
		</div>
		<!-- CSV出力メニュー -->
		<div class="csvMenu">
			<jsp:include page="include/csv_header.jsp"></jsp:include>
			<div id="csvMenu" class="menuFrame">
				<!-- CSV出力 -->
				<jsp:include page="include/csv.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<div class="GRID">
		<div class="grid-base" id="grid-base">
		<!-- ページャー -->
		<div class="pager" id="timecard_pager"></div>
		<!-- グリッド -->
		<div class="grid" id="grid"></div>
		</div>
		<br>
		<div class="etc">
			<b>※使い方ガイド※</b><br>
			<ol>
			<li><div><u>雇用形態の検索</u></div>　　画面左側の[検索メニュー]メニュー > 各種[検索]ボタンで検索できます。 </li>
			<li><div><u>雇用形態の登録</u></div>　　画面左側の[新規登録]メニュー > [作成]</li>
			<li><div><u>雇用形態の編集</u></div>　　表右側の[編集・管理] > 部署の情報を変更できます。</li>
			</ol>
		</div>
	</div>
	</div>
</div>

<!-- DIALOG -->
<div id="update_confirm_dialog"></div>
<div id="loading_dialog"></div>
<div id="update_complete_dialog"></div>
<jsp:include page="include/dialog-form_employment_insert.jsp"></jsp:include>
<jsp:include page="include/dialog-form_employment_update.jsp"></jsp:include>
<jsp:include page="include/dialog_download-link.jsp"></jsp:include>

<!-- JAVASCRIPT -->
<script src="../js/slick.core.js"></script>
<script src="../js/slick.grid.js"></script>
<script src="../js/slick.dataview.js"></script>
<script src="../js/slick.formatters.js"></script>
<script src="../js/slick.pager.js"></script>
<script src="../js/plugins/slick.rowselectionmodel.js"></script>
<script src="../js/plugins/slick.checkboxselectcolumn.js"></script>

<script src="../my-content/header/header.js"></script>
<script src="../my-content/template/menu.js"></script>
<script src="../my-content/grid/employment.js"></script>
<script src="../my-content/util.js"></script>
<script src="../my-content/jquery.japanese-input-change.js"></script>
<script src="../my-content/grid/button_employment_insert.js"></script>
<script src="../my-content/grid/button_employment_update.js"></script>
<script src="../my-content/grid/button_employment_csv.js"></script>

<script type="text/javascript">
$(function () {
	headerInit("employment");	// header.js参照
	employmentInit();	// my-content#grid#employment.js参照
	gridWidthToFit();
	employmentMENUInit();	// my-content#template#menu.js参照
});
</script>
</body>
</html>