<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザ</title>
<!-- ファビコン -->
<link rel="shortcut icon" href="../my-content/images/meditechno_logo.jpg">
<!-- キャッシュ無効 -->
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<!-- JAVASCRIPT -->
<script src="../js/jquery-1.10.2.js"></script>
<script src="../js/jquery-ui-1.10.3.custom.js"></script>
<script src="../js/jquery.event.drag-2.2.js"></script>
<script src="../js/jquery.ui.datepicker-ja.js"></script>
<!-- CSS -->
<link rel="stylesheet" href="../js/slick.grid.css"/>
<link rel="stylesheet" href="../js/slick.pager.css" type="text/css"/>
<link rel="stylesheet" href="../css/ui-lightness/jquery-ui-1.10.3.custom.css"/>
<link rel="stylesheet" href="../my-content/header/header.css"/>
<link rel="stylesheet" href="../my-content/template/template.css"/>
</head>
<body>
<!-- HEADER -->
<jsp:include page="../WEB-INF/include/header.jsp"></jsp:include>
<!-- CONTENTS -->
<div class="CONTENTS">
	<div class="clearfix">
	<div class="MENU">
		<!-- 検索メニュー -->
		<div class="queryMenu">
			<jsp:include page="include/query_header.jsp"></jsp:include>
			<div id="queryMenu" class="menuFrame">
				<!-- 全件表示 & 入力リセット -->
				<jsp:include page="include/query_all-or-today.jsp"></jsp:include>
				<!-- 日付範囲検索 -->
				<jsp:include page="include/query_date-range.jsp"></jsp:include>
				<!-- ユーザに対する管理状況検索 -->
				<jsp:include page="include/query_enable.jsp"></jsp:include>
				<!-- ユーザに対するメール打刻許可検索 -->
				<jsp:include page="include/query_mailenable.jsp"></jsp:include>
			</div>
		</div>
		<!-- ユーザ登録 -->
		<div class="insertMenu">
			<jsp:include page="include/insert_header.jsp"></jsp:include>
			<div id="insertMenu" class="menuFrame">
				<!-- ユーザを追加 -->
				<jsp:include page="include/insert_user.jsp"></jsp:include>
			</div>
		</div>
		<!-- CSV出力メニュー -->
		<div class="csvMenu">
			<jsp:include page="include/csv_header.jsp"></jsp:include>
			<div id="csvMenu" class="menuFrame">
				<!-- CSV出力 -->
				<jsp:include page="include/csv.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<div class="GRID">
		<div class="grid-base" id="grid-base">
		<!-- ページャー -->
		<div class="pager" id="timecard_pager"></div>
		<!-- グリッド -->
		<div class="grid" id="grid"></div>
		</div>
		<br>
		<div class="etc">
			<b>※使い方ガイド※</b><br>
			<ol>
			<li><div><u>ユーザの検索</u></div>　　画面左側の[検索メニュー]メニュー > 各種[検索]ボタンで検索できます。 </li>
			<li><div><u>ユーザの追加</u></div>　　画面左側の[新規登録]メニュー > [ユーザを追加]</li>
			<li><div><u>ユーザ情報の編集</u></div>　　表右側の[編集・管理] > そのユーザの情報を変更できます。</li>
			<li><div><u>社員カード(ICカード)の登録・編集</u></div>　　表右側の[社員カード] > 社員カード登録準備、および社員カードの情報の閲覧・変更が行えます。<br>
			※ 本サイトからICカード情報を直接登録・変更することはできません。<br>
			※ 専用アプリがインストールされたタブレット等を用いて社員カード(ICカード)を登録して下さい。<br>
			※ 一人のユーザに対し複数枚の社員カードを登録することは可能です。</li>
			</ol>
		</div>
	</div>
	</div>
</div>

<!-- DIALOG -->
<div id="update_confirm_dialog"></div>
<div id="loading_dialog"></div>
<div id="update_complete_dialog"></div>
<jsp:include page="include/dialog-form_users_insert.jsp"></jsp:include>
<jsp:include page="include/dialog-form_users_update.jsp"></jsp:include>
<jsp:include page="include/dialog-form_idm.jsp"></jsp:include>
<jsp:include page="include/dialog_download-link.jsp"></jsp:include>

<!-- JAVASCRIPT -->
<script src="../js/slick.core.js"></script>
<script src="../js/slick.grid.js"></script>
<script src="../js/slick.dataview.js"></script>
<script src="../js/slick.formatters.js"></script>
<script src="../js/slick.pager.js"></script>
<script src="../js/plugins/slick.rowselectionmodel.js"></script>
<script src="../js/plugins/slick.checkboxselectcolumn.js"></script>

<script src="../my-content/header/header.js"></script>
<script src="../my-content/template/menu.js"></script>
<script src="../my-content/grid/users.js"></script>
<script src="../my-content/util.js"></script>
<script src="../my-content/jquery.japanese-input-change.js"></script>
<script src="../my-content/grid/button_users_insert.js"></script>
<script src="../my-content/grid/button_users_update.js"></script>
<script src="../my-content/grid/button_idm_update.js"></script>
<script src="../my-content/grid/button_users_csv.js"></script>

<script type="text/javascript">
$(function () {
	headerInit("users");	// header.js参照
	usersInit();	// users.js参照
	gridWidthToFit();	// util.js参照
	usersMENUInit();	// menu.js参照
});
</script>
</body>
</html>