<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>マイページ・過去勤怠</title>
<!-- ファビコン -->
<link rel="shortcut icon" href="../my-content/images/meditechno_logo.jpg">
<!-- キャッシュ無効 -->
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<!-- JAVASCRIPT -->
<script src="../js/jquery-1.10.2.js"></script>
<script src="../js/jquery-ui-1.10.3.custom.js"></script>
<script src="../js/jquery.event.drag-2.2.js"></script>
<script src="../js/jquery.ui.datepicker-ja.js"></script>
<!-- CSS -->
<link rel="stylesheet" href="../js/slick.grid.css"/>
<link rel="stylesheet" href="../js/slick.pager.css" type="text/css"/>
<link rel="stylesheet" href="../css/ui-lightness/jquery-ui-1.10.3.custom.css"/>
<link rel="stylesheet" href="../my-content/header/header.css"/>
<link rel="stylesheet" href="../my-content/template/template.css"/>
</head>
<body>
<!-- HEADER -->
<jsp:include page="../WEB-INF/include/header.jsp"></jsp:include>
<!-- CONTENTS -->
<div class="CONTENTS">
	<div class="clearfix">
	<div class="MENU">
		<!-- 検索メニュー -->
		<div class="queryMenu">
			<jsp:include page="include/query_header.jsp"></jsp:include>
			<div id="queryMenu" class="menuFrame">
				<!-- 全件表示 & 入力リセット -->
				<jsp:include page="include/query_all-or-today.jsp"></jsp:include>
				<!-- 年・月検索 -->
				<jsp:include page="include/query_year-month.jsp"></jsp:include>
				<!-- 日付範囲検索 -->
				<jsp:include page="include/query_date-range.jsp"></jsp:include>
			</div>
		</div>
		<!-- ユーザ情報 -->
		<div class="myMenu">
			<jsp:include page="include/update_header_my.jsp"></jsp:include>
			<div id="myMenu" class="menuFrame">
				<!-- ユーザ情報を変更する -->
				<jsp:include page="include/update_my.jsp"></jsp:include>
			</div>
		</div>
	</div>
	<div class="GRID">
		<div class="grid-base" id="grid-base">
		<!-- ページャー -->
		<div class="pager" id="timecard_pager"></div>
		<!-- グリッド -->
		<div class="grid" id="grid"></div>
		</div>
		<!-- ページャー説明 -->
		<div>※100行を超える場合は自動的にページ分けされます。<a id="pager-top-scroll" href="#wrap">(表上部参照)</a></div>
		<br>
		<br>
		<div class="etc">
			<b>※使い方ガイド※</b><br>
			<ol>
			<li><div><u>タイムカードの検索</u></div>　　画面左側の[検索メニュー]メニュー > 各種[検索]ボタンで検索できます。 </li>
			<li><div><u>タイムカードの絞込</u></div>　　画面左側の[検索メニュー]メニュー > 各種[絞込]ボタンで表示列から絞込みを行います。 </li>
			<li><div><u>ユーザ情報の変更</u></div>　　画面左側の[ユーザ情報]メニュー > [ユーザ情報を変更する] </li>
			<li><div><u>CSVファイルのダウンロード</u></div>　　画面左側の[CSV]メニュー > [ダウンロード] </li>
			<li><div><u>ログの詳細確認</u></div>　　表右側の[ログ] > そのタイムカードに関する詳細情報を閲覧できます。</li>
			</ol>
		</div>
	</div>
	</div>
</div>

<!-- DIALOG -->
<div id="update_confirm_dialog"></div>
<div id="loading_dialog"></div>
<div id="update_complete_dialog"></div>
<jsp:include page="include/dialog-form_my_update.jsp"></jsp:include>
<jsp:include page="include/dialog-div_tclog_viewer.jsp"></jsp:include>
<jsp:include page="include/dialog-form_timecard_csv.jsp"></jsp:include>
<jsp:include page="include/dialog_download-link.jsp"></jsp:include>

<!-- JAVASCRIPT -->
<script src="../js/slick.core.js"></script>
<script src="../js/slick.grid.js"></script>
<script src="../js/slick.dataview.js"></script>
<script src="../js/slick.formatters.js"></script>
<script src="../js/slick.pager.js"></script>
<script src="../js/plugins/slick.rowselectionmodel.js"></script>
<script src="../js/plugins/slick.checkboxselectcolumn.js"></script>
<script src="../js/encoding.js"></script>

<script src="../my-content/jquery.japanese-input-change.js"></script>
<script src="../my-content/header/header.js"></script>
<script src="../my-content/template/menu.js"></script>
<script src="../my-content/grid/my-history.js"></script>
<script src="../my-content/grid/util_history.js"></script>
<script src="../my-content/util.js"></script>
<script src="../my-content/grid/button_my_update.js"></script>
<script src="../my-content/grid/button_tclog_viewer.js"></script>
<script src="../my-content/grid/button_timecard_csv.js"></script>

<script type="text/javascript">
$(function () {
	headerInit("my");	// header.js参照
	var uid = <%= request.getAttribute("uid") %>;
	historyInit(uid);	// my-history.js参照
	gridWidthToFit();	// util.js参照
	myhistoryMENUInit();	// menu.js参照
});
</script>
</body>
</html>