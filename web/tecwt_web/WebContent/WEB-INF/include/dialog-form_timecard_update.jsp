<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-form_timecard_update" title="タイムカードの編集・削除">
	<p class="validateTips">編集したい部分を正しく入力してください。</p>
	<form class="dialog-form_form" id="dialog-form_timecard_update_form">
		<fieldset>

		<input type="hidden" id="dialog-form_timecard_update_default_ismove" name="default_ismove">
		<input type="hidden" id="dialog-form_timecard_update_oid" name="oid">
		<input type="hidden" id="dialog-form_timecard_update_default_uid" name="default_uid">
		<input type="hidden" id="dialog-form_timecard_update_uname" name="uname">
		<input type="hidden" id="dialog-form_timecard_update_default_tdate" name="default_tdate">
		<input type="hidden" id="dialog-form_timecard_update_times" name="times">
		<input type="hidden" id="dialog-form_timecard_update_wname" name="wname">
		<input type="hidden" id="dialog-form_timecard_update_wname_move" name="wname_move">
		<input type="hidden" id="dialog-form_timecard_update_change" name="change">

		<!-- <input type="checkbox" id="dialog-form_timecard_update_ismove" name="ismove" value="on">移動を挟む<br> -->
		<ul class="dialog_ul">
			<li>
			<table>
				<tr>
					<td colSpan="5">
						<label for="dialog-form_timecard_update_uid">●　名前</label>
						<select name="uid" id="dialog-form_timecard_update_uid"></select>
					</td>
				</tr>
				<tr>
					<td class="dialog-form_form_search_box"></td>
					<td><input id="dialog-form_timecard_update_search_users_text" type="text" style="width:100px;" placeholder="文字を入力後→"></td>
					<td><input id="dialog-form_timecard_update_search_users_button" type="button" value="候補絞込"></td>
					<td><input id="dialog-form_timecard_update_search_users_button_reset" type="button" value="候補のクリア"></td>
					<td><span id="dialog-form_timecard_update_search_users_message" style="color:red"></span></td>
				</tr>
			</table>
			</li>
			<li>
			<label for="dialog-form_timecard_update_tdate">●　日付</label>
			<input class="dateText" type="text" name="tdate" id="dialog-form_timecard_update_tdate" readonly="readonly" style="width:75px;">
			</li>
			<li>
			<label for="dialog-form_timecard_update_intime">●　出勤</label>
			<label id="dialog-form_timecard_update_intime">
				<select class="timeBox" id="dialog-form_timecard_update_intime_h" name="intime_h"></select>:
				<select class="timeBox" id="dialog-form_timecard_update_intime_m" name="intime_m"></select>
			</label>
			</li>
			<li>
			<label for="dialog-form_timecard_update_movetime">●　移動</label>
			<label id="dialog-form_timecard_update_movetime">
				<select class="timeBox" id="dialog-form_timecard_update_movetime_h" name="movetime_h"></select>:
				<select class="timeBox" id="dialog-form_timecard_update_movetime_m" name="movetime_m"></select>
			</label>
			</li>
			<li>
			<label for="dialog-form_timecard_update_outtime">●　退勤</label>
			<label id="dialog-form_timecard_update_outtime">
				<select class="timeBox" id="dialog-form_timecard_update_outtime_h" name="outtime_h"></select>:
				<select class="timeBox" id="dialog-form_timecard_update_outtime_m" name="outtime_m"></select>
			</label>
			</li>
			<li>
			<table>
				<tr>
					<td colSpan="5">
						<label for="dialog-form_timecard_update_wid">●　勤務種別</label>
						<select name="wid" id="dialog-form_timecard_update_wid"></select>
					</td>
					<td>
						<label>
						<input id="dialog-form_timecard_update_search_worktype_enable" type="checkbox" style="margin-left:5px;" checked>
						凍結を除く</label>
					</td>
				</tr>
				<tr>
					<td class="dialog-form_form_search_box"></td>
					<td><input id="dialog-form_timecard_update_search_worktype_text" type="text" style="width:100px;" placeholder="文字を入力後→"></td>
					<td><input id="dialog-form_timecard_update_search_worktype_button" type="button" value="候補絞込"></td>
					<td><input id="dialog-form_timecard_update_search_worktype_button_reset" type="button" value="候補のクリア"></td>
					<td><span id="dialog-form_timecard_update_search_worktype_message" style="color:red"></span></td>
				</tr>
				<tr>
					<td></td>
					<td><input id="dialog-form_timecard_update_temporary" type="button" value="仮入力"></td>
				</tr>
			</table>
			</li>
			<li>
			<table>
				<tr><td colSpan="2"><label for="dialog-form_timecard_update_note">●　業務内容(50字以内)</label></td></tr>
				<tr>
					<td class="dialog-form_form_search_box"></td>
					<td><textarea name="note" id="dialog-form_timecard_update_note" rows="3" cols="40" placeholder="空白"></textarea></td>
				</tr>
			</table>
			</li>
			<label for="dialog-form_timecard_update_breaktime">●　休憩時間</label>
			<input type="text" name="breaktime" id="dialog-form_timecard_update_breaktime" style="width:40px;">
			</li>
			<li>
			<label for="dialog-form_timecard_update_fare">●　交通費</label>
			<input type="text" name="fare" id="dialog-form_timecard_update_fare" style="width:55px;">
			</li>
			<li>
			<label for="dialog-form_timecard_update_wcount">●　処理数</label>
			<input type="text" name="wcount" id="dialog-form_timecard_update_wcount" style="width:75px;">
			</li>
			<li>
			<label for="dialog-form_timecard_update_rcount">●　申出数</label>
			<input type="text" name="rcount" id="dialog-form_timecard_update_rcount" style="width:75px;">
			</li>
			<li>
			<li>
			<div class="dialog-form_sepalator">
				<label><b>以下　移動後の内容</b></label><hr size="2" width="100%" color="#000000" align="right">
			</div>
			</li>
			<li>
			<table>
				<tr>
					<td colSpan="5">
						<label for="dialog-form_timecard_update_wid_move">●　(移)勤務種別</label>
						<select name="wid_move" id="dialog-form_timecard_update_wid_move"></select>
					</td>
					<td>
						<label>
						<input id="dialog-form_timecard_update_search_worktype_move_enable" type="checkbox" style="margin-left:5px;">
						凍結を除く</label>
					</td>
				</tr>
				<tr>
					<td class="dialog-form_form_search_box"></td>
					<td><input id="dialog-form_timecard_update_search_worktype_move_text" type="text" style="width:100px;" placeholder="文字を入力後→"></td>
					<td><input id="dialog-form_timecard_update_search_worktype_move_button" type="button" value="候補絞込"></td>
					<td><input id="dialog-form_timecard_update_search_worktype_move_button_reset" type="button" value="候補のクリア"></td>
					<td><span id="dialog-form_timecard_update_search_worktype_move_message" style="color:red"></span></td>
				</tr>
			</table>
			</li>
			<li>
			<table>
				<tr><td colSpan="2"><label for="dialog-form_timecard_update_note_move">●　(移)業務内容(50字以内)</label></td></tr>
				<tr>
					<td class="dialog-form_form_search_box"></td>
					<td><textarea name="note_move" id="dialog-form_timecard_update_note_move" rows="3" cols="40" placeholder="空白"></textarea></td>
				</tr>
			</table>
			</li>
			<li>
			<label for="dialog-form_timecard_update_wcount_move">●　(移)処理数</label>
			<input type="text" name="wcount_move" id="dialog-form_timecard_update_wcount_move" style="width:75px;">
			</li>
			<li>
			<label for="dialog-form_timecard_update_rcount_move">●　(移)申出数</label>
			<input type="text" name="rcount_move" id="dialog-form_timecard_update_rcount_move" style="width:75px;">
			</li>
		</ul>
		</fieldset>
	</form>
</div>