<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-form_unit_insert" title="端末受付枠の追加">
	<p class="validateTips">専用アプリをインストールした端末で、アプリの初回利用時に端末を認識するためのパスワードを決めて下さい。(重複可)</p>
	<form class="dialog-form_form" id="dialog-form_unit_insert_form">
		<fieldset>
		<ul class="dialog_ul">
			<li>
			<label for="dialog-form_unit_insert_pass">パス</label>
			<input class="inputBox" type="text" id="dialog-form_unit_insert_pass" name="pass">
			</li>
		</ul>
		</fieldset>
	</form>
</div>