<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-form_department_insert" title="部署の新規追加">
	<p class="validateTips">新しく追加する部署の情報を入力してください</p>
	<form class="dialog-form_form" id="dialog-form_department_insert_form">
		<fieldset>
		<ul class="dialog_ul">
			<li>
				<label for="dialog-form_department_insert_name">部署名</label>
				<input class="inputBox" type="text" id="dialog-form_department_insert_name" name="name">
			</li>
			<li>
				<label for="dialog-form_department_insert_kana">カナ</label>
				<input class="inputBox" type="text" id="dialog-form_department_insert_kana" name="kana">
			</li>
			<li>
				<table>
					<tr>
						<td colSpan="5">
							<label for="dialog-form_department_insert_manager">責任者</label>
							<select id="dialog-form_department_insert_manager" name="manager"></select>
						</td>
					</tr>
					<tr>
						<td class="dialog-form_form_search_box"></td>
						<td><input id="dialog-form_department_insert_search_users_text" type="text" style="width:100px;" placeholder="文字を入力後→"></td>
						<td><input id="dialog-form_department_insert_search_users_button" type="button" value="候補絞込"></td>
						<td><input id="dialog-form_department_insert_search_users_button_reset" type="button" value="候補のクリア"></td>
						<td><span id="dialog-form_department_insert_search_users_message" style="color:red"></span></td>
					</tr>
				</table>
			</li>
			<li>
				<label for="dialog-form_department_insert_note">メモ</label><br>
				<textarea name="note" id="dialog-form_department_insert_note" rows="3" cols="40"></textarea>
			</li>
		</ul>
		</fieldset>
	</form>
</div>