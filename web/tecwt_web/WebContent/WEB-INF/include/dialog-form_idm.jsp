<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-form_idm" title="社員カード(IDM)の確認・削除">
	<p id="name" class="validateTips" style="text-decoration: underline;"></p>
	<p class="validateTips">削除したい社員カード(IDM)を選択して下さい。(複数可)</p>
	<form class="dialog-form_form" id="dialog-form_idm_form">
		<fieldset>
		<div class="idm-checkboxs-div" id="idm-checkboxs-div"></div>
		<input type="hidden" id="dialog-form_idm_oid" name="oid">
		<input type="hidden" id="dialog-form_idm_uid" name="uid">
		<input type="hidden" id="dialog-form_idm_delete-idm" name="delete-idm">
		</fieldset>
	</form>
</div>