<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-form_timecard_insert" title="タイムカードの作成">
	<div class="dialog-form_memo">
		<p class="validateTips">作成可能なパターンは以下の通りです。<br>項目に従って入力して下さい。</p>
		<ul>
			<li style="padding-bottom: 3px;"><label style="text-decoration:underline;">出勤のみ</label><br>
名前、日付、出勤</li>
			<li style="padding-bottom: 3px;"><label style="text-decoration:underline;">出勤と退勤</label><br>
名前、日付、出勤、退勤、勤務種別、業務内容、処理数、申出数、休憩時間、交通費</li>
			<li style="padding-bottom: 3px;"><label style="text-decoration:underline;">出勤と移動</label><br>
名前、日付、出勤、移動、勤務種別、業務内容、処理数、申出数</li>
			<li style="padding-bottom: 3px;"><label style="text-decoration:underline;">出勤と移動と退勤</label><br>
すべての項目</li>
		</ul>
	</div>
	<form class="dialog-form_form" id="dialog-form_timecard_insert_form">
		<fieldset>
		<ul class="dialog_ul">
			<li>
			<table>
				<tr>
					<td colSpan="5">
						<label for="dialog-form_timecard_insert_uid">●　名前</label>
						<select id="dialog-form_timecard_insert_uid" name="uid"></select>
					</td>
				</tr>
				<tr>
					<td class="dialog-form_form_search_box"></td>
					<td><input id="dialog-form_timecard_insert_search_users_text" type="text" style="width:100px;" placeholder="文字を入力後→"></td>
					<td><input id="dialog-form_timecard_insert_search_users_button" type="button" value="候補絞込"></td>
					<td><input id="dialog-form_timecard_insert_search_users_button_reset" type="button" value="候補のクリア"></td>
					<td><span id="dialog-form_timecard_insert_search_users_message" style="color:red"></span></td>
				</tr>
			</table>
			</li>
			<li>
			<label for="dialog-form_timecard_insert_tdate">●　日付</label>
			<input type="text" id="dialog-form_timecard_insert_tdate" name="tdate"><br>
			</li>
			<li>
			<label for="dialog-form_timecard_insert_intime">●　出勤</label>
			<select id="dialog-form_timecard_insert_intime_h" name="intime_h"></select>:
			<select id="dialog-form_timecard_insert_intime_m" name="intime_m"></select>
			</li>
			<li>
			<label for="dialog-form_timecard_insert_movetime">●　移動</label>
			<select id="dialog-form_timecard_insert_movetime_h" name="movetime_h"></select>:
			<select id="dialog-form_timecard_insert_movetime_m" name="movetime_m"></select>
			<label>
				<input id="dialog-form_timecard_insert_vacation" type="checkbox" style="margin-left:25px;" onclick="clickVacation(this.checked);">
				有休</label>
			</li>
			<li>
			<label for="dialog-form_timecard_insert_outtime">●　退勤</label>
			<select id="dialog-form_timecard_insert_outtime_h" name="outtime_h"></select>:
			<select id="dialog-form_timecard_insert_outtime_m" name="outtime_m"></select>
			</li>
			<li>
			<table>
				<tr>
					<td colSpan="5">
						<label for="dialog-form_timecard_insert_wid">●　勤務種別</label>
						<select id="dialog-form_timecard_insert_wid" name="wid"></select>
					</td>
					<td>
						<label>
						<input id="dialog-form_timecard_insert_search_worktype_enable" type="checkbox" style="margin-left:5px;" checked>
						凍結を除く</label>
					</td>
				</tr>
				<tr>
					<td class="dialog-form_form_search_box"></td>
					<td><input id="dialog-form_timecard_insert_search_worktype_text" type="text" style="width:100px;" placeholder="文字を入力後→"></td>
					<td><input id="dialog-form_timecard_insert_search_worktype_button" type="button" value="候補絞込"></td>
					<td><input id="dialog-form_timecard_insert_search_worktype_button_reset" type="button" value="候補のクリア"></td>
					<td><span id="dialog-form_timecard_insert_search_worktype_message" style="color:red"></span></td>
				</tr>
				<tr>
					<td></td>
					<td><input id="dialog-form_timecard_insert_temporary" type="button" value="仮入力"></td>
				</tr>
			</table>
			</li>
			<li>
			<table>
				<tr><td colSpan="2"><label for="dialog-form_timecard_insert_note">●　業務内容(50字以内)</label></td></tr>
				<tr>
					<td class="dialog-form_form_search_box"></td>
					<td><textarea name="note" id="dialog-form_timecard_insert_note" rows="3" cols="40" placeholder="空白"></textarea></td>
				</tr>
			</table>
			</li>
			<label for="dialog-form_timecard_insert_breaktime">●　休憩時間</label>
			<input type="text" id="dialog-form_timecard_insert_breaktime" name="breaktime"/><br>
			</li>
			<li>
			<label for="dialog-form_timecard_insert_fare">●　交通費</label>
			<input type="text" id="dialog-form_timecard_insert_fare" name="fare"><br>
			</li>
			<li>
			<label for="dialog-form_timecard_insert_wcount">●　処理数</label>
			<input type="text" id="dialog-form_timecard_insert_wcount" name="wcount"><br>
			</li>
			<li>
			<label for="dialog-form_timecard_insert_rcount">●　申出数</label>
			<input type="text" id="dialog-form_timecard_insert_rcount" name="rcount"><br>
			</li>
			<li>
			<li>
			<div class="dialog-form_sepalator">
				<label><b>以下　移動後の内容</b></label><hr size="2" width="100%" color="#000000" align="right">
			</div>
			</li>
			<li>
			<table>
				<tr>
					<td colSpan="5">
						<label for="dialog-form_timecard_insert_wid_move">●　(移)勤務種別</label>
						<select id="dialog-form_timecard_insert_wid_move" name="wid_move"></select>
					</td>
					<td>
						<label>
						<input id="dialog-form_timecard_insert_search_worktype_move_enable" type="checkbox" style="margin-left:5px;">
						凍結を除く</label>
					</td>
				</tr>
				<tr>
					<td class="dialog-form_form_search_box"></td>
					<td><input id="dialog-form_timecard_insert_search_worktype_move_text" type="text" style="width:100px;" placeholder="文字を入力後→"></td>
					<td><input id="dialog-form_timecard_insert_search_worktype_move_button" type="button" value="候補絞込"></td>
					<td><input id="dialog-form_timecard_insert_search_worktype_move_button_reset" type="button" value="候補のクリア"></td>
					<td><span id="dialog-form_timecard_insert_search_worktype_move_message" style="color:red"></span></td>
				</tr>
			</table>
			</li>
			<li>
			<table>
				<tr><td colSpan="2"><label for="dialog-form_timecard_insert_note_move">●　(移)業務内容(50字以内)</label></td></tr>
				<tr>
					<td class="dialog-form_form_search_box"></td>
					<td><textarea name="note_move" id="dialog-form_timecard_insert_note_move" rows="3" cols="40" placeholder="空白"></textarea></td>
				</tr>
			</table>
			</li>
			<li>
			<label for="dialog-form_timecard_insert_wcount_move">●　(移)処理数</label>
			<input type="text" id="dialog-form_timecard_insert_wcount_move" name="wcount_move"><br>
			</li>
			<li>
			<label for="dialog-form_timecard_insert_rcount_move">●　(移)申出数</label>
			<input type="text" id="dialog-form_timecard_insert_rcount_move" name="rcount_move"><br>
			</li>
		</ul>
		<input type="hidden" id="dialog-form_timecard_insert_uname" name="uname">
		<input type="hidden" id="dialog-form_timecard_insert_wname" name="wname">
		<input type="hidden" id="dialog-form_timecard_insert_wname" name="wname_move">
		<input type="hidden" id="dialog-form_timecard_insert_ismove" name="ismove">
		</fieldset>
	</form>
</div>