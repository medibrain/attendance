<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="menu">
	<div class="btnContent">
		<div class="textContent">
指定の勤怠システムを新しい端末で利用できるようにする場合に使用します。<br><br>
パスワードを付与した登録枠を作成し、端末から指定したパスワードを入力することで登録が完了します。<br>
作成後に表示される<b> 組織ID </b>は必ずメモしておいてください。※表右側の「詳細・編集」からも確認することができます。<br><br>
なお、受付状態にできるのは<b>一枠のみ</b>です。
		</div>
		<input type="button" id="insert_unit_button" value="端末受付枠を追加">
	</div>
</div>