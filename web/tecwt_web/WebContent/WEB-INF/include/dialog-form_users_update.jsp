<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-form_users_update" title="ユーザ情報の編集・管理">
	<p class="validateTips">編集したい部分を正しく入力してください。</p>
	<form class="dialog-form_form" id="dialog-form_users_update_form">
		<fieldset>
		<ul class="dialog_ul">
			<li>ログインID
			<table>
				<tr>
					<td><input type="text" id="dialog-form_users_update_lname" name="lname" style="width:150px;"></td>
				</tr>
				<tr>
					<td><input type="text" id="dialog-form_users_update_lname_confirm" style="width:150px;" placeholder="再入力"></td>
				</tr>
			</table>
			</li>
			<li>パスワード
			<table>
				<tr>
					<td><input type="password" id="dialog-form_users_update_pass" name="pass" style="width:150px;"></td>
				</tr>
				<tr>
					<td><input type="password" id="dialog-form_users_update_pass_confirm" style="width:150px;" placeholder="再入力"></td>
				</tr>
			</table>
			</li>
			<li>
			<label for="dialog-form_users_update_name">名前</label>
			<input type="text" id="dialog-form_users_update_name" name="name" style="width:150px;">
			</li>
			<li>
			<label for="dialog-form_users_update_kana">カナ</label>
			<input type="text" id="dialog-form_users_update_kana" name="kana" style="width:150px;">
			</li>
			<li>
				<label for="dialog-form_users_update_employment">雇用形態：</label>
				<select id="dialog-form_users_update_employment" name="employment"></select>
			</li>
			<li>
				<label for="dialog-form_users_update_worklocation">勤務地：</label>
				<select id="dialog-form_users_update_worklocation" name="worklocation"></select>
			</li>
			<li>
				<label for="dialog-form_users_update_department">部署：</label>
				<select id="dialog-form_users_update_department" name="department"></select>
			</li>
			<li>
				<label for="dialog-form_users_update_code">社員番号</label>
				<input type="text" id="dialog-form_users_update_code" name="code" style="width:150px;">
			</li>
			<li>
				<label for="dialog-form_users_update_mailladd">メールアドレス</label>
				<input type="text" id="dialog-form_users_update_mailladd" name="mailladd" style="width:200px;">
			</li>
			<li>
				<label for="dialog-form_users_update_mailenable">メール打刻</label>
				<select id="dialog-form_users_update_mailenable" name="mailenable">	<!-- boolean型で返す -->
					<option value="true">許可する</option>
					<option value="false">----(許可しない)</option>
				</select>
			</li>
			<li>
				<label for="dialog-form_users_update_group">グループ：</label>
				<select id="dialog-form_users_update_group" name="group"></select>
			</li>
			<li>
				<label for="dialog-form_users_update_hiredate">雇用開始日</label>
				<input type="text" id="dialog-form_users_update_hiredate" name="hiredate">
			</li>
			<li>
				<label for="dialog-form_users_update_paidvacationdays">有休残日数</label>
				<input type="text" id="dialog-form_users_update_paidvacationdays" name="paidvacationdays" style="width:20px;">
			</li>
			<li>
				<label for="dialog-form_users_update_admin">閲覧権限</label>
				<select id="dialog-form_users_update_admin" name="admin">	<!-- boolean型で返す -->
					<option value="true">管理者</option>
					<option value="false">一般(「マイページ・過去勤怠」のみ閲覧可能)</option>
				</select>
			</li>
		</ul>
		<input type="hidden" id="dialog-form_users_update_oid" name="oid">
		<input type="hidden" id="dialog-form_users_update_uid" name="uid">
		<input type="hidden" id="dialog-form_users_update_enable" name="enable">
		<input type="hidden" id="dialog-form_users_update_change" name="change">
		</fieldset>
	</form>
</div>