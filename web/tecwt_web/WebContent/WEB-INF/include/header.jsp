<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- HEADER HEADER -->
<header>
<div class="headerBox">
	<div id="headerBox_image">
		<img src="../my-content/images/header_image.png" height=80>
	</div>
	<div class="headerBox_datetime" id="headerBox_datetime"></div>
	<a id="logout">ログアウト</a>
</div>
</header>

<!-- HEADER TAB -->
<nav class="nav">
	<ul class="tab" id="header_tab">
		<li class="tab_li" id="tab_0"><span>TOP</span></li>
		<li class="tab_li" id="tab_1"><span>マイページ</span></li>
		<%
		String admin = (String)request.getAttribute("admin");
		boolean isAdmin = admin.equals("true");
		%>
		<% if(isAdmin) { %><li class="tab_li" id="tab_2"><span>勤怠表</span></li><% } %>
		<% if(isAdmin) { %>
			<li class="tab_li_parent" id="tab_3"><span>社員</span>
				<ul class="tab_ul_children">
					<li id="tab_3_1"><span>>　部署</span></li>
					<li id="tab_3_2"><span>>　勤務地</span></li>
					<li id="tab_3_3"><span>>　ユーザ</span></li>
					<li id="tab_3_4"><span>>　雇用形態</span></li>
					<li id="tab_3_5"><span>>　グループ</span></li>
				</ul>
			</li>
		<% } %>
		<% if(isAdmin) { %>
			<li class="tab_li_parent" id="tab_4"><span>勤務種別</span>
				<ul class="tab_ul_children">
					<li id="tab_4_1"><span>>　客先</span></li>
					<li id="tab_4_2"><span>>　カテゴリー</span></li>
					<li id="tab_4_3"><span>>　種別</span></li>
				</ul>
			</li>
		<% } %>
		<% if(isAdmin) { %><li class="tab_li" id="tab_5"><span>端末・アプリ</span></li><% } %>
		<% if(!isAdmin) { %><li class="tab_li" id="tab_6"><span>勤務種別コード</span></li><% } %>
		<% if(!isAdmin) { %><li class="tab_li" id="tab_7"><span>作業内容コード</span></li><% } %>
	</ul>
</nav>