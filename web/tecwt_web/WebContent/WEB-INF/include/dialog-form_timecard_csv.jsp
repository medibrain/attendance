<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-form_csv" title="CSV形式のダウンロード">
	<p class="validateTips">
	タイムカードをCSVファイルとしてダウンロードすることができます。<br>
	<br>
	<b>現在表示されている内容でダウンロードする場合</b>は[現在表示されている一覧を出力する]にチェックを入れてください。<br>
	<b>年・月を選択する場合</b>は上記のチェックを外し、表示されるセレクトボックスから該当する年・月を選択して下さい。なお、月のみで出力することはできません。
	</p>
	<form class="dialog-form_form" id="dialog-form_csv_form">
		<fieldset>
		<input class="dialog_item" type="checkbox" id="dialog-form_csv_isviewlist" /><label for="dialog-form_csv_isviewlist"> 現在表示されている一覧を出力する</label>
		<ul class="dialog_ul">
			<li id="dialog-form_csv_date">
			<label for="dialog-form_csv_year"></label>
			<select id="dialog-form_csv_year"></select>年
			<label for="dialog-form_csv_month"></label>
			<select id="dialog-form_csv_month"></select>月分を出力する
			</li>
			<li>
			<input type="checkbox" id="dialog-form_csv_istimeexist" /><label for="dialog-form_csv_istimeexist"> 【出勤のみ】,【移動のみ】,【退勤のみ】は含めない</label>
			</li>
			<li>
			<label for="dialog-form_csv_filename">保存するファイル名</label>
			<input type="text" id="dialog-form_csv_filename" />.csv
			</li>
			<li>
			<label for="dialog-form_csv_type">出力タイプ</label>
			<p>
			<label><input type="radio" id="dialog-form_csv_type1" name="dialog-form_csv_type" value="timecard">タイムカード</label>
			<label><input type="radio" id="dialog-form_csv_type2" name="dialog-form_csv_type" value="timecard_incomestatement">損益計算</label>
			<label><input type="radio" id="dialog-form_csv_type3" name="dialog-form_csv_type" value="worklist">作業内容一覧</label>
			</p>
			</li>
		</ul>
		</fieldset>
	</form>
</div>