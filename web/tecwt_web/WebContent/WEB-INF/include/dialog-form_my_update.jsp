<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-form_my_update" title="ユーザ情報の変更">
	<p class="validateTips">編集したい部分を正しく入力してください。</p>
	<form class="dialog-form_form" id="dialog-form_my_update_form">
		<fieldset>
		<ul>
			<li>ログインID
			<table>
			<tr>
				<td><input type="text" id="dialog-form_my_update_lname" name="lname" style="width:150px;"></td>
			</tr>
			<tr>
				<td><input type="text" id="dialog-form_my_update_lname_confirm" style="width:150px;">(再入力)</td>
			</tr>
			</table>
			</li>
			<li>パスワード
			<table>
			<tr>
				<td><input type="password" id="dialog-form_my_update_pass" name="pass" style="width:150px;"></td>
			</tr>
			<tr>
				<td><input type="password" id="dialog-form_my_update_pass_confirm" style="width:150px;">(再入力)</td>
			</tr>
			</table>
			</li>
		</ul>
		<input type="hidden" id="dialog-form_my_update_oid" name="oid">
		<input type="hidden" id="dialog-form_my_update_uid" name="uid">
		<input type="hidden" id="dialog-form_my_update_change" name="change">
		</fieldset>
	</form>
</div>