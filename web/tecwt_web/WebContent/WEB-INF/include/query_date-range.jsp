<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="menu">
<form id="query_tdate_range">
	<label>●  日付の範囲で検索</label>
	<div class="formContent">
	<label>日付A :</label>
	<input class="dateTextArea" type="text" id="tdate_range1" name="date1" readonly="readonly"><br>
	<label>日付B :</label>
	<input class="dateTextArea" type="text" id="tdate_range2" name="date2" readonly="readonly"><br>
	</div>
	<input type="button" id="query_tdate_range_button" value="検索">
	<input type="button" id="query_tdate_range_narrowing_button" value="絞込" style="visibility:hidden;">
	<input type="button" id="query_tdate_range_reset_button" value="..." style="visibility:hidden;">
</form>
</div>