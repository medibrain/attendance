<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="menu">
<form id="query_year-month">
	<label>●  年・月で検索</label>
	<div class="formContent">
		<div>
			<label>年 :</label>
			<select class="yearBox" id="history_tdate_year" name="year" ></select>
		</div>
		<div>
			<label>月 :</label>
			<select class="monthBox" id="history_tdate_month" name="month" ></select>
		</div>
	</div>
	<input type="button" id="query_tdate_yearmonth_button" value="検索">
	<input type="button" id="query_tdate_nowmonth_button" value="今月" style="visibility:hidden;color:red;">
	<input type="button" id="query_tdate_lastmonth_button" value="先月" style="visibility:hidden;color:blue;">
	<input type="button" id="query_tdate_nextmonth_button" value="来月" style="visibility:hidden;color:green;">
</form>
</div>