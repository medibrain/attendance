<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-form_department_update" title="部署の編集・管理">
	<p class="validateTips">編集したい部分を正しく入力してください。</p>
	<form class="dialog-form_form" id="dialog-form_department_update_form">
		<fieldset>
		<ul class="dialog_ul">
			<li>
				<label for="dialog-form_department_update_name">部署名</label>
				<input class="inputBox" type="text" id="dialog-form_department_update_name" name="name">
			</li>
			<li>
				<label for="dialog-form_department_update_kana">カナ</label>
				<input class="inputBox" type="text" id="dialog-form_department_update_kana" name="kana">
			</li>
			<li>
				<table>
					<tr>
						<td colSpan="5">
							<label for="dialog-form_department_update_manager">責任者</label>
							<select id="dialog-form_department_update_manager" name="manager"></select>
						</td>
					</tr>
					<tr>
						<td class="dialog-form_form_search_box"></td>
						<td><input id="dialog-form_department_update_search_users_text" type="text" style="width:100px;" placeholder="文字を入力後→"></td>
						<td><input id="dialog-form_department_update_search_users_button" type="button" value="候補絞込"></td>
						<td><input id="dialog-form_department_update_search_users_button_reset" type="button" value="候補のクリア"></td>
						<td><span id="dialog-form_department_update_search_users_message" style="color:red"></span></td>
					</tr>
				</table>
			</li>
			<li>
				<label for="dialog-form_department_update_note">メモ</label><br>
				<textarea name="note" id="dialog-form_department_update_note" rows="3" cols="40"></textarea>
			</li>
		</ul>
		<input type="hidden" id="dialog-form_department_update_enable" name="enable">
		<input type="hidden" id="dialog-form_department_update_departmentid" name="departmentid">
		<input type="hidden" id="dialog-form_department_update_change" name="change">
		</fieldset>
	</form>
</div>