<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-div_tclog_viewer">
	<p>※【位置情報】はクリックすると<span style="color:red;">GoogleMap</span>を開きます。</p>
	<table class="dialog-div_table" id="dialog-div_tclog_viewer_table">
		<thead>
			<tr>
				<th class="dialog-div_table_logtype">ログタイプ</th>
				<th class="dialog-div_table_logdate">操作日</th>
				<th class="dialog-div_table_logtime">操作時刻</th>
				<th class="dialog-div_table_func">打刻内容</th>
				<th class="dialog-div_table_ll">位置情報(GoogleMap)</th>
				<th class="dialog-div_table_unitname">端末名</th>
			</tr>
		</thead>
		<tbody id="dialog-div_tclog_viewer_table_tbody">
		</tbody>
	</table>
</div>