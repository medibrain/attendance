<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-form_group_insert" title="グループの新規追加">
	<p class="validateTips">新しく追加するグループの情報を入力してください</p>
	<form class="dialog-form_form" id="dialog-form_group_insert_form">
		<fieldset>
		<ul class="dialog_ul">
			<li>
				<label for="dialog-form_group_insert_name">名前</label>
				<input class="inputBox" type="text" id="dialog-form_group_insert_name" name="name">
			</li>
			<li>
				<label for="dialog-form_group_insert_kana">カナ</label>
				<input class="inputBox" type="text" id="dialog-form_group_insert_kana" name="kana">
			</li>
			<li>
				<label for="dialog-form_group_insert_note">メモ</label><br>
				<textarea name="note" id="dialog-form_group_insert_note" rows="3" cols="40"></textarea>
			</li>
		</ul>
		</fieldset>
	</form>
</div>