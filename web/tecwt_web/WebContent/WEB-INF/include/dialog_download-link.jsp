<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog_download-link" title="CSVのダウンロード">
	<div style="text-align:center; padding:10px 10px;">
		<a href="" style="color:blue" onclick="document.form1.submit();return false;">ここをクリックするとダウンロードを開始します</a>
		<form id="download-form" name="form1" method="POST" action="download/csv"></form>
	</div>
	<div style="text-align:left; padding:10px 10px;">
		※ダウンロードできない場合は他のブラウザで行っていただくか、<br>もしくは下記リンクをクリックして表示されるページにて文字をコピーしてメモ帳などに貼り付け、CSV形式で保存して下さい。<br>
		<a href="" style="color:black" onclick="document.form2.submit();return false;">ダウンロードできない場合はここをクリックして下さい</a>
		<form id="download-form_dlfalse" name="form2" method="POST" action="download/csv?dl=false" target="_blank"></form>
	</div>
</div>