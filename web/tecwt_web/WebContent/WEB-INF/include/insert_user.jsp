<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="menu">
	<div class="btnContent">
		<div class="textContent">
新規ユーザを作成します。<br><br>ユーザを作成すると当サイトにログインできる他、指定の勤怠システムと連動することができるようになります。
		</div>
		<input type="button" id="insert_user_button" value="ユーザを追加">
	</div>
</div>