<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-form_users_insert" title="新規ユーザの追加">
	<p class="validateTips">以下の項目を入力・選択して下さい。</p>
	<form class="dialog-form_form" id="dialog-form_users_insert_form">
		<fieldset>
		<ul class="dialog_ul">
			<li>ログインID
			<table>
				<tr>
					<td><input type="text" id="dialog-form_users_insert_lname" name="lname" style="width:150px;"></td>
				</tr>
				<tr>
					<td><input type="text" id="dialog-form_users_insert_lname_confirm" name="lname_confirm" style="width:150px;" placeholder="再入力"></td>
				</tr>
			</table>
			</li>
			<li>パスワード
			<table>
				<tr>
					<td><input type="password" id="dialog-form_users_insert_pass" name="pass" style="width:150px;"></td>
				</tr>
				<tr>
					<td><input type="password" id="dialog-form_users_insert_pass_confirm" name="pass_confirm" style="width:150px;" placeholder="再入力"></td>
				</tr>
			</table>
			</li>
			<li>
			<label for="dialog-form_users_insert_name">名前</label>
			<input type="text" id="dialog-form_users_insert_name" name="name" style="width:100px;">
			</li>
			<li>
			<label for="dialog-form_users_insert_kana">カナ</label>
			<input type="text" id="dialog-form_users_insert_kana" name="kana" style="width:100px;">
			</li>
			<li>
				<label for="dialog-form_users_insert_employment">雇用形態：</label>
				<select id="dialog-form_users_insert_employment" name="employment"></select>
			</li>
			<li>
				<label for="dialog-form_users_insert_worklocation">勤務地：</label>
				<select id="dialog-form_users_insert_worklocation" name="worklocation"></select>
			</li>
			<li>
				<label for="dialog-form_users_insert_department">部署：</label>
				<select id="dialog-form_users_insert_department" name="department"></select>
			</li>
			<li>
			<label for="dialog-form_users_insert_code">社員番号</label>
			<input type="text" id="dialog-form_users_insert_code" name="code" style="width:100px;">
			</li>
			<li>
			<label for="dialog-form_users_insert_mailladd">メールアドレス</label>
			<input type="text" id="dialog-form_users_insert_mailladd" name="mailladd" style="width:200px;">
			</li>
			<li>
			<label for="dialog-form_users_insert_mailenable">メール打刻</label>
			<select id="dialog-form_users_insert_mailenable" name="mailenable">	<!-- boolean型で返す -->
				<option value="true">許可する</option>
				<option value="false">----(許可しない)</option>
			</select>
			</li>
			<li>
				<label for="dialog-form_users_insert_group">グループ：</label>
				<select id="dialog-form_users_insert_group" name="group"></select>
			</li>
			<li>
				<label for="dialog-form_users_insert_hiredate">雇用開始日</label>
				<input type="text" id="dialog-form_users_insert_hiredate" name="hiredate">
			</li>
			<li>
				<label for="dialog-form_users_insert_paidvacationdays">有休残日数</label>
				<input type="text" id="dialog-form_users_insert_paidvacationdays" name="paidvacationdays" style="width:20px;">
			</li>
			<li>
			<label for="dialog-form_users_insert_admin">閲覧権限</label>
			<select id="dialog-form_users_insert_admin" name="admin">	<!-- boolean型で返す -->
				<option value="false">一般(「マイページ・過去勤怠」のみ閲覧可)</option>
				<option value="true">管理者</option>
			</select>
			</li>
		</ul>
		</fieldset>
	</form>
</div>