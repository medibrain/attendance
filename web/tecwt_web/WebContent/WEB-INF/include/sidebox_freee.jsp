<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="csvMenu">
	<div class="menuHeader" id="freeeHeader">
		<table>
			<tr>
				<td class="plus">+</td>
				<td class="text">Freee連携</td>
			</tr>
		</table>
	</div>
	<div id="freeeMenu" class="menuFrame">
		<div class="menu">
			<div class="btnContent">
				<div class="textContent">
					Freeeとの連携の設定を行います。
				</div>
				<input type="button" id="freee_init_button" value="初期設定">
				<input type="button" id="freee_refresh_button" value="トークン更新">
				<input type="button" id="freee_relink_button" value="再連携・事業所選択">
			</div>
		</div>
	</div>
</div>