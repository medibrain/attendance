<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-form_unit_update" title="端末の編集・管理">
	<p class="validateTips">編集したい部分を正しく入力してください。</p>
	<form class="dialog-form_form" id="dialog-form_unit_update_form">
		<fieldset>
		<ul class="dialog_ul">
			<li>
			<label for="dialog-form_unit_update_name">端末名</label>
			<input class="inputBox" type="text" id="dialog-form_unit_update_name" name="name">
			</li>
			<li>
			<label for="dialog-form_unit_update_pass">パス</label>
			<input class="inputBox" type="text" id="dialog-form_unit_update_pass" name="pass">
			</li>
			<li>
			<label for="dialog-form_unit_update_oid">組織ID</label>
			<input class="inputBox" type="text" id="dialog-form_unit_update_oid" name="pass">
			</li>
			<li>
			<label for="dialog-form_unit_update_note">memo</label><br>
			<textarea name="note" id="dialog-form_unit_update_note" rows="3" cols="40"></textarea>
			</li>
		</ul>
		<input type="hidden" id="dialog-form_unit_update_enable" name="enable">
		<input type="hidden" id="dialog-form_unit_update_unitid" name="unitid">
		<input type="hidden" id="dialog-form_unit_update_change" name="change">
		</fieldset>
	</form>
</div>