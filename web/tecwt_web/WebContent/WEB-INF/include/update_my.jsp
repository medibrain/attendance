<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="menu">
	<div class="myContent">
		<p>社員番号: <label id="my_code"></label></p>
		<p>名前: <label id="my_name"></label> (<label id="my_kana"></label>)</p>
		<p>メールアドレス: <label id="my_mailladd"></label></p>
	</div>
	<div class="btnContent">
		<input type="button" id="update_my_button" value="ユーザ情報を変更する">
	</div>
</div>