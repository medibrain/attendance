<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-form_employment_update" title="雇用形態の編集・管理">
	<p class="validateTips">編集したい部分を正しく入力してください。</p>
	<form class="dialog-form_form" id="dialog-form_employment_update_form">
		<fieldset>
		<ul class="dialog_ul">
			<li>
				<label for="dialog-form_employment_update_name">名前</label>
				<input class="inputBox" type="text" id="dialog-form_employment_update_name" name="name">
			</li>
			<li>
				<label for="dialog-form_employment_update_kana">カナ</label>
				<input class="inputBox" type="text" id="dialog-form_employment_update_kana" name="kana">
			</li>
			<li>
				<label for="dialog-form_employment_update_note">メモ</label><br>
				<textarea name="note" id="dialog-form_employment_update_note" rows="3" cols="40"></textarea>
			</li>
		</ul>
		<input type="hidden" id="dialog-form_employment_update_enable" name="enable">
		<input type="hidden" id="dialog-form_employment_update_employmentid" name="employmentid">
		<input type="hidden" id="dialog-form_employment_update_change" name="change">
		</fieldset>
	</form>
</div>