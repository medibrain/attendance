<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="menu">
<form id="history_query_time_range">
	<label>●  時間の範囲で検索</label>
	<div class="formContent">
	<label>出勤 :</label>
	<select class="timeBox" id="history_intime_h" name="inH" ></select>:
	<select class="timeBox" id="history_intime_m" name="inM" ></select><br>
	<label>移動 :</label>
	<select class="timeBox" id="history_movetime_h" name="moveH" ></select>:
	<select class="timeBox" id="history_movetime_m" name="moveM" ></select><br>
	<label>退勤 :</label>
	<select class="timeBox" id="history_outtime_h" name="outH" ></select>:
	<select class="timeBox" id="history_outtime_m" name="outM" ></select><br>
	</div>
	<input type="button" id="history_query_time_range_button" value="検索">
	<input type="button" id="history_query_time_narrowing_button" value="絞込">
	<input type="button" id="history_query_time_reset_button" value="...">
</form>
</div>