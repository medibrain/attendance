<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-form_worktype_update" title="勤務種別の編集・管理">
	<p class="validateTips">編集したい部分を正しく入力してください。</p>
	<form class="dialog-form_form" id="dialog-form_worktype_update_form">
		<fieldset>
		<ul class="dialog_ul">
			<li>
				<label for="dialog-form_worktype_update_name">勤務種別名</label>
				<input class="worktypeBox" type="text" id="dialog-form_worktype_update_name" name="name">
			</li>
			<li>
				<label for="dialog-form_worktype_update_kana">カナ (必ずカタカナで入力して下さい)</label>
				<input class="worktypeBox" type="text" id="dialog-form_worktype_update_kana" name="kana">
			</li>
			<li>
				<label for="dialog-form_worktype_update_projectcode">プロジェクトコード</label>
				<input class="worktypeBox" type="text" id="dialog-form_worktype_update_projectcode" name="projectcode">
			</li>
			<li>
				<label for="dialog-form_worktype_update_customer">客先：</label>
				<select id="dialog-form_worktype_update_customer" name="customer"></select>
			</li>
			<li>
				<label for="dialog-form_worktype_update_category">カテゴリー：</label>
				<select id="dialog-form_worktype_update_category" name="category"></select>
			</li>
			<li>
				<table>
					<tr>
						<td colSpan="5">
							<label for="dialog-form_worktype_update_manager">責任者</label>
							<select id="dialog-form_worktype_update_manager" name="manager"></select>
						</td>
					</tr>
					<tr>
						<td class="dialog-form_form_search_box"></td>
						<td><input id="dialog-form_worktype_update_search_users_text" type="text" style="width:100px;" placeholder="文字を入力後→"></td>
						<td><input id="dialog-form_worktype_update_search_users_button" type="button" value="候補絞込"></td>
						<td><input id="dialog-form_worktype_update_search_users_button_reset" type="button" value="候補のクリア"></td>
						<td><span id="dialog-form_worktype_update_search_users_message" style="color:red"></span></td>
					</tr>
				</table>
			</li>
			<li>
				<label for="dialog-form_worktype_update_note">memo</label><br>
				<textarea id="dialog-form_worktype_update_note" name="note" rows="3" cols="40"></textarea>
			</li>
		</ul>
		<input type="hidden" id="dialog-form_worktype_update_oid" name="oid">
		<input type="hidden" id="dialog-form_worktype_update_wid" name="wid">
		<input type="hidden" id="dialog-form_worktype_update_enable" name="enable">
		<input type="hidden" id="dialog-form_worktype_update_change" name="change">
		</fieldset>
	</form>
</div>