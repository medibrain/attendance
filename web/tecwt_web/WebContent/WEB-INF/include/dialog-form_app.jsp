<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-form_app" title="Androidアプリのダウンロード">
apkファイル(Androidアプリ)のダウンロードを開始します。<br>
<br>
Android端末から直接ダウンロードした場合、ステータスバーの通知から「<span style="color:red">jp.co.tecwt.medi_techno_service.apk</span>」を選択し、
表示された画面からインストールを行って下さい。<br>
<br>
<br>
※ CAUTION ※<br>
なお、使用できる端末は「NFC対応のもの」のみとなります。<br>
また実際に使用される場合には、tecwt.comの「端末・アプリ」画面にて端末登録を行って下さい。
</div>