<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="dialog-form" id="dialog-form_worktype_insert" title="勤務種別の追加">
	<p class="validateTips">以下の項目を全て入力して下さい。</p>
	<form class="dialog-form_form" id="dialog-form_worktype_insert_form">
		<fieldset>
		<ul class="dialog_ul">
			<li>
				<label for="dialog-form_worktype_insert_name">勤務種別名</label>
				<input class="worktypeBox" type="text" id="dialog-form_worktype_insert_name" name="name">
			</li>
			<li>
				<label for="dialog-form_worktype_insert_kana">カナ (必ずカタカナで入力して下さい)</label>
				<input class="worktypeBox" type="text" id="dialog-form_worktype_insert_kana" name="kana">
			</li>
			<li>
				<label for="dialog-form_worktype_insert_projectcode">プロジェクトコード</label>
				<input class="worktypeBox" type="text" id="dialog-form_worktype_insert_projectcode" name="projectcode">
			</li>
			<li>
				<label for="dialog-form_worktype_insert_customer">客先：</label>
				<select id="dialog-form_worktype_insert_customer" name="customer"></select>
			</li>
			<li>
				<label for="dialog-form_worktype_insert_category">カテゴリー：</label>
				<select id="dialog-form_worktype_insert_category" name="category"></select>
			</li>
			<li>
				<table>
					<tr>
						<td colSpan="5">
							<label for="dialog-form_worktype_insert_manager">責任者</label>
							<select id="dialog-form_worktype_insert_manager" name="manager"></select>
						</td>
					</tr>
					<tr>
						<td class="dialog-form_form_search_box"></td>
						<td><input id="dialog-form_worktype_insert_search_users_text" type="text" style="width:100px;" placeholder="文字を入力後→"></td>
						<td><input id="dialog-form_worktype_insert_search_users_button" type="button" value="候補絞込"></td>
						<td><input id="dialog-form_worktype_insert_search_users_button_reset" type="button" value="候補のクリア"></td>
						<td><span id="dialog-form_worktype_insert_search_users_message" style="color:red"></span></td>
					</tr>
				</table>
			</li>
			<li>
				<label for="dialog-form_worktype_insert_note">memo</label><br>
				<textarea id="dialog-form_worktype_insert_note" name="note" rows="3" cols="40"></textarea>
			</li>
		</ul>
		</fieldset>
	</form>
</div>