<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="content-style-type" content="text/css">
<style type="text/css">
.workin-title {
	width: 600px;
	margin-top: 10px;
	margin-bottom: 20px;
	line-height: 30px;
	vertical-align: middle;
}
.workin-title span {
	width: 400px;
	font-size: 20px;
	padding-left: 2px;
	padding-right: 2px;
	border-bottom: solid 1px #444444;
	font-weight: 600;
}
#workin-base {
	overflow: scroll;
	border: 2px inset #aaaaaa;
	width: 100%;
	height: 100%;
	margin-top: 30px;
	padding-left: 20px;
	background-color: #ffffff;
	text-align: left;
}
#workin-list {
	width: 100%;
	margin: 0;
	padding-left: 10px;
}
.border {
	width: 95%;
	margin-left: 0;
	text-align: left;
}
.left {
	width: 40px;
	padding-left: 20px;
	font-size: 15px;
	text-align: cneter;
	font-weight: 600;
}
.center {
	width: auto;
	padding-left: 30px;
	font-size: 15px;
	text-align: left;
}
.centerOut {
	width: auto;
	padding-left: 30px;
	font-size: 15px;
	text-align: left;
	color: red;
}
.right {
	width: 40px;
	font-size: 15px;
	text-align: left;
	color: red;
}
.rightRight {
	width: auto;
	padding-left: 5px;
	font-size: 15px;
	text-align: left;
}
.rightRightOut {
	width: auto;
	padding-left: 5px;
	font-size: 15px;
	text-align: left;
	color:red;
}
.rightTop {
	width: 100%;
	padding-right: 30px;
	text-align: right;
}
.empty {
	padding-left: 15px;
}
.reload {
	width: 50px;
	margin-left: 50px;
	margin-bottom: 5px;
	font-size: 12px;
	padding: 5px 10px;
	background-color: orange;
	color: white;
	border-radius: 5px;
	border-style: none;
	vertical-align: bottom;
}
.reload:hover {
	background-color: red;
	color: white;
}
</style>
</head>
<div id="workin-base">
<div class="workin-title"><span>本日の出勤状況</span><a id="reload" class="reload">更新</a></div>
<table id="workin-list"></table>
</div>
</html>