package jp.co.tecwt.medi_techno_service.dialog;

import jp.co.tecwt.medi_techno_service.R;
import jp.co.tecwt.medi_techno_service.data.DialogHolder;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * 画面が回転してもダイアログは保持されます。
 * show()を実行することでonCreateDialog()が実行され、ダイアログが表示されます。
 */
@SuppressLint("InflateParams")
public class TextDialog extends DialogFragment {

	public enum INPUT_MODE { TEXT, NUMBER, NUMBER_PASSWORD }

	private String title;
	private INPUT_MODE inputMode = INPUT_MODE.TEXT;
	private String pText;
	private String nText;
	private boolean pExist;
	private boolean nExist;

	private TextPositiveListener pListener;
	private TextNegativeListener nListener;
	private Callback callback;
	private View view;


	/**
	 * コールバック。
	 */
	public interface Callback {
		/**
		 * 入力された文字列を返します。空文字も含めます。
		 * @param result
		 */
		public void onResult(String result);
		/**
		 * キャンセルされた場合に呼ばれます。
		 */
		public void onCancel();
	}


	/**
	 * コールバックを登録する。
	 * @param callback
	 * @return
	 */
	public TextDialog setCallback(Callback callback) {
		this.callback = callback;
		return this;
	}
	/**
	 * タイトルを設定する。
	 * @param title
	 * @return
	 */
	public TextDialog setTitle(String title) {
		this.title = title;
		return this;
	}
	/**
	 * 入力モードを設定する。
	 * @param inputMode
	 * @return
	 */
	public TextDialog setInputMode(INPUT_MODE inputMode) {
		this.inputMode = inputMode;
		return this;
	}
	/**
	 * ポジティブボタンの文字を設定します。
	 * @param text
	 * @return
	 */
	public TextDialog setPositiveText(String text) {
		if(text == null || text.equals("")) {
			this.pExist = false;
			this.pText = "";
		} else {
			this.pExist = true;
			this.pText = text;
		}
		return this;
	}
	/**
	 * ネガティブボタンの文字を設定します。
	 * @param text
	 * @return
	 */
	public TextDialog setNegativeText(String text) {
		if(text == null || text.equals("")) {
			this.nExist = false;
			this.nText = "";
		} else {
			this.nExist = true;
			this.nText = text;
		}
		return this;
	}


	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		this.view = LayoutInflater.from(this.getActivity()).inflate(R.layout.dialog_text, null);
		
		String text = null;
		if (savedInstanceState != null) {
			this.pListener = (TextPositiveListener)DialogHolder.INSTANCE.getPositibeListener();
			this.nListener = (TextNegativeListener)DialogHolder.INSTANCE.getNegatibeListener();
			Object[] obj = DialogHolder.INSTANCE.getValues();
			if(obj != null) {
				// 復帰時
				text = obj[0].toString();
				this.callback = (Callback)DialogHolder.INSTANCE.getCallback();
				this.setTitle(DialogHolder.INSTANCE.getTitle());
				switch (DialogHolder.INSTANCE.getMode()) {
					case 1:
						this.setInputMode(INPUT_MODE.TEXT);
						break;
					case 2:
						this.setInputMode(INPUT_MODE.NUMBER);
						break;
					case 3:
						this.setInputMode(INPUT_MODE.NUMBER_PASSWORD);
						break;
					default:
						this.setInputMode(INPUT_MODE.TEXT);
						break;
				}
				this.setPositiveText(DialogHolder.INSTANCE.getPositibeText());
				this.setNegativeText(DialogHolder.INSTANCE.getNegativeText());
			}
		} else {
			this.pListener = new TextPositiveListener(this.view);
			this.nListener = new TextNegativeListener();
		}

		EditText edit = (EditText)this.view.findViewById(R.id.edittext_dialog_text);
		edit.setText(text);
		edit.setOnKeyListener(this.textKeyListener);
		switch (this.inputMode) {
			case TEXT:
				edit.setInputType(InputType.TYPE_CLASS_TEXT);
				break;
			case NUMBER:
				edit.setInputType(InputType.TYPE_CLASS_NUMBER);
				break;
			case NUMBER_PASSWORD:
				edit.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
				break;
		}

		AlertDialog.Builder dialog = new AlertDialog.Builder(this.getActivity());
		dialog.setTitle(this.title);
		dialog.setView(this.view);
		if(this.pExist) dialog.setPositiveButton(this.pText, this.pListener);
		if(this.nExist) dialog.setNegativeButton(this.nText, this.nListener);
		return dialog.create();
	}


	private class TextPositiveListener implements DialogInterface.OnClickListener {
		private View view;
		public TextPositiveListener(View view) {
			this.view = view;
		}
		@Override
		public void onClick(DialogInterface dialog, int witch) {
			String text = ((EditText)this.view.findViewById(R.id.edittext_dialog_text)).getText().toString();
			// コールバックする
			if(TextDialog.this.callback != null) {
				TextDialog.this.callback.onResult(text);
			}
		}
	}
	private class TextNegativeListener implements DialogInterface.OnClickListener {
		@Override
		public void onClick(DialogInterface dialog, int witch) {
			// コールバックする
			if(TextDialog.this.callback != null) {
				TextDialog.this.callback.onCancel();
			}
		}
	}


	/**
	 * 入力監視。改行させない
	 */
	private OnKeyListener textKeyListener = new OnKeyListener() {
		@Override
		public boolean onKey(View view, int keyCode, KeyEvent event) {
			if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
				// 改行させない
				InputMethodManager imm = (InputMethodManager)TextDialog.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				return true;
			}
			return false;
		}
	};


	@Override
	public void onPause() {
		super.onPause();

		// 値の保存
		Object[] values = new Object[1];
		values[0] = ((EditText)this.view.findViewById(R.id.edittext_dialog_text)).getText().toString();
		DialogHolder.INSTANCE.setTitle(this.title);
		switch (inputMode) {
		case TEXT:
			DialogHolder.INSTANCE.setMode(1);
			break;
		case NUMBER:
			DialogHolder.INSTANCE.setMode(2);
			break;
		case NUMBER_PASSWORD:
			DialogHolder.INSTANCE.setMode(3);
			break;
		}
		DialogHolder.INSTANCE.setValues(values);
		DialogHolder.INSTANCE.setCallback(this.callback);
		DialogHolder.INSTANCE.setPositiveText(this.pText);
		DialogHolder.INSTANCE.setNegativeText(this.nText);
		DialogHolder.INSTANCE.setNegativeListener(this.pListener);
		DialogHolder.INSTANCE.setCancelListener(this.nListener);
	}

}
