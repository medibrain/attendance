package jp.co.tecwt.medi_techno_service.view;

import jp.co.tecwt.medi_techno_service.data.MyPreference;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;

/**
 * 全Activityの土台。
 * 共通処理をここで一手に担う。
 */
public class BaseActivity extends Activity {
	
	private NfcAdapter nfc;
	private PendingIntent pendingIntent;
	
	
	/**
	 * NFC読み取り
	 */
	public void nfcEnableForegroundDispatch() {
		if(this.nfc != null) {
			this.nfc.enableForegroundDispatch(this, this.pendingIntent, null, null);
		}
	}
	
	/**
	 * NFC読み取り解除
	 */
	public void nfcDisableForegroundDispatch() {
		if(this.nfc != null) {
			this.nfc.disableForegroundDispatch(this);
		}
	}
	
	/**
	 * 戻る
	 */
	public OnClickListener backBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			BaseActivity.this.finish();
		}
	};
	
	/**
	 * Top
	 */
	public OnClickListener topBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			Intent topIntent = new Intent(BaseActivity.this, Main.class);
			topIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
			BaseActivity.this.startActivity(topIntent);
		}
	};
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// フルスクリーン
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		// スマホなら全画面が縦固定
		if(MyPreference.getInstance(this).isPhone()) {
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}
		
		// NFC読み取り準備
		this.nfc = NfcAdapter.getDefaultAdapter(this.getApplicationContext());
		Intent intent = new Intent(this, this.getClass());
		intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		this.pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
	}
	
	@Override
	public void onStart() {
		super.onStart();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		this.nfcEnableForegroundDispatch();
	}
	
	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		/*
		 * NFCを使用するクラスが任意にオーバーライドする。
		 */
	}
	
	@Override
	public void onPause() {
		super.onPause();
		this.nfcDisableForegroundDispatch();
	}
	
	@Override
	public void onStop() {
		super.onStop();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

}
