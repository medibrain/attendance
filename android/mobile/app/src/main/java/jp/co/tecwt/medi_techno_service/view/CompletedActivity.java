package jp.co.tecwt.medi_techno_service.view;

import jp.co.tecwt.medi_techno_service.R;
import jp.co.tecwt.medi_techno_service.data.MyPreference;
import jp.co.tecwt.medi_techno_service.data.Singleton;
import jp.co.tecwt.medi_techno_service.dialog.MyDialog;
import jp.co.tecwt.medi_techno_service.util.NextAct;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 送信完了画面。
 */
public class CompletedActivity extends BaseActivity {

	public static final String NOT_IN = "not_in";
	public static final String UID = "uid";
	public static final String NAME = "name";
	public static final String DATE = "date";
	public static final String TIME = "time";
	public static final String SYUBETSU = "syubetsu";
	public static final String KYUKEI = "kyukei";
	public static final String GYOUMU = "gyoumu";
	public static final String KOUTSUUHI = "koutsuuhi";
	public static final String SYORIMAISUU = "syorimaisuu";
	public static final String MOUSHIDEMAISUU = "moushidemaisuu";

	private Handler handler;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// レイアウトファイル適用
		setContentView(R.layout.activity_completed_2);

		// ルートに応じて表示内容変更
		Bundle extras = this.getIntent().getExtras();
		switch(Singleton.INSTANCE.getSyuttaiStatus()) {
		case Main.SYUKKIN:
			this.syukkinInit(extras);
			break;
		case Main.TAIKIN:
			this.taikinInit(extras);
			break;
		case Main.IDOU:
			this.idouInit(extras);
			break;
		}

		// 未出勤の場合は強制的に出勤画面へ遷移する
		boolean notIn = false;
		if(extras.getBoolean(NOT_IN)) {
			MyDialog dialog = new MyDialog();
			dialog.setTitle("警告!!");
			dialog.setMessage("本日の[ 出勤 ]がまだ行われていません！\n"
					+ "必ず出勤を行ってから退勤を行って下さい。");
			dialog.setPositive_button("今すぐ出勤する", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Singleton.INSTANCE.init();
					NextAct.init();
					Singleton.INSTANCE.setAppInit_DeviceID(MyPreference.getInstance(CompletedActivity.this).getDeviceID());
					Singleton.INSTANCE.setSyuttai("出勤");
					Singleton.INSTANCE.setSyuttaiStatus(Main.SYUKKIN);
					Intent nextIntent = NextAct.getNextIntent(CompletedActivity.this, Main.SYUKKIN);
					CompletedActivity.this.startActivity(nextIntent);
				}
			});
			dialog.setNegative_button("後で行う(非推奨)", null);
			dialog.show(CompletedActivity.this.getFragmentManager(), null);
			notIn = true;
		}

		// 正常打刻なら60秒後に自動的にTOPへ遷移
		this.handler = new Handler();
		if(!notIn) {
			this.handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					Intent topIntent = new Intent(CompletedActivity.this, Main.class);
					topIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
					CompletedActivity.this.startActivity(topIntent);
				}
			}, 60000);
		}
	}

	private void syukkinInit(Bundle extras) {
		((LinearLayout)this.findViewById(R.id.linearlayout_base_syubetsu)).setVisibility(View.GONE);
		((LinearLayout)this.findViewById(R.id.linearlayout_base_syubetsu_idou)).setVisibility(View.GONE);
		((LinearLayout)this.findViewById(R.id.linearlayout_base_gyoumu)).setVisibility(View.GONE);
		((LinearLayout)this.findViewById(R.id.linearlayout_base_gyoumu_idou)).setVisibility(View.GONE);
		((LinearLayout)this.findViewById(R.id.linearlayout_base_kyukei)).setVisibility(View.GONE);
		((LinearLayout)this.findViewById(R.id.linearlayout_base_koutsuuhi)).setVisibility(View.GONE);
		((LinearLayout)this.findViewById(R.id.linearlayout_base_syorimaisuu)).setVisibility(View.GONE);
		((LinearLayout)this.findViewById(R.id.linearlayout_base_moushidemaisuu)).setVisibility(View.GONE);

		((TextView)this.findViewById(R.id.textview_completed_name)).setText(extras.getString(NAME));
		((TextView)this.findViewById(R.id.textview_completed_date)).setText(extras.getString(DATE));
		((TextView)this.findViewById(R.id.textview_completed_time)).setText(extras.getString(TIME));
	}

	private void taikinInit(Bundle extras) {
		((LinearLayout)this.findViewById(R.id.linearlayout_base_syubetsu_idou)).setVisibility(View.GONE);
		((LinearLayout)this.findViewById(R.id.linearlayout_base_gyoumu_idou)).setVisibility(View.GONE);

		((TextView)this.findViewById(R.id.textview_completed_name)).setText(extras.getString(NAME));
		((TextView)this.findViewById(R.id.textview_completed_date)).setText(extras.getString(DATE));
		((TextView)this.findViewById(R.id.textview_completed_time)).setText(extras.getString(TIME));
		((TextView)this.findViewById(R.id.textview_completed_syubetsu)).setText(extras.getString(SYUBETSU));
		((TextView)this.findViewById(R.id.textview_completed_kyukei)).setText(extras.getString(KYUKEI));
		((TextView)this.findViewById(R.id.textview_completed_gyoumu)).setText(extras.getString(GYOUMU));
		((TextView)this.findViewById(R.id.textview_completed_koutsuuhi)).setText(extras.getString(KOUTSUUHI));
		((TextView)this.findViewById(R.id.textview_completed_syorimaisuu)).setText(extras.getString(SYORIMAISUU));
		((TextView)this.findViewById(R.id.textview_completed_moushidemaisuu)).setText(extras.getString(MOUSHIDEMAISUU));
	}

	private void idouInit(Bundle extras) {
		((LinearLayout)this.findViewById(R.id.linearlayout_base_syubetsu)).setVisibility(View.GONE);
		((LinearLayout)this.findViewById(R.id.linearlayout_base_gyoumu)).setVisibility(View.GONE);
		((LinearLayout)this.findViewById(R.id.linearlayout_base_kyukei)).setVisibility(View.GONE);
		((LinearLayout)this.findViewById(R.id.linearlayout_base_koutsuuhi)).setVisibility(View.GONE);

		((TextView)this.findViewById(R.id.textview_completed_name)).setText(extras.getString(NAME));
		((TextView)this.findViewById(R.id.textview_completed_date)).setText(extras.getString(DATE));
		((TextView)this.findViewById(R.id.textview_completed_time)).setText(extras.getString(TIME));
		((TextView)this.findViewById(R.id.textview_completed_syubetsu_idou)).setText(extras.getString(SYUBETSU));
		((TextView)this.findViewById(R.id.textview_completed_gyoumu_idou)).setText(extras.getString(GYOUMU));
		((TextView)this.findViewById(R.id.textview_completed_syorimaisuu)).setText(extras.getString(SYORIMAISUU));
		((TextView)this.findViewById(R.id.textview_completed_moushidemaisuu)).setText(extras.getString(MOUSHIDEMAISUU));
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(event.getAction() == MotionEvent.ACTION_UP) {
			// Topへ
			Intent topIntent = new Intent(this, Main.class);
			topIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
			this.startActivity(topIntent);
		}

		return super.onTouchEvent(event);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		this.handler.removeCallbacksAndMessages(null);	// 手動で閉じた場合はキャンセルする
	}

	@Override
	public void onBackPressed() {
		/*
		 * Backキー無効化
		 */
	}

}
