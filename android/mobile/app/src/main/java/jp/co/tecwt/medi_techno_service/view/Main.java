package jp.co.tecwt.medi_techno_service.view;

import java.io.File;
import java.util.Calendar;
import java.util.Timer;

import jp.co.tecwt.medi_techno_service.R;
import jp.co.tecwt.medi_techno_service.data.DialogHolder;
import jp.co.tecwt.medi_techno_service.data.MyPreference;
import jp.co.tecwt.medi_techno_service.data.Singleton;
import jp.co.tecwt.medi_techno_service.dialog.MyDialog;
import jp.co.tecwt.medi_techno_service.dialog.TextDialog;
import jp.co.tecwt.medi_techno_service.model.AppNewVersionChecker;
import jp.co.tecwt.medi_techno_service.util.Constant;
import jp.co.tecwt.medi_techno_service.util.MyHttpClient2;
import jp.co.tecwt.medi_techno_service.util.MyHttpClient3_Download;
import jp.co.tecwt.medi_techno_service.util.MyTimerTask;
import jp.co.tecwt.medi_techno_service.util.NextAct;
import jp.co.tecwt.medi_techno_service.util.Util;
import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * Top画面。
 */
@SuppressLint("DefaultLocale")
public class Main extends BaseActivity {

	public static final int SYUKKIN = 1;
	public static final int TAIKIN = 2;
	public static final int IDOU = 3;

	private TextView txt_date;
	private TextView txt_time;
	private Timer timer;

	private LocationManager locationManager;
	private boolean isGPSLoading;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 初期設定済みか判定
		boolean appinit = MyPreference.getInstance(this).getAppInit();
		if(!appinit) {

			// 初期設定画面へ
			Intent appInitIntent = new Intent(this, AppInitActivity.class);
			this.startActivity(appInitIntent);
			this.finish();
			return;
		}

		// 更新後の初期化済みか判定
		MyPreference pref = MyPreference.getInstance(this);
		if (!pref.getApplicationUpdateInitialized() &&
				pref.getApplicationPreviousVersion() != Util.GetApplicationVersionCode(this)) {

			/*
			 * 更新内容
			 */
			pref.writeLateTaikinConfirmTime(1730);	//確認時間を17:30に変更

			pref.writeApplicationUpdateInitialized(true);	//更新完了
		}

		// GPS取得準備
		this.locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
	}

	/**
	 * GPSリスナー
	 */
	private LocationListener locationListener = new LocationListener() {
		@Override
		public void onLocationChanged(Location location) {
			Singleton.INSTANCE.setLat(location.getLatitude());
			Singleton.INSTANCE.setLng(location.getLongitude());
		}
		@Override
		public void onProviderEnabled(String provider) {}
		@Override
		public void onProviderDisabled(String provider) {}
		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {}
	};

	/**
	 * 終了
	 */
	private OnClickListener applicationFinishBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			Main.this.finish();
		}
	};

	/**
	 * 履歴
	 */
	private OnClickListener logBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			Intent intent = new Intent(Main.this, LogActivity.class);
			Main.this.startActivity(intent);
		}
	};

	/**
	 * 管理設定
	 */
	private OnClickListener adminBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			showPassDialog();
		}
		private void showPassDialog() {
			// 多重起動阻止
			String DIALOG_NAME = this.getClass().getName() + "_showPassDialog";;
			if(Main.this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
				return;
			}
			DialogHolder.INSTANCE.init();
			// パスワード認証ダイアログ表示
			TextDialog dialog = new TextDialog();
			dialog.setTitle("パスワードを入力");
			dialog.setPositiveText("確認");
			dialog.setNegativeText("キャンセル");
			dialog.setCallback(new TextDialog.Callback() {

				@Override
				public void onResult(String result) {
					MyPreference pref = new MyPreference(Main.this);
					String pass = pref.getPassword();
					if(result.equals(pass)) {
						Intent intent = new Intent(Main.this, AdminActivity.class);
						Main.this.startActivity(intent);
					} else {
						showMessageDialog("エラー", "パスワードが違います", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								showPassDialog();
							}
						});
					}
				}

				@Override
				public void onCancel() {}
			});
			dialog.show(Main.this.getFragmentManager(), DIALOG_NAME);
		}
	};

	/**
	 * 出
	 */
	private OnClickListener syukkinBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {

			Singleton.INSTANCE.setSyuttai("出勤");
			Singleton.INSTANCE.setSyuttaiStatus(Main.SYUKKIN);

			Intent nextIntent = NextAct.getNextIntent(Main.this, Main.SYUKKIN);
			Main.this.startActivity(nextIntent);
		}
	};

	/**
	 * 移動
	 */
	private OnClickListener idouBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {

			Singleton.INSTANCE.setSyuttai("移動");
			Singleton.INSTANCE.setSyuttaiStatus(Main.IDOU);

			Intent nextIntent = NextAct.getNextIntent(Main.this, Main.IDOU);
			Main.this.startActivity(nextIntent);
		}
	};

	/**
	 * 退
	 */
	private OnClickListener taikinBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {

			Singleton.INSTANCE.setSyuttai("退勤");
			Singleton.INSTANCE.setSyuttaiStatus(Main.TAIKIN);

			Intent nextIntent = new Intent(Main.this, WorkListActivity.class);
			Main.this.startActivity(nextIntent);
		}
	};


	@Override
	public void onResume() {
		super.onResume();

		// 縦横でレイアウト変更
		this.layoutInit();

		// 初期化
		Singleton.INSTANCE.init();
		NextAct.init();

		// 初期化されても端末IDは常に記載しておく
		Singleton.INSTANCE.setAppInit_DeviceID(MyPreference.getInstance(this).getDeviceID());

		// プリファレンス
		MyPreference pref = new MyPreference(this);

		// 各種利用チェック項目
		boolean check = true;

		// NFC
		if(check && !NfcAdapter.getDefaultAdapter(this).isEnabled()) {
			// OFF
			// 設定画面を開く
			Intent settingIntent = new Intent(Settings.ACTION_AIRPLANE_MODE_SETTINGS);
			this.showSettingsDialog(
					"NFCが利用できない設定となっています。このままではアプリを使用できません。\n設定画面を開きますか？", settingIntent);
			check = false;
		}

		// GPS
		if(check && !this.locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			// OFF
			// 設定画面を開く
			Intent settingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			this.showSettingsDialog(
					"位置情報の取得ができない設定となっています。このままではアプリを使用できません。\n設定画面を開きますか？", settingIntent);
			check = false;
		} else if(check) {
			// ON
			// GPS取得開始
			if(this.locationManager != null && this.isGPSLoading == false) {
				this.locationManager.requestLocationUpdates(
						LocationManager.GPS_PROVIDER, 0, 0, this.locationListener);
				this.locationManager.requestLocationUpdates(
						LocationManager.NETWORK_PROVIDER, 0, 0, this.locationListener);
				this.isGPSLoading = true;
			}
		}

		// INTERNET
		ConnectivityManager cm =  (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = cm.getActiveNetworkInfo();
		if(check && (info == null || !info.isConnectedOrConnecting())) {
			// OFF
			// 設定画面を開く
			Intent settingIntent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
			this.showSettingsDialog(
					"インターネット通信ができない設定となっています。このままではアプリを使用できません。\n設定画面を開きますか？", settingIntent);
			check = false;
		}

		// AppVersion
		if(check && pref.getUpdateCheckFlag()) {
			checkAppVersion();
		}

		if(this.timer == null) {
			// 時間取得開始
			this.timer = new Timer();
			MyTimerTask task = new MyTimerTask(new MyTimerTask.TimerListener() {
				@Override
				public void update(int[] datetimes) {
					String date = String.format(
							"%d年 %d月%d日 (%s)",
							datetimes[MyTimerTask.y],
							datetimes[MyTimerTask.M],
							datetimes[MyTimerTask.d],
							MyTimerTask.WEEKS[datetimes[MyTimerTask.w]]);
					String time = String.format(
							"%02d : %02d",
							datetimes[MyTimerTask.k],
							datetimes[MyTimerTask.m]);
					Main.this.txt_date.setText(date);
					Main.this.txt_time.setText(time);
				}
			});
			this.timer.schedule(task, 0, 1000);	// 0ミリ秒後実行、以後1000ミリ秒毎に実行
		}
	}

	private void checkAppVersion() {
		// 多重起動阻止
		final String DIALOG_NAME = this.getClass().getName() + "_checkAppVersion";
		if(this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
			return;
		}
		final MyPreference pref = new MyPreference(this);
		Calendar calLast = Calendar.getInstance(Constant.TIMEZONE, Constant.LOCALE);
		calLast.setTimeInMillis(pref.getUpdateCheckDate());
		int lastYMD = calLast.get(Calendar.YEAR)*10000 + (calLast.get(Calendar.MONTH)+1)*100 + calLast.get(Calendar.DAY_OF_MONTH);
		Calendar calNow = Calendar.getInstance(Constant.TIMEZONE, Constant.LOCALE);
		int nowYMD = calNow.get(Calendar.YEAR)*10000 + (calNow.get(Calendar.MONTH)+1)*100 + calNow.get(Calendar.DAY_OF_MONTH);
		if(lastYMD >= nowYMD) {
			return;//１日経過していなければ無視
		}
		final AppNewVersionChecker checker = new AppNewVersionChecker(this);
		checker.connect(new MyHttpClient2.Responser() {

			@Override
			public void onFailed() {}

			@Override
			public void onError(String errorcode, String message) {}

			@Override
			public void onCompleted(String json) {
				pref.writeUpdateCheckDate(System.currentTimeMillis());//確認日付を記録
				if(checker.isExistNewVersion()) {
					// 最新版ダウンロード確認
					MyDialog dialog = new MyDialog();
					dialog.setMode(MyDialog.MODE_NORMAL);
					dialog.setTitle("CAUTION!!");
					dialog.setPositive_button("はい", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							// 最新版ダウンロード開始
							checker.download(new MyHttpClient3_Download.Responser() {

								@Override
								public void onFailed() {
									// 失敗
									Main.this.showErrorDialog(null, null);
								}

								@Override
								public void onError(String errorcode, String message) {
									// 人為的失敗
									Main.this.showErrorDialog(errorcode, message);
								}

								@Override
								public void onCompleted(File tmpApkFile) {
									// 成功
									// インストーラ起動
									Intent intent = new Intent(Intent.ACTION_VIEW);
									Uri dataUri = Uri.fromFile(tmpApkFile);
									intent.setDataAndType(dataUri, "application/vnd.android.package-archive");
									startActivity(intent);
								}
							}, "ダウンロード中...");
						}
					});
					if(checker.isEditable()) {
						dialog.setMessage("アプリの最新版がアップロードされています。\nダウンロードを開始しますか？");
						dialog.setNegative_button("キャンセル", null);
					} else {
						dialog.setMessage("アプリの最新版がアップロードされています。\n"
								+ "このアップデートには重要な変更が含まれているため、現在のバージョンのままでは利用できません。\n"
								+ "「はい」を選択してダウンロードを開始して下さい。");
					}
					dialog.setCancelable(false);
					dialog.show(getFragmentManager(), DIALOG_NAME);
				}
			}
		});
	}

	private void showSettingsDialog(String message, final Intent intent) {

		// 多重起動阻止
		String DIALOG_NAME = this.getClass().getName();
		if(this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
			return;
		}

		// ダイアログ表示
		MyDialog dialog = new MyDialog();
		dialog.setMode(MyDialog.MODE_NORMAL);
		dialog.setTitle("警告");
		dialog.setMessage(message);
		dialog.setCancelable(false);
		dialog.setPositive_button(
				"開く",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Main.this.startActivity(intent);
					}
				});
		dialog.setNegative_button(
				"アプリを終了する",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Main.this.finish();
					}
				});
		dialog.show(this.getFragmentManager(), DIALOG_NAME);
	}

	private void showMessageDialog(String title, String message, DialogInterface.OnClickListener okListener) {
		MyDialog dialog = new MyDialog();
		dialog.setMode(MyDialog.MODE_NORMAL);
		dialog.setTitle(title);
		dialog.setMessage(message);
		dialog.setPositive_button("OK", okListener);
		dialog.setCancelable(true);
		FragmentManager manager = null;
		try {
			manager = this.getFragmentManager();
			dialog.show(manager, null);
		} catch (Exception ex) {
			ex.printStackTrace();
			//okListener.onClick(null, 0);
		}
	}

	/**
	 * エラーダイアログ表示
	 * @param errorcode
	 * @param message
	 */
	private void showErrorDialog(String errorcode, String message) {

		// 多重起動阻止
		String DIALOG_NAME = this.getClass().getName();
		if(this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
			return;
		}

		String dialogMessage;
		if(errorcode == null && message == null) {
			dialogMessage = "エラーが発生しました。お手数ですが再度操作を行ってください。";
		}else {
			dialogMessage = "エラーが発生しました。お手数ですが再度操作を行ってください。"+"\n―――"+"\nerrorcode: "+errorcode+"\nmessage: "+message;
		}
		MyDialog dialog = new MyDialog();
		dialog.setMode(MyDialog.MODE_NORMAL);
		dialog.setTitle("ERROR!!");
		dialog.setMessage(dialogMessage);
		dialog.setPositive_button("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				//Intent topIntent = new Intent(AdminActivity.this, Main.class);
				//topIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
				//AdminActivity.this.startActivity(topIntent);
			}
		});
		dialog.setCancelable(false);
		dialog.show(this.getFragmentManager(), DIALOG_NAME);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		this.layoutInit();
	}

	private void layoutInit() {

		// 縦横でレイアウト変更
		Configuration config = this.getResources().getConfiguration();
		if(config.orientation == Configuration.ORIENTATION_PORTRAIT) {
			// 縦
			this.setContentView(R.layout.activity_main_portrait);
		}else {
			// 横
			this.setContentView(R.layout.activity_main);
		}

		this.txt_date = (TextView)this.findViewById(R.id.textview_date);
		this.txt_time = (TextView)this.findViewById(R.id.textview_time);

		// リスナー登録
		Button application_finish = (Button)this.findViewById(R.id.button_application_finish);
		application_finish.setOnClickListener(this.applicationFinishBtn);
		Button log = (Button)this.findViewById(R.id.button_log);
		log.setOnClickListener(this.logBtn);
		Button admin = (Button)this.findViewById(R.id.button_admin);
		admin.setOnClickListener(this.adminBtn);
		Button syukkin = (Button)this.findViewById(R.id.button_syukkin);
		syukkin.setOnClickListener(this.syukkinBtn);
		Button idou = (Button)this.findViewById(R.id.button_idou);
		idou.setOnClickListener(this.idouBtn);
		Button taikin = (Button)this.findViewById(R.id.button_taikin);
		taikin.setOnClickListener(this.taikinBtn);

		// フォント救済
		Typeface font = Typeface.createFromAsset(getAssets(), "MTLmr3m.ttf");
		this.txt_date.setTypeface(font);
		//this.txt_time.setTypeface(font);
		application_finish.setTypeface(font);
		log.setTypeface(font);
		admin.setTypeface(font);
		syukkin.setTypeface(font);
		idou.setTypeface(font);
		taikin.setTypeface(font);

		// 移動可否
		MyPreference pref = new MyPreference(this);
		if(pref.getIdouFlag()) {
			this.findViewById(R.id.button_idou).setVisibility(View.VISIBLE);
		} else {
			this.findViewById(R.id.button_idou).setVisibility(View.GONE);
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		// タイマー停止
		if(this.timer != null) {
			this.timer.cancel();
			this.timer = null;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		// GPS取得停止
		if(this.locationManager != null) {
			this.locationManager.removeUpdates(this.locationListener);
			this.locationManager = null;
		}
	}

}
