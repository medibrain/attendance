package jp.co.tecwt.medi_techno_service.view;

import java.util.ArrayList;

import jp.co.tecwt.medi_techno_service.R;
import jp.co.tecwt.medi_techno_service.data.LogDB;
import jp.co.tecwt.medi_techno_service.data.Singleton;
import jp.co.tecwt.medi_techno_service.dialog.MyDialog;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 履歴画面。
 */
@SuppressLint("InflateParams")
public class LogActivity extends BaseActivity {

	private ExpandableListView exListview;
	private LogListAdapter adapter;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// レイアウトファイル適用
		setContentView(R.layout.activity_log);

		// リスナー登録
		((Button)this.findViewById(R.id.button_back)).setOnClickListener(this.backBtn);
		((Button)this.findViewById(R.id.button_top)).setOnClickListener(this.topBtn);
		((Button)this.findViewById(R.id.button_delete)).setOnClickListener(this.deleteBtn);

		// アダプター登録
		this.exListview = (ExpandableListView)this.findViewById(R.id.expandablelistview_log);
		this.adapter = new LogListAdapter(this);
		this.exListview.setAdapter(this.adapter);
		this.exListview.setEmptyView(this.findViewById(R.id.textview_nodata));
		this.exListview.setOnChildClickListener(childClick);

		// 最新日時の項目を開いた状態にしておく
		this.exListview.performItemClick(getCurrentFocus(), 0, 0);
	}

	/**
	 * 履歴削除ボタン
	 */
	private OnClickListener deleteBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			MyDialog dialog = new MyDialog();
			if(Singleton.INSTANCE.getLog_DeleteDateSize() != 0) {
				final String[] dates = Singleton.INSTANCE.getLog_DeleteDateArray();
				dialog.setTitle("確認");
				dialog.setMessage("チェックが入れられている " + dates.length + " 件の日付のデータを削除します。\n本当によろしいですか？\n\n※削除したデータは復元することができません。\n※サーバ上のデータは削除されません。");
				dialog.setPositive_button("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// 削除
						SQLiteDatabase db = new LogDB(LogActivity.this).getWritableDatabase();
						StringBuilder where = new StringBuilder();
						for (int i = 0; i < dates.length; i++) {
							if (i > 0) where.append(" OR ");
							where.append("DATE(").append(LogDB.LOG_DATETIME_EDIT).append(") = ?");
						}
						db.delete(LogDB.TABLE_LOG, where.toString(), dates);
						db.close();
						// 描画更新(再取得)
						LogActivity.this.adapter = new LogActivity.LogListAdapter(LogActivity.this);
						LogActivity.this.exListview.setAdapter(LogActivity.this.adapter);
						LogActivity.this.exListview.performItemClick(getCurrentFocus(), 0, 0);
						// トースト表示
						Toast.makeText(LogActivity.this, dates.length+"件 削除しました。", Toast.LENGTH_SHORT).show();
						Singleton.INSTANCE.clear_DeleteDates();
					}
				});
				dialog.setNegative_button("キャンセル", null);
			} else {
				dialog.setMessage("削除したい日付を選択して下さい。");
				dialog.setPositive_button("OK", null);
			}
			dialog.show(LogActivity.this.getFragmentManager(), null);
		}
	};

	/**
	 * タイムカードをクリック
	 */
	private OnChildClickListener childClick = new OnChildClickListener() {
		@Override
		public boolean onChildClick(ExpandableListView parent, View view,
				int groupPosition, int childPosition, long id) {
			LogData item = (LogData)LogActivity.this.adapter.getChild(groupPosition, childPosition);
			MyDialog dialog = new MyDialog();
			dialog.setTitle("詳細情報");
			String syuttai = "";
			if(item.syuttaiStatus == Main.SYUKKIN) syuttai = "出勤";
			if(item.syuttaiStatus == Main.IDOU) syuttai = "移動";
			if(item.syuttaiStatus == Main.TAIKIN) syuttai = "退勤";
			String text =
					"勤怠： "+syuttai+"\n"
					+ "指定日時： "+item.datetime_edit_time_full+"\n"
					+ "実際日時： "+item.datetime_auto_time_full+"\n"
					+ "----\n";
			if(item.syubetsu != null) text += "勤務種別： "+item.syubetsu+"\n";
			if(item.note != null) text += "業務内容： "+item.note+"\n";
			if(item.breaktime != null) text += "休憩時間： "+item.breaktime+"\n";
			if(item.fare != null) text += "交通費： "+item.fare+"\n";
			if(item.wcount != null) text += "処理数： "+item.wcount+"\n";
			if(item.rcount != null) text += "申出数： "+item.rcount+"\n";
			dialog.setMessage(text);
			dialog.setPositive_button("閉じる", null);
			dialog.show(LogActivity.this.getFragmentManager(), null);
			return false;
		}
	};


	/**
	 * 各種履歴を表示するアダプター
	 */
	private class LogListAdapter extends BaseExpandableListAdapter {

		private Context context;
		private ArrayList<String> unixtimes;
	    private ArrayList<ArrayList<LogData>> logs;

		public LogListAdapter(Context context) {
			this.context = context;
			this.unixtimes = new ArrayList<String>();
			this.logs = new ArrayList<ArrayList<LogData>>();
			this.init();
		}

		private void init() {
			this.setDays();
			this.setLogs();
		}

		private void setDays() {
			SQLiteDatabase db = new LogDB(this.context).getReadableDatabase();
			String dateMethod = "DATE("+LogDB.LOG_DATETIME_EDIT+")";	// SQLite:date関数
			Cursor cursor = db.query(
					LogDB.TABLE_LOG, 					// テーブル名
					new String[]{dateMethod}, 			// [SELECT] 日付のみ
					null, 								// [WHERE] 全行
					null, 								// [WHEREの?に入る値]
					dateMethod, 						// [GROUP BY] 日付
					null, 								// [HAVING]
					LogDB.LOG_DATETIME_EDIT + " DESC"); // [ORDER BY] 入力時刻降順

			if(cursor.moveToFirst()) {
				String unixtime = "";
				do {
					// 親Viewに入れる
					unixtime = cursor.getString(0);
					this.unixtimes.add(unixtime);
				} while(cursor.moveToNext());
			}
			cursor.close();
		}

		private void setLogs() {

			// 子Viewの器を作成
			for(int i=0; i<this.unixtimes.size(); i++) {
				this.logs.add(new ArrayList<LogData>());
			}

			SQLiteDatabase db = new LogDB(this.context).getReadableDatabase();
			String DATE = "date";
			String TIME = "time";
			String dateMethod = "DATE("+LogDB.LOG_DATETIME_EDIT+") as "+DATE;	// SQLite:date関数
			String timeMethod = "TIME("+LogDB.LOG_DATETIME_EDIT+") as "+TIME;	// SQLite:time関数
			String[] select = new String[] {
					"*",
					dateMethod,
					timeMethod,
				};
			Cursor cursor = db.query(
					LogDB.TABLE_LOG, 													// テーブル名
					select, 															// [SELECT] 全項目
					null, 																// [WHERE] 全行
					null, 																// [WHEREの?に入る値]
					null, 																// [GROUP BY]
					null, 																// [HAVING]
					LogDB.LOG_DATETIME_EDIT + " DESC, " + LogDB.LOG_PRIMARY + " DESC"); // [ORDER BY] 入力時刻降順、ID降順

			if(cursor.moveToFirst()) {
				String date;
				String time;
				LogData logData;
				do {
					date = cursor.getString(cursor.getColumnIndexOrThrow(DATE));
					for(int i=0; i<this.unixtimes.size(); i++) {
						if(date.equals(this.unixtimes.get(i))) {
							logData = new LogData();
							logData.name = cursor.getString(cursor.getColumnIndexOrThrow(LogDB.LOG_NAME));
							time = cursor.getString(cursor.getColumnIndexOrThrow(TIME));
							logData.datetime_edit_time = time.substring(0, 5);	// 00:00の部分抜き出し
							logData.datetime_edit_time_full = cursor.getString(cursor.getColumnIndexOrThrow(LogDB.LOG_DATETIME_EDIT));
							logData.datetime_auto_time_full = cursor.getString(cursor.getColumnIndexOrThrow(LogDB.LOG_DATETIME_AUTO));
							logData.syuttaiStatus = cursor.getInt(cursor.getColumnIndexOrThrow(LogDB.LOG_SYUTTAISTATUS));
							logData.syubetsu = cursor.getString(cursor.getColumnIndexOrThrow(LogDB.LOG_SYUBETSU));
							logData.note = cursor.getString(cursor.getColumnIndexOrThrow(LogDB.LOG_GYOUMU));
							logData.breaktime = cursor.getString(cursor.getColumnIndexOrThrow(LogDB.LOG_KYUKEI));
							logData.fare = cursor.getString(cursor.getColumnIndexOrThrow(LogDB.LOG_KOUTSUUHI));
							logData.wcount = cursor.getString(cursor.getColumnIndexOrThrow(LogDB.LOG_SYORIMAISUU));
							logData.rcount = cursor.getString(cursor.getColumnIndexOrThrow(LogDB.LOG_MOUSHIDEMAISUU));
							this.logs.get(i).add(logData);
							break;
						}
					}
				} while(cursor.moveToNext());
			}
			cursor.close();
		}

		/**
		 * 親Viewを返す
		 */
		@Override
		public View getGroupView(final int arg0, boolean arg1, View arg2, ViewGroup arg3) {
			if(arg2 == null) {
				arg2 = LayoutInflater.from(this.context).inflate(R.layout.item_log_separator, null);
			}

			final String date = this.getGroup(arg0).toString();
			((TextView)arg2.findViewById(R.id.textview_separator)).setText(date);
			boolean checked = Singleton.INSTANCE.getLog_DeleteDate(arg0) != null;
			((CheckBox)arg2.findViewById(R.id.checkbox_delete_check)).setChecked(checked);
			((CheckBox)arg2.findViewById(R.id.checkbox_delete_check)).setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					boolean isCheck = ((CheckBox)v).isChecked();
					if(isCheck) {
						// checkしたのであれば削除リストに追加
						Singleton.INSTANCE.addLog_DeleteDate(arg0, date);
					} else {
						// checkを外したのであれば削除リストから削除
						Singleton.INSTANCE.removeLog_DeleteDate(arg0);
					}
				}
			});
			return arg2;
		}

		/**
		 * 子Viewを返す
		 */
		@Override
		public View getChildView(int arg0, int arg1, boolean arg2, View arg3, ViewGroup arg4) {
			if(arg3 == null) {
				arg3 = LayoutInflater.from(this.context).inflate(R.layout.item_log2, null);
			}

			LogData logData = (LogData)this.getChild(arg0, arg1);

			// 値反映
			((TextView)arg3.findViewById(R.id.textview_name)).setText(logData.name);
			((TextView)arg3.findViewById(R.id.textview_datetime)).setText(logData.datetime_edit_time);
			((TextView)arg3.findViewById(R.id.textview_syubetsu)).setText(logData.syubetsu);
			switch(logData.syuttaiStatus) {
			case Main.SYUKKIN:
				((TextView)arg3.findViewById(R.id.textview_syuttai)).setText("出勤");
				break;
			case Main.TAIKIN:
				((TextView)arg3.findViewById(R.id.textview_syuttai)).setText("退勤");
				break;
			case Main.IDOU:
				((TextView)arg3.findViewById(R.id.textview_syuttai)).setText("移動");
				break;
			}

			// イベントハンドラ


			return arg3;
		}

		@Override
		public Object getChild(int arg0, int arg1) {
			return this.logs.get(arg0).get(arg1);
		}
		@Override
		public long getChildId(int arg0, int arg1) {
			return arg1;
		}
		@Override
		public int getChildrenCount(int arg0) {
			return this.logs.get(arg0).size();
		}
		@Override
		public Object getGroup(int arg0) {
			return this.unixtimes.get(arg0);
		}
		@Override
		public int getGroupCount() {
			return this.unixtimes.size();
		}
		@Override
		public long getGroupId(int arg0) {
			return arg0;
		}
		@Override
		public boolean hasStableIds() {
			return true;
		}
		@Override
		public boolean isChildSelectable(int arg0, int arg1) {
			return true;
		}

	}

	private class LogData {
		public String name;
		public String datetime_edit_time;
		public String datetime_edit_time_full;
		public String datetime_auto_time_full;
		public int syuttaiStatus;
		public String syubetsu;
		public String note;
		public String breaktime;
		public String fare;
		public String wcount;
		public String rcount;
	}
}
