package jp.co.tecwt.medi_techno_service.util;

import java.util.Calendar;

import android.text.format.DateFormat;

public class DateTime {

	public static Calendar GetToday() {
		return Calendar.getInstance(Constant.TIMEZONE, Constant.LOCALE);
	}

	public static String GetYYYYMMDD(Calendar cal) {
		return DateFormat.format("yyyyMMdd", cal.getTime()).toString();
	}
}
