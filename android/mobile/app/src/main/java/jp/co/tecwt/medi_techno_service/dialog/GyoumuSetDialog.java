package jp.co.tecwt.medi_techno_service.dialog;

import jp.co.tecwt.medi_techno_service.R;
import jp.co.tecwt.medi_techno_service.data.DialogHolder;
import jp.co.tecwt.medi_techno_service.data.MyPreference;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

@SuppressLint("InflateParams")
public class GyoumuSetDialog extends DialogFragment {

	private static final int VALUE_SOSHIKI_ID = 0;

	private TextSetListener listener;
	private Callback callback;
	private View view;
	private int number;

	private MyPreference pref;


	/**
	 * 変更確定コールバック。
	 */
	public interface Callback {
		public void callback(String result);
	}

	public GyoumuSetDialog() {}

	public GyoumuSetDialog(int type) {
		DialogHolder.INSTANCE.setMode(type);
	}

	/**
	 * 変更確定のコールバックを登録する。
	 * @param callback
	 * @return
	 */
	public GyoumuSetDialog setCallback(Callback callback) {
		DialogHolder.INSTANCE.setCallback(callback);
		return this;
	}


	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		this.pref = MyPreference.getInstance(this.getActivity());

		// 表示するView
		this.view = LayoutInflater.from(this.getActivity()).inflate(R.layout.dialog_textset_add, null);

		// リスナー復帰
		Object[] values = DialogHolder.INSTANCE.getValues();
		if(values == null) {
			// 新規
			this.listener = new TextSetListener(view);
		}else {
			// 復帰
			this.listener = (TextSetListener)DialogHolder.INSTANCE.getPositibeListener();
		}
		this.callback = (Callback)DialogHolder.INSTANCE.getCallback();

		// Spinner
		Spinner spinner = (Spinner)this.view.findViewById(R.id.spinner_textset_space);
		String[] textsets = null;
		switch (DialogHolder.INSTANCE.getMode()) {
			case MyPreference.GYOUMU_TYPE_NORMAL:
				textsets = new String[MyPreference.KEY_TEXTSETS.length];
				break;
			case MyPreference.GYOUMU_TYPE_SELECT:
				textsets = new String[MyPreference.KEY_TEXTSETS_SELECT.length];
				break;
			default:
				break;
		}
		String text;
		for(int i=0; i<textsets.length; i++) {
			text = this.pref.getTextSet(i+1, DialogHolder.INSTANCE.getMode());	// 全登録テンプレートを取得
			textsets[i] = (i+1)+". "+text;	// 例）1. abc
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, textsets);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);	// Spinnerに設定
		spinner.setOnItemSelectedListener(this.itemSelectedListener);

		// EditText
		EditText edit = (EditText)this.view.findViewById(R.id.edittext_text);
		edit.setOnKeyListener(this.TextSetKeyListener);

		// 実際に表示するダイアログ
		AlertDialog.Builder dialog = new AlertDialog.Builder(this.getActivity());
		dialog.setTitle("テンプレートの追加");
		dialog.setView(view);
		dialog.setPositiveButton("追加", this.listener);
		dialog.setNegativeButton("キャンセル", null);
		return dialog.create();
	}

	/**
	 * Spinner選択時
	 */
	private OnItemSelectedListener itemSelectedListener = new OnItemSelectedListener() {
		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			// 選択位置を記録
			GyoumuSetDialog.this.number = position+1;

			// テンプレートをEditTextに反映
			String text = GyoumuSetDialog.this.pref.getTextSet(GyoumuSetDialog.this.number, DialogHolder.INSTANCE.getMode());
			((EditText)GyoumuSetDialog.this.view.findViewById(R.id.edittext_text)).setText(text);
		}
		@Override
		public void onNothingSelected(AdapterView<?> parent) {}
	};

	/**
	 * Viewのウィジェットリスナー。
	 */
	private class TextSetListener implements DialogInterface.OnClickListener {

		private View view;

		public TextSetListener(View view) {
			this.view = view;
		}

		@Override
		public void onClick(DialogInterface dialog, int witch) {
			String text = ((EditText)this.view.findViewById(R.id.edittext_text)).getText().toString();

			MyPreference pref = MyPreference.getInstance(GyoumuSetDialog.this.getActivity());

			if(text.equals("")) {
				// 無記入なら削除
				pref.writeTextSet(GyoumuSetDialog.this.number, "", DialogHolder.INSTANCE.getMode());
			} else {
				// 業務内容テンプレート保存
				pref.writeTextSet(GyoumuSetDialog.this.number, text, DialogHolder.INSTANCE.getMode());
			}

			// コールバックする
			if(GyoumuSetDialog.this.callback != null) {
				GyoumuSetDialog.this.callback.callback(text);
			}

			DialogHolder.INSTANCE.init();
		}
	}


	/**
	 * 入力監視。
	 */
	private OnKeyListener TextSetKeyListener = new OnKeyListener() {
		@Override
		public boolean onKey(View view, int keyCode, KeyEvent event) {
			if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
				// 改行させない
				InputMethodManager imm = (InputMethodManager)GyoumuSetDialog.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				return true;
			}
			return false;
		}
	};


	@Override
	public void onPause() {
		super.onPause();

		// 値の保存
		Object[] values = new Object[1];
		values[VALUE_SOSHIKI_ID] = ((EditText)this.view.findViewById(R.id.edittext_text)).getText().toString();
		DialogHolder.INSTANCE.setValues(values);
		DialogHolder.INSTANCE.setCallback(this.callback);
		DialogHolder.INSTANCE.setPositiveListener(this.listener);
		DialogHolder.INSTANCE.setNumber(this.number);
	}

}