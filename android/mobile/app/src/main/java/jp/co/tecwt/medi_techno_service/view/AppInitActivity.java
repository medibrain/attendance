package jp.co.tecwt.medi_techno_service.view;

import jp.co.tecwt.medi_techno_service.R;
import jp.co.tecwt.medi_techno_service.data.MyPreference;
import jp.co.tecwt.medi_techno_service.data.Singleton;
import jp.co.tecwt.medi_techno_service.dialog.MyDialog;
import jp.co.tecwt.medi_techno_service.util.MyHttpClient2;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import java.util.UUID;

public class AppInitActivity extends BaseActivity {

	private String deviceID;
	private EditText edit_name;
	private EditText edit_pass;
	private EditText edit_soshikiid;
	private Button edit;

	private boolean exist_name;
	private boolean exist_pass;
	private boolean exist_soshikiid;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 初回IMEは非表示
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		// シリアル番号を取得
		// ※新しいOSバージョンでは取れないのでUUID発行で暫定対処
		this.deviceID = UUID.randomUUID().toString();

		// レイアウトファイル適用
		this.setContentView(R.layout.activity_appinit);

		this.edit_name = (EditText)this.findViewById(R.id.edittext_name);
		this.edit_pass = (EditText)this.findViewById(R.id.edittext_pass);
		this.edit_soshikiid = (EditText)this.findViewById(R.id.edittext_soshikiid);

		// リスナー登録
		this.edit_name.addTextChangedListener(nameWat);
		this.edit_pass.addTextChangedListener(this.passWat);
		this.edit_soshikiid.addTextChangedListener(this.soshikiidWat);
		((Button)this.findViewById(R.id.button_cancel)).setOnClickListener(this.cancelBtn);
		this.edit = (Button)this.findViewById(R.id.button_edit);
		this.edit.setOnClickListener(this.editBtn);
	}


	/**
	 * 端末名の入力監視。
	 */
	private TextWatcher nameWat = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if(s.toString().length() > 0) {
				AppInitActivity.this.exist_name = true;
			}
			AppInitActivity.this.buttonUpdate();
		}
		@Override
		public void afterTextChanged(Editable s) {}
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
	};

	/**
	 * パスワードの入力監視。
	 */
	private TextWatcher passWat = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if(s.toString().length() > 0) {
				AppInitActivity.this.exist_pass = true;
			}
			AppInitActivity.this.buttonUpdate();
		}
		@Override
		public void afterTextChanged(Editable s) {}
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
	};

	/**
	 * 組織IDの入力監視。
	 */
	private TextWatcher soshikiidWat = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			if(s.toString().length() > 0) {
				AppInitActivity.this.exist_soshikiid = true;
			}
			AppInitActivity.this.buttonUpdate();
		}
		@Override
		public void afterTextChanged(Editable s) {}
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
	};

	private void buttonUpdate() {
		if(this.edit_name.getText().toString().length() > 0) {
			this.exist_name = true;
		}else {
			this.exist_name = false;
		}
		if(this.edit_pass.getText().toString().length() > 0) {
			this.exist_pass = true;
		}else {
			this.exist_pass = false;
		}
		if(this.edit_soshikiid.getText().toString().length() > 0) {
			this.exist_soshikiid = true;
		}else {
			this.exist_soshikiid = false;
		}

		if(this.exist_name && this.exist_pass && this.exist_soshikiid) {
			this.edit.setEnabled(true);
		}else {
			this.edit.setEnabled(false);
		}
	}

	/**
	 * やめる
	 */
	private OnClickListener cancelBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			AppInitActivity.this.textClear();
			AppInitActivity.this.finish();
		}
	};

	/**
	 * 登録
	 */
	private OnClickListener editBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {

			// 一時登録
			Singleton.INSTANCE.setAppInit_DeviceName(AppInitActivity.this.edit_name.getText().toString());
			Singleton.INSTANCE.setAppInit_DeviceID(AppInitActivity.this.deviceID);
			Singleton.INSTANCE.setAppInit_Password(AppInitActivity.this.edit_pass.getText().toString());
			Singleton.INSTANCE.setAppInit_SoshikiID(AppInitActivity.this.edit_soshikiid.getText().toString());

			// 送信
			Resources res = AppInitActivity.this.getResources();
			String url = res.getString(R.string.login_url);
			MyHttpClient2 client = new MyHttpClient2(AppInitActivity.this, url, AppInitActivity.this.responser);
			client.setTimeOut(res.getInteger(R.integer.timeout_connect), res.getInteger(R.integer.timeout_read));
			client.enabledProgressDialog(AppInitActivity.this, "通信中", "しばらくお待ちください", null);
			client.addPostParam(res.getString(R.string.login_first_param_name1), res.getString(R.string.login_first_param_value1));
			client.addPostParam(res.getString(R.string.login_first_param_name2), MyHttpClient2.urlEncode(AppInitActivity.this.edit_soshikiid.getText().toString()));
			client.addPostParam(res.getString(R.string.login_first_param_name3), AppInitActivity.this.deviceID);
			client.addPostParam(res.getString(R.string.login_first_param_name4), MyHttpClient2.urlEncode(AppInitActivity.this.edit_name.getText().toString()));
			client.addPostParam(res.getString(R.string.login_first_param_name5), MyHttpClient2.urlEncode(AppInitActivity.this.edit_pass.getText().toString()));
			client.addPostParam(res.getString(R.string.login_first_param_name6), "");
			client.execute();
		}
	};

	/**
	 * レスポンス受け取り
	 */
	private MyHttpClient2.Responser responser = new MyHttpClient2.Responser() {

		@Override
		public void onFailed() {
			// 失敗
			AppInitActivity.this.showErrorDialog();
		}

		@Override
		public void onError(String errorcode, String message) {
			// 人為的失敗
			AppInitActivity.this.showErrorDialog(errorcode, message);
		}

		@Override
		public void onCompleted(String json) {
			// 成功

			// プリファレンスに保存
			MyPreference pref = MyPreference.getInstance(AppInitActivity.this);
			pref.writeSoshikiID(Singleton.INSTANCE.getAppInit_SoshikiID());
			pref.writeDeviceName(Singleton.INSTANCE.getAppInit_DeviceName());
			pref.writeDeviceID(Singleton.INSTANCE.getAppInit_DeviceID());
			pref.writePassword((Singleton.INSTANCE.getAppInit_Password()));
			pref.writeAppInit(MyPreference.APPINIT_STATUS_OK);

			// この端末がスマホなのかタブレットなのかを保存
			Resources r = AppInitActivity.this.getResources();
			Configuration configuration = r.getConfiguration();
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR2) {
				if ((configuration.screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK)
						< Configuration.SCREENLAYOUT_SIZE_LARGE) {
					pref.writeDeviceType("phone");
				} else {
					pref.writeDeviceType("tablet");
				}
			} else {
				if (configuration.smallestScreenWidthDp < 600) {
					pref.writeDeviceType("phone");
				} else {
					pref.writeDeviceType("tablet");
				}
			}

			// Top画面へ
			Intent topIntent = new Intent(AppInitActivity.this, Main.class);
	        AppInitActivity.this.startActivity(topIntent);
	        AppInitActivity.this.finish();

			return;
		}
	};

	private void showErrorDialog() {
		this.showErrorDialog(null, null);
	}

	private void showErrorDialog(String errorcode, String message) {

		// 多重起動阻止
		final String DIALOG_NAME = this.getClass().getName();
		if(this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
			return;
		}

		String dialogMessage;
		if(errorcode == null && message == null) {
			dialogMessage = "エラーが発生しました。お手数ですが再度操作を行ってください。";
		}else {
			dialogMessage = "エラーが発生しました。お手数ですが再度操作を行ってください。"+"\n―――"+"\nerrorcode: "+errorcode+"\nmessage: "+message;
		}
		MyDialog dialog = new MyDialog();
		dialog.setMode(MyDialog.MODE_NORMAL);
		dialog.setTitle("ERROR!!");
		dialog.setMessage(dialogMessage);
		dialog.setPositive_button("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// そのまま入力を続けてもらう
//				AppInitActivity.this.finish();
			}

		});
		dialog.setCancelable(false);
		dialog.show(this.getFragmentManager(), DIALOG_NAME);
	}

	/**
	 * 次回起動時に表示されることがあるため、入力内容を破棄する。
	 */
	private void textClear() {
		Singleton.INSTANCE.init();
		this.edit_name.getEditableText().clear();
		this.edit_pass.getEditableText().clear();
		this.edit_soshikiid.getEditableText().clear();
	}

	@Override
	public void onResume() {
		super.onResume();
		this.edit_name.setText(Singleton.INSTANCE.getAppInit_DeviceName());
		this.edit_pass.setText(Singleton.INSTANCE.getAppInit_Password());
		this.edit_soshikiid.setText(Singleton.INSTANCE.getAppInit_SoshikiID());
		this.buttonUpdate();
	}

	@Override
	public void onPause() {
		super.onPause();
		Singleton.INSTANCE.setAppInit_DeviceName(this.edit_name.getText().toString());
		Singleton.INSTANCE.setAppInit_DeviceID(this.deviceID);
		Singleton.INSTANCE.setAppInit_Password(this.edit_pass.getText().toString());
		Singleton.INSTANCE.setAppInit_SoshikiID(this.edit_soshikiid.getText().toString());
	}

	@Override
	public void onBackPressed() {
		this.textClear();
		super.onBackPressed();
	}

}
