package jp.co.tecwt.medi_techno_service.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import jp.co.tecwt.medi_techno_service.R;
import jp.co.tecwt.medi_techno_service.data.DialogHolder;
import jp.co.tecwt.medi_techno_service.data.MyPreference;
import jp.co.tecwt.medi_techno_service.data.Singleton;
import jp.co.tecwt.medi_techno_service.dialog.MyDialog;
import jp.co.tecwt.medi_techno_service.dialog.TextDialog;
import jp.co.tecwt.medi_techno_service.util.MyHttpClient2;
import jp.co.tecwt.medi_techno_service.util.NextAct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 種別画面。
 */
public class SyubetsuActivity extends BaseActivity {

	private ExpandableListView logexlistview;
	private ExpandableListView exlistview;
	private List<List<Syubetsu>> syubetsuLists;

//	private boolean checkON;
	private boolean isJump;

	private boolean isStatusSearch;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// レイアウトファイル適用
		setContentView(R.layout.activity_syubetsu);

		// ルートに応じて表示する内容を変更
		TextView syubetsuView = (TextView)this.findViewById(R.id.textview_syubetsu_title);
		switch(Singleton.INSTANCE.getSyuttaiStatus()) {
		case Main.SYUKKIN:
			syubetsuView.setText(this.getResources().getString(R.string.syubetsu_syukkin_title));
			break;
		case Main.TAIKIN:
			syubetsuView.setText(this.getResources().getString(R.string.syubetsu_taikin_title));
			break;
		case Main.IDOU:
			syubetsuView.setText(this.getResources().getString(R.string.syubetsu_idou_title));
			break;
		}

		// リスナー登録
		((Button)this.findViewById(R.id.button_back)).setOnClickListener(this.backBtn);
		((Button)this.findViewById(R.id.button_top)).setOnClickListener(this.topBtn);

		// リスト関連
		this.logexlistview = (ExpandableListView)this.findViewById(R.id.expandablelistview_syubetsu_log);
		this.exlistview = (ExpandableListView)this.findViewById(R.id.expandablelistview_syubetsu);
		this.exlistview.setEmptyView(this.findViewById(R.id.textview_nodata));

		// １週間のうちで選択上位５位までの勤務種別を取得
		//this.getSyubetsuLogData();

		// 種別を取得
		this.getSyubetsuData2();

		/*
		 * 必須確認
		 * this.checkON = this.getIntent().getBooleanExtra(NextAct.ISHISSU, true);
		 */

		// 確認画面からのジャンプかを判定
		this.isJump = this.getIntent().getBooleanExtra("isJump", false);

		// 検索モードではない
		isStatusSearch = false;
	}

	/**
	 * 一週間以内に選択された勤務種別ベスト５をリストビューに表示します。
	 */
	/*
	private void getSyubetsuLogData() {
		SQLiteDatabase db = new LogDB(this).getReadableDatabase();
//		「一週間以内に選択された上位５つの場合」は以下を使用する
//		String nowdate = "DATE('now', '+9 hours')";
//		String tdate = "strftime('%Y-%m-%d', "+LogDB.LOG_DATETIME_EDIT+")";
//		String before7date = "DATE(DATE('now', '+9 hours'), '-7 days')";
//		String[] select = new String[]{LogDB.LOG_SYUBETSU_ID, LogDB.LOG_SYUBETSU, "COUNT("+LogDB.LOG_SYUBETSU_ID+") as c"};
//		Cursor cursor = db.query(
//			LogDB.TABLE_LOG,	// テーブル名
//			select,				// 種別ID,種別名,種別ID件数
//			nowdate+">="+tdate+" and "+tdate+">="+before7date+" and "+LogDB.LOG_SYUBETSU_ID+"!=0",	// 一週間以内
//			null,
//			LogDB.LOG_SYUBETSU_ID,
//			null,
//			"c DESC",
//			"5");
		// 「最新の打刻から上位５つ」
		Cursor cursor = db.query(
				LogDB.TABLE_LOG,
				new String[]{LogDB.LOG_SYUBETSU_ID, LogDB.LOG_SYUBETSU},
				LogDB.LOG_SYUBETSU_ID+"!=0",
				null,
				LogDB.LOG_SYUBETSU_ID,
				null,
				LogDB.LOG_DATETIME_EDIT+" DESC",
				"5");

		List<Syubetsu> list = new ArrayList<Syubetsu>();
		if(cursor.moveToFirst()) {
			String worktype;
			int wid;
			Syubetsu syubetsu;
			do {
				worktype = cursor.getString(cursor.getColumnIndexOrThrow(LogDB.LOG_SYUBETSU));
				wid = cursor.getInt(cursor.getColumnIndexOrThrow(LogDB.LOG_SYUBETSU_ID));
				syubetsu = new Syubetsu(null,worktype,wid);
				list.add(syubetsu);
			} while(cursor.moveToNext());
		}
		cursor.close();
		// 履歴をリストビューに反映
		setSyubetsuLog(list);
	}
	*/

	/**
	 * 勤務種別を取得します。
	 */
	private void getSyubetsuData2() {
		Resources resources = this.getResources();
		String url = resources.getString(R.string.syubetsu_url);
		int timeout_connect = resources.getInteger(R.integer.timeout_connect);
		int timeout_read = resources.getInteger(R.integer.timeout_read);
		MyHttpClient2 client = new MyHttpClient2(this, url, this.responser);
		client.enabledProgressDialog(this, "通信中", "しばらくお待ちください", null);
		client.setTimeOut(timeout_connect, timeout_read);
		client.addPostParam(
				resources.getString(R.string.syubetsu_param_name1),
				resources.getString(R.string.syubetsu_param_value1));
		client.execute();
	}


	/**
	 * レスポンス受け取り
	 */
	private MyHttpClient2.Responser responser = new MyHttpClient2.Responser() {

		@Override
		public void onFailed() {
			// 失敗
			SyubetsuActivity.this.showErrorDialog();
		}

		@Override
		public void onError(String errorcode, String message) {
			// 人為的失敗
			SyubetsuActivity.this.showErrorDialog(errorcode, message);
		}

		@Override
		public void onCompleted(String json) {
			// 成功
			try {
				// root
				JSONObject rootObj = new JSONObject(json);

				// data
				JSONArray dataArray = rootObj.getJSONArray("data");
				SyubetsuActivity.this.setSyubetsu(dataArray);

				return;

			} catch(JSONException ex) {
				ex.printStackTrace();
			}

			// JSONに問題あり
			SyubetsuActivity.this.showErrorDialog();
			return;
		}
	};

	/**
	 * 勤務種別を選択履歴リストに反映します。
	 * @param dataArray
	 */
	private void setSyubetsuLog(final List<Syubetsu> dataArray) {
		// アダプター作成
		final LogExListAdapter adapter = new LogExListAdapter(this, dataArray);

	    //生成した情報をセット
	    this.logexlistview.setAdapter(adapter);
	    // イベントリスナー
	    this.logexlistview.setOnChildClickListener(new OnChildClickListener() {
	    	@Override
	    	public boolean onChildClick(ExpandableListView parent, View v,
	                int groupPosition, int childPosition, long id) {
	    		// 種別クリック
	    		Syubetsu syubetsu = (Syubetsu)adapter.getChild(groupPosition, childPosition);
	    		Singleton.INSTANCE.setSyubetsu(syubetsu.name);
				Singleton.INSTANCE.setSyubetsuId(syubetsu.wid);

				// 確認画面からのジャンプならアクティビティを閉じる
				if(SyubetsuActivity.this.isJump) {
					SyubetsuActivity.this.finish();
					return false;
				}

				Intent nextIntent = NextAct.getNextIntent(SyubetsuActivity.this, Singleton.INSTANCE.getSyuttaiStatus());
				SyubetsuActivity.this.startActivityForResult(nextIntent, 0);
	    		return false;
	    	}
	    });
	    this.logexlistview.setOnItemLongClickListener(new OnItemLongClickListener() {
	    	@Override
	    	public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
	    		if (ExpandableListView.getPackedPositionType(id) == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
	    			if(isStatusSearch) {
	    				return false;
	    			}
	                int groupPosition = ExpandableListView.getPackedPositionGroup(id);
	                int childPosition = ExpandableListView.getPackedPositionChild(id);
	                final Syubetsu syubetsu = (Syubetsu)adapter.getChild(groupPosition, childPosition);
	                MyDialog dialog = new MyDialog();
	                dialog.setTitle("確認");
	                dialog.setMessage(String.format("「%s」を履歴から削除してもよろしいですか？", syubetsu.name));
	                dialog.setPositive_button("削除する", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							MyPreference pref = new MyPreference(SyubetsuActivity.this);
							boolean result = pref.removeSyubetsuLog(syubetsu.wid);
							if(!result) {
								Toast.makeText(SyubetsuActivity.this, "削除に失敗しました", Toast.LENGTH_SHORT).show();
							} else {
								for(Syubetsu s : dataArray) {
									if(s.wid == syubetsu.wid) {
										int delIndex = dataArray.indexOf(s);
										if(delIndex != -1) {
											dataArray.remove(delIndex);
										}
										break;
									}
								}
								Toast.makeText(SyubetsuActivity.this, "削除しました", Toast.LENGTH_SHORT).show();
								adapter.notifyDataSetChanged();
							}
						}
					});
	                dialog.setNegative_button("キャンセル", null);
	                dialog.show(SyubetsuActivity.this.getFragmentManager(), null);
	                return true;
	            }
	            return false;
	    	}
	    });

	    // 親リスト全開放
	    this.logexlistview.expandGroup(0);
	}

	/**
	 * 勤務種別の選択履歴をリストに反映します
	 * @param enableSyubetsuList
	 * @return
	 */
	private List<Syubetsu> getSyubetsuLogData(List<Syubetsu> enableSyubetsuList) {
		MyPreference pref = MyPreference.getInstance(this);
		Syubetsu syubetsu;
		int[] wids = pref.getSyubetsuLogs();
		List<Syubetsu> wLogs = new ArrayList<Syubetsu>();
		BASE:
		for(int wid : wids) {
			//履歴が存在しない場合は無視
			if(wid == -1) {
				continue;
			}
			//すでにリストに登録しているものは無視
			for(Syubetsu s : wLogs) {
				if(wid == s.wid) {
					continue BASE;
				}
			}
			//勤務種別を登録
			for(Syubetsu s : enableSyubetsuList) {
				if(s.wid == wid) {
					syubetsu = new Syubetsu(s.kana, s.name, s.wid);
					wLogs.add(syubetsu);
					break;
				}
			}
		}
		return wLogs;
	}

	/**
	 * 勤務種別を候補リストに反映します。<br>
	 * @param dataArray
	 * @throws JSONException
	 */
	private void setSyubetsu(JSONArray dataArray) throws JSONException {
		// 親ノードのリスト
		List<String> parentList = new ArrayList<String>();
		// 全体の子ノードのリスト
		this.syubetsuLists = new ArrayList<List<Syubetsu>>();

		// 親ノードに表示する内容を生成
		String[] abcNames = {"あ","か","さ","た","な","は","ま","や","ら","わ"};
		for (String abcName : abcNames) {
			// 親ノードのリストに内容を格納
			parentList.add(abcName);
		}

		// 子ノードに表示する内容を生成
		// JSON解析
		List<Syubetsu> list = new ArrayList<Syubetsu>();
		JSONObject data;
		Syubetsu syubetsu;
		for(int i=0; i<dataArray.length(); i++) {
			data = dataArray.getJSONObject(i);
			syubetsu = new Syubetsu(data.getString("kana"), data.getString("name"), data.getInt("wid"));
			list.add(syubetsu);
		}
		// ソート
		Collections.sort(list, new Comparator<Syubetsu>() {
	        public int compare(Syubetsu s0, Syubetsu s1) {
	            return s0.kana.compareTo(s1.kana);
	        }
	    });

		// あかさたな...単位に分割して格納する
		String[] abcs = {"ア","カ","サ","タ","ナ","ハ","マ","ヤ","ラ","ワ"};
		// 先に器を作成
		for(int i=0; i<abcs.length; i++) {
			this.syubetsuLists.add(new ArrayList<Syubetsu>());
		}
		// 後ろから検証(便宜上)
		int addedIndex = list.size()-1;	// 検証済みインデックス
		checkEnd:
		for(int abcIndex=abcs.length-1; abcIndex>=0; abcIndex--) {
			for(int kanaIndex=addedIndex; kanaIndex>=0; kanaIndex--) {
				if(abcs[abcIndex].compareToIgnoreCase(list.get(kanaIndex).kana) <= 0) {
					// kanaがabc以下であれば格納して次のkanaを検証
					this.syubetsuLists.get(abcIndex).add(list.get(kanaIndex));
					if(kanaIndex == 0) {
						break checkEnd;	// これ以上検証する必要はないので全部終わる
					}
				} else {
					// kanaがabcよりも大きければ現abcの比較を終了して次のabcへ
					addedIndex = kanaIndex;	// 検証中のインデックスを記録
					break;
				}
			}
		}
		// 後ろから検証したので順番を逆転する
		for(int i=0; i<this.syubetsuLists.size();) {
			Collections.reverse(this.syubetsuLists.get(i));	// 逆転
			if(this.syubetsuLists.get(i).size() == 0) {
				// 空っぽならいらない
				this.syubetsuLists.remove(i);
				parentList.remove(i);
			} else {
				i++;	// 削除しなかった場合のみインクリメント
			}
		}

		// アダプター作成
		MyExListAdapter adapter = new MyExListAdapter(this, parentList, this.syubetsuLists);

	    //生成した情報をセット
	    this.exlistview.setAdapter(adapter);
	    // イベントリスナー
	    this.exlistview.setOnChildClickListener(new OnChildClickListener() {
	    	@Override
	    	public boolean onChildClick(ExpandableListView parent, View v,
	                int groupPosition, int childPosition, long id) {
	    		// 種別クリック
	    		Syubetsu syubetsu = SyubetsuActivity.this.syubetsuLists.get(groupPosition).get(childPosition);
	    		Singleton.INSTANCE.setSyubetsu(syubetsu.name);
				Singleton.INSTANCE.setSyubetsuId(syubetsu.wid);

				// 確認画面からのジャンプならアクティビティを閉じる
				if(SyubetsuActivity.this.isJump) {
					SyubetsuActivity.this.finish();
					return false;
				}

				Intent nextIntent = NextAct.getNextIntent(SyubetsuActivity.this, Singleton.INSTANCE.getSyuttaiStatus());
				SyubetsuActivity.this.startActivityForResult(nextIntent, 0);
	    		return false;
	    	}
	    });

	    // 履歴リストも作成
	    List<Syubetsu> logs = getSyubetsuLogData(list);
	    setSyubetsuLog(logs);

	    // 親リスト全開放
//	    for(int i=0; i<parentList.size(); i++) {
//	        this.exlistview.expandGroup(i);
//	    }
	}

	private class Syubetsu {
		public String kana;
		public String name;
		public int wid;
		public Syubetsu(String kana, String name, int wid) {
			this.kana = kana;
			this.name = name;
			this.wid = wid;
		}
	}

	@SuppressLint("InflateParams")
	private class LogExListAdapter extends BaseExpandableListAdapter {
		private Context context;
		private List<Syubetsu> children;
		public LogExListAdapter(Context context, List<Syubetsu> syubetsuLogList) {
			this.context = context;
			this.children = syubetsuLogList;
		}
		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = LayoutInflater.from(this.context).inflate(R.layout.item_syubetsu_log_separator, null);
				ImageButton searchBtn = (ImageButton)convertView.findViewById(R.id.imagebutton_search);
				searchBtn.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// 種別名と種別カナで検索
						DialogHolder.INSTANCE.init();	// 中身からっぽにしておく
						TextDialog dialog = new TextDialog();
						dialog.setTitle("検索する文字を入力して下さい");
						dialog.setPositiveText("検索");
						dialog.setNegativeText("キャンセル");
						dialog.setCallback(new TextDialog.Callback() {

							@Override
							public void onResult(String result) {
								if(result == null || result.equals("")) {
									// 何もしない
									return;
								}
								// 検索
								List<Syubetsu> searchList = new ArrayList<Syubetsu>();
								for(List<Syubetsu> list : SyubetsuActivity.this.syubetsuLists) {
									for(Syubetsu syubetsu : list) {
										// 種別名・種別カナ検索
										if(syubetsu.name.contains(result) || syubetsu.kana.contains(result)) {
											searchList.add(syubetsu);
										}
									}
								}
								// 更新
								setSyubetsuLog(searchList);
								isStatusSearch = true;
							}

							@Override
							public void onCancel() {}
						});
						dialog.show(SyubetsuActivity.this.getFragmentManager(), null);
					}
				});
			}
			return convertView;
		}
		@Override
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = LayoutInflater.from(this.context).inflate(R.layout.item_syubetsu_child, null);
			}
			final Syubetsu syubetsu = (Syubetsu)this.getChild(groupPosition, childPosition);
			((TextView)convertView.findViewById(R.id.textview_child)).setText(syubetsu.name);
			return convertView;
		}
		@Override
		public Object getChild(int arg0, int arg1) { return this.children.get(arg1); }
		@Override
		public long getChildId(int arg0, int arg1) { return arg1; }
		@Override
		public int getChildrenCount(int arg0) { return this.children.size(); }
		@Override
		public Object getGroup(int arg0) { return arg0; }
		@Override
		public int getGroupCount() { return 1; }
		@Override
		public long getGroupId(int arg0) { return arg0; }
		@Override
		public boolean hasStableIds() { return true; }
		@Override
		public boolean isChildSelectable(int arg0, int arg1) { return true; }
	}

	/**
	 * 勤務種別表示用リストアダプター
	 */
	@SuppressLint("InflateParams")
	private class MyExListAdapter extends BaseExpandableListAdapter {
		private Context context;
		private List<String> groups;
		private List<List<Syubetsu>> children;
		public MyExListAdapter(Context context, List<String> groups, List<List<Syubetsu>> syubetsuList) {
			this.context = context;
			this.groups = groups;
			this.children = syubetsuList;
		}
		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = LayoutInflater.from(this.context).inflate(R.layout.item_syubetsu_parent, null);
			}
			((TextView)convertView.findViewById(R.id.textview_parent)).setText(this.groups.get(groupPosition));
			return convertView;
		}
		@Override
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = LayoutInflater.from(this.context).inflate(R.layout.item_syubetsu_child, null);
			}
			final Syubetsu syubetsu = (Syubetsu)this.getChild(groupPosition, childPosition);
			((TextView)convertView.findViewById(R.id.textview_child)).setText(syubetsu.name);
			return convertView;
		}
		@Override
		public Object getChild(int arg0, int arg1) { return this.children.get(arg0).get(arg1); }
		@Override
		public long getChildId(int arg0, int arg1) { return arg1; }
		@Override
		public int getChildrenCount(int arg0) { return this.children.get(arg0).size(); }
		@Override
		public Object getGroup(int arg0) { return this.groups.get(arg0); }
		@Override
		public int getGroupCount() { return this.groups.size(); }
		@Override
		public long getGroupId(int arg0) { return arg0; }
		@Override
		public boolean hasStableIds() { return true; }
		@Override
		public boolean isChildSelectable(int arg0, int arg1) { return true; }
	}

	private void showErrorDialog() {
		this.showErrorDialog(null, null);
	}

	private void showErrorDialog(String errorcode, String message) {

		// 多重起動阻止
		String DIALOG_NAME = this.getClass().getName();
		if(this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
			return;
		}

		String dialogMessage;
		if(errorcode == null && message == null) {
			dialogMessage = "エラーが発生しました。お手数ですが再度操作を行ってください。";
		}else {
			dialogMessage = "エラーが発生しました。お手数ですが再度操作を行ってください。"+"\n―――"+"\nerrorcode: "+errorcode+"\nmessage: "+message;
		}
		MyDialog dialog = new MyDialog();
		dialog.setMode(MyDialog.MODE_NORMAL);
		dialog.setTitle("ERROR!!");
		dialog.setMessage(dialogMessage);
		dialog.setPositive_button("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent topIntent = new Intent(SyubetsuActivity.this, Main.class);
				topIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
				SyubetsuActivity.this.startActivity(topIntent);
			}
		});
		dialog.setCancelable(false);
		dialog.show(this.getFragmentManager(), DIALOG_NAME);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}


	@Override
	public void onActivityResult(int req, int res, Intent d) {
		// ページ数カウントダウン
		NextAct.countDown(this, Singleton.INSTANCE.getSyuttaiStatus());
	}

}
