package jp.co.tecwt.medi_techno_service.view

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.ContentValues
import android.content.Intent
import android.media.SoundPool
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import jp.co.tecwt.medi_techno_service.R
import kotlinx.android.synthetic.main.activity_work_list.*
import android.net.Uri
import android.nfc.NfcAdapter
import android.support.v7.app.AlertDialog
import android.text.format.DateFormat
import android.util.Log
import android.webkit.*
import android.widget.Toast
import jp.co.tecwt.medi_techno_service.data.LogDB
import jp.co.tecwt.medi_techno_service.data.MyPreference
import jp.co.tecwt.medi_techno_service.data.Singleton
import jp.co.tecwt.medi_techno_service.util.DateTime
import jp.co.tecwt.medi_techno_service.util.Passward
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat


class WorkListActivity : AppCompatActivity() {
    private var soundPool: SoundPool? = null
    private var soundID: Int? = null

    private lateinit var nfcAdapter: NfcAdapter
    private lateinit var pendingIntent: PendingIntent

    private var isEnableNFC: Boolean = false
        @Synchronized get
        @Synchronized set

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_work_list)


        soundPool = SoundPool.Builder().build()?.apply {
            soundID = load(this@WorkListActivity, R.raw.loaded, 1)
        }

        nfcAdapter = NfcAdapter.getDefaultAdapter(applicationContext)
        val intent = Intent(this, WorkListActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        }
        pendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        webView.clearCache(true)

        val preference = MyPreference.getInstance(this)

        val oid = preference.soshikiID
        val unitid = Singleton.INSTANCE.appInit_DeviceID.toString()
        val pass = preference.password
        val latitude = Singleton.INSTANCE.lat.toString()
        val longitude = Singleton.INSTANCE.lng.toString()
        val (checktime, passtime, outpass) = preference.run {
            val check = if (lateTaikinConfirmFlag) lateTaikinConfirmTime else -1
            if (lateTaikinPasswordFlag) {
                val timepass = Passward.GetTimecardPassward(DateTime.GetYYYYMMDD(DateTime.GetToday()))
                Triple(check, lateTaikinPasswordTime, timepass)
            } else {
                Triple(check, -1, "")
            }
        }

        val url = resources.getString(R.string.worklist_url)
        webView.settings.javaScriptEnabled = true
        webView.setWebViewClient(object: WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                if (url?.contains("#") == true) {
                    return
                }
                val script = """
                setupParameter($oid, "$unitid", "$pass", "$latitude", "$longitude",
                    "app", $checktime, $passtime, "$outpass");
                startReadNFC();
                """
                view?.evaluateJavascript(script, {})
                isEnableNFC = true
            }

            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                val uri = Uri.parse(url)
                return when (uri.scheme) {
                    "close" -> {
                        finish()
                        true
                    }
                    "retry" -> {
                        isEnableNFC = true
                        true
                    }
                    "complete" -> {
                        completeSend()
                        true
                    }
                    else -> super.shouldOverrideUrlLoading(view, url)
                }
            }

            override fun onReceivedError(view: WebView?, errorCode: Int, description: String?, failingUrl: String?) {
                AlertDialog.Builder(this@WorkListActivity).apply {
                    setTitle("通信エラー")
                    setMessage("通信状況を確認してください")
                    setPositiveButton("OK") { _, _ ->
                        this@WorkListActivity.finish()
                    }
                }.show()
            }

            override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                AlertDialog.Builder(this@WorkListActivity).apply {
                    setTitle("通信エラー")
                    setMessage("通信状況を確認してください")
                    setPositiveButton("OK") { _, _ ->
                        this@WorkListActivity.finish()
                    }
                }.show()
            }
        })

        webView.loadUrl(url)
    }

    override fun onResume() {
        super.onResume()
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, null, null)
    }

    override fun onPause() {
        super.onPause()
        nfcAdapter.disableForegroundDispatch(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        soundPool?.release()
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        readNFC(intent)
    }

    override fun onBackPressed() {
        webView.evaluateJavascript("confirmClose();", {})
    }

    private fun readNFC(intent: Intent) {
        // 連続読み取り不可
        if (isEnableNFC) {
            isEnableNFC = false
        } else {
            return
        }

        // ID読み取り
        val id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID)
                    .joinToString(separator = "", transform = { String.format("%02X", it) })
                    .trim { it <= ' ' }

        Singleton.INSTANCE.id = id
        Singleton.INSTANCE.datetimeAuto = System.currentTimeMillis()

        val script = """readNFC("$id");"""
        webView.evaluateJavascript(script, {})
    }

    private fun completeSend() {
        val script = """requestCompleteData();"""
        webView.evaluateJavascript(script, {
            saveLog(it)
        })
        playSound()
    }

    private fun playSound() {
        val soundPool = soundPool ?: return
        val soundID = soundID ?: return
        soundPool.play(soundID, 0.5f, 0.5f, 0, 0, 1f)
    }

    private fun saveLog(jsonString: String) {
        try {
            val rootObj = JSONObject(jsonString)

            // DBに保存
            val db = LogDB(this).writableDatabase
            val data = ContentValues()
            data.put(LogDB.LOG_ID, Singleton.INSTANCE.id)
            data.put(LogDB.LOG_NAME, rootObj.getString("name"))
            val dateString = rootObj.getString("tdate") + " " + rootObj.getString("outtime")
            val date = SimpleDateFormat("yyyyMMdd HH:mm").parse(dateString)
            data.put(LogDB.LOG_DATETIME_EDIT, SimpleDateFormat("yyyy-MM-dd kk:mm:ss").format(date))
            val datetime_auto = Singleton.INSTANCE.datetimeAuto
            data.put(LogDB.LOG_DATETIME_AUTO, DateFormat.format("yyyy-MM-dd kk:mm:ss", datetime_auto).toString())
            data.put(LogDB.LOG_SYUTTAISTATUS, Singleton.INSTANCE.syuttaiStatus)
            data.put(LogDB.LOG_SYUBETSU, "-")
            data.put(LogDB.LOG_GPS_LAT, Singleton.INSTANCE.lat)
            data.put(LogDB.LOG_GPS_LNG, Singleton.INSTANCE.lng)
            data.put(LogDB.LOG_GYOUMU, "-")
            data.put(LogDB.LOG_KYUKEI, rootObj.getString("breaktime"))
            data.put(LogDB.LOG_KOUTSUUHI, rootObj.getString("fare"))
            data.put(LogDB.LOG_SYORIMAISUU, "-")
            data.put(LogDB.LOG_MOUSHIDEMAISUU, "-")
            data.put(LogDB.LOG_SYUBETSU_ID, "-")
            db.insertOrThrow(LogDB.TABLE_LOG, "", data)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}

