package jp.co.tecwt.medi_techno_service.data;

public enum DialogHolder {
	
	INSTANCE;
	
	private int mode;
	private String title;
	private String message;
	private String posiTxt;
	private Object posiLsn;
	private String negaTxt;
	private Object negaLsn;
	private Object cancelLsn;
	private Object callback;
	private Object[] values;
	private int number;
	
	public void init() {
		this.mode = -1;
		this.title = null;
		this.message = null;
		this.posiTxt = null;
		this.posiLsn = null;
		this.negaTxt = null;
		this.negaLsn = null;
		this.cancelLsn = null;
		this.callback = null;
		this.values = null;
		this.number = 0;
	}
	
	public void setMode(int mode) {
		this.mode = mode;
	}
	public int getMode() {
		return this.mode;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTitle() {
		return this.title;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessage() {
		return this.message;
	}
	
	public void setPositiveText(String text) {
		this.posiTxt = text;
	}
	public String getPositibeText() {
		return this.posiTxt;
	}
	
	public void setPositiveListener(Object listener) {
		this.posiLsn = listener;
	}
	public Object getPositibeListener() {
		return this.posiLsn;
	}
	
	public void setNegativeText(String text) {
		this.negaTxt = text;
	}
	public String getNegativeText() {
		return this.negaTxt;
	}
	
	public void setNegativeListener(Object listener) {
		this.negaLsn = listener;
	}
	public Object getNegatibeListener() {
		return this.negaLsn;
	}
	
	public void setCancelListener(Object listener) {
		this.cancelLsn = listener;
	}
	public Object getCancelListener() {
		return this.cancelLsn;
	}
	
	public void setCallback(Object callback) {
		this.callback = callback;
	}
	public Object getCallback() {
		return this.callback;
	}
	
	public void setValues(Object[] values) {
		this.values = values;
	}
	public Object[] getValues() {
		return this.values;
	}
	
	public void setNumber(int number) {
		this.number = number;
	}
	public int getNumber() {
		return this.number;
	}

}
