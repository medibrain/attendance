package jp.co.tecwt.medi_techno_service.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 送信履歴保存用DB
 */
public class LogDB extends SQLiteOpenHelper {

	public static final String DB_NAME = "log_database";
	public static final int DB_VERSION = 2;

	// 送信履歴テーブル
	public static final String TABLE_LOG = "log_table";
	public static final String LOG_PRIMARY = "_id";
	public static final String LOG_ID = "idm";
	public static final String LOG_NAME = "name";
	public static final String LOG_UID = "uid";
	public static final String LOG_DATETIME_EDIT = "datetime_edit";
	public static final String LOG_DATETIME_AUTO = "datetime_auto";
	public static final String LOG_SYUTTAISTATUS = "syuttaiStatus";
	public static final String LOG_SYUBETSU = "syubetsu";
	public static final String LOG_GPS_LAT = "gps_lat";
	public static final String LOG_GPS_LNG = "gps_lng";
	public static final String LOG_GYOUMU = "gyoumu";
	public static final String LOG_KYUKEI = "kyukei";
	public static final String LOG_KOUTSUUHI = "koutsuuhi";
	public static final String LOG_SYORIMAISUU = "syorimaisuu";
	public static final String LOG_MOUSHIDEMAISUU = "moushidemaisuu";
	public static final String LOG_SYUBETSU_ID = "syubetsu_id";


	public LogDB(Context context) {
		// 第三引数：クエリ時に自作Cursorオブジェクトを返す場合に定義する
		// 第四引数：現在の値と相違がある場合、onUpgrade()がコールされる
		super(context, DB_NAME, null, DB_VERSION);
	}


	@Override
	public void onCreate(SQLiteDatabase db) {
		String log_sql = new StringBuilder()
			.append("create table ")
			.append(TABLE_LOG)
			.append(" (").append(LOG_PRIMARY).append(" integer primary key autoincrement, ")
			.append(LOG_ID).append(" text not null, ")
			.append(LOG_UID).append(" integer, ")
			.append(LOG_NAME).append(" text not null, ")
			.append(LOG_DATETIME_EDIT).append(" text, ")
			.append(LOG_DATETIME_AUTO).append(" text, ")
			.append(LOG_SYUTTAISTATUS).append(" integer, ")
			.append(LOG_SYUBETSU).append(" text, ")
			.append(LOG_GPS_LAT).append(" real, ")
			.append(LOG_GPS_LNG).append(" real, ")
			.append(LOG_GYOUMU).append(" text, ")
			.append(LOG_KYUKEI).append(" text, ")
			.append(LOG_KOUTSUUHI).append(" text, ")
			.append(LOG_SYORIMAISUU).append(" text, ")
			.append(LOG_MOUSHIDEMAISUU).append(" text, ")
			.append(LOG_SYUBETSU_ID).append(" integer default 0);")
			.toString();
		db.execSQL(log_sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//		db.execSQL("drop table "+DB_NAME+";");
//		onCreate(db);

		if (oldVersion < newVersion) {
			// カラムの追加
			if(oldVersion == 1 && newVersion == 2) {
				db.execSQL(
						"alter table " + TABLE_LOG +
						" add column " + LOG_SYUBETSU_ID + " integer default 0"
					);
			}
		}
	}

}
