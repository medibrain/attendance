package jp.co.tecwt.medi_techno_service.data;

import jp.co.tecwt.medi_techno_service.R;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * 各種設定情報保存プリファレンス
 *
 * プリファレンスとは……
 * 設定情報など、そのアプリに強く依存する情報を保存するのに最適な簡易永続化手段。
 * アンインストールやデータ削除を行うことで消滅する。
 */
public class MyPreference {

	private static final String PREF_NAME = "pref";
	private static final String KEY_APPLICATION_UPDATE_INITIALIZED = "application_update_initialized";
	private static final String KEY_APPLICATION_PREVIOUS_VERSIONCODE = "application_previous_versioncode";

	public static final String KEY_SYUKKIN_SYUBETSU = "syukkin_syubetsu";
	public static final String KEY_SYUKKIN_EDITTIME = "syukkin_edittime";
	public static final String KEY_IDOU_SYORIMAISUU = "idou_syorimaisuu";
	public static final String KEY_IDOU_MOUSHIDEMAISUU = "idou_moushidemaisuu";
	public static final String KEY_IDOU_GYOUMU = "idou_gyoumu";
	public static final String KEY_IDOU_EDITTIME = "idou_edittime";
	public static final String KEY_IDOU_SYUBETSU = "idou_syubetsu";
	public static final String KEY_TAIKIN_SYUBETSU = "taikin_syubetsu";
	public static final String KEY_TAIKIN_KYUKEI = "taikin_kyukei";
	public static final String KEY_TAIKIN_KOUTSUUHI = "taikin_koutsuuhi";
	public static final String KEY_TAIKIN_SYORIMAISUU = "taikin_syorimaisuu";
	public static final String KEY_TAIKIN_MOUSHIDEMAISUU = "taikin_moushidemaisuu";
	public static final String KEY_TAIKIN_GYOUMU = "taikin_gyoumu";
	public static final String KEY_TAIKIN_EDITTIME = "taikin_edittime";
	public static final String KEY_APPINIT = "appinit";
	public static final String KEY_DEVICE_NAME = "device_name";
	public static final String KEY_SOSHIKI_ID = "soshikiid";
	public static final String KEY_PASSWORD = "password";
	public static final String KEY_DEVICE_ID = "deviceid";
	public static final String KEY_URL = "url";
	public static final String KEY_DO_NOT_USE_DEFAULT_URL = "donotusedefaulturl";
	public static final String KEY_DEVICE_TYPE = "device_type";
	public static final String KEY_UPDATE_CHECK_DATE = "update_check_date";
	public static final String KEY_UPDATE_CHECK_FLAG = "update_check_flag";
	public static final String KEY_IDOU_FLAG = "idou_flag";
	public static final String KEY_INTIME_CHANGE_FLAG = "intime_change_flag";
	public static final String KEY_OUTTIME_CHANGE_FLAG = "outtime_change_flag";
	public static final String KEY_WORKINGHOUR_SYUKKIN_TIME = "workinghour_syukkin_time";
	public static final String KEY_WORKINGHOUR_TAIKIN_TIME = "workinghour_taikin_time";
	public static final String KEY_FASTSYUKKIN_CONFIRM_FLAG = "fastsyukkin_confirm_flag";
	public static final String KEY_FASTSYUKKIN_CONFIRM_TIME = "fastsyukkin_confirm_time";
	public static final String KEY_FASTSYUKKIN_PASSWORD_FLAG = "fastsyukkin_password_flag";
	public static final String KEY_FASTSYUKKIN_PASSWORD_TIME = "fastsyukkin_password_time";
	public static final String KEY_LATETAIKIN_CONFIRM_FLAG = "latetaikin_confirm_flag";
	public static final String KEY_LATETAIKIN_CONFIRM_TIME = "latetaikin_confirm_time";
	public static final String KEY_LATETAIKIN_PASSWORD_FLAG = "latetaikin_password_flag";
	public static final String KEY_LATETAIKIN_PASSWORD_TIME = "latetaikin_password_time";
	public static final String KEY_LATESYUKKIN_PERMISSION_MINUTE = "latesyukkin_permission_minute";
	public static final String KEY_GYOUMU_TYPE = "gyoumu_type";

	public static final String KEY_NORMAL_TEXTSIZE = "normalTextSize";
	public static final String KEY_MAIN_SYUTTAIBTN_TEXTSIZE = "main_syuttaiBtnTextSize";
	public static final String KEY_MAIN_IDOUBTN_TEXTSIZE = "main_idouBtnTextSize";
	public static final String KEY_MAIN_LOGADMIN_TEXTSIZE = "main_logAdminBtnTextSize";
	public static final String KEY_MAIN_ENDBTN_TEXTSIZE = "main_endBtnTextSize";
	public static final String KEY_MAIN_DATE_TEXTSIZE = "main_dateTextSize";
	public static final String KEY_MAIN_TIME_TEXTSIZE = "main_timeTextSize";
	public static final String KEY_HEADER_TITLE_TEXTSIZE = "header_titleTextSize";
	public static final String KEY_HEADER_BTN_TEXTSIZE = "header_btnTextSize";
	public static final String KEY_FOOTER_NEXTBTN_TEXTSIZE = "footer_nextBtnTextSize";
	public static final String KEY_FOOTER_NORMALBTN_TEXTSIZE = "footer_normalBtnTextSize";
	public static final String KEY_ADMIN_BTN_TEXTSIZE = "admin_btnTextSize";
	public static final String KEY_LIST_PARENT_TEXTSIZE = "list_parentTextSize";
	public static final String KEY_LIST_CHILD_TEXTSIZE = "list_childTextSize";
	public static final String KEY_LIST_HEADEREXPLAIN_TEXTSIZE = "list_headerExplainTextSize";
	public static final String KEY_LIST_EMPTY_TEXTSIZE = "list_emptyTextSize";
	public static final String KEY_NUMBER_UPDOWNBTN_TEXTSIZE = "number_updownBtnTextSize";
	public static final String KEY_NUMBER_INPUTBTN_TEXTSIZE = "number_inputBtnTextSize";
	public static final String KEY_NUMBER_BIGBTN_TEXTSIZE = "number_bigBtnTextSize";
	public static final String KEY_NUMBER_BACKBTN_TEXTSIZE = "number_backBtnTextSize";
	public static final String KEY_NUMBER_SUBTITLEBTN_TEXTSIZE = "show_subtitleTextSize";
	public static final String KEY_SHOW_NUMBER_TEXTSIZE = "show_numberTextSize";
	public static final String KEY_SHOW_UNIT_TEXTSIZE = "show_unitTextSize";
	public static final String KEY_NFC_TITLE_TEXTSIZE = "nfc_titleTextSize";
	public static final String KEY_NFC_CONTENT_TEXTSIZE = "nfc_contentTextSize";
	public static final String KEY_NFC_CARD_TEXTSIZE = "nfc_cardTextSize";
	public static final String KEY_NFC_SUPPLEMENT_TEXTSIZE = "nfc_supplementTextSize";
	public static final String KEY_LOG_CONTENT_TEXTSIZE = "log_contentTextSize";
	public static final String KEY_LOG_TIME_TEXTSIZE = "log_timeTextSize";
	public static final String KEY_LOG_ICON_TEXTSIZE = "log_iconTextSize";
	public static final String KEY_NYUURYOKUKOUMOKU_SELECT_TEXTSIZE = "nyuuryokukoumoku_selectTextSize";
	public static final String[] KEY_TEXTSETS = {
		"textset_1","textset_2","textset_3","textset_4","textset_5","textset_6","textset_7","textset_8","textset_9"
	};
	public static final String[] KEY_TEXTSETS_SELECT = {
		"textset_1","textset_2","textset_3","textset_4","textset_5","textset_6","textset_7","textset_8","textset_9","textset_10",
		"textset_11","textset_12","textset_13","textset_14","textset_15","textset_16","textset_17","textset_18","textset_19","textset_20",
		"textset_21","textset_22","textset_23","textset_24","textset_25","textset_26","textset_27","textset_28","textset_29","textset_30",
		"textset_31","textset_32","textset_33","textset_34","textset_35","textset_36","textset_37","textset_38","textset_39","textset_40",
		"textset_41","textset_42","textset_43","textset_44","textset_45","textset_46","textset_47","textset_48","textset_49","textset_50",
	};
	public static final String[] KEY_SYUBETSU_LOG_SETS = {
		"syubetsu_log_1", "syubetsu_log_2", "syubetsu_log_3", "syubetsu_log_4", "syubetsu_log_5"
	};
	public static final String KEY_SYUBESTU_LOG_NEXT_WRITE_INDEX = "syubetsuLog_nextWriteIndex";

	public static final int STATUS_HISSU = 0;
	public static final int STATUS_VISIBLE = 1;
	public static final int STATUS_INVISIBLE = 2;

	public static final boolean APPINIT_STATUS_OK = true;
	public static final boolean APPINIT_STATUS_NG = false;

	public static final boolean DEFAULT_URL_DO_NOT_USE = true;
	public static final boolean DEFAULT_URL_USE = false;

	public static final int GYOUMU_TYPE_NORMAL = 0;
	public static final int GYOUMU_TYPE_SELECT = 1;

	private static MyPreference myPreference;
	private SharedPreferences pref;


	public synchronized static MyPreference getInstance(Context context) {
		if(myPreference == null) {
			myPreference = new MyPreference(context);
		}
		return myPreference;
	}

	public MyPreference(Context context) {
		this.pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
	}

	/**
	 * アプリ更新に付随する初期化を行なったかを記録する
	 * @param initialized
	 * @return
	 */
	public boolean writeApplicationUpdateInitialized(boolean initialized) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putBoolean(MyPreference.KEY_APPLICATION_UPDATE_INITIALIZED, initialized);
		return editor.commit();
	}

	/**
	 * アプリ更新に付随する初期化を行なったかを確認する。初期値はfalse
	 * @return
	 */
	public boolean getApplicationUpdateInitialized() {
		return this.pref.getBoolean(KEY_APPLICATION_UPDATE_INITIALIZED, false);
	}

	/**
	 * 前回のアプリのバージョンを記録する
	 * @param previousVersion
	 * @return
	 */
	public boolean writeApplicationPreviousVersionCode(int previousVersionCode) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putInt(MyPreference.KEY_APPLICATION_PREVIOUS_VERSIONCODE, previousVersionCode);
		return editor.commit();
	}

	/**
	 * 前回のアプリのバージョンを確認する。初期値は-1
	 * @return
	 */
	public int getApplicationPreviousVersion() {
		return this.pref.getInt(KEY_APPLICATION_PREVIOUS_VERSIONCODE, -1);
	}

	/**
	 * 項目設定を保存する。
	 * @param MyPreference_FiledKey
	 * @param status
	 * @return
	 */
	public boolean writeNyuuryokuKoumokuCheckedPosition(String MyPreference_FieldKey, int status) {
		String key = null;
		if (MyPreference_FieldKey.equals(MyPreference.KEY_SYUKKIN_SYUBETSU)) key = MyPreference.KEY_SYUKKIN_SYUBETSU;
		else if (MyPreference_FieldKey.equals(MyPreference.KEY_SYUKKIN_EDITTIME)) key = MyPreference.KEY_SYUKKIN_EDITTIME;
		else if (MyPreference_FieldKey.equals(MyPreference.KEY_IDOU_SYORIMAISUU)) key = MyPreference.KEY_IDOU_SYORIMAISUU;
		else if (MyPreference_FieldKey.equals(MyPreference.KEY_IDOU_MOUSHIDEMAISUU)) key = MyPreference.KEY_IDOU_MOUSHIDEMAISUU;
		else if (MyPreference_FieldKey.equals(MyPreference.KEY_IDOU_GYOUMU)) key = MyPreference.KEY_IDOU_GYOUMU;
		else if (MyPreference_FieldKey.equals(MyPreference.KEY_IDOU_EDITTIME)) key = MyPreference.KEY_IDOU_EDITTIME;
		else if (MyPreference_FieldKey.equals(MyPreference.KEY_IDOU_SYUBETSU)) key = MyPreference.KEY_IDOU_SYUBETSU;
		else if (MyPreference_FieldKey.equals(MyPreference.KEY_TAIKIN_SYUBETSU)) key = MyPreference.KEY_TAIKIN_SYUBETSU;
		else if (MyPreference_FieldKey.equals(MyPreference.KEY_TAIKIN_KYUKEI)) key = MyPreference.KEY_TAIKIN_KYUKEI;
		else if (MyPreference_FieldKey.equals(MyPreference.KEY_TAIKIN_KOUTSUUHI)) key = MyPreference.KEY_TAIKIN_KOUTSUUHI;
		else if (MyPreference_FieldKey.equals(MyPreference.KEY_TAIKIN_SYORIMAISUU)) key = MyPreference.KEY_TAIKIN_SYORIMAISUU;
		else if (MyPreference_FieldKey.equals(MyPreference.KEY_TAIKIN_MOUSHIDEMAISUU)) key = MyPreference.KEY_TAIKIN_MOUSHIDEMAISUU;
		else if (MyPreference_FieldKey.equals(MyPreference.KEY_TAIKIN_GYOUMU)) key = MyPreference.KEY_TAIKIN_GYOUMU;
		else if (MyPreference_FieldKey.equals(MyPreference.KEY_TAIKIN_EDITTIME)) key = MyPreference.KEY_TAIKIN_EDITTIME;

		SharedPreferences.Editor editor = this.pref.edit();
		editor.putInt(key, status);
		return editor.commit();
	}

	/**
	 * 初期設定が完了しているかのフラグを保存する。
	 * @param MyPreference_FieldStatus
	 * @return
	 */
	public boolean writeAppInit(boolean MyPreference_FieldStatus) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putBoolean(MyPreference.KEY_APPINIT, MyPreference_FieldStatus);
		return editor.commit();
	}

	/**
	 * 接続先URLを保存する。
	 * @param url
	 * @return
	 */
	public boolean writeURL(String url) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putString(MyPreference.KEY_URL, url);
		return editor.commit();
	}

	/**
	 * 端末名を保存する。
	 * @param deviceName
	 * @return
	 */
	public boolean writeDeviceName(String deviceName) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putString(MyPreference.KEY_DEVICE_NAME, deviceName);
		return editor.commit();
	}

	/**
	 * 組織IDを保存する。
	 * @param soshikiid
	 * @return
	 */
	public boolean writeSoshikiID(String soshikiid) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putString(MyPreference.KEY_SOSHIKI_ID, soshikiid);
		return editor.commit();
	}

	/**
	 * パスワードを保存する。
	 * @param password
	 * @return
	 */
	public boolean writePassword(String password) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putString(MyPreference.KEY_PASSWORD, password);
		return editor.commit();
	}

	/**
	 * 端末IDを保存する。
	 * @param deviceID
	 * @return
	 */
	public boolean writeDeviceID(String deviceID) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putString(MyPreference.KEY_DEVICE_ID, deviceID);
		return editor.commit();
	}

	/**
	 * デフォルトURLを使用しないフラグを保存する。
	 * @param MyPreference_fieldValue
	 * @return
	 */
	public boolean writeDoNotUseDefaultURL(boolean MyPreference_fieldValue) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putBoolean(MyPreference.KEY_DO_NOT_USE_DEFAULT_URL, MyPreference_fieldValue);
		return editor.commit();
	}

	/**
	 * 項目設定の状態を取得する。
	 * @param MyPreference_FieldKey
	 * @return
	 */
	public int getNyuuryokuKoumokuCheckedPosition(String MyPreference_FieldKey) {
		int defaultValue = STATUS_HISSU;
		return this.pref.getInt(MyPreference_FieldKey, defaultValue);
	}

	/**
	 * 接続先URLを習得する。
	 * @return
	 */
	public String getURL() {
		return this.pref.getString(MyPreference.KEY_URL, null);
	}

	/**
	 * 自動で最適な接続先URLを取得する。
	 * @param context
	 * @return
	 */
	public String getURLAuto(Context context) {
		if(this.getDoNotUseDefaultURL()) {
			return this.getURL();
		}else {
			return context.getResources().getString(R.string.default_url);
		}
	}

	/**
	 * デフォルトURLを使用するかのフラグを取得する。
	 * @return
	 */
	public boolean getDoNotUseDefaultURL() {
		return this.pref.getBoolean(MyPreference.KEY_DO_NOT_USE_DEFAULT_URL, false);
	}

	/**
	 * 初期設定済みかをのフラグを取得する。
	 * @return
	 */
	public boolean getAppInit() {
		return this.pref.getBoolean(MyPreference.KEY_APPINIT, false);
	}

	/**
	 * 端末名を取得する。
	 * @return
	 */
	public String getDeviceName() {
		return this.pref.getString(KEY_DEVICE_NAME, null);
	}

	/**
	 * パスワードを取得する。
	 * @return
	 */
	public String getPassword() {
		return this.pref.getString(MyPreference.KEY_PASSWORD, null);
	}

	/**
	 * 組織IDを取得する。
	 * @return
	 */
	public String getSoshikiID() {
		return this.pref.getString(MyPreference.KEY_SOSHIKI_ID, null);
	}

	/**
	 * 端末IDを取得する。
	 * @return
	 */
	public String getDeviceID() {
		return this.pref.getString(MyPreference.KEY_DEVICE_ID, null);
	}

	/**
	 * 端末のタイプを保存する。<br>
	 * "phone":スマホ<br>
	 * "tablet":タブレット
	 * @param deviceType
	 * @return
	 */
	public boolean writeDeviceType(String deviceType) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putString(MyPreference.KEY_DEVICE_TYPE, deviceType);
		return editor.commit();
	}
	/**
	 * 端末のタイプを取得する。<br>
	 * "phone":スマホ<br>
	 * "tablet":タブレット
	 * @return
	 */
	public String getDeviceType() {
		return this.pref.getString(MyPreference.KEY_DEVICE_TYPE, null);
	}
	/**
	 * この端末は携帯か否かを取得する。
	 * @return
	 */
	public boolean isPhone() {
		String type = this.getDeviceType();
		if(type == null) return false;
		if(type.equals("phone")) return true;
		else return false;
	}

	/**
	 * テキストサイズを設定します。
	 * @param textSize
	 * @return
	 */
	public boolean writeNormalTextSize(float textSize) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putFloat(MyPreference.KEY_DEVICE_TYPE, textSize);
		return editor.commit();
	}
	/**
	 * テキストサイズを取得します。
	 * @return
	 */
	public float getNormalTextSize() {
		return this.pref.getFloat(MyPreference.KEY_NORMAL_TEXTSIZE, 0);
	}

	/**
	 * 業務内容のテンプレートを保存する。<br>
	 * numberはnormal(1~9),select(1~50)の範囲内で指定
	 * @param number
	 * @param text
	 * @return
	 */
	public boolean writeTextSet(int number, String text, int type) {
		SharedPreferences.Editor editor = this.pref.edit();
		if (type == GYOUMU_TYPE_NORMAL) {
			if (number < 1 && KEY_TEXTSETS.length < number) return false;
			editor.putString(MyPreference.KEY_TEXTSETS[number-1], text);
		} else if (type == GYOUMU_TYPE_SELECT) {
			if (number < 1 && KEY_TEXTSETS_SELECT.length < number) return false;
			editor.putString(MyPreference.KEY_TEXTSETS_SELECT[number-1], text);
		}
		return editor.commit();
	}
	/**
	 * 業務内容のテンプレートを取得する。<br>
	 * numberはnormal(1~9),select(1~50)の範囲内で指定<br>
	 * 未設定時は""を返却
	 * @param number
	 * @return
	 */
	public String getTextSet(int number, int type) {
		if (type == GYOUMU_TYPE_NORMAL) {
			if (number < 1 && KEY_TEXTSETS.length < number) return null;
			return this.pref.getString(MyPreference.KEY_TEXTSETS[number-1], "");
		} else if (type == GYOUMU_TYPE_SELECT) {
			if (number < 1 && KEY_TEXTSETS_SELECT.length < number) return null;
			return this.pref.getString(MyPreference.KEY_TEXTSETS_SELECT[number-1], "");
		}
		return null;
	}

	/**
	 * バージョン確認日付を記録します。
	 * @param currentTimeMillis
	 * @return
	 */
	public boolean writeUpdateCheckDate(long currentTimeMillis) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putLong(KEY_UPDATE_CHECK_DATE, currentTimeMillis);
		return editor.commit();
	}
	/**
	 * バージョン確認日付を取得します。
	 * @return
	 */
	public long getUpdateCheckDate() {
		return this.pref.getLong(KEY_UPDATE_CHECK_DATE, 0);
	}

	/**
	 * 自動バージョン確認を行うかのフラグを記録します。
	 * @param flag
	 * @return
	 */
	public boolean writeUpdateCheckFlag(boolean flag) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putBoolean(KEY_UPDATE_CHECK_FLAG, flag);
		return editor.commit();
	}
	/**
	 * 自動バージョン確認を行うかのフラグを取得します。
	 * @return
	 */
	public boolean getUpdateCheckFlag() {
		return this.pref.getBoolean(KEY_UPDATE_CHECK_FLAG, true);
	}


	/**
	 * 移動を行うかのフラグを記録します。
	 * @param flag
	 * @return
	 */
	public boolean writeIdouFlag(boolean flag) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putBoolean(KEY_IDOU_FLAG, flag);
		return editor.commit();
	}
	/**
	 * 移動を行うかのフラグを取得します。
	 * @return
	 */
	public boolean getIdouFlag() {
//		return this.pref.getBoolean(KEY_IDOU_FLAG, true);
		// 新バージョン用（移動は消滅）
		return false;
	}

	/**
	 * 出勤時間変更を行うかのフラグを記録します。
	 * @param flag
	 * @return
	 */
	public boolean writeInTimeChangeFlag(boolean flag) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putBoolean(KEY_INTIME_CHANGE_FLAG, flag);
		return editor.commit();
	}
	/**
	 * 出勤時間変更を行うかのフラグを取得します。
	 * @return
	 */
	public boolean getInTimeChangeFlag() {
		return this.pref.getBoolean(KEY_INTIME_CHANGE_FLAG, false);
	}

	/**
	 * 退勤時間変更を行うかのフラグを記録します。
	 * @param flag
	 * @return
	 */
	public boolean writeOutTimeChangeFlag(boolean flag) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putBoolean(KEY_OUTTIME_CHANGE_FLAG, flag);
		return editor.commit();
	}
	/**
	 * 退勤時間変更を行うかのフラグを取得します。
	 * @return
	 */
	public boolean getOutTimeChangeFlag() {
		return this.pref.getBoolean(KEY_OUTTIME_CHANGE_FLAG, false);
	}

	/**
	 * 所定出勤時間を記録します。
	 * @param hhmm
	 * @return
	 */
	public boolean writeWorkingHourSyukkin(int hhmm) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putInt(KEY_WORKINGHOUR_SYUKKIN_TIME, hhmm);
		return editor.commit();
	}
	/**
	 * 所定出勤時間を取得します。<br>
	 * 初期値は900（午前9時）です。
	 * @return
	 */
	public int getWorkingHourSyukkin() {
		return this.pref.getInt(KEY_WORKINGHOUR_SYUKKIN_TIME, 900);
	}

	/**
	 * 所定退勤時間を記録します。
	 * @param hhmm
	 * @return
	 */
	public boolean writeWorkingHourTaikin(int hhmm) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putInt(KEY_WORKINGHOUR_TAIKIN_TIME, hhmm);
		return editor.commit();
	}
	/**
	 * 所定退勤時間を取得します。<br>
	 * 初期値は1700（午後5時）です。
	 * @return
	 */
	public int getWorkingHourTaikin() {
		return this.pref.getInt(KEY_WORKINGHOUR_TAIKIN_TIME, 1700);
	}

	/**
	 * 早出承認確認のフラグを記録します。
	 * @param flag
	 * @return
	 */
	public boolean writeFastSyukkinConfirmFlag(boolean flag) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putBoolean(KEY_FASTSYUKKIN_CONFIRM_FLAG, flag);
		return editor.commit();
	}
	/**
	 * 早出承認確認のフラグを取得します。初期値false
	 * @return
	 */
	public boolean getFastSyukkinConfirmFlag() {
		return this.pref.getBoolean(KEY_FASTSYUKKIN_CONFIRM_FLAG, false);
	}

	/**
	 * 早出承認確認時間を記録します。
	 * @param hhmm
	 * @return
	 */
	public boolean writeFastSyukkinConfirmTime(int hhmm) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putInt(KEY_FASTSYUKKIN_CONFIRM_TIME, hhmm);
		return editor.commit();
	}
	/**
	 * 早出承認確認時間を取得します。初期値は845（午前8時45分）です。
	 * @return
	 */
	public int getFastSyukkinConfirmTime() {
		return this.pref.getInt(KEY_FASTSYUKKIN_CONFIRM_TIME, 845);
	}

	/**
	 * 早出パスワード入力のフラグを記録します。
	 * @param flag
	 * @return
	 */
	public boolean writeFastSyukkinPasswordFlag(boolean flag) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putBoolean(KEY_FASTSYUKKIN_PASSWORD_FLAG, flag);
		return editor.commit();
	}
	/**
	 * 早出パスワード入力のフラグを取得します。初期値false
	 * @return
	 */
	public boolean getFastSyukkinPasswordFlag() {
		return this.pref.getBoolean(KEY_FASTSYUKKIN_PASSWORD_FLAG, false);
	}

	/**
	 * 早出パスワード入力要求時間を記録します。
	 * @param hhmm
	 * @return
	 */
	public boolean writeFastSyukkinPasswordTime(int hhmm) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putInt(KEY_FASTSYUKKIN_PASSWORD_TIME, hhmm);
		return editor.commit();
	}
	/**
	 * 早出パスワード入力要求時間を取得します。初期値は830（午前8時30分）です。
	 * @return
	 */
	public int getFastSyukkinPasswordTime() {
		return this.pref.getInt(KEY_FASTSYUKKIN_PASSWORD_TIME, 830);
	}

	/**
	 * 残業承認確認のフラグを記録します。
	 * @param flag
	 * @return
	 */
	public boolean writeLateTaikinConfirmFlag(boolean flag) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putBoolean(KEY_LATETAIKIN_CONFIRM_FLAG, flag);
		return editor.commit();
	}
	/**
	 * 残業承認確認のフラグを取得します。初期値true
	 * @return
	 */
	public boolean getLateTaikinConfirmFlag() {
		return this.pref.getBoolean(KEY_LATETAIKIN_CONFIRM_FLAG, true);
	}

	/**
	 * 残業承認確認時間を記録します。
	 * @param hhmm
	 * @return
	 */
	public boolean writeLateTaikinConfirmTime(int hhmm) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putInt(KEY_LATETAIKIN_CONFIRM_TIME, hhmm);
		return editor.commit();
	}
	/**
	 * 残業承認確認時間を取得します。<br>
	 * 初期値は1730（午後5時30分）です。
	 * @return
	 */
	public int getLateTaikinConfirmTime() {
		return this.pref.getInt(KEY_LATETAIKIN_CONFIRM_TIME, 1730);
	}

	/**
	 * 残業パスワード入力のフラグを記録します。
	 * @param flag
	 * @return
	 */
	public boolean writeLateTaikinPasswordFlag(boolean flag) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putBoolean(KEY_LATETAIKIN_PASSWORD_FLAG, flag);
		return editor.commit();
	}
	/**
	 * 残業パスワード入力のフラグを取得します。初期値true
	 * @return
	 */
	public boolean getLateTaikinPasswordFlag() {
		return this.pref.getBoolean(KEY_LATETAIKIN_PASSWORD_FLAG, true);
	}

	/**
	 * 残業パスワード入力要求時間を記録します。
	 * @param hhmm
	 * @return
	 */
	public boolean writeLateTaikinPasswordTime(int hhmm) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putInt(KEY_LATETAIKIN_PASSWORD_TIME, hhmm);
		return editor.commit();
	}
	/**
	 * 残業パスワード入力要求時間を取得します。初期値は1745（午後5時45分）です。
	 * @return
	 */
	public int getLateTaikinPasswordTime() {
		return this.pref.getInt(KEY_LATETAIKIN_PASSWORD_TIME, 1745);
	}

	/**
	 * 遅延出勤に対する許容時間(分)を記録します。<br>
	 * 例えば5(分)と設定した場合、規定出勤時間から5分以内であれば特別に規定出勤時間で打刻が許可されます。
	 * @param minute
	 * @return
	 */
	public boolean writeLateSyukkinPermissionMinute(int minute) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putInt(KEY_LATESYUKKIN_PERMISSION_MINUTE, minute);
		return editor.commit();
	}

	/**
	 * 遅延出勤の許容時間を取得します。初期値は5(分)です。
	 * @return
	 */
	public int getLateSyukkinPermissionMinute() {
		return this.pref.getInt(KEY_LATESYUKKIN_PERMISSION_MINUTE, 5);
	}

	/**
	 * 業務内容の入力方法を設定します。
	 * @param type
	 * @return
	 */
	public boolean writeGyoumuType(int type) {
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putInt(KEY_GYOUMU_TYPE, type);
		return editor.commit();
	}

	/**
	 * 業務内容の入力方法を取得します。初期値は「GYOUMU_TYPE_NORMAL」です。
	 * @return
	 */
	public int getGyoumuType() {
		return this.pref.getInt(KEY_GYOUMU_TYPE, GYOUMU_TYPE_NORMAL);
	}






	/**
	 * 勤務種別の選択履歴を記録します。<br>
	 * すでに同じものが登録されている場合は登録はせず、順番だけを変更します。<br>
	 * 例）◆=最新データ, ●=次保存場所<br>
	 * now order:1,2,3,4,5__Add >> 3<br>
	 * new order:3,1,2,4,5
	 * @param wid
	 * @return
	 */
	public boolean writeSyubetsuLog(int wid) {
		if(isLogWidEqualLatastWid(wid)) {
			return true;
		}
		if(checkLogWidOverlapAndChange(wid)) {
			return true;
		}
		int nextWriteIndex = this.getSyubetsuLogNextWriteIndex();
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putInt(KEY_SYUBETSU_LOG_SETS[nextWriteIndex], wid);
		boolean result = editor.commit();
		if(result) {
			writeSyubetsuLogNextWriteIndex(nextWriteIndex);
		}
		return result;
	}

	private boolean isLogWidEqualLatastWid(int wid) {
		int nextWriteIndex = getSyubetsuLogNextWriteIndex();
		int latastIndex = nextWriteIndex - 1;
		if(latastIndex < 0) latastIndex = KEY_SYUBETSU_LOG_SETS.length - 1;
		int latastWid = this.pref.getInt(KEY_SYUBETSU_LOG_SETS[latastIndex], -1);
		if(latastWid == wid) {
			return true;
		}
		return false;
	}

	private boolean checkLogWidOverlapAndChange(int wid) {
		//最新データがある場所を取得
		int nextWriteIndex = getSyubetsuLogNextWriteIndex();
		int latastIndex = nextWriteIndex - 1;
		if(latastIndex < 0) latastIndex = KEY_SYUBETSU_LOG_SETS.length - 1;
		//重複確認
		int[] nowWids = this.getSyubetsuLogsOrder();
		int overlapIndex = -1;
		for(int i=0; i<KEY_SYUBETSU_LOG_SETS.length; i++) {
			if(nowWids[i] == wid) {
				overlapIndex = i;//重複箇所を取得
				break;
			}
		}
		if(overlapIndex == -1) {
			return false;
		}
		//最新データ位置から重複wid位置までの値を一つずつ後ろにズラす
		SharedPreferences.Editor editor = this.pref.edit();
		int changeLength = latastIndex - overlapIndex;//変更回数を計算
		if(changeLength < 0) {
			changeLength = KEY_SYUBETSU_LOG_SETS.length + changeLength;
		}
		int nowChangeIndex = overlapIndex;//入れる場所
		int getValueIndex = overlapIndex + 1;//入れる値がある場所
		if(getValueIndex >= KEY_SYUBETSU_LOG_SETS.length) {
			getValueIndex = 0;
		}
		int getValue;//入れる値
		for(int i=0; i<changeLength; i++) {
			getValue = this.pref.getInt(KEY_SYUBETSU_LOG_SETS[getValueIndex], -1);
			editor.putInt(KEY_SYUBETSU_LOG_SETS[nowChangeIndex], getValue);//入れる
			nowChangeIndex++;//次に移行
			getValueIndex++;
			if(nowChangeIndex >= KEY_SYUBETSU_LOG_SETS.length) {
				nowChangeIndex = 0;
			}
			if(getValueIndex >= KEY_SYUBETSU_LOG_SETS.length) {
				getValueIndex = 0;
			}
		}
		//最後に最新データ位置に重複widを入れる
		editor.putInt(KEY_SYUBETSU_LOG_SETS[latastIndex], wid);
		editor.commit();
		return true;
	}

	/**
	 * 勤務種別の選択履歴(５回分)一覧を、最新のものを先頭にして配列で取得します。<br>
	 * @return
	 */
	public int[] getSyubetsuLogs() {
		int[] wids = new int[KEY_SYUBETSU_LOG_SETS.length];
		int nextWriteIndex = getSyubetsuLogNextWriteIndex();
		int index = nextWriteIndex;
		index--;
		for(int i=0; i<KEY_SYUBETSU_LOG_SETS.length; i++) {
			if(index < 0) index = KEY_SYUBETSU_LOG_SETS.length - 1;
			wids[i] = this.pref.getInt(KEY_SYUBETSU_LOG_SETS[index], -1);
			index--;
		}
		return wids;
	}

	/**
	 * 勤務種別の選択履歴を配列順に取得します
	 * @return
	 */
	public int[] getSyubetsuLogsOrder() {
		int[] wids = new int[KEY_SYUBETSU_LOG_SETS.length];
		for(int i=0; i<KEY_SYUBETSU_LOG_SETS.length; i++) {
			wids[i] = this.pref.getInt(KEY_SYUBETSU_LOG_SETS[i], -1);
		}
		return wids;
	}

	/**
	 * 指定した勤務種別を履歴から削除します。<br>
	 * 履歴に存在しない場合は無視されます。return false
	 * @param logWid
	 * @return
	 */
	public boolean removeSyubetsuLog(int logWid) {
		for(String key : KEY_SYUBETSU_LOG_SETS) {
			if(logWid == this.pref.getInt(key, -1)) {
				SharedPreferences.Editor editor = this.pref.edit();
				editor.putInt(key, -1);
				return editor.commit();
			}
		}
		return false;
	}

	/**
	 * 勤務種別履歴を全て削除します。
	 * @return
	 */
	public boolean removeSyubetsuLogAll() {
		SharedPreferences.Editor editor = this.pref.edit();
		for(String key : KEY_SYUBETSU_LOG_SETS) {
			editor.putInt(key, -1);
		}
		return editor.commit();
	}

	private boolean writeSyubetsuLogNextWriteIndex(int currentIndex) {
		int nextWriteIndex = currentIndex;
		nextWriteIndex++;
		if(nextWriteIndex == KEY_SYUBETSU_LOG_SETS.length) {
			nextWriteIndex = 0;
		}
		SharedPreferences.Editor editor = this.pref.edit();
		editor.putInt(KEY_SYUBESTU_LOG_NEXT_WRITE_INDEX, nextWriteIndex);
		return editor.commit();
	}

	private int getSyubetsuLogNextWriteIndex() {
		return this.pref.getInt(KEY_SYUBESTU_LOG_NEXT_WRITE_INDEX, 0);
	}
}
