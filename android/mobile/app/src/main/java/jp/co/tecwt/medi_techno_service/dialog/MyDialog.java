package jp.co.tecwt.medi_techno_service.dialog;

import jp.co.tecwt.medi_techno_service.data.DialogHolder;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * 画面回転時でも状態を保持可能なダイアログ。
 * show()を実行することでonCreateDialog()が実行される。
 */
public class MyDialog extends DialogFragment {
	
	public static final int MODE_NORMAL = 0;
	public static final int MODE_PROGRESS = 1;
	
	private int mode;
	private String title;
	private String message;
	private String positive_text;
	private DialogInterface.OnClickListener positive_listener;
	private String negative_text;
	private DialogInterface.OnClickListener negative_listener;
	private DialogInterface.OnCancelListener cancel_listener;
	
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		if(savedInstanceState != null) {
			// 値の復帰
			this.mode = DialogHolder.INSTANCE.getMode();
			this.title = DialogHolder.INSTANCE.getTitle();
			this.message = DialogHolder.INSTANCE.getMessage();
			this.positive_text = DialogHolder.INSTANCE.getPositibeText();
			this.positive_listener = (DialogInterface.OnClickListener)DialogHolder.INSTANCE.getPositibeListener();
			this.negative_text = DialogHolder.INSTANCE.getNegativeText();
			this.negative_listener = (DialogInterface.OnClickListener)DialogHolder.INSTANCE.getNegatibeListener();
			this.cancel_listener = (DialogInterface.OnCancelListener)DialogHolder.INSTANCE.getCancelListener();
		}
		
		// モード確認
		Dialog dialog = null;
		switch(this.mode) {
		case MODE_NORMAL:
			dialog = createNormalDialog();
			break;
		case MODE_PROGRESS:
			dialog = createProgressDialog();
			break;
		}

		return dialog;
	}
	
	/**
	 * 通常ダイアログ作成。
	 * @return
	 */
	private Dialog createNormalDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
		if(this.title != null) builder.setTitle(this.title);
		if(this.message != null) builder.setMessage(this.message);
		if(this.positive_text != null) {
			builder.setPositiveButton(
					this.positive_text, 
					this.positive_listener != null ? this.positive_listener:null);
		}
		if(this.negative_text != null) {
			builder.setNegativeButton(
					this.negative_text, 
					this.negative_listener != null ? this.negative_listener:null);
		}
		return builder.create();
	}
	
	/**
	 * プログレスダイアログ作成。
	 * @return
	 */
	private Dialog createProgressDialog() {
		ProgressDialog dialog = new ProgressDialog(this.getActivity());
		dialog.setTitle(this.title);
		dialog.setMessage(this.message);
		dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		if(this.cancel_listener != null) {
			dialog.setCancelable(true);
			dialog.setOnCancelListener(this.cancel_listener);
		}else {
			dialog.setCancelable(false);
		}
		return dialog;
	}
	
	/**
	 * モードを設定する。
	 * @param MyDialog_FieldMode
	 * @return
	 */
	public MyDialog setMode(int MyDialog_FieldMode) {
		this.mode = MyDialog_FieldMode;
		return this;
	}

	/**
	 * タイトルを設定する。
	 * @param title
	 * @return
	 */
	public MyDialog setTitle(String title) {
		this.title = title;
		return this;
	}

	/**
	 * 本文を設定する。
	 * @param message
	 * @return
	 */
	public MyDialog setMessage(String message) {
		this.message = message;
		return this;
	}

	/**
	 * ポジティブボタンを設定する。
	 * @param text
	 * @param positive_listener
	 * @return
	 */
	public MyDialog setPositive_button(String text, DialogInterface.OnClickListener positive_listener) {
		this.positive_text = text;
		this.positive_listener = positive_listener;
		return this;
	}

	/**
	 * ネガティブボタンを設定する。
	 * @param text
	 * @param negative_listener
	 * @return
	 */
	public MyDialog setNegative_button(String text, DialogInterface.OnClickListener negative_listener) {
		this.negative_text = text;
		this.negative_listener = negative_listener;
		return this;
	}
	
	/**
	 * キャンセルリスナーを登録する。
	 * @param cancel_listener
	 * @return
	 */
	public MyDialog setCancel_button(DialogInterface.OnCancelListener cancel_listener) {
		this.cancel_listener = cancel_listener;
		return this;
	}
	
	
	@Override
	public void onPause() {
		super.onPause();
		
		// 値を保存する
		DialogHolder.INSTANCE.setMode(this.mode);
		DialogHolder.INSTANCE.setTitle(this.title);
		DialogHolder.INSTANCE.setMessage(this.message);
		DialogHolder.INSTANCE.setPositiveText(this.positive_text);
		DialogHolder.INSTANCE.setPositiveListener(this.positive_listener);
		DialogHolder.INSTANCE.setNegativeText(this.negative_text);
		DialogHolder.INSTANCE.setNegativeListener(this.negative_listener);
		DialogHolder.INSTANCE.setCancelListener(this.cancel_listener);
	}

}
