package jp.co.tecwt.medi_techno_service.view;

import java.util.ArrayList;

import jp.co.tecwt.medi_techno_service.data.MyPreference;
import jp.co.tecwt.medi_techno_service.data.Singleton;
import jp.co.tecwt.medi_techno_service.dialog.MyDialog;
import jp.co.tecwt.medi_techno_service.util.MyHttpClient2;
import jp.co.tecwt.medi_techno_service.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;

/**
 * ID登録画面。
 */
public class IDEditActivity extends BaseActivity {
	
	private ArrayAdapter<String> adapter;
	private GridView gridview;
	private ArrayList<Integer> uidList;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// レイアウトファイル適用
		setContentView(R.layout.activity_idedit);
		
		this.adapter = new ArrayAdapter<String>(this, R.layout.item_idedit);
		
		((Button)this.findViewById(R.id.button_back)).setOnClickListener(this.backBtn);
		((Button)this.findViewById(R.id.button_top)).setOnClickListener(this.topBtn);
		
		// リスナーおよびアダプター登録
		this.gridview = (GridView)this.findViewById(R.id.gridview_editor);
		this.gridview.setAdapter(this.adapter);
		this.gridview.setEmptyView(this.findViewById(R.id.textview_nodata));
		this.gridview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// 次の画面へ
				Singleton.INSTANCE.setIDEditEditorID(IDEditActivity.this.uidList.get(position));
				Singleton.INSTANCE.setIDEditEditorName(parent.getAdapter().getItem(position).toString());
				
				Intent nextIntent = new Intent(IDEditActivity.this, IDEditNfcReadActivity.class);
				IDEditActivity.this.startActivityForResult(nextIntent, 0);
			}
		});
		
		this.getEditorData2();
	}
	
	/**
	 * ID登録希望者一覧を取得する。
	 */
	private void getEditorData2() {
		Resources resources = this.getResources();
		String url = resources.getString(R.string.idedit_url);
		int timeout_connect = resources.getInteger(R.integer.timeout_connect);
		int timeout_read = resources.getInteger(R.integer.timeout_read);
		MyHttpClient2 client = new MyHttpClient2(this, url, this.responser);
		client.enabledProgressDialog(this, "通信中", "しばらくお待ちください", null);
		client.setTimeOut(timeout_connect, timeout_read);
		client.addPostParam(
				resources.getString(R.string.idedit_getwaitlist_param_name1), 
				resources.getString(R.string.idedit_getwaitlist_param_value1));
		client.addPostParam(
				resources.getString(R.string.idedit_getwaitlist_param_name2), 
				MyPreference.getInstance(this).getSoshikiID());
		client.execute();
	}
	

	/**
	 * レスポンス受け取り
	 */
	private MyHttpClient2.Responser responser = new MyHttpClient2.Responser() {
		
		@Override
		public void onFailed() {
			// 失敗
			IDEditActivity.this.showErrorDialog();
		}
		
		@Override
		public void onError(String errorcode, String message) {
			// 人為的失敗
			IDEditActivity.this.showErrorDialog(errorcode, message);
		}
		
		@Override
		public void onCompleted(String json) {
			// 成功
			try {
				// root
				JSONObject rootObj = new JSONObject(json);
				Log.d("ID Edit", rootObj.toString(4));

				IDEditActivity.this.adapter.clear();
				
				// data
				JSONArray dataArray = rootObj.getJSONArray("data");
				IDEditActivity.this.uidList = new ArrayList<Integer>();
				for(int i=0; i<dataArray.length(); i++) {
					JSONObject data = dataArray.getJSONObject(i);
					
					// uid
					Integer uid = data.getInt("uid");
					
					// name
					String name = data.getString("name");
					
					// アダプターに追加
					IDEditActivity.this.uidList.add(uid);
					IDEditActivity.this.adapter.add(name);
				}
				
				// 描画更新
				IDEditActivity.this.adapter.notifyDataSetChanged();
				return;
				
			} catch(JSONException ex) {
				ex.printStackTrace();
			}

			// JSONに問題あり
			IDEditActivity.this.showErrorDialog();
			return;
		}
	};
	
	private void showErrorDialog() {
		this.showErrorDialog(null, null);
	}
	
	private void showErrorDialog(String errorcode, String message) {
		
		// 多重起動阻止
		final String DIALOG_NAME = this.getClass().getName();
		if(this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
			return;
		}
		
		String dialogMessage;
		if(errorcode == null && message == null) {
			dialogMessage = "エラーが発生しました。お手数ですが再度操作を行ってください。";
		}else {
			dialogMessage = "エラーが発生しました。お手数ですが再度操作を行ってください。"+"\n―――"+"\nerrorcode: "+errorcode+"\nmessage: "+message;
		}
		MyDialog dialog = new MyDialog();
		dialog.setMode(MyDialog.MODE_NORMAL);
		dialog.setTitle("ERROR!!");
		dialog.setMessage(dialogMessage);
		dialog.setPositive_button("OK", new DialogInterface.OnClickListener() {			
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				IDEditActivity.this.setResult(AdminActivity.RESULT_ID_EDIT_NG);
				IDEditActivity.this.finish();
			}
		});
		dialog.setCancelable(false);
		dialog.show(this.getFragmentManager(), DIALOG_NAME);
	}
	
	/**
	 * 縦横に合わせて列数を変更する
	 * @param config
	 */
	private void setGridViewColumn(Configuration config) {
		if(config == null) {
			return;
		}
		
		if(config.orientation == Configuration.ORIENTATION_PORTRAIT) {
			// 縦
			this.gridview.setNumColumns(1);
		}else {
			// 横
			this.gridview.setNumColumns(2);
		}
	}
	
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		this.setGridViewColumn(newConfig);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		// 次画面での終わり方に応じてこの画面の終わり方も変える
		if(resultCode == AdminActivity.RESULT_ID_EDIT_OK) {
			this.setResult(AdminActivity.RESULT_ID_EDIT_OK);
			this.finish();
		}else if(resultCode == AdminActivity.RESULT_ID_EDIT_NG) {
			this.setResult(AdminActivity.RESULT_ID_EDIT_NG);
			this.finish();
		}
	}

}
