package jp.co.tecwt.medi_techno_service.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.format.DateFormat;

public class NotifyDB extends SQLiteOpenHelper {

	public static final String DB_NAME = "notify_database";
	public static final int DB_VERSION = 1;

	public NotifyDB(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String notifySQL =
				"CREATE TABLE notify " +
				"(_id INTEGER PRIMARY KEY AUTOINCREMENT, "+
				"startdate TEXT NOT NULL, "+
				"finishdate TEXT NOT NULL, "+
				"title TEXT NOT NULL, "+
				"message TEXT NOT NULL, "+
				"showworks INTEGER NOT NULL, "+
				"showmode INTEGER NOT NULL);";
		String checkSQL =
				"CREATE TABLE check " +
				"(_id INTEGER PRIMARY KEY AUTOINCREMENT, "+
				"nid INTEGER NOT NULL, "+
				"uid INTEGER NOT NULL, "+
				"uname TEXT NOT NULL, "+
				"checkdate TEXT NOT NULL);";
		db.execSQL(notifySQL);
		db.execSQL(checkSQL);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		/*
		 * 新バージョン移行時の変更点を記述
		 * ・カラム増減
		 * ・名前変更
		 * など
		 */
	}

	/**
	 * 全通知を取得します
	 * @param context
	 * @return
	 */
	public static List<Notify> GetNotifyList(Context context) throws IllegalArgumentException  {
		String notifySQL = "SELECT * FROM notify ORDER BY finishdate DESC, startdate DESC, _id DESC;";
		SQLiteDatabase db = new NotifyDB(context).getReadableDatabase();
		Cursor nCur = db.rawQuery(notifySQL, new String[]{});
		return toNotifyList(nCur);
	}

	/**
	 * 指定した日付が期間に含まれる通知を全て取得します
	 * @param context
	 * @param inDate
	 * @return
	 */
	public static List<Notify> GetNotifiesInDuration(Context context, Calendar inDate) {
		String date = getDateString(inDate);
		String notifySQL =
				"SELECT * " +
				"FROM notify " +
				"WHERE startdate <= ? AND ? <= finishdate" +
				"ORDER BY finishdate DESC, startdate DESC, _id DESC;";
		SQLiteDatabase db = new NotifyDB(context).getReadableDatabase();
		Cursor nCur = db.rawQuery(notifySQL, new String[]{date, date});
		return toNotifyList(nCur);
	}

	private static List<Notify> toNotifyList(Cursor nCur) throws IllegalArgumentException {
		List<Notify> list = new ArrayList<Notify>();
		if(nCur.moveToFirst()) {
			Notify notify;
			do {
				notify = new Notify();
				notify.id = nCur.getInt(nCur.getColumnIndexOrThrow("_id"));
				notify.startDate = nCur.getString(nCur.getColumnIndexOrThrow("startdate"));
				notify.finishDate = nCur.getString(nCur.getColumnIndexOrThrow("finishdate"));
				notify.title = nCur.getString(nCur.getColumnIndexOrThrow("title"));
				notify.message = nCur.getString(nCur.getColumnIndexOrThrow("message"));
				notify.SetShowWorksArray(nCur.getInt(nCur.getColumnIndexOrThrow("showworks")));
				notify.showMode = nCur.getInt(nCur.getColumnIndexOrThrow("showmode"));
				list.add(notify);
			} while(nCur.moveToNext());
		}
		return list;
	}

	/**
	 * 指定したユーザの既読状態を確認します
	 * @param context
	 * @param nid
	 * @param uid
	 * @return
	 */
	public static boolean IsCheckinNotify(Context context, int nid, int uid) {
		String checkSQL = "SELECT * FROM check WHERE nid=? AND uid=?;";
		SQLiteDatabase db = new NotifyDB(context).getReadableDatabase();
		String n = String.valueOf(nid);
		String u = String.valueOf(uid);
		Cursor nCur = db.rawQuery(checkSQL, new String[]{n, u});
		if(nCur.moveToFirst()) {
			return true;
		}
		return false;
	}

	/**
	 * 通知を新規作成します
	 * @param context
	 * @param notify
	 * @return
	 */
	public static boolean CreateNotify(Context context, Notify notify) {
		SQLiteDatabase db = new NotifyDB(context).getWritableDatabase();
		ContentValues val = new ContentValues();
		val.put("startdate", notify.startDate);
		val.put("finishdate", notify.finishDate);
		val.put("title", notify.title);
		val.put("message", notify.message);
		val.put("showworks", notify.GetConvertedShowWorks());
		val.put("showmode", notify.showMode);
		long result = db.insert("notify", null, val);
		if(result == -1) {
			return false;
		}
		return true;
	}

	/**
	 * 通知の内容を変更します
	 * @param context
	 * @param notify
	 * @return
	 */
	public static boolean UpdateNotify(Context context, Notify notify) {
		SQLiteDatabase db = new NotifyDB(context).getWritableDatabase();
		ContentValues val = new ContentValues();
		val.put("startdate", notify.startDate);
		val.put("finishdate", notify.finishDate);
		val.put("title", notify.title);
		val.put("message", notify.message);
		val.put("showworks", notify.GetConvertedShowWorks());
		val.put("showmode", notify.showMode);
		long result = db.update("notify", val, "_id=?", new String[]{String.valueOf(notify.id)});
		if(result == -1) {
			return false;
		}
		return true;
	}

	/**
	 * 通知を削除します
	 * @param context
	 * @param nid
	 * @return
	 */
	public static boolean DeleteNotify(Context context, int nid) {
		SQLiteDatabase db = new NotifyDB(context).getWritableDatabase();
		long result = db.delete("notify", "_id=?", new String[]{String.valueOf(nid)});
		if(result == -1) {
			return false;
		}
		return true;
	}

	/**
	 * 既読にします<br>
	 * （すでに既読状態の場合に期間中表示する場合は日付だけを更新します）
	 * @param context
	 * @param notify
	 * @param nid
	 * @param uid
	 * @param uname
	 * @param checkDate
	 * @return
	 */
	public static boolean CheckinNotify(Context context, Notify notify, int uid, String uname, Calendar checkDate) {
		SQLiteDatabase db = new NotifyDB(context).getWritableDatabase();
		ContentValues val = new ContentValues();
		val.put("checkdate", getDateString(checkDate));
		long result;
		if (IsCheckinNotify(context, notify.id, uid)) {
			if (notify.showMode == Notify.SHOW_MODE_DURATION) {
				String n = String.valueOf(notify.id);
				String u = String.valueOf(uid);
				result = db.update("check", val, "nid=? AND uid=?", new String[]{n, u});
			}
			else {
				return true;
			}
		} else {
			val.put("nid", notify.id);
			val.put("uid", uid);
			val.put("uname", uname);
			result = db.insert("check", null, val);
		}
		if(result == -1) {
			return false;
		}
		return true;
	}

	private static String getDateString(Calendar date) {
		return DateFormat.format("yyyy-MM-dd", date).toString();
	}

	public static class Notify {
		public static enum SHOW_WORKS {IN, MOVE, OUT}
		public static final String SHOW_WORKS_SEPARATE = ", ";
		public static final int SHOW_MODE_ONCE = 1;//初回のみ
		public static final int SHOW_MODE_DURATION = 2;//期間中ずっと
		public int id;
		public String startDate;
		public String finishDate;
		public String title;
		public String message;
		public SHOW_WORKS[] showWorks;
		public int showMode;
		public void SetShowWorksArray(int showWorksNumber) {
			if(showWorksNumber == 0) {
				this.showWorks = new SHOW_WORKS[0];
				return;
			}
			// ビットの左から順に[退勤][移動][出勤]
			// 例）4 => [1][0][0] => [退勤]時のみ
			String flags = Integer.toBinaryString(showWorksNumber);
			flags = String.format("%3s", flags).replace(' ', '0');	// 先頭0埋め3桁確保
			List<SHOW_WORKS> list = new ArrayList<SHOW_WORKS>();
			if (flags.charAt(0) == '1') list.add(SHOW_WORKS.OUT);
			if (flags.charAt(1) == '1') list.add(SHOW_WORKS.MOVE);
			if (flags.charAt(2) == '1') list.add(SHOW_WORKS.IN);
			this.showWorks = (SHOW_WORKS[])list.toArray(new SHOW_WORKS[0]);
		}
		public String GetShowModeStr() {
			switch(showMode) {
				case SHOW_MODE_ONCE:
					return "初回のみ";
				case SHOW_MODE_DURATION:
					return "期間中公開";
				default:
					return "不明";
			}
		}
		public String GetShowWorksStr() {
			if (showWorks == null || showWorks.length == 0) {
				return "不明";
			}
			String str = "";
			for (SHOW_WORKS work : showWorks) {
				if (work == SHOW_WORKS.IN) {
					str = str + "出" + SHOW_WORKS_SEPARATE;
				} else if (work == SHOW_WORKS.MOVE) {
					str = str + "移動" + SHOW_WORKS_SEPARATE;
				} else if (work == SHOW_WORKS.OUT) {
					str = str + "退" + SHOW_WORKS_SEPARATE;
				} else {
					str = str + "不明" + SHOW_WORKS_SEPARATE;
				}
			}
			if (str.length() > SHOW_WORKS_SEPARATE.length()) {
				str = str.substring(0, str.length() - SHOW_WORKS_SEPARATE.length());
			}
			return str;
 		}
		public int GetConvertedShowWorks() {
			int value = 0;
			for (SHOW_WORKS work : showWorks) {
				switch (work) {
				case IN:
					value = value + 1;
					break;
				case MOVE:
					value = value + 10;
					break;
				case OUT:
					value = value + 100;
					break;
				default:
					break;
				}
			}
			return Integer.parseInt(String.valueOf(value), 2);
		}
	}

	public static class Check {
		public int id;
		public int nid;
		public int uid;
		public String uname;
		public String checkdate;
	}
}
