package jp.co.tecwt.medi_techno_service.data;

import java.net.CookieManager;

public enum Session {
	
	INSTANCE;
	
	private String sessionID;
	private CookieManager cookieManager;
	
	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}	
	public String getSessionID() {
		return this.sessionID;
	}
	
	public void setCookieManager(CookieManager cookieManager) {
		this.cookieManager = cookieManager;
	}
	public CookieManager getCookieManager() {
		return this.cookieManager;
	}
	
	public void init() {
		this.sessionID = null;
		this.cookieManager = null;
	}

}
