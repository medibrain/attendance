package jp.co.tecwt.medi_techno_service.data;

import android.util.SparseArray;


/**
 * シングルトン。
 * Activity間で値を共有するためのもの。
 */
public enum Singleton {

	INSTANCE;

	private String id;
	private long datetime_edit;
	private long datetime_auto;
	private String syuttai;
	private int syuttaiStatus;
	private String syubetsu;
	private int syubetsuId;
	private double gps_latitude;
	private double gps_longitude;
	private String gyoumu;
	private String kyukei;
	private String koutsuuhi;
	private String syorimaisuu;
	private String moushidemaisuu;
	private int idedit_editorid;
	private String idedit_editorname;
	private String idedit_readid;
	private boolean idedit_isConfirm;
	private boolean idedit_isRandom;
	private String soshikiid;
	private String appinit_devicename;
	private String appinit_password;
	private String appinit_soshikiid;
	private String appinit_deviceid;
	private SparseArray<String> deleteDates = new SparseArray<String>();


	public void setID(String id) { this.id = id; }
	public String getID() { return this.id; }

	public void setDatetimeEdit(long datetime_edit) { this.datetime_edit = datetime_edit; }
	public long getDatetimeEdit() { return this.datetime_edit; }

	public void setDatetimeAuto(long datetime_auto) { this.datetime_auto = datetime_auto; }
	public long getDatetimeAuto() { return this.datetime_auto; }

	public void setSyuttai(String syuttai) { this.syuttai = syuttai; }
	public String getSyuttai() { return this.syuttai; }

	public void setSyuttaiStatus(int syuttaiStatus) { this.syuttaiStatus = syuttaiStatus; }
	public int getSyuttaiStatus() { return this.syuttaiStatus; }

	public void setSyubetsu(String syubetsu) { this.syubetsu = syubetsu; }
	public String getSyubetsu() { return this.syubetsu; }

	public void setSyubetsuId(int syubetsuId) { this.syubetsuId = syubetsuId; }
	public int getSyubetsuId() { return this.syubetsuId; }

	public void setLat(double lat) { this.gps_latitude = lat; }
	public double getLat() { return this.gps_latitude; }

	public void setLng(double lng) { this.gps_longitude = lng; }
	public double getLng() { return this.gps_longitude; }

	public void setGyoumu(String gyoumu) { this.gyoumu = gyoumu; }
	public String getGyoumu() { return this.gyoumu; }

	public void setKyukei(String kyukei) { this.kyukei = kyukei; }
	public String getKyukei() { return this.kyukei; }

	public void setKoutsuuhi(String koutsuuhi) { this.koutsuuhi = koutsuuhi; }
	public String getKoutsuuhi() { return this.koutsuuhi; }

	public void setSyoriMaisuu(String syorimaisuu) { this.syorimaisuu = syorimaisuu; }
	public String getSyoriMaisuu() { return this.syorimaisuu; }

	public void setMoushideMaisuu(String moushidemaisuu) { this.moushidemaisuu = moushidemaisuu; }
	public String getMoushideMaisuu() { return this.moushidemaisuu; }

	public void setIDEditEditorID(int idedit_editorid) { this.idedit_editorid = idedit_editorid; }
	public int getIDEditEditorID() { return this.idedit_editorid; }

	public void setIDEditEditorName(String idedit_editorname) { this.idedit_editorname = idedit_editorname; }
	public String getIDEditEditorName() { return this.idedit_editorname; }

	public void setIDEditReadID(String idedit_readid) { this.idedit_readid = idedit_readid; }
	public String getIDEditReadID() { return this.idedit_readid; }

	public void setIDEditIsConfirm(boolean idedit_isConfirm) { this.idedit_isConfirm = idedit_isConfirm; }
	public boolean getIDEditIsConfirm() { return this.idedit_isConfirm; }

	public void setIDEditIsRandom(boolean idedit_isRandom) { this.idedit_isRandom = idedit_isRandom; }
	public boolean getIDEditIsRandom() { return this.idedit_isRandom; }

	public void setSoshikiID(String soshikiid) { this.soshikiid = soshikiid; }
	public String getSoshikiID() { return this.soshikiid; }

	public void setAppInit_DeviceName(String deviceName) { this.appinit_devicename = deviceName; }
	public String getAppInit_DeviceName() { return this.appinit_devicename; }

	public void setAppInit_Password(String pass) { this.appinit_password = pass; }
	public String getAppInit_Password() { return this.appinit_password; }

	public void setAppInit_SoshikiID(String soshikiid) { this.appinit_soshikiid = soshikiid; }
	public String getAppInit_SoshikiID() { return this.appinit_soshikiid; }

	public void setAppInit_DeviceID(String deviceid) { this.appinit_deviceid = deviceid; }
	public String getAppInit_DeviceID() { return this.appinit_deviceid; }

	public void addLog_DeleteDate(int id, String date) { this.deleteDates.put(id, date); }
	public String getLog_DeleteDate(int id) { return this.deleteDates.get(id); }
	public void removeLog_DeleteDate(int id) { this.deleteDates.remove(id); }
	public int getLog_DeleteDateSize() { return this.deleteDates.size(); }
	public String[] getLog_DeleteDateArray() {
		String[] dates = new String[this.deleteDates.size()];
		for(int i=0; i<this.deleteDates.size(); i++) {
			dates[i] = this.deleteDates.valueAt(i);
		}
		return dates;
	}
	public void clear_DeleteDates() { this.deleteDates.clear(); }


	public void init() {
		this.id = null;
		this.datetime_edit = 0;
		this.datetime_auto = 0;
		this.syuttai = null;
		this.syuttaiStatus = 0;
		this.syubetsu = null;
		this.syubetsuId = 0;
//		this.gps_latitude = 0;Mainに帰るたびに初期化していては取得が間に合わないことがあるので、
//		this.gps_longitude = 0;一度取得すればそれをずっと保持し続け、OSによってアプリが完全に終了したタイミングでのみ0,0に戻る
		this.gyoumu = null;
		this.kyukei = null;
		this.koutsuuhi = null;
		this.syorimaisuu = null;
		this.moushidemaisuu = null;
		this.idedit_editorid = 0;
		this.idedit_editorname = null;
		this.idedit_readid = null;
		this.idedit_isConfirm = false;
		this.idedit_isRandom = false;
		this.soshikiid = null;
		this.appinit_devicename = null;
		this.appinit_password = null;
		this.appinit_password = null;
		this.appinit_soshikiid = null;
		this.deleteDates.clear();
	}

}
