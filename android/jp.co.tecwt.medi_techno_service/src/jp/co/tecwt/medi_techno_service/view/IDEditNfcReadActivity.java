package jp.co.tecwt.medi_techno_service.view;

import jp.co.tecwt.medi_techno_service.data.MyPreference;
import jp.co.tecwt.medi_techno_service.data.Singleton;
import jp.co.tecwt.medi_techno_service.dialog.MyDialog;
import jp.co.tecwt.medi_techno_service.util.MyHttpClient2;
import jp.co.tecwt.medi_techno_service.R;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioManager;
import android.media.SoundPool;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * ID登録NFC読み取り画面。
 */
public class IDEditNfcReadActivity extends BaseActivity {

	private View readingView;
	private View readingView_confirm;
	private View readView;
	private View readView_error;
	private TextView readId;
	private TextView editorName;
	
	private SoundPool soundPool;
	private int soundId;
	
	
	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// レイアウトファイル適用
		setContentView(R.layout.activity_idedit_nfc);
		
		this.readingView = this.findViewById(R.id.linearlayout_reading);
		this.readingView_confirm = this.findViewById(R.id.linearlayout_reading_confirm);
		this.readView = this.findViewById(R.id.linearlayout_read);
		this.readView_error = this.findViewById(R.id.linearlayout_readed_error);
		this.readId = (TextView)this.findViewById(R.id.textview_readid);
		this.editorName = (TextView)this.findViewById(R.id.textview_editor_name);
		
		// リスナー登録
		((Button)this.findViewById(R.id.button_back)).setOnClickListener(this.backBtn);
		((Button)this.findViewById(R.id.button_top)).setOnClickListener(this.topBtn);
		((Button)this.findViewById(R.id.button_reload)).setOnClickListener(this.reloadListener);
		((Button)this.findViewById(R.id.button_reload_error)).setOnClickListener(this.reloadListener);
		((Button)this.findViewById(R.id.button_edit)).setOnClickListener(this.editListener);
		
		// 読み取り状態に応じてレイアウトを変更
		if(Singleton.INSTANCE.getIDEditReadID() == null) {
			// かざす(一回目)
			this.showReading();
		}else if(Singleton.INSTANCE.getIDEditReadID() != null && Singleton.INSTANCE.getIDEditIsConfirm() && !Singleton.INSTANCE.getIDEditIsRandom()) {
			// かざす(再度確認)
			this.showReadingConfirm();
		}else if(Singleton.INSTANCE.getIDEditReadID() != null && Singleton.INSTANCE.getIDEditIsConfirm() && Singleton.INSTANCE.getIDEditIsRandom()) {
			// かざした(ランダムIDなため登録不可状態)
			
		}else {
			// かざした(登録可能状態)
			this.showReaded();
			this.readId.setText("ID: "+Singleton.INSTANCE.getIDEditReadID());
			this.editorName.setText("名前: "+Singleton.INSTANCE.getIDEditEditorName());
		}
		
		// 効果音再生インスタンス生成
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
			// API 21 未満
			this.soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
		} else {
			// API 21以上
			SoundPool.Builder builder = new SoundPool.Builder();
			this.soundPool = builder.build();
		}
		this.soundId = this.soundPool.load(this, R.raw.loaded, 1);	// 優先度1を指定する
		
		// デフォルトはキャンセル
		this.setResult(Activity.RESULT_CANCELED);
	}
	
	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		
		if(Singleton.INSTANCE.getIDEditReadID() != null && Singleton.INSTANCE.getIDEditIsConfirm() && Singleton.INSTANCE.getIDEditIsRandom()) {
			// エラー画面では無視
			return;
		}
		
		// ID読み取り
		byte[] idBytes = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
		StringBuilder builder = new StringBuilder();
        for (byte b : idBytes) {
            String hex = String.format("%02X", b);	// %02X = 必ず2桁にする
            builder.append(hex);
        }
        String id = builder.toString().trim();
        
		// NFC読込成功時の効果音
		this.soundPool.play(this.soundId, 0.5f, 0.5f, 0, 0, 1);
        
        // IDMのランダム性判定
        if(Singleton.INSTANCE.getIDEditIsConfirm()) {
        	String firstID = Singleton.INSTANCE.getIDEditReadID();
        	if(firstID.equals(id)) {
                // IDMがランダムなものではないので登録を許可
                this.readId.setText("ID: "+id);
                this.editorName.setText("名前: "+Singleton.INSTANCE.getIDEditEditorName());
                this.showReaded();
                return;
        	}else {
        		// IDMがランダムなので登録不可
        		this.showReadedError();
        		return;
        	}
        }
        
        // トースト表示
        Toast.makeText(this, "読取: "+id, Toast.LENGTH_LONG).show();
        
        // 保存
        Singleton.INSTANCE.setIDEditReadID(id);
        this.showReadingConfirm();
	}
	
	private void showReading() {
		Singleton.INSTANCE.setIDEditReadID(null);
		Singleton.INSTANCE.setIDEditIsConfirm(false);
        this.readingView.setVisibility(View.VISIBLE);
        this.readingView_confirm.setVisibility(View.GONE);
        this.readView.setVisibility(View.GONE);
        this.readView_error.setVisibility(View.GONE);
	}
	
	private void showReadingConfirm() {
		Singleton.INSTANCE.setIDEditIsConfirm(true);
        this.readingView.setVisibility(View.GONE);
        this.readingView_confirm.setVisibility(View.VISIBLE);
        this.readView.setVisibility(View.GONE);
        this.readView_error.setVisibility(View.GONE);
	}
	
	private void showReaded() {
		Singleton.INSTANCE.setIDEditIsConfirm(true);
        this.readingView.setVisibility(View.GONE);
        this.readingView_confirm.setVisibility(View.GONE);
        this.readView.setVisibility(View.VISIBLE);
        this.readView_error.setVisibility(View.GONE);
	}
	
	private void showReadedError() {
        this.readingView.setVisibility(View.GONE);
        this.readingView_confirm.setVisibility(View.GONE);
        this.readView.setVisibility(View.GONE);
        this.readView_error.setVisibility(View.VISIBLE);
	}

	
	/**
	 * やり直す
	 */
	private OnClickListener reloadListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			IDEditNfcReadActivity.this.showReading();
			// NFC読み取り
			IDEditNfcReadActivity.this.nfcEnableForegroundDispatch();
		}
	};
	
	/**
	 * 登録
	 */
	private OnClickListener editListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			// 送信
			Resources resources = IDEditNfcReadActivity.this.getResources();
			MyHttpClient2 client = new MyHttpClient2(
					IDEditNfcReadActivity.this, 
					resources.getString(R.string.idedit_url), 
					IDEditNfcReadActivity.this.responser);
			client.enabledProgressDialog(IDEditNfcReadActivity.this, "通信中", "しばらくお待ちください", null);
			client.setTimeOut(resources.getInteger(R.integer.timeout_connect), resources.getInteger(R.integer.timeout_read));
			client.addPostParam(
					resources.getString(R.string.idedit_sendid_param_name1), 
					resources.getString(R.string.idedit_sendid_param_value1));
			client.addPostParam(
					resources.getString(R.string.idedit_sendid_param_name2), 
					MyPreference.getInstance(IDEditNfcReadActivity.this).getSoshikiID());
			client.addPostParam(
					resources.getString(R.string.idedit_sendid_param_name3), 
					String.valueOf(Singleton.INSTANCE.getIDEditEditorID()));
			client.addPostParam(
					resources.getString(R.string.idedit_sendid_param_name4), 
					Singleton.INSTANCE.getIDEditReadID());
			client.execute();
			
			// NFC読み取り解除
			IDEditNfcReadActivity.this.nfcDisableForegroundDispatch();
		}
	};
	
	private MyHttpClient2.Responser responser = new MyHttpClient2.Responser() {
		
		@Override
		public void onFailed() {
			// 失敗
			IDEditNfcReadActivity.this.showErrorDialog();
		}
		
		@Override
		public void onError(String errorcode, String message) {
			// 人為的失敗
			IDEditNfcReadActivity.this.showErrorDialog(errorcode, message);
		}
		
		@Override
		public void onCompleted(String json) {
			// 成功
			try {
				// root
				JSONObject rootObj = new JSONObject(json);
				Log.d("ID Edit Nfc", rootObj.toString(4));
				
				// name
				//String name = rootObj.getString("name");
				
				// idm
				//String idm = rootObj.getString("idm");
				
				// message
				//String message = rootObj.getString("message");
				
				// 成功
				IDEditNfcReadActivity.this.setResult(AdminActivity.RESULT_ID_EDIT_OK);
				IDEditNfcReadActivity.this.finish();
			
			} catch(JSONException ex) {
				ex.printStackTrace();
				IDEditNfcReadActivity.this.showErrorDialog();
			}
			
			return;
		}
	};
	
	private void showErrorDialog() {
		this.showErrorDialog(null, null);
	}
	
	private void showErrorDialog(String errorcode, String message) {
		
		// 多重起動阻止
		final String DIALOG_NAME = this.getClass().getName();
		if(this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
			return;
		}
		
		String dialogMessage;
		if(errorcode == null && message == null) {
			dialogMessage = "エラーが発生しました。お手数ですが再度操作を行ってください。";
		}else {
			dialogMessage = "エラーが発生しました。お手数ですが再度操作を行ってください。"+"\n―――"+"\nerrorcode: "+errorcode+"\nmessage: "+message;
		}
		MyDialog dialog = new MyDialog();
		dialog.setMode(MyDialog.MODE_NORMAL);
		dialog.setTitle("ERROR!!");
		dialog.setMessage(dialogMessage);
		dialog.setPositive_button("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				IDEditNfcReadActivity.this.setResult(AdminActivity.RESULT_ID_EDIT_NG);
				IDEditNfcReadActivity.this.finish();
			}
		});
		dialog.setCancelable(false);
		dialog.show(this.getFragmentManager(), DIALOG_NAME);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		
		// リセット
		Singleton.INSTANCE.init();
		
		// 開放
		if(this.soundPool != null) {
			this.soundPool.release();
			this.soundPool = null;
		}
	}

}
