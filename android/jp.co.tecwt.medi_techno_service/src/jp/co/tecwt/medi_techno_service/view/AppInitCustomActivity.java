package jp.co.tecwt.medi_techno_service.view;

import jp.co.tecwt.medi_techno_service.data.DialogHolder;
import jp.co.tecwt.medi_techno_service.data.MyPreference;
import jp.co.tecwt.medi_techno_service.dialog.MyDialog;
import jp.co.tecwt.medi_techno_service.dialog.SoshikiIDDialog;
import jp.co.tecwt.medi_techno_service.dialog.URLDialog;
import jp.co.tecwt.medi_techno_service.R;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * 詳細設定画面。
 */
public class AppInitCustomActivity extends BaseActivity {
	
	private TextView currentURL;
	private TextView currentSoshikiID;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// レイアウトファイル適用
		this.setContentView(R.layout.activity_appinitcustom);
		
		MyPreference pref = MyPreference.getInstance(this);
		
		// 現在登録中の各種値を表示
		this.currentURL = (TextView)this.findViewById(R.id.textview_current_url);
		this.currentURL.setText(pref.getURLAuto(this));
		this.currentSoshikiID = (TextView)this.findViewById(R.id.textview_current_soshikiid);
		this.currentSoshikiID.setText(pref.getSoshikiID());
		((TextView)this.findViewById(R.id.textview_current_device_name)).setText(pref.getDeviceName());
		((TextView)this.findViewById(R.id.textview_current_device_id)).setText(pref.getDeviceID());
		
		// リスナー登録
		((Button)this.findViewById(R.id.button_back)).setOnClickListener(this.backBtn);
		((Button)this.findViewById(R.id.button_top)).setOnClickListener(this.topBtn);
		((Button)this.findViewById(R.id.button_url_change)).setOnClickListener(this.urlBtn);
		((Button)this.findViewById(R.id.button_url_initialize)).setOnClickListener(this.urlInitBtn);
		((Button)this.findViewById(R.id.button_soshikiid_change)).setOnClickListener(this.soshikiidBtn);
	}
	
	
	/**
	 * URL変更
	 */
	private View.OnClickListener urlBtn = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			
			// 多重起動阻止
			final String DIALOG_NAME = AppInitCustomActivity.this.getClass().getName();
			if(AppInitCustomActivity.this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
				return;
			}
			
			// 初期化
			DialogHolder.INSTANCE.init();
			
			// ダイアログ表示
			URLDialog dialog = new URLDialog();
			dialog.setCancelable(false);
			dialog.setCallback(AppInitCustomActivity.this.urlCallback);
			dialog.show(AppInitCustomActivity.this.getFragmentManager(), DIALOG_NAME);
		}
	};
	
	/**
	 * URL初期化
	 */
	private View.OnClickListener urlInitBtn = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			
			// 多重起動阻止
			final String DIALOG_NAME = AppInitCustomActivity.this.getClass().getName();
			if(AppInitCustomActivity.this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
				return;
			}
			
			MyDialog dialog = new MyDialog();
			dialog.setMode(MyDialog.MODE_NORMAL);
			dialog.setTitle("確認");
			dialog.setMessage("URLを最初の状態に戻します。\n現在の情報が失われますが、よろしいですか？");
			dialog.setPositive_button("OK", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					String defaultURL = AppInitCustomActivity.this.getResources().getString(R.string.default_url);
					MyPreference pref = MyPreference.getInstance(AppInitCustomActivity.this);
					pref.writeURL(defaultURL);
					pref.writeDoNotUseDefaultURL(MyPreference.DEFAULT_URL_USE);
					AppInitCustomActivity.this.currentURL.setText(defaultURL);
				}
			});
			dialog.setNegative_button("キャンセル", null);
			dialog.show(AppInitCustomActivity.this.getFragmentManager(), DIALOG_NAME);
		}
	};
	
	/**
	 *  URL変更コールバック
	 */
	private URLDialog.Callback urlCallback = new URLDialog.Callback() {		
		@Override
		public void callback(String result) {
			AppInitCustomActivity.this.currentURL.setText(result);
		}
	};
	
	/**
	 * 組織ID変更
	 */
	private View.OnClickListener soshikiidBtn = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			final String DIALOG_NAME = AppInitCustomActivity.this.getClass().getName();
			if(AppInitCustomActivity.this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
				return;
			}
			
			// 初期化
			DialogHolder.INSTANCE.init();
			
			// ダイアログ表示
			SoshikiIDDialog dialog = new SoshikiIDDialog();
			dialog.setCancelable(false);
			dialog.setCallback(AppInitCustomActivity.this.soshikiIDCallback);
			dialog.show(AppInitCustomActivity.this.getFragmentManager(), DIALOG_NAME);
		}
	};
	
	/**
	 * 組織ID変更コールバック
	 */
	private SoshikiIDDialog.Callback soshikiIDCallback = new SoshikiIDDialog.Callback() {		
		@Override
		public void callback(String result) {
			AppInitCustomActivity.this.currentSoshikiID.setText(result);
		}
	};

}
