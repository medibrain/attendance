package jp.co.tecwt.medi_techno_service.view;

import java.util.ArrayList;

import jp.co.tecwt.medi_techno_service.R;
import jp.co.tecwt.medi_techno_service.data.DialogHolder;
import jp.co.tecwt.medi_techno_service.data.MyPreference;
import jp.co.tecwt.medi_techno_service.data.Singleton;
import jp.co.tecwt.medi_techno_service.dialog.GyoumuSetDialog;
import jp.co.tecwt.medi_techno_service.util.NextAct;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 業務内容画面。
 */
public class GyoumuActivity extends BaseActivity {

	private int type;
	private View viewNormal;
	private View viewSelect;
	private EditText edit;
	private SelectAdapter adapter;

	private Button next;
	private boolean checkON;
	private boolean isJump;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// 初回IME非表示
		this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

		// レイアウトファイル適用
		setContentView(R.layout.activity_gyoumu);

		// リスナー登録(汎用)
		((Button)this.findViewById(R.id.button_back)).setOnClickListener(this.backBtn);
		((Button)this.findViewById(R.id.button_top)).setOnClickListener(this.topBtn);
		this.next = (Button)this.findViewById(R.id.button_next);	//先に呼ばないとヌルポ
		this.next.setOnClickListener(this.nextBtn);

		// 必須判定
		this.checkON = this.getIntent().getBooleanExtra(NextAct.ISHISSU, true);

		// 入力方法の取得と反映
		MyPreference pref = MyPreference.getInstance(this);
		this.type = pref.getGyoumuType();
		switch (this.type) {
			case MyPreference.GYOUMU_TYPE_NORMAL:
				this.initTypeNormal();
				break;
			case MyPreference.GYOUMU_TYPE_SELECT:
				this.initTypeSelect();
				break;
		}
	}

/**########################################
 * Normal
 * #######################################*/

	private void initTypeNormal() {
		this.viewNormal = this.findViewById(R.id.include_normal);
		this.viewNormal.setVisibility(View.VISIBLE);
		this.edit = (EditText)this.viewNormal.findViewById(R.id.edittext_honbun);
		((Button)this.viewNormal.findViewById(R.id.button_textset_add)).setOnClickListener(this.textAddBtn);
		((Button)this.viewNormal.findViewById(R.id.button_textset1)).setOnClickListener(this.textSetBtn);
		((Button)this.viewNormal.findViewById(R.id.button_textset2)).setOnClickListener(this.textSetBtn);
		((Button)this.viewNormal.findViewById(R.id.button_textset3)).setOnClickListener(this.textSetBtn);
		((Button)this.viewNormal.findViewById(R.id.button_textset4)).setOnClickListener(this.textSetBtn);
		((Button)this.viewNormal.findViewById(R.id.button_textset5)).setOnClickListener(this.textSetBtn);
		((Button)this.viewNormal.findViewById(R.id.button_textset6)).setOnClickListener(this.textSetBtn);
		((Button)this.viewNormal.findViewById(R.id.button_textset7)).setOnClickListener(this.textSetBtn);
		((Button)this.viewNormal.findViewById(R.id.button_textset8)).setOnClickListener(this.textSetBtn);
		((Button)this.viewNormal.findViewById(R.id.button_textset9)).setOnClickListener(this.textSetBtn);
		this.edit.setOnKeyListener(this.gyoumuKeyListener);
		this.edit.addTextChangedListener(this.gyoumuWatcher);
		this.textsetUpdate();
	}

	//入力監視(文字)
	private TextWatcher gyoumuWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {

			// 保存する
			String text = s.toString();
			Singleton.INSTANCE.setGyoumu(text);
			GyoumuActivity.this.buttonUpdate(text);
		}
		@Override
		public void afterTextChanged(Editable s) {}
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
	};

	//入力監視(改行)
	private OnKeyListener gyoumuKeyListener = new OnKeyListener() {
		@Override
		public boolean onKey(View view, int keyCode, KeyEvent event) {
			if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
				// 改行不可
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				return true;
			}
			return false;
		}
	};

	//テンプレートの追加
	private OnClickListener textAddBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			// 多重起動阻止
			final String DIALOG_NAME = this.getClass().getName();
			if(GyoumuActivity.this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
				return;
			}
			// ダイアログ内の値をリフレッシュ
			DialogHolder.INSTANCE.init();
			// ダイアログ表示
			GyoumuSetDialog dialog = new GyoumuSetDialog(GyoumuActivity.this.type);
			if (GyoumuActivity.this.type == MyPreference.GYOUMU_TYPE_NORMAL) {
				dialog.setCallback(GyoumuActivity.this.callback);
			} else if (GyoumuActivity.this.type == MyPreference.GYOUMU_TYPE_SELECT) {
				dialog.setCallback(GyoumuActivity.this.callbackSelect);
			}
			dialog.show(GyoumuActivity.this.getFragmentManager(), DIALOG_NAME);
		}
	};

	//テンプレートボタンイベントハンドラ
	private OnClickListener textSetBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			String text = ((Button)view).getText().toString();
			if(!text.equals("")) {	// 文字列がある場合のみ反映
				String setText = GyoumuActivity.this.edit.getText().toString() + text;
				GyoumuActivity.this.edit.setText(setText);	// 後ろに追記
				GyoumuActivity.this.edit.setSelection(setText.length());	// カーソルを最後尾に
			}
		}
	};

	//テンプレート変更コールバック
	public GyoumuSetDialog.Callback callback = new GyoumuSetDialog.Callback() {
		@Override
		public void callback(String result) {
			GyoumuActivity.this.textsetUpdate();
		}
	};

	//テンプレート全更新
	private void textsetUpdate() {
		MyPreference pref = MyPreference.getInstance(this);
		((Button)this.viewNormal.findViewById(R.id.button_textset1)).setText(pref.getTextSet(1, this.type));
		((Button)this.viewNormal.findViewById(R.id.button_textset2)).setText(pref.getTextSet(2, this.type));
		((Button)this.viewNormal.findViewById(R.id.button_textset3)).setText(pref.getTextSet(3, this.type));
		((Button)this.viewNormal.findViewById(R.id.button_textset4)).setText(pref.getTextSet(4, this.type));
		((Button)this.viewNormal.findViewById(R.id.button_textset5)).setText(pref.getTextSet(5, this.type));
		((Button)this.viewNormal.findViewById(R.id.button_textset6)).setText(pref.getTextSet(6, this.type));
		((Button)this.viewNormal.findViewById(R.id.button_textset7)).setText(pref.getTextSet(7, this.type));
		((Button)this.viewNormal.findViewById(R.id.button_textset8)).setText(pref.getTextSet(8, this.type));
		((Button)this.viewNormal.findViewById(R.id.button_textset9)).setText(pref.getTextSet(9, this.type));
	}


/**########################################
 * Select
 * #######################################*/

	private void initTypeSelect() {
		this.viewSelect = this.findViewById(R.id.include_select);
		this.viewSelect.setVisibility(View.VISIBLE);
		this.adapter = new SelectAdapter(this);
		((ListView)this.viewSelect.findViewById(R.id.listview_select)).setAdapter(this.adapter);
		((Button)this.viewSelect.findViewById(R.id.button_textset_add)).setOnClickListener(this.textAddBtn);
	}

	public GyoumuSetDialog.Callback callbackSelect = new GyoumuSetDialog.Callback() {
		@Override
		public void callback(String result) {
			GyoumuActivity.this.textsetSelectUpdate();
		}
	};

	private void textsetSelectUpdate() {
		this.adapter.setList();
		this.adapter.notifyDataSetChanged();	//更新
	}

	private void noDataTextGone() {
		((TextView)this.viewSelect.findViewById(R.id.textview_nodata)).setVisibility(View.GONE);
	}

	public class SelectAdapter extends BaseAdapter {
		GyoumuActivity act;
		LayoutInflater inf;
		ArrayList<String> textset = new ArrayList<String>();
		ArrayList<Boolean> checked = new ArrayList<Boolean>();
		public SelectAdapter(GyoumuActivity act) {
			this.act = act;
			this.inf = (LayoutInflater)act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			setList();
		}
		public String getGyoumuString(String separator) {
			StringBuilder sb = new StringBuilder();
			boolean over = false;
			for (int i = 0; i < checked.size(); i++) {
				if (checked.get(i)) {
					if (sb.length() > 0) {
						over = true;
					}
					if (over) sb.append(separator);
					sb.append(textset.get(i));
				}
			}
			String result = sb.toString();
			return result;
		}
		public void setList() {
			textset.clear();
			checked.clear();
			String curGyoumu = Singleton.INSTANCE.getGyoumu();
			String gyoumu;
			for (int i = 0; i < MyPreference.KEY_TEXTSETS_SELECT.length; i++) {
				gyoumu = MyPreference.getInstance(act).getTextSet(i + 1, GyoumuActivity.this.type);
				if (gyoumu == null || gyoumu.equals("")) continue;
				textset.add(gyoumu);
				if (curGyoumu == null) checked.add(false);
				else checked.add(curGyoumu.contains(gyoumu));	//復帰
			}
			if (textset.size() > 0) act.noDataTextGone();
			buttonUpdate();
		}
		private void buttonUpdate() {
			for (int i = 0; i < checked.size(); i++) {
				if (checked.get(i)) {
					act.buttonUpdate("1");	//ボタン有効化のための適当な文字列
					return;
				}
			}
			act.buttonUpdate("");
		}
		@Override
		public View getView(final int position, View view, ViewGroup parent) {
			if (view == null) {
				view = inf.inflate(R.layout.item_gyoumu_select_cell, parent, false);
			}
			final CheckBox cb = (CheckBox)view.findViewById(R.id.checkbox_gyoumu_check);
			cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
					checked.set(position, arg1);	//位置と状態を記録
					buttonUpdate();
				}
			});
			cb.setChecked(checked.get(position));	//復帰
			TextView tv = (TextView)view.findViewById(R.id.textview_gyoumu);
			tv.setText(textset.get(position));
			tv.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View paramView) {
					cb.setChecked(!checked.get(position));
				}
			});
			return view;
		}
		@Override
		public int getCount() {
			return textset.size();
		}
		@Override
		public Object getItem(int arg0) {
			return textset.get(arg0);
		}
		@Override
		public long getItemId(int arg0) {
			return 0;
		}
	}




	/**
	 * 次へ
	 */
	private OnClickListener nextBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {

			if (GyoumuActivity.this.type == MyPreference.GYOUMU_TYPE_NORMAL) {
				// 改行を削除
				String text = Singleton.INSTANCE.getGyoumu();
				String BR = System.getProperty("line.separator");
				if(text != null && text.contains(BR)) {
					Singleton.INSTANCE.setGyoumu(text.replaceAll(BR, ""));
				}
			}
			else if (GyoumuActivity.this.type == MyPreference.GYOUMU_TYPE_SELECT) {
				String text = GyoumuActivity.this.adapter.getGyoumuString("、");
				if (text.length() > 50) {
					text = text.substring(0, 50);	//50字にカット
					Toast.makeText(GyoumuActivity.this, "50字を超えた分はカットされました。", Toast.LENGTH_LONG).show();
				}
				Singleton.INSTANCE.setGyoumu(text);
			}

			// 確認画面からのジャンプならアクティビティを閉じる
			if(GyoumuActivity.this.isJump) {
				GyoumuActivity.this.finish();
				return;
			}

			Intent nextIntent = NextAct.getNextIntent(GyoumuActivity.this, Singleton.INSTANCE.getSyuttaiStatus());
			GyoumuActivity.this.startActivityForResult(nextIntent, 0);
		}
	};

	/**
	 * ボタンを更新する。
	 * @param text
	 */
	private void buttonUpdate(String text) {
		if(this.checkON) {
			if(text == null || text.equals("")) {
				this.next.setEnabled(false);
			}else {
				this.next.setEnabled(true);
			}
		}else {
			this.next.setEnabled(true);
		}
	}


	@Override
	public void onResume() {
		super.onResume();

		// 復帰
		String gyoumu = Singleton.INSTANCE.getGyoumu();
		if (this.type == MyPreference.GYOUMU_TYPE_NORMAL) {
			if(gyoumu != null) {
				this.edit.setText(gyoumu);
			}
			this.buttonUpdate(this.edit.getText().toString());
		}

		// 確認画面からのジャンプかを判定
		this.isJump = this.getIntent().getBooleanExtra("isJump", false);

		// ジャンプしてきた場合は[次へ]の文字を「確定」に変える
		if(this.isJump) {
			this.next.setText("確定");
		}
	}

	@Override
	public void onActivityResult(int req, int res, Intent d) {
		// ページ数をカウントダウン
		NextAct.countDown(this, Singleton.INSTANCE.getSyuttaiStatus());
	}

}
