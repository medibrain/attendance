package jp.co.tecwt.medi_techno_service.view;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import jp.co.tecwt.medi_techno_service.R;
import jp.co.tecwt.medi_techno_service.data.LogDB;
import jp.co.tecwt.medi_techno_service.data.MyPreference;
import jp.co.tecwt.medi_techno_service.data.Singleton;
import jp.co.tecwt.medi_techno_service.dialog.MyDialog;
import jp.co.tecwt.medi_techno_service.util.MyHttpClient2;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.media.SoundPool;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class NfcReadActivity extends BaseActivity {

	private SoundPool soundPool;
	private int soundId;
	private boolean isNfcLoaded;
	private MyDialog chikokuDialog;
	private TimeZone timeZone;
	private Locale locale;


	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// レイアウトファイル適用
		setContentView(R.layout.activity_nfc);

		// タイムゾーン・ロケール設定
		this.timeZone = TimeZone.getTimeZone("Asia/Tokyo");
		this.locale = Locale.JAPAN;

		// リスナー登録
		((Button)this.findViewById(R.id.button_back)).setOnClickListener(this.backBtn);
		((Button)this.findViewById(R.id.button_top)).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// 確認画面だけは削除確認する
				MyDialog dialog = new MyDialog();
				dialog.setTitle("CAUTION!");
				dialog.setMessage("入力した情報が失われますが本当によろしいですか？");
				dialog.setPositive_button("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent topIntent = new Intent(NfcReadActivity.this, Main.class);
						topIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
						NfcReadActivity.this.startActivity(topIntent);
					}
				});
				dialog.setNegative_button("キャンセル", null);
				dialog.show(NfcReadActivity.this.getFragmentManager(), null);
			}
		});
		((LinearLayout)this.findViewById(R.id.linearlayout_syukkin_time)).setTag(TimeActivity.class);
		((LinearLayout)this.findViewById(R.id.linearlayout_syukkin_time)).setOnClickListener(this.jump);
		((LinearLayout)this.findViewById(R.id.linearlayout_taikin_syubetsu)).setTag(SyubetsuActivity.class);
		((LinearLayout)this.findViewById(R.id.linearlayout_taikin_syubetsu)).setOnClickListener(this.jump);
		((LinearLayout)this.findViewById(R.id.linearlayout_taikin_syorimaisuu)).setTag(SyoriMaisuuActivity.class);
		((LinearLayout)this.findViewById(R.id.linearlayout_taikin_syorimaisuu)).setOnClickListener(this.jump);
		((LinearLayout)this.findViewById(R.id.linearlayout_taikin_moushidemaisuu)).setTag(MoushideMaisuuActivity.class);
		((LinearLayout)this.findViewById(R.id.linearlayout_taikin_moushidemaisuu)).setOnClickListener(this.jump);
		((LinearLayout)this.findViewById(R.id.linearlayout_taikin_kyukei)).setTag(KyukeiActivity.class);
		((LinearLayout)this.findViewById(R.id.linearlayout_taikin_kyukei)).setOnClickListener(this.jump);
		((LinearLayout)this.findViewById(R.id.linearlayout_taikin_koutsuuhi)).setTag(KoutsuuhiActivity.class);
		((LinearLayout)this.findViewById(R.id.linearlayout_taikin_koutsuuhi)).setOnClickListener(this.jump);
		((LinearLayout)this.findViewById(R.id.linearlayout_taikin_gyoumu)).setTag(GyoumuActivity.class);
		((LinearLayout)this.findViewById(R.id.linearlayout_taikin_gyoumu)).setOnClickListener(this.jump);
		((LinearLayout)this.findViewById(R.id.linearlayout_taikin_time)).setTag(TimeActivity.class);
		((LinearLayout)this.findViewById(R.id.linearlayout_taikin_time)).setOnClickListener(this.jump);
		((LinearLayout)this.findViewById(R.id.linearlayout_idou_syubetsu)).setTag(SyubetsuActivity.class);
		((LinearLayout)this.findViewById(R.id.linearlayout_idou_syubetsu)).setOnClickListener(this.jump);
		((LinearLayout)this.findViewById(R.id.linearlayout_idou_syorimaisuu)).setTag(SyoriMaisuuActivity.class);
		((LinearLayout)this.findViewById(R.id.linearlayout_idou_syorimaisuu)).setOnClickListener(this.jump);
		((LinearLayout)this.findViewById(R.id.linearlayout_idou_moushidemaisuu)).setTag(MoushideMaisuuActivity.class);
		((LinearLayout)this.findViewById(R.id.linearlayout_idou_moushidemaisuu)).setOnClickListener(this.jump);
		((LinearLayout)this.findViewById(R.id.linearlayout_idou_gyoumu)).setTag(GyoumuActivity.class);
		((LinearLayout)this.findViewById(R.id.linearlayout_idou_gyoumu)).setOnClickListener(this.jump);
		((LinearLayout)this.findViewById(R.id.linearlayout_idou_time)).setTag(TimeActivity.class);
		((LinearLayout)this.findViewById(R.id.linearlayout_idou_time)).setOnClickListener(this.jump);

		// 文字色を赤にする
		((TextView)this.findViewById(R.id.textview_hosoku1)).setText(Html.fromHtml("※認識位置は端末の<font color=#ff0000>裏側</font>です。※"));

		// 効果音再生インスタンス生成
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
			// API 21 未満
			this.soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
		} else {
			// API 21以上
			SoundPool.Builder builder = new SoundPool.Builder();
			this.soundPool = builder.build();
		}
		this.soundId = this.soundPool.load(this, R.raw.loaded, 1);	// 優先度1を指定する

		// 出勤時に現在時刻が09:05を超えていればメッセージ
		if(Singleton.INSTANCE.getSyuttaiStatus() == Main.SYUKKIN) {
			Calendar calendar = Calendar.getInstance(this.timeZone, this.locale);
			int realTime = calendar.get(Calendar.HOUR_OF_DAY) * 100 + calendar.get(Calendar.MINUTE);
			if(905 < realTime) {
				this.chikokuDialog = new MyDialog();
				this.chikokuDialog.setTitle("CAUTION!!");
				this.chikokuDialog.setMessage(
						"遅刻です。\n"
						+ "打刻は出勤と同時に行いましょう。\n\n"
						+ "※時刻を変更する場合は時間をタップして下さい");
				this.chikokuDialog.setPositive_button("OK", null);
				this.chikokuDialog.show(this.getFragmentManager(), null);
			}
		}
	}

	/**
	 * ジャンプリスナー<br>
	 * setTag()で保存したclassインスタンスを利用してアクティビティを開きます。<br>
	 * 開いたアクティビティが閉じる場合は本アクティビティに戻ります。
	 */
	private OnClickListener jump = new OnClickListener() {
		public void onClick(View v) {
			Intent intent = new Intent(NfcReadActivity.this, (Class<?>)v.getTag());
			intent.putExtra("isJump", true);
			NfcReadActivity.this.startActivity(intent);
		};
	};


	@Override
	public void onDestroy() {
		super.onDestroy();

		// 開放
		if(this.soundPool != null) {
			this.soundPool.release();
			this.soundPool = null;
		}
	}

	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);

		// 連続読み取り不可
		if(this.isNfcLoaded == true) {
			return;
		}

		// ID読み取り
		byte[] idBytes = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
		StringBuilder builder = new StringBuilder();
        for (byte b : idBytes) {
            String hex = String.format("%02X", b);	// %02X = 2桁にする
            builder.append(hex);
        }
        String id = builder.toString().trim();

        Singleton.INSTANCE.setID(id);
        Singleton.INSTANCE.setDatetimeAuto(System.currentTimeMillis());

        Resources resources = this.getResources();
        String url = resources.getString(R.string.nfc_url);
        int timeout_connect = resources.getInteger(R.integer.timeout_connect);
		int timeout_read = resources.getInteger(R.integer.timeout_read);
		MyHttpClient2 client = new MyHttpClient2(this, url, this.responser);
		client.enabledProgressDialog(this, "通信中", "しばらくお待ちください", null);
		client.setTimeOut(timeout_connect, timeout_read);

		// ルートに応じて送信内容変更
		switch(Singleton.INSTANCE.getSyuttaiStatus()) {
		case Main.SYUKKIN:
			this.addSyukkinParams(client);
			break;
		case Main.TAIKIN:
			this.addTaikinParams(client);
			break;
		case Main.IDOU:
			this.addIdouParams(client);
			break;
		}
		client.execute();

		// ダイアログ閉じる
		if(this.chikokuDialog != null) {
			this.chikokuDialog.dismiss();
		}

		this.isNfcLoaded = true;
	}

	private void addSyukkinParams(MyHttpClient2 client) {
		Resources resources = this.getResources();
		client.addPostParam(resources.getString(R.string.nfc_syukkin_param_name1), "in");
		client.addPostParam(
				resources.getString(R.string.nfc_syukkin_param_name2),
				MyPreference.getInstance(this).getSoshikiID());
		client.addPostParam(resources.getString(R.string.nfc_syukkin_param_name3), this.nullCheckOrBlankReturn(Singleton.INSTANCE.getID()));
//		client.addPostParam(resources.getString(R.string.nfc_syukkin_param_name4), this.nullCheckOrBlankReturn(String.valueOf(Singleton.INSTANCE.getSyubetsuId())));
		client.addPostParam(resources.getString(R.string.nfc_syukkin_param_name4), this.nullCheckOrBlankReturn(String.valueOf(Singleton.INSTANCE.getLat())));
		client.addPostParam(resources.getString(R.string.nfc_syukkin_param_name5), this.nullCheckOrBlankReturn(String.valueOf(Singleton.INSTANCE.getLng())));
		String datetime_edit = DateFormat.format("yyyyMMddkkmm", Singleton.INSTANCE.getDatetimeEdit()).toString();
		client.addPostParam(resources.getString(R.string.nfc_syukkin_param_name6), datetime_edit);
		String datetime_auto = DateFormat.format("yyyyMMddkkmm", Singleton.INSTANCE.getDatetimeAuto()).toString();
		client.addPostParam(resources.getString(R.string.nfc_syukkin_param_name7), datetime_auto);
		client.addPostParam(resources.getString(R.string.nfc_syukkin_param_name8), this.nullCheckOrBlankReturn(String.valueOf(Singleton.INSTANCE.getAppInit_DeviceID())));
	}

	private void addTaikinParams(MyHttpClient2 client) {
		Resources resources = this.getResources();
		client.addPostParam(resources.getString(R.string.nfc_taikin_param_name1), "out");
		client.addPostParam(
				resources.getString(R.string.nfc_taikin_param_name2),
				MyPreference.getInstance(this).getSoshikiID());
		client.addPostParam(resources.getString(R.string.nfc_taikin_param_name3), this.nullCheckOrBlankReturn(Singleton.INSTANCE.getID()));
		client.addPostParam(resources.getString(R.string.nfc_taikin_param_name4), this.nullCheckOrBlankReturn(String.valueOf(Singleton.INSTANCE.getLat())));
		client.addPostParam(resources.getString(R.string.nfc_taikin_param_name5), this.nullCheckOrBlankReturn(String.valueOf(Singleton.INSTANCE.getLng())));
		String datetime_edit = DateFormat.format("yyyyMMddkkmm", Singleton.INSTANCE.getDatetimeEdit()).toString();
		client.addPostParam(resources.getString(R.string.nfc_taikin_param_name6), datetime_edit);
		String datetime_auto = DateFormat.format("yyyyMMddkkmm", Singleton.INSTANCE.getDatetimeAuto()).toString();
		client.addPostParam(resources.getString(R.string.nfc_taikin_param_name7), datetime_auto);
		client.addPostParam(resources.getString(R.string.nfc_taikin_param_name8), this.nullCheckOrBlankReturn(MyHttpClient2.urlEncode(Singleton.INSTANCE.getGyoumu())));
		client.addPostParam(resources.getString(R.string.nfc_taikin_param_name9), this.nullCheckOrBlankReturn(Singleton.INSTANCE.getSyoriMaisuu()));
		client.addPostParam(resources.getString(R.string.nfc_taikin_param_name10), this.nullCheckOrBlankReturn(Singleton.INSTANCE.getMoushideMaisuu()));
		client.addPostParam(resources.getString(R.string.nfc_taikin_param_name11), this.nullCheckOrBlankReturn(Singleton.INSTANCE.getKoutsuuhi()));
		client.addPostParam(resources.getString(R.string.nfc_taikin_param_name12), this.nullCheckOrBlankReturn(Singleton.INSTANCE.getKyukei()));
		client.addPostParam(resources.getString(R.string.nfc_syukkin_param_name13), this.nullCheckOrBlankReturn(String.valueOf(Singleton.INSTANCE.getAppInit_DeviceID())));
		client.addPostParam(resources.getString(R.string.nfc_syukkin_param_name14), this.nullCheckOrBlankReturn(String.valueOf(Singleton.INSTANCE.getSyubetsuId())));
	}

	private void addIdouParams(MyHttpClient2 client) {
		Resources resources = this.getResources();
		client.addPostParam(resources.getString(R.string.nfc_idou_param_name1), "move");
		client.addPostParam(
				resources.getString(R.string.nfc_idou_param_name2),
				MyPreference.getInstance(this).getSoshikiID());
		client.addPostParam(resources.getString(R.string.nfc_idou_param_name3), this.nullCheckOrBlankReturn(Singleton.INSTANCE.getID()));
		client.addPostParam(resources.getString(R.string.nfc_idou_param_name4), this.nullCheckOrBlankReturn(MyHttpClient2.urlEncode(Singleton.INSTANCE.getGyoumu())));
		client.addPostParam(resources.getString(R.string.nfc_idou_param_name5), this.nullCheckOrBlankReturn(Singleton.INSTANCE.getSyoriMaisuu()));
		client.addPostParam(resources.getString(R.string.nfc_idou_param_name6), this.nullCheckOrBlankReturn(Singleton.INSTANCE.getMoushideMaisuu()));
		client.addPostParam(resources.getString(R.string.nfc_idou_param_name7), this.nullCheckOrBlankReturn(String.valueOf(Singleton.INSTANCE.getSyubetsuId())));
		client.addPostParam(resources.getString(R.string.nfc_idou_param_name8), this.nullCheckOrBlankReturn(String.valueOf(Singleton.INSTANCE.getLat())));
		client.addPostParam(resources.getString(R.string.nfc_idou_param_name9), this.nullCheckOrBlankReturn(String.valueOf(Singleton.INSTANCE.getLng())));
		String datetime_edit = DateFormat.format("yyyyMMddkkmm", Singleton.INSTANCE.getDatetimeEdit()).toString();
		client.addPostParam(resources.getString(R.string.nfc_idou_param_name10), datetime_edit);
		String datetime_auto = DateFormat.format("yyyyMMddkkmm", Singleton.INSTANCE.getDatetimeAuto()).toString();
		client.addPostParam(resources.getString(R.string.nfc_idou_param_name11), datetime_auto);
		client.addPostParam(resources.getString(R.string.nfc_syukkin_param_name12), this.nullCheckOrBlankReturn(String.valueOf(Singleton.INSTANCE.getAppInit_DeviceID())));
	}

	/**
	 * nullであれば空文字を返す。
	 * @param str
	 * @return
	 */
	private String nullCheckOrBlankReturn(String str) {
		if(str == null) {
			return "";
		}
		return str;
	}


	/**
	 * レスポンス受け取り
	 */
	private MyHttpClient2.Responser responser = new MyHttpClient2.Responser() {

		@Override
		public void onFailed() {
			// 失敗
			NfcReadActivity.this.showErrorDialog();
		}

		@Override
		public void onError(String errorcode, String message) {
			// 人為的失敗
			NfcReadActivity.this.showErrorDialog(errorcode, message);
		}

		@Override
		public void onCompleted(String json) {
			// 成功
			try {
				// root
				JSONObject rootObj = new JSONObject(json);
				Log.d("Syuttai Nfc", rootObj.toString(4));

				// timecard
				JSONArray timecardArray = rootObj.getJSONArray("timecard");

				// name
				String name = timecardArray.getJSONObject(timecardArray.length()-1).getString("name");

				// DBに保存
				SQLiteDatabase db = new LogDB(NfcReadActivity.this).getWritableDatabase();
				ContentValues val = new ContentValues();
				val.put(LogDB.LOG_ID, Singleton.INSTANCE.getID());
				val.put(LogDB.LOG_NAME, name);
				long datetime_edit = Singleton.INSTANCE.getDatetimeEdit();
				val.put(LogDB.LOG_DATETIME_EDIT, DateFormat.format("yyyy-MM-dd kk:mm:00", datetime_edit).toString());
				long datetime_auto = Singleton.INSTANCE.getDatetimeAuto();
				val.put(LogDB.LOG_DATETIME_AUTO, DateFormat.format("yyyy-MM-dd kk:mm:ss", datetime_auto).toString());
				val.put(LogDB.LOG_SYUTTAISTATUS, Singleton.INSTANCE.getSyuttaiStatus());
				val.put(LogDB.LOG_SYUBETSU, Singleton.INSTANCE.getSyubetsu());
				val.put(LogDB.LOG_GPS_LAT, Singleton.INSTANCE.getLat());
				val.put(LogDB.LOG_GPS_LNG, Singleton.INSTANCE.getLng());
				val.put(LogDB.LOG_GYOUMU, Singleton.INSTANCE.getGyoumu());
				val.put(LogDB.LOG_KYUKEI, Singleton.INSTANCE.getKyukei());
				val.put(LogDB.LOG_KOUTSUUHI, Singleton.INSTANCE.getKoutsuuhi());
				val.put(LogDB.LOG_SYORIMAISUU, Singleton.INSTANCE.getSyoriMaisuu());
				val.put(LogDB.LOG_MOUSHIDEMAISUU, Singleton.INSTANCE.getMoushideMaisuu());
				val.put(LogDB.LOG_SYUBETSU_ID, Singleton.INSTANCE.getSyubetsuId());
				db.insert(LogDB.TABLE_LOG, null, val);

				// 選択履歴を保存
				MyPreference pref = new MyPreference(NfcReadActivity.this);
				pref.writeSyubetsuLog(Singleton.INSTANCE.getSyubetsuId());

				// 送信完了画面に送信時のパラメータを送る
				Intent nextIntent = new Intent(NfcReadActivity.this, CompletedActivity.class);
				switch(Singleton.INSTANCE.getSyuttaiStatus()) {
				case Main.SYUKKIN:
					NfcReadActivity.this.syukkinPutExtra(rootObj, nextIntent);
					break;
				case Main.TAIKIN:
					NfcReadActivity.this.taikinPutExtra(rootObj, nextIntent);
					break;
				case Main.IDOU:
					NfcReadActivity.this.idouPutExtra(rootObj, nextIntent);
					break;
				}

				// 打刻完了の効果音
				NfcReadActivity.this.soundPool.play(NfcReadActivity.this.soundId, 0.5f, 0.5f, 0, 0, 1);

		        NfcReadActivity.this.startActivity(nextIntent);
				return;

			} catch(JSONException ex) {
				ex.printStackTrace();
			}

			// JSONに問題あり
			NfcReadActivity.this.showErrorDialog();
			return;
		}
	};

	private void showErrorDialog() {
		this.showErrorDialog(null, null);
	}

	private void showErrorDialog(final String errorcode, String message) {

		// 多重起動阻止
		final String DIALOG_NAME = this.getClass().getName();
		if(this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
			return;
		}

		String dialogMessage = "エラーが発生しました。お手数ですが再度操作を行ってください。\n";
		if(errorcode == null && message == null) {
			dialogMessage = "状態が回復しない場合はアプリまたは端末本体の再起動を試して下さい。";
		}else {
			if(errorcode.equals("timecard-in11") ||
					errorcode.equals("timecard-move11") ||
					errorcode.equals("timecard-out11")) {
				dialogMessage =
						"端末で設定されている日時が実際の日時と相違している可能性があります。"
						+ "再度打刻を行い、なおこのメッセージが表示される場合は「設定」アプリにて日時が"
						+ "自動設定となっていることを確認して下さい。解決しない場合は担当者まで申し出て下さい。\n";
			}
			dialogMessage += "―――"+"\nerrorcode: "+errorcode+"\nmessage: "+message;
		}
		MyDialog dialog = new MyDialog();
		dialog.setMode(MyDialog.MODE_NORMAL);
		dialog.setTitle("ERROR!!");
		dialog.setMessage(dialogMessage);
		dialog.setPositive_button("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				if(errorcode != null) {
					if(errorcode.equals("timecard-in02") ||
						errorcode.equals("timecard-move02") ||
						errorcode.equals("timecard-out02")) {
						return;//IDMエラーの場合は再度かざせるようにする
					}
				}
				Intent topIntent = new Intent(NfcReadActivity.this, Main.class);
				topIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
				NfcReadActivity.this.startActivity(topIntent);
			}
		});
		dialog.setCancelable(false);
		dialog.show(this.getFragmentManager(), DIALOG_NAME);
	}

	private void syukkinInit(View syukkin) {
		// 値入力
		((TextView)syukkin.findViewById(R.id.textview_syuttai)).setText(Singleton.INSTANCE.getSyuttai(), TextView.BufferType.SPANNABLE);
		Calendar calendar = Calendar.getInstance(this.timeZone, this.locale);
		calendar.setTimeInMillis(Singleton.INSTANCE.getDatetimeEdit());
		String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
		if(hour.length() == 1) hour = "0" + hour;
		String minute = String.valueOf(calendar.get(Calendar.MINUTE));
		if(minute.length() == 1) minute = "0" + minute;
		String hourMinute = hour + ":" + minute;
		((TextView)syukkin.findViewById(R.id.textview_syukkin_time)).setText(hourMinute, TextView.BufferType.SPANNABLE);

		// 非表示設定
		MyPreference pref = MyPreference.getInstance(this);
		if(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_SYUKKIN_EDITTIME) == 2) {
			syukkin.findViewById(R.id.nfc_linearlayout_base_syukkin_time).setVisibility(View.GONE);
		}
//		if(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_SYUKKIN_SYUBETSU) == 2) {
//			syukkin.findViewById(R.id.nfc_linearlayout_base_syukkin_syubetsu).setVisibility(View.GONE);
//		}
	}

	private void taikinInit(View taikin) {
		((TextView)taikin.findViewById(R.id.textview_syuttai)).setText(Singleton.INSTANCE.getSyuttai(), TextView.BufferType.SPANNABLE);
		((TextView)taikin.findViewById(R.id.textview_syubetsu)).setText(Singleton.INSTANCE.getSyubetsu(), TextView.BufferType.SPANNABLE);
		((TextView)taikin.findViewById(R.id.textview_kyukei)).setText(Singleton.INSTANCE.getKyukei(), TextView.BufferType.SPANNABLE);
		((TextView)taikin.findViewById(R.id.textview_koutsuuhi)).setText(Singleton.INSTANCE.getKoutsuuhi(), TextView.BufferType.SPANNABLE);
		((TextView)taikin.findViewById(R.id.textview_syorimaisuu)).setText(Singleton.INSTANCE.getSyoriMaisuu(), TextView.BufferType.SPANNABLE);
		((TextView)taikin.findViewById(R.id.textview_moushidemaisuu)).setText(Singleton.INSTANCE.getMoushideMaisuu(), TextView.BufferType.SPANNABLE);
		((TextView)taikin.findViewById(R.id.textview_gyoumu)).setText(Singleton.INSTANCE.getGyoumu(), TextView.BufferType.SPANNABLE);
		Calendar calendar = Calendar.getInstance(this.timeZone, this.locale);
		calendar.setTimeInMillis(Singleton.INSTANCE.getDatetimeEdit());
		String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
		if(hour.length() == 1) hour = "0" + hour;
		String minute = String.valueOf(calendar.get(Calendar.MINUTE));
		if(minute.length() == 1) minute = "0" + minute;
		String hourMinute = hour + ":" + minute;
		((TextView)taikin.findViewById(R.id.textview_taikin_time)).setText(hourMinute, TextView.BufferType.SPANNABLE);

		MyPreference pref = MyPreference.getInstance(this);
		if(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_TAIKIN_SYUBETSU) == 2) {
			taikin.findViewById(R.id.nfc_linearlayout_base_syukkin_syubetsu).setVisibility(View.GONE);
		}
		if(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_TAIKIN_EDITTIME) == 2) {
			taikin.findViewById(R.id.nfc_linearlayout_base_taikin_time).setVisibility(View.GONE);
		}
		if(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_TAIKIN_GYOUMU) == 2) {
			taikin.findViewById(R.id.nfc_linearlayout_base_taikin_gyoumu).setVisibility(View.GONE);
		}
		if(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_TAIKIN_KOUTSUUHI) == 2) {
			taikin.findViewById(R.id.nfc_linearlayout_base_taikin_koutsuuhi).setVisibility(View.GONE);
		}
		if(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_TAIKIN_KYUKEI) == 2) {
			taikin.findViewById(R.id.nfc_linearlayout_base_taikin_kyukei).setVisibility(View.GONE);
		}
		if(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_TAIKIN_MOUSHIDEMAISUU) == 2) {
			taikin.findViewById(R.id.nfc_linearlayout_base_taikin_moushidemaisuu).setVisibility(View.GONE);
		}
		if(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_TAIKIN_SYORIMAISUU) == 2) {
			taikin.findViewById(R.id.nfc_linearlayout_base_taikin_syorimaisuu).setVisibility(View.GONE);
		}
	}

	private void idouInit(View idou) {
		((TextView)idou.findViewById(R.id.textview_syuttai)).setText(Singleton.INSTANCE.getSyuttai(), TextView.BufferType.SPANNABLE);
		((TextView)idou.findViewById(R.id.textview_syubetsu)).setText(Singleton.INSTANCE.getSyubetsu(), TextView.BufferType.SPANNABLE);
		((TextView)idou.findViewById(R.id.textview_syorimaisuu)).setText(Singleton.INSTANCE.getSyoriMaisuu(), TextView.BufferType.SPANNABLE);
		((TextView)idou.findViewById(R.id.textview_moushidemaisuu)).setText(Singleton.INSTANCE.getMoushideMaisuu(), TextView.BufferType.SPANNABLE);
		((TextView)idou.findViewById(R.id.textview_gyoumu)).setText(Singleton.INSTANCE.getGyoumu(), TextView.BufferType.SPANNABLE);
		Calendar calendar = Calendar.getInstance(this.timeZone, this.locale);
		calendar.setTimeInMillis(Singleton.INSTANCE.getDatetimeEdit());
		String hour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
		if(hour.length() == 1) hour = "0" + hour;
		String minute = String.valueOf(calendar.get(Calendar.MINUTE));
		if(minute.length() == 1) minute = "0" + minute;
		String hourMinute = hour + ":" + minute;
		((TextView)idou.findViewById(R.id.textview_idou_time)).setText(hourMinute, TextView.BufferType.SPANNABLE);

		MyPreference pref = MyPreference.getInstance(this);
		if(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_IDOU_EDITTIME) == 2) {
			idou.findViewById(R.id.nfc_linearlayout_base_idou_time).setVisibility(View.GONE);
		}
		if(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_IDOU_GYOUMU) == 2) {
			idou.findViewById(R.id.nfc_linearlayout_base_idou_gyoumu).setVisibility(View.GONE);
		}
		if(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_IDOU_MOUSHIDEMAISUU) == 2) {
			idou.findViewById(R.id.nfc_linearlayout_base_idou_moushidemaisuu).setVisibility(View.GONE);
		}
		if(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_IDOU_SYORIMAISUU) == 2) {
			idou.findViewById(R.id.nfc_linearlayout_base_idou_syorimaisuu).setVisibility(View.GONE);
		}
		if(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_IDOU_SYUBETSU) == 2) {
			idou.findViewById(R.id.nfc_linearlayout_base_idou_syubetsu).setVisibility(View.GONE);
		}
	}

/*	private Spannable getSpan(String text) {
		if(text == null) return null;
		Spannable span = Spannable.Factory.getInstance().newSpannable(text);
		UnderlineSpan us = new UnderlineSpan();
		span.setSpan(us, 0, text.length(), span.getSpanFlags(us));
		return span;
	}*/

	private void syukkinPutExtra(JSONObject rootObj, Intent nextIntent) throws JSONException {
		checkIntime(rootObj, nextIntent);
		JSONArray timecardArray = rootObj.getJSONArray("timecard");
		JSONObject data = timecardArray.getJSONObject(timecardArray.length()-1);
		nextIntent.putExtra(CompletedActivity.UID, data.getString("uid"));
		nextIntent.putExtra(CompletedActivity.NAME, data.getString("name"));
		nextIntent.putExtra(CompletedActivity.DATE, data.getString("tdate"));
		nextIntent.putExtra(CompletedActivity.TIME, data.getString("intime"));
	}

	private void taikinPutExtra(JSONObject rootObj, Intent nextIntent) throws JSONException {
		checkIntime(rootObj, nextIntent);
		JSONArray timecardArray = rootObj.getJSONArray("timecard");
		int dataSize = timecardArray.length();
		if(dataSize == 1) {
			// 移動なし
			JSONObject data = timecardArray.getJSONObject(dataSize-1);
			nextIntent.putExtra(CompletedActivity.UID, data.getString("uid"));
			nextIntent.putExtra(CompletedActivity.NAME, data.getString("name"));
			nextIntent.putExtra(CompletedActivity.DATE, data.getString("tdate"));
			nextIntent.putExtra(CompletedActivity.TIME, data.getString("outtime"));
			nextIntent.putExtra(CompletedActivity.KYUKEI, data.getString("breaktime"));
			nextIntent.putExtra(CompletedActivity.GYOUMU, data.getString("content"));
			nextIntent.putExtra(CompletedActivity.KOUTSUUHI, data.getString("fare"));
			nextIntent.putExtra(CompletedActivity.SYORIMAISUU, data.getString("wcount"));
			nextIntent.putExtra(CompletedActivity.MOUSHIDEMAISUU, data.getString("rcount"));
			nextIntent.putExtra(CompletedActivity.SYUBETSU, data.getString("worktype"));
		}else if(dataSize > 1) {
			// 移動あり
			for(int i=0; i<timecardArray.length(); i++) {
				// data
				JSONObject data = timecardArray.getJSONObject(i);
				String times = data.getString("times");

				if(times.equals("1")) {
					nextIntent.putExtra(CompletedActivity.KYUKEI, data.getString("breaktime"));
					nextIntent.putExtra(CompletedActivity.KOUTSUUHI, data.getString("fare"));
				}
				else if(times.equals("2")) {
					nextIntent.putExtra(CompletedActivity.UID, data.getString("uid"));
					nextIntent.putExtra(CompletedActivity.NAME, data.getString("name"));
					nextIntent.putExtra(CompletedActivity.DATE, data.getString("tdate"));
					nextIntent.putExtra(CompletedActivity.TIME, data.getString("outtime"));
					nextIntent.putExtra(CompletedActivity.SYUBETSU, data.getString("worktype"));
					nextIntent.putExtra(CompletedActivity.GYOUMU, data.getString("content"));
					nextIntent.putExtra(CompletedActivity.SYORIMAISUU, data.getString("wcount"));
					nextIntent.putExtra(CompletedActivity.MOUSHIDEMAISUU, data.getString("rcount"));
				}
			}
		}
	}

	private void idouPutExtra(JSONObject rootObj, Intent nextIntent) throws JSONException {
		checkIntime(rootObj, nextIntent);
		JSONArray timecardArray = rootObj.getJSONArray("timecard");
		for(int i=0; i<timecardArray.length(); i++) {
			// data
			JSONObject data = timecardArray.getJSONObject(i);
			String times = data.getString("times");

			if(times.equals("1")) {
				nextIntent.putExtra(CompletedActivity.GYOUMU, data.getString("content"));
				nextIntent.putExtra(CompletedActivity.SYORIMAISUU, data.getString("wcount"));
				nextIntent.putExtra(CompletedActivity.MOUSHIDEMAISUU, data.getString("rcount"));
				nextIntent.putExtra(CompletedActivity.SYUBETSU, data.getString("worktype"));
			}
			else if(times.equals("2")) {
				nextIntent.putExtra(CompletedActivity.UID, data.getString("uid"));
				nextIntent.putExtra(CompletedActivity.NAME, data.getString("name"));
				nextIntent.putExtra(CompletedActivity.DATE, data.getString("tdate"));
				nextIntent.putExtra(CompletedActivity.TIME, data.getString("intime"));
			}
		}
	}

	private void checkIntime(JSONObject root, Intent nextIntent) throws JSONException {
		JSONArray timecardArray = root.getJSONArray("timecard");
		JSONObject timecard1 = timecardArray.getJSONObject(0);
		String intime = timecard1.getString("intime");
		if(intime != null && intime.contains("-1")) {
			nextIntent.putExtra(CompletedActivity.NOT_IN, true);
		} else {
			nextIntent.putExtra(CompletedActivity.NOT_IN, false);
		}
	}

	@Override
	public void onResume() {
		super.onResume();

		LinearLayout syukkin = (LinearLayout)this.findViewById(R.id.include_syukkin);
		LinearLayout taikin = (LinearLayout)this.findViewById(R.id.include_taikin);
		LinearLayout idou = (LinearLayout)this.findViewById(R.id.include_idou);

		// ルートに応じてレイアウト変更
		switch(Singleton.INSTANCE.getSyuttaiStatus()) {
		case Main.SYUKKIN:
			taikin.setVisibility(View.GONE);
			idou.setVisibility(View.GONE);
			this.syukkinInit(syukkin);
			break;
		case Main.TAIKIN:
			syukkin.setVisibility(View.GONE);
			idou.setVisibility(View.GONE);
			this.taikinInit(taikin);
			break;
		case Main.IDOU:
			syukkin.setVisibility(View.GONE);
			taikin.setVisibility(View.GONE);
			this.idouInit(idou);
			break;
		}
	}

}
