package jp.co.tecwt.medi_techno_service.view;

import java.io.File;

import jp.co.tecwt.medi_techno_service.R;
import jp.co.tecwt.medi_techno_service.data.DialogHolder;
import jp.co.tecwt.medi_techno_service.data.MyPreference;
import jp.co.tecwt.medi_techno_service.data.Singleton;
import jp.co.tecwt.medi_techno_service.dialog.AuthDialog;
import jp.co.tecwt.medi_techno_service.dialog.MyDialog;
import jp.co.tecwt.medi_techno_service.model.AppNewVersionChecker;
import jp.co.tecwt.medi_techno_service.util.MyHttpClient2;
import jp.co.tecwt.medi_techno_service.util.MyHttpClient3_Download;
import jp.co.tecwt.medi_techno_service.util.Util;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class AdminActivity extends BaseActivity {

	public static final int REQUEST_ID_EDIT = 1;
	public static final int RESULT_ID_EDIT_OK = 1;
	public static final int RESULT_ID_EDIT_NG = 2;

	public static final int REQUEST_APPINITCUSTOM = 2;
	public static final int RESULT_SOSHIKI_ID_OK = 1;
	public static final int RESULT_SOSHIKI_ID_NG = 2;

	public static final String DIALOG_TAG = "AdminActivity";

	private boolean isLoadingUpdate;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// レイアウトファイル適用
		setContentView(R.layout.activity_admin);

		// リスナー登録
		((Button)this.findViewById(R.id.button_back)).setOnClickListener(this.backBtn);
		((Button)this.findViewById(R.id.button_top)).setOnClickListener(this.topBtn);
		((Button)this.findViewById(R.id.button_idedit)).setOnClickListener(this.ideditBtn);
		((Button)this.findViewById(R.id.button_appinitcustom)).setOnClickListener(this.appinitcustomBtn);
		((Button)this.findViewById(R.id.button_nyuuryokukoumoku)).setOnClickListener(this.nyuuryokukoumokuBtn);
		((Button)this.findViewById(R.id.button_unitinfo)).setOnClickListener(this.unitinfoBtn);
		((Button)this.findViewById(R.id.button_update)).setOnClickListener(this.updateBtn);
	}


	// ID登録
	private OnClickListener ideditBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			Intent ideditIntent = new Intent(AdminActivity.this, IDEditActivity.class);
			AdminActivity.this.startActivityForResult(ideditIntent, REQUEST_ID_EDIT);
		}
	};

	// 詳細設定
	private OnClickListener appinitcustomBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			AdminActivity.this.showAuthDialog(AdminActivity.this.appinitcustomCallback);
		}
	};

	// 項目設定
	private OnClickListener nyuuryokukoumokuBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			Intent ideditIntent = new Intent(AdminActivity.this, NyuuryokuKoumokuActivity.class);
			AdminActivity.this.startActivityForResult(ideditIntent, AdminActivity.REQUEST_ID_EDIT);
		}
	};

	// 端末情報
	private OnClickListener unitinfoBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			// 端末情報取得
			MyPreference pref = MyPreference.getInstance(AdminActivity.this);
			String deviceName = pref.getDeviceName();
			String deviceId = pref.getDeviceID();
			StringBuilder text = new StringBuilder();
			text.append("\n");
			text.append("端末名(初期)： ").append(deviceName).append("\n");
			text.append("\n");
			text.append("端末ID： ").append(deviceId).append("\n");
			text.append("\n");
			String versionName = Util.GetApplicationVersionName(AdminActivity.this);
			text.append("アプリのバージョン: ").append(versionName != null ? versionName : "表示できません").append("\n");
			text.append("\n");
			text.append("現在位置： ")
				.append(Singleton.INSTANCE.getLat()).append(", ")
				.append(Singleton.INSTANCE.getLng()).append("\n");

			// ダイアログ表示
			MyDialog dialog = new MyDialog();
			dialog.setTitle("端末情報");
			dialog.setMessage(text.toString());
			dialog.setPositive_button("OK", null);
			dialog.setCancelable(false);
			dialog.show(AdminActivity.this.getFragmentManager(), null);
		}
	};

	// アプリ更新
	private OnClickListener updateBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			// 連続起動阻止
			if(isLoadingUpdate) {
				return;
			} else {
				isLoadingUpdate = true;
			}

			final AppNewVersionChecker checker = new AppNewVersionChecker(AdminActivity.this);
			checker.connect(new MyHttpClient2.Responser() {

				@Override
				public void onFailed() {
					// 失敗
					AdminActivity.this.showErrorDialog(null, null);
					isLoadingUpdate = false;
				}

				@Override
				public void onError(String errorcode, String message) {
					// 人為的失敗
					AdminActivity.this.showErrorDialog(errorcode, message);
					isLoadingUpdate = false;
				}

				@Override
				public void onCompleted(String json) {
					if(checker.isExistNewVersion()) {
						// 最新版ダウンロード確認
						MyDialog dialog = new MyDialog();
						dialog.setMode(MyDialog.MODE_NORMAL);
						dialog.setTitle("CAUTION!!");
						dialog.setMessage("アプリの最新版がアップロードされています。\nダウンロードを開始しますか？");
						dialog.setPositive_button("はい", new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								// 最新版ダウンロード開始
								checker.download(new MyHttpClient3_Download.Responser() {

									@Override
									public void onFailed() {
										// 失敗
										AdminActivity.this.showErrorDialog(null, null);
										isLoadingUpdate = false;
									}

									@Override
									public void onError(String errorcode, String message) {
										// 人為的失敗
										AdminActivity.this.showErrorDialog(errorcode, message);
										isLoadingUpdate = false;
									}

									@Override
									public void onCompleted(File tmpApkFile) {
										// 成功
										try {
											//現在のバージョンコード記録
											MyPreference pref = MyPreference.getInstance(AdminActivity.this);
											int versionCode = Util.GetApplicationVersionCode(AdminActivity.this);
											pref.writeApplicationPreviousVersionCode(versionCode);
											//更新時初期化フラグ初期化
											pref.writeApplicationUpdateInitialized(false);
										} catch (Exception ex) {}
										// インストーラ起動
										Intent intent = new Intent(Intent.ACTION_VIEW);
										Uri dataUri = Uri.fromFile(tmpApkFile);
										intent.setDataAndType(dataUri, "application/vnd.android.package-archive");
										startActivity(intent);
										isLoadingUpdate = false;
									}
								}, "ダウンロード中...");
							}
						});
						dialog.setNegative_button("キャンセル", null);
						dialog.setCancelable(false);
						dialog.show(getFragmentManager(), null);
					} else {
						// 終了
						MyDialog dialog = new MyDialog();
						dialog.setMode(MyDialog.MODE_NORMAL);
						dialog.setTitle("アプリのバージョン");
						dialog.setMessage("現在このアプリは最新版です。");
						dialog.setPositive_button("OK", null);
						dialog.show(getFragmentManager(), null);
					}
					isLoadingUpdate = false;
				}
			});
		}
	};

	/**
	 * 認証確認ダイアログ表示
	 * @param callback
	 */
	private void showAuthDialog(AuthDialog.Callback callback) {
		// 多重起動阻止
		if(AdminActivity.this.getFragmentManager().findFragmentByTag(DIALOG_TAG) != null) {
			return;
		}

		AuthDialog dialog = new AuthDialog();
		dialog.setNextClassCallBack(callback);
		dialog.show(AdminActivity.this.getFragmentManager(), DIALOG_TAG);
	}

	// 認証確認完了
	public AuthDialog.Callback appinitcustomCallback = new AuthDialog.Callback() {
		@Override
		public void callback() {
			// ダイアログの値を初期化
			DialogHolder.INSTANCE.init();

			// 画面遷移
			Intent ideditIntent = new Intent(AdminActivity.this, AppInitCustomActivity.class);
			AdminActivity.this.startActivityForResult(ideditIntent, AdminActivity.REQUEST_APPINITCUSTOM);
		}
	};

	/**
	 * エラーダイアログ表示
	 * @param errorcode
	 * @param message
	 */
	private void showErrorDialog(String errorcode, String message) {

		// 多重起動阻止
		String DIALOG_NAME = this.getClass().getName();
		if(this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
			return;
		}

		String dialogMessage;
		if(errorcode == null && message == null) {
			dialogMessage = "エラーが発生しました。お手数ですが再度操作を行ってください。";
		}else {
			dialogMessage = "エラーが発生しました。お手数ですが再度操作を行ってください。"+"\n―――"+"\nerrorcode: "+errorcode+"\nmessage: "+message;
		}
		MyDialog dialog = new MyDialog();
		dialog.setMode(MyDialog.MODE_NORMAL);
		dialog.setTitle("ERROR!!");
		dialog.setMessage(dialogMessage);
		dialog.setPositive_button("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				//Intent topIntent = new Intent(AdminActivity.this, Main.class);
				//topIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
				//AdminActivity.this.startActivity(topIntent);
			}
		});
		dialog.setCancelable(false);
		dialog.show(this.getFragmentManager(), DIALOG_NAME);
	}


	@Override
	public void onResume() {
		super.onResume();
		// 値の初期化
		Singleton.INSTANCE.init();
		DialogHolder.INSTANCE.init();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(requestCode) {

		case AdminActivity.REQUEST_ID_EDIT:
			// ID登録完了
			if(resultCode == AdminActivity.RESULT_ID_EDIT_OK) {
				MyDialog dialog = new MyDialog();
				dialog.setTitle("登録完了");
				dialog.setMessage("登録が完了しました。どの端末からでも利用できます。");
				dialog.setPositive_button("OK", null);
				dialog.setCancelable(false);
				dialog.show(this.getFragmentManager(), null);
			}
			break;

		default:
			break;
		}
	}

}
