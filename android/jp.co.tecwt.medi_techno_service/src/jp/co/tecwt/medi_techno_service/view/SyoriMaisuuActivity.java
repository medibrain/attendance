package jp.co.tecwt.medi_techno_service.view;

import java.util.ArrayList;

import jp.co.tecwt.medi_techno_service.data.Singleton;
import jp.co.tecwt.medi_techno_service.util.NextAct;
import jp.co.tecwt.medi_techno_service.R;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * 処理枚数画面。
 */
public class SyoriMaisuuActivity extends BaseActivity {
	
	private TextView txt_syorimaisuu;
	private static final int MAX_LENGTH = 7;
	private ArrayList<String> strs = new ArrayList<String>(SyoriMaisuuActivity.MAX_LENGTH);
	
	private Button next;
	private boolean checkON;
	private boolean isJump;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// 必須確認
		this.checkON = this.getIntent().getBooleanExtra(NextAct.ISHISSU, true);
	}

	
	/**
	 * 次へ
	 */
	private OnClickListener nextBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			Singleton.INSTANCE.setSyoriMaisuu(SyoriMaisuuActivity.this.numberBuild());
			
			// 確認画面からのジャンプならアクティビティを閉じる
			if(SyoriMaisuuActivity.this.isJump) {
				SyoriMaisuuActivity.this.finish();
				return;
			}
			
			Intent nextIntent = NextAct.getNextIntent(SyoriMaisuuActivity.this, Singleton.INSTANCE.getSyuttaiStatus());
			SyoriMaisuuActivity.this.startActivityForResult(nextIntent, 0);
		}
	};
	
	/**
	 * 番号各種
	 */
	private OnClickListener numberBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			String number = view.getTag().toString();
			
			if(SyoriMaisuuActivity.this.strs.size() == 1 && SyoriMaisuuActivity.this.numberBuild().equals("0")) {
				// 入力されている数値が0なら消去
				SyoriMaisuuActivity.this.strs.remove(0);
			}
			
			if(SyoriMaisuuActivity.this.strs.size() < SyoriMaisuuActivity.MAX_LENGTH) {
				// 番号をリストに入れる
				SyoriMaisuuActivity.this.strs.add(number);
				SyoriMaisuuActivity.this.numberPrint();
				SyoriMaisuuActivity.this.buttonUpdate();
			}
		}
	};
	
	/**
	 * Back
	 */
	private OnClickListener numberbackBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			/*
			if(SyoriMaisuuActivity.this.strs.size() > 0) {
				SyoriMaisuuActivity.this.strs.remove(SyoriMaisuuActivity.this.strs.size()-1);
				SyoriMaisuuActivity.this.numberPrint();
				SyoriMaisuuActivity.this.buttonUpdate();
			}*/
			SyoriMaisuuActivity.this.strs.clear();
			SyoriMaisuuActivity.this.numberPrint();
			SyoriMaisuuActivity.this.buttonUpdate();
		}
	};
	
	private void numberPrint() {
		String number = this.numberBuild();
		this.txt_syorimaisuu.setText(number);
	}
	
	private String numberBuild() {
		StringBuilder builder = new StringBuilder();
		for(String str : this.strs) {
			builder.append(str);
		}
		return builder.toString();
	}
	
	private void buttonUpdate() {
		if(this.checkON) {
			if(this.strs.size() == 0) {
				this.next.setEnabled(false);
			}else {
				this.next.setEnabled(true);
			}
		}else {
			this.next.setEnabled(true);
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		// 縦横でレイアウト変更
		Configuration config = this.getResources().getConfiguration();
		if(config.orientation == Configuration.ORIENTATION_PORTRAIT) {
			// 縦
			this.setContentView(R.layout.activity_syorimaisuu_portrait);
		}else {
			// 横
			this.setContentView(R.layout.activity_syorimaisuu);
		}
		
		this.txt_syorimaisuu = (TextView)this.findViewById(R.id.textview_syorimaisuu);
		
		// リスナー登録
		((Button)this.findViewById(R.id.button_back)).setOnClickListener(this.backBtn);
		((Button)this.findViewById(R.id.button_top)).setOnClickListener(this.topBtn);
		this.next = (Button)this.findViewById(R.id.button_next);
		this.next.setOnClickListener(this.nextBtn);
		((Button)this.findViewById(R.id.button_0)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_1)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_2)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_3)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_4)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_5)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_6)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_7)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_8)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_9)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_numberback)).setOnClickListener(this.numberbackBtn);
		
		// 値を復帰
		String syorimaisuu = Singleton.INSTANCE.getSyoriMaisuu();
		if(syorimaisuu != null && this.strs.size() == 0) {
			char[] tmp = syorimaisuu.toCharArray();
			for(int i=0; i<tmp.length; i++) {
				this.strs.add(String.valueOf(tmp[i]));
			}
			this.txt_syorimaisuu.setText(syorimaisuu);
		}
		this.numberPrint();
		this.buttonUpdate();
		
		// 確認画面からのジャンプかを判定
		this.isJump = this.getIntent().getBooleanExtra("isJump", false);
		
		// ジャンプしてきた場合は[次へ]の文字を「確定」に変える
		if(this.isJump) {
			this.next.setText("確定");
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		// 保存
		Singleton.INSTANCE.setSyoriMaisuu(this.numberBuild());
	}
	
	@Override
	public void onActivityResult(int req, int res, Intent d) {
		// ページ数をカウントダウン
		NextAct.countDown(this, Singleton.INSTANCE.getSyuttaiStatus());
	}

}
