package jp.co.tecwt.medi_techno_service.view;

import java.util.ArrayList;

import jp.co.tecwt.medi_techno_service.data.Singleton;
import jp.co.tecwt.medi_techno_service.util.NextAct;
import jp.co.tecwt.medi_techno_service.R;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * 休憩時間画面。
 */
public class KyukeiActivity extends BaseActivity {
	
	private TextView txt_kyukei;
	private static final int MAX_LENGTH = 3;
	private ArrayList<String> strs_minute = new ArrayList<String>(MAX_LENGTH);
	
	private Button next;
	private boolean checkON;
	private boolean isJump;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// デフォルトは60分
		String kyukei = Singleton.INSTANCE.getKyukei();
		if(kyukei == null) {
			Singleton.INSTANCE.setKyukei("60");
		}
		
		// 必須確認
		this.checkON = this.getIntent().getBooleanExtra(NextAct.ISHISSU, true);
	}
	
	
	/**
	 * 60
	 */
	private OnClickListener plus60Btn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			
			// 強制60
			KyukeiActivity.this.strs_minute.clear();
			KyukeiActivity.this.strs_minute.add("6");
			KyukeiActivity.this.strs_minute.add("0");
			
			KyukeiActivity.this.numberPrint();
			KyukeiActivity.this.buttonUpdate();
		}
	};
	
	/**
	 * +15
	 */
	private OnClickListener plusBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			
			String minuteStr = KyukeiActivity.this.numberMinuteBuild();
			if(minuteStr == null || minuteStr.equals("")) {
				// 未入力からなら強制15
				KyukeiActivity.this.strs_minute.clear();
				KyukeiActivity.this.strs_minute.add("1");
				KyukeiActivity.this.strs_minute.add("5");
				KyukeiActivity.this.numberPrint();
				KyukeiActivity.this.buttonUpdate();
				return;
			}
			
			int minute = Integer.valueOf(minuteStr);
			
			// +15
			minute = minute + 15;
			
			// 999まで
			if(minute > 999) {
				return;
			}
			
			// 数字をバラしてリストに入れる
			String numberStr = String.valueOf(minute);
			KyukeiActivity.this.strs_minute.clear();
			for(int i=0; i<numberStr.length(); i++) {
				KyukeiActivity.this.strs_minute.add(String.valueOf(numberStr.charAt(i)));
			}
			
			KyukeiActivity.this.numberPrint();
			KyukeiActivity.this.buttonUpdate();
		}
	};
	
	/**
	 * -15
	 */
	private OnClickListener minusBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			
			String minuteStr = KyukeiActivity.this.numberMinuteBuild();
			if(minuteStr == null || minuteStr.equals("")) {
				// 未入力なら強制0
				KyukeiActivity.this.strs_minute.add("0");
				KyukeiActivity.this.numberPrint();
				KyukeiActivity.this.buttonUpdate();
				return;
			}
			
			int minute = Integer.valueOf(minuteStr);
			
			// -15
			minute = minute - 15;
			
			// 正の数のみ
			if(minute < 0) {
				return;
			}
			
			// 数字をバラしてリストに入れる
			String numberStr = String.valueOf(minute);
			KyukeiActivity.this.strs_minute.clear();
			for(int i=0; i<numberStr.length(); i++) {
				KyukeiActivity.this.strs_minute.add(String.valueOf(numberStr.charAt(i)));
			}
			
			KyukeiActivity.this.numberPrint();
			KyukeiActivity.this.buttonUpdate();
		}
	};
	
	/**
	 * 次へ
	 */
	private OnClickListener nextBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			Singleton.INSTANCE.setKyukei(KyukeiActivity.this.numberMinuteBuild());
			
			// 確認画面からのジャンプならアクティビティを閉じる
			if(KyukeiActivity.this.isJump) {
				KyukeiActivity.this.finish();
				return;
			}
			
			Intent nextIntent = NextAct.getNextIntent(KyukeiActivity.this, Singleton.INSTANCE.getSyuttaiStatus());
			KyukeiActivity.this.startActivityForResult(nextIntent, 0);
		}
	};
	
	private void numberPrint() {
		this.txt_kyukei.setText(this.numberMinuteBuild());
	}
	
	private String numberMinuteBuild() {
		StringBuilder builder = new StringBuilder();
		for(String str : this.strs_minute) {
			builder.append(str);
		}
		return builder.toString();
	}
	
	private void buttonUpdate() {
		if(checkON) {
			if(this.strs_minute.size() > 0) {
				this.next.setEnabled(true);
			}else {
				this.next.setEnabled(false);
			}
		}else {
			this.next.setEnabled(true);
		}
	}
	
	
	@Override
	public void onResume() {
		super.onResume();
		
		// 縦横でレイアウト変更
		Configuration config = this.getResources().getConfiguration();
		if(config.orientation == Configuration.ORIENTATION_PORTRAIT) {
			// 縦
			this.setContentView(R.layout.activity_kyukei2_portrait);
		}else {
			// 横
			this.setContentView(R.layout.activity_kyukei2);
		}
		
		// リスナー登録
		((Button)this.findViewById(R.id.button_back)).setOnClickListener(this.backBtn);
		((Button)this.findViewById(R.id.button_top)).setOnClickListener(this.topBtn);
		((Button)this.findViewById(R.id.button_plus60)).setOnClickListener(this.plus60Btn);
		((Button)this.findViewById(R.id.button_plus)).setOnClickListener(this.plusBtn);
		((Button)this.findViewById(R.id.button_minus)).setOnClickListener(this.minusBtn);
		this.next = (Button)this.findViewById(R.id.button_next);
		this.next.setOnClickListener(this.nextBtn);
		this.txt_kyukei = (TextView)this.findViewById(R.id.textview_kyukei);
		
		// 入力されていた値を復帰
		String kyukei = Singleton.INSTANCE.getKyukei();
		if(kyukei != null && this.strs_minute.size() == 0) {
			char[] tmp = kyukei.toCharArray();
			for(int i=0; i<tmp.length; i++) {
				this.strs_minute.add(String.valueOf(tmp[i]));
			}
		}
		this.numberPrint();
		this.buttonUpdate();
		
		// 確認画面からのジャンプかを判定
		this.isJump = this.getIntent().getBooleanExtra("isJump", false);
		
		// ジャンプしてきた場合は[次へ]の文字を「確定」に変える
		if(this.isJump) {
			this.next.setText("確定");
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		// 保存
		Singleton.INSTANCE.setKyukei(this.numberMinuteBuild());
	}
	
	@Override
	public void onActivityResult(int req, int res, Intent d) {
		// ページ数をカウントダウン
		NextAct.countDown(this, Singleton.INSTANCE.getSyuttaiStatus());
	}

}
