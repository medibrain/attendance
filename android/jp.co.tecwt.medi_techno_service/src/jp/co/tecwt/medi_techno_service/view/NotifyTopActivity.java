package jp.co.tecwt.medi_techno_service.view;

import java.util.ArrayList;
import java.util.List;

import jp.co.tecwt.medi_techno_service.R;
import jp.co.tecwt.medi_techno_service.data.NotifyDB;
import jp.co.tecwt.medi_techno_service.data.NotifyDB.Notify;
import jp.co.tecwt.medi_techno_service.dialog.MyDialog;
import jp.co.tecwt.medi_techno_service.util.DateTime;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;

/*
 * まだまだ未完成
 * レイアウトもタブレット版のみできてる
 */
public class NotifyTopActivity extends BaseActivity {

	private ExpandableListView currentExListView;
	private ExpandableListView exListView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		this.setContentView(R.layout.activity_notify_top);
		((Button)this.findViewById(R.id.button_back)).setOnClickListener(backBtn);
		((Button)this.findViewById(R.id.button_top)).setOnClickListener(topBtn);
		((Button)this.findViewById(R.id.button_notify_create)).setOnClickListener(createBtn);
		((Button)this.findViewById(R.id.button_notify_edit)).setOnClickListener(editBtn);
		this.currentExListView = (ExpandableListView)this.findViewById(R.id.expandableListView_currentNotityExList);
		this.exListView = (ExpandableListView)this.findViewById(R.id.expandableListView_notifyExList);
		setNotifyLists();
	}

	private void setNotifyLists() {
		// 現在表示される通知一覧
		List<Notify> cNList = NotifyDB.GetNotifiesInDuration(this, DateTime.GetToday());
		List<String> sCList = new ArrayList<String>();
		for (Notify n : cNList) {
			sCList.add(createListTitle(n.startDate, n.finishDate, n.title));
		}
		CurrentNotifyListAdapter cAdapter = new CurrentNotifyListAdapter(this, sCList, cNList);
		this.currentExListView.setAdapter(cAdapter);
		// 通知一覧（履歴）
		List<Notify> nList = NotifyDB.GetNotifyList(this);
		List<String> sList = new ArrayList<String>();
		for (Notify n : nList) {
			sList.add(createListTitle(n.startDate, n.finishDate, n.title));
		}
		NotifyListAdapter adapter = new NotifyListAdapter(this, sList, nList);
		this.exListView.setAdapter(adapter);
	}

	/**
	 * 現在表示期間中の通知一覧を管理する<br>
	 * 親Viewに期間とタイトル、子Viewに本文を表示。これらは一対一となります。
	 */
	private class CurrentNotifyListAdapter extends BaseExpandableListAdapter {
		private Context context;
		private List<String> sList;
		private List<Notify> nList;
		public CurrentNotifyListAdapter(Context context, List<String> sList, List< Notify> nList) {
			this.context = context;
			this.sList = sList;
			this.nList = nList;
		}
		@Override
		public View getGroupView(final int arg0, boolean arg1, View arg2, ViewGroup arg3) {
			/*
			 * 親View
			 * [タイトル　表示期間] [編集・削除ボタン]
			 */
			if(arg2 == null) {
				arg2 = LayoutInflater.from(this.context).inflate(R.layout.item_notify_separator, null);
			}

			final String title = sList.get(arg0).toString();
			((TextView)arg2.findViewById(R.id.textview_separator)).setText(title);
			((Button)arg2.findViewById(R.id.button_notify_edit)).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					MyDialog dialog = new MyDialog();
					dialog.setTitle("確認");
					dialog.setMessage("［" + title + "］を削除してもよろしいですか？¥n※削除したものは復元できません。");
					dialog.setPositive_button("OK", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							NotifyDB.DeleteNotify(NotifyTopActivity.this, nList.get(arg0).id);
						}
					});
					dialog.setNegative_button("キャンセル", null);
					dialog.show(getFragmentManager(), null);
				}
			});
			return arg2;
		}
		@Override
		public View getChildView(int arg0, int arg1, boolean arg2, View arg3, ViewGroup arg4) {
			/*
			 * 子View
			 * [本文]
			 * [表示タイプ]
			 * [表示勤怠]
			 */
			if(arg3 == null) {
				arg3 = LayoutInflater.from(this.context).inflate(R.layout.item_notify_child, null);
			}

			Notify notify = (Notify)this.getChild(arg0, arg1);
			((TextView)arg3.findViewById(R.id.textview_child_message)).setText(notify.message);
			((TextView)arg3.findViewById(R.id.textview_child_showmode)).setText(notify.GetShowModeStr());
			((TextView)arg3.findViewById(R.id.textview_child_showwork)).setText(notify.GetShowWorksStr());

			return arg3;
		}
		@Override
		public Object getChild(int arg0, int arg1) {
			return this.nList.get(arg1);
		}
		@Override
		public long getChildId(int arg0, int arg1) {
			return arg1;
		}
		@Override
		public int getChildrenCount(int arg0) {
			return this.nList.size();
		}
		@Override
		public Object getGroup(int arg0) {
			return this.sList.get(arg0);
		}
		@Override
		public int getGroupCount() {
			return this.sList.size();
		}
		@Override
		public long getGroupId(int arg0) {
			return arg0;
		}
		@Override
		public boolean hasStableIds() {
			return true;
		}
		@Override
		public boolean isChildSelectable(int arg0, int arg1) {
			return true;
		}
	}

	/**
	 * 通知一覧（履歴）を管理する<br>
	 * 親Viewに期間とタイトル、子Viewに本文を表示。これらは一対一となります。
	 */
	private class NotifyListAdapter extends BaseExpandableListAdapter {
		private Context context;
		private List<String> sList;
		private List<Notify> nList;
		public NotifyListAdapter(Context context, List<String> sList, List< Notify> nList) {
			this.context = context;
			this.sList = sList;
			this.nList = nList;
		}
		@Override
		public View getGroupView(int arg0, boolean arg1, View arg2, ViewGroup arg3) {
			return null;
		}
		@Override
		public View getChildView(int arg0, int arg1, boolean arg2, View arg3, ViewGroup arg4) {
			return null;
		}
		@Override
		public Object getChild(int arg0, int arg1) {
			return this.nList.get(arg1);
		}
		@Override
		public long getChildId(int arg0, int arg1) {
			return arg1;
		}
		@Override
		public int getChildrenCount(int arg0) {
			return this.nList.size();
		}
		@Override
		public Object getGroup(int arg0) {
			return this.sList.get(arg0);
		}
		@Override
		public int getGroupCount() {
			return this.sList.size();
		}
		@Override
		public long getGroupId(int arg0) {
			return arg0;
		}
		@Override
		public boolean hasStableIds() {
			return true;
		}
		@Override
		public boolean isChildSelectable(int arg0, int arg1) {
			return true;
		}
	}

	private String createListTitle(String startDate, String finishDate, String title) {
		if(startDate == null || finishDate == null || title == null) {
			return "【----/--/-- 〜 ----/--/--】　---";
		}
		String strD = startDate.replace("-", "/");
		String fnsD = finishDate.replace("-", "/");
		String[] strs = strD.split("/", 0);
		String[] fnss = fnsD.split("/", 0);
		if(strs.length != 3 || strs.length != fnss.length) {
			return String.format("【%s 〜 %s】　%s", strD, fnsD, title);
		}
		if(strs[0].equals(fnss[0])) {
			//年が一緒
			return String.format("【%s 〜 %s】　%s", strD, fnss[1] + "/" +fnss[2], title);
		}
		if(strs[0].equals(fnss[0]) && strs[1].equals(fnss[1])) {
			//年月が一緒
			return String.format("【%s 〜 %s】　%s", strD, fnss[2], title);
		}
		return String.format("【%s 〜 %s】　%s", strD, fnsD, title);
	}

	private OnClickListener createBtn = new OnClickListener() {
		@Override
		public void onClick(View v) {
			/*
			 * 登録画面へ
			 */
		}
	};

	private OnClickListener editBtn = new OnClickListener() {
		@Override
		public void onClick(View v) {
			/*
			 * 過去作成リスト画面へ
			 */
		}
	};
}
