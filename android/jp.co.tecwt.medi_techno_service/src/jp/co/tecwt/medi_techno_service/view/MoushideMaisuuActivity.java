package jp.co.tecwt.medi_techno_service.view;

import java.util.ArrayList;

import jp.co.tecwt.medi_techno_service.data.Singleton;
import jp.co.tecwt.medi_techno_service.util.NextAct;
import jp.co.tecwt.medi_techno_service.R;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * 申出枚数画面。
 */
public class MoushideMaisuuActivity extends BaseActivity {
	
	private TextView txt_moushidemaisuu;
	private static final int MAX_LENGTH = 7;
	private ArrayList<String> strs = new ArrayList<String>(MoushideMaisuuActivity.MAX_LENGTH);
	
	private Button next;
	private boolean checkON;
	private boolean isJump;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// 必須確認
		this.checkON = this.getIntent().getBooleanExtra(NextAct.ISHISSU, true);		
	}
	

	/**
	 * 次へ
	 */
	private OnClickListener nextBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			Singleton.INSTANCE.setMoushideMaisuu(MoushideMaisuuActivity.this.numberBuild());
			
			// 確認画面からのジャンプならアクティビティを閉じる
			if(MoushideMaisuuActivity.this.isJump) {
				MoushideMaisuuActivity.this.finish();
				return;
			}

			Intent nextIntent = NextAct.getNextIntent(MoushideMaisuuActivity.this, Singleton.INSTANCE.getSyuttaiStatus());
			MoushideMaisuuActivity.this.startActivityForResult(nextIntent, 0);
		}
	};
	

	/**
	 * 番号各種
	 */
	private OnClickListener numberBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			String number = view.getTag().toString();
			
			if(MoushideMaisuuActivity.this.strs.size() == 1 && MoushideMaisuuActivity.this.numberBuild().equals("0")) {
				// 現在の数値が0であれば消去する
				MoushideMaisuuActivity.this.strs.remove(0);
			}
			
			if(MoushideMaisuuActivity.this.strs.size() < MoushideMaisuuActivity.MAX_LENGTH) {
				// 数字をリストに入れる
				MoushideMaisuuActivity.this.strs.add(number);
				MoushideMaisuuActivity.this.numberPrint();
				MoushideMaisuuActivity.this.buttonUpdate();
			}
		}
	};
	

	/**
	 * Back
	 */
	private OnClickListener numberbackBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			/*
			if(MoushideMaisuuActivity.this.strs.size() > 0) {
				MoushideMaisuuActivity.this.strs.remove(MoushideMaisuuActivity.this.strs.size()-1);
				MoushideMaisuuActivity.this.numberPrint();
				MoushideMaisuuActivity.this.buttonUpdate();
			}*/
			MoushideMaisuuActivity.this.strs.clear();
			MoushideMaisuuActivity.this.numberPrint();
			MoushideMaisuuActivity.this.buttonUpdate();
		}
	};
	
	private void numberPrint() {
		String number = this.numberBuild();
		this.txt_moushidemaisuu.setText(number);
	}
	
	private String numberBuild() {
		StringBuilder builder = new StringBuilder();
		for(String str : this.strs) {
			builder.append(str);
		}
		return builder.toString();
	}
	
	private void buttonUpdate() {
		if(this.checkON) {
			if(this.strs.size() == 0) {
				this.next.setEnabled(false);
			}else {
				this.next.setEnabled(true);
			}
		}else {
			this.next.setEnabled(true);
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		// 縦横でレイアウト変更
		Configuration config = this.getResources().getConfiguration();
		if(config.orientation == Configuration.ORIENTATION_PORTRAIT) {
			// 縦
			this.setContentView(R.layout.activity_moushidemaisuu_portrait);
		}else {
			// 横
			this.setContentView(R.layout.activity_moushidemaisuu);
		}
		
		this.txt_moushidemaisuu = (TextView)this.findViewById(R.id.textview_moushidemaisuu);
		
		// リスナー登録
		((Button)this.findViewById(R.id.button_back)).setOnClickListener(this.backBtn);
		((Button)this.findViewById(R.id.button_top)).setOnClickListener(this.topBtn);
		this.next = (Button)this.findViewById(R.id.button_next);
		this.next.setOnClickListener(this.nextBtn);
		((Button)this.findViewById(R.id.button_0)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_1)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_2)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_3)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_4)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_5)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_6)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_7)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_8)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_9)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_numberback)).setOnClickListener(this.numberbackBtn);
		
		// 入力されている値を復帰する
		String MoushideMaisuu = Singleton.INSTANCE.getMoushideMaisuu();
		if(MoushideMaisuu != null && this.strs.size() == 0) {
			char[] tmp = MoushideMaisuu.toCharArray();
			for(int i=0; i<tmp.length; i++) {
				this.strs.add(String.valueOf(tmp[i]));
			}
			this.txt_moushidemaisuu.setText(MoushideMaisuu);
		}
		this.numberPrint();
		this.buttonUpdate();
		
		// 確認画面からのジャンプかを判定
		this.isJump = this.getIntent().getBooleanExtra("isJump", false);
		
		// ジャンプしてきた場合は[次へ]の文字を「確定」に変える
		if(this.isJump) {
			this.next.setText("確定");
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		// 保存
		Singleton.INSTANCE.setMoushideMaisuu(this.numberBuild());
	}
	
	@Override
	public void onActivityResult(int req, int res, Intent d) {
		// ページ数をカウントダウン
		NextAct.countDown(this, Singleton.INSTANCE.getSyuttaiStatus());
	}
}
