package jp.co.tecwt.medi_techno_service.view;

import java.util.ArrayList;

import jp.co.tecwt.medi_techno_service.R;
import jp.co.tecwt.medi_techno_service.data.MyPreference;
import jp.co.tecwt.medi_techno_service.dialog.MyDialog;
import jp.co.tecwt.medi_techno_service.util.Util;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class NyuuryokuKoumokuActivity extends BaseActivity {

	private ExpandableListView exListView;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// レイアウトファイル適用
		setContentView(R.layout.activity_nyuuryokukoumoku);

		// リスナー登録
		((Button)this.findViewById(R.id.button_back)).setOnClickListener(this.backBtn);
		((Button)this.findViewById(R.id.button_top)).setOnClickListener(this.topBtn);
		this.exListView = (ExpandableListView)this.findViewById(R.id.expandablelistview_nyuuryokukoumoku);

		// アダプター登録
		MyExpandableListAdapter exAdapter = new MyExpandableListAdapter(this);
		this.exListView.setAdapter(exAdapter);
	}


	/**
	 * 項目表示アダプター
	 */
	private class MyExpandableListAdapter extends BaseExpandableListAdapter {

		private ArrayList<String> groups;
	    private ArrayList<ArrayList<Item>> children;
	    private Context context = null;

	    public MyExpandableListAdapter(Context context) {
	    	this.context = context;
	    	this.init();
	    }

	    private void init() {
	    	this.groups = new ArrayList<String>();				//親リスト
	    	this.children = new ArrayList<ArrayList<Item>>();	//子リスト

	    	MyPreference pref = MyPreference.getInstance(this.context);

	    	//時間
	    	this.groups.add("時間の設定【重要】");
	    	ArrayList<Item> timeSetting = new ArrayList<Item>();
	    	timeSetting.add(new Item("規定の時間：", ITEM_TYPE.TEXTVIEW, null));
	    	timeSetting.add(new Item("業務開始（出勤）時刻", ITEM_TYPE.TIME, MyPreference.KEY_WORKINGHOUR_SYUKKIN_TIME).setTime(pref.getWorkingHourSyukkin()));
	    	timeSetting.add(new Item("業務終了（退勤）時刻", ITEM_TYPE.TIME, MyPreference.KEY_WORKINGHOUR_TAIKIN_TIME).setTime(pref.getWorkingHourTaikin()));
	    	timeSetting.add(new Item("時間の修正：", ITEM_TYPE.TEXTVIEW, null));
	    	timeSetting.add(new Item("出勤の過去打刻許可", ITEM_TYPE.FLAG, MyPreference.KEY_INTIME_CHANGE_FLAG).setCheckedPosition(pref.getInTimeChangeFlag() ? 0 : 1));
	    	timeSetting.add(new Item("退勤の未来打刻許可", ITEM_TYPE.FLAG, MyPreference.KEY_OUTTIME_CHANGE_FLAG).setCheckedPosition(pref.getOutTimeChangeFlag() ? 0 : 1));
	    	timeSetting.add(new Item("その他の時間：", ITEM_TYPE.TEXTVIEW, null));
	    	timeSetting.add(new Item("出勤の遅延許容時間", ITEM_TYPE.TIME_MINUTE, MyPreference.KEY_LATESYUKKIN_PERMISSION_MINUTE).setTime(pref.getLateSyukkinPermissionMinute()));
	    	this.children.add(timeSetting);

	    	//早出
	    	this.groups.add("早出設定");
	    	ArrayList<Item> fastSyukkinSetting = new ArrayList<Item>();
	    	fastSyukkinSetting.add(new Item("早出（規定出勤時刻よりも早い出勤）：", ITEM_TYPE.TEXTVIEW, null));
	    	fastSyukkinSetting.add(new Item("承認確認", ITEM_TYPE.FLAG, MyPreference.KEY_FASTSYUKKIN_CONFIRM_FLAG).setCheckedPosition(pref.getFastSyukkinConfirmFlag() ? 0 : 1));
	    	fastSyukkinSetting.add(new Item("承認確認時刻", ITEM_TYPE.TIME, MyPreference.KEY_FASTSYUKKIN_CONFIRM_TIME).setTime(pref.getFastSyukkinConfirmTime()));
	    	fastSyukkinSetting.add(new Item("パスワード要求", ITEM_TYPE.FLAG, MyPreference.KEY_FASTSYUKKIN_PASSWORD_FLAG).setCheckedPosition(pref.getFastSyukkinPasswordFlag() ? 0 : 1));
	    	fastSyukkinSetting.add(new Item("パスワード要求時刻", ITEM_TYPE.TIME, MyPreference.KEY_FASTSYUKKIN_PASSWORD_TIME).setTime(pref.getFastSyukkinPasswordTime()));
	    	this.children.add(fastSyukkinSetting);

	    	//残業
	    	this.groups.add("残業設定");
	    	ArrayList<Item> lateTaikinSetting = new ArrayList<Item>();
	    	lateTaikinSetting.add(new Item("残業（規定退勤時刻よりも遅い打刻）：", ITEM_TYPE.TEXTVIEW, null));
	    	lateTaikinSetting.add(new Item("承認確認", ITEM_TYPE.FLAG, MyPreference.KEY_LATETAIKIN_CONFIRM_FLAG).setCheckedPosition(pref.getLateTaikinConfirmFlag() ? 0 : 1));
	    	lateTaikinSetting.add(new Item("承認確認時刻", ITEM_TYPE.TIME, MyPreference.KEY_LATETAIKIN_CONFIRM_TIME).setTime(pref.getLateTaikinConfirmTime()));
	    	lateTaikinSetting.add(new Item("パスワード要求", ITEM_TYPE.FLAG, MyPreference.KEY_LATETAIKIN_PASSWORD_FLAG).setCheckedPosition(pref.getLateTaikinPasswordFlag() ? 0 : 1));
	    	lateTaikinSetting.add(new Item("パスワード要求時刻", ITEM_TYPE.TIME, MyPreference.KEY_LATETAIKIN_PASSWORD_TIME).setTime(pref.getLateTaikinPasswordTime()));
	    	this.children.add(lateTaikinSetting);

	    	//出勤
	    	this.groups.add("打刻設定：出勤");
	    	ArrayList<Item> syukkinSetting = new ArrayList<Item>();
	    	syukkinSetting.add(new Item("入力項目：", ITEM_TYPE.TEXTVIEW, null));
	    	syukkinSetting.add(new Item("業務の開始時刻", ITEM_TYPE.CHECKABLE, MyPreference.KEY_SYUKKIN_EDITTIME).setHissu(true).setVisible(false).setInVisible(false).setCheckedPosition(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_SYUKKIN_EDITTIME)));
	    	this.children.add(syukkinSetting);

	    	//移動
	    	this.groups.add("打刻設定：移動");
	    	ArrayList<Item> idouSetting = new ArrayList<Item>();
	    	idouSetting.add(new Item("許可：", ITEM_TYPE.TEXTVIEW, null));
	    	idouSetting.add(new Item("移動を許可", ITEM_TYPE.FLAG, MyPreference.KEY_IDOU_FLAG).setCheckedPosition(pref.getIdouFlag() ? 0 : 1));
	    	idouSetting.add(new Item("入力項目：", ITEM_TYPE.TEXTVIEW, null));
	    	idouSetting.add(new Item("移動を開始する時刻", ITEM_TYPE.CHECKABLE, MyPreference.KEY_IDOU_EDITTIME).setHissu(true).setVisible(false).setInVisible(false).setCheckedPosition(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_IDOU_EDITTIME)));
	    	idouSetting.add(new Item("移動前の種別", ITEM_TYPE.CHECKABLE, MyPreference.KEY_IDOU_SYUBETSU).setHissu(true).setVisible(false).setInVisible(false).setCheckedPosition(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_IDOU_SYUBETSU)));
	    	idouSetting.add(new Item("移動前の処理枚数", ITEM_TYPE.CHECKABLE, MyPreference.KEY_IDOU_SYORIMAISUU).setHissu(true).setVisible(true).setInVisible(true).setCheckedPosition(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_IDOU_SYORIMAISUU)));
	    	idouSetting.add(new Item("移動前の申し出枚数", ITEM_TYPE.CHECKABLE, MyPreference.KEY_IDOU_MOUSHIDEMAISUU).setHissu(true).setVisible(true).setInVisible(true).setCheckedPosition(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_IDOU_MOUSHIDEMAISUU)));
	    	idouSetting.add(new Item("移動前の業務内容", ITEM_TYPE.CHECKABLE, MyPreference.KEY_IDOU_GYOUMU).setHissu(true).setVisible(true).setInVisible(true).setCheckedPosition(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_IDOU_GYOUMU)));
	    	this.children.add(idouSetting);

	    	//退勤
	    	this.groups.add("打刻設定：退勤");
	    	ArrayList<Item> taikinSetting = new ArrayList<Item>();
	    	taikinSetting.add(new Item("入力項目：", ITEM_TYPE.TEXTVIEW, null));
	    	taikinSetting.add(new Item("業務の終了時刻", ITEM_TYPE.CHECKABLE, MyPreference.KEY_TAIKIN_EDITTIME).setHissu(true).setVisible(false).setInVisible(false).setCheckedPosition(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_TAIKIN_EDITTIME)));
	    	taikinSetting.add(new Item("種別", ITEM_TYPE.CHECKABLE, MyPreference.KEY_TAIKIN_SYUBETSU).setHissu(true).setVisible(false).setInVisible(false).setCheckedPosition(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_TAIKIN_SYUBETSU)));
	    	taikinSetting.add(new Item("処理枚数", ITEM_TYPE.CHECKABLE, MyPreference.KEY_TAIKIN_SYORIMAISUU).setHissu(true).setVisible(true).setInVisible(true).setCheckedPosition(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_TAIKIN_SYORIMAISUU)));
	    	taikinSetting.add(new Item("申し出枚数", ITEM_TYPE.CHECKABLE, MyPreference.KEY_TAIKIN_MOUSHIDEMAISUU).setHissu(true).setVisible(true).setInVisible(true).setCheckedPosition(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_TAIKIN_MOUSHIDEMAISUU)));
	    	taikinSetting.add(new Item("業務内容", ITEM_TYPE.CHECKABLE, MyPreference.KEY_TAIKIN_GYOUMU).setHissu(true).setVisible(true).setInVisible(true).setCheckedPosition(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_TAIKIN_GYOUMU)));
	    	taikinSetting.add(new Item("休憩時間", ITEM_TYPE.CHECKABLE, MyPreference.KEY_TAIKIN_KYUKEI).setHissu(true).setVisible(true).setInVisible(true).setCheckedPosition(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_TAIKIN_KYUKEI)));
	    	taikinSetting.add(new Item("交通費", ITEM_TYPE.CHECKABLE, MyPreference.KEY_TAIKIN_KOUTSUUHI).setHissu(true).setVisible(true).setInVisible(true).setCheckedPosition(pref.getNyuuryokuKoumokuCheckedPosition(MyPreference.KEY_TAIKIN_KOUTSUUHI)));
	    	this.children.add(taikinSetting);

	    	//入力方法
	    	this.groups.add("入力方法");
	    	ArrayList<Item> inputTypeSetting = new ArrayList<Item>();
	    	inputTypeSetting.add(new Item("入力する方法を設定します", ITEM_TYPE.TEXTVIEW, null));
	    	inputTypeSetting.add(new Item("業務内容",ITEM_TYPE.GYOUMU, MyPreference.KEY_GYOUMU_TYPE).setCheckedPosition(pref.getGyoumuType()));
	    	this.children.add(inputTypeSetting);

	    	//バージョン更新
	    	this.groups.add("バージョン更新について");
	    	ArrayList<Item> updateSetting = new ArrayList<Item>();
	    	updateSetting.add(new Item("アプリ起動時に更新確認", ITEM_TYPE.FLAG, MyPreference.KEY_UPDATE_CHECK_FLAG).setCheckedPosition(pref.getUpdateCheckFlag() ? 0 : 1));
	    	this.children.add(updateSetting);
	    }

	    /**
	     * 親View
	     */
		@Override
		public View getGroupView(int arg0, boolean arg1, View arg2, ViewGroup arg3) {
			if(arg2 == null) {
				arg2 = LayoutInflater.from(this.context).inflate(R.layout.item_nyuuryokukoumoku_parent, null);
			}
			((TextView)arg2.findViewById(R.id.textview_parent)).setText(this.getGroup(arg0).toString());
			return arg2;
		}

		/**
		 * 子View
		 */
		@Override
		public View getChildView(int arg0, int arg1, boolean arg2, View arg3, ViewGroup arg4) {
			final Item item = (Item)this.getChild(arg0, arg1);
			View view = null;
			switch(item.itemType) {
			case TEXTVIEW:
				view = getTypeTextView(item);
				break;
			case CHECKABLE:
				view = getTypeCheckableView(item);
				break;
			case FLAG:
				view = getTypeFlagView(item);
				break;
			case TIME:
				view = getTypeTimeView(item);
				break;
			case TIME_MINUTE:
				view = getTypeTimeMinuteView(item);
				break;
			case GYOUMU:
				view = getGyoumuView(item);
				break;
			default:
				break;
			}
			return view;
		}

		private View getTypeTextView(final Item item) {
			// Viewの設定
			View view = LayoutInflater.from(this.context).inflate(R.layout.item_nyuuryokukoumoku_child_textview, null);
			((TextView)view.findViewById(R.id.textview_title)).setText(item.title);
			return view;
		}

		private View getTypeCheckableView(final Item item) {
			// Viewの設定
			View view = LayoutInflater.from(this.context).inflate(R.layout.item_nyuuryokukoumoku_child_checkable, null);
			((TextView)view.findViewById(R.id.textview_title)).setText(item.title);
			// チェックをつける
			RadioGroup radioGroup = (RadioGroup)view.findViewById(R.id.radiogroup_status);
			int resourceID = 0;
			switch(item.checkedPosition) {
			case 0:
				resourceID = R.id.radio_hissu;
				break;
			case 1:
				resourceID = R.id.radio_visible;
				break;
			case 2:
				resourceID = R.id.radio_invisible;
				break;
			}
			radioGroup.check(resourceID);

			// チェックボタンを表示するか否か
			RadioButton hissu = (RadioButton)radioGroup.findViewById(R.id.radio_hissu);
			hissu.setVisibility(item.hissu ? View.VISIBLE:View.INVISIBLE);
			hissu.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					item.checkedPosition = 0;
					MyPreference pref = MyPreference.getInstance(MyExpandableListAdapter.this.context);
					pref.writeNyuuryokuKoumokuCheckedPosition(item.MyPreference_FieldKey, item.checkedPosition);
				}
			});
			RadioButton visible = (RadioButton)radioGroup.findViewById(R.id.radio_visible);
			visible.setVisibility(item.visible ? View.VISIBLE:View.INVISIBLE);
			visible.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					item.checkedPosition = 1;
					MyPreference pref = MyPreference.getInstance(MyExpandableListAdapter.this.context);
					pref.writeNyuuryokuKoumokuCheckedPosition(item.MyPreference_FieldKey, item.checkedPosition);
				}
			});
			RadioButton invisible = (RadioButton)radioGroup.findViewById(R.id.radio_invisible);
			invisible.setVisibility(item.invisible ? View.VISIBLE:View.INVISIBLE);
			invisible.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					item.checkedPosition = 2;
					MyPreference pref = MyPreference.getInstance(MyExpandableListAdapter.this.context);
					pref.writeNyuuryokuKoumokuCheckedPosition(item.MyPreference_FieldKey, item.checkedPosition);
				}
			});
			return view;
		}

		private View getTypeFlagView(final Item item) {
			// Viewの設定
			View view = LayoutInflater.from(this.context).inflate(R.layout.item_nyuuryokukoumoku_child_flag, null);
			((TextView)view.findViewById(R.id.textview_title)).setText(item.title);
			// チェックをつける
			RadioGroup radioGroup = (RadioGroup)view.findViewById(R.id.radiogroup_status);
			int resourceID = 0;
			switch(item.checkedPosition) {
			case 0:
				resourceID = R.id.radio_enable;
				break;
			case 1:
				resourceID = R.id.radio_disable;
				break;
			}
			radioGroup.check(resourceID);
			// イベントハンドラ
			RadioButton enable = (RadioButton)radioGroup.findViewById(R.id.radio_enable);
			enable.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					item.checkedPosition = 0;
					updateTypeFlag(item.MyPreference_FieldKey, true);
				}
			});
			RadioButton disable = (RadioButton)radioGroup.findViewById(R.id.radio_disable);
			disable.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					item.checkedPosition = 1;
					updateTypeFlag(item.MyPreference_FieldKey, false);
				}
			});
			return view;
		}

		private View getTypeTimeView(final Item item) {
			// Viewの設定
			View view = LayoutInflater.from(this.context).inflate(R.layout.item_nyuuryokukoumoku_child_time, null);
			// タイトルの設定
			final TextView titleView = (TextView)view.findViewById(R.id.textview_title);
			titleView.setText(item.title);
			// 時間の入力
			final int hour = item.time / 100;
			final int minute = item.time - (hour * 100);
			final EditText editHour = (EditText)view.findViewById(R.id.edittext_hour);
			final EditText editMinute = (EditText)view.findViewById(R.id.edittext_minute);
			editHour.setText(String.valueOf(hour));
			editMinute.setText(String.valueOf(minute));
			// イベントハンドラ
			Button buttonTimeOK = (Button)view.findViewById(R.id.button_time_ok);
			buttonTimeOK.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					//キーボードを閉じる
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);	//キーボード及び予測変換も閉じる
					titleView.setFocusable(true);
					titleView.setFocusableInTouchMode(true);
					titleView.requestFocus();
					//チェック
					boolean changeable = true;
					int intH = Util.ToInt(editHour.getText().toString());
					int intM = Util.ToInt(editMinute.getText().toString());
					if (intH < 0 || 23 < intH) {
						editHour.setText(String.valueOf(hour));
						changeable = false;
					}
					if (intM < 0 || 45 < intM || intM % 15 != 0) {
						editMinute.setText(String.valueOf(minute));
						changeable = false;
					}
					if (!changeable) {
						MyDialog dialog = new MyDialog();
						dialog.setTitle("エラー");
						dialog.setMessage("時間の値が不正です。時は0〜23、分は0,15,30,45のみ有効です。");
						dialog.setPositive_button("OK", null);
						dialog.show(getFragmentManager(), "TimeError");
						return;
					}
					//更新
					int time = intH * 100 + intM;
					if (time == item.time) return;	//冗長回避
					updateTypeTime(item.MyPreference_FieldKey, time);
					item.time = time;	//最新の情報に変更
				}
			});
			return view;
		}

		private View getTypeTimeMinuteView(final Item item) {
			// Viewの設定
			View view = LayoutInflater.from(this.context).inflate(R.layout.item_nyuuryokukoumoku_child_time_minute, null);
			// タイトルの設定
			final TextView titleView = (TextView)view.findViewById(R.id.textview_title);
			titleView.setText(item.title);
			// 時間の入力
			final int minute = item.time;
			final EditText editMinute = (EditText)view.findViewById(R.id.edittext_minute);
			editMinute.setText(String.valueOf(minute));
			// イベントハンドラ
			Button buttonTimeOK = (Button)view.findViewById(R.id.button_time_ok);
			buttonTimeOK.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					//キーボードを閉じる
					InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);	//キーボード及び予測変換も閉じる
					titleView.setFocusable(true);
					titleView.setFocusableInTouchMode(true);
					titleView.requestFocus();
					//チェック
					boolean changeable = true;
					int intM = Util.ToInt(editMinute.getText().toString());
					if (intM < 0 || 59 < intM) {
						editMinute.setText(String.valueOf(minute));
						changeable = false;
					}
					if (!changeable) {
						MyDialog dialog = new MyDialog();
						dialog.setTitle("エラー");
						dialog.setMessage("時間の値が不正です。0〜59のみ有効です。");
						dialog.setPositive_button("OK", null);
						dialog.show(getFragmentManager(), "TimeError");
						return;
					}
					//更新
					if (intM == item.time) return;	//冗長回避
					updateTypeTime(item.MyPreference_FieldKey, intM);
					item.time = intM;	//最新の情報に変更
				}
			});
			return view;
		}

		private View getGyoumuView(final Item item) {
			// Viewの設定
			View view = LayoutInflater.from(this.context).inflate(R.layout.item_nyuuryokukoumoku_child_gyoumutype, null);
			// タイトルの設定
			final TextView titleView = (TextView)view.findViewById(R.id.textview_title);
			titleView.setText(item.title);
			// チェックをつける
			RadioGroup radioGroup = (RadioGroup)view.findViewById(R.id.radiogroup_status);
			int resourceID = 0;
			switch(item.checkedPosition) {
			case 0:
				resourceID = R.id.radio_normal;
				break;
			case 1:
				resourceID = R.id.radio_select;
				break;
			}
			radioGroup.check(resourceID);
			// イベントハンドラ
			RadioButton normal = (RadioButton)radioGroup.findViewById(R.id.radio_normal);
			normal.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					item.checkedPosition = 0;
					MyPreference pref = MyPreference.getInstance(MyExpandableListAdapter.this.context);
					pref.writeGyoumuType(MyPreference.GYOUMU_TYPE_NORMAL);
				}
			});
			RadioButton select = (RadioButton)radioGroup.findViewById(R.id.radio_select);
			select.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					item.checkedPosition = 1;
					MyPreference pref = MyPreference.getInstance(MyExpandableListAdapter.this.context);
					pref.writeGyoumuType(MyPreference.GYOUMU_TYPE_SELECT);
				}
			});
			return view;
		}

		private void updateTypeFlag(String MyPreference_FieldKey, boolean flag) {
			MyPreference pref = MyPreference.getInstance(MyExpandableListAdapter.this.context);
			if(MyPreference_FieldKey.equals(MyPreference.KEY_UPDATE_CHECK_FLAG)) {
				pref.writeUpdateCheckFlag(flag);
			} else if (MyPreference_FieldKey.equals(MyPreference.KEY_IDOU_FLAG)) {
				pref.writeIdouFlag(flag);
			} else if (MyPreference_FieldKey.equals(MyPreference.KEY_INTIME_CHANGE_FLAG)) {
				pref.writeInTimeChangeFlag(flag);
			} else if (MyPreference_FieldKey.equals(MyPreference.KEY_OUTTIME_CHANGE_FLAG)) {
				pref.writeOutTimeChangeFlag(flag);
			} else if (MyPreference_FieldKey.equals(MyPreference.KEY_FASTSYUKKIN_CONFIRM_FLAG)) {
				pref.writeFastSyukkinConfirmFlag(flag);
			} else if (MyPreference_FieldKey.equals(MyPreference.KEY_FASTSYUKKIN_PASSWORD_FLAG)) {
				pref.writeFastSyukkinPasswordFlag(flag);
			} else if (MyPreference_FieldKey.equals(MyPreference.KEY_LATETAIKIN_CONFIRM_FLAG)) {
				pref.writeLateTaikinConfirmFlag(flag);
			} else if (MyPreference_FieldKey.equals(MyPreference.KEY_LATETAIKIN_PASSWORD_FLAG)) {
				pref.writeLateTaikinPasswordFlag(flag);
			}
		}

		private void updateTypeTime(String MyPreference_FieldKey, int time) {
			MyPreference pref = MyPreference.getInstance(MyExpandableListAdapter.this.context);
			if (MyPreference_FieldKey.equals(MyPreference.KEY_WORKINGHOUR_SYUKKIN_TIME)) {
				pref.writeWorkingHourSyukkin(time);
			} else if (MyPreference_FieldKey.equals(MyPreference.KEY_WORKINGHOUR_TAIKIN_TIME)) {
				pref.writeWorkingHourTaikin(time);
			} else if (MyPreference_FieldKey.equals(MyPreference.KEY_FASTSYUKKIN_CONFIRM_TIME)) {
				pref.writeFastSyukkinConfirmTime(time);
			} else if (MyPreference_FieldKey.equals(MyPreference.KEY_FASTSYUKKIN_PASSWORD_TIME)) {
				pref.writeFastSyukkinPasswordTime(time);
			} else if (MyPreference_FieldKey.equals(MyPreference.KEY_LATETAIKIN_CONFIRM_TIME)) {
				pref.writeLateTaikinConfirmTime(time);
			} else if (MyPreference_FieldKey.equals(MyPreference.KEY_LATETAIKIN_PASSWORD_TIME)) {
				pref.writeLateTaikinPasswordTime(time);
			} else if (MyPreference_FieldKey.equals(MyPreference.KEY_LATESYUKKIN_PERMISSION_MINUTE)) {
				pref.writeLateSyukkinPermissionMinute(time);
			}
		}

		@Override
		public Object getChild(int arg0, int arg1) {
			return this.children.get(arg0).get(arg1);
		}
		@Override
		public long getChildId(int arg0, int arg1) {
			return arg1;
		}
		@Override
		public int getChildrenCount(int arg0) {
			return this.children.get(arg0).size();
		}
		@Override
		public Object getGroup(int arg0) {
			return this.groups.get(arg0);
		}
		@Override
		public int getGroupCount() {
			return this.groups.size();
		}
		@Override
		public long getGroupId(int arg0) {
			return arg0;
		}
		@Override
		public boolean hasStableIds() {
			return true;
		}
		@Override
		public boolean isChildSelectable(int arg0, int arg1) {
			return true;
		}

	}



	/**
	 * 各項目に表示する内容を保持するクラス。
	 * 第四～第六引数はそれぞれ必須、任意、非表示の選択自体を可能とするかどうかを決めるフラグ。
	 * 例) 第五引数のvisibleがfalseであれば、表示・選択されるのは必須と非表示のみ。
	 * 第七引数は現在選択しているポジション。MyPreferenceで定義している値が入る。
	 */
	private class Item {
		public String title;
		public ITEM_TYPE itemType;
		public String MyPreference_FieldKey;
		public boolean hissu;
		public boolean visible;
		public boolean invisible;
		public int checkedPosition;
		public int time;
		public Item(String title, ITEM_TYPE type, String MyPreference_FieldKey) {
			this.title = title;
			this.itemType = type;
			this.MyPreference_FieldKey = MyPreference_FieldKey;
		}
		public Item setHissu(boolean hissu) {
			this.hissu = hissu;
			return this;
		}
		public Item setVisible(boolean visible) {
			this.visible = visible;
			return this;
		}
		public Item setInVisible(boolean invisible) {
			this.invisible = invisible;
			return this;
		}
		public Item setCheckedPosition(int checkedPosition) {
			this.checkedPosition = checkedPosition;
			return this;
		}
		public Item setTime(int time) {
			this.time = time;
			return this;
		}
	}

	private enum ITEM_TYPE {
		TEXTVIEW,
		FLAG,
		CHECKABLE,
		TIME,
		TIME_MINUTE,
		GYOUMU,
	}

}
