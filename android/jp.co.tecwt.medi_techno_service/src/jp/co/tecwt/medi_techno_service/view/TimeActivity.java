package jp.co.tecwt.medi_techno_service.view;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import jp.co.tecwt.medi_techno_service.R;
import jp.co.tecwt.medi_techno_service.data.DialogHolder;
import jp.co.tecwt.medi_techno_service.data.MyPreference;
import jp.co.tecwt.medi_techno_service.data.Singleton;
import jp.co.tecwt.medi_techno_service.dialog.MyDialog;
import jp.co.tecwt.medi_techno_service.dialog.TextDialog;
import jp.co.tecwt.medi_techno_service.util.DateTime;
import jp.co.tecwt.medi_techno_service.util.NextAct;
import jp.co.tecwt.medi_techno_service.util.Passward;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class TimeActivity extends BaseActivity {

	private TextView txt_time_hour;
	private TextView txt_time_minute;
	private TextView txt_time_colon;
	private static final int NEED_LENGTH = 2;
	private ArrayList<String> strs_hour = new ArrayList<String>(TimeActivity.NEED_LENGTH);
	private ArrayList<String> strs_minute = new ArrayList<String>(TimeActivity.NEED_LENGTH);
	private TimeZone timeZone;
	private Locale locale;
	private Calendar calendar;

	private Button next;
	private boolean checkON;
	private boolean isJump;
	private boolean isBack;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// レイアウトファイル適用
		setContentView(R.layout.activity_time);

		// コントロール取得
		this.txt_time_hour = (TextView)this.findViewById(R.id.textview_time_hour);
		this.txt_time_minute = (TextView)this.findViewById(R.id.textview_time_minute);
		this.txt_time_colon = (TextView)this.findViewById(R.id.textview_time_colon);

		// タイムゾーン・ロケール設定
		this.timeZone = TimeZone.getTimeZone("Asia/Tokyo");
		this.locale = Locale.JAPAN;

		// 縦横判定
		Resources res = this.getResources();
		this.onConfigurationChanged(res.getConfiguration());

		// リスナー登録
		((Button)this.findViewById(R.id.button_back)).setOnClickListener(this.backBtn);
		((Button)this.findViewById(R.id.button_top)).setOnClickListener(this.topBtn);
		this.next = (Button)this.findViewById(R.id.button_next);
		this.next.setOnClickListener(this.nextBtn);
		this.next.setEnabled(false);
		((Button)this.findViewById(R.id.button_0)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_1)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_2)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_3)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_4)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_5)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_6)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_7)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_8)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_9)).setOnClickListener(this.numberBtn);
		((Button)this.findViewById(R.id.button_numberback)).setOnClickListener(this.numberbackBtn);
		((Button)this.findViewById(R.id.button_plus)).setOnClickListener(this.plusBtn);
		((Button)this.findViewById(R.id.button_minus)).setOnClickListener(this.minusBtn);

		// 必須確認
		this.checkON = this.getIntent().getBooleanExtra(NextAct.ISHISSU, true);

		// 初入力なら現在の時間を記録しといてあげる
		if(Singleton.INSTANCE.getDatetimeEdit() == 0) {
			this.setDefaultTime();
		}

		// ルートに応じて表示内容を変更
		TextView textview = (TextView)this.findViewById(R.id.textview_time_title);
		switch(Singleton.INSTANCE.getSyuttaiStatus()) {
		case Main.SYUKKIN:
			textview.setText("開始時刻を入力");
			break;
		case Main.IDOU:
			textview.setText("移動開始時刻を入力");
			break;
		case Main.TAIKIN:
			textview.setText("終了時刻を入力");
			break;
		}
	}


	/**
	 * 次へ or 確定
	 */
	private OnClickListener nextBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			// 特別時間内処理
			TimeActivity.this.specialCode(Singleton.INSTANCE.getSyuttaiStatus());
		}
	};

	/**
	 * 番号各種
	 */
	private OnClickListener numberBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			String number = view.getTag().toString();
			int numInt = TimeActivity.this.numberToInt(number);

			// 時
			if(TimeActivity.this.strs_hour.size() < TimeActivity.NEED_LENGTH) {
				// 一桁目が3以上だったら先頭に0を追加
				if(TimeActivity.this.strs_hour.size() == 0 && numInt > 2) {
					TimeActivity.this.strs_hour.add("0");
					TimeActivity.this.strs_hour.add(number);
					TimeActivity.this.numberPrint();
					TimeActivity.this.buttonUpdate();
					return;
				}
				// 一桁目が2だったら4以上の入力は無視する
				if(TimeActivity.this.strs_hour.size() == 1 && TimeActivity.this.strs_hour.get(0).equals("2") && numInt > 3) {
					return;
				}
				// 正常
				TimeActivity.this.strs_hour.add(number);
				TimeActivity.this.numberPrint();
				TimeActivity.this.buttonUpdate();
				return;
			}
		}
	};

	/**
	 * Back
	 */
	private OnClickListener numberbackBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			/*
			if(TimeActivity.this.strs_hour.size() == 0) {
				return;
			}
			TimeActivity.this.strs_hour.remove(TimeActivity.this.strs_hour.size()-1);*/
			TimeActivity.this.strs_hour.clear();
			TimeActivity.this.numberPrint();
			TimeActivity.this.buttonUpdate();
		}
	};

	/**
	 * +15
	 */
	private OnClickListener plusBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			// 00:00 -> 00:15
			if(TimeActivity.this.numberMinuteBuild().equals("00")) {
				TimeActivity.this.strs_minute.clear();
				TimeActivity.this.strs_minute.add("1");
				TimeActivity.this.strs_minute.add("5");
			// 00:15 -> 00:30
			}else if(TimeActivity.this.numberMinuteBuild().equals("15")) {
				TimeActivity.this.strs_minute.clear();
				TimeActivity.this.strs_minute.add("3");
				TimeActivity.this.strs_minute.add("0");
			// 00:30 -> 00:45
			}else if(TimeActivity.this.numberMinuteBuild().equals("30")) {
				TimeActivity.this.strs_minute.clear();
				TimeActivity.this.strs_minute.add("4");
				TimeActivity.this.strs_minute.add("5");
			// 00:45 -> 01:00
			}else if(TimeActivity.this.numberMinuteBuild().equals("45")) {
				try {
					int hour = Integer.parseInt(TimeActivity.this.numberHourBuild());
					TimeActivity.this.strs_hour.clear();
					if(hour == 23) {
						TimeActivity.this.strs_hour.add("0");
						TimeActivity.this.strs_hour.add("0");
					}else {
						hour = hour + 1;
						if(hour < 10) {
							TimeActivity.this.strs_hour.add("0");
							TimeActivity.this.strs_hour.add(String.valueOf(hour));
						}else {
							char[] ns = String.valueOf(hour).toCharArray();
							for(char n : ns) {
								TimeActivity.this.strs_hour.add(String.valueOf(n));
							}
						}
					}
					TimeActivity.this.strs_minute.clear();
					TimeActivity.this.strs_minute.add("0");
					TimeActivity.this.strs_minute.add("0");
				} catch(NumberFormatException ex) {
					ex.printStackTrace();
				}
			}
			TimeActivity.this.numberPrint();
			TimeActivity.this.buttonUpdate();
		}
	};

	/**
	 * -15
	 */
	private OnClickListener minusBtn = new OnClickListener() {
		@Override
		public void onClick(View view) {
			// 00:00 -> 23:45
			if(TimeActivity.this.numberMinuteBuild().equals("00")) {
				try {
					int hour = Integer.parseInt(TimeActivity.this.numberHourBuild());
					TimeActivity.this.strs_hour.clear();
					if(hour == 0) {
						TimeActivity.this.strs_hour.add("2");
						TimeActivity.this.strs_hour.add("3");
					}else {
						hour = hour - 1;
						if(hour < 10) {
							TimeActivity.this.strs_hour.add("0");
							TimeActivity.this.strs_hour.add(String.valueOf(hour));
						}else {
							char[] ns = String.valueOf(hour).toCharArray();
							for(char n : ns) {
								TimeActivity.this.strs_hour.add(String.valueOf(n));
							}
						}
					}
					TimeActivity.this.strs_minute.clear();
					TimeActivity.this.strs_minute.add("4");
					TimeActivity.this.strs_minute.add("5");
				} catch(NumberFormatException ex) {
					ex.printStackTrace();
				}
			// 00:15 -> 00:00
			}else if(TimeActivity.this.numberMinuteBuild().equals("15")) {
				TimeActivity.this.strs_minute.clear();
				TimeActivity.this.strs_minute.add("0");
				TimeActivity.this.strs_minute.add("0");
			// 00:30-> 00:15
			}else if(TimeActivity.this.numberMinuteBuild().equals("30")) {
				TimeActivity.this.strs_minute.clear();
				TimeActivity.this.strs_minute.add("1");
				TimeActivity.this.strs_minute.add("5");
			// 00:45 -> 00:30
			}else if(TimeActivity.this.numberMinuteBuild().equals("45")) {
				TimeActivity.this.strs_minute.clear();
				TimeActivity.this.strs_minute.add("3");
				TimeActivity.this.strs_minute.add("0");
			}
			TimeActivity.this.numberPrint();
			TimeActivity.this.buttonUpdate();
		}
	};

	private void setDefaultTime() {
		this.calendar = Calendar.getInstance(timeZone, locale);
		int year = this.calendar.get(Calendar.YEAR);
		int month = this.calendar.get(Calendar.MONTH);
		int day = this.calendar.get(Calendar.DAY_OF_MONTH);
		int hour = this.calendar.get(Calendar.HOUR_OF_DAY);
		int minute = this.calendar.get(Calendar.MINUTE);

		// 15分刻みにする(出勤時は繰り上げ、移動・退勤時は繰り下げ)
		boolean isSyukkin = Singleton.INSTANCE.getSyuttaiStatus() == Main.SYUKKIN;
		try {
			long convertTime = 0;
			if(minute == 0 || minute == 15 || minute == 30 || minute == 45) {
				// そのまま
				convertTime = this.calendar.getTimeInMillis();
			} else if(0 < minute && minute < 15) {
				// 0~15
				if(isSyukkin) {
					// 15にする(出勤)
					this.calendar.set(year, month, day, hour, 15);
				} else {
					// 0にする(移動・退勤)
					this.calendar.set(year, month, day, hour, 0);
				}
			} else if(15 < minute && minute < 30) {
				// 15~30
				if(isSyukkin) {
					// 30にする(出勤)
					this.calendar.set(year, month, day, hour, 30);
				} else {
					// 15にする(移動・退勤)
					this.calendar.set(year, month, day, hour, 15);
				}
			} else if(30 < minute && minute < 45) {
				// 30~45
				if(isSyukkin) {
					// 45にする(出勤)
					this.calendar.set(year, month, day, hour, 45);
				} else {
					// 30にする(移動・退勤)
					this.calendar.set(year, month, day, hour, 30);
				}
			} else if(45 < minute) {
				// 45~00
				if(isSyukkin) {
					// 00にする(出勤)
					int h = hour;
					int m = 0;
					if(h == 23) {
						h = 0;
					} else {
						h = hour + 1;
					}
					this.calendar.set(year, month, day, h, m);
				} else {
					// 45にする(移動・退勤)
					this.calendar.set(year, month, day, hour, 45);
				}
			}
			convertTime = this.calendar.getTimeInMillis();
			Singleton.INSTANCE.setDatetimeEdit(convertTime);
		} catch(NumberFormatException ex) {
			ex.printStackTrace();
		}

		// 出勤の場合で...
		if(Singleton.INSTANCE.getSyuttaiStatus() == Main.SYUKKIN) {

			this.calendar.setTimeInMillis(System.currentTimeMillis());
			int nowHour = this.calendar.get(Calendar.HOUR_OF_DAY);
			int nowMinute = this.calendar.get(Calendar.MINUTE);
			int nowTime = nowHour * 100 + nowMinute;
			MyPreference pref = new MyPreference(this);
			int workingSyukkin = pref.getWorkingHourSyukkin();
			int workingH = workingSyukkin / 100;
			int workingM = workingSyukkin - workingH * 100;

			//出勤時刻が規定出勤時間未満の場合は規定の時間に合わせる
			if (nowTime < workingSyukkin) {
				this.calendar.set(year, month, day, workingH, workingM);
				Singleton.INSTANCE.setDatetimeEdit(this.calendar.getTimeInMillis());
			}
			//出勤時刻が規定出勤時間+指定許容時間内であれば規定の時間にしてあげる
			else if (nowTime <= workingSyukkin + pref.getLateSyukkinPermissionMinute()) {
				this.calendar.set(year, month, day, workingH, workingM);
				Singleton.INSTANCE.setDatetimeEdit(this.calendar.getTimeInMillis());
			}
		}
		// 描画更新
		this.loadTime(Singleton.INSTANCE.getDatetimeEdit());
		this.numberPrint();
		this.buttonUpdate();
	}

	private void numberPrint() {
		this.txt_time_hour.setText(this.numberHourBuild());
		this.txt_time_minute.setText(this.numberMinuteBuild());
	}

	private String numberHourBuild() {
		StringBuilder builder = new StringBuilder();
		for(String str : this.strs_hour) {
			builder.append(str);
		}
		return builder.toString();
	}

	private String numberMinuteBuild() {
		StringBuilder builder = new StringBuilder();
		for(String str : this.strs_minute) {
			builder.append(str);
		}
		return builder.toString();
	}

	private int numberToInt(String intgerStringOnly) {
		int result = -1;
		try {
			result = Integer.parseInt(intgerStringOnly);
		} catch(NumberFormatException ex) {
			ex.printStackTrace();
		}
		return result;
	}

	private void buttonUpdate() {
		if(checkON) {
			if(this.strs_hour.size() == TimeActivity.NEED_LENGTH && this.strs_minute.size() == TimeActivity.NEED_LENGTH) {
				this.next.setEnabled(true);
			}else {
				this.next.setEnabled(false);
			}
		}else {
			this.next.setEnabled(true);
		}
	}

	private void loadTime(long timeMillis) {
		this.strs_hour.clear();
		this.strs_minute.clear();
		String[] hhmm = getForwardZeroHourAndMinute(timeMillis);
		String hour = hhmm[0];
		String minute = hhmm[1];
		this.txt_time_hour.setText(hour);
		this.txt_time_minute.setText(minute);
		// 文字列を分解して「時」に入れる
		for(int i=0; i<hour.length(); i++) {
			this.strs_hour.add(String.valueOf(hour.charAt(i)));
		}
		// 文字列を分解して「分」に入れる
		for(int i=0; i<minute.length(); i++) {
			this.strs_minute.add(String.valueOf(minute.charAt(i)));
		}
	}

	private String[] getForwardZeroHourAndMinute(long timeMillis) {
		this.calendar = Calendar.getInstance(timeZone, locale);
		this.calendar.setTimeInMillis(timeMillis);
		String hour = String.valueOf( this.calendar.get(Calendar.HOUR_OF_DAY));
		if(hour.length() == 1) hour = "0" + hour;
		String minute = String.valueOf( this.calendar.get(Calendar.MINUTE));
		if(minute.length() == 1) minute = "0" + minute;
		return new String[] { hour, minute };
	}

	private long saveTime() {
		// UNIXタイムに変換
		/*
		 * 入力時間は秒以下をキリのよい数字に強制変換する。
		 * 例） 2015/3/24 00:00:01 -> 2015/3/24 00:00:00
		 */
		int year = this.calendar.get(Calendar.YEAR);
		int month = this.calendar.get(Calendar.MONTH);
		int day = this.calendar.get(Calendar.DAY_OF_MONTH);
		this.calendar.set(year, month, day, this.numberToInt(this.numberHourBuild()), this.numberToInt(this.numberMinuteBuild()));
		/*
		 *  使用するメソッドはミリ秒まで必要なので、ミリ秒部分を一度切り捨てた後に再度0を追加して値を揃え、ソートできるようにする。
		 *  例） 1427155200[485] -> 1427155200[] -> 1427155200[000]
		 *  [2015/09/15]廃止
		 */
		long time = this.calendar.getTimeInMillis();
		Singleton.INSTANCE.setDatetimeEdit(time);
		return time;
	}

	/**
	 * ある特定の時間に対して行われる特別な処理を定義します。<br>
	 * 特別な処理の後に関しては、TimeActivity#nextAction()を確認してください。
	 */
	private void specialCode(int syuttai) {

		// 入力されている「時間」
		String strH = this.numberHourBuild();
		String strM = this.numberMinuteBuild();
		int time = this.numberToInt(strH + strM);

		// 実際の時間
		Calendar calendar = Calendar.getInstance();
		int realTime = calendar.get(Calendar.HOUR_OF_DAY) * 100 + calendar.get(Calendar.MINUTE);

		// ダイアログ名
		final String DIALOG_NAME = this.getClass().getName();

		// アプリ設定
		MyPreference pref = new MyPreference(this);

		// 各種チェック
		switch(syuttai) {

		case Main.SYUKKIN:
			//現実時間が打刻時間を超過している場合
			if (realTime > time) {
				//過去出勤が許可されているなら問題なし
				if (pref.getInTimeChangeFlag()) {
					this.nextAction();
					break;
				}
				else {
					//許容時間内なら問題なし
					if (realTime <= pref.getWorkingHourSyukkin() + pref.getLateSyukkinPermissionMinute()) {
						this.nextAction();
						break;
					}
					//それ以外は許可しない
					else {
						String[] hhmm = getForwardZeroHourAndMinute(Singleton.INSTANCE.getDatetimeEdit());
						this.showDialogOKTimeReset(DIALOG_NAME, "出勤時間が不正です",
								"指定された時間では打刻できません。\n" +
								"現在、打刻可能な時間は【" + hhmm[0] + ":" + hhmm[1] + "】以降です。", "OK");
						return;
					}
				}
			}
			//打刻時間が早出パスワード入力要求時刻よりも早い場合はパスワードを要求する
			else if (pref.getFastSyukkinPasswordFlag() && time <= pref.getFastSyukkinPasswordTime()) {
				String yyyyMMdd = DateTime.GetYYYYMMDD(DateTime.GetToday());
				this.showDialogInputPassword(DIALOG_NAME, "早出承認パスワードを入力して下さい",
						"確認", "キャンセル", Passward.GetTimecardPassward(yyyyMMdd));
				return;
			}
			//打刻時間が早出承認確認時刻よりも早い場合は確認を行う
			else if (pref.getFastSyukkinConfirmFlag() && time <= pref.getFastSyukkinConfirmTime()) {
				this.showDialogPositiveNextOrNegativeCancel(DIALOG_NAME, "早出承認の確認",
						"出勤時刻が規定されている時間よりも早くなっています。\n早出の申請は行なっていますか？", "はい", "いいえ");
				return;
			}
			else {
				//何にも引っかからなかったので正常処理
				this.nextAction();
				break;
			}

		case Main.IDOU:
			//何にも引っかからなかったので正常処理
			this.nextAction();
			break;

		case Main.TAIKIN:
			//現実時間よりも遅い時間で入力されている場合は許可しない
			if (!pref.getOutTimeChangeFlag() && realTime < time) {
				String[] hhmm = getForwardZeroHourAndMinute(Singleton.INSTANCE.getDatetimeEdit());
				this.showDialogOKTimeReset(DIALOG_NAME, "退勤時間が不正です",
						"指定された時間では打刻できません。\n" +
						"現在、打刻可能な時間は【" + hhmm[0] + ":" + hhmm[1] + "】以前です。", "OK");
				return;
			}
			//打刻時間が残業パスワード入力要求時刻よりも遅い場合はパスワードを要求する
			else if (pref.getLateTaikinPasswordFlag() && time >= pref.getLateTaikinPasswordTime()) {
				String yyyyMMdd = DateTime.GetYYYYMMDD(DateTime.GetToday());
				this.showDialogInputPassword(DIALOG_NAME, "残業承認パスワードを入力して下さい",
						"確認", "キャンセル", Passward.GetTimecardPassward(yyyyMMdd));
				return;
			}
			//打刻時間が残業承認確認時刻よりも遅い場合は確認を行う
			else if (pref.getLateTaikinConfirmFlag() && time >= pref.getLateTaikinConfirmTime()) {
				this.showDialogPositiveNextOrNegativeCancel(DIALOG_NAME, "残業承認の確認",
						"退勤時刻が規定されている時間を超えています。\n時間外勤務の申請は行なっていますか？", "はい", "いいえ");
				return;
			}
			else {
				// 何にも引っかからなかったので正常処理
				this.nextAction();
				break;
			}
		}
	}

	/**
	 * 確定後、現在のふさわしい時間に変更します。
	 * @param DIALOG_NAME
	 * @param title
	 * @param message
	 * @param positive
	 */
	private void showDialogOKTimeReset(
			String DIALOG_NAME, String title, String message, String positive) {
		// 多重起動阻止
		if(this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
			return;
		}
		// ダイアログ作成
		final MyDialog mydialog = new MyDialog();
		mydialog.setMode(MyDialog.MODE_NORMAL);
		mydialog.setTitle(title);
		mydialog.setMessage(message);
		if(positive != null && !positive.equals("")) {
			mydialog.setPositive_button(positive, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					TimeActivity.this.setDefaultTime();
				}
			});
		}
		mydialog.setCancelable(false);
		mydialog.show(this.getFragmentManager(), DIALOG_NAME);
	}

	/**
	 * ダイアログを表示し、positiveだった場合はTimeActivity#nextAction()をコールします。<br>
	 * negativeだった場合はTimeActivity#cancelAction()をコールします。<br>
	 * positiveとnegativeは、nullもしくは空文字列だった場合はボタンを作成しません。
	 * @param DIALOG_NAME
	 * @param title
	 * @param message
	 */
	private void showDialogPositiveNextOrNegativeCancel(
			String DIALOG_NAME, String title, String message, String positive, String negative) {
		// 多重起動阻止
		if(this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
			return;
		}
		// ダイアログ作成
		final MyDialog mydialog = new MyDialog();
		mydialog.setMode(MyDialog.MODE_NORMAL);
		mydialog.setTitle(title);
		mydialog.setMessage(message);
		if(positive != null && !positive.equals("")) {
			mydialog.setPositive_button(positive, new DialogInterface.OnClickListener() {
				// 正常に次の画面へ移行する
				@Override
				public void onClick(DialogInterface dialog, int which) {
					DialogHolder.INSTANCE.init();
					TimeActivity.this.nextAction();
				}
			});
		}
		if(negative != null && !negative.equals("")) {
			mydialog.setNegative_button(negative, new DialogInterface.OnClickListener() {
				// 申請を行うよう促し、処理はすべてキャンセルする
				@Override
				public void onClick(DialogInterface dialog, int which) {
					DialogHolder.INSTANCE.init();
					TimeActivity.this.cancelAction();
				}
			});
		}
		mydialog.setCancelable(false);
		mydialog.show(this.getFragmentManager(), DIALOG_NAME);
	}

	/**
	 * パスワードを求めるダイアログを表示します。<br>
	 * 合致した場合はnextAction()をコールします。キャンセルされた場合はcancelAction()をコールします。<br>
	 * 引数は全て必須です。
	 * @param DIALOG_NAME
	 * @param title
	 * @param positive
	 * @param negative
	 * @param passward
	 */
	private void showDialogInputPassword(
			String DIALOG_NAME, String title, String positive, String negative, final String passward) {
		//
		if (passward == null) return;

		// 多重起動阻止
		if(this.getFragmentManager().findFragmentByTag(DIALOG_NAME) != null) {
			return;
		}
		// ダイアログ作成
		final TextDialog textDialog = new TextDialog();
		textDialog.setTitle(title);
		textDialog.setInputMode(TextDialog.INPUT_MODE.NUMBER_PASSWORD);
		textDialog.setPositiveText(positive);
		textDialog.setNegativeText(negative);
		textDialog.setCallback(new TextDialog.Callback() {

			@Override
			public void onResult(String result) {
				DialogHolder.INSTANCE.init();
				if (passward.equals(result)) {
					TimeActivity.this.nextAction();
				} else {
					MyDialog mydialog = new MyDialog();
					mydialog.setMode(MyDialog.MODE_NORMAL);
					mydialog.setTitle("パスワードが違います");
					mydialog.setMessage("責任者に残業申請は行っていますか。\nもう一度パスワードを確認して下さい。");
					mydialog.setPositive_button("OK", new DialogInterface.OnClickListener() {
						// 正常に次の画面へ移行する
						@Override
						public void onClick(DialogInterface dialog, int which) {
							DialogHolder.INSTANCE.init();
						}
					});
					mydialog.show(TimeActivity.this.getFragmentManager(), null);
				}
			}

			@Override
			public void onCancel() {
				DialogHolder.INSTANCE.init();
				TimeActivity.this.cancelAction();
			}
		});
		textDialog.setCancelable(false);
		textDialog.show(this.getFragmentManager(), DIALOG_NAME);
	}

	/**
	 * 申請を行うよう促し、処理はすべてキャンセルする。
	 */
	private void cancelAction() {
		MyDialog noDialog = new MyDialog();
		noDialog.setMode(MyDialog.MODE_NORMAL);
		noDialog.setTitle("申請を行って下さい");
		noDialog.setMessage("申請が行われていない場合は打刻を完了することができません。必要な手続きを経た上で再度操作を行って下さい。");
		noDialog.setPositive_button("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// Topへ
				DialogHolder.INSTANCE.init();
				Intent topIntent = new Intent(TimeActivity.this, Main.class);
				topIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
				TimeActivity.this.startActivity(topIntent);
			}
		});
		noDialog.setCancelable(false);
		noDialog.show(TimeActivity.this.getFragmentManager(), String.valueOf(System.currentTimeMillis()));	// 画面回転を許可して回転後にここを通ると落ちるので注意
	}

	/**
	 * 次に行うアクション
	 */
	private void nextAction() {
		// 時間記録
		TimeActivity.this.saveTime();

		// ジャンプならアクティビティを閉じる
		if(this.isJump) {
			this.finish();
			return;
		}

		// 次の画面へ
		Intent nextIntent = NextAct.getNextIntent(TimeActivity.this, Singleton.INSTANCE.getSyuttaiStatus());
		TimeActivity.this.startActivityForResult(nextIntent, 0);
	}

	@Override
	public void onResume() {
		super.onResume();

		// 復帰
		this.loadTime(Singleton.INSTANCE.getDatetimeEdit());
		this.numberPrint();
		this.buttonUpdate();

		// 確認画面からのジャンプかを判定
		this.isJump = this.getIntent().getBooleanExtra("isJump", false);

		// ジャンプしてきた場合は[次へ]の文字を「確定」に変える
		if(this.isJump) {
			this.next.setText("確定");
		}

		// 出勤だけは時間画面はパスする
		if(!this.isJump && !this.isBack && Singleton.INSTANCE.getSyuttaiStatus() == Main.SYUKKIN) {
			TimeActivity.this.specialCode(Singleton.INSTANCE.getSyuttaiStatus());
			return;
		}

		// 縦横判定
		this.fontSizeChange(this.getResources().getConfiguration());
	}

	@Override
	public void onPause() {
		super.onPause();
		// 保存
		this.saveTime();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		this.fontSizeChange(newConfig);	// 縦横でフォントサイズ変更
	}

	/**
	 * 縦横でフォントサイズを変更する
	 * @param config
	 */
	private void fontSizeChange(Configuration config) {
		// スマホでなければ以下を実行
		if(!MyPreference.getInstance(this).isPhone()) {
			int size = 0;
			switch(config.orientation) {
			case Configuration.ORIENTATION_PORTRAIT:
				size = 100;
				break;
			case Configuration.ORIENTATION_LANDSCAPE:
				size = 70;
				break;
			}
			this.txt_time_hour.setTextSize(size);
			this.txt_time_minute.setTextSize(size);
			this.txt_time_colon.setTextSize(size - 10);
		}
	}

	@Override
	public void onActivityResult(int req, int res, Intent d) {
		// ページ数をカウントダウン
		NextAct.countDown(this, Singleton.INSTANCE.getSyuttaiStatus());
		if(res == Activity.RESULT_CANCELED) {
			this.isBack = true;	//　Backキーもしくは戻るで帰ってきた場合
		} else {
			this.isBack = false;
		}
	}

}
