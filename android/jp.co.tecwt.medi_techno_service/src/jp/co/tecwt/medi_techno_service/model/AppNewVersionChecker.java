package jp.co.tecwt.medi_techno_service.model;

import java.io.File;

import jp.co.tecwt.medi_techno_service.R;
import jp.co.tecwt.medi_techno_service.util.MyHttpClient2;
import jp.co.tecwt.medi_techno_service.util.MyHttpClient3_Download;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;

public class AppNewVersionChecker {

	private int currentVersionCode;
	private int versionCode;
	private boolean editable;
	private String downloadUrl;
	private Context con;
	private Resources res;

	public AppNewVersionChecker(Context con) {
		this.con = con;
		this.res = con.getResources();
		this.editable = true;
		this.currentVersionCode = this.getCurrentVersionCode(con);
	}

	private int getCurrentVersionCode(Context con){
		try {
			PackageManager pm = con.getPackageManager();
			PackageInfo pi = pm.getPackageInfo(con.getPackageName(), PackageManager.GET_META_DATA);
			return pi.versionCode;
		} catch(NameNotFoundException ex) {
			ex.printStackTrace();
		}
		return -1;
	}

	public void connect(final MyHttpClient2.Responser responser) {
		String url = res.getString(R.string.update_url);
		int timeout_connect = res.getInteger(R.integer.timeout_connect);
		int timeout_read = res.getInteger(R.integer.timeout_read);
		MyHttpClient2.Responser myResponser = new MyHttpClient2.Responser() {

			@Override
			public void onFailed() {
				responser.onFailed();
			}

			@Override
			public void onError(String errorcode, String message) {
				responser.onError(errorcode, message);
			}

			@Override
			public void onCompleted(String json) {
				// 接続成功
				try {
					// root
					JSONObject rootObj = new JSONObject(json);

					// versionCode抽出
					try {
						AppNewVersionChecker.this.versionCode = rootObj.getInt("versionCode");
					} catch(JSONException ex){
						AppNewVersionChecker.this.versionCode = -1;
					}

					// editable抽出
					try {
						AppNewVersionChecker.this.editable = rootObj.getBoolean("editable");
					} catch(JSONException ex){
						AppNewVersionChecker.this.editable = true;
					}

					// downloadUrl抽出
					try {
						AppNewVersionChecker.this.downloadUrl = rootObj.getString("url");
					} catch(JSONException ex){
						AppNewVersionChecker.this.downloadUrl = null;
					}

				} catch(JSONException ex) {
					ex.printStackTrace();
				}
				responser.onCompleted(json);
			}
		};
		// サーバ問い合わせ
		MyHttpClient2 client = new MyHttpClient2(con, url, myResponser);
		client.enabledProgressDialog(con, "通信中", "アプリの最新バージョンを確認しています", null);
		client.setTimeOut(timeout_connect, timeout_read);
		client.addPostParam(
				res.getString(R.string.update_param_name1),
				res.getString(R.string.update_param_value1));
		client.execute();
	}

	/**
	 * 新しいバージョンが存在するかを確認します。
	 * @return
	 */
	public boolean isExistNewVersion() {
		if(this.currentVersionCode == -1) return false;
		if(this.versionCode == -1) return false;
		if(this.currentVersionCode < this.versionCode) return true;
		return false;
	}

	/**
	 * 未更新状態でも打刻可能とするかを確認します。
	 * @return
	 */
	public boolean isEditable() {
		return this.editable;
	}

	/**
	 * ダウンロードを行います。
	 * @param responser
	 * @param progressMessage
	 */
	public void download(final MyHttpClient3_Download.Responser responser, String progressMessage) {
		if(this.downloadUrl == null) {
			responser.onFailed();
			return;
		}
		// サーバ問い合わせ
		try {

			// タイムアウト
			int timeout_connect = res.getInteger(R.integer.timeout_connect);
			int timeout_read = res.getInteger(R.integer.timeout_read);

			// 問い合わせ結果コールバック
			MyHttpClient3_Download.Responser myResponser = new MyHttpClient3_Download.Responser() {

				@Override
				public void onFailed() {
					// 失敗
					responser.onFailed();
				}

				@Override
				public void onError(String errorcode, String message) {
					// 人為的失敗
					responser.onError(errorcode, message);
				}

				@Override
				public void onCompleted(File tmpApkFile) {
					// 成功
					responser.onCompleted(tmpApkFile);
				}
			};

			// 通信設定
			MyHttpClient3_Download client = new MyHttpClient3_Download(this.con, this.downloadUrl, myResponser);
			client.setTimeOut(timeout_connect, timeout_read);
			client.enabledProgressDialog(this.con, null, progressMessage, null);
			client.setProgressDialogStyle(ProgressDialog.STYLE_HORIZONTAL);
			client.execute();

		} catch(Exception ex) {
			ex.printStackTrace();
			return;
		}
	}
}
