package jp.co.tecwt.medi_techno_service.dialog;

import java.util.ArrayList;

import jp.co.tecwt.medi_techno_service.data.DialogHolder;
import jp.co.tecwt.medi_techno_service.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class AuthDialog extends DialogFragment {
	
	private static final int VALUE_ANSWER = 0;
	private static final int VALUE_KEY = 1;
	
	private EditText passEdit;
	private Callback callback;
	private String key;
	private boolean isCompleted;
	

	/**
	 * 変更確定コールバック。
	 */
	public interface Callback {
		public void callback();
	}
	

	/**
	 * コールバックを登録する。
	 * @param callback
	 * @return
	 */
	public AuthDialog setNextClassCallBack(Callback callback) {
		DialogHolder.INSTANCE.setCallback(callback);
		return this;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		Object[] values = DialogHolder.INSTANCE.getValues();
		String answer = null;
		if(values == null) {
			// 新規
			this.key = createKey();
		}else {
			// 復帰
			answer = (String)values[VALUE_ANSWER];
			this.key = (String)values[VALUE_KEY];
		}
		this.callback = (Callback)DialogHolder.INSTANCE.getCallback();

		this.setCancelable(false);
		return this.createAuthDialog(answer, this.key);
	}
	
	private Dialog createAuthDialog(String answer, String key) {
		View view = LayoutInflater.from(this.getActivity()).inflate(R.layout.dialog_authenticate, null);
		((TextView)view.findViewById(R.id.textview_authenticate_key)).setText(key);
		this.passEdit = (EditText)view.findViewById(R.id.edittext_authenticate_pass);
		this.passEdit.setText(answer);
		Dialog dialog = new AlertDialog.Builder(this.getActivity())
			.setTitle("認証確認")
			.setView(view)
			.setPositiveButton("確定", new AuthListener(this.getActivity(), key, this.passEdit, this.callback))
			.setNegativeButton("キャンセル", null)
			.create();
		InputMethodManager inputMethodManager = (InputMethodManager)this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(this.passEdit.getWindowToken(), 0);
		return dialog;
	}

	/**
	 * 【重要】 キーを作成する。
	 * @return
	 */
	private static String createKey() {
		int KEY_LENGTH = 6;
		
		// 1. UNIX * 1914
		long time = System.currentTimeMillis();
		time = time * 1914;
		
		// 2. 1の結果からランダムに6個の数字を選んで6桁の数字を作成
		String timeStr = String.valueOf(time);
		ArrayList<String> numbers = new ArrayList<String>();
		for(int i=0; i<timeStr.length(); i++) {
			numbers.add(String.valueOf(timeStr.charAt(i)));
		}
		StringBuilder randomNumbers = new StringBuilder();
		for(int i=0; i<KEY_LENGTH; i++) {
			int index = (int)(Math.random()*numbers.size());
			String number = numbers.remove(index);
			if(i == 0 && number.equals("0")) {
				// 先頭が0の場合は1にする
				number = "1";
			}
			randomNumbers.append(number);
		}
		
		// 16進数化
		int number = Integer.valueOf(randomNumbers.toString());
		String hex = Integer.toHexString(number);
		
		return hex;
	}
	
	
	/**
	 * 【重要】 キーに対応したパスワードを作成する。
	 * @param key
	 * @return
	 */
	private static String createPass(String key) {
		String hex = null;
		try {
			
			// 1. 10進数化する
			long hex10number = Long.parseLong(key, 16);
			
			// 2. 値の左から順に、+1,+2,+3,+4,+5,+6する
			String hex10numberStr = String.valueOf(hex10number);
			int[] numbers = new int[hex10numberStr.length()];
			for(int i=0; i<numbers.length; i++) {
				numbers[i] = Integer.valueOf(String.valueOf(hex10numberStr.charAt(i)));
				numbers[i] = numbers[i] + (i + 1);
				if(numbers[i] >= 10) {
					// 加算して2桁以上になる場合は末尾の数字のみを使用する
					String number = String.valueOf(numbers[i]);
					String lastNumber = String.valueOf(number.charAt(number.length()-1));
					numbers[i] = Integer.valueOf(lastNumber);
				}
			}
			
			// 3. 逆に並び替える
			StringBuilder builder = new StringBuilder();
			for(int i=numbers.length-1; i>=0; i--) {
				if(i == numbers.length - 1 && numbers[i] == 0) {
					numbers[i] = 1;
				}
				builder.append(numbers[i]);
			}
			
			// 16進数化
			int beforePass = Integer.valueOf(builder.toString());
			hex = Integer.toHexString(beforePass);
			
		} catch(NumberFormatException ex) {
			ex.printStackTrace();
		}
		
		return hex;
	}
	

	/**
	 * 認証確認Viewのウィジェットリスナー。
	 */
	public class AuthListener implements DialogInterface.OnClickListener {
		
		private Activity act;
		private String key;
		private EditText passEdit;
		private Callback callback;
		
		
		public AuthListener(Activity act, String key, EditText passEdit, Callback callback) {
			this.act = act;
			this.key = key;
			this.passEdit = passEdit;
			this.callback = callback;
		}
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			String answer = this.passEdit.getText().toString();
			
			// 合致
			if(answer.equals(createPass(key))) {
				
				if(this.callback == null) {
					// 何もしない
					return;
				}

				// コールバックする
				AuthDialog.this.isCompleted = true;
				this.callback.callback();
			}
			
			// ハズレ
			else {
				new MyDialog()
					.setMessage("パスワードが違います。")
					.setPositive_button("OK", null)
				.show(this.act.getFragmentManager(), null);
			}
		}
	}
	
	
	@Override
	public void onPause() {
		super.onPause();
		
		// 変更完了
		if(isCompleted) {
			// 値は保存しない
			return;
		}
		
		// 値を保存する
		String[] values = new String[2];
		values[VALUE_ANSWER] = this.passEdit.getText().toString();
		values[VALUE_KEY] = this.key;
		DialogHolder.INSTANCE.setValues(values);
		DialogHolder.INSTANCE.setCallback(this.callback);

	}

}
