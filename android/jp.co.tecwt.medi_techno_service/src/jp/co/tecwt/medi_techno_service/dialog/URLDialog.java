package jp.co.tecwt.medi_techno_service.dialog;

import jp.co.tecwt.medi_techno_service.data.DialogHolder;
import jp.co.tecwt.medi_techno_service.data.MyPreference;
import jp.co.tecwt.medi_techno_service.R;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

@SuppressLint("InflateParams")
public class URLDialog extends DialogFragment {
	
	public static final int VALUE_PROTOCOL = 0;
	public static final int VALUE_URL = 1;
	
	private URLListener listener;
	private Callback callback;
	private View view;
	
	
	/**
	 * 変更確定コールバック。
	 */
	public interface Callback {
		public void callback(String result);
	}
	
	
	/**
	 * 変更確定コールバックの登録。
	 * @param callback
	 * @return
	 */
	public URLDialog setCallback(Callback callback) {
		DialogHolder.INSTANCE.setCallback(callback);
		return this;
	}
	
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		this.view = LayoutInflater.from(this.getActivity()).inflate(R.layout.dialog_url_change, null);
		
		int protocol = -1;
		String url = null;
		
		Object[] values = DialogHolder.INSTANCE.getValues();
		if(values == null) {
			// 新規
			this.listener = new URLListener(view);
		}else {
			// 復帰
			this.listener = (URLListener)DialogHolder.INSTANCE.getPositibeListener();
			protocol = Integer.valueOf(values[VALUE_PROTOCOL].toString());
			url = values[VALUE_URL].toString();
		}
		this.callback = (Callback)DialogHolder.INSTANCE.getCallback();
		
		RadioGroup group = (RadioGroup)view.findViewById(R.id.radioGroup_protocol);
		((RadioButton)group.findViewById(R.id.radio_http)).setOnClickListener(this.listener);
		((RadioButton)group.findViewById(R.id.radio_https)).setOnClickListener(this.listener);
		group.check(protocol);
		EditText edit = (EditText)view.findViewById(R.id.edittext_url);
		edit.setText(url);
		edit.setOnKeyListener(this.urlKeyListener);
		
		AlertDialog.Builder dialog = new AlertDialog.Builder(this.getActivity());
		dialog.setTitle("URLの変更");
		dialog.setView(view);
		dialog.setPositiveButton("OK", this.listener);
		dialog.setNegativeButton("キャンセル", null);
		return dialog.create();
	}
	
	
	/**
	 * URL変更Viewのウィジェットリスナー。
	 */
	private class URLListener implements View.OnClickListener, DialogInterface.OnClickListener {
		
		private View view;
		private String protocol;
		
		public URLListener(View view) {
			this.view = view;
		}
		
		@Override
		public void onClick(View view) {
			this.protocol = ((RadioButton)view).getText().toString();
		}
		
		@Override
		public void onClick(DialogInterface dialog, int witch) {
			String domain = ((EditText)this.view.findViewById(R.id.edittext_url)).getText().toString();
			
			if(protocol == null || domain.equals("")) {
				// 無記入ならキャンセルと同様
				return;
			}
			
			// URL成型
			String url = this.protocol + domain;
			String lastStr = String.valueOf(url.charAt(url.length()-1));
			if(!lastStr.equals("/")) {
				// 末尾に/を追加
				url = url + "/";
			}
			
			// デフォルトのURLは以後使用しない
			MyPreference pref = MyPreference.getInstance(URLDialog.this.getActivity());
			pref.writeURL(url);
			pref.writeDoNotUseDefaultURL(MyPreference.DEFAULT_URL_DO_NOT_USE);
			
			// コールバックする
			if(URLDialog.this.callback != null) {
				URLDialog.this.callback.callback(url);
			}
		}
	}
	

	/**
	 * 入力監視。0
	 */
	private OnKeyListener urlKeyListener = new OnKeyListener() {
		@Override
		public boolean onKey(View view, int keyCode, KeyEvent event) {
			if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
				// 改行させない
				InputMethodManager imm = (InputMethodManager)URLDialog.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				return true;
			}
			return false;
		}
	};
	
	
	@Override
	public void onPause() {
		super.onPause();
		
		// 値の保存
		Object[] values = new Object[2];
		values[VALUE_PROTOCOL] = ((RadioGroup)this.view.findViewById(R.id.radioGroup_protocol)).getCheckedRadioButtonId();
		values[VALUE_URL] = ((EditText)this.view.findViewById(R.id.edittext_url)).getText().toString();
		DialogHolder.INSTANCE.setValues(values);
		DialogHolder.INSTANCE.setCallback(this.callback);
		DialogHolder.INSTANCE.setPositiveListener(this.listener);
	}

}
