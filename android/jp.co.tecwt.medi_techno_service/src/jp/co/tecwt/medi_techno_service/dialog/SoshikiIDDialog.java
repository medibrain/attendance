package jp.co.tecwt.medi_techno_service.dialog;

import jp.co.tecwt.medi_techno_service.data.DialogHolder;
import jp.co.tecwt.medi_techno_service.data.MyPreference;
import jp.co.tecwt.medi_techno_service.R;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * 画面が回転してもダイアログは保持されます。
 * show()を実行することでonCreateDialog()が実行され、ダイアログが表示されます。
 */
@SuppressLint("InflateParams")
public class SoshikiIDDialog extends DialogFragment {
	
	private static final int VALUE_SOSHIKI_ID = 0;
	
	private SoshikiIDListener listener;
	private Callback callback;
	private View view;
	
	
	/**
	 * 変更確定コールバック。
	 */
	public interface Callback {
		public void callback(String result);
	}
	
	
	/**
	 * 変更確定のコールバックを登録する。
	 * @param callback
	 * @return
	 */
	public SoshikiIDDialog setCallback(Callback callback) {
		DialogHolder.INSTANCE.setCallback(callback);
		return this;
	}
	
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		this.view = LayoutInflater.from(this.getActivity()).inflate(R.layout.dialog_soshikiid_change, null);
		
		String soshikiid = null;
		
		Object[] values = DialogHolder.INSTANCE.getValues();
		if(values == null) {
			// 新規
			this.listener = new SoshikiIDListener(view);
		}else {
			// 復帰
			this.listener = (SoshikiIDListener)DialogHolder.INSTANCE.getPositibeListener();
			soshikiid = values[VALUE_SOSHIKI_ID].toString();
		}
		this.callback = (Callback)DialogHolder.INSTANCE.getCallback();
		
		EditText edit = (EditText)this.view.findViewById(R.id.edittext_soshikiid);
		edit.setText(soshikiid);
		edit.setOnKeyListener(this.soshikiIDKeyListener);
		
		AlertDialog.Builder dialog = new AlertDialog.Builder(this.getActivity());
		dialog.setTitle("組織IDの変更");
		dialog.setView(view);
		dialog.setPositiveButton("OK", this.listener);
		dialog.setNegativeButton("キャンセル", null);
		return dialog.create();
	}
	
	
	/**
	 * 組織ID変更Viewのウィジェットリスナー。
	 */
	private class SoshikiIDListener implements DialogInterface.OnClickListener {
		
		private View view;
		
		public SoshikiIDListener(View view) {
			this.view = view;
		}
		
		@Override
		public void onClick(DialogInterface dialog, int witch) {
			String soshikiID = ((EditText)this.view.findViewById(R.id.edittext_soshikiid)).getText().toString();
			
			if(soshikiID.equals("")) {
				// 無記入ならキャンセルと同様
				return;
			}
			
			// 組織ID保存
			MyPreference pref = MyPreference.getInstance(SoshikiIDDialog.this.getActivity());
			pref.writeSoshikiID(soshikiID);
			
			// コールバックする
			if(SoshikiIDDialog.this.callback != null) {
				SoshikiIDDialog.this.callback.callback(soshikiID);
			}
		}
	}
	
	
	/**
	 * 入力監視。
	 */
	private OnKeyListener soshikiIDKeyListener = new OnKeyListener() {
		@Override
		public boolean onKey(View view, int keyCode, KeyEvent event) {
			if(event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
				// 改行させない
				InputMethodManager imm = (InputMethodManager)SoshikiIDDialog.this.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				return true;
			}
			return false;
		}
	};
	
	
	@Override
	public void onPause() {
		super.onPause();
		
		// 値の保存
		Object[] values = new Object[1];
		values[VALUE_SOSHIKI_ID] = ((EditText)this.view.findViewById(R.id.edittext_soshikiid)).getText().toString();
		DialogHolder.INSTANCE.setValues(values);
		DialogHolder.INSTANCE.setCallback(this.callback);
		DialogHolder.INSTANCE.setPositiveListener(this.listener);
	}

}
