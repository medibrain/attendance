package jp.co.tecwt.medi_techno_service.util;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.TimerTask;

import android.os.Handler;

/**
 * 時刻表示クラス。
 */
public class MyTimerTask extends TimerTask {

	public static final int y = 0;
	public static final int M = 1;
	public static final int d = 2;
	public static final int k = 3;
	public static final int m = 4;
	public static final int s = 5;
	public static final int w = 6;
	public static final String[] WEEKS = {"日", "月", "火", "水", "木", "金", "土"};

	private TimerListener listener;
	private Handler hanlder;
	private Calendar cal;
	private TimeZone timeZone;
	private Locale locale;
	private int[] datetimes;


	// interface
	public interface TimerListener {
		public void update(int[] datetimes);
	}

	// constructor
	public MyTimerTask(TimerListener listener) {
		this.listener = listener;
		this.hanlder = new Handler();
		this.datetimes = new int[7];
		this.timeZone = TimeZone.getTimeZone("Asia/Tokyo");
		this.locale = Locale.JAPAN;
	}

	@Override
	public void run() {

		// HandlerでUI操作する
		this.hanlder.post(new Runnable() {
			@Override
			public void run() {
				MyTimerTask.this.cal = Calendar.getInstance(MyTimerTask.this.timeZone, MyTimerTask.this.locale);
				MyTimerTask.this.datetimes[MyTimerTask.y] = MyTimerTask.this.cal.get(Calendar.YEAR);
				MyTimerTask.this.datetimes[MyTimerTask.M] = MyTimerTask.this.cal.get(Calendar.MONTH) + 1;
				MyTimerTask.this.datetimes[MyTimerTask.d] = MyTimerTask.this.cal.get(Calendar.DATE);
				MyTimerTask.this.datetimes[MyTimerTask.k] = MyTimerTask.this.cal.get(Calendar.HOUR_OF_DAY);
				MyTimerTask.this.datetimes[MyTimerTask.m] = MyTimerTask.this.cal.get(Calendar.MINUTE);
				MyTimerTask.this.datetimes[MyTimerTask.s] = MyTimerTask.this.cal.get(Calendar.SECOND);
				MyTimerTask.this.datetimes[MyTimerTask.w] = MyTimerTask.this.cal.get(Calendar.DAY_OF_WEEK) - 1;
				MyTimerTask.this.listener.update(MyTimerTask.this.datetimes);
			}
		});
	}

}
