package jp.co.tecwt.medi_techno_service.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import jp.co.tecwt.medi_techno_service.R;
import jp.co.tecwt.medi_techno_service.data.MyPreference;
import jp.co.tecwt.medi_techno_service.data.Session;
import jp.co.tecwt.medi_techno_service.util.MyHttpClient2.ResultData;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.res.Resources;
import android.os.AsyncTask;

/**
 * POST通信を行うクラス。
 * セッションが切れていた場合、自動的にログインを行う。
 * セッションIDはメモリ保存とし、アプリが終了した場合は破棄される。
 */
public class MyHttpClient2 extends AsyncTask<Void, Void, ResultData> {

	
	protected String url;
	protected String loginUrl;
	protected boolean isTimeout;
	protected int connectTimeout;
	protected int readTimeout;
	protected HashMap<String, String> list;
	protected Responser responser;
	
	protected ProgressDialog dialog;
	protected int style;
	protected Context context;
	protected String title;
	protected String message;
	protected OnCancelListener cancelListener;
	
	
	/**
	 * レスポンス受け取り先。
	 */
	public interface Responser {
		public void onFailed();
		public void onError(String errorcode, String message);
		public void onCompleted(String json);
	}
	
	public MyHttpClient2(Context context, String url, Responser responser) {
		
		this.context = context;
		
		if(context != null && url != null) {
			String baseURL = MyPreference.getInstance(context).getURLAuto(context);
			// Request URL
			this.url = baseURL + url;
			// Login URL
			this.loginUrl = baseURL + context.getResources().getString(R.string.login_url);
		}else {
			this.url = null;
		}
		
		this.responser = responser;		
		this.list = new HashMap<String, String>();
	}
	
	/**
	 * レスポンス受け取り先登録。
	 * @param responser
	 */
	public synchronized void setResponser(Responser responser) {
		this.responser = responser;
	}
	
	/**
	 * プログレスダイアログを表示する。
	 * @param context
	 * @param title
	 * @param message
	 * @param cancelListener
	 */
	public void enabledProgressDialog(Context context, String title, String message, OnCancelListener cancelListener) {
		this.context = context;
		this.title = title;
		this.message = message;
		this.cancelListener = cancelListener;
		this.style = ProgressDialog.STYLE_SPINNER;
	}
	
	/**
	 * タイムアウトを設定する。
	 * @param connectTimeout
	 * @param readTimeout
	 */
	public void setTimeOut(int connectTimeout, int readTimeout) {
		if(connectTimeout < 0 && readTimeout < 0) {
			this.isTimeout = false;
		}else {
			this.isTimeout = true;
		}
		this.connectTimeout = connectTimeout;
		this.readTimeout = readTimeout;
	}
	
	/**
	 * パラメータを設定する。
	 * @param key
	 * @param value
	 * @return
	 */
	public MyHttpClient2 addPostParam(String key, String value) {
		this.list.put(key, value);
		return this;
	}
	
	/**
	 * プログレスダイアログのスタイルを設定する。
	 * @param style
	 */
	public void setProgressDialogStyle(int style) {
		this.style = style;
	}
	
	protected void showProgress() {
		if(this.context == null) {
			return;
		}
		
		this.dialog = new ProgressDialog(this.context);
		this.dialog.setTitle(this.title);
		this.dialog.setMessage(this.message);
		this.dialog.setProgressStyle(this.style);
		if(this.cancelListener != null) {
			this.dialog.setCancelable(true);
			this.dialog.setOnCancelListener(this.cancelListener);
		}else {
			this.dialog.setCancelable(false);
		}
		this.dialog.show();	
	}
	
	/**
	 * プログレスダイアログを閉じる。
	 */
	public synchronized void closeProgress() {
		if(this.dialog != null) {
			this.dialog.dismiss();
		}
	}
	
	protected String getParams() {
		// key=value&key=value&...の形式にする
		StringBuilder builder = new StringBuilder();
		for(Map.Entry<String, String> entry : this.list.entrySet()) {
			builder.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
		}
		return builder.toString();
	}
	
	/**
	 * 通信を開始する。
	 */
	public void execute() {
		if(this.url == null) {
			return;
		}		
		super.execute();
	}
	

	/**
	 * execute()後に実行される。
	 */
	@Override
	public void onPreExecute() {
		this.showProgress();
	}
	
	/**
	 * onPreExecute()後に実行される。
	 */
	@Override
	public ResultData doInBackground(Void... no) {
		
		try {			
			
			ResultData result;
			
			// 古いセッションID
			String sessionID = this.getOldSessionID();
			
			// リクエスト送信
			result = this.postRequest(this.url, sessionID);
			
			// 取得確認
			if(!this.isSessionDisconnectOrHumanError(result)) {
				// 正常取得
				return result;
			}
			
			// セッション切れ以外のエラー
			if(result.isError && !result.isSessionError) {
				return result;
			}
			
			// セッション切れのためログイン
			result = this.postLogin(this.loginUrl);
			
			// ログイン時にエラー発生
			if(result.isError) {
				return result;
			}
			
			// 新しいセッションID
			String newSessionID = this.getNewSessionID();
			
			// 再度リクエスト送信
			result = this.postRequest(this.url, newSessionID);
			
			// 最終確認
			if(this.isSessionDisconnectOrHumanError(result)) {
				// エラー発生
				return result;
			}

			// 正常取得
			return result;

			
		} catch(IOException ex) {
			ex.printStackTrace();
		} catch(JSONException ex) {
			ex.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * 保存されているセッションIDを取得する。
	 * @return
	 */
	public String getOldSessionID() {
		
		// Cookie管理
		CookieManager cookieManager = new CookieManager();
		cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
		CookieHandler.setDefault(cookieManager);
		Locale.setDefault(Locale.US);
		Session.INSTANCE.setCookieManager(cookieManager);
		
		return Session.INSTANCE.getSessionID();
	}
	
	/**
	 * リクエスト送信を行う。
	 * @param url
	 * @param sessionID
	 * @return
	 * @throws IOException
	 */
	protected ResultData postRequest(String url, String sessionID) throws IOException {

		// 通信準備
		URLConnection connection = new URL(url).openConnection();
		connection.setDoOutput(true);	// POST
		if(this.isTimeout) {
			connection.setConnectTimeout(this.connectTimeout);
			connection.setReadTimeout(this.readTimeout);
		}
		
		// セッションID登録
		if(sessionID != null) {
			connection.setRequestProperty("Cookie", "JSESSIONID=" + sessionID);
		}
		
		// パラメータ追加
		PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(connection.getOutputStream() , "UTF-8"));
		printWriter.print(this.getParams());
		printWriter.close();

		// レスポンス受け取り
		String str = this.getResponse(connection);
        
		// 専用オブジェクトで返す
        return new ResultData(str, null, null);
	}
	
	/**
	 * レスポンスを受け取る。
	 * @param connection
	 * @return
	 * @throws IOException
	 */
	protected String getResponse(URLConnection connection) throws IOException {
		InputStream objStream = connection.getInputStream();
        InputStreamReader objReader = new InputStreamReader(objStream, "UTF-8"); 
        BufferedReader objBuf = new BufferedReader(objReader);  
        StringBuilder objBuilder = new StringBuilder();  
        String sLine;  
        while((sLine = objBuf.readLine()) != null){  
        	objBuilder.append(sLine);  
        }
        String str = objBuilder.toString(); 
        objStream.close();
        objReader.close();
        return str;
	}
	
	/**
	 * セッション切れ等のエラーを確認。
	 * @param result
	 * @return
	 * @throws JSONException
	 */
	protected boolean isSessionDisconnectOrHumanError(ResultData result) throws JSONException {
		return this.jsonParse(result).isError;
	}
	
	/**
	 * JSONを解析する。
	 * @param result
	 * @return
	 * @throws JSONException
	 */
	protected ResultData jsonParse(ResultData result) throws JSONException {
		
		// root
		JSONObject rootObj = new JSONObject(result.json);
		
		String status = rootObj.getString("status");
		if(status.equals("error")) {
			
			result.isError = true;
			
			String errorcode = rootObj.getString("errorcode");
			result.errorcode = errorcode;
			
			if(errorcode.equals("session-error")) {
				// セッション切れ
				result.isSessionError = true;
				return result;
			}
			
			// その他のエラー
			result.message = rootObj.getString("message");
			return result;
		}
		
		// 正常
		return result;
	}
	
	/**
	 * ログインを行う。
	 * @param url
	 * @return
	 * @throws IOException
	 * @throws JSONException
	 */
	public ResultData postLogin(String url) throws IOException, JSONException {
		
		// 接続準備
		URLConnection connection = new URL(url).openConnection();
		connection.setDoOutput(true);	// POST
		
		// パラメーター追加
		PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(connection.getOutputStream() , "UTF-8"));
		printWriter.print(this.getLoginParams());
		printWriter.close();
		
		// レスポンス受け取り
		String str = this.getResponse(connection);

		// 専用オブジェクトで返す
		ResultData result = new ResultData(str, null, null);
		return this.jsonParse(result);
	}
	
	/**
	 * ログイン用パラメーターを作成する。
	 * @return
	 */
	protected String getLoginParams() {
		StringBuilder builder = new StringBuilder();
		Resources res = this.context.getResources();
		MyPreference pref = MyPreference.getInstance(context);
		builder.append(res.getString(R.string.login_param_name1)).append("=").append(res.getString(R.string.login_param_value1)).append("&");
		builder.append(res.getString(R.string.login_param_name2)).append("=").append(pref.getSoshikiID()).append("&");
		builder.append(res.getString(R.string.login_param_name3)).append("=").append(pref.getDeviceID()).append("&");
		builder.append(res.getString(R.string.login_param_name4)).append("=").append(pref.getPassword()).append("&");
		return builder.toString();
	}
	
	/**
	 * 新しいセッションIDを保存＆取得する。
	 * @return
	 */
	public String getNewSessionID() {
		
		// Cookie Load
		CookieStore cs = Session.INSTANCE.getCookieManager().getCookieStore();
		List<HttpCookie> cookies = cs.getCookies();
		
		// SessionID Load
		String sessionID = null;
		if(cookies != null && cookies.size() > 0) {
			sessionID = cookies.get(0).getValue();
			Session.INSTANCE.setSessionID(sessionID);
		}
		
		return sessionID;
	}	
	
	/**
	 * doInBackground()後に実行される。
	 */
	@Override
	public void onPostExecute(ResultData result) {
		this.closeProgress();
		
		synchronized(this) {
			
			if(this.responser == null) {
				// 何もしない
				return;
			}
			
			if(result == null) {
				// エラー
				this.responser.onFailed();
				return;
			}
			
			if(result.isError) {
				// 人為的エラー
				this.responser.onError(result.errorcode, result.message);
				return;
			}
			
			// 正常
			this.responser.onCompleted(result.json);
		}
	}
	
	/**
	 * URLエンコードを行う。
	 * *,-は手動パーセントエンコード。半角スペースは+からさらにパーセントエンコードする。
	 * @param str
	 * @return
	 */
	public static String urlEncode(String str) {
		if(str == null) {
			return null;
		}
		
		String charset = "UTF-8";
		try {
			
			// Java標準
			str = URLEncoder.encode(str, charset);
			
			// *
			str = str.replaceAll("\\*", "%2a");
			
			// -
			str = str.replaceAll("\\-", "%2d");
			
			// 半角スペース
			str = str.replaceAll("\\+", "%20");
			
		} catch(UnsupportedEncodingException ex) {
			ex.printStackTrace();
		}
		return str;
	}
	
	
	/**
	 * レスポンス専用クラス。
	 */
	public class ResultData {
		public String json;
		public String errorcode;
		public String message;
		public boolean isSessionError;
		public boolean isError;
		public ResultData(String json, String errorcode, String message) {
			this.json = json;
			this.errorcode = errorcode;
			this.message = message;
		}
	}

}
