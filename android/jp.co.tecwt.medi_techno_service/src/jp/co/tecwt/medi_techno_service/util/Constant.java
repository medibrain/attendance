package jp.co.tecwt.medi_techno_service.util;

import java.util.Locale;
import java.util.TimeZone;

public class Constant {

	public static TimeZone TIMEZONE = TimeZone.getTimeZone("Asia/Tokyo");
	public static Locale LOCALE = Locale.JAPAN;

}
