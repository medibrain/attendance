package jp.co.tecwt.medi_techno_service.util;

import jp.co.tecwt.medi_techno_service.data.MyPreference;
import jp.co.tecwt.medi_techno_service.view.GyoumuActivity;
import jp.co.tecwt.medi_techno_service.view.KoutsuuhiActivity;
import jp.co.tecwt.medi_techno_service.view.KyukeiActivity;
import jp.co.tecwt.medi_techno_service.view.Main;
import jp.co.tecwt.medi_techno_service.view.MoushideMaisuuActivity;
import jp.co.tecwt.medi_techno_service.view.NfcReadActivity;
import jp.co.tecwt.medi_techno_service.view.SyoriMaisuuActivity;
import jp.co.tecwt.medi_techno_service.view.SyubetsuActivity;
import jp.co.tecwt.medi_techno_service.view.TimeActivity;
import android.content.Context;
import android.content.Intent;

/**
 * 出退のルートを定義する。
 * 画面が戻るなどした場合は感知できないので、手動でページ数を操作する。NextAct.countDown()
 */
public class NextAct {
	
	public static final String ISHISSU = "isHissu";
	
	
	// 出勤
	private static final Holder[] ROUTE_SYUKKIN = 
	{
		new Holder(TimeActivity.class, MyPreference.KEY_SYUKKIN_EDITTIME),
	};
	private static int syukkinCount;

	
	// 移動
	private static final Holder[] ROUTE_IDOU = 
	{
		new Holder(TimeActivity.class, MyPreference.KEY_IDOU_EDITTIME),
		new Holder(SyubetsuActivity.class, MyPreference.KEY_IDOU_SYUBETSU),
		new Holder(GyoumuActivity.class, MyPreference.KEY_IDOU_GYOUMU), 
		new Holder(SyoriMaisuuActivity.class, MyPreference.KEY_IDOU_SYORIMAISUU), 
		new Holder(MoushideMaisuuActivity.class, MyPreference.KEY_IDOU_MOUSHIDEMAISUU),  
	};
	private static int idouCount;
	
	
	// 退勤
	private static final Holder[] ROUTE_TAIKIN = 
	{
		new Holder(TimeActivity.class, MyPreference.KEY_TAIKIN_EDITTIME),
		new Holder(SyubetsuActivity.class, MyPreference.KEY_TAIKIN_SYUBETSU),
		new Holder(GyoumuActivity.class, MyPreference.KEY_TAIKIN_GYOUMU), 
		new Holder(SyoriMaisuuActivity.class, MyPreference.KEY_TAIKIN_SYORIMAISUU),
		new Holder(MoushideMaisuuActivity.class, MyPreference.KEY_TAIKIN_MOUSHIDEMAISUU), 
		new Holder(KyukeiActivity.class, MyPreference.KEY_TAIKIN_KYUKEI), 
		new Holder(KoutsuuhiActivity.class, MyPreference.KEY_TAIKIN_KOUTSUUHI),  
	};
	private static int taikinCount;
	
	
	public static void init() {
		syukkinCount = 0;
		idouCount = 0;
		taikinCount = 0;
	}
	
	/**
	 * 遷移する先のIntentを取得する。
	 * @param context
	 * @param route
	 * @return
	 */
	public static Intent getNextIntent(Context context, int route) {
		switch(route) {
		case Main.SYUKKIN:
			return NextAct.syukkin(context);
		case Main.IDOU:
			return NextAct.idou(context);
		case Main.TAIKIN:
			return NextAct.taikin(context);
		}
		return null;
	}
	
	/**
	 * 出勤ルート。
	 * @param context
	 * @return
	 */
	private static Intent syukkin(Context context) {
		if(ROUTE_SYUKKIN.length  == syukkinCount) {
			// NFC読み取り画面へ移動する
			NextAct.syukkinCount++;
			return getFinalIntent(context);
		}
		
		Intent intent = null;
		
		// 遷移先画面の状態に応じて変化する
		int status = MyPreference.getInstance(context).getNyuuryokuKoumokuCheckedPosition(ROUTE_SYUKKIN[NextAct.syukkinCount].key);
		Class<?> nextClass = null;
		boolean isHissu = true;
		switch(status) {
		case MyPreference.STATUS_HISSU:
			nextClass = ROUTE_SYUKKIN[NextAct.syukkinCount].nextClass;
			isHissu = true;
			break;
		case MyPreference.STATUS_VISIBLE:
			nextClass = ROUTE_SYUKKIN[NextAct.syukkinCount].nextClass;
			isHissu = false;
			break;
		case MyPreference.STATUS_INVISIBLE:
			// ひとつ飛ばしてもう一回確認する
			NextAct.syukkinCount++;
			return NextAct.syukkin(context);
		}
		
		// ページ数カウントアップ
		NextAct.syukkinCount++;
		
		// 必須かどうかのフラグをセット
		intent = new Intent(context, nextClass);
		intent.putExtra(NextAct.ISHISSU, isHissu);
		
		return intent;
	}
	
	/**
	 * 移動ルート。
	 * @param context
	 * @return
	 */
	private static Intent idou(Context context) {
		
		if(ROUTE_IDOU.length == idouCount) {
			// NFC画面へ移動
			NextAct.idouCount++;
			return getFinalIntent(context);
		}
		
		// 遷移先の状態に合わせて変化する
		int status = MyPreference.getInstance(context).getNyuuryokuKoumokuCheckedPosition(ROUTE_IDOU[NextAct.idouCount].key);
		Class<?> nextClass = null;
		boolean isHissu = true;
		switch(status) {
		case 0:
			nextClass = ROUTE_IDOU[NextAct.idouCount].nextClass;
			isHissu = true;
			break;
		case 1:
			nextClass = ROUTE_IDOU[NextAct.idouCount].nextClass;
			isHissu = false;
			break;
		case 2:
			// 一つ飛ばして再度確認
			NextAct.idouCount++;
			return NextAct.idou(context);
		}
		
		// ページ数カウントアップ
		NextAct.idouCount++;
		
		// 必須かどうかのフラグをセット
		Intent intent = new Intent(context, nextClass);
		intent.putExtra(NextAct.ISHISSU, isHissu);
		
		return intent;
	}
	
	/**
	 * 退勤ルート。
	 * @param context
	 * @return
	 */
	private static Intent taikin(Context context) {
		
		if(ROUTE_TAIKIN.length == taikinCount) {
			// NFC画面へ移動
			NextAct.taikinCount++;
			return getFinalIntent(context);
		}
		
		// 遷移先の状態に合わせて変化する
		int status = MyPreference.getInstance(context).getNyuuryokuKoumokuCheckedPosition(ROUTE_TAIKIN[NextAct.taikinCount].key);
		Class<?> nextClass = null;
		boolean isHissu = true;
		switch(status) {
		case 0:
			nextClass = ROUTE_TAIKIN[NextAct.taikinCount].nextClass;
			isHissu = true;
			break;
		case 1:
			nextClass = ROUTE_TAIKIN[NextAct.taikinCount].nextClass;
			isHissu = false;
			break;
		case 2:
			// 一つ飛ばして再度確認
			NextAct.taikinCount++;
			return NextAct.taikin(context);
		}
		
		// ページ数カウントアップ
		NextAct.taikinCount++;
		
		// 必須かどうかのフラグをセット
		Intent intent = new Intent(context, nextClass);
		intent.putExtra(NextAct.ISHISSU, isHissu);
		
		return intent;
	}
	
	/**
	 * 最後の画面。
	 * @param context
	 * @return
	 */
	private static Intent getFinalIntent(Context context) {
		return new Intent(context, NfcReadActivity.class);
	}
	
	/**
	 * ページ数をカウントダウンする。
	 * @param route
	 */
	public static void countDown(Context context, int route) {
		switch(route) {
		case Main.SYUKKIN:
			syukkinCount--;
			if(syukkinCount < 0 || syukkinCount >= ROUTE_SYUKKIN.length) break;	// NFCまで行くと範囲外に出てしまうのでチェック
			if(MyPreference.getInstance(context).getNyuuryokuKoumokuCheckedPosition(ROUTE_SYUKKIN[syukkinCount].key) == 2) {
				// 非表示なら一つ飛ばして再度確認
				NextAct.countDown(context, route);
			}
			break;
		case Main.IDOU:
			idouCount--;
			if(idouCount < 0 || idouCount >= ROUTE_IDOU.length) break;
			if(idouCount > 0 && MyPreference.getInstance(context).getNyuuryokuKoumokuCheckedPosition(ROUTE_IDOU[idouCount].key) == 2) {
				NextAct.countDown(context, route);
			}
			break;
		case Main.TAIKIN:
			taikinCount--;
			if(taikinCount < 0 || taikinCount >= ROUTE_TAIKIN.length) break;
			if(taikinCount > 0 && MyPreference.getInstance(context).getNyuuryokuKoumokuCheckedPosition(ROUTE_TAIKIN[taikinCount].key) == 2) {
				NextAct.countDown(context, route);
			}
			break;
		}
	}
	
	
	/**
	 * クラス情報保存用。
	 */
	private static class Holder {
		public Class<?> nextClass;
		public String key;
		public Holder(Class<?> nextClass, String MyPereference_FieldKey) {
			this.nextClass = nextClass;
			this.key = MyPereference_FieldKey;
		}
	}

}
