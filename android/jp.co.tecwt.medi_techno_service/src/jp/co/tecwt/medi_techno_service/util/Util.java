package jp.co.tecwt.medi_techno_service.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public class Util {

	/**
	 * 文字列を数値に変換します。<br>
	 * 失敗した場合は-1を返します。
	 * @param number
	 * @return
	 */
	public static int ToInt(String number) {
		try {
			return Integer.parseInt(number);
		} catch (NumberFormatException ex) {
			return -1;
		}
	}

	/**
	 * 文字列を数字の配列に変換します。<br>
	 * 失敗した場合はnullを返します。
	 * @param number
	 * @return
	 */
	public static int[] ToInts(String number) {
		try {
			int[] ints = new int[number.length()];
			int i = 0;
			for (char c : number.toCharArray()) {
				int conv = ToInt(String.valueOf(c));
				ints[i++] = conv;
			}
			return ints;
		} catch (NumberFormatException ex) {
			return null;
		}
	}

	/**
	 * アプリのバージョンコード(int)を取得します。失敗した場合は-1を返します。
	 * @param con
	 * @return
	 */
	public static int GetApplicationVersionCode(Context con) {
		try {
			PackageManager pm = con.getPackageManager();
			PackageInfo pi = pm.getPackageInfo(con.getPackageName(), PackageManager.GET_ACTIVITIES);
			return pi.versionCode;
		} catch (Exception ex) {
			return -1;
		}
	}

	/**
	 * アプリのバージョン名（x.y.z）を取得します。失敗した場合はnullを返します。
	 * @param con
	 * @return
	 */
	public static String GetApplicationVersionName(Context con) {
		try {
			PackageManager pm = con.getPackageManager();
			PackageInfo pi = pm.getPackageInfo(con.getPackageName(), PackageManager.GET_ACTIVITIES);
			return pi.versionName;
		} catch (Exception ex) {
			return null;
		}
	}
}
