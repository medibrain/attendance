package jp.co.tecwt.medi_techno_service.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;

import android.content.Context;

public class MyHttpClient3_Download extends MyHttpClient2 {

	public Responser responser;
	public File tmpApkFile;

	public interface Responser {
		public abstract void onFailed();
		public abstract void onError(String errorcode, String message);
		public abstract void onCompleted(File tmpApkFile);
	}

	public MyHttpClient3_Download(Context context, String url, Responser responser) {
		super(context, url, null);
		this.responser = responser;
	}

	@Override
	public ResultData doInBackground(Void... no) {
		ResultData result;

		try {

			// セッション
			String sessionID = this.getOldSessionID();

			// コネクション
			HttpURLConnection con = connect(new URL(url), sessionID);

			// コンテントタイプ
			if(!isSuccessConnect(con)) {
				// セッション取り直し
				result = this.postLogin(this.loginUrl);
				if(result.isError) {
					result.errorcode = "login-1";
					result.message = "ログインエラー。";
					return result;
				}
				String newSessionID = this.getNewSessionID();
				con = connect(new URL(url), newSessionID);
				if(!isSuccessConnect(con)) {
					result.errorcode = "login-2";
					result.message = "ログインエラー。";
					return result;
				}
			}

			// レスポンス
			InputStream in = con.getInputStream();

			// 保存先
			File cacheDir = context.getExternalCacheDir();
			tmpApkFile = File.createTempFile("apk", ".apk", cacheDir);
			FileOutputStream out = new FileOutputStream(tmpApkFile);

			// ファイル生成
			byte[] buffer = new byte[1024];
			int bufferLength = 0;
			int max = con.getContentLength();
			int total = 0;
			while((bufferLength = in.read(buffer)) > 0) {
				out.write(buffer, 0, bufferLength);
				total += bufferLength;
				dialog.setProgress((int)(total * 100 / max));
			}
			out.close();
			in.close();

			result = new ResultData("{\"status\":\"success\"}", null, null);
			return result;

		} catch(IOException ex) {
			ex.printStackTrace();
			result = new ResultData("{\"status\":\"error\"}", null, null);
			result.isError = true;
			result.errorcode = "download-1";
			result.message = "通信に失敗しました。"+ex.getMessage();
			return result;
		} catch(JSONException ex) {
			ex.printStackTrace();
			result = new ResultData("{\"status\":\"error\"}", null, null);
			result.isError = true;
			result.errorcode = "download-2";
			result.message = "JSON解析に失敗しました。"+ex.getMessage();
			return result;
		} catch(Exception ex) {
			ex.printStackTrace();
			result = new ResultData("{\"status\":\"error\"}", null, null);
			result.isError = true;
			result.errorcode = "download-3";
			result.message = "ダウンロード中にエラーが発生しました。"+ex.getMessage();
			return result;
		}
	}

	protected HttpURLConnection connect(URL url, String sessionID) throws IOException {
		HttpURLConnection con = (HttpURLConnection)url.openConnection();
		// ヘッダ
		con.setRequestProperty("Cookie", "JSESSIONID=" + sessionID);
		// POST
		con.setRequestMethod("POST");
		con.setDoOutput(true);
		// タイムアウト設定
		if(this.isTimeout) {
			con.setConnectTimeout(this.connectTimeout);
			con.setReadTimeout(this.readTimeout);
		}
		// パラメータ追加
		String params = this.getParams();
		if(params != null && !params.equals("")) {
			PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(con.getOutputStream() , "UTF-8"));
			printWriter.print(params);
			printWriter.close();
		}

		return con;
	}

	protected boolean isSuccessConnect(HttpURLConnection con) throws Exception {
		String contentType = con.getContentType();
		if(contentType == null) {
			throw new Exception("コンテントタイプを取得できませんでした。");
		}
		if(contentType.equals("application/json")) {
			// セッション切れ
			return false;
		}
		return true;
	}

	@Override
	public void onPostExecute(ResultData result) {
		this.closeProgress();

		synchronized(this) {

			if(this.responser == null) {
				// 何もしない
				return;
			}

			if(result == null) {
				// エラー
				this.responser.onFailed();
				return;
			}

			if(result.isError) {
				// 人為的エラー
				this.responser.onError(result.errorcode, result.message);
				return;
			}

			// 正常
			this.responser.onCompleted(tmpApkFile);
		}
	}

}
